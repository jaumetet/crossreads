[
	{
		"pid" : 0,
		"p" : " <p> <b>MARC</b></p><p> Els títols de les obres d'un artista composen sovint un vocabulari personal, esdevenen una mena de trets que dibuixen un autoretrat de la seva personalitat i dels plantejaments de la seva obra. A l'obra de Carles Pujol dels darrers quinze anys hi ha alguns títols que remeten al camp de la pintura, en el qual inicià la seva trajectòria artística, però que abandonaria gradualment -vers la meitat dels anys setanta-, per eixamplar els plantejaments del seu art amb la tridimensionalitat de les instal.lacions, la dimensió temporal pròpia del vídeo, i la suma de totes quatre dimensions en allò que anomenem vídeo-instal.lacions. I tanmateix, més d'un cop els títols introdueixen termes vinculats a la pràctica de la pintura, com l'al.lusió (més aviat privada) al format universal d'una tela o bastidor (<i>81 x 65</i>), o bé referències o citacions molt concretes (<i>Meninas</i>, <i>Velázquez</i>), o també termes del seu lèxic més comú (<i>Collage</i> i, ara i aquí, senzillament <i>Pintura</i>).</p>"
	},
	{
		"pid" : 1,
		"p" : "<p> Quan Carles Pujol començava a treballar en vídeo i instal.lacions, sempre insistia que ell es considerava bàsicament un pintor i que no pretenia sinó perllongar una recerca d'arrel plàstica, formal, amb uns mitjans que haurien de permetre-li la introducció d'altres dimensions i components: l'espai tridimensional (i la percepciófísica directa de l'obra per l'espectador), el temps, el moviment, l'atzar, el so, etc., s'afegirien així a uns plantejaments basats en conceptes plàstics elementals tals com línia, superficie, espai, forma, color, textura, perspectiva, composició, etc. La noció de instal.lació i l'ús del vídeo, emperò, han esdevingut finalment els territoris propis de l'art de Carles Pujol i el llenguatge i suports de la pintura -o, potser millor dit, del quadre-, si encara hi són d'alguna manera presents, han pres altres relacions i funcions.</p>"
	},
	{
		"pid" : 2,
		"p" : "<p> Dic això perquè, en les seves instal.lacions, Carles Pujol ha utilitzat sovint bastidors o marcs de fusta i altres materials; superfícies o teles pintades; estructures o cossos geomètrics, i els mateixos materials o elements que ha trobat en les sales o espais d'exposició on han estat presentades. D'entre tots aquests elements, trobo la idea de marc especialment rellevant en moltes de les seves obres: el marc com a codi de la visió i la perspectiva, artifici i engany de la percepció de l'espai; peròtambé com element arquitectònic (<i>Marcs de porta</i>, 1982), espai visual emmarcat per la pantalla de vídeo (o per la projecció de diapositives), marc d'un mirall, etc. I el marc esdevé freqüentment un cos tridimensional distorsionat, fora de lloc o desproporcionat en les seves dimensions. El marc, abans que delimitar, enquadrar, separar allò que hi ha dins d'allò que hi ha fora, més aviat remet a l'espai al voltant, multiplica o relativitza els angles de visió i fa transitable el llindar.</p>"
	},
	{
		"pid" : 3,
		"p" : "<p> Si el vídeo és també una forma d'emmarcar, sovint suposa endemés una determinada manera de cohesionar l'estructura tridimensional de la instal.lació. Per exemple, la imatge en el monitor o monitors-plana, opaca i virtual- \"solda\" incongruentment els elements sòlids que l'envolten, desvetlla una perspectiva oculta que dóna coherència a una figura o forma fragmentada i esparsa en diferents plans de l'espai, multiplica també els punts de vista a través de la mobilitat o pel contrast entre estatisme i moviment, remarca uns elements discrets que esdevenen substància pròpia de l'obra, etc. La pantalla de vídeo és d'altra banda un marc amb vidre, una il.lusió de transparència contradita per l'opacitat immaterial de la imatge-llum i pel volum tridimensional de l'aparell(monitor o televisor) que la produeix.</p>"
	},
	{
		"pid" : 4,
		"p" : "<p>És possible així que el treball de Carles Pujol entri moltes vegades en el terreny de la <i>Tautologia</i>-justament el títol d'una de les seves instal.lacions (batejada així per Carles Hac Mor)-, tanmateix un dels més fecunds en tot l'art i el pensament moderns. Però, tot resseguint el vocabulari i repertori dels títols amb que encetava aquest text, hom podria parlar també del món dels miralls (<i>Alícia!</i>-aquella que de la mà de Lewis Carroll en va travessar un-, <i>Reflex</i>, i les referències evidents i ja esmentades de <i>Meninas</i> i <i>Velázquez</i>).I també dels trencaclosques i d'entranyables invencions d'òptica recreativa (<i>Puzzle</i>, <i>Calidoscopi</i>).I també de les emocions subjectives i abstractes de la música(<i>Homenatge a Erik Satie</i>, <i>Suite</i>, <i>Partita</i>, <i>Cantatrice</i>, etc.). Els títols són gairebé sempre així d'eixuts; de vegades més obvis, més intrigants, o arbitraris. Les obres que designen són similarment d'una eixutesa delusòria, de forma coherent amb el reductivisme al que la pintura última de Carles Pujol s'havia atansat més i més. El rerafons de l'obra d'un artista pot fer-se o no visible a través de cadascuna de les seves peces; aquestes línies voldrien tot just fornir-ne un \"marc\".</p><p> EUGENI BONET</p> "
	}
]