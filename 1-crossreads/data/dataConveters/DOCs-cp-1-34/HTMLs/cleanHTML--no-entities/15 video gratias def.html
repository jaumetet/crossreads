  <p> <b>ViDeo Gratias</b><b>, o «comunicación diferida»: Espacio autobiográfico en la videografía de Juan Downey</b>  </p>
<p>
«La necesidad de hacer vídeo nace como la dependencia del aguarrás, la droga y el médium, del urgente deseo de conectarse. […] Y en efecto, lo que se comunica solamente es ese deseo de conectarse: una comunicación diferida. […] El deseo de hacer público algo personal.» – Juan Downey (1987)</p>
<p>  </p>
<p> El componente autobiográfico, subjetivo o interior ha sido una constante señalada en diversos comentarios o análisis del arte de Juan Downey, tanto como en sus propios escritos. No se puede decir que su obra fuera esencialmente autobiográfica, pero sí que penetra reiteradamente en el espacio de lo autobiográfico o, dicho sea más holgadamente, de lo interior. No es mi propósito el discutir aquí
los «problemas de vocabulario»[[[1]]] la exactitud o impropiedad de determinados conceptos principalmente heredados de lo literario, sino recoger una vez más un motivo y un
ángulo desde el que abordar ahora la obra de Downey con una cierta amplitud; no en su totalidad, ya que tratamos de un artista de una considerable versatilidad en cuanto a las disciplinas y a los medios
(y <i>multimedios</i> o <i>intermedios</i>)
que ha barajado y explorado a lo largo de su trayectoria, pero sí
tratando de abarcar varias de las obras más descollantes –incluyendo algunas un tanto olvidadas o que han permanecido fuera de circulación durante varios años– de lo que hoy es, desdichadamente, un <i>corpus</i> cerrado por la prematura muerte de su autor.</p>
<p>  </p>
<p> Raymond Bellour, siguiendo las dilucidaciones de algunos estudiosos franceses de la autobiografía y otras literaturas interiores,[[[2]]] ha preferido hablar de <i>autorretratos</i> a propósito de la obra videográfica de varios artistas, entre los cuales Juan Downey. La noción de autorretrato, que Bellour toma de los escritos de Beaujour, permitiría esquivar las restricciones implícitas en la acepción más usual de <i>autobiografía</i>, sea en diccionarios generales o especializados, o según la definición propuesta por Lejeune en su célebre ensayo <i>El pacto autobiográfico</i>:
«Relato retrospectivo en prosa que una persona real hace de su propia existencia, poniendo énfasis en su vida individual y, en particular, en la historia de su personalidad.» Por el contrario, el
<i>autorretrato</i> de Beaujour/Bellour escaparía a la rigidez (aunque maleable idiosincrásicamente) de lo retrospectivo y lo sucesivo –y, en consecuencia, de la deidad del <i>cronos</i>–, tanto como de la remisión genérica a los patrones de la prosa, la narratividad e incluso la individualidad entendida literalmente. «El autorretrato –sintetiza Bellour– se distingue primeramente de la autobiografía por la ausencia de toda forma de relato consecutivo.
[…] El autorretrato se sitúa así del lado de la analogía, de lo metafórico, de lo poético, más que de lo narrativo.»[[[3]]] Por otro lado, Lejeune mismo ha ido expandiendo y flexibilizando sus premisas sobre la autobiografía como objeto preciso de estudio, considerando otros conceptos y aspectos más vastos, como los de
<i>literatura interior</i> o <i>espacio autobiográfico</i>, y su traslación a otros medios/media.</p>
<p>  </p>
<p> La noción de autobiografía ha sido utilizada con menos ambages a propósito de los cines anormativos o de vanguardia, sin por ello renunciar a establecer distinciones entre unas expresiones más genéricas y, hasta cierto punto, sinónimas –<i>cine subjetivo</i>, <i>cine del yo</i> o <i>en primera persona</i>, de nuevo la laxitud del <i>espacio autobiográfico</i>–, y otras que desglosan sus tipologías más singulares: el diario o dietario fílmico, el retrato y el autorretrato, el <i>travelogue</i> o memoria de viaje, la carta y la confesión íntima, la reconstrucción genealógica, la ficción autobiográfica, incluso el cine familiar por sus rasgos propios.[[[4]]]  </p>
<p>  </p>
<p> Aparte de reincidir igualmente en unas pautas terminológicas bien asentadas en la teoría e historia de la literatura y las bellas artes (y aunque podamos preguntarnos si tales trasplantes son siempre adecuados, pertinentes y esclarecedores), cabe resaltar seguidamente dos aspectos importantes. En primer lugar, la trascendencia de lo privado a lo público –«el deseo de hacer público algo personal», de que hablaba Downey–, oponiendo un enclave de identidad y subjetividad frente a la pasividad y la anomia favorecidas por la sociedad de la información y el espectáculo. Y, en segundo lugar, la tabla de salvación de los sistemas de pequeño formato y relativamente asequibles para la captura de imágenes y la creación audiovisual, ingeniados y comercializados de cara al aficionado, al pequeño profesional o a otros usos más bien efímeros y recónditos, pero que brindan también la oportunidad de que broten y se consoliden ciertas prácticas, exploraciones y alternativas innovadoras: artes y <i>poiesis</i> audiovisuales, nuevas corrientes documentalistas, antropología visual, cine de vanguardia y underground, videoarte y televisión de guerrilla, formas expandidas y tecnomediales… por enumerar apenas una pequeña porción de un vocabulario recapitulativo o en vigor. Y, sin exagerar en cuanto a cualquier tipo de determinismo tradicionalmente sospechoso y mal visto –sea de orden tecnológico o estético, y finalmente ideológico–, puede intuirse una circunstancia genética propiciadora del peso específico que ha adquirido la subjetividad, la identidad, la afirmación del yo (o del «nosotros», pero a una microescala apropiada), ante los peores rasgos de la cultura de la uniformidad y del androide replicado (antes que replicante).</p>
<p>  </p>
<p> La obra de Juan Downey, tras superar «la dependencia del aguarrás», se orientó hacia una conexión/comunicación <i>de yo a ti</i> por medio de lo tecnológico y lo desmaterializado. Por una parte, comporta, como ya se ha dicho, repetidas incursiones en el espacio autobiográfico –y en los de lo antropológico, cultural, político, comunicacional, procesual, etc.–; por la otra, hoy puede atisbarse
(aunque por añadidura y desde las convenciones de lo retrospectivo)
su entera trayectoria y obra como si fuera parte o reflejo de una especie de autobiografía colectiva, una «autobiografía de todos»
(aludiendo aquí a Gertrude Stein y su <i>Everybody’s Autobiography</i>).
Este es un plano bien común en las escrituras autobiográficas e interiores en el sentido más amplio de la expresión, ya que la exclusiva reclusión en el yo es harto improbable, incluso cuando los demás quedan ocultos o salvaguardados por el anonimato, el esfumino, los pseudónimos, los disfraces de la ficción y las figuras retóricas. En el caso de Downey, no hay un reflejo inmediato de una comunidad artística o subcultural, tal como la que puede hallarse, por ejemplo (y son, con todo, ejemplos bien diversos) en la obra audiovisual de Jonas Mekas, Andy Warhol o Nam June Paik a modo de crónica, álbum de retratos o retrato de grupo. Sin embargo, muchos vericuetos en la obra de Downey devienen extrapolables para todo un colectivo y un tejido de referencias de ubicación inmediata en <i>su</i>
época (e incluso, en alguna medida, en la nuestra).</p>
<p>  </p>
<p> Lo que refleja pues la andadura artística, intelectual y vital de Juan Downey es la efervescencia de determinados motivos e impulsos que hallan su cenit en las vertientes conexas del arte tecnológico, medial, conceptual, procesual, contracultural, híbrido y alternativo de los años sesenta y setenta, y su posterior continuidad y evolución (no siempre progresión y transgresión). Mediante sus máquinas y construcciones interactivas iniciales, Downey se adentró
primeramente en el bricolaje del maquinario tecnológico, que procuraba un intercambio de papeles entre creador y receptor. Los títulos mismos son ya bien explícitos: <i>Do It Yourself</i>, <i>Do Your Own Concert</i>, <i>Spectramate</i>, <i>Selectramate</i>…
Siempre buscando un copartícipe para activar el dispositivo.
Seguidamente, el acercamiento a temas ecológicos, sociopolíticos y urbanísticos (recuérdese que Downey tuvo formación de arquitecto), con planteamientos lindantes con lo idealista y utópico, nos remite a la aproximación de las prácticas artísticas conceptuales a los modelos propios de las ciencias humanas y sociales, y a una estética que desprecia el valor terminal del objeto (que deja de ser precioso). Downey volvería luego a la exploración de los sistemas interactivos, mediante la tecnología del disco láser y otras connivencias tecnomediales.</p>
<p>  </p>
<p> Entretanto, un hallazgo capital en su trayectoria fue el del vídeo y de una incipiente comunidad de exploradores y guerrilleros del audiovisual electrónico. Downey dijo: «El vídeo, más claramente que cualquier otro material o procedimiento artístico, condujo mis propósitos estéticos más cerca de los asuntos políticos y sociales.»[[[5]]] De ahí se derivan las series y capítulos centrales en la obra de Downey: su periplo transamericano (<i>Video Trans Americas</i>), sus ensayos sobre los sistemas de signos y de valores en la cultura occidental (<i>The Thinking Eye</i>, <i>Hard Times and Culture</i>), más sus airadas regresiones al tambaleante estado de su tierra madre como capítulo aparte (<i>Chile June 1971, Publicness</i>, <i>In the Beginning</i>, <i>Bi-Deo</i>, <i>Chicago Boys</i>, <i>The Motherland</i>, <i>No</i>, <i>The Return of the Motherland</i>).</p>
<p>  </p>
<p> Sus primeros acercamientos al vídeo (mono y multicanal), lo fueron también a la performance y la instalación como extensión multimedia o tecnomedial; pero tales distingos son un tanto superfluos, ya que prácticamente no existían en aquella época. Una cosa llevaba a la otra, precisamente por el estado de indefinición, incluso algo salvaje, de unas tecnologías, medios y conceptos que constituían aún terrenos vírgenes, novedosos y chocantes (para los artistas curiosos y más aún para el público). Ese estado silvestre de una tecnología relativamente reciente como el vídeo favorecía, entre otras cosas, unos usos imaginativos de sus aparentes desventajas o precariedades por comparación con otros medios más evolucionados como el cine: baja resolución; frecuentes deficiencias en cuanto a las posibilidades de elaboración/posproducción del material; una cualidad harto efímera pese a que, por momentos, se creyera todo lo contrario… (A la par que, por otra parte, uno se maravillaba de otros aspectos: instantaneidad, portabilidad, fonocaptación integrada, bajo coste y reciclabilidad del soporte, etc.). En un contexto extratelevisivo, ajeno a las industrias del entretenimiento y espectáculo –más bien por oposición a ellas–, y al margen de los laboratorios de televisión mejor dotados (que Downey también visitó), el vídeo devino un arte plural y expandido; instigó (y/o se contagió de) lo procesual y lo mixto, la polivisión y la bidireccionalidad, la síntesis y tratamiento de imágenes, lo ambiental y envolvente, lo contracultural y lo político, etc.</p>
<p>  </p>
<p> La deambulación nómada es otro factor recurrente en la observación de la obra y la trayectoria vital de Downey. Recojo las observaciones de un par de coterráneos suyos que hablan de «un fuerte componente de extranjería y, por ello, de distancia»,[[[6]]] y de cualquier obra suya como «un viaje, una expedición, no solo a un lugar específico, sino a la interioridad del autor y a la nuestra».[[[7]]]Así, tras un primer ciclo videográfico al encuentro de sus raíces al sur del norte –de ese norte que, tan a menudo, se hace llamar arrogantemente como si fuera el continente entero de América–, tanto la serie de <i>El ojo pensante</i>, como la que apenas había iniciado a su muerte, representan otras rutas de viaje de ida y vuelta entre culturas y temporalidades diversas, y entre el espacio introspectivo y el abierto al ancho mundo.</p>
<p>  </p>
<p> <i>Moving</i>
(1974) es como un <i>road movie</i> esencialista que condensa el inicial periplo transamericano de Downey. Describe un viaje que se orienta primero hacia el oeste
(Estados Unidos) y consecutivamente hacia el sur (Perú y Bolivia), mediante diversos medios de locomoción, en compañía y en solitario, y pasando de los sonidos mediáticos y mecanizados a los folclóricos y en estado nativo del <i>rumbo sur</i> emprendido. Y <i>Moving</i>, en castellano, se traduce ambivalentemente, tal como ya se encargó
de subrayar su autor: <i>en movimiento</i> y <i>emotivo</i>.[[[8]]] Razón de más para resaltar este vídeo elemental (y que algunos ojos verían casi como <i>privado</i>, en el sentido de propio de un amateur)
en tanto que pieza emblemática del conjunto <i>Video Trans Americas</i> y de las diversas expediciones y entregas temáticas que lo integran a lo largo de la década de los setenta, puesto que revela el desdoblamiento temático básico (y, a partir, de ahí múltiple) del ciclo entero: el viaje y las emociones (viaje a los orígenes, travesía de autodescubrimiento, tránsito vital y odisea transcultural).</p>
<p>  </p>
<p> Desde
<i>Video Trans Americas</i>, toda la obra de Juan Downey constituye un autorretrato en obstinado movimiento, en pertinaz exploración de sí mismo y de sus herramientas. «El video es un espejo que puede ser manipulado tanto en el tiempo como en el espacio» escribió. Y algo después, en el mismo texto: «Yo, el agente del cambio, al manipular el video para descodificar mis propias raíces, quedé por siempre descifrado y me convertí en un verdadero fruto de mi tierra, menos intelectual y más poético.»[[[9]]]</p>
<p>  </p>
<p> Downey grabó entre 1976 y 1977, durante una estancia en la selva amazónica junto a los yanomamis y acompañado también por su familia, las imágenes de <i>The Laughing Alligator</i>
–<i>El caimán con la risa de fuego</i>, en una versión en español, anterior y más breve– y (amén de otras piezas, variaciones y títulos) su traslación como instalación, <i>The Circle of Fires</i>; las piezas culminantes del ciclo transamericano, con una elaboración que puede emplazarse al otro extremo de <i>Moving</i> y otros vídeos mono o multicanal de apenas unos años antes. Observo esto porque, a partir de aquí, su obra (como la de otros artistas y activistas de su generación) adquiere otro tipo de acabados y giros aparentemente más admisibles desde unos puntos de vista tanto televisivos como museísticos, lo que nos conducirá inmediatamente a su producción durante los años ochenta y hasta la fecha de su muerte. (Sin embargo, Downey había anunciado su intención de simultanear el uso de unos medios y formatos comparativamente más exiguos o humildes.)</p>
<p>  </p>
<p> <i>The Laughing Alligator</i> tiene todavía muchos elementos de <i>travelogue</i>
íntimo, entre las atribuciones genéricas que pudieran hacérsele por una parte al documentalista-antropólogo, por otra parte, al turista exótico (aunque nada accidental), introduciendo sin embargo un sinnúmero de fracturas o luxaciones: fragmentación del sujeto narrador y alternancia de voces (aunque se trate de miembros de la minúscula comunidad familiar visitante), alegorías y anacronismos varios con los que el autor firmante interfiere en el hilo documental, la inestable sensación que produce un estilo de montaje abundante en planos cortos que se suceden entrecortados por saltos constantes (<i>jump cuts</i>), la movilidad de la cámara-ojo y su falta de respeto por los encuadres euclidianos, y algunas otras disfunciones respecto a la gramática normativa del documental. Por último, es la voz de Downey la que introduce las motivaciones y premisas interiores, las inquietudes y emociones sentidas, los sentimientos de igualdad y distancia recogidos al cabo de la convivencia con los yanomamis y de regreso al mundo «civilizado».</p>
<p>  </p>
<p> Ese regreso/reingreso y su digestión le llevaron a emprender otros viajes «para mirar la cultura occidental con otros ojos».[[[10]]] Me refiero a la serie <i>The Thinking Eye</i>, título que, aunque remite literalmente a una expresión de Paul Klee, puede interpretarse de nuevo en un sentido ambivalente según la fonética inglesa (<i>Eye/I</i>, es decir: ojo y yo). Tanto su precedente en la doble versión/interpretación –como cinta monocanal y como instalación intermedia– de <i>Las Meninas</i>
(<i>The Maidens of Honor</i>)
según Downey (más las citas incorporadas de Foucault y Kubler), un conjunto fechado entre 1975-1976, como la primera entrega de la serie propiamente dicha –el video <i>The Looking Glass </i>(El espejo, 1982)–, se recrean en la equivalencia entre una cosa y otra, apelando/interpelando a un legado cultural occidental particularmente fuerte, sólido y persistente, cargado por tanto de ideologías y ecos varios. Del mito de Narciso se asciende hasta el espejo/reflejo de la imagen tecnológica, pasando por otras recreaciones estéticas e intelectuales mediante poliédricas alusiones a la obra pictórica de Velázquez, Van Eyck, Holbein y Picasso; a la arquitectura aristocrática de Versalles, Fontainebleau y El Escorial; al psicoanálisis, al estructuralismo, a la iconología y a la posmodernidad. En este sentido, <i>The Looking Glass</i> es como una enciclopedia de bolsillo, aunque caprichosa y ligeramente enloquecida debido a su clara firma subjetiva con ostensibles incisos de humor (si bien incorpora de nuevo, tal como en <i>Las Meninas (The Maidens of Honor)</i>, otras citas textuales: ahora mediante intervenciones como las de Leo Steinberg o Eunice Lipton, por destacar las más sustanciales).</p>
<p>  </p>
<p> Otras realizaciones sucesivas del mismo ciclo –con el esclarecedor subtítulo de <i>Culture as an Instrument of Active Thought</i>–
nos descubren de qué se trata en este viaje transcultural y semiótico que más bien prescinde de la brújula, del metrónomo o de otros instrumentos de mesura. Downey parafrasea los estilos y formatos habituales en el documental cultural televisivo, juega con sus estereotipos y, frente a la pereza alentada por lo divulgativo y premasticado, opone el propósito activo de lo cognitivo. El «ojo pensante» implica un ojo <i>vigilante</i>, escudriñador de mitos, hitos y contratos socioculturales que, ya sea en primer o último término, comportan una mediación institucionalizadora. Una mirada alerta (y alertadora) ante las cortas miras de una cultura aposentada.  </p>
<p>  </p>
<p> La investigación sobre signos y significados, sobre interpretaciones y desplazamientos simbólicos que Downey prosiguió a lo largo de dos cintas –<i>Information Withheld</i>
(1983) y <i>Shifters</i>
(1984)–, incluso la videohagiografía <i>J.S.
Bach</i>
(1986) y su complemento interactivo y musicográfico, <i>Bachdisc</i>
(1988), saltan continuamente del tiempo histórico a la actualidad posmoderna, y de lo erudito a lo introspectivo, incitando a estrujar el «yo pensante». Supongo que <i>Hard Times and Culture</i> se planteaba como una miniserie dentro de aquella, pues, a grandes rasgos, la única parte realizada, <i>Vienna fin-de-siècle</i>
(1990), prosigue la misma tónica, solo que con una percepción más sombría ya enunciada desde el propio título. ¡Malos tiempos para la lírica!, como reza el popular dicho. Sin embargo, Downey adelantó
que pretendía referirse a «aquellas ocasiones en la historia en las que una crisis total, no solo económica sino también de conciencia, coincide curiosamente con la excelencia en el arte».[[[11]]] Por tanto, nunca con un punto de vista pesimista o desesperanzado.</p>
<p>  </p>
<p> La inscripción del espacio autobiográfico no es explícita y constante, sino ocasional, en estas últimas obras: por ejemplo, la referencia a la madre fallecida mientras Downey trabajaba en su
<i>bachiana</i>, o el papel de guía que asume en <i>Information Whitheld</i> y <i>Vienna fin-de-siècle</i> para introducirnos en la maraña de contrastes, referencias, citas, alegorías, anacronismos, <i>punctums</i> y parasíntesis de sus ensayos siempre subjetivos (y puesto que retomo los referentes literarios, cabe recordar que el género al que dio nombre Montaigne se incluye en el macroconjunto de los géneros interiores). Downey se refirió asimismo al concepto de «memorias prestadas», para señalar a continuación: «La subjetividad de una persona en realidad es la memoria del subconsciente de muchas otras».[[[12]]]</p>
<p>  </p>
<p> Finalmente, otra circunscripción en la obra de Downey por donde reaparecen los motivos autobiográficos o introspectivos la hallamos en varios de los vídeos en los que revisitó su tierra madre. A partir del cruento golpe de estado de Pinochet y sus dramáticas consecuencias, Downey se sentirá exilado, y no ya un expatriado por elección. En
<i>Chile June 1971</i>
(editado en 1974) o <i>Chicago Boys</i>
(1982-1983) no hay lugar para lo autobiográfico, lo metafórico o lo mitopoético. Ahí se va al grano; se trata en el primer caso de un memorial para Salvador Allende dedicado a las esperanzas decapitadas de la época socialista, y en el segundo, de un contundente reportaje
(en el mejor sentido de la palabra) sobre las consecuencias de la aplicación en Chile de las teorías económicas ingeniadas por Milton Friedman y sus <i>boys</i> y seguidores, donde yo veo casi un proemio a <i>Hard Times and Culture</i>.
(Excepto que aquí la cultura pasa forzosamente a un segundo término, pues más bien se trata de la desaparición de una cultura en sentido más amplio, suplantada por una falsa cultura multinacional, estandarizada e importada con facilidades de pago). Por el contrario, en el díptico formado por <i>The Motherland/La Madre Patria</i>
(1987) y <i>The Return of the Motherland</i>
(1989) –con un precedente más lejano en <i>Publicness</i>
(1974)–, Downey se explayó en lo simbólico, lo hiperbólico, lo surrealista, lo grotesco, lo goyesco, lo instintivo y lo personal-e-intransferible –«arroyo de ideas interiores»–[[[13]]], aunque con el contrapunto sarcástico o brutal de lo factual, mediante retazos apropiados e incorporados con malicia a la recreación mitopersonal, combinando <i>ethos</i> y <i>pathos</i>, del drama patrio. Es un acto más de autognosis donde lo personal se vierte en lo social, colectivo y público.</p>
<p>  </p>
<p>  </p>
<p>  </p> <div id="sdfootnote1">
	<p>
	[1]
	Philippe
	Lejeune: «Cinéma
	et autobiographie: problèmes de vocabulaire», <i>Revue
	Belge du Cinéma</i>,
	núm. 19 («L’Écriture du Je au cinéma») (1987).</p>
<p>
	
	</p>  <div id="sdfootnote2">
	<p>
	[2]
	Michel
	Beaujour: <i>Miroirs
	d’encre</i>.
	París: Seuil, 1979; Philippe
	Lejeune:
	<i>El
	pacto autobiográfico y otros estudios</i>.
	Madrid: Megazul-Endymion, 1994 [Selección, a cargo de Angel G.
	Loureiro, de diversos ensayos tomados de <i>Le
	Pacte autobiographique</i>
	(París: Seuil, 1975), <i>Je
	est un Autre. </i><i>L’autobiographie,
	de la littérature aux médias</i>
	(París: Seuil, 1980), y <i>Moi
	aussi</i>
	(París: Seuil, 1986)].
	
	</p>
<p>
	
	</p>  <div id="sdfootnote3">
	<p>
	[3],
	Raymond Bellour&nbsp;: «Autoportraits»,
	en R. Bellour y Anne-Marie Duguet, <i>Communications</i>,
	núm. 48 («Vidéo»). París: Seuil, 1988.</p>  <div id="sdfootnote4">
	<p>
	[4]
	P.
	Adams Sitney:
	«Autobiography
	in Avant-Garde Film», <i>Millennium
	Film Journal</i>,
	núm. 1 (1977-1978).
	Recogido también en P. Adams Sitney: <i>The
	Avant-Garde Film: A Reader of Theory and Criticism</i>.
	Nueva York: New York University Press (Anthology Film Archives
	Series), 1978; Raymond
	Bellour: «Autoportraits»
	(op. cit.); Yann Beauvais,
	Yann y Jean-Michel Bouhours (ed.):
	<i>Le
	Je filmé</i>.
	París:
	Centre Georges Pompidou / Scratch Projection, 1995; y Roger
	Odin (ed.): <i>Le
	film de famille: usage privé, usage public</i>.
	París: Méridiens Klincksieck, 1995. 
	</p>
<p>
	
	</p>
<p>
	
	</p>
<p>
	</p>  <div id="sdfootnote5">
	<p>
	[5]
	Juan
	Downey:
	«Travelogues of Video Trans Americas», en Ira Schneider y Beryl
	Korot (ed.): <i>Video
	Art. An Anthology</i>.
	Nueva
	York / Londres: Harcourt Brace Jovanovich, 1976.</p>
<p>
	</p>  <div id="sdfootnote6">
	<p>
	[6]
	Diamela
	Eltit:
	«Madre
	Patria»,
	<i>Juan
	Downey: Video porque Te Ve</i>.
	Santiago de Chile: Ediciones Visuala Galería, 1987 [cat. exp.]</p>
<p>
	</p>  <div id="sdfootnote7">
	<p>[7]
	Carlos Aldunate:
	«Viaje hacia la totalidad»,
	<i>Juan Downey:
	instalaciones, dibujos y videos</i>.
	Santiago de Chile: Museo Nacional de Bellas Artes, 1995 [cat. exp.]</p>  <div id="sdfootnote8">
	<p>[8]
	Juan Downey:
	Carta a Gonzalo Díaz (22 agosto de 1987) citada por Juan Pastor
	Mellado: «El misterio de la gran pirámide» en <i>Juan
	Downey: instalaciones, dibujos y videos</i>
	(op. cit. nota 7)</p>  <div id="sdfootnote9">
	<p>[9]
	Juan
	Downey:
	«Travelogues
	of Video Trans Americas» (op. cit. nota 5).</p>  <div id="sdfootnote10">
	<p>[10]
	Ann Sargent Wooster:
	«Juan Downey», en <i>Juan
	Downey: instalaciones, dibujos y videos</i>,
	(op. cit. nota 7).</p>  <div id="sdfootnote11">
	<p>[11]
	Juan Downey:
	Entrevista grabada en vídeo
	por Carlos Flores, 1987.</p>  <div id="sdfootnote12">
	<p>[12]
	Ibíd.</p>  <div id="sdfootnote13">
	<p>[13]
	Ibíd.</p>  </body> </html>