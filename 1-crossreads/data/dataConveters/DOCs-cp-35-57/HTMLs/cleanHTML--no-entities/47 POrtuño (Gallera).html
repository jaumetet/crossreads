  <h1 lang="es-ES" class="western">PASAJES DE LA ESCULTURA LITERAL A LA ESCULTURA SOCIAL</h1> 
<p> <b>Eugeni Bonet</b></p>


<p> <b>1.</b></p>

<p> Un libro de referencia sobre la escultura en el siglo XX, <i>Passages in Modern Sculpture</i>
(1977) de Rosalind Krauss, me proporciona más que un motivo de paráfrasis en combinación con otras expresiones descriptivas de ciertas encrucijadas halladas en tales <i>pasajes</i>.
Los conceptos de escultura, teatro y reportaje (en el mejor sentido de una palabra banalizada por ciertas ligerezas mediáticas) serán los aquí atravesados en relación a unas prácticas no cubiertas pero sí presentidas en el libro de Krauss, y ciertamente enfocadas en otros de sus posteriores escritos; al formular por ejemplo la noción de un <i>campo expandido</i> de la escultura contemporánea.</p>

<p> En este sentido, uno de los pasajes clave del libro de Krauss es aquel en el que entabla una cortés controversia con las tesis puristas y conservadoras —sin embargo bien sagaces— de otro crítico e historiador del arte, Michael Fried, formuladas diez años antes en su ensayo “Art and Objecthood” (1967): una razonada diatriba contra la degradación minimalista del arte hacia la <i>objetualidad</i> y la <i>teatralidad</i>, lo que en otras palabras se traduce en una subordinación a la temporalidad o <i>la duración de la experiencia</i>; mientras que el arte tendría por misión más elevada trascender el tiempo, hasta instalarse en “un presente continuo y perpetuo” de vigencia estética.</p>

<p> En la prosopopeya de Fried, el teatro es más bien un circunloquio filosófico (aunque hasta el teatro mismo, nos dice, ha de combatir con su enemigo interno, trayendo aquí a colación los nombres de Brecht y Artaud) y así, con un matiz peyorativo —pues su tesis, alineada con las de Clement Greenberg, es una de individualidad de las artes, de esencialidad absoluta, de exclusividad incontaminada de cada práctica artística—, escribe: “What lies <i>between</i> the arts is theatre” [Aquello que se halla <i>entre</i> las artes es teatro]; lo que nos recuerda por otro lado que el anhelo de la <i>obra de arte total</i> ha sido perseguido en primer lugar por el arte dramatúrgico.</p>

<p> En cambio, las apostillas y consideraciones de Krauss se dirigen expresamente al pasaje, al atajo que emprende la escultura moderna —de hecho las artes visuales en general— hacia una teatralidad o dramaturgia incluso más rotunda, constatando una tendencia que se impone a partir de los años 60 (no sin precedentes varios, yendo más allá
en todo caso de la escultura “aplicada” al diseño escénico/escenográfico) y que busca deliberadamente la contaminación de la tecnología, de la materia común y el objeto vulgar, del movimiento y de la acción física, de la temporalidad y de lo perecedero, desembocando por una parte en el arte de acción o
<i>performance</i>, por otra en el arte de instalación o <i>environment</i>; por una parte, un arte de representación cercano a lo teatral pero tendente a minimizar el aparato dramatúrgico, por otra un arte de ambientación próximo a lo escenográfico y donde todo acto de representación es latente o diferido.</p>

<p> Una de las obras que Krauss destaca en su apreciación positiva de la teatralidad en la escultura contemporánea es una de las videoinstalaciones de pasillo de Bruce Nauman —la actualmente conocida con el título de
<i>Live/Taped Video Corridor</i>, exhibida por primera vez en 1970—, hasta el punto que es la imagen elegida como portada del libro. Una pieza que resulta reveladora porque prefigura o constata otro pasaje capital hacia la escultura expandida, la escultura aumentada de la instalación y, precisando un poco más, la <i>escultura de media</i>
(<i>media sculpture</i>), por manejar ahora otra expresión, en este caso introducida por Les Levine, un artista también pionero en la exploración de los medios electrónicos.</p>

<p> Sobre el hecho que sea el arte minimal —o <i>literalista</i>, como Fried prefirió llamarlo— el que desencadene esta reflexión, en definitiva encaminada a una nueva percepción (ampliada) de la escultura, Hal Foster ha aportado agudas observaciones en su ensayo
“The Crux of Minimalism” (1986), que entre otras referencias e ideas considera los textos precitados de Krauss y de Fried. Para Foster el papel crucial del minimalismo estriba en la ruptura que comporta respecto a la modernidad tardía —”la autonomía formalista del arte es al mismo tiempo alcanzada y rota”—; reiterando el instinto transgresor vanguardista —”específicamente su trastorno de las categorías formales del arte institucional”—; y que, al mismo tiempo, “abre la vía al arte postmoderno”, lo que determina que el minimal sea un referente genealógicamente imprescindible para comprender el arte de hoy y de las tres o cuatro
últimas décadas.</p>

<p> Refiriéndose al caso específico de la escultura —que algunos (tanto artistas como teóricos) pasan a reformular entonces como arte <i>en tres dimensiones</i>—, Foster aduce: “[C]on el minimalismo la ‘escultura’ ya no se coloca aparte, sobre un pedestal o como arte puro, sino que se resitúa entre los objetos y se redefine en relación al espacio. En esta transformación, el observador, una vez rechazado el espacio seguro y soberano del arte formal, se lanza de nuevo al aquí y ahora; y más que examinar la superficie de una obra para hacer un detallado estudio topográfico de las propiedades del medio, se le induce a explorar las consecuencias perceptuales de una intervención particular en un lugar determinado.”</p>

<p> Yendo más lejos en cuanto a lo que sugieren estas frases y las palabras empleadas —el abandono del pedestal (en el sentido convencional y en el figurado), la intervención en un sitio (un <i>aquí
y ahora</i>)
específico…—, en efecto nos acercamos, mediante un léxico y referentes plenamente vigentes en el arte actual, a otros conceptos amplificados o maleados de tal forma —me interesa resaltar aquí
los de <i>escultura medial</i> y <i>escultura social</i>
(aunque cabe pensar en otros más difusos como <i>instalación</i> o <i>arte público</i>)—
que habría que preguntarse incluso si una palabra como “escultura”
tiene otra utilidad hoy en día que aquella que se acomoda a la tradición y sus estandartes académicos/canónicos (sin pretender implicar que eso sea caduco, ilegítimo o rechazable de plano).</p>

<p> Claro está que el minimalismo no puede tomarse como el metro-patrón único (menos aún universal)
para seguir los sucesivos pasajes y virajes del arte postmoderno. El análisis de Foster privilegia el minimalismo considerando el factor rupturista antes mencionado, mientras que otros movimientos coetáneos como Fluxus o el Pop, el Situacionismo incluso, mantuvieron de entrada una obediencia neo-dadá, es decir de fidelidad a un primer proyecto vanguardista. Al entrar en la recta final de su reconsideración del minimalismo, Foster comenta someramente las proyecciones genealógicas que se desprenden de dicho episodio; recoge sumariamente diversos análisis e interpretaciones y añade aquello que le parece omitido en otras narrativas historiográficas del arte último: “un elemento crucial: la preocupación con la constitución (sexual, lingüística, etc.) del sujeto”.</p>

<p> El pasaje postliteralista —no sólo en cuanto al minimalismo, sino también, por ejemplo, al nihilismo Fluxus, a la frivolidad reprográfica del Pop, a las tautologías del conceptualismo, o al materialismo autorreferencial en las artes audiovisuales— viene significado, en efecto, por traer a un primer término, dar todo su relieve, a la identidad y al entorno social del sujeto: ya no un ente abstracto e inocente en un éter estético-filosófico, sino que ubicado en un marco bien real de conflictos sociales, políticos, étnicos, de identificación sexual, de confrontaciones nacionales, de desequilibrio global, etc. Aquí, tras la alegoría del teatro (en los textos de Fried y Krauss) pienso que cabría introducir, al menos yuxtaponer, la del <i>reportaje</i> en el sentido en que comporta un acto de presencia, testimonio y
<i>mediación</i> activa ante y en la realidad, en la prosa del mundo, mediante su fijación documental, la persecución de la verdad, la encuesta, la confrontación intersubjetiva y su comunicación pública.</p>

<p> De aquí que me refiera finalmente al concepto de <i>escultura social</i> que Joseph Beuys introdujo, no precisamente para referirse a su obra en el sentido convencional del término, sino a propósito de sus métodos dialógicos como enseñante y en su entrega al activismo político: el acicate de la discusión pública para llevar la actividad artística más allá del círculo de los especialistas e intervenir activamente en el proyecto de una sociedad futura basada en la democracia directa. Es decir, la realidad social deviene la materia misma de la escultura en tanto que arte de modelar y dar forma. De manera que, sea en términos de <i>escultura social</i> o de <i>escultura medial</i>
—pues tampoco Les Levine ha usado dicho término en un sentido literal, sino en uno abierto—, o frecuentemente con una doble implicación <i>medial/social</i>, en el arte postliteralista se produce una tendencia a difuminar todo encasillamiento disciplinar y a afrontar en cambio, con los medios que más convengan, los inestéticos problemas que afean el mundo, su historia y los presagios del futuro inmediato.</p>


<p> <b>2.</b></p>

<p> La consideración de los pasajes referidos previamente me ha parecido un prolegómeno oportuno para tratar de la trayectoria de Pedro Ortuño, cuyos pasos he tenido ocasión de seguir (y de refrendar en más de una ocasión)
desde 1989-1990 y sus primeras creaciones de videoescultura/instalación, cuando concluía sus estudios de Bellas Artes, donde eligió sucesivamente las especialidades de Escultura
(en Valencia) e Imagen (en Barcelona). Tanto su formación como la obra que ha desarrollado a lo largo de diez años reflejan en gran medida su propio pasaje desde una disciplina artística tradicional
(o impartida tradicionalmente) a otras exploraciones audiovisuales, multimedia y de escultura <i>medial/social</i> que abjuran virtualmente del pedestal disciplinario.</p>

<p> Sus primeros trabajos me interesaron en su momento por su rigor y nitidez conceptual, jugando con elegancia con la yuxtaposición de objetos e imágenes, con ideas de transformación y permanencia, con percepciones ilusorias y transposiciones engañosas. El más interesante de esta primera serie —no porque fuera más original, sino porque diera a intuir otras expectativas— considero que es
<i>Idea de cuadrado y de círculo (por medios de comunicación) para mass media</i>
(1989), donde, partiendo de unas premisas formalistas y que se dirían referidas a la historia del arte como disciplina académica —el Suprematismo de Malevich, el concepto de <i>realidad absoluta</i>
(por oposición a <i>objetiva</i>), los hallazgos renacentistas de la <i>perspectiva curiosa</i> y el <i>trompe l’œil</i>—, pretende apuntar en cambio a las distorsiones y engaños de los media. La relación entre la imagen y un objeto conexo (en este caso los elementos dibujados en la acción registrada por la cámara de vídeo) establece una contigüidad siempre artificiosa, pero ahora descubriendo expresamente el hiato entre la realidad aparente de las figuras geométricas “correctamente” representadas en el espacio ilusorio de la pantalla y la geometría contrahecha por la artimaña puesta en juego en su grabación videográfica.</p>

<p> En proyectos sucesivos, Pedro Ortuño ha combinado el vídeo, la fotografía y los elementos escultóricos u objetuales para abordar otras relaciones, configuraciones y temáticas más complejas: <i>El quinto sentido del pájaro carpintero</i>
(1990), <i>Espacio y Tiempo Sonoro</i>
(1990), <i>Penta Rhei</i>
(1990), <i>Espacio y Tiempo cinético</i>
(1992) y <i>The Last Vehicle</i>
(1992-94). Relaciones menos literalistas entre los elementos tridimensionales y las implicaciones de la imagen, el sonido, el vector temporal y la impresión de movimiento; configuraciones que tienden a un despliegue más rotundo como instalación, como espacio transitable antes que como proscenio; temáticas que se abren al espacio urbano, al sincretismo en las artes (tomando como referente al músico y arquitecto Iannis Xenakis), a las máquinas de la velocidad y la visión (tomando como referente los escritos de Paul Virilio).</p>

<p> <i>The Last Vehicle</i> ha sido un proyecto determinante en la trayectoria de Ortuño, llevado a cabo durante su primera estancia en Estados Unidos —gracias a una ayuda del Downtown Community Television Center (DCTV) de Nueva York, uno de los crisoles autogestionarios más curtidos en el vídeo documental/alternativo— y desdoblado en un vídeo monocanal y una instalación. El título proviene de un texto de Virilio y la temática de la velocidad, el movimiento cinético/cinemático, prosigue la que se había apuntado en otras obras inmediatamente anteriores, ahora en relación a un interés por la arqueología del cine y del cine expandido y a una reflexión donde el ferrocarril, el cinematógrafo y las imágenes electrónicas encarnan —según lo formula Virilio— el progreso en el transporte, la mutación vehicular que conduce a la inercia polar del viaje sedentario o
“voyage de l’immeuble”. Esto último sería en todo caso un subtexto de la obra, pues Ortuño finalmente prefirió recoger un comentario de Wim Wenders, tal como recogió imágenes de archivo y emblemáticas de la historia del cine.</p>

<p> En cuanto a la instalación —sólo exhibida en una versión un tanto esquelética o a escala reducida por comparación con la envergadura inicial del proyecto—, ésta pretende recrear el dispositivo escenográfico de algunas experiencias cinematográficas primitivas como los <i>Hale’s Tours</i>
(1904) o anteriores incluso, donde la sala de proyección era el simulacro de un vagón de tren o de otros vehículos reales o imaginarios (como el <i>Ballon-Cinéorama</i> de Raoul Grimoin-Sanson en 1900, o la <i>Time Machine</i> que H.G. Wells proyectó en 1895 junto al pionero del cine británico Robert W. Paul), a veces con complementos estimuladores de la sensación realista tales como efectos de movimiento, ambientación sonora y corrientes de aire.</p>

<p> Decía que este proyecto ha sido determinante en la obra de Ortuño, o quizá cabría decirlo de las circunstancias que concurren en su periodo de elaboración: la experiencia de trabajo en DCTV y el conocimiento de otros pivotes y colectivos en el bullicio artístico y activista de Nueva York; el contacto y la ocasional colaboración con artistas como Muntadas, Juan Downey y Ken Feingold. Para retomar el tropo de los <i>pasajes</i>, Ortuño culmina entonces el tránsito de la videoescultura literalista de sus inicios a la escultura expandida/medial, a la vez que emprende el paso sucesivo a un modo de escultura medial/social
(que, como se desprende de lo expuesto en la primera parte de este texto, no siempre es escultura en una acepción literal). En efecto, después de <i>The Last Vehicle</i>, o ya desde otros proyectos iniciados simultáneamente, su trabajo se fundamenta en el reportaje en el (buen) sentido referido anteriormente, tanto da si el medio o material es finalmente la fotografía o la intervención en un espacio público, un vídeo monocanal o una instalación vídeo/multimedia, o una adaptación dinámica del proyecto a posibilidades y secuelas diversas. Un somero repaso de otros trabajos me servirá de apoyo.</p>

<p> <i>Getting There Is All That Matters</i>
(1993) es un proyecto de arte público, tangencialmente relacionado con <i>The Last Vehicle</i>, para una estación del metro de Nueva York, utilizando el soporte y formato de una valla de publicidad, y auspiciado por la entidad pública Metropolitan Transit Authority como parte de su programa
<i>Arts for Transit</i>.
Tomando como referencia el primer estatuto por el que se regía antiguamente el transporte público en la ciudad de Nueva York (y jugando por tanto con la apariencia de la publicidad institucional), la composición de Ortuño combina la cita literal, el reportaje fotográfico y el destacado énfasis de tres palabras contenidas y enlazadas en la prosa administrativa de dicho texto; palabras que el autor ha interpretado como claves del <i>American Way of Life</i> y que giran de sentido en relación con el título de la pieza, con
“aquello que más importa alcanzar”: seguridades y beneficios.</p>

<p> Los vídeos <i>La cuna del Daiquiri</i>
(1994/1995) y <i>Las lavanderas de San Cristóbal</i>
(1994/1997) —las fechas entre paréntesis indican respectivamente el año de grabación y el de su edición/finalización— son sendos reportajes que documentan la asfixia de Cuba y un flagrante intento de fraude electoral en el estado mexicano de Chiapas. Ambas cintas puntúan su visión testimonial con el contrapunto de unas reverberaciones mediáticas: en un caso el oficialismo o la indiferencia de los eslóganes en las vallas de publicidad y propaganda (del intimidante “Junto a tí siempre” al globalizado
“The United Colors of Benetton”); en el otro las informaciones de la TV y la radio y las cuñas de propaganda institucional, más unos anacrónicos insertos cinematográficos de Cantinflas “el peladito”. Contrapuntos que resaltan el literal acercamiento de la cámara y su micrófono —una humilde cámara de 8 mm— a los descontentos, a los discrepantes, a los balseros, a la sal de la tierra.</p>

<p> El vídeo <i>Abanico rojo</i>
(1996) es un trabajo impecable de historia privada y memoria colectiva a propósito de la guerra civil española y los “niños de la guerra” acogidos en la URSS, combinando imágenes de archivo y entrevistas con diversos testimonios de aquel éxodo y de una generación sucesiva ya nacida allí. Es un trabajo de “acercamiento entre generaciones”, tal como lo intuye y describe una de las personas entrevistadas, que merecería hallar un hueco televisivo si no fuera por la cerrilidad de las televisiones españolas y autonómicas, mientras que los circuitos del vídeo independiente son aquí y ahora tan enclenques que uno llega a dudar si verdaderamente se puede decir que existen. Sin embargo, Pedro Ortuño no es alguien que se deje abatir por los obstáculos y <i>Abanico rojo</i> queda como un proyecto abierto, a partir del cual ha esbozado otros con la expectativa de incorporar nuevos recursos interactivos y telemáticos.</p>

<p> <i>You Are Invited To Visit The White House</i>
(1996/1997), proyecto nuevamente desdoblado en un vídeo monocanal y una instalación, documenta una manifestación de lucha contra el SIDA convocada por ACT UP en Washington DC, a lo largo de la cual se cumplieron varios proyectos activistas, culminando con el lanzamiento de cenizas en los jardines de la Casa Blanca —un gesto colectivo que había sido imaginado por una de las víctimas de la enfermedad, el artista David Wojnarowicz— y una intervención policial disuasiva. Estas imágenes son puntuadas por un plano intercalado de uno de los puntos de información turística situados en los alrededores de la residencia presidencial, y por algunas reiteraciones consecutivas del mensaje grabado que invita a su visita al pulsar un botón. En su versión como instalación, por otra parte, el vídeo se complementa con una réplica de dicho punto de información y la imagen ampliada de un policía a caballo, y es al pulsar el botón correspondiente que se escucha el sonido del vídeo; es decir, las voces airadas de quienes no son bien recibidos en la Casa Blanca.</p>

<p> En el proyecto que Ortuño ha desarrollado para La Gallera se advierte de entrada una premeditada configuración específica para esta sala pública de exposiciones, en la que, contando con los medios más habituales en su trabajo
—vídeos, fotografías, objetos, sonido—, pretende escenificar un combate figurado a dos niveles, con sus gallos de pelea y sus
(valientes que no cobardes) gallinas, y así evocar alegóricamente el uso que tuvo originalmente el edificio y otras connotaciones que su nombre inspira. Por una parte una pelea ruidosa y violentamente espectacular, espoleada por los medios de masas y su propia riña, competitiva y limpia: de guante o de cuello blanco. Y a otro nivel, la lucha pacífica, que no cándida, de las identidades segregadas y reivindicativas; concretamente, de los colectivos feministas y de gays y lesbianas. Una vez más, un principio de contrapunto que quiere ser tan inmediato (y simple en su dualidad) como esclarecedor
(y complejo en sus entrelíneas): la desarticulación de los discursos dominantes en la arena central, la plataforma del espectáculo; el reportaje entregado a la voz y presencia de los sectores excluidos, en áreas periféricas y que, sin embargo, son las que reclaman detención, tiempo, atención, como poniendo en cuestión la misma idea de centro.</p>

<p> La noción de escultura social, finalmente, es apenas una de las claves posibles para interpretar ciertas sendas del arte actual en su acento crítico y comprometido.
Ese compromiso que adquiere Pedro Ortuño al abordar un tema como el ahora elegido, en el que se diría que son más contundentes y eficaces las palabras y los gestos (y gestas) resueltamente activistas. La oportunidad del reportaje, entonces, es la de recoger la oralidad del debate; aunque aquí sea un elemento más entre otros.</p>

<p> Cuando la obra de Ortuño pertenecía más claramente al rango de la “escultura”, parece como si causara recelo su incorporación de un medio como el vídeo, tan mal aposentado y poco reconocido en nuestros ámbitos artísticos. Y cuando en sus trabajos de vídeo ha tendido al “reportaje”, me constan actitudes pacatas que han puesto en tela de juicio que eso fuera (video)arte. En este sentido, el concepto de escultura social permite un razonamiento más abierto para enfocar adecuadamente su obra y trayectoria, incluso sin perder de vista su ya lejano punto de fuga.</p>

<p> septiembre 1998</p>




<p> <b>REFERENCIAS</b></p>

<p> Eugeni Bonet: “Sin pedestal”, en <i>El quinto sentido del pájaro carpintero</i>, catálogo de la exposición de Pedro Ortuño (Murcia: Región de Murcia - Consejería de Cultura, Educación y Turismo, 1990).</p>

<p> Eugeni Bonet: “La instalación como hipermedio (una aproximación)”, en <i>Media Culture</i>, ed. Claudia Giannetti (Barcelona: ACC L’Angelot, 1995).</p>

<p> Hal Foster: “The Crux of Minimalism”, en <i>Individualism: A Selected History of Contemporary Art</i>
(Abbeville, NY, 1986). Traducción castellana: “Lo esencial del minimalismo”, en <i>Minimal Art</i>
(Donostia-San Sebastián: Diputación Foral de Gipuzkoa/Koldo Mitxelena Kulturenea, 1996).</p>

<p> Michael Fried: “Art and Objecthood”, <i>Artforum</i>, junio 1967; posteriormente recogido en <i>Minimal Art: A Critical Anthology</i>, ed. Gregory Battcock (Nueva York: E.P. Dutton, 1968). Traducción castellana: “Arte y Objetualidad”, en <i>Minimal Art</i>
(op. cit.).</p>

<p> Rosalind E. Krauss: <i>Passages in Modern Sculpture</i>
(Nueva York: The Viking Press, 1977).</p>

<p> Rosalind E. Krauss:
“Sculpture in the Expanded Field”, en <i>October</i>, nº 8 (primavera 1979).</p>

<p> Paul Virilio: “Le dernier véhicule”, en <i>L’inertie polaire</i>
(París: Christian Bourgois Éditeur, 1990).</p> 