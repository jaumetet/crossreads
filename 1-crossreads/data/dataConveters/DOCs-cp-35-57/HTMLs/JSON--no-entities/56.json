[
	{
		"pid" : 0,
		"p" : " <p> <b>Entre tocaios</b></p><p> </p><p> </p><p>L’Eugeni que això escriu, i l’Eugènia sobre la que em demanen d’escriure, sembla que erem predestinats a creuar els nostres camins. No només per coses del fat o l’atzar, sinó per unes que diguéssim afinitats electives. El cinema (en tot cas unes determinades maneres de fer, concebre i festejar el cinema) en fou una.</p><p> </p>"
	},
	{
		"pid" : 1,
		"p" : "<p> Des del 1974 jo escrivia a <i>Star</i>, una revista contracultural pròpia de l’època. Escrivia sobre cinema més o menys marginal, <i>underground</i> i rar, però que hom podia veure a la Filmo, l’Institut Alemany o altres circuits alternatius. Un dia, el director de la publicació–sabedor de les meves antenes multidisciplinars i cosmopolites–em demanà si coneixia una tal Eugènia Balcells. Gens ni mica, peròen la miscel.lània de notes breus que oferia la revista se’n donava aviat notícia. L’Eugènia encara recorda amb esglai la prosa <i>enrotllada</i> que la introduïa com una tía que acaba de llegar de Nueva York, suposo que amb motiu de la seva instal.lació <i>Supermercart</i> a la Sala Vinçon, amb la qual es va donar a conèixer com artista.</p><p> </p>"
	},
	{
		"pid" : 2,
		"p" : "<p>Com que jo freqüentava tant les foscúries cinematogràfiques com els circuits de l’art alternatiu, no vam tardar a coincidir en qualque vernissatge. En general, als artistes conceptuals, el cinema no els interessava gaire, i si s’encuriosien pel vídeo era per la novetat–o perquè del videoart sí que se’n parlava a les publicacions i xerrameques de l’art–, tot i que, localment, era vist com un mitjà inabastable. L’Eugènia, en canvi, tenia un fort interès tant pel vídeo com pel cinema, i els seus primers supervuits ja eren molt polits i elaborats; no en và, no eren realment primerencs, sinóque es beneficiaven llunyanament d’un aprenentatge adolescent en la pràctica privada, casolana, del cinema a petita escala.</p><p> </p>"
	},
	{
		"pid" : 3,
		"p" : "<p>Hi havia aleshores un nou esclat dels petits formats, el cinema marginal en uns vessants entre solipsistes i alternatius; així com un enrenou incipient pel que fa al vídeo. S’hi barrejaven ascendències, inclinacions i aspiracions ben diferents. Inevitablement es formaren clans i grupuscles, amistançaments cordials i enemistats rivals (i un xic tribals). Per aquesta banda, vaig arreplegar una colla d’amistats noves, i aviat començàrem a compartir unes rutes i afeccions comuns que tenien un lloc de cita ben habitual (a voltes diari o gairebé, em temo que avui no seria així) a la Filmo, tot i que també feiem altres sortides més gamberres.</p><p> </p>"
	},
	{
		"pid" : 4,
		"p" : "<p>L’Eugènia s’hi va afegir en algun punt, tot i que de vegades feia un posat de condescendència davant les nostres rucades; després de tot, hi havia una diferència d’edat de deu anys i més entre ella i la resta de nosaltres. En qualsevol cas, les dèries audiovisuals eren allò que ens ajuntava. Entre mig, jo vaig fer la primera escapada a Nova York i encontorns, i després de tres mesos d’enlluernament, en vaig tornar amb l’empenta d’unes idees força arriscades i utòpiques per tal de consolidar una hipotètica plataforma del cinema i el vídeo experimental o diferent, com el que preteniem fer nosaltres.</p><p> </p>"
	},
	{
		"pid" : 5,
		"p" : "<p> No cal dir que no vàrem consolidar gairebé res, però en l’intent de fer castells algunes coses sí vam fer i, per episòdiques que fossin, tanmateix van ser prou intenses i crucials per alguns de nosaltres. Jo ja m’havia empescat unes sigles, FVI(per Film Vìdeo Informació), i comptava amb una base arxivística i de contactes. Ens posarem a organitzar unes programacions esporàdiques (que, més endavant, confiavem que esdevinguessin estables), tot estirant la tradició de les sessions golfes i furtives de l’escena independent/<i>underground</i> casolana. També endegarem esforçadament una revista (després he preferit dir-ne un <i>fanzine</i>), <i>Visual</i>, que no passà d’un segon número (datat a maig de 1978). I millor no rememoro alguna que altra decepció, en tractar d’obtenir un cert suport i picar-nos els dits amb un mecenes fatxenda.</p><p> </p>"
	},
	{
		"pid" : 6,
		"p" : "<p> Al cos de l’Efa-Ve-I, a més de l’Eugènia i jo, hi participaren en Manuel Huerga, en Juan Bufill i l’Ignasi Julià (que va ser el primer a tirar la tovallola). De tant en tant hi treien el cap o donaven un cop de mà altres elements, entre els quals Luis Serra Platinos (el nostre fotògraf oficial), JoséLuis Guerín i Carles Hac Mor. En realitat, l’<i>esprit de corps</i> no va durar gaire, després que, a les acaballes del 1979, l’Eugènia escampés la boira per a emprendre una segona aventura americana; quan a uns altres se’ls hi va acostar l’hora de complir els deures marcials; quan, en definitiva, es va produir un campi-qui-pugui semblant al que ha consumit la flama efímera de tantes bandes de rock (i aquesta no és una figura gens capriciosa, car el rock i altres músiques formaven la banda sonora que igualment ens aplegava).</p><p> </p>"
	},
	{
		"pid" : 7,
		"p" : "<p>Després d’aquestes pinzellades sobre els embolics que s’afegien a allòque feia cadascú, ara vull referir-me a la significació de la producció fílmica de l’Eugènia; tot tractant de no ser gaire redundant pel que fa a d’altres ocasions que he tingut d’ocupar-me de la seva obra, la qual abasta diferents suports, formats i modalitats (la més característica: aquella tan difícil d’acotar de la instal.lació, o l’obra que es fa i es desfà per a un espai físic i de temps).</p><p> </p>"
	},
	{
		"pid" : 8,
		"p" : "<p> La filmografia <i>strictu sensu</i> de l’Eugènia no abasta gaire més d’un lustre, de la primera versió d’<i>Àlbum</i>(1975) a <i>For/Against</i>(1983, amb Peter Van Riper), contribució a un film col.lectiu del mateix títol; i no gaire més que mitja dotzena de títols, de 3 a 45 minuts de llargada, en super-8 o en 16mm i amb més afany que recursos. Ara sembla que vol embrancar-se en l’aspiracióllargament demorada d’un llargmetratge, amb tota una altra escala de producció que la de quan filavem prim amb la butxaca i els mitjans a l’abast. Però, al marge d’aquest possible retorn a la praxi del cinema, jo crec que aquella primera i llunyana etapa va ser determinant quant al rumb posterior de l’art de l’Eugènia.</p><p> </p>"
	},
	{
		"pid" : 9,
		"p" : "<p> Això penso que es palesa sobretot als films que es distancien més de la mena d’obres amb les quals es donà a conèixer en l’àmbit de les arts visuals; obres d’una flaire entre sociològica i antagonista (amb cert biaix feminista), que prenien com a matèria primera les icones i els estereotips dels mitjans de masses i la societat de l’espectacle. En aquest sentit, films com <i>Presenta</i>(1977), l’aportació al col.lectiu <i>En la ciudad</i> (1977) i <i>Boy meets Girl</i> (1978), tenen una estreta connexió amb les exposicions o instal.lacions <i>Supermercart</i>(1976), <i>Re-prise</i>(1976-77, inèdita però fins el 1992), <i>Ofertes</i>(1977) i <i>Fin</i>(1978)</p><p> </p>"
	},
	{
		"pid" : 10,
		"p" : "<p> En canvi, Eugènia va realitzar <i>Àlbum</i> gairebé com una mena d’exercici privat, un exercici de memòria que diguéssim, mentre que <i>Fuga</i>(1979) pertany plenament a una etapa més experimental. Ambdós films tenen, segons la meva percepció, alguna cosa de cinema amateur, casolà, personal; investits tanmateix d’un rigor formal que els hi dóna una força especial. L’un és un viatge (que diria l’Eugènia) a través de l’àlbum de postals d’una avantpassada de l’autora; l’altre, una comèdia d’embolics sense argument i on els embolics són més aviat els de l’espai i el temps, les presències i absències, els enquadraments i les sobreimpressions que se succeeixen, en una fuga silent i executada a partir d’una mena de partitura gràfica (i oberta a l’atzar i la improvisació).</p><p> </p>"
	},
	{
		"pid" : 11,
		"p" : "<p> D’<i>Àlbum</i>, l’Eugènia en va fer el 1978 un <i>remake</i> en 16mm, una versió més polida i que redueix a més de la meitat la durada d’aquella que havia rodat en super-8 primerament. Allò que havia concebut com una cinta privada, aliena al vessant públic, el concerniment social del seu art, va ser incorporat aleshores a una nova direcció que es perfilava en el seu quefer, a la vegada que s’intensificava el seu interès pel cinema i les expansibilitats audiovisuals. Més recentment, el biaix autobiogràfic d’aquell film –a través, en tot cas, de l’evocació d’unes arrels familiars– es pot reconèixer a <i>En el cor de les coses</i> (1998), un conjunt de cinc instal.lacions que recreen les estances d’una llar al.legòrica. I, pel que jo sé, una intriga familiar és tambépresent al nou projecte fílmic que trama actualment l’Eugènia.</p><p> </p>"
	},
	{
		"pid" : 12,
		"p" : "<p> Tanmateix, és a <i>Fuga</i> on es prefigura el gir que prendria seguidament la seva obra. I aixòen diversos aspectes. La mirada circular –panoràmica o polièdrica–que hom pot retrobar al vídeo <i>Indian Circle</i> (1981) i en tantes instal.lacions i videoinstal.lacions, de <i>From the Center</i> (1982-83) a <i>Un espai propi</i> (2000), on tal vegada saldàallò que se’n diu un deute amb la lectura efervorida de Virginia Woolf. Els <i>sound works</i> dels anys 1981-1983 –i la col.laboració amb compositors i intèrprets d’una ascendència entre Cage, Fluxus i el minimalisme–; car, si <i>Fuga</i>és un film absolutament silent, nogensmenys enceta –juntament amb<i>133</i> (hi arribaré de seguida)– una recerca sobre les correspondències entre el so i la imatge. Les densitats, les espessors en fi, de l’art de les llums i les ombres, les projeccions i l’encís òptic d’unes imatges a la vegada hermètiques i universals, vigoroses iinframinses;com per exemple les de <i>Traspassar límits</i> (1996), una de les instal.lacions de l’Eugènia que jo en diria de llanterna màgica.</p><p> </p>"
	},
	{
		"pid" : 13,
		"p" : "<p> Hi ha també alguna videoinstal.lació que neix d’un projecte primerament fílmic. És el cas de <i>TV Weave / Teixit TV</i> (1985). Originalment l’Eugènia havia pensat en un film on la imatge es reduiria a una franja molt estreta, gairebé prima com una línia; com qui mira per una escletxa. A la fi, aquesta idea anà a parar a una instal.laciótan senzilla com reeixida: un mur de televisors cegats, a excepció d’unes quantes escletxes a cada pantalla, entre les quals espurnejen els colors i els indicis icònics dels senyals electrònics.</p><p> </p>"
	},
	{
		"pid" : 14,
		"p" : "<p>Per acabar afegiré alguna cosa sobre el film que vam fer junts entre 1978-1979. Un dia, a ca l’Eugènia, vam posar-nos a escoltar unàlbum discogràfic que ella havia adquirit per sonoritzar alguna pel.li de les que feiem. La col.lecció (tres discs LP) duia el títol de 133 Auténticos Efectos Sonoros, i aquests estaven organitzats per motius temàtics. Aviat se’ns va acudir que aquell compendi de sons era un film en potència, un argument d’imatges acústiques. Engrescant-nos mutuament, vam decidir que li posariem otornariem les imatges que hi mancaven, i aleshores jo vaig proposar que, en tot cas, les imatges també haurien de ser unes de preexistents, sense rodar-ne ni una nosaltres.</p><p> </p>"
	},
	{
		"pid" : 15,
		"p" : "<p> Així va néixer <i>133</i>: al llarg de setmanes i mesos anarem cercant, recollint i garbellant bobines i retalls de pel.lícula que compravem, gairebé a pes, a un quincallaire. Era com un joc (que ens preniem ben seriosament)d’ajuntar sons i imatges, i no necessàriament amb literalitat o versemblança. Quan vam donar per acabat el muntatge,el vam dur a Fotofilm on van refusar de fer-se’n càrrec: adduïren que els hi espatllaria les màquines, tal era l’estat i la brutícia del nostre apedaçament de bocins i deixalles. Finalment, quan l’Eugènia va marxar a Nova York, es va endur el cadàver exquisit i allí sívan saber fer-ne una neteja i tirar-ne còpies.</p><p> </p>"
	},
	{
		"pid" : 16,
		"p" : "<p>Després, els estris mecànics del cinema –càmeres i projectors, movioles i empalmadores–, pràcticament els vam arraconar i desar. El vídeo semblava un mitjà més a l’abast i amb més empenta; a més, la pel.lícula verge s’havia posat pels núvols i el cinema deien que agonitzava. (Això, a la geografia espanyola, es va reflectir ben aviat en una volatilització gairebé total de les mogudes alternatives que s’havien mantingut fins aleshores.)</p><p> </p>"
	},
	{
		"pid" : 17,
		"p" : "<p>Peròvet aquí que ara tenim tot aquest rebombori del cinema digital i uns quants elements que demostren que les tecnologies electròniques afavoreixen avui, més que mai abans, la resistència i la pluralitat davant la uniformització. Els puristes recalcitrants ja poden arrufar el nas davant la digitalitis dels nostres dies, com fan aquells escriptors que reneguen de l’ordinador i es vanten de seguir utilitzant la màquina d’escriure o la ploma; però avui ja no hi ha estris ni maneres canòniques de fer cinema (bé que hi ha tot un aparat gremial i docent que sí que fa que ho sembli). En tot cas, que hi hagi més opcions ja ens està bé als proscrits i dissidents del cinema consuetudinari, que així renovem la picor de la pantalla primordial.</p><p> </p> "
	}
]
