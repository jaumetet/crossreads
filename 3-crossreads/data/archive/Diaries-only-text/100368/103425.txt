we would all get off the position safely. After we heard what was actually going to happen we were more at ease but candidly speaking I do not expect to get away.
Dec 15th Wed.
 Still on the "qui vive" Bringing up large quantities of stores from the A.S. Depot and storing them inside the station grounds. Foodstuff of all descriptions are stored here. Small trestle bridge being built from the beach to the sunken ship "Milo". Issued with forty eight hours iron rations and ordered to keep our water bottles filled. We have also been issued with first field dressings and large quantities are in the tents in case of emergency.
Dec 16th Thurs.
 A nose cap from a shell landed in B.R.S dugout. He had a narrow escape from injury. The shells seem to be getting more near every time, to our place of abode. The hits all but getting on to the piers and the barges. As I write this the shells are buzzing over our heads and bursting too near to be comfortable. Two members of our unit woke up this morning and found a shell case lying between them. Apparently the shell after having got rid of its shrapnel had dropped through the roof of their dugout whilst they were asleep
 

 and it did not disturb there slumbers. Put on guard over the water tanks in Reserve Gully. They have been filled with water and are only to be used by the hospital. D.I. relieves me. At twelve oclock midnight just as I had gone on guard duty a great flare got up just from about the last tent in the C.C.S. Then a short time afterwards the Army Ordnance Stores and the Army Service Stores were set afire. The whole of north beach was lighted up and bellows of smoke mounted high up into the air and became so dense that the full moon and stars were hidden from one's sight. The Turks throughout the night kept sending shells over at irregular intervals and some of them landed just by the side of the "Milo" and the new trestle bridge which had so lately been put up. I was standing beside the water tanks troops in small bodies of two hundreds or so, came marching quietly down the gully. Their boots were padded with sandbags etc. in order to deaden the noise of marching and so well was this accomplished that they seemed like treading on air and they vanished before I knew. A chap came running down the gully in a great hurry. Soon afterwards he came back carrying
 

 a stretcher and on making inquiries I learnt that a shell had dropped into an officer's dug-out and had wounded him. A couple of mules passed by very slowly and they made me start, they, being white gave me the impression of seeing ghosts. Soon afterwards an Aussie thinking that it was better to ride than to walk had mounted this mule and passed by me like an apparition, the other mule walked quietly behind. In the early morning nothing but the sound of a few birds and the crack, crack, crack of machine guns and rifles firing broke the dead silence. It was exceedingly eerie waiting on day breaking. Nearly all the Indians have left here. They did valuable work transporting stores up to the trenches with their mules and carts. This accounts for the few mules that are staying about without anyone in charge. The sun, as it rose this morning took on a peculiar reddish appearance, owing I have no doubt to the dense smoke which is coming from the burning stores. The sea was beautiful and calm all night.
Dec 18th Sat.
 A divine service eas held in the shrapnel shelter. The Rev Captain Dexter gave a splendid
 

 and an appropriate address based on Psalm 46. rather a peculiar incident was, in illustrating his address, he told us three stories which were taken from the book entitled "The Psalms in Human Life" which I had read during my stay on Anzac and a book which I have in my possession at this time. As we came out of the shelter after the service, shrapnel burst over our heads and spread into the sea near the "Milo". Had a parade of all men and received orders how to proceed during the evacuation. The unit has been divided into four parties. No 4 party is to leave the shelter first, No 3 next No 2 to follow and No 1 last. D. and myself are in No 2 party. Bingham who is being evacuated sick is taking a letter which he is going to post to George, so tha if I am missing the people at home will know that I am a prisoner of war in Turkey. We have been addressed by Major Campbell our new O.C. and the tone of his address gave us confidence that we would get off all right.
Dec 19th Sun.
 On water tank guard, I saw no troops during the night but I am told that ten thousand had left Anzac Cove pier during the nightime. Shrapnel was sent over during the night-time. The "Stores" fire still burns fiercely.
 

 In spite of the fact that this fire has been burning two nights and a day, there is a tremendous amount still to be burned up yet. A large number of bottles of beer and champagne etc. were destroyed last night. These belonged to our officers. Such luxuries could never be allowed to common rank and file Oh! No, No. Ordered to take our packs down to the shelter and to parade there at ten thirty to-night. Put up a notice on our dug-out "Apartments to let" Apply. When we come back again. A large number of troops with muffled feet passed on their way to the beach, to embark. A beautiful moonlight night and the sea nice and calm. As we waited quietly on the weary hours passing away we were provided with music from the gramophone which had only been in use in the officers mess previously. The one tune that was the favourite and was played over a few times was "the Rosary". A copper had been placed near the shelter and our cooks made cocoa for us. The floor of the shelter was covered with hospital mattresses and on these we all lay huddled to-gether till the time should come for us to depart. Strange to say not a shell has been sent over to-night. The slightest sound made us prick up our ears expecting every minute to be heavily bombarded.
 

 I have only a faint recollection of hearing No 4 and 3 depart. About 3.30 in the morning we, who were left, automatically put our packs on our back and left the shelter and made for the beach. On our way we met the machine gunners carrying their machine guns along. They had lived the ridge above us and were our last line of defence. We crossed over the small trestle bridge on to the "Milo" and from the "Milo" we embarked on the barge. We waited on the barge for some time and during the time we were waiting a terrific crack, crack of rifles and the heavy sound of an explosion either of a mine or a big gun being blown up, was heard. At five minutes to four the barge left the "Milo" and we were speeding our way to the Hospital ship "Dongala", not a man from our unit missing. How this evacuation was worked is marvellous, for had the Turks known of our intention there must have been a very different story to tell. It seems strange that not one shell should drop on the beach during the whole of the night we were leaving. There had been a terrific bombardment of Gabe Tepe during yesterday forenoon and rumour had it that Beachy Bill had been silenced,
 

 be that the reason or not I do not know, but this is certain, no shell dropped on the beach during the final evacuation.
Dec 20th Mon.
 Much to the surprise of the staff on board the "Dongola" instead of seeing wounded men pouring on to the ship. A thing they were fully prepared for, they saw healthy but tired men walking up the steps carrying their packs on their backs. We were all dog-weary, I hadn't had a decent wash all the time I had been on the Peninsular, and had about a fortnights growth of beard on my face, wearing a Tommy cap on my head, an Aussie tunic and Tommy trousers I for one must have looked a picture. When we got aboard the "Dongola" we all rushed the bathrooms for a bath, Hot cocoa was provided and clean sheets and a comfortable bed made us feel as if we had landed into Paradise. Looking from the ship away on to the position we had just left all one could see was the bright light of the burning stores, and about fifty yards from the hospital ship a British warship slowly passed us by. I was too dead tired to take any more interest in what was going on and getting down
 

 below to my bed I tumbled into it and slept soundly.We arrived in the harbour at Lemnos about eleven oclock. We had only been a few hours on the "Dongola" but we were splendidly looked after. The nurses on board were tumbling over themselves taking our photos.A rather amusing thing happened while the nurses were taking our photo some of the chaps who had cameras snapped the nurses at the same time. There were a large number of ships of all descriptions in the harbour mostly relating to the men of wars and hospital ships. Rumour has it that the Turks poured shrapnel all over the beach after we left this morning, luckily for us that we had flown. After we had dinner we embarked on the ships boats and were towed by a pinnace to the "Dunbar Castle". Col G. who was our O.C. up to two days ago but had left us before the evacuation, was on this ship. Some of our officers went aboard her for a short time. We then went to Mudros, one of the large towns on the Island. On our arrival there we had to march to Sarpi Camp. After a good deal of hanging about we found very uncomfortable quarters awaiting us.
Dec 21st Tues.
 Heavy rain fell after breakfast. The roads were ankle deep
 

 in mud. We left Sarpi Camp for No 3 A.G.H. In order to avoid the muddy roads we sailed over in small boat across a bay in which there were a great many brightly coloured boats of the sailing type presumably fishing boats belonging to the islanders.
Dec 22nd Wed.
 R I went for a walk around the villages which were widely scattered and built any old how. They were out of bounds to troops from twelve till five oclock. Later we saw a number of windmills situated on a raised part of the island. These windmills were built of stones and do not seem to change their position like the windmills that I have seen. We went over to the church which stood on the outside of the village. We could not get inside it but peering in through the window we saw figures painted in gaudy colours like those in R.C. Churches. The grave-yard surrounded the church and in the corner of the churchyard was a small house into which the bones of those, after they have been a certain time in the earth are thrown. This little house was half full of human bones of all kinds. Rather a strange custom and gruesome manner of doing things. A military graveyard begun 
 

 since the troops arrived here is growing rapidly. It is being well kept. 
 We are having a very easy time at present and wandering over these fields one felt as if they were free men again, we are at present free from all military restraint. The old farmer ploughing with the old fashioned wooden plough drawn by a pair of bullocks, The women washing their clothes at the well outside of the village, Little children carrying buckets of water from the well and the birds singing in the fields makes a very pretty scene. We parted the village school door. It, being open and hearing the sound of many children's voices in an unknown tongue we looked inside. There was the old schoolmaster, a venerable old man seated on a high chair and a desk in front of him giving lessons of some nature or other to a big number of Greek children. The children were all attention when we got to the door but one or two of them noticed us and and the signal was going round the room so that we thought that it would be better to leave them. At present everything is bare and barren on the island but as far as I can learn things are very much different in the summer time. There are a large number of 
 

 military hospitals on Lemnos island. They are all very well laid out and are principly tents with a few weather board buildings. The land is covered with stones and these have been utilized to decorate the outside of the tents.
Dec 23rd Thursday
 A large mail received by our unit. Received two parcels and newspapers and letters from home. They were all very acceptable at this time. No bread for tea, so that the contents of the parcels came in very handy. The large ship "Aquatania" which is now a hospital ship has arrived in the harbour. It is the largest ship in the world at present. The sunsets are beautiful here.
Frid 24th
 Went out to see if we could find Johnny L. but was not successful. Got a billy can full of good things sent by the people of Australia. There is one given between every two men. The billy that D I got contained one tin of Tobacco and packet of cigarette papers, one pair of socks, one toothbrush, one tube toothpaste, safety pins, buttons, knitted face washer, one tin vaseline, packet Boracic acid, cake chocolate, writing pad, mirror, bootlaces, chewing gum, leather Kangaroo and Patriotic button. All the empty
 

 places in the tin were lightly packed with good wishes. A Christmas feeling prevails in the air at present. A few chaps went round the hospitals in a motor lorry singing Xmas Carols.
Dec 25th Sat. Xmas.
 Paraded at eleven o-clock and attended church. Had Plum Pudding for dinner. This P.P. was also provided by the people of Aust-- Took a walk through one or two of the Greek villages. The houses are built of freestone but in an irregular fashion. No regular streets abgout the place. Nearly every house had turned itself into a place for selling luxuries, Chocolate, oranges, nuts, tin fruit, sardines, biscuits, figs etc were for sale but at exorbiant prices. Still so crowded were these small villages withtroops and there was such a demand for these luxuries that even at exorbiant prices these luxuries were being sold in large quantities. 
 Scattered all around these village streets were refuse of all descriptions. Orange skin, papers, tins galore, gave the place a very unwholsome appearance. Between selling luxuries, the women washing soldiers clothes, and the men rowing boats about from one place to another the islanders must be making a mint of money. The troops have made a splendid road over the island.
 

 Outside of the village the inevitable game of pitch and toss is being played by the troops, especially the Aussies, for large sums of money. Nothing less than notes being taken. One Greek youth was playing at an anchor board. He was only about fourteen years of age, and he had won about thirty bets without a loss. 
 Others spent there time playing football and the usual excitement reminded one of Saturday afternoon games at home. During the forenoon D.R.I. went through the Egyptian camp. They are a labour corps helping to make roadways on the island. They look a dirty, miserable, set of individuals and are of all ages. They are dressed in the most grotesque manner imaginable. It seems that they pick up any old piece of clothing that they find lying about and wear it. The result is one sees them going about dressed up in old pyjama suits, Overcoats blankets etc and these, in conjunction with their own native dress make them look ridiculous in the extreme. Their chatter, chatter all the time they are working makes one wish them far enough away. A big number of them have died here. We passed their graveyard, each grave being marked by stones gathered from round about. 
 A general air of jollity is prevailing about the camp to-day.
Dec 26th Sun.
 

 Had route marches to-day in order to give us exercise. There was quite a spring touch about the air which made walking a great pleasure indeed. We have got very comfortable quarters, a big contrast from the miserable existence which we had, when we were on our way to Anzac, at this place. We had evidence of the strict discipline of the so-called British Army. Two young fellows were standing tied to a post. This punishment they had got for purely minor offences. 
 Men in the Aussie army do things ten times worse and get nothing done to them for it.
Tuesd. Dec 28th
 Since coming to this island I have witnessed some of the most beautiful sunsets and sunrises that I have ever seen. Read A. Proctor's "Legends and Lyrics". 
 We were all put on fatigue work for No 3 A.G.H. What a grousing we had over it and it ended more in comedy than in work. Being fatigue for a unit that has plenty of men of their own does not go down very well with us, who have just come off the peninsular. Especially so when the N.C.O.s are trying to bully us into it. 
 In the evening we went to a Lecture on "Canada" given by Capt Frost, a Canadian Chaplain attached to one of the hospitals
 

 The Lecture was very good and the songs sung which included "The Maple Leaf" "Annie Laurie" "Home Sweet Home". Imitation of the bagpipes on the mouth organ were greatly applauded by the large audience.
Dec 29th Wed.
 Route marching and drill are becoming a daily occurrence now. The officers say that they want to keep us fit. Our officers all being medical men they would be better to leave the drill alone, as they only make laughing stocks of themselves when they attempt to give commands. In the afternoon we went to one of the villages. The same crowded streets and the orange sellers doing a roaring trade. Came across the ancient and the modern in one house. An old woman was busy seated at the doorway of her house spinning wool by hand. It was pretty to see her deftly spin a nice even thread of wool by her hand. Inside the doorway of the same house a young woman was working away on a Singers sewing machine. Looking inside some of the houses of these villagers they look very clean and nice but simply furnished.
Dec 30th Thurs.
 Went a long route march over the hills to the thermo springs which have a curative effect it is said. The march was about fifteen miles. This range of low hills are of a volcanic nature.
 

 There were some pretty pieces of country situated amongst those barren hills. We marched along the splendid road that has been made here, when we got outside of the long line of tents we cut across country on to a narrow and rough path on which we could only walk in single file. As we climbed up the hills we passed villages which have been built, very high up and the higher lands are used for sheep rearing. Herds of sheep and goats are all over the hills. Some of the sheep have bells hanging round there necks and there moving about gave a very pleasing sound in the still quietness of the land. After a little refreshment we went in and had a sulphur bath. This was paid for by one of our odfficers. This sulphur bath was very refreshing. It came from out of the earth and was exceedingly hot. The room was lined with enamel tiles. The water poured out into four basins and by means of tins we poured the hot sulphur water over us. 
 We returned by short cuts to the camp again and arrived back about four o'clock.
Dec 31st Frid.
 Dreamt that I had been taken prisoner by the Turks and after going through various unpleasant experience I was awakened by a tremendous noise of sirens foghorns etc going off. They caused a tremendous noise the reason being that they were 
 

 welcoming in the New Year. The three Tommy Officers who were on the peninsular with us left the unit to-day. 
 New Years Day 1916
 Very quiet day. Roads too muddy for walking. Went to a concert held by No 2 Canadian stationary.
Jan 2nd Sun.
 The large Hospital Ship "Britannia" came into the bay to-day.
Jan 5th Wed.
 Very stormy to-day. Storm played havoc with our tents. Inoculated and specimen of blood extracted from our ears. Cases of meningitis are said to have been amongst us. Some of the boys suffered from the inoculation considerably and all had a look of absolute misery about them from the effects of the inoculation.
Jan 6th Thurs.
 Five of our number were transferred to hospital, sick.
Jan 7th Fri.
 Feeling much better today. Fatigue work for No 3 G.H. Treating the work as a huge joke. Exasperating the bullying Sgt. Major of No 3 G.H. counted out. Went for the O.C. but he could do no good whatever No 3 A.G.H. men loafing in their tents and have the audacity to ask us to work for them. The Army makes men chronic
 

 grumblers.
Whither it was for spite, or what it was I do not know but we had onions cooked with their skins on for dinner to-day. Reported it to the Orderly Officer. As per usual, no satisfaction.
Jan 9th Sun.
 Football match, Australian rules, between our Officers, N.C.Os. against men. The men win. Read and thoroughly enjoyed "Multitude and Solitude" by John Masefield.
Jan 10th Mond.
 No 3 A.S.H. taking down this hospital as they are going to move. We are working more in harmony with now that their own unit men are working also.
Jan 12th Wed.
 Read "Tom Sawyer" by Mark Twain. Fatigue work along with Tommies taking down the Hospital.
Jan 13th Thurs.
 Paraded with full marching order to embark this morning. We are about the last Australian unit to leave Lemnos. 
 There are so many Scots and English troops about here that taking a walk of an evening one feels like "bein at hame". The old familiar tongue is being spoken.
Jan 14th Frid.
 Parade at seven forty five, Dress parade eight forty five.
Went on board the barge along with the sisters of No 2 Stationary Hospital. By it we were taken
 

 out to the Hospital Ship "Dunvegan Castle". This ship is not as good as the "Dongola".
Jan 15th Sat.
 Heavy rain fell during the night and all day. The sea very choppy. Still in the harbour at Lemnos. No 2 S.H. who have been loading their hospital gear on board all day made a tremendous disturbance on board. They seem to have got a great deal of intoxicating liquors, the result is, the place is more like a rowdy public house on a Saturday night than a hospital ship.
Jan 16th Sun.
 Went to the Church of England service to-day. The weather much improved. In No 2 S.H. still loading up their gear. The "Olympic" sailed passed us on its way out of the harbour.
Jan 17th Mon.
 Sailed out of the harbour at eleven o'clock this morning. Fine weather and a calm sea. We sailed past many ships, warships etc and also the large hospital ship Mauratania as we sailed out of Lemnos Harbour. There were two good sized Hospital ships lying along side of the Mauratania and yet they looked like mere pigmies beside it. This made us realize how big the "Mauratania" is.
This ship the "Dunvegan Castle" has got the name of being a rocker and it is acting up 
 

 up to its reputation. In spite of the fact that the sea is comparatively calm this ship is rocking considerably. A big number of Scotch chaps, mostly Aberdonians, are on the hospital staff of this ship. Read the "Mill on the Floss" by George Elliot. 
 A concert was given by the R.A.M.C. on board. The songs and the accent of the homeland prevailed, naturally. The criticism of the Aussies, to me, was rather amusing. They said that it required an interpreter to understand what was said sung, however, judging by the applause and the laughter amongst them they seemed to thoroughly enjoy themselves. 
Jan 19th Wed.
 No 2 S.H. have got their mail. Ours is said to have gone over to Tel-el-Kebir much to our disappointment. Reached the harbour of Alexandria between the hours of seven and eight this morning. There is lying near our ship a large number of sailing ships. They are said to belong to the Turks and have been captured by us. No 2 S.H. have a "Blow out" to-night. It seems that two of their officers have been promoted and to celebrate the occasion they have given the staff some drink. The hullabaloo can be better imagined than discribed. The noise of their loud laughter
 

 and the looseness of their tongues with rude jest along with the fumes from the beer and the smoke of their pipes are to say the least of, disgusting.
Jan 20th
 Finished reading "A Son of Hagar" by Hall Caine.
Jan 21st
 Tugged up to the wharf about two thirty. An exciting thing happened as we were being tugged up the harbour. A few of the native boatmen who pester our lives out in these harbours were sailing near the ship. Our propellor suddenly began to work causing a kind of suction and it was with great difficulty that these boats got free of it. One boat with a man and a boy in it upset but were saved.
[Transcriber's note: 
 Pg. 9. Mamalukes = Mamluks
 Pg. 78. Mauratania = Mauretania] 
[Transcribed by Gail Gormley, Adrian Bicknell for the State Library of New South Wales]