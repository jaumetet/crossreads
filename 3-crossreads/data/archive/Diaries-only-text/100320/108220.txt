25/11/18 Bohain
 A wet miserable day of drizzle. The 5th Bde marched out in it and looked very bad. Colonel Miles came around and mentioned points for us to avoid tomorrow. Went round to Matthews billet had a yarn with him. The people were very agreeable and find it hard to realise that they are free. The woman of the house said they were down in the cellar when allied troops entered the village. She went up to get some water and saw an English soldier coming warily scouting along the street and realised that the Huns had gone for good. This morning in the streets there were many people returning to their houses pushing along little hand carts holding all their poor belongings ? a few mattresses, blankets chairs.

 
 25/11/18 Bohain
 After lunch walked down to where the Brigade starting point is to be tomorrow. A foggy day and mud and slush everywhere. Passing motor lorries splashed mud over everyone. The local Hotel de Ville has been burnt down by the Huns. Streets crowded with all kinds of troops. A cinema show close to Bde HQ was crowded out and a large number of men were waiting in queue out in the street. Went down to the 24th and saw Selleck and then to the 22nd and had a talk to all of them up there ? a good lot of chaps. Jack came along to mess and spent the evening with me. Brown Hislop have had a very busy day engaged on details covering rations and finding billets for the Brigade group tomorrow. A mine blew up the railway near Cambrai.

 
 26/11/18 Bohain
 It was dark at 6.15 when Charlesworth called me but daylight grew during shaving time. A dull day but fine and the sun will likely shine out later. Breakfasted at 7 am and all the signs symptoms and evidences of moving day were about. Men running round with packs and other gear and everyone busy restless. Not having moved in route march for some time as a Brigade it will be rather interesting to see today how long different sized columns take to pass certain points. The present strengths of Bns are about 22nd 530 23rd 560 24th 870. No rations have come to hand and the troops are only rationed up to tonight owing to a mine having blown up the railway line somewhere near Cambrai. Hislop Bde Bomb Officer is a good youth with plenty of sense.

 
 26/11/18 St Souplet
 The brigade passed its starting point at 9.30 pm and on the whole was turned out well. With BM McColl stood by the side of the road and watched all go past. Fog came down and we could not see far ahead. The Battalions all carrying their colours and flags. General Robertson was at a x roads and I met him there and again watched the column go past. We got into the village of St Souplet ? a place which had been well knocked about by the guns. Had a few sandwiches and then to horse again and rode through St Benin to Le Cateau. En route passed a large high railway bridge demolished. It had rested on high masonry piers but an explosion had ruined the whole of it. Coming into Le Cateau saw duds being blown up by electricity. The cobble stones

 
 26/11/18 St Souplet ? Le Cateau
 were very slippery. Plenty of troops were in the town of Le Cateau it does not seem much damaged. The church has two quaint towers and has been shelled. Pushed on to Corps Headquarters and there attended a conference of all the Brigadiers and Divnl commanders of the AIF ? a historic gathering. Had a talk to General Gellibrand who was in good form. Saw General Birdwood and had a short yarn with him. He made a speech saying John Monash was to go to England as Demobilization Director and that Sir John Hobbs was to command the Corps. Discussed all the plans for demobilization. Returned to Bde. at dark held a conference. Brazenor back. Arranged for car at 10 tomorrow.

 
 27/11/18 St Souplet
 Note: photocopied page cut off so not 100% certain of date
 Very raw and cold at dawn when called by batman. Rose shaved and had breakfast by 7 o'c. All bustle and stir prior to move on to the Favriel area. Brazenor came in and we had a long yarn as I handed over command of the Brigade to him. At this billet there is a very old man of 70 and his wife who are living in the most utter misery ? cold, short of food and with their only son dead. They expected a golden age when the armistice came but find that things are still bad. War is very terrible to poor people like this. A car was coming from DHQ to pick me up here and take me as far on my journey as Amiens at 10.30 but at 11 there is still no sign of it. Waited a long time and then took a walk down to the Town Mayors Office to see if he had a phone and then tried to 

 
 27/11/18 Note: photocopied page cut off so not certain of date; cannot read enough of place
 raise 2nd Aus Div through the HQ Bde. of RAH but without success. Returning to the billet I found the car waiting. Jumped aboard and sailed off in style lounging back in the glass covered auto of the Divisional General with an aide de camp in front. Roads were rather bad. A dreadful stretch of desolating land had to be traversed ? all the villages obliterated or in ruins. Poor people poking about the few stones of their homes. There were many gangs of German prisoners at work on the roads. Came through sideroads until we found Villers Bret. and then into Amiens. After an hours wait took train at Amiens station at 3.55 for Paris. Crowds of Yanks aboard. Three as travelling companions and a few spiffing Frenchmen butted in "La Guerre fini" the staple remark. Feeling very unwell with

 
 27/11/18
 a bad cold. Reached Paris at 7.45 pm and had a great hunt for taxi a small boy contemptuous of 2c tip found a taxi at last. Drove to Hotel Continental and secured a room. Many places decorated with flags ready for the visit of King George V tomorrow. Streets brightly alight and electric signs in evidence ? a great change. Left Continental and walked out to dinner. Noticed Vendome Column sandbagged at base. Went to Arrigones in Boullevardde Italiens for dinner ? a good one soup de poisson, fish and a pint of Chianti 9 fr 35. Amusing to watch the gesticulations of diners. Returning to hotel noticed nice little girls plying their trade and many [indecipherable] Parissienes with escorts. All daintly charming with a chicness that is all their own which the miss of England lacks. Bed 10.30 ? a long day.

 
 28/11/18 Paris
 Had a troubled night's rest as the room was overheated and I had a feverish cold. However a good sweat seemed to do me good and I felt better after a hot bath. Went down to a breakfast of chocolate and eggs ? very dear. Then to a chemists in the Place Vendome which was a mass of flags and royal arms. Unfortunately the rain which was a persistent drizzle spoilt things rather. Through the Rue de la Paix to the Opera and had a look at the Statue of Dance which is said to be rather ?abandoned?. Took tube at the Opera for Gare de Lyon. A trip in the Paris tube is always a pleasure so clean and efficient ? a great contrast to London. At Gare de Lyon booked through to Monte Carlo reserved a seat. Returned to the Continental for a lunch of tongue salad and a bottle of

 
 28/11/18 Paris
 beer ? 11 francs and 1 fr. tip (10/-). The rain was coming down quite heavily as I walked down to the Place de la Concorde all beflagged and crowded with captured Hun guns. Crowds were assembling and there were plenty of soldiers of the Republican Guard with their brass helmets. Walked down the Champs Elysees where enterprising people were selling chairs and other stands. After a fairly long wait a cannon sounded cheering started and His Insignificant Majesty King George V drove past with M Poincaire as companion. Following him came the Prince of Wales and Prince Albert. In front were soldiers of the Guard and blue helmeted mounted poilus lined the street. Now took the tube to St Germain de Pres a very old old church and dark

 
 28/11/18 Paris
 within. Old people creeping about "this tomb" and old people sitting in chairs to sell candles relics. Now marching on the map to the Church of St Sulpice ? an immense place and not so dull. Again noticed old age by devotion trying to ensure good things after death. Walked down to the Jardins de Luxembourg now desolate and winter dreary. Now to Boulevard St Michel and past the Cluny Museum. Cafes gaily lit up. Smart officers pretty girls. Yanks everywhere. Tubed home from Odeon there were great crowds about the whole place. Did soon arrive at the Continental. Took taxi for Gare de Lyon at 7. with some American sisters as companions. Found my reserved seat in the train for Nice and got a pillow and blanket for 4 fr.

 
 29/11/19 en route Marseilles
 A troubled night. Slept in snatches sitting up in my seat with a rather charming mademoiselle sitting just opposite. She was well dressed and agreeable ? later turning out to be a Madame with a little girl aged 4 and the wife of a French naval officer at Toulon. An elderly Frenchman of independent means and his wife were also travelling and were very agreeable to talk to. We passed Lyons in the dark and as soon as dawn came found ourselves running along the RhoneValley. Breakfasted in the restaurant car and made the acquaintance of 3 Australian nurses. The country now underwent a change becoming more hilly and rocky as we reached Avignon. The sun now began to shine out warmly and we were conscious of entering a new country ? a land of sun and of pleasure. This is 

 
 29/11/19 Marseilles
 a wonderful contrast to up North. Green trees in full leaf ? a grand sight. After running through rather rocky barren country abounding in funny old castles we swung out on to flatter land with many vines. Made Marseilles at noon and got a basket from the buffet. One does not get a good view of the city. The railway now ran along the sea and the sun was hot. At Toulon handed Madame of the opposite seat over to her husband a Commodore in the French Navy. The country from now on was very lovely. We came into St Raphael in daylight. The Corniche d?or commences here and a road fringes the sea the whole way. Many beautiful bays and fine villas. Green trees abound. Reached Nice at 6 and the old French couple who were so friendly got out and I had the place to myself. The 

 
 29/11/18 Nice
 train is cut in half at Nice and waits half an hour. The wagon restaurant or dining car has had a wonderful day bfast today going all day long. The journey from Nice to Monte Carlo did not take long and I found a charabanc at the station which ran me up to the Monte Carlo Palace Hotel a fine place where only a few people seemed to be staying. At dinner there were not many people at all. Two girls in a family party were smoking and "making up" at the table. This place is in the Principality of Monaco and an independent state. The Casino is going in full swing and some of the people from here went down during the evening. I wrote and read for a while and then retired early to my room to make up by a good sleep tonight for what I lost last night.

 
 30/11/18 Monte Carlo
 Beautiful sunny day like Australian spring. Went down to the Casino only a short distance through streets all lined with palms and other tropical trees. The Casino is a fine building all white and there were crowds of Americans concentrating to see it. Went in the main door and through all the different gambling rooms. There are not many tables but there is plenty of room for spectators. The little money rakes were lying on the tables ? the wheels are very smooth running and flat on the tables. Saw the richly decorated little theatre. Went out on the terraces overlooking the bluest of seas. The mountains rise steeply behind the town white patches of rock. In the distance one sees other towns breasting the water's edge all their buildings snow white. The highest peaks of a chain 

 
 30/11/18 Monte Carlo
 East of the town represent the frontier between Italy France. Many Australian trees flourish here. Trams run hourly to Cap Martin and Mentone so I took one and had a pleasant ride. From about halfway one could look back and see the town of Monte Carlo clustered up the hillside. Rounding Cap Martin came into Mentone which is extremely pretty with high fog wreathed mountains and all terraced. There were many French black troops here and they seem to occupy unused hotels as barracks. Towering over the town is the luxurious Riviera Palace Hotel ? a magnificent place embowered in lovely gardens. I lunched here. It is about the most palatial hotel I have ever seen. The view of headland and sea from the windows of the 

 
 30/11/18 Mentone Monte Carlo
 Banquet Hall reminded me of Mount Lavinia in Ceylon. In the gardens were a good many flowering gums. Returned to Monte Carlo about 3 and just in time for an orchestral concert in the theatre of the Casino. There was a good audience and the music was first class. A good many English speaking people other than the multitude of Americans. After another stroll on the terraces went into the Atrium of the Casino which is a sort of lounge. A good crowd appeared to be thronging into the gambling rooms. A few little "ships passing in the night". Taken all round things are dead quiet in Monte Carlo. Shall voyage into Nice tomorrow and see what is doing there. Things livened up considerably after dinner. The usual party was there and 

 
 1/12/18 Monte Carlo
 Another fine day sitting out in front of the Casino admiring the moonshaped bay swinging round to Cap Martin when who should come up but Davis and his wife with him. Got aboard the Nice tram at 10.These trams creak and squeal all the journey. The run down to Monaco and then on to Nice is gorgeous ? high cliffs, hundreds of white villas and deep blue sea. Reached Nice about 11.30. Passing through the old part of the city we came on a square and it flashed through my mind how like a square in Turin it was. Just then I noticed that the chief caf in the square was Caf de Torino. The whole town of Nice is strongly reminiscent of Turin with its arcades. Left my coat in Hotel Ruhl Anglais and took a promenade on the far

 
 1/12/18 Nice
 famed Promenade de les Anglais. It fronts the sea and is lined with palms. The lovely warm sun had enticed everyone out and it was a very chic crowd that was walking up and down. Crowds of Americans and many French officers. The latter all gay with their decorations. For lunch went to the Hotel Negresco which is a most palatial place wonderfully furnished and the acme of luxury. They charged me for a moderate lunch 16 fr. plus 1.50 fr = 17.50 fr = 14/2!!! Continued the promenading after lunch and had a conversation with a piece of impertinence who had spent long time in Morocco. She said she was available did I need a cicerone for Cannes or Antibes. Returned to Monte Carlo about 8.30 and went to the Casino. It was 

 
 1/12/18 Monte Carlo
 crowded and very gay. (By the way today was Sunday!) Met some staff officers there from Cap Martin and arranged to dine with them on Wednesday. After dinner things were deadly quiet and I determined to clear off to Nice in the morning.
2/12/18 Another lovely morning. The bright sun shining right into the room. Paid bill and walked round the Casino for the last time. The view all round this place is simply superb. The whole place is swept and garnished two or three times daily ? not matches or cigarette butts even are left lying round. The quaintly uniformed guard look to the cleaning up of all. Caught the train at 10.30 and rather enjoyed a little cinema drama. Evidently they were making a film and doing the parting at 

 
 2/12/18 Monte Carlo - Nice
 Monte Carlo right on the spot. The dresses of some of the ladies were daring. Reached Nice at 11.30 and was driven to the Hotel du Luxembourg which is a poor place badly furnished. Decided to have all my meals out. Put in a considerable portion of the day lounging in the sun on the Promenade des Anglais with occasional excursions up the streets. Put in an interesting trip round the Galeries Lafayette hoping to buy a piece of Mons ribbon. The shop was crowded. Darkness comes up at 4. At 6 I went down to the Hotel Ruhl Anglais which is very select and expensive ? a gay crowd here. Dinner very elaborate cost 21 fr. including the tip. Afterwards took a walk and met the tiniest of little Parisiennes out for a walk. Did not reach hotel until 11.30 soon fell to sleep tired out.

 
 3/12/18 Nice
 Bedroom flooded with bright warm sunlight though the day is inclined to be hazy. Went out on to the Promenade des Anglais and read the "Petit Nicois" sitting basking in the sun. Took a walk down to the station and located the APM. After lunch went out on the Prom. again and had a long talk with a little girl who turned out to be a Pole but educated in France. Her father was taking his apperitif and we had a long yarn. Very convenable proper she did not hesitate to discuss the stupides. Dined at Ruhl et Anglais and had a long yarn with a Lieutenant of Artillery from New York City. The view from the prom is very fine ? one horn of the Bay des Angles is [indecipherable] Hill and the other is Californie. Very beautiful English uniforms a rarety here. [indecipherable].

 
 4/12/18 Nice
 Another fine day but inclined to be hazy and here is how I loafed it away. Stayed in bed till 9 and then had chocolate and a roll before going up the Avenue de la Gare to buy a local paper. Returned to Promenade des Anglais there to bask in the sun and read the latest news till lunch. Watched the crowd passing and repassing and then went to the Hotel Westminster for lunch. After repeated the performance meeting a Yank Lt and walking with him and having an adventure with two little Nicois ? walkon walkoff actresses at Les Varieties. To Yank YMCA in Casino until 7.30 and then dined well and dearly at the Hotel Ruhl et Anglais. This is one of the finest hotels to be met with anywhere admirable cusine. Few people about the streets as I returned to room at Hotel du Luxembourg.

 
 5/12/18 Nice
 After a cup of chocolate set off at 9.30 for a walk up the street and to buy a local paper with all the latest news. Took it and sat on a seat on the Promenade des Anglais in the beautiful radiant sun. At 10.30 paid my bill and took the hotel omnibus to the station where I found that the reservation of my seat was only as far as Marseilles. However there was a Yank Colonel on the train who was not using a ticket he had so he gave it to me. The view around Les Antibes was very fine. Had a long conversation with a French Major who was wearing the Legion of Honour Croix de Guerre, DSO MC. He spoke perfect English and was liaisason officer between Generals Foch and Haig ? consequently well in the know. The scenery round Cannes was very fine and thereafter 

 
 5/12/18 Cannes
 as we went along lovely little glimpses of landlocked bays little islands kept coming into view. Dined on the restaurant car with 3 French people ? one a very corpulent not overclean cure. Yarned again with the nurses until Marseilles where I changed trains and after a bit of rushing about at last got settled down in a carriage with an old French couple and officers of French Infantry and one of their Flying Corps. On all the trains here there are plenty of Americans. The journey homeall night was the usual tiring and fatiguing one. Had dinner in the dining car and an animated conversation with a very dainty little Parisienne who was sitting at the same table. Later she was in deep conversation with a Japanese officer ? evidently her cicerone.

 
 6/12/18 Paris
 The train was about 2 hours late when we were at La Roche and could not make it up. From the green leaf and sun of the South to the fog and bareness of the North was a transition that was very evident this morning. We pulled into the Gare du Nord at about 10 and I tubed to the Tuileries and then walked to the Continental where my room was reserved. It was fortunate that I had wired for it from Nice as I heard them turning other people away. Good room and bathroom 25 fr a day ? another instance of the cost of living. The hotel is kept almost oppressively hot by steam heating. Walked round to the Place du Marche St Honore and reported to the OC Troops. Then walked up to the Avenue de l?Opera where battalions of blue coated poilus were depiling. They halted piled 

 
 [date and place have not been photocopied/or cut off]
 arms and made a very neat job of piling their packs rifles. Did a little shopping and returned to lunch at the hotel. Afterwards again went up to Avenue de l?Opera where crowds were gathering and got a good position on an "island". The girls at upper windows amused themselves throwing down money eatables and cigarettes to the poilus lining the streets. One lady won much applause by walking along with a big box of cigars letting the men help themselves. A good natured crowd. The scene looking up to the Opera was a memorable one the fine street all bedecked with flags ? lovely buildings. After a big crowd of gendarmes coming along a motor dashed by with M Clemenceau in it. Soon after came gendarmes on bicycles and then a prancing escort of dragoons of the Republican Guard. In a 

 
 6/12/18 Paris
 carriage following was King Albert of Belgium and M Proncare ? Albert looked worried. The queen was in the next carriage and the Crown Prince in the one following. After the procession had passed various battalions started marching home ? some cavalry and lancers passed all very smart capable and all in the ordinary horizon blue steel helmets. Booked a seat at Thais tomorrow night and walked down past Caf de la Paix for a block. After a roam around Bvd des Italiens came back to Rue de la Paix now very brilliantly illuminated many of the modistes having bright electric signs out. There is an enormous amount of wealth in this street. In the twilight a battalion of chasseurs marched past with a band and raised tremendous enthusiasm. They march with a 

 
 6/12/18 Paris
 short sharp quick trot like step. The midinettes crowded to the windows over the street and the air was full of acclamations. I stood next to an old vanman in the street. He said "We fete them for they have won after 4 years. Two boys of mine are killed". Returned to hotel passing many beautifully dressed girls. After writing for a while set out again up the street. There were great illuminations tonight. The Opera Square was full of electric signs and the Place Vendome also very gay indeed. Quite a blaze of light everywhere. Went to Arrigonis Caf Italiens and dined well having as neighbours at the next table 2 American officers with two girls. All very well oiled. Had a long yarn with them and then pushed off down the thronged and lighted boulevards through the Place Vendome back to hotel. A  bottle Chianti has a great effect on one.

 
 7/12/18 Paris
 Rather dull morning. First tubed up to Gare du Nord and there saw the RSO. The busy station as usual crowded with French American troops. Set off on a little tour of exploration round the Rue LaFayette but soon took a ?bus back to the Opera. The usual crowd was outside the Printemps and other big shops. Near the Opera bought a seat for Faust on Sunday night. Then to hotel and then by tube to Boulevard Michel. Found the Cluny closed and then had a fine lunch for 6 francs. Oysters fish and coffee. Walked over to Notre Dame and found it as sombre as ever. An old man holding out the holy water brush and a nun soliciting alms for the poor. Taper sellers and others live all day in this dark cold place. Met some American nurses and took them over to Sainte Chapelle now quite 

 
 7/12/18 Paris
 different as all the glass has been removed. Got through into the Law Courts and saw the lawyers walking up and down. By cheek got into the Supreme Court ? lovely ceiling and decorations. Now saw where Bolo was tried and then went across to St Severin a very old church dated 600 AD and with fine stained glass windows. Through to the back to St Julien des Pauvres which is also an old place and the chief Greek church in Paris. Now to the Sorbonne and Church of the Sorbonne where we saw Richlieu's tomb with his hat hanging over it and then up to the Pantheon which was closed. Noticed the statue of Le Pensura. Now went to the church of St Genieve (?) where we saw her shrine surrounded by candles ? a very pretty little church this. Walked down to Bde Mich

 
 7/12/18 Paris
 and there left the nurses. Got lost after wandering among the old booksellers boxes and ended up at Gare du Orleans. Tubed to Opera bought the book of Faust and then returned to hotel. Went to Arrigonis for dinner and then to the Opera to see "Thais". Had a few words with a very insolent elderly doorkeeper and came near to knocking him right down the marble staircase. Had a fairly good seat next to some Yank VDVADs and we were soon friends. The house was crowded and there were any number of English speaking people there. A very fine dancer in the "divertissement". The scenery music of this play are both very fine indeed. Walked back at 11 along the Rue de la Paix to my hotel and then read for a while before going to sleep ? a tiring day.

 
 8/12/18 Paris
 Got out at 9.30 and took the tube to Marbeuf. All the Champs Elysees down as far as the Arc de Triomphe is lined with captured enemy guns. Walked a long way up the Rue de la Boetie and found Hotel Rocheufauld looking for Sisters Wilson Morton. They had just left so I went off chasing up to the Gare St Lazare where they were waiting to put their baggage in the consigne. Found them and then walked down to the Madeline where we went inside in time to hear High Mass commence. An old man wearing a skull cap sits at the entrance of the church holding a holy water brush out. A bearded priest in gorgeous vestments walked in procession in through the church waving a spray of incense or holy water. He was preceded by an old beadle in cocked hat who struck the pavement heavily with his staff as 

 
 8/12/18 Paris 
 he swung along. From here we took tube to Abesses and walked up through picturesque slums of Montmartre to the Eglise Sacre Coeur which crowns the height. The day was slightly foggy so the view was spoiled. Inside the big church is by no means bare and many hundreds of candles were burning. Now returned to tube so to Opera. Walked to Arrigonis where we lunched well and then by metro to Cite. Had a look at the Conciergerie and then to Notre Dame which lighted up looked better than ever before. A bishop in crozier mitre and golden vestments was conducting a ceremony. To St Julien Les Pauvres and St Severin. At the latter there were crowds of nuns and novices and a cleric was delivering an eloquent sermon. We then walked down Boul Mich and tubed back to Opera

 
 8/12/18 Paris
 where I left the nurses. To hotel. the Place del?Opera and Rue de la Paix very crowded. Had a rest in the hotel wrote and read up the story of Faust for tonight. Went to the Opera after dinner hurriedly taken at Arrigonis. Arrived just in time and had a seat between two French girls and a civilian. Close up to the orchestra and a splendid position. "Faust" was rendered splendidly and an excellent ballet was also included in the bill. Between acts walked down into the foyer which brilliantly illuminated is a very fine hall. Everyone who is anyone walking up and down here. The concluding scene of Faust was much differently staged to the representation I saw at Florence. The interior decorations woodwork carving etc. of the Opera pass description. Returned to the hotel just before midnight. 

 
 9/12/18 Paris - Caudry
 Awoke at 7 and shaved. Paid bill and taxied to Gare du Nord. Took a walk round the streets here before finally taking seat. Had an uneventful and quick journey to Amiens. Left Paris 9.30 ? Arrived Amiens at 11.30. Went straight to consigne dumped bag and then up to Hotel de la Paix for a bite of lunch. Met a Major of the RAF who gave me a lift to Peronne in his car ? a quick ride. There caught at once a motor lorry or bus and had a dreadfully jolty ride to Caudry which we reached at 6.30 about 80-100 miles by road. The journey from Paris to Caudry just about a record I think! Saw Town Major, got a billet, bought some food from GFC and had hot tea and hot food at church. Many old soldiers. Turned into bed with a subaltern of the 4th Divisional Artillery.

 
 10/12/18 Caudry
 Awoke and up at 8.30. The Canadian Town Major presented us with a good breakfast. Lorryhopped to Le Cateau and there caught another which took us to Landrecies. Changed lorries here into an artillery column and went on to Avesnes. the country now began to undergo a remarkable change becoming enclosed. Very green and covered with hedges ? just like Somersetshire. Rain commenced now. Made for 4th Div. When at Avesnes lunched and then went to Corps HQ where I found out the location of the battalion. Returning down the street met a YMCA man and had afternoon tea at their HQ. A 6th 
 Btl car drove up then and took me out to the very little hamlet of Bologne ? not unlike Selles. Had a yarn to the General and then back to Mess. Bean spent the evening getting history from us.

 
 11/12/18 Boulogne
 A wet morning. Spent it in walking around the streets and lanes of this truly rural hamlet inspecting the company lines and talking to officers and men there. Rations are not too good at all. The country is all enclosed hedges and quite like Selles and like Taunton. People have not had a bad time. We run two messes for the officers. HQ A B at one and C D at the other. The afternoon was also very wet. Hutton came back from Charleroi which he says is a splendid fine town and in full swing. We shall 13 kilos out and Mayer says cognac other liquor is frequent. Warmed to the French people at my billet they have a peculiar accent patois. Got very little news from the Huns during their occupation. Wrote letters all evening. 

 
 12/12/18 Boulogne
 Another very wet and beastly day. Went up the winding road into the little hamlet as above and found Colonel James at his billet. After a yarn we walked down to Bde. and there met Generals Rosenthal Robertson. Inspected transport all the morning and had a sort of conference about doings when we get as far forward as Charleroi. Walked back to billets and lunched. In spite of pouring rain a few of our sports enthusiasts were hard at their training running up down the roads. The buildings around here all have very low rooves and no windows practically. Classes on the go all the morning for telegraphy, English and other educational subjects. If the weather were at all fine things would be different. A claim today for a duckhouse pinched!! Jack dined here.

 
 13/12/18 Boulogne
 Another inclement day but fortunately a little finer. Went down to "D" Co who are quite a step down the road. Saw Groves and the others but everyone is loafing about under the influence of nothing to do and all day to do it in. Afternoon some home mail came. Two football matches played in wet grassy fields hedge surrounded and forming a bleak looking picture. Shall not be sorry when moving day comes and takes us away from here.
 14/12/18
 Much finer. The sun came out for awhile in the morning. Visited A B Coys. and had a talk to different people. At 2.15 went down to the village and attended a meeting of all Bde. officers under presidency of General Rosenthal. Talk about Charleroi. Had afternoon tea with the General Colonels James and Savage. After mess attended a meeting of mess and then an A Co concert.

 
 15/12/18 Boulogne
 Sunday. A much finer and milder day. There was a voluntary church service held by Padre Blood at the HQ billet and at 11 I held a conference of 
 OCs QSM IO at the mess and discussed matters regarding future shifts. After lunch took the Keithlet for a ride through Avesnes out on to the main road which we have to take on Tuesday. This country is remarkably like Somerset and from one place just before entering Avesnes there is a fine glimpse of gentle undulating old world scenery. Through Avesnes ? quite a good town with several old world buildings and twisting cobbled streets. Rode home in time to see a thrilling football match between A Coy B. Pay night tonight. Spent part of the evening watching Pat Gorman writing his history which is now completed up to Ville-sur-Ancre.

 
 16/12/18 Boulogne
 Full marching order parades and lectures to the men but the rain came down pretty constantly throughout the morning. Inspected the personnel of the Transport Section and found them below standard. Told Edwards he was for a company and put Thewlis on in his place. Had a talk to Harricks about "C" Coy. and then did some correcting of the Battn. history. The whole of the afternoon put in at reading and writing. The comedy of the duckhouse still continues and tonight Gorman, a g.s. wagon and the pioneers are to figure in its reconstruction. There is some dissatisfaction regarding the present running of the mess and tomorrow night we hold a mess meeting to discuss it. The Keithlet brought the usual mass of papers a little earlier.