1919
Aug. 21
[Photograph with the caption, "G.V.R., Aunt Nell, Mum. Taken at Anzac Buffet, The Domain"]
A decorated taxi was waiting for us in fact had been engaged in the morning and drove us all out to Shepherd St. Ashfield, where the house had been decorated with flags.
[Photograph of decorated house with the caption, "Shepherd St. Ashfield"]
Auntie Bell, Minie, Isabel and Florence came rushing out to meet me. The house