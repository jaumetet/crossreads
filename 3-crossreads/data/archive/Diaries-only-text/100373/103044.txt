quantity of printing material to print photos of the troops landing at the Anzac Beach.
Night Duty B. Block. Bot two books [indecipherable] Legends by Adelaide's Proctor, Psalms in Human Life by Prothero. 
I notice that the wounds of the men are healing up beautifully. 
One of my mates D. was at an Arab wedding. The customs on this occasion were very amusing but according to what he says they don't have much respect for women. 
The number of patients in hospital are gradually diminishing. They are being sent to Helonan Convalescent Home.
The flies in the daytime, and the mosquitoes at night are awful to bear.
Prizes are being given by the Officers in the hospital for the best description of any incident which occurred during the "landing" "Springy" is acting G.C. of the hospital at present.
May 13th Thur.
Went to Cairo. A visit to Cairo always means a visit to Groppi's for one.
May 14th 15th Sat.
Cairo. Went into the grotto at Esbekiah Garden.
May 16th Sun.
Went to the St. Andrews Church. Had been on night duty, which, coupled with the very hot day made one very drowsy. To work during the night is very well, but one cannot very well get sleep during the day, owing to the extreme heat even in tents and also the flies almost drive 


 one wrong in the mind. To lie under the shade of a tree or wall is much cooler than to lie in closeness of a tent. The flowing robes and the loosely fitting, light coloured clothes of the natives gives a good illustration of how the people of a country cloth themselves to suit their climate conditions.
During the hotter part of the day from 12 midday to 3 or 4 o'clock in the afternoon one comes across natives lying in the shade of a tree or a wall even on the pavement of the streets in the city. Lying on the ground they throw their cloak over their heads to save them from the fly nuisance. I can't help thinking that the manner in which the native women carry their water jugs and their baskets on their heads is meant for the same purpose, namely to ward off the flies. By carrying thse things on their heads, their hands are free. It is very interesting to see these women balancing their water jugs and baskets on their heads and waving the hands and arms in a peculiar fashion as if to whisk away the flies. Necessity is the mother of invention and this method saves the use of the fly whisks that the people of this nation use at this time of the year in Egypt.
A kind of tree which looks very pretty at this time of the year is  . It is similar to our Rowan tree but has beautiful blossom of a nice bluish tint. It is grown a great deal here to line the roadways and looks very pretty.
Amongst the working class of the city