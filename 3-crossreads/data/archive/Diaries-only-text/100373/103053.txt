and I have also met others who were bored with the place.
June 13th Sun
Went to the Scots Church in Cairo this morning. During the day the sun was exceedingly hot and the sweat simply pours out of one. The sweat could be seen on my light drill clothes which we wear here.
It amuses one to see some of the chaps who have not yet been issued with their light clothing, coming in to church with the sleeves of their tunic rolled up.
The evenings are nice and cool. It is very cold about 2 o'clock in the morning.
In spite of the fact that we are having a comparatively easy time here I wish to get away to the scene of action. Trying to arrange a transfer with a chap in the Field Amb.
June 14th 15th Tues. Temperature 1150
Being on night duty owing to the extreme heat during the day I cannot sleep well. The tents seem to accentuate the heat. One advantage of Ghezirah, their are no mosquitoes.
June 16th Wed.
Walked through the Ghezirah Gardens this evening. There is a feeling of peace and quietness their in the cool of the evening. Women of different nationalities come out to these gardens as the sun goes down. Most of them are nursemaids looking after the children.
The gardens are beautifully kept. The grass is beautiful and green and are continually being 


 
 watered by natives during the hot part of the day.
These gardens are situated along the banks of the River Nile. Along the banks of the River, numerous House boats are fastened. On these, people have their homes and also use them for journeying up and down the river for pleasure.
Across the river, on the other side stand the Kasr-el-Nil Barracks, a huge place, in which English Territorials are at present stationed. At present the river looks very pretty and with an occasional neat little yacth sailing along or the heavy looking flat bottom cargo boats with their tall fin shaped sails and the edge of the boat almost (to) on a level with the water and the dark coloured crew forms a very pretty picture.
June 17th Thur.
Recieved a letter which had taken 7 months to reach me from Aust -.
June 18th Fri. June 19th Sat. June 20th Sun.
Went to church with M. Rev W. Gillans last day. He goes on leave to Scotland. Rev W. Moffat from Elgin takes his place.
The services begin at 9.30 instead of 10.30 as it is expected that the weather would be much cooler. On former occasions the church was closed for three months at this season but owing to the large number of military they are endeavouring to hold services for their benefit. A large number of new patients arrived here to-day from the Darde-
June 21st Mon


 Day begins to break here about 4 o'clock in the morning. Darkness comes on at 8 o'clock in the evening. There is no twilight, darkness coming on very suddenly.
June 22 23rd Wed.
Had a walk down the "Mouski".
June 24th 25th 26th Sat
Took M. out to see the Pyramids this afternoon. This is the fifth time this trip had been intended and by some unforeseen circumstances had to be postponed. A few mishaps also happened during the trip which added spice to the outing. 
First of all we lost the five minutes to four tram and had to wait half an hour on the next one. When we were about two miles from the Pyramids this car jumped the rails and we had to get off.
I tried to bargain for a camel to take us to the Pyramids but all to no avail, so we had practically to walk all the way.
When we reached the foot of the Pyramids we managed to get a camel and a donkey and have a joy-ride round the Pyramids and the Sphinx. Arrived home at 8 o'clock. 
Soldiers being invalided home to Australia left here to embark on the "Ballarat" at Alex ? only 6 patients left in No. 6 ward.
June 29th Sun.
Went to church. New minister present. Exceedingly hot to-day.
June 28th Mon.
Left off night duty.
June 29th 30th Wed.