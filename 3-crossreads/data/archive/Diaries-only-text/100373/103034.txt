interesting news.
Have taken some photo's with D.I.'s camera.
On night duty. Very hot sleeping in tents during the day time. Not so much work however.
The Pyramids are a marvellous piece of work, but it gives one greater pleasure to observe the manner and customs of the people of this country, especially, the lower and agricultural classes.
The more I see of the natives in there everyday life the more I can understand the scenes depicted in the Bible.
One feels as they go about here that they have been placed on the "magic carpet" and transported into strange lands.
March 2nd 3rd 4th 5th 6th 7th Sunday.
Sentence passed on L. 90 days hard labour, G 6 month's hard labour D. 1 year. The whole of our Corps was paraded and the sentence read to the men, the prisoners charged with stealing the money from the patient were marched off under guard after hearing the sentence read to them.
I ([indecipherable]) have gone almost stone deaf these last few days.
A great many people are climbing the Pyramid to-day. They look like ants as they climb up the side.
March 8th Mon.
All our stores have been sent away, so we all expect to move off shortly.
Nos 4, 5, 6 wards are full up again. Most of them are suffering from 


 Rheumatics, Earaches, Influenza.
March 8th, 9th, 10th, 11th, 12th Fri.
Notice of Embarkation.
March 13th Sat. 14th Sunday, 15th Mon.
Drove out to Heliopolis in the motor ambulance. Got my ears examined. The cause of the deafness being the sand getting into the passages of the ears. Told that the trouble would probably leave me when I left the country. Undergoing treatment. Off duty. I can sympathize with anyone who suffer from deafness.
March 16th Tues.
Everything that can be done without has been sent away from here.
Those men not on duty in the hospital are out on the sand getting stretcher and wagon drill.
Have felt a great deal better of the few days rest. 
Had a stroll up to the Pyramids along with P.S. and W.A. Took a few snaps.
March 17th Wed 18th Thu.
Went into Cairo along with W.A. Visited the "Mouski" district, the bazaar etc which I found exceedingly interesting. Had tea in Groppi.
March 19th Friday 20th Sat.
A party of 4 of our unit went back to-day on a hospital transport to Aust - . 
Visited the Cairo Museum and had a general look around.
March 21st Sat.
The warship "Ocean" which we passed on our way at Port Suez has been sunk at the Dardanelles.
There are still a great many rumours as to our going away. One gets sick hearing them.
Saw a native funeral pass our camp