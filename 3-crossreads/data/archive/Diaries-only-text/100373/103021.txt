I notice that amongst those who had never seen a foreign shore before this time, talk very loundly on how they dealt with the native when he bought anything. Probably the native got the best of the bargain after all is said and done.
There are a very large number of jack-daws, both in the streets of Colombo and also in the harbour.
Dec 29th.
Had a few hours ashore this forenoon. Warned that all parties were to be aboard at 2 o'clock.
I had a very pleasant walk through the streets again. Brt. a few precious stones (?) moonstones are very lucky, so the native said from whom I brt. them. Brt. a copy of English Songs and Ballads from one of the biggest booksellers shops here. Strange to say a portrait of "Robbie Burns" graced the frontispiece. A nurse was taken aboard the ship on a stretcher suffering from sunstroke.
Going over to Colombo in a small boat was a most hair-raising experience. There was a fair breeze blowing and coupled with the unskilfulness of the boatsmen I really thought every minute we were going to be upset.
We had a slightly better experience on our return journey but I never felt safe until I set foot on board the ship again.


 This instance of the natives begging nature will help to illustrate the dodges they get up to. I happened to wash a few clothes and having dried them in the sun was taking them down when one of a number of natives who were loading bags of potatoes on to the ship came over to me and asked me for one of the articles. I refused him, however. Two hours later when I had almost forgotten the above incident he came over to me again. He chatted away in a humerous fashion in broken English for some time, then made reference to the clothes again. He said "that thing would do him". I retorted with "that thing would also do me". "Ah! he says "You go to London, you get plenty clothes". Instances of this kind are always cropping up.
(Travelled 267 miles). We left Colombo Harbour at 8 pm. Two searchlights kept sweeping the seas as we sailed out. At times they were fixed on our ship and lighted the deck up beautifully.
Dec 30th. Phy Ex Kit Inspection.
Dec 31st M.O.
Passed the Laccadives Islands. According to the atlas they are a group of fourteen islands and have a population of 14,000. We were not very near them however, and to us they appeared to be only a mere outline except at the point of one there appeared to be a building which we took to be a lighthouse.
This being Hogmarney I was reflecting to-night where I was a year ago.


 I remember along with J. B taking a walk up to the Steeple at home about midnight. To-night J is in Portland, B is in France and I am on the High Seas. 
Dec. In spite of the fact that we were on a crowded ship and far out to sea, the "boys" determined to pass it in the old, old way. Some of them got basins, pails, etc. from, they only, know where and marching up and down banging at the things causing a terrible row and endeavouring to sing some of the popular songs above the noise. The noise they made just about excelled the noise of the native band in the streets of Colombo. It was pandermonium itself for an hour or two. 
The officers and sisters had a fancy dress ball to-night amongst them-selves.
Jan 1st.
"A Happy New Year" was the first words of the boys on awakening this morning.
A "Daily Paper" is being published by some humorous members of the units. The Humorous and Sarcastic news, published in the paper, are splendid. It is a type-written and posted up for all to read.
There was a scarcity of water in the men's Shower Baths. The complaint was report to O.O. and remedied.
A paragraph to this effect was put in the paper as follows
Meteorological Survey:- showery this morning in the