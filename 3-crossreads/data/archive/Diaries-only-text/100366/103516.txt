poor chaps on the other side of it they were twice as far from it as me. It dug a hole about 30 feet deep in the road turned carts horses upside down in all directions. After the bombardment ceased while I was being taken to the field dressing station I could not help but notice that there were dozens of poor wounded chaps all round. The total casualties in our unit alone was four killed 16 wounded. Of the sights of the dead I will not speak as they presented too horrid a sight, But the way the shell hurled men about in the air was terrifying to look upon. 
All the time their Aeroplanes rained bombs down which however did no damage they were ultimately driven of lost 3 machines.
After passing through the dressing station I was put into an ambulance, together with others brought to Grove Town Clearing Station situated near Bray, It is a British Hospital I appear to be the only Australian here. There is a Manchester man on one side of me a Seaforth Highlander on the other between them they keep me amused.
24.11.16 Friday
Still in Grove town much better. My wound is not deep is healing rapidly soon I will be out again. Many pitiful cases are coming in I wont


 stay here any longer than I can help. It is a funny routine one goes through here consists of opening your mouth for the thermometer keeping your bed clothes straight tidy.
Altogether I am not feeling well but am not too bad. They have shaved the hair away from the wound I am half bald.
25.11.16 Saturday
Grove Town Hospital
26.11.16 Sunday 
Was moved today from Grove Town Hospital taken on small trucks to the Hospital Train on the main Line from where I understand we are being shifted to Rouen Hospital.
27.11.16 Monday
After spending all night in the train we arrived at Rouen at 9am were carried out to the waiting ambulance taken to No 5 General Hospital. I am under observation as my temperature keeps high I am troubled with headaches.
27.11.16 to 10.12.16
In Rouen Hospital marked for England. Left by ambulance at 10.30 pm was 


 put aboard a train for Le Havre. Spent all night in the train was unloaded on the following morning.
11.12.16
On board the Hospital ship. "Wirilda" which is an Australian Hospital Ship. Crossed the channel, sea smooth, tied up outside Southampton all night went into Harbour next morning.
12.12.16
Unloaded put aboard British Hospital Train destination unknown but thought to be close to London. 7.30 Train pulled up in Oxford was unloaded brought to Cowley Section. No. 3 Southern General Hospl where I suppose I will be for some time
12.12.16
Headaches rather bad x rayed.
14.12.16
Have been told that nothing can be done for my complaint will have to grin bear it what the outcome of it will be I cannot say.
The knock has given me meningitis but I intend to fight it hard. A strange thing is that I was always frightened of the


 Days
 4 France-	Hospital -	23/11/16 to 26/11/16 -Grove Town
 14 - 		Hospital -	27/11/16 to 10/12/16 -Rouen No.5
51 -  		Hospital-	11/12/16 to 31/1/17-Oxford No.3 Cowley Sect. S.G.H.
164-	England	Hospital-	2/2/17 t0 14/1/17 -Dartford No 3. A.A.H
 61 - 	Hospital-	15/7/17 to 14/7/17 - A.H.ship Kanowa
 39 - 		Hospital-	 15/9/17 to 24/10/17 - Randwick
Total 333 days


 Saunders
 Bond
 Stubs
 Chesney
 Allen
 Wood
 Reynolds
 [indecipherable]
 
 Nutman
 Cpl Sowby
 [indecipherable]
 
 [indecipherable]
 
 Brown
 Savage
 Doherty
 Cliff
 Mills
 Grace
 Devereaux
[Transcribed by Rex Minter, Val Ridley, Adrian Bicknell for the State Library of New South Wales]