Tues. Mar. 27. Yesterday wrote Lill today to Mother Arthur David. Wet, windy day. 
Wed. 28. Hospital ship "Asturias" sunk by the Huns. Another wet day.
Thur. 29. Still got the Huns on the move.
Fri. Mar. 30th Went into Albert with Billy Williams had a good look round the Town. Quite a number of civilians have returned the place is looking quite civilised. Got Letter from Ruth B.
Sat. Mar. 31 Some H.Q. Staff has moved into Albert the Town is full of Red Caps. One has to be careful about saluting these days.


 Sun. April 1st Last night we had quite a full house. 3 of our Sergeants 4 of the 15th camped in our Hut kept the place lively till all hours. 
 This morning they went away to an officer's School. After Tea George Hand I went into Albert again had a "petit [beeras?]" . It is most amusing. For  Franc we get a Pork Beans tin of Beer for un franc we get a Butter Tin Full. The Town is full of Tommies. It's rotten not getting any mail. We are beginning to wonder what's gone wrong. I commenced a Letter to France, but I'm hanged if I know what to put in it.
Monday 2. Cold bleak wind. About midday is snowed very heavily. Last night a Fritz plane came over dropped a bomb in the Town. Evidently they are after H.Q. but they missed badly did no damage whatever.
 Tonight we got some new Music from Blighty, so now we can go ahead with a new Programme.
Poor old M.S. I wonder if she is getting my Letters alright, I hope so.


 Tues. MarApril 3. The Y.M.C.A. Hut manager sent in today to know if we would put on our Show on Thursday night, so we are busy now arranging a Programme.
Wed. 4. Good news in the Paper at last. America coming in the War. So now we are hoping that Fritz will throw up the sponge let us get back to Sunny New South.  Hooray. Mail came in. Got 3 Letters from M.S. Now I can write a decent Letter in return. The Prince of Wales attended the Anzac Show in Albert. Bob Roberts offered to put us in the seat among the Heads, but we have a rehearsal on, I can't afford the time.
Thurs. MarApril 5. Commenced Letter to M.S. Glorious day, sun shining gloriously. Plenty of planes about. George Hand went into Amiens, so I was left to run the Show.
 Johnnie Moore also went to Amiens failed to return. Horrie Playford Wally Saunders did not like to go on without him, so Freddy Saunders had the Piano to himself. He did very well too. Everything went off very well the crowd was most appreciative.


 Good Friday Apr. 6th Our Bearers came back from the Trenches today where they have been following up old Fritz. We are putting on a Concert tomorrow night we had a rehearsal tonight, so as to give ?em some of our best.
Saturday 9. Yesterday the Frenchmen on the Farm killed a big pig, to give the Bearers some pork; but when our cook inspected it, he discovered it to be an old sow about 90 years of age; too tough to chew; so Pork was off. Johnnie Moore went into Amiens purchased a big supply of good things. The Banquet was gorgeous our Concert was thoroughly enjoyed by the Bearers.
 Easter Sunday 8/4/17. Lovely day. After Dinner Dodson I went into  Baupaume. We got a lift in a motor Lorry. It was most interesting the whole way out. Fritz left hundreds of tons of Barbed wire behind a huge stack of coal. On the roadside we saw a whole train, about 20 or 30 trucks, which our guns had blown off the Line. We also counted 7 tanks which Fritz had "knocked out". The country is just like a Honeycomb, shell holes joining up to one another. We saw MacCallum in Bapaume. He is acting as detail to the 31st Batt. I asked one of the traffic police where the Town Hall was. He looked a bit surprised pointing to a vacant piece of ground said "That's where it was." The Huns certainly made a Job of blowing it up. 


 Easter Sunday The cemetery is a very interesting place. Fritz went to some trouble in planting flowers making the graves look nice. They also had some very nice stones to some of the Graves; especially the officers. They had one of our airmen buried with some of their men his name was painted on the stone in English. There is a very fine statue that Fritz erected in memory of their men who fell in the taking holding of Bapaume. After a stroll through the Town we got aboard a Motor Lorry returned to Bellevue Farm.
Mon. Apr. 9. [no entry]
 Tues. Apr. 10. [no entry]