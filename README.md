Crossreads 
=======

This is a prototype used to explore non-linear reading, or  derive-reading, or cross-reading. This is about to read a text in  alternatives orders. 

Description
-----------

Initially, a text or a collection of texts is divided in small pieces (paragraphs),
then we calculate the similarity among pieces.
And finally we define paths through the pieces

This repo contents:
- All used data conversion scripts [under data/dataConverters] 
- And a web interface [100% client-side javascript] that encourage mixed types of exploration, ranging from timeline, to flat list with filters, and crossreading. 

Crossreading enables non-linear navigation by jumping from one piece to another according the pieces similarity, and, firstly, according to reader decisions..

Flavors
-------

* 1-crossreads/ -> this is the first prototype of crossreads. Developed thanks to Sònia López and MACBA [Museum of contemporary Art of Barcelona] for the exhibition "THE LISTENING EYE. EUGENI BONET: SCREENS, PROJECTIONS, WRITINGS". It can be visited online here: http://www.macba.cat/eugenibonet/
NOTE: the collection of writings from Eugeni Bonet are presented in Catalan and Spanish

* 2-crossreads/ -> this is a second prototype. This time the collection of texts will be in English. Similarity and interaction will be improved. Shortly  more news about this [June 26th, 2014].



[last update: June 26th, 2014]: Active development of 2-crossreads.



