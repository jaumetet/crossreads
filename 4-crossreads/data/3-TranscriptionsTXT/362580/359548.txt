 [Page 57] 
 
 Am awfully pleased that Vi. has married &amp; to a man her brother approves.  He will know if he is good enough for her.  So glad you are having such a number of visitors.  It makes it very cheerful for dear old mum. 
 Just a word in private heard that Fred is in England &amp; going back to Aust.  Being discharged.  Hope it is nothing serious.  The military is a funny 