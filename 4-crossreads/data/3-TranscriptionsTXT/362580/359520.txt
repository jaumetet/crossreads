 [Page 29] 
 The mittens you sent I gave to Norman last night.  Poor old Keith may be on his way to Australia now.  Hope he is much better.  I received a letter from his mother &amp; Jean to-day &ndash; poor woman, she will be upset when she sees her dear boy without a limb. 
 Vi must have been very ill too &ndash; I had a letter from Mrs &amp; Mr Osburne yesterday &amp; Vi was staying with them.  I cannot find out definitely where poor old Fred is, so unable to write or send anything to him. 
 Little Eddy's letter is lovely &amp; the pansy arrived quite safely.  He is a busy boy, having a garden of his own.  Hope he had a joyful time for his birthday. 
 I did not buy a Panama but got a little  Grey Felt  &amp; everyone but Dorothy says it suits me &amp; like it very much. 
 I did receive the card the  family  sent so I  trust  the family receive &amp; like my photo I sent with Sister Hart (of Queensland) 