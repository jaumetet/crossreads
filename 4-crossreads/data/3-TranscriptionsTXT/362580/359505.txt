 [Page 14] 
 Xmas letters next.  The weather is simply beautiful now &amp; things are much happier here too. 
 Miss Eyres wrote me such a nice letter too, she is a dear and it was very good of her to go out to see you.  She is such a busy woman. 
 Awfully sorry to hear the Thomas family are so ill.  Must try &amp; scribble a few lines to-night to Annie. 
 Receive Crissie Stirlings letter every week safely &amp; the boys do enjoy it.  Must be silly I think &ndash; should have written last week to you Belle to wish you Many Happy Returns of the Day.  Hope you will have a very, very, happy day &amp; a prosperous year.  Trust you are all well &amp; happy.  Mind to take the money for your birthday present &amp; wish I was there with you.  Fondest love &amp; kisses to you all. 
 From Your loving sister Edith. 