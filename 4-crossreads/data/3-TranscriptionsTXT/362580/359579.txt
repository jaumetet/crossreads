 [Page 88] 
 ago, so have not much news to tell this time.  No mail in yet.  Yesterday I slept from 8.15 a.m. till 3.45 p.m.  Had a cup of tea &amp; cake &ndash; then slept again until 5.30 p.m. 
 To day I did not get up until 5 p.m. had a bonser sleep so feel splendid. 
 Fred &amp; one of his Officers took two of us to dinner to-night to The Belgian Restaurant.  I had a late night, out till 9.30 p.m. so we had a great time, thoroughly enjoyed ourselves. 
 Shall be sending you some more of my own  snaps , soon, they are not printed yet, but the negative is awfully 