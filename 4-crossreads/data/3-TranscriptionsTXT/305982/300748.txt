 [Page 82] 
 that hard to keep warm.  Billious attack today 
 30/11/16 Cold again  Saw Ned [indecipherable] 1/12/16 Cold again always cold  Bull Ring to day did nothing  Ice on ponds 
 2/12/16 Hellish cold again today mist about whitish colour awful hard to keep warm  news about Rumania getting knocked about etc  a sorry plight by the look of things 
 3/12/16 Sunday to day  nothing of any interest.  Saw Abel Sheath today 
 4/12/16 Nothing doing  rather cold 
 5/12/16 Quit a number of letters today from Aust.  Post card from (Xmas) Mrs Sheath [indecipherable] Vera Compton 
 6/12/16 Cold still as per usual  Rumania looks rather glumb 
 7/12/16 Rumania dished up properly  raining now. 
 8/12/16 Another draft off to day but none for 13th Batt so will have to stop here  a while longer  raining all day  Saw Abel Sheath again to day 
 9/12/16 Nothing doing other than to read English papers about the Pleb 
