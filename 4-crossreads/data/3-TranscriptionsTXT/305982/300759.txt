 [Page 93] 
 Fine airmanship.  still cold 
 29/1/17 Fritz shelling again last night very close to us.  our first casualtys  usual sights to day aeroplanes etc, a few comforts came up today 
 30/1/17 Snowing again  today  this morning though not much.  Fritz up again in planes he is getting into his old stride again what is doing goodness knows  a beautiful afternoon but cold of course a little warmth in same if sitting in a dugout  Fritz on to us &amp; [indecipherable] afternoon  Silver sunset 
 31/1/17 Beautiful sunrise about 6.30  red glow ball.  Fritz up again doing good work etc.  He gave us hell in supports today.  Big stuff he was throwing about &amp; had a close shave in our trenches.  Mail up  Big one I received  a few old letters  Great news about a stunt coming off at -----  on the night of the 1/2/17.  Sincerely hope it will fix matters up about the war of course that is if it is true. 
