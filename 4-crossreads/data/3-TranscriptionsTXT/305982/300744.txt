 [Page 78] 
 other than Peggy 
 8/11/16 Raining as per usual same old parades 
 9/11/16 Glorious day sun shining all day went for a fine march  big falling out when returning this afternoon. 
 10/11/16 Usual parades warned to go away or rather to be ready to move tonight.  but was cancelled this afternoon  pay today  Boys having a good beano re bar etc. 
 11/11/16 Warned again to day to be ready to move off tonight 10.30 whether we will or not is another thing  I went to Weymouth last night in car, missed train 10.10 one &amp; walked home a matter of 15 &frac12;  miles arrived back in camp at 2 oclock this morning  beautiful walk new boots overcoat etc  Letter from [indecipherable] 12/11/16 We move off this time for France intrained at 1 A.M. this morning  lot of boys drunk but not to bad  laughable  We arrived at Folkestone at 7.30 this morning after a long trip in train  good reception from people enroute school kiddies.  Breakfast in rest camp a good meal.  Beautiful seaside resort Folkestone is glorious so  natural quiet  a long march from railway station (Shorncliffe)  we are [indecipherable] we sailed at 12 oclock 
