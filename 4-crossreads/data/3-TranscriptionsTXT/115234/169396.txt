 a6490177.html 
 on night of 24 th  Feb. was one of raiding party.&nbsp; Went across to sector Fritz trench. had lively bomb fight.&nbsp; Three nights of Hell let loose.&nbsp; lost gum boots in mud.&nbsp; Came out &amp; went back to Townsville Camp.&nbsp; From there went up to  Flers  laid for three days &amp; nights in Bayonet trench. near Turks lane.&nbsp; On the morning of 17th March. 
