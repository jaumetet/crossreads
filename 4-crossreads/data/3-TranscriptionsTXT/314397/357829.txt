 [Page 33] 
 Saturday. 5.10.18. Am up before a board this morning. Waited all this morning &amp; still have to wait a bit longer this afternoon. Gee it was icy cold this morning. Went back after lunch &amp; went before the board. Got 3 weeks sick leave in Blighty from it. Board consisted of Colonel &amp; 2 captains. 
 Sunday 6.10.18. Went to Paris Plage in the afternoon &amp; caught two Waacs. Had lunch, tea &amp; dinner at the club. 
 Monday 7.10.18 Went before the D.D.M.S this morning &amp; he confirmed our leave. Leave Etaples tomorrow &amp; go to Blighty the same day. Went to Paris Plage with the Waac &amp; wandered around. Collected warrant for Blighty at 9pm 
 Tuesday 8.10.18. Left Etaples at 3.43am. Gee it was icy cold &amp; dark. Arrived at Boulougne about 5am &amp; went to the club. Had a wash &amp; brush up &amp; then some brekker. After brekker went to the 
