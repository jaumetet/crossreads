 [Page 19] 
 the beach &amp; had a bonny swim. Do me every day. After dinner (very poor too) had a few kicks of a football &amp; then went to some pictures here. They were excellent too. 
 Sunday. 25.8.18. Brekker at 9.30. Went for a swim in the morning. Went for a stroll after tea. After dinner played football &amp; did a bit of a sing song. It started to rain about 9pm rained extra heavy. 
 Monday 26.8.18. Rained to some order last night. Started our routine this morning. Standing orders &amp; fitting of equipment etc. Had squad drill (turning by numbers etc) Physical training &amp; a lecture on Leadership. Went to some pictures after dinner &amp; they were very good. 
 Tuesday 27.8.18 Had more squad drill, physical training, musketry, map reading &amp; a lecture by the doctor on Sanitation. Went for stroll after 
