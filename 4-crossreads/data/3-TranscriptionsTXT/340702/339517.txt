 [Page 19] 
 30.10.17 Received a letter from Uncle Jack &amp; answered it. 
 31.10. Fritz Bombed like Blue Blazes last night. It was Bright Moonlight in the early hours, &amp; one could hear the humming of his propellers all the time, also the "Crump"Crump" of the Bombs all the time. 
 4.11.17 Sunday. Changed over the Shop wiring yesterday O.K. New generator (Salvage) installed Now awaiting prime mover &amp; capacity test. 
 6.11.17 Tuesday. Went to Poperinghe yesterday &amp; was overhauled by the M.P.s. They thought they had a sure catch. The New Dynamo came a "gutser". It is now adrift. We are going to cut out the Interpole field &amp; connect the shunt field coils in parallel &amp; then run her as a plain Shunt Dynamo. The field coils are now getting an extra bake. Met a Tramway man today. I believe Jim Wilks was his name. He has been away 3 years. Came to France in January 1917. He told me Price is Q.M. of the 8th field ambulance. 
 7.11.17 Wednesday. Letter from Clare. Very definite rumor that we move from here to Armentierres front very soon (10 days). Drank full rum issue tonight &ndash; usual result - Dopey as a fowl. 
 8.11 Just seen one of our planes bring a Fritz down. 
 9.11. Friday The "Baron" (Schott) has officially vanished &amp; the official mind is much exercised over him. He has 
