 [Page 609] 
 other than march, did not even break step, but "carried on" as if nothing had happened but after going three or four paces he turned and laughed very heartily at the "Good old Mother" who having recovered her "treasure" was again in the best of good humour &amp; shook her clenched hand, at the laughing culprit, threateningly. 
 When last seen the whole family were waving very enthusiastically &amp; held up the dog to be seen. 
 It was prophesied that when the next lot of "Aussies" march out of Monte Video Camp, that dog will be locked up-inside the house. 
 The R/Station seemed to be teeming with Red Cross &amp; Comforts Funds workers, with every thing in the shape of Eatables &ndash; fruit confectionery biscuits cakes etc also tea Cigarettes &amp; tobacco. 
 A happy lot of men left Weymouth R/Station that day, the band formed up &amp; played all the well known battalion marches &amp; the most appropriate tunes, which appealed to all in such a way that many plainly showed how much they were affected by the demonstration. 
 All along that Railroad journey wherever &amp; whenever possible people flocked to the Stations offering sweets Cigarettes etc &amp; where not possible they stood &amp; cheered &amp; waved handkerchiefs &amp; hats as the train passed on. 
 Being Sunday probably more people were about than customary &amp; owing to the lack of "transports" the Hospital trains were fewer than "troopers", &amp; especially of "Aussies" going home. 
 On arrival at Plymouth it did not take long to embark &amp; the Hospital Ship ("Ayrshire") moved out into the stream. Shortly afterwards the "Durham Castle" moved away too &amp; anchored. 
 Both vessels were to sail in "Company" but as the latter vessel was fully loaded with "Tommies" the men aboard the "Ayrshire" were for a while at a loss to know why! 
