 [Page 555] 
 these would flatten down passages in the wire, by means of which the men would dash through &amp; attack. 
 The whole of the men reached the wire as planned without detection &amp; were thus quietly lying within a few yards of the Hun trench patiently waiting for the time to elapse for the Tanks to appear &amp; prepare the openings through which the men would "surprise" the unsuspecting Huns. 
 This wait was very trying, and as the time for the expected arrival of the Tanks approached the nervous strain was very great. Anxious eyes were eagerly watching for the very first sign of their approach. The time crept slowly on, very much too slowly for the waiting men, anxiety become intense but still no tanks, oh well it can't be time yet, but heads kept turning, with the minds wondering at the delay 
 Suddenly the very faintest signs of dawning day dissipated all possible chance of a surprise attack Not only would they now be heard but the Tanks must be "seen" by the enemy in daylight. "Curse the tanks" 
 With the breaking of day the pent up eager anxious feelings were turned to dispair. What could save them in their present position now? Nothing! Tanks or no Tanks a brutal time was in store for them! 
 Broad daylight at last, &amp; then an order was passed along, "To Return". What a discomfiture! What a botched blunder! What a climax! The startled Huns had little suspected that such a body of men had been within earshot of their safe retreat half the night. 
 The Staggering Shock of the awful danger they so barely escaped, at first unsteadied their aim, but recovering before the attackers could succeed in securing cover, their Machine guns &amp; Rifles took a terrible toll. 
 Something had gone wrong What! As usual No one can tell! The Tanks 
