 [Page 605] 
 may never present itself to the Sentry 
 After being made quite satisfied that there was no friend or even an acquaintance the tickets could be forwarded to, the Old Lady pleasantly remarked that she was certainly about to enjoy the very best time she ever had in Theatre going. 
 At the same time the Sentry requested his Landlady not to forget to inform the Lady in Charge of the arrangement for the Special Matinee, that the Sentry would have kept his promise to attend &amp; to remain in his place in the Theatre for the first intermission but for the fact that he had been summoned by telegram to return to Camp immediately, in order to return to Australia &amp; obtain his discharge 
 Had he ignored the order till his furlough had finished, strong as the temptation to do so might have been, it would have meant getting into unnecessary trouble &amp; probably having to wait a long time practically confined to the precincts of a Camp, in which it was really no pleasure to stay, till another "transport" had been placed on the run to Australia. 
 After spending one day travelling about the good old city as a valediction the Sentry boarded the train to take him back to Weymouth. 
 On his arrival there he soon had his own ideas confirmed by hearing his name read out, when the men were mustered, as being one of those on the "Boat Roll". 
 It was understood to mean that all on the "Boat Roll" would almost immediately leave the Camp for the port of embarkation. 
 Early the next morning the fortunate ones were marched to the Railway Station &amp; boarded a train to carry them to Plymouth where the Hospital Ship was lying. 
 The local residents, evidently roused out of bed earlier than usual by the gaily sounding tunes played by the Camp Band in the lead, gave 
