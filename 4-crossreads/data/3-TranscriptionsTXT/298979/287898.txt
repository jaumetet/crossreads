 [Page 511] 
 He shouted out What oh! You [dash]s I'm bound for "Blighty"!! (this was the man of whisky fame) 
 The replies he received were. You lucky [dash]!, You always were lucky! 
 Further examination showed that the sentry although with the other two only three feet from the shell burst, escaped without a single scratch, but the next three men were suffering so badly from shell shock that they would have to "go out" at once 
 Therefore it was arranged that the shell shocked men should do their best to assist the wounded man &amp; they started &ndash; eagerly started &ndash; to report to Company Headquarters at the back of the wood, &ndash; it was simply part of a wall without even an NCO in charge let alone an officer &ndash; &amp; then continue on a long walk to the aid post. 
 They eventually reached their destination without further mishap but the shell-shocked men were sent on to the base, the Doctor would not allow them to go back again The loss of these five men gave a little more room but did not lessen the misery. 
 At day light although hard to believe every man in the trench was satisfied that the man lying across the end of the trench was dead. 
 With the dawn matters did not mend in fact if anything they became worse, so bad that the B Coy Sergt Major informed A Coy Sergt at six oclock that the Sergt &amp; his men would have to hold the post alone as he the Sergt Major, &amp;  his men were going out. 
 The Sergt wanted to know by whose orders the Sergt Major was taking out his 20 men &amp; handing the position over to the Sergt, especially when not one man of his 20 had been touched whereas the Sergts men had been reduced to 10. 
 The Sergt Major simply said he was senior NCO &amp; the Sergt had to take instructions from him. 
 As they passed out the remarks of the Sergt &amp; his men were too 
