 a6692049.html 
 
 
 
  1917  
 45 
 
 
 Aug 28 
 Wet and very windy.&nbsp; At signalling all day.&nbsp; News of Italy&#39;s smashing defeat on the Isonzo Front to hand.&nbsp; Good batch of mail from home. 
 
 
 Aug 29 
 Left billet for Aire 9.30.&nbsp; For heavy training on canal.&nbsp; Wet, on and off, all day.&nbsp; On picquet on arrival from 1.30 to 4.30.&nbsp; 11.30 to 2.30.&nbsp; Advised by Lt Clarke that John cannot be transferred on account of his being attached to an Infantry regiment or unit.&nbsp; Spent hour in C. A. hut reading and smoking.&nbsp; Returned to quarters - our billet being a huge barge - for rest ready to go on picquet at 11.30 pm. 
 
 
 Aug 30 
 Came off picquet early morning and awakened feeling very tired.&nbsp; Still dull and nasty.&nbsp; Day off for picquet relief.&nbsp; Rode to Arcques after tea to see John and tell him about transfer.&nbsp; Saw him and handed him home mail.&nbsp; Not upset about transfer going awry.&nbsp; Returned to barge at 7.30 and wrote rest of evening. 
 
 
 Aug 31 
 Heavy bridging all day.&nbsp; Went to Sercus on dental in afternoon.&nbsp; Did not get attended to even after the muddy ride across.&nbsp; Saw Lt Dumas.Yet again all day.&nbsp; Had hot bath in town after tea.&nbsp; Also haircut. 
 
 
 Sept 1 
 Dental in morning to Tommy Hospital across the canal.&nbsp; This was, in pre-war days a French prison and rumor has it that Dreyfus was imprisoned there before being sent to Devil Island.&nbsp; It certainly looks like it as the rooms are all cell-like with heavily-barred windows.&nbsp; Got nerve-killing dressing in my tooth.&nbsp; Spent rest of cold and damp day on bridging. 
 
 
 Sep 2 
 Still at heavy bridging.&nbsp; Got pay &pound;1/9/4.&nbsp; Did washing on deck after dinner.&nbsp; Jack came over from Arcques.&nbsp; Went up town for stroll and met Len Anderson.&nbsp; All 3 went round town and returned to barge. 
 
 
 Sep 3 
 Bridging all day again.&nbsp; Morning making ready.&nbsp; After-noon getting gear in place.&nbsp; Nice fine day.&nbsp; Quite a change to the weather of late. 
 
 
 Sept 4 
 Usual thing.&nbsp; After tea went up town and met Billy Caldwell.&nbsp; Spent rest of evening on the craft. 
 
 
 Sept 5 
 Bridging again.&nbsp; 2.30.&nbsp; Fell in and marched back to Sercus again to our old billet glad to think we had done with this heavy work.&nbsp; Hot march and tiring. 
 
 
 Sept 6 
 Signalling all morning.&nbsp; After dinner, Gas drill and gas lecture by Lt Clarke.&nbsp; Heavy thunderstorm after tea.&nbsp; Fall of Riga reported from Russian front. 
 
 
 Sept 7 
 Signalling.&nbsp; Afternoon.&nbsp; Bath parade and did washing. 
 
 
 Sept 8 
 Signalling morning.&nbsp; Got boots mended in afternoon. 
 
 
 
 &nbsp; 