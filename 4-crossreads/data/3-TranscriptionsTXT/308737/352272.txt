 [Page 16] 
 Mrs Butler "Orvieto" 277 Birrell St Bondi New South Wales 
 30/10/14 Darling Mum, Nance &amp; John 
 I don't know whether you got my last letter or not. Cant tell anything about ourselves. Strict censorship being maintained on our correspondence. Only allowed to write on p.c. Not allowed to tell you the name of this boat, which you already know. Is'nt it absurd. Will have to explain all sorts of things when they let us write freely again. We are getting on first rate here. Remember me to Mrs Moody of the Middleton Beach Hotel Well we are not even allowed to tell you where we are at present, but I'm sure you'll guess that yourself. Anyhow Bless you all &amp; best wishes 
 Your boy John 
