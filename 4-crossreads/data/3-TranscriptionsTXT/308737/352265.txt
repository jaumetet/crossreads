 [Page 9] 
 we'll be leaving here to-morrow week, but I think its impossible because the men ar'nt good enough &amp; have a lot more to learn &amp; there are no uniforms in sight not half the horses &amp; we have got to spend days on the rifle range teaching men to shoot. 
 This picture of the camp is only a tail end of it We are'nt in the bell tents like this but in ones like this [good small sketch of rectangular tent] Eight of us. 
 I'll give you a plan of how we sleep. 
 [Sketch of floor plan with eight names &ndash; four on each side of the tent] Leslie Stark   Billie Robinson Willott   Clarence McDougall Sunny Robinson   John [indecipherable] Butler Gordon Robertson   Archie Campbell 
 The Tents are all only 8 by 10 feet so you can imagine the tight fit. The place is full of dust the whole camp is one dust hole. But we dont mind &ndash; we are quite happy. We sleep on the hard ground with a waterproof sheet &amp; a blanket only so we are hardened 
