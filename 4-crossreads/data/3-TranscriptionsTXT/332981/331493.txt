 [Page 29] 
 all of them wore white armlets. After they had been given the necessary instructions the party returned to the German Lines. 
 An hour had been fixed for the arrival of Herr Ergberger and the two German generals and a French General and a number of other superior officers were on the spot to meet them. For hours they waited, but there was no sign of the Germans. 
 Bonfires and flares were lit to guide the expected visitors, and eventually their approach was heralded by the appearance if the German road menders sent to fill in the mine &amp; shell holes. Finally, about nine o'clock the headlights of three big open touring cars signalled the approach of the Delegates. 
 They alighted from their cars and, in the words of a French Officer, "They were not proud". 
 Forest Chateau. 
 Accompanied by French Officers and guides, the Germans were conveyed in French closed cars, with drawn blinds to the Railhead. Here a special train was 