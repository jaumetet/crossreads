 [Page 41] 
 19 April 1915 Monday 
 The fourth Reinfs arrive   &amp;  are sent into camp at a place by the name of Zeitoun We are set to work cleaning up the    the  whole camp at Heliopolis on account of an order that we may have to go on to Zeitoun but later the order is cancelled. 
 1915 April 20 Tuesday 
 Finish cleaning   &amp;  burning   &amp;  have our first proper parade with our new officers 
