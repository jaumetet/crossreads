 [Page 28] 
 24 March 1915 Wednesday 
 The cold wakes us up at 5 in the morning all worn out   &amp;  stiff from sleeping in our boots   &amp;  puttees. Find that no arrangement have been made for feeding us. So make a breakfast on Tinned Beef   &amp;  Biscuits brought from the Ship. More biscuit for tea, no dinner. Get some tents   &amp;  settle down to make the best of it 
 1915 March 25 Thursday 
 Set our own cooks to work and draw rations for the day, so all joyful again, now we have had a good feed. Do our first drill this morning in the sands of the desert. Terrible tiring on the feet   &amp; legs   &amp; damn hard to get the men to keep step 
