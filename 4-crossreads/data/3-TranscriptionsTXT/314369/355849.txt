 [Page 75] 
 26 June 1915 Saturday 
 Sapping 7.30 am to 12 o'c The Turks shell several of our trenches on Walkers ridge   &amp;  use machine guns killing about twelve   &amp;  wounding upwards of 40 Light Horsemen (including their Colonel killed) Letters from Home. 
 1915 June 27 Sunday 4th After Trinity 
 Sapping again from 7 am The job is now practically finished being in all 7 ft deep 5 ft wide to allow the pack mules through   &amp;  about three miles in length The Turks gave us another little surprise this afternoon by infilading Anzac Cove with a new 4.7 in. Howitzer from the right of the position. It tore up the beach   &amp;  killed about 9   &amp;  wounding quite a crowd. 
