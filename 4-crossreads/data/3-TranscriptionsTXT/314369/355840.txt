 [Page 66] 
 8 June 1915 Tuesday 
 Start out at 7 am this morning as we have further to go on the same job. One of A co's men wounded not far from us. Get good news of one of our submarines having sunk several warships &amp; transports belonging to Turkey. Write letters home. 
 1915 June 9 Wednesday 
 Inlying piquet again Have a turn out   &amp; inspection at 9 am. 
