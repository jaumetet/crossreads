 [Page 109] 
 2 September 1915 Thursday 
 Get a bit of a surprise when in walked dad. He was overjoyed to see me &amp;  could not do enough for me. He brought a bag of grapes  &amp;  figs  &amp;  gave me another watch mine having gone a bit cronk. He had a bad time physically but is making up for it in other ways. He is drawing Gun. Sgts pay (Viz: 12/- per day)   &amp;  doing nothing for it. {Another victory for me I stuck on the peninsular longer than any of the other Lindsell's} 
 1915 September 3 Friday 
 About a dozen sisters   &amp;  large number of Patients leave here for Australia this morning Am not feeling too good must have caught a cold or something foot getting on well. Dad comes in about 6 pm   &amp;  brings a tin of Pineapple which goes down very well. Sleep a little better than for several nights past. 
