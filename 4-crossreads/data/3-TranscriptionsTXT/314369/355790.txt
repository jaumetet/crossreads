 [Page 16] 
 28 February 1915 Sunday 2nd in Lent 
 Work on spuds suspended for the day Church parade at 10 am   &amp;  8 pm. Nobody attends. 
 1915 March 1 Monday 
 The Spud Job commences again on new line. We rig up a set of pulleys   &amp;  set to w/- the steam winch result &ndash; about 10 times as quick   &amp;  hardly any labor. Weather continues fine   &amp;  cool. The old man is made Armourer Sergeant 
