 [Page 21] 
 10 March 1915    Wednesday  it. A boat load of men more or less drunk would come aboard about every 10 minutes. 
  Wednesday  
 Take aboard a quantity of provisions   &amp;  also change the crew taking a black crowd instead of the Chinamen. Troopship "Nile" w/- a regiment of Punjabis ties up near us amid much cheering   &amp;  playing of bagpipes which the native troops play very well Clear out of port at dusk. 
 1915 March 11 Thursday 
 Take up the usual routine again. Time is beginning to hang heavy the novelty has worn off   &amp;  we want to be at it again. 
