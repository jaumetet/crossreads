 [Page 10] 
 Transcribers note: Written on Letterhead of Australian Imperial Force, Base Records Office, Victoria Barracks, Melbourne 
 27th August 1918 
 Dear Madam, 
 The enclosed certificate relating to your husband, the late No. 3770 Private T. Bell, 20th Battalion, was forwarded to this office for transmission to you, 
 A complete record of the period of incapacitation of the late soldier was forwarded on 19th inst. To the Grand Secretary I.U.I.O.O.F., 185 Elizabeth St, Sydney 
 Yours faithfully J.M. Lean Major. Officer i/c Base Records. 
 Mrs. B. Bell 15 Edwards St Merewether Glebe N.S.W. 
 Enclosure 
