 [Page 4] 
 Letterhead of Military Forces of the Commonwealth, 2nd Military District District Head Quarters, Victoria Barracks 
 Sydney, 8th May 1918 
 Mrs, B. Bell, Greta. 
 Dear Madam, 
 In confirmation of my telegraphic advice it is with sincere regret I have to inform you that your Husband, No 3770 Private T Bell, 20th Battalion A.I.F. is officially reported to have been "Killed in Action" on the 8th April 1918. 
 I am directed to convey to you the deepest regret and sympathy of Their Majesties The King and Queen and the Commonwealth Government in the loss that you and the Army have sustained by the death of this Soldier. 
 Yours faithfully, Signed: Wm J   [indecipherable] Major. For A.A.G. 2nd Military District. 
