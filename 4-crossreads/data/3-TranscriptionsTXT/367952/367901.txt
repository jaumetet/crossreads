 [Page 11] 
 Transcribers note:  Written on Letterhead of Australian Imperial Force, Records Section. Westminster London 
 Quote: RED/10070/21/1. 
 29th November, 1918 
 No. 7040 Pte D.F. Campbell, 18th Bn. Aust. Imp. Force. France 
 3770 Pte T. Bell 20th Bn, A.I.F. Deceased. 
 In reply to your letter of 7th instant which was forwarded to this office by Chaplain W.E. Burkett, 33rd Bn. Regarding your Uncle. The above-mentioned soldier you are advised that records show him to have been Killed in Action 8.4.18. 
 Burial particulars are not yet available, but enquries have been instituted and immediately additional information is forthcoming you will be again communicated with. 
 Signed:   [indecipherable] For Officer i/c Records. 
