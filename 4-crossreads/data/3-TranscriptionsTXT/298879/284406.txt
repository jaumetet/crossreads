 [Page 125] 
 July 1915 Thursday 15 Fine&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Several piers nearly completed. They are built mainly by Egyptian engineers 
 Friday 16 Fine    Very dusty. Clouds of dust fly over the camp. The flies and heat are very trying. Have been acting brakes-man on a water cart to No1 Aust. Stn. Hospital. 