 [Page 34] 
 the Turks got well back into position before the British gained their objective. They reached part of their objective but had to retire owing to the opposition they met on the summit of hill 971 and the failure of the Welsh Fusiliers to reinforce in time. 
 Harry was wounded today hit in the shoulder while rushing into a sap to reinforce some lads who were being hard pressed by the Turk. 7.8.15. 
 The 7th Bn releived us 8.8.15. 
 The majority of the 7th were wiped out by a bomb attack 
