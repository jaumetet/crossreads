 [Page 38] 
 typhoid aboard 28.10.15 
 Landed at Anzac once again after 7 weeks rest 30.10.15 
 Took up position in firing line. One of our Leiut. was wounded while taking over this position. 
 Met Jack today it is our first meeting since we left Australia. 
 Heavy bombardment down south this afternoon. Our position here does not look too comfortable. It seem a hot bed for all kinds of shells and is situated between the 7th L.H. and 3rd Inf. Bde. The name of this position 
