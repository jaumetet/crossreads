 [Page 13] 
 Allied troops are here waiting a favorable opportunity to force a landing on the Peninsular. We had several route marches through Lemnos and found the green hills quite a change after sandy Egypt. 
 Left Lemnos Ild. 24.4.15. We steamed all night and anchored next morning within sight of Gallopoli. We were awakened about 4 a.m. by the boomb of heavy guns and scrambled on deck in time to see 5 or 6 of our warships blowing 
