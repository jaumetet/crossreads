 [Page 68] 
 of our batteries today but very little damage was done 22.4.16 
 The enemy are having a few pot shots at our billet with large howitzers but so far have failed to get one on the mark. 24.4.16 
 A good many large grenades thrown over by enemy. Leave to England started today 1 man from each Bn 25.4.16 
 Gas alarm last night great excitement. We sent up about 6 flares. The flares 
