 [Page 19] 
 line on the 2.5.15 and had to climb up a rope to get to our position. There are dozens of dead men lying about mostly belonging to the Naval Bde and R.M.L.Is. These units were given this position to hold while we were resting. The turks made several heavy counter attacks and they had all they could do to hold on to their position. 
 Abdul has quietened down now with the exception of snipers who pick off any man who happens 
