 [Page 67] 
 There are four of us on this kind of work. Our duties are to keep the telephone lines in good order. 19.4.16 
 The enemy shelled the 9th. Battalion in one of their billets. Their casualties were 18 killed and 35 wounded 20.4.16. 
 There are a good number of our aeroplanes up observing today and "Old Fritz" is wasting thousands of shots at them 21.4.16 
 The enemy fired about 200 5.9 howitzer shells at one 
