 [Page 76] 
 activity and some fine aerial manouveres. Immelman dropped from the clouds in amongst about a dozen of our scout machines. He made a great scatter amongst them but failed to bring any of them down 9.6.16 
 Our battalion took over last night at 11.30 p.m. We expect to remain here about a month. More artillery fire today 10.6.16 
 The Sixth bn made a raid on the German trenches on the left of our position 
