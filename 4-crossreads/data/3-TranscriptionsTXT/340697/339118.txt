 [Page 81] 
 by Gen Walker. He complemented us on our good work done on the 28th 1.7.16 
 We returned to our unit this afternoon and expect to leave for reserve tomorrow. 2.7.16 
 We returned to Bde reserve at Cul de Sac last night and expect to go back into Divisional reserve at the end of the week. There is increased artillery activity all along the line. Raids are being carried out nearly every night. This is supposed to be the begining of the 
