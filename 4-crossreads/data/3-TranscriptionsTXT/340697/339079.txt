 [Page 42] 
 A severe bombardment all day today. The turks got information regarding the changing of shifts at the Pine. The 6th Bde had about 157 casualties 29.11.15. 
 The turks threw some lachoramatory bombs over in the 3rd. Bn lines last night. They also used the same class of bomb at Chatham Post a few nights ago. There were no serious consequences with the exception of a severe smarting of the eyes to all those in the vicinity of the explosions 8.12.15 
