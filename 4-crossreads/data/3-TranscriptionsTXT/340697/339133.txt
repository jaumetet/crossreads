 [Page 96] 
 military medal for work done in first Pozieres stunt 19.9.16 
 Inspected by Gen Plumer. 21.9.16 
 Bde church parade. Some medals presented, (and also ribbons to decorations won at Pozieres). by Gen Birdwood 24.9.16 
 Went up into line at Bluff. Same procedure as last time of taking over. In passing through Ypres all troops we supplied with drink of cocoa by the people of the Aus. Comforts 
