 [Page 24] 
 to enable the Red Cross to collect the wounded and bury the dead. Under cover of the armistice the turks collected all their available reserves and prepared for a big attack but our artillery got onto these parties and bombarded them all night. 
 The moment the hour of the armistice was up, the enemy opened a rapid fire from machine gun, rifles, and shrapnil along our whole front. They attacked in several places but were easily driven off. Some 
