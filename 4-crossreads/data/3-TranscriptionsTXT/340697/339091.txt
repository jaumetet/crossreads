 [Page 54] 
 7 days C.B. Which means 2 hours pack drill each day and all fatigues. "Stiff luck". 31.12.15. 
 On leave to Cairo for 48 hours. "Hurray!" 12.1.16 
 Arrived home from Cairo having had a glorious time 14.1.16. 
 Two Divisions all being inspected by Gen Murray acting G.O.C. of Egyptian forces 15.1.16 
 Doctor in 26th bn commited suicide 30.1.16. 
 Went to Zertoun school of instructions. We are going to 
