 [Page 35] 
 last night 9.8.15 
 A torpedo from a submarine sank the Royal George this morning only 600 were saved out of 2000 troops 16.8.15. 
 We celebrated the anniversary of the 1st Bn today. We drank the reinforcements rum issue and also received their cigarettes 17.8.15. 
 Major Davidson died of wounds 19.8.15. 
 We expect to be releived some time this week. Our destination is supposed to be Embros. 3.9.15. 
 The Battalion left here today for Mudros. The transport 
