 [Page 237] 
 are getting more comfy &amp; settled down now, but it is just possible that we might move from here any day. 
 Best of Regards to all the neighbours, Mrs. Stephen &amp; the Dobboes, also to Otto, Amy &amp; Family. 
 Love to you all at Home, Harry. 