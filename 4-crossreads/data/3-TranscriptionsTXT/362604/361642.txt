 [Page 73] 
 Australian Red Cross 
 Bulford 20/4/18 
 Dear Dad, Mum &amp; George, 
 We have had several letters from you all but Ellis has taken them all away to answer &amp; He's out to-night.  I'll get them all &amp; answer later in the week.  We have had a horrid week of rain, hail, snow &amp; ice.  I hope it clears up soon because we want to play cricket &amp; Tennis.  I went for a walk this afternoon but it was not a bright day so I soon returned &amp; I have been 