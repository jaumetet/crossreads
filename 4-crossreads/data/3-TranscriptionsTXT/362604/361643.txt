 [Page 74] 
 reading &amp; writing ever since.  It is supper time now &amp; I am going to have a little, bread butter jam &amp; Tea.  Ellis hates this tack as He always did, has to come down to it sometimes.  He is a greater fiend than ever on meat, but He very often has to go without. 
 Well, Mum, Dad &amp; George, I must ring off for tonight. 
 We are all in the best of Health. 
 Lots of Love From Harry 