 [Page 162] 
 Australian Red Cross Bulford 22/8/18 
 Dear Dad, 
 I'm answering your letters of the same Dates as Mum's.  I was pleased to hear of Dave's arrival &amp; of all the ceremony in connection with the Ring.  I guess I'll have to bring my wife with me for all my old Girls will be married or engaged before I get Home again.  I do not understand Dave saying that He had something for George &amp; then not having anything.  I do not remember giving Him anything.  I have so many correspondents &amp; all are so 