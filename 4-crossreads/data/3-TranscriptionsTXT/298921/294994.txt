 [Page 22] 
 January 1915 Thursday 21 Tonight we give the ship's officers &amp; engineers a dinner, what with the purchasing of hoses to clean decks &amp; subs to sports etc: there are a good many Exps' for Officers &ndash; But we have a good ship's company &amp; are glad to have the opportunity to show our appreciation. 
