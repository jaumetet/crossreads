 [Page 9] 
 Dec 24 Thursday Rifle inspection, all hands now well, but strangles and ringworm in horses give trouble - 3 Japanese  cruisers are escorting us, &amp; when all the transports foregather at Albany there will be 16 of us. Xmas Eve we are getting up a Concert &amp; some cakes bought. 
