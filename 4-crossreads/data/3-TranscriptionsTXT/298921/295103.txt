 [Page 131] 
 May, 1915 Monday 10 Engaged 150 natives to day to help in the horse lines which present a deserted appearance.  Cutler  from Deniliquin is now driving the Brigade car, I was surprised when he told me who he was &amp; where he came from, I am acting Brigade Major I have to use the car a good deal. Love to all, I post this tonight. 
