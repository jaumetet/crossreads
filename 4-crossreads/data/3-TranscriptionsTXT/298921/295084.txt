 [Page 112] 
 April, 1915 Wednesday, 21 Moved via Heliopolis Cairo &amp; the old Roman Aqueducts to Maidi &ndash; where the 2nd Brigade made us welcome. We messed with them &amp; attended their Prize fights after &ndash; my orderly is going to ride their Buckjumper on our return. 
