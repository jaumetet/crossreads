 [Page 158] 
 June, 1915 Tuesday 8 Mrs Ferror sent me another Cake &amp; a tin of Chocolates very good of her &ndash; I am very fit &amp; getting plenty of office work at present which is a change after Field days on the Desert, I am doing Brigade Major or Camp Adjutants work now, which may mean promotion later on. 
