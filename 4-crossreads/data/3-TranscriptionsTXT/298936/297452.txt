 [Page 127] 
 October 1915 
 Saturday 23 
 Decided to give the job best so applied to Staff Serg. to allow me to get back to stretcher work &amp; let [indecipherable] take my place. 
 They both thought me mad for batman's job is notoriously "good" but I fail to see it absolutely. 
 Arrived back at camp before dinner &amp; began to feel worse. 
 Capt. Conrick said he was glad to get me back &amp; I shuddered to think of going on sick parade so turned into dug out &amp; took some medicine. 
 Temperature 102&deg;. 
