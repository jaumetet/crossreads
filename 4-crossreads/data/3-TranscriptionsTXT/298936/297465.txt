 [Page 140] 
 November, 1915 
 Friday 5 
 Light horse captured a trench from the Turks &amp; made another by sapping cut. 
 Turks tried to bomb them out but were unsuccessful. 
 New squad from [indecipherable]/[indecipherable] station, carrying along the beach. 
