 [Page 124] 
 October 1915 
 Wednesday 20 
 Artillery Lane. 
 No patients in morning. 
 Heard that Sir Ian H. has been recalled &amp; Gen Munro sent in his place. 
 Fellows very pessimistic so tried a little optimism on them, chiefly of the "Watch Russia" description. 
