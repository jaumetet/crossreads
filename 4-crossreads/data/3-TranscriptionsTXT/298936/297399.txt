 [Page 74] 
 August 1915 
 Tuesday 31 
 Arrived opposite Suez at about 6 a.m. &amp; [indecipherable] over until 11 oclock when we moved into the wharf. 
 Terribly busy having all medical stores packed ready for disembarkation. 
 Got kits together &amp; moved our men off into a special carriage reserved for A.M.C. 
 Train left for Cairo at 2.20 &amp; arrived at 8 or 9. Very interesting country at parts.  Goats &amp; camels tended by picturesque Arabs in oases' 
 Date palms &amp; various plants under cultivation sand for the rest. 
 Arrived 1st Hosp. at 10 supper &amp; tent. 
