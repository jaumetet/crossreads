 [Page 149] 
 November, 1915 
 Tuesday 16 
 In hospital. 
 Howitzer fell into Hospital cookhouse &amp; wounded [indecipherable] &amp; Taylor (3rd time) &amp; Sergt Hayes. 
 [indecipherable] badly burned all over &amp; not expected to recover. 
