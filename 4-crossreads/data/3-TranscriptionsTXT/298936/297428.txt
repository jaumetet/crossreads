 [Page 103] 
 September 1915 
 Wednesday 29 
 Put on stretcher &amp; went to 'Shell Green" for 24 hours duty. 
 A clear flat crest to hill is "Shell Green" no longer green but brown. 
 Very dangerous when artillery active because several guns round it which the Turks fire at. 
 Several .75 shells burst around the green and one burst at a dug out 10 yards away, wounding 2 men, one of whom died soon after we got him in. 
 Total for the day till dark 6 men including a brigadier wounded in neck. 
 Heard a lot about Simpson from boys. (in trenches) 
