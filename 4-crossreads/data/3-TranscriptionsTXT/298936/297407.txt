 [Page 82] 
 September 1915 Wednesday 8 
 Received instructions to prepare to leave tomorrow. 
 Paid in the morning.  Complemented by the Captain for good work in [indecipherable]. Sorry to loose me. 
 Had a flare up in the evening with Hunter with whom I am very good friends now. 
 Rather a pity I should [indecipherable]. 
 Met Colonel on return &amp; he gave me good wishes &amp; wants me to write how many hundreds of letters 
