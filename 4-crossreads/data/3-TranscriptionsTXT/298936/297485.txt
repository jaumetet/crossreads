 [Page 160] 
 November, 1915 
 Tuesday 30 
 Still cold &amp; snowing but the wind is dropping &amp; so [indecipherable] for us for we could get the circulation going. 
 Walked over to Bridges Rd. &ndash; about a mile &ndash; for water &amp; got quarter rations last night the Turks bombarded Lone Pine trenches &amp; killed or wounded 250. 
 60 were caught in a tunnel &amp; buried alive. 
 Our camp was shelled by heavy howitzers but no damage was done for most of them 
