 [Page 137] 
 November, 1915 
 Tuesday 2 
 Day off. 
 First time for a considerable time. 
 Went down to the beach in the afternoon to get beef or condensed milk, but got neither.  Very narrow escape from shrapnel pellets outside officers mess.  Beach was absolutely rained on by shrapnel.  Had Roberts &amp; Kause to tea, but can hardly call it a success for the porridge &amp; cocoa were not ready until nearly all were tired of waiting for them. 
