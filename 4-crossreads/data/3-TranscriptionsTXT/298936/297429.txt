 [Page 104] 
 September 1915 
 Thursday 30 
 On camp fatigue. 
 All that required to be done was carrying water &amp; rations. Getting used to being under fire although when a shrapnel comes rather close it gives one a bit of a fright. 
 Several aeroplanes mostly our own overhead during the day observing for the warships firing over to the Turkish lines. 
 It is interesting to see the shells fired by the Turks bursting near the shore (sometimes). 
 They are not very successful at that kind of firing. 
