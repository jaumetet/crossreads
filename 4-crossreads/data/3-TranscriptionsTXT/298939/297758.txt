 [Page 13] 
 I came down here yesterday evening &amp; was put in H Section. Tomorrow morning I parade in front of the C.O. Col. Marks &amp; present my credentials. 
 Tuesday 24th April 1917 Yesterday morning I paraded to C.O. and he told me what I already knew, i.e. that the ambulance had three dispensers, but one was an infantry man. If his transfer went through he would get the place; if not I would be called up. For the present my services were urgently required for fatigues. So kind of him. 
