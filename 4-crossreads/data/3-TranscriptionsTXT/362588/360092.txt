 [Page 37] 
 uch longer, it is quite impossible to put much energy into work here. 
 Richards had vile luck.  He has been left at Suez with appendicitis. 
 I saw J.W.S. Lucas in Alexandria the other day.  He was sent over to Gallipoli &amp; was there for a month without landing.  He is 2nd in command of the &hellip; and after doing their job for a month they went back to Alexandria &amp; have been there ever since.  As the men are practically fully trained, J.W.S. has absolutely nothing to do and is consequently having a perfectly rotten time. 
 I have nothing else of interest to say.  My mail supply is very poor.  I am certain many letters sent to me have gone astray so the more there are written the better chance I have of getting some. 
 Best love to all the family Yours very sincerely C.R. Lucas 
