 [Page 186] 
 [Arabic words.] [Several pages torn out.] [indecipherable] the consumption of which occupying until the fall in for morning parade in which all paraded, only the last picket who waited round the horse lines with manouevres [indecipherable] defective.  Clothing in laced boots and in [indecipherable words] palled in the morning light. 
 [Transcriber's notes: Neguilliet &ndash; possibly Hod el Negiliat &ndash; P. 1 Mafeesh &ndash; Arabic for 'nothing' &ndash; P. 16 Cam grano salis' - with a grain of salt &ndash; P. 22 Magdhaba &ndash; also spelt Mag Dabeh, Magdaba &ndash; P. 26 
 [Transcribed by Judy Gimbert for the State Library of New South Wales] 
 