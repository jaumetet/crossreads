 [Page 176] 
 think, candidly speaking, that I have a ghost of a chance but it is too good a job not to try for it. This afternoon we are going over to be gassed.  If it is the real stuff I shall probably die a horrible death as my mask has been kicking about for a long time &amp; is probably useless now. 
 4th June This morning about an hour ago, the black tailed taube came over &amp; dropped 4 bombs two of which fell about 100 yds. from us. They were only billycan bombs but still quite unpleasant enough. 
 8th June My application for a transfer to the C.T.C. has been turned down as I expected. Two little notes were attached to mine one &ndash; 
 Will you approve this transfer O.C. 1st L.H.F.A. and another No transfer with British units are permitted G.P. Dixon For A.D.M.S. Desert Column. 
 I then respectfully begged to point out that many Australians were in it, numbered it 4 &amp; 