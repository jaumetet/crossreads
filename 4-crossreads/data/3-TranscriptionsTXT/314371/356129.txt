 [Page 4] 
 April 30, Mon. Warned for the line in real earnest, issued w/- iron rations, amtn. etc. 
  April  May 1, Tues. Weather very hot.  Altered draft from a small one of returned men to large one of reinforcements about 300. 
 May 2, Wed. Windy, still standing by. 
 May 3, Thurs. 
 May 5th, Sat. Drew 48 hrs. rations and proceeded by rail to "Albert".  Saw the famous hanging statue of the Madonna &amp; child on Albert Cath.  Stayed in rest camp all night. 
 May 6th, Sun. Marched to a village near Bapaume viz:-  "Ligney Thilloy" [Ligny Thilloy] where we joined the 14th M.G.C., received a hearty welcome from officers &amp; men. 