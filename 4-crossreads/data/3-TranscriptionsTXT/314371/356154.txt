 [Page 29] 
 plenty of fruit from the native "Bum boats", very hot &amp; sultry.  The town is small but prettily situated at the foot of some fine hills. 
 March 19th Pulled out at 5 p.m. &amp; continued our so far very pleasant trip. 
 April 1st Ships fired big guns.  One S.A. boy effected by firing. 
 April 3rd South African died. 
 April 4th Dead man buried at 9.30 a.m. 
 April 5th Am Orderly Sgt. 
 April 11th Saw the Southern X for the first time.  Sighted 4 ships to N. at 6 p.m.  Lost them at dusk. 