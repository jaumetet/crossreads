 [Page 8] 
 About 7 o'clock I started for the clearing Stn. Went from there by motor to Bapaume where we spent the night. 
 Wed. 16th At 9 a.m. we were put aboard the hosp. train, made a very slow passage to Boulogne where we were carried by motor to No. 3 Canadian Gen. Hosp. 
 Thur. 17th After a good sleep woke up feeling real good &amp; Hungry. After breakfast the doctor had a look at us &amp; we had our hurts dressed. The weather is perishingly cold again. 
 Fri. 18th Still resting.  Cut now almost O.K. again. 
 Sat. 19th Same again, getting restless &amp; want to get out. 