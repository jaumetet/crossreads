 [Page 2] 
 [Letterhead &ndash; The Salvation Army] [Printed text] The Salvation Army. Headquarters: 69 Bourke St., Melbourne. 
 W. Bramwell Booth, General James Hay, Commissioner 
 Salvation Army Institute, Military Camp at 
 Bendigo B Coy 10/10/15 
 Dear Auntie Florrie &amp; Evie I expect you think it about time you had a few lines from me, but I suppose Doris has given you the outlines of my first experiences of camp life. 
 The first week I used to feel very tired of a night &amp; used to be glad to get to bed. The getting up at 6 am of a morning is rather hard, but a fellow has got to get used to it. 
 We went for a route march the other day &amp; the following comprises our kit. 1 Military great Coat, 1 hat, 2 pairs of Blueys 2 pairs of under pants &amp; shirts, 2 pairs of sox, 2 pairs of heavy boots 1 Cardington Jacket, 1 double pair of grey blankets 1 single pair of white &amp; a water proof sheet, &amp; a kit bag. I can tell you it is rather a heavy load to lug along, I know I found it so. 