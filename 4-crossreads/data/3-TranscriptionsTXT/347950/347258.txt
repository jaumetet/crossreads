 [Page 46] 
 15th Dec. 5AM. Captured French Barque "Marshall Daveout of Nantes sailing to Dacar with wheat. Wireless &amp; 2 3in guns aboard. Took off crew &amp; provisions. 1 pm sank her &amp; proceeded. 16th Dec Seaplane taken on deck again &amp; made flight at 5 pm. 17th Dec Seaplane up twice, morning &amp; evening 
