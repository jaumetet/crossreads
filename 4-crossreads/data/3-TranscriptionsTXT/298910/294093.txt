 [Page 76] 
 Sunday 3rd Monday 4th  Tuesday 5th  Wednesday 6th  Thursday 7th  Friday 8th Saturday 9th 
 Feeling only fair again &ndash; Met Norm Hill &ndash; attached to the 5th F. Amb, also Geo Chapband &amp; Jin Ferguson who are S. Br in the 12th F. Amb. Jim Ferguson who was not too good was evacuated today. Had letter from Frank, Lill Gort &amp; Kit Lock. Sgt Plummer &amp; Sgt Duffy left the Field Hospital yesterday &amp; went into Amiens, they both got pinched &ndash; so expect them both to lose their stripes. Raining all day. Very quiet day &ndash; hardly any patients in. 
 Sunday 10th Sunday 30th 
 During this period have been at times very queer &ndash; intermittent fever &amp; my voice is still missing &ndash; Have at times tried to do duty but am very easily knocked up. 
 Our Bearers are back again at M.D.S. after having  38  days in the trenches. On Xmas evening all our Bearers of C Section 
