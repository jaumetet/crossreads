 [Page 92] 
 Friday  2/3/17 
 Foggy &amp; frosty. Heavy Artillery going night &amp; day. 5th Division making successful raids nightly &ndash; Very busy. 
 3/3/17  Saturday 
 Foggy &ndash; very little sun &ndash; 29th Battalion doing splendid work &ndash; have been out numbered on several occasions, but have stuck to their gains &ndash; Very busy. 
 4/3/17  Sunday 
 Foggy &amp; Frosty &ndash; 14th Brigade still doing excellent work &ndash; they were specially mentioned in dispatches &ndash; particularly the 29th. I fixed up a Capt Herdman, who recommended 8 Military Medals, 2 D.C.M.s 
 Two women were brought in from the German Trenches &ndash; quite a mystery. Stretcher Bearers being called out at all hours to different sectors. 
 5th  Monday 
 Heavy fall of snow &ndash; nearly 12 inches fell. Things very lively in our sector &amp; very busy &ndash; night &amp; day. How pleased I was when mail was announced to get 5 letters 
