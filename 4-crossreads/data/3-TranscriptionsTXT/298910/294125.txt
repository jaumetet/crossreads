 [Page 108] 
 nearby is also just a heap &amp; heaps of coal have been thrown down and all around the wells. 
 Thursday  29/3/17 
 Dull day, cold &amp; drizzling rain. Rest of the ambulance &ndash; excepting a small party left behind to run the Station at Bernafay terminus under Major Pigdon, left for Bapaume to make 15th H.Q. there. Myself &amp; a lot of others left for Buigny to make up a team of mobile Bearers. Only a few casualtys. Altho the roads are good in places &amp; much superior to what we have been used to, but the amount of traffic &amp; the drizzling rain makes it very slushy &amp; our feet never dry. Fritz shelling around us very freely with big stuff. 
 Friday  30/3/17 
 Dull &ndash; cold &amp; drizzling rain. Very few casualtys. Drinking water hard to get hold of &ndash; so many traps of filth etc. Harry Ramsden found some womens clothes in Labarcoque &amp; dressed up in them, he did look a break up even made the Adjutant laugh. Heavy firing at night. 
