 [Page 66] 
 a bruse on my cheek where I fell last night &amp; hit it on the Pick handle There is a great lot of loose wire about all these battle fields Telephone &amp; Barb. 
 Sunday 15th Road repairing Three of our Baloons shot down A perfect day. Got more Chats on my shirt this evening than I have had all the time I have been here 
 Monday 16th Working on the road again. Saw one of our planes shot down 2 Taubers were seen to fall last night. Heard 