 [Page 28] 
 Thursday 11th July Very quiet but busy day today. 
 Were no parades, and during the morning commenced to read up some pamphlets I have shelved for some time. In the middle of this Mr Garland, the WO.  came along with a request for me to clear up some concert party stuff which is reposing in the end of the dental hut, to be used for patients in the event of sudden offensives. Sorted out the gear 
