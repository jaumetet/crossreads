 [Page 54] 
 by the Guards, most of  whom  the survivors of whom they had passed on their way up. Here they dug in. 
 From that day to the present they have held  first  a front first from Meteren  almost  to the Nieppe Forest, then  from  round Meteren, &amp; lately from in front of Merris to the forest. In the first day after the rush there was only the Aust 1st Div. in the front line on the whole sector protecting 
