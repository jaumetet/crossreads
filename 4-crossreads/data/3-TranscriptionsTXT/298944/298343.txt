 [Page 75] 
 the cottage) was a coat of arms, - a rose or some such emblem surrounded by floral work. The big barn was C section billet, &amp; was also used for concerts, as we could see from the chalked up notices of previous visits by evening concert parties. 
 Passed through Hueringhen &amp; then through Blecques, where I noticed a very old church in pure Gothic style, to Helfaut, where we had tea in a paddock 
