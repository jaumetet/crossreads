 [Page 47] 
 packing the rest in a biscuit tin for secret conveyance, by motor ambulance 
 To day the main body of the 88th British Field Ambulance arrived, and enterprising members of that unit took over our bivouac. At the present moment they are busily engaged in hammering &amp; otherwise attending to it. 
 The various members of our unit are spread about on the grass round the pond, having lost their homes, 
