 [Page 81] 
 July 1918 30. Sunday Lovely day. Dawdled about. Girlie brought another little friend. A Miss Naylor came from London. 
 1 July, Monday. Got up feeling weak.  Maud and S.  went for ferns. Retired before dinner with an attack. 
 2. Tuesday. Feeling weak but got up &amp; pottered about. Lovely weather. 
 3. Wednesday Left for London. Paul &amp; Olaf met us at Waterloo. I went to bed at 2 &amp; at 4 had a violent attack. Damp, cold bed I think. Had to call Mrs Carpenter. F.P. called up but I was too ill to answer. Lovely day. 
