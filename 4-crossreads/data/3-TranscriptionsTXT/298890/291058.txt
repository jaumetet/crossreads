 [Page 131] 
 22. Sunday Miss Smith &amp; I at home.  Many Indians.  Ethel &amp; K. Adams. F. Post. Mr. &amp; Mrs. Carpenter &amp; others.  Wet dull day. 
 23. Monday Worked in office &amp; went home to Miss Hodge at night.  Miss Wren had nice dinner waiting. 
 24. Tuesday Worked in office &amp; went home to Miss Hodge. 
 25. Wednesday Lovely frosty morning but grey later.  Dinner with Miss H &amp; Miss W.  Supper alone &amp; went to bed. 
