 [Page 53] 
 24. Sunday. Beautiful warm sun all day. Miss Hodge had tea at  Lyceum for Helena Normanton then Miss H &amp; I went home to supper with Mrs Baft.  Dr. Schutlebury from Endell St. there. Fog down at Piccadilly. Clear moonlight at Golder's Green. 
 25. Monday On Monday night shivered with Malaria again. dull and cold. Struggled out of bed &amp; spent a miserable day on sofa. Dull &amp; cold. 
 27. Wednesday  Feeling very depressed and weak. Cold &amp; grey 
