 [Page 65] 
 5. Sunday Grey showery day  Went to Dr. Synge's for tea Mrs French &amp; Fred Post &ndash; a y. man from Calif were there. 
 6. Monday Nice day.  Not v. well.  Pottered  In evening went to Serb lectures at Lyceum.  F. Post there.  He walked to Marble Arch with me. 
 7. Tuesday Cold showery day.  Went to Miss Newcomb at 10 left at 5.30.  Miss Hodge clean off about brutal letter from Dr Pethick.  Feeling very skew whiff 
 8. Wednesday Wrote letters.  Not v. well.  Went to Minerva at 4.30 to tea with Mrs. French &amp;  Mrs. Aldridge.  Home with latter for an hour then to hear Brailsford on dangers of Inter-nationalism at Ashburnham club. 
