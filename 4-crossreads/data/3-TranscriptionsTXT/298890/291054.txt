 [Page 127] 
 8 Sunday Drizzle &amp; fog &amp; black twilight all day.  Claude called up at 5.30.  I met him down town &amp; went to pictures at Marble Arch.  Took quinine Sat &amp; Sun 
 9 Monday Fog.  Black &amp; wet  office [indecipherable]  went to bed very tired.  Finished re-reading "On a St" [?] 
 10 Tuesday Office routine home &amp; to bed very tired. 
 11 Wednesday Office routine.  In evening A A. Miss [indecipherable] &amp; Booth Scott Mrs Dalrymple &amp; S met at Old Vic  for 'Love's Labour Lost."  Dark, dark, days 
