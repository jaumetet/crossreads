 [Page 52] 
 Locrehof Camp Camp Order issued containing the appointment of three Drivers. Weather calm and mild, slight showers during the day. 
 23.1.18 Work on walls continued.  Weather mild and misty, few showers during day. 
 24.1.18 Allotment of 1 Officer for Corps Intelligence School received.  2/Lieut. MacMullen nominated.  Also allotment of 1 Officer and 3 N.C.Os to attend Corps Gas School, both these schools assembling on the 27th January. Weather still mild and cloudy, no rain. 
 25.1.18 In preparation for move, all Stores were checked and preliminary packing commenced.  C.O. (Capt. D.A. Whitehead) visited new Camp, also C.O. 5th Aust. Machine Gun Coy., orders having been received that this Company would relieve the 5th in the line. Lieut. W.H. Mathieson proceeded on leave. 2/Lieut. MacMullen proceeded by train to Intelligence Course, Aust. Corps School, Aveluy. Administrative Instruction No. 27 regarding move received from D.H.Q. 
 Reference Sheet B. &amp; F. 28 