 [Page 16] 
 23rd Aust. Machine Gun Coy. 
 Perham Down Camp &ndash; 13-2-17 Acting under Authority C.R., A.I.F. 5103 (A) dated 29-1-17 the 3rd Divisional Machine Gun Coy. was formed, strength 3 Officers and 150 Other ranks.  The following Officers and other ranks had joined from France. 
 Lieut. R.B. Coleman, No. 10, Sgt. E.D. Bennett Lieut. C.P. Bradburn, No. 35, Sgt. W.J. Corby 2/Lieut. R.L. Walker, No. 79, Sgt. T.R. Irwin No. 40, Pte. T. Copland No. 125, Pte. C.W. Taylor. 
 15.2.17 Lieut. Bradburn &amp; 2/Lieut. Walker proceeded to Grantham to undergo a Machine Gun Course. 
 17.2.17 34 specialists including 25 Drivers, 5 Signallers, 2 Saddlers, 2 Artificers, proceeded to Grantham to undergo Specialist courses. 
 20.2.17 The following Officers &amp; N.C.O. reported from France, 
 Capt. Algie, W.M., 10th M.G. Coy. Lieut. Chambers, A.L., 10th M.G. Coy. Lieut. Wedd, C.C., 9th M.G. Coy. 