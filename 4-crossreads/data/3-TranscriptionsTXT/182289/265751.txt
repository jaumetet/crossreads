 [Page 6] 
 21.11.16 9th A.M. G. Coy leave Camp 29.  Entrained at Amesbury Railway Station.  Departed 5.30 a.m., arrived Southampton 8.30.  Embarked at Southampton Docks on SS Hunslett.  Departed 7 p.m. 
 22.11.16 Disembarked at Le Havre at 9 a.m. and marched to Rest Camp. 
 23.11.16 Left rest Camp at 7.30 a.m., marched to station.  Entrained at 1.30 p.m. for unknown destination. 
 24.11.16 Detrained at Bailleul and marched to Outersteenne [also spelt Outtersteene], a village 3 miles from Bailleul. 
 26.11.16 Outersteenne, Three Officers and 61 men leave for firing line, as advance Party.  Pte. Bunn accidentally shot in hand, conveyed to Hospital. 
 28.11.16 One Officer and 16 men leave for firing Line. 
 29.11.16 Headquarters and remainder of Company leave Outersteende at 9 a.m. and proceed to Armentieres.  Headquarters established Rue National. 
 30.11.16 &ndash; Chapelle Armentieres Right Sector:- 1.30-1.40 p.m.  Evening trench Mortars fired on 102nd Brigade Lines.  2.30-3.10 p.m. Heavy bombardment on enemy lines 