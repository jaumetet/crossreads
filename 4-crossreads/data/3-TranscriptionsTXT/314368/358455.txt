 [Page 245] 
 Australian Imperial Force Base Records Office, Victoria Barracks. Melbourne. 
 30th April, 1919. 
 Dear Sir, 
 I have to acknowledge receipt of your letter dated 20th inst., and in reply to state advice has been received that your son, Gordon Thomas Edwards, 54th Battalion, was appointed 2nd Lieutenant on 2.1.19, but same has not yet been promulgated in the Commonwealth of Australia Gazette.  This Officer proceeded to Officers Cadet Battalion, England, on 19/6/18 ex 14th Australian Infantry School. It is not the practice of the department to notify next-of-kin when members of the Australian Imperial Force are granted a commission or transferred from one unit to another. 
 Mails addressed as under should be redirected on arrival abroad to wherever your son is located:- 
 2nd Lieutenant G.T. Edwards, 54th Battalion, Australian Imperial Force, ABROAD. 
 Yours faithfully, JMLean, Major, Officer i/c Base Records. 
 T.J. Edwards Esq., "Orangedale" Stuart Town, N.S.W. 