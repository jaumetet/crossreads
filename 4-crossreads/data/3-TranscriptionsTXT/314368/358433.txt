 [Page 221] 
 Australian Imperial Force Base Records Office\ Victoria Barracks Melbourne 28th May, 1918. 
 Dear Sir, 
 I now beg to advise you that Lance Corporal G.T. Edwards, has been reported wounded. 
 His postal address will be- 
 Mr No.1647 Lance Corporal G.T. Edwards, 54th Battalion, Australian Imperial Force, Abroad. 
 Don't fail to send change of address to Base Records, Melbourne. In the absence of further reports it is to be assumed that satisfactory progress is being maintained, but anything later received will be promptly transmitted, it being clearly understood that if no further advice is forwarded this department has no more information to supply. 
 Yours faithfully, 
 Jmlean Major, Officer in Charge, Base Records. 