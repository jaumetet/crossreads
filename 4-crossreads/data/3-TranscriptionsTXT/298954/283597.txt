 [Page 142] 
 We even had dancing a couple of times to a whistling accompaniment;  but the Russkis got excited and pounded off into "Czardos" or something. 
 The din brought a frantic Schwester up from the Wachtstube and the "Czardos" ceased. 
 26 corpses passed our window today en route to the Morgue.   A submarine dived in the Bay during her tests with a conning tower open.   Finis. 
 Our view over the harbour of Kiel from our balcony is splendid.   Torpedo&ndash;flotillas leave daily on patrol work (mostly in fours);   submarines are coming and going continuously. 
 The "Wolf" left for Lubeck a few days after our arrival. 
