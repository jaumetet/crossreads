 [Page 94] 
 The stewards got away with most of it but the next table boasts a big dented plated horror of a tea-pot which is fought for by the other table on an average once during each three meals. 
 Thu.  22nd Nov. We are now well down towards the Cape &ndash; the first albotrosses we have seen for many months arrived today &ndash; two of them. 
 Fri. 23rd Nov. Smooth, heavy swell. 
 Hundreds of birds flying round the vessel mostly the black Cape Pigeons and albatrosses. 
 Sat. 24th. Off the Cape;  have crossed all the Australian and Asiatic trade routes without any success;  this morning "Wolf" swung North again to re-cross the various trade-routes. 
 Fri. 30th Nov. A bad week for me.Two days ago the Younger Butcher carved my foot;  yesterday I had to return to the hospital with violent pains.   A vessel captured this morning; I knew nothing of the capture till Bob told me during a visit to the hospital. 
