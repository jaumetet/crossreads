 [Page 237] 
 Tue. 19th. This was my first appearance in quite a new role &amp; I made a huge success of "making-up" if I say it myself As Shouldn't. 
 Then I dressed Smith for his "soubrette" turn, &amp;, after I had turned him out as he was not often turned out before, the pig refused to trust his damned face to my ministrations, asking my Hated Rival to do the work. 
 I plotted vengeance &ndash; and got it.   West "in front" during Smith's turn, noticed all his little faults of "make-up" &amp; returned to the dressing room with a grim expression boding no good for the star soubrette. 
 For "The Wrong Flat", I was to dress &amp; make-up Harry Whittaker, the "heroine".    Harry has always till now looked gauche and awkwardly dressed, chiefly owing to his clothes, his fondness of a too pallid make-up, &amp; the awful hats he wears. 
 I rushed round to the Italian wardrobe, "wangled" a bonser white toilette with a super-smart black tagel hat which came from Berlin &amp; got the coiffeur to dress the good blonde wig in a high, "piled-up" style. 
