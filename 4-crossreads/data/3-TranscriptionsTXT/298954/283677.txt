 [Page 222] 
 "An Awful Story" Caste. Princess Pellitoes .. a coloured damsel ..   H.L. Smith King Gobblu  H.J. Heck. Miss O. Perc, the passenger    R. Alexander The Chf Officer &hellip; of the wrecked vessel  &hellip; E.A. Buckingham The Bosun   &hellip;   C. Hampson Pall ..  Another passenger &hellip; O. Newble. Cannibals etc. Songs, "Tell me".   Smith &amp; Hampson -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "Upidee" (parody Excelsior")   Ensemble. -&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; "On Moonlight Bay"    Ensemble. 
 Smith's make-up was great, a little frizzled wig, a black skin, a bouncing figure &amp; his only garment made of rushes. 
 Sun. 29th A repeat concert, much better, all old stuff.   The "Story" was ditched (no black make-up left) Smith &amp; B sang "Milestones", Dennelt.  "I love the Ladies" &amp; "Swim Son Swim". 
 Tue. 1st Oct. A stormy general meeting of the English theatrical company. 
 The committee were censured for putting on such a show &amp; a lively meeting is predicted for Friday. (I think, talk &amp; do nothing but things theatrical now.   'Tis a most welcome diversion.) 
