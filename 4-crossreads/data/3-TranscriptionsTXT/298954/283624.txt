 [Page 169] 
 2nd Appel is at 9 p.m. 
 Mon. 8th Apl. (cont.) To resume yet again:- 
 After dinner the Old Inhabitants usually play Bridge, but we new arrivals are too busy wondering when &amp; for where our marching orders will be. 
 Tue. 9th Apl. While walking under the trees this morning the General actually noticed humble me!  Further, he walked with me for about half an hour and chatted must amiably. 
 My Mess looked on in Awe and when the old boy buzzed off they wanted to know all he said, how he said it, &amp; why he said it. 
 I replied indifferently that he "just talked", in a tone that indicated that it was quite a usual custom of mine to take constitutionals with Generals. 
 In the evening I was again Observed by a Personage.   Lt. Colonel Howard Bury (K.R.R.) walked for an hour or two in the evening with me. 
