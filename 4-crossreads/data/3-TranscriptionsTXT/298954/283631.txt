 [Page 176] 
 The villagers heard of this and accompanied our meal with a rain of stones on the tin roof. 
 The stoning continued as we marched back to our two carriages side-tracked in the deserted station &ndash; Townshend beside me getting a hit which cut his ear. 
 Sometime during the night we hitched on to a passing train and daylight found us pushing through the pine forests which surround Berlin. 
 Mon 15th We caught glimpses later of busy canals, busier tree-lined thoroughfares, enormous numbers of the usual "flats", but really saw little of Berlin, as we circled round on the Ringbahn instead of going through the city.   We had breakfast (bread, wurst, &amp; coffee ersatz) at a militar depot, right on the railway line, shunting in to the Berlin Stettiner Bahnhof at 10 am.   The Stettiner is the main station for North Germany. 
