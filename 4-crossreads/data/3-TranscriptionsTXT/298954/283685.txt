 [Page 230] 
 A typical incident occurred yesterday.   For years all arriving prisoners are locked in the strafe barrack till required for interrogation purposes. 
 Fri. 25th (cont.) No blankets have been supplied, even in winter.   Now that it looks as though Foch is on the move. &ndash; a box of blankets was  yesterday  placed in the "Strafe" barrack.   After 4 years!   Speech (A) 
 Mon 28th. Food is very scarce in the lager &ndash; no packets again for 14 days. 
 Mc "en relief" last week, I go on today.   (Sounds like entering the workhouse, doesn't it?   When one has not received a packet for 14 days the Relief committee dole out weekly grants till the packets again resume arriving.   All sorts of scandal about the committee dining on 6 courses &amp; feeding German "lydies", of course.) 
