 [Page 18] 
 road beyond Chateau Wood. Home at 8.30 and worked hard on dugout till 4. Just finished &amp; heavy thunderstorm   bed early. 
 20.10.17. Out on job at 4 and repaired lane broken in many places. Shelling heavy  &amp;  had to retire at 10.30. Guard at 4.30 
 21.10.17. Guard. Improvements to home. Fine day. Shelling round here last night &amp;  bombs dropped nearly every night. Piece through dugout. 
 22.10.17. Rain  &amp; wind. No work and read and wrote. 
 23.10.17. Out repairing line at 4 and Kernot wouldnt  take us on job until late  &amp;  got reprimanded by Pioneer Colonel. Alver wounded. 
 24.10.17. Made bunks &amp; improvements and read. Fine 25.10.17 Strong N.E. wind and did some loading in aftn and read. 
 26.10.17 Home all day. Two little jobs making sign boards and a sieve. Rest of time improved door of dugout and wrote a little. Fine day. 
 27. 