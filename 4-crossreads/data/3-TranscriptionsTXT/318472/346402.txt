 [Page 111] 
 26 June 1918 Wednesday Made a start with another Draft of about 60 today &amp; got them through. The M.O. &amp; R.M.O. kept very busy all day. 
 1918 June 27 Thursday Busy all day getting Draft through the Kit store &amp; now have them all complete for France Got through about 170 this week &amp; they are ready to go at any time. 
