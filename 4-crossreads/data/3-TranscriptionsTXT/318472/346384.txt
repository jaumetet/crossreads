 [Page 93] 
 8 June 1918 Saturday Arrived in London 8.5 a.m. Did a little business &amp; then went to Leytonstone to see my friends &amp; left Waterloo 6.P.M. arriving back in camp about 9.30 P.M. feeling fairly tired after a good enjoyable holiday. 
 1918 June 9 Sunday 2 after Trinity 
 Not doing very much today takings things easy to get over the tired feeling after coming back from Leave. In the afternoon Sgt Gunner &amp; I went for a stroll through Dinton  &amp; Barford. Raining fairly heavy in the forenoon. 
