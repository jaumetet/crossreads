 [Page 189] 
 12 September 1918 Thursday A dull day with a freshening breeze and Sea. Fairly heavy rain in the evening We are now well into the "Bight". Good news continues from Western Front though things are getting quieter there. Nothing of importance on the Boat. 
 1918 September 13 Friday A nice bright day but nothing of import to note. The boat is travelling very slowly since leaving the West and at the present rate of speed it will be late Mon: before we reach Port. Good news continues from the Front but things are much quieter. 
