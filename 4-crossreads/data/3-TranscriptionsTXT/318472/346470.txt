 [Page 179] 
 2 September 1918 Monday The wind has no way abated &amp; the sea is fairly rough with a heavy sea. About 8 P.M. there was a fairly heavy thunderstorm but we appear to have missed the worst of it. No war news. 
 1918 September 3 Tuesday A nice sunny day with a fair wind &amp; swell. Still steering about 10 kts. &amp; everything going well. Everybody appears to be getting tired of the journey and all will be glad when it is finished. 
