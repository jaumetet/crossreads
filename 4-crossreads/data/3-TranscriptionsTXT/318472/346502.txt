 [Page 211] 
 4 October 1918 Friday Just mooched about home and in the afternoon Dorrie Ruby and I went across to Duntroon where I saw a few of the old boys. Good news still from Fronts. 
 Saturday 5-10-18 Mother Dorrie. Ruth Ruby and I went to Cotter in the afternoon per motor &amp; had a very enjoyable afternoon. The Weir at the Cotter River is complete to a depth of about 76' and there is a fine flow of water running over the top. Work there is now stopped. 
 Sunday 6th 
 Layed in till late Got up and went down to River with Mac &amp; Malkie &amp; had a row in the boat. In the afternoon Mother Ruby &amp; I went to Upper Canberra to a Memorial Service to George Porter killed in France. Had a stroll around later &amp; a Sing Song at night and that is about all. Nice fine weather Sgt Shaw to tea -? [indecipherable] from [indecipherable]. 
