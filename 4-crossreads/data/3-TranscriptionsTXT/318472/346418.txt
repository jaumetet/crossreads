 [Page 127] 
 12 July 1918 Friday Went up for Medical Colonel Newmarch presiding. Do not know the result yet. Things going along in the same camp-like way. 
 1918 July 13 Saturday Light factiques &ndash; Battn parade &amp; games for the Troops in the morning. Sgt Clark &amp; I went to Easton &amp; Portland in the afternoon &amp; had a very pleasant time. Went to Pictures at night which were only middling. 
