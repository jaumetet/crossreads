 [Page 39] 
 15 April 1918 Monday A very uneventful day. The weather continues cold and damp. Smith and I went to Albany Wards Show at night fairly good show. Getting tired of Camp life 
 1918 April 16 Tuesday We are having miserable wet weather and it is very cold. There is nothing of interest to note. - Just the same old routine. 
