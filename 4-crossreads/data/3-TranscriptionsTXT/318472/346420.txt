 [Page 129] 
 14 July 1918 Sunday 7 After Trinity 
 Just the usual camp routine Nothing of much importance going Wet &amp; squally Sgt Clark &amp; I went for a stroll down town at night but were home early. 
 1918 July 15 Monday News through that D16 carrying returns from here has been Torpedoed. Believed all saved. Nothing of much importance going or rather the Germans have started an offensive against the French but are not making much progress. 
