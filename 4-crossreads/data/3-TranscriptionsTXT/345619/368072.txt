 [Page 120] 
 [Postcard showing a cartoon of an elderly, well dressed woman speaking to a soldier who looks a little bored.  The woman says to the soldier:] You'll have had some narrow escapes from death" [to which the soldier replies] Rather! I once fell out of a pram when I was a kid. 
