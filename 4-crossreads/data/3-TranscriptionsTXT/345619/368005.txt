 [Page 53] 
 [Reverse of the above post card] Master Frank Burrowes Franurra Rooty Hill N.S.W. Australia 
 Isn't this a beautiful old gateway? There are lots of places like this in this City but we did not have time to go and see them. We are having a great time here in Cornwall, such lots of little kids like my two, &amp; that's what I like best, heaps of love &amp; kisses from Dad XXXXXXXXXX 
