 [Page 23] 
 I remember years &amp; years &amp; years ago when I was a little kid, I read "The Coral Islands" and I thought it was a fine book, I suppose you like all sorts of books now. 
 I suppose Mum told you, that card I sent from France with "Bonne Annee" means the same as "A Happy New Year" so I ought to have kept one to send you about now. 
 Fancy you going on the Stage, I wish I had been 
