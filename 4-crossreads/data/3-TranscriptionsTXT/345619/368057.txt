 [Page 105] 
 [Reverse of the above postcard] Address 7008 Dinkydumps 18th Batallion A.I.F. 
 On Active Service Mrs Mum Dearheart I forgot to tell you in my letter just closed you can leave out the "21st rf" now &amp; just address as above. The "I" in A.I.F. stands for "Imperial" not "Infantry" I hope you like the enclosed cards, I think they are just lovely expressions. Good night my very own wife, with kisses &amp; squeezes &amp; hugs (&amp; pats on the ____) from your Husband 
