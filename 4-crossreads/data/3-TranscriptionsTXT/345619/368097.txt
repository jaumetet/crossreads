 [Page 145] 
 [Reverse of the above postcard. This is to Burrowes' mother] Mrs Burrowes Upperby Rooty Hill N.S.W. 
 18/4/18 It is not quite as bad as this picture, here, but it is quite enough of a rush. Have sent you a good few cards &amp; a couple of letters, hope you got them. Hope you are well &amp; will be glad when I get a letter. Love from Arthur 
