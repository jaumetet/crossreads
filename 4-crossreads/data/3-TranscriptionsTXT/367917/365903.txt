 [Page 6] 
 fund in connection with the Company; I should like it to go towards helping anyone having financial difficulty in finishing his Univ. course on his return from the front, but leave it open for it to be applied in that or any other way you may think best. 
 I hope I may be able to give further help if required at a later date &amp; it will be a pleasure if I can do so. 
 Please pass the donation in as an anonymous one as I do not like my name published. 