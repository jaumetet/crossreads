 [Page 38] 
 115  Mach Gun Detach  
 116 10 LH placed under orders no 3 section O.C. in relief 5th LH which rejoined 1st Aust Div. 
 117 Men Exposing selves  It has been noticed that large numbers men Expose themselves to enemy's view &amp; fire during the recent informal suspension of hostilities thus disclosing our defences &amp; arrangement of our interior conditions &amp; communication. Men not on duty in connexion with actual Pour parlons, are not to show themselves. 
 120  Promotion in Aust Imp Force  
 121 Traffic. owing to congested state of beach it is necessary to forbid beach to all but officers, N.C.O.s &amp; men whose duties necessitate them being there. To give Effect no one will be permitted to remain on beach unless in possession of pass or in a party under officer, or N.C.O, 
 name= "a4892039">  
 [Page 39] 122 All occupying dugouts in bank on beach are informed that it may be necessary for them to vacate, &amp; no more dugout intended for occupation will on any account be made in bank. 
 123 Summary of Casualties for the week is to be entered on back of army form B231 which is to be rendered to Div HQ every Thursday morning 
 124 Captured Arms, amm etc All Turk rifles captured &amp; all Amm, particularly army 65 MM. are to be at once handed in to nearest Ordnance officer with a view to them being sent to England. 
 B.O. 125 Disposal of effects of deceased The pay book &amp; Ident[ity] disc of a deceased soldier &amp; any personal effects which have sentimental value, sent 