 [Page 16] 
 the 17th move off.  MacCallum has been sent off the Bde. owing to new establishment &amp; is rejoining his unit.  Arthur is well.  If I transfer I shall probably not get any mails of any sort until the new address is received in Australia and the necessary interval for mail to come over has elapsed. 
 I saw the best football match I have ever seen here yesterday, Maori v Wellington Rugby Union on the shores of Lake Timsah.  Wellington won 5-0. 
 Best love to all. 
 Yours ever C.R. Lucas, Capt. 
