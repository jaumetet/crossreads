 [Page 172] 
 Thursday 3 June 1915 
 Nov. 20th Back form Lemnos to Durrants Post. 
 Nov. 27th Snow for the first time &ndash; early fall &ndash; numerous cases of frost bite!  Everything very quiet.  Gallipoli will never spell success to Allies. 
 Dec. 13th First rumours of Evacuation.  Are we on the eve of the most stupendous, colossal blunder in the annals of military history?  Maybe, may be not.  A week will tell, at any rate. 
 Dec. 17 Gen. Birdwood visits firing line. 
 Dec. 19th "Der Tag".  Everything upside down.  Waste beyond description.  Many own crosses &amp; friendly warring against the Teutons left with Johnny Turk.  Also Dummies galore. 