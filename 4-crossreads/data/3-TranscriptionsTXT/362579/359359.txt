 [Page 119] 
 July 22nd &ndash; so you shall be seeing him soon &ndash; the poor boy has really been too long away   &amp;  I believe is unfit for Active Service. His mother will be delighted to see him. 
 Had a very quiet time at last C.C.S. but had letters so they cheered us a bit. None from Aust. fort ever so long. Hope you are all well   &amp;  happy. 
 Fondest love   &amp;  kisses to you all. 
 From, Yours lovingly, Edith 