 [Page 161] 
 knew her. She is a charming woman. Hope she has a real good time. 
 Have not received the    Cake   yet, hope it will arrive son, am longing for a good feed of home made cake. Make me a big one when I return, Oh to think of those nice sponge cakes Belle   &amp;  Ethel make. You'll think that sounds healthy &ndash; will I am. 
 Fondest love   &amp;  kisses to you all. Suppose you will all be thinking of Xmas by the time this reaches you. 
 Love to all my friends. 
 From Your loving daughter Edith 