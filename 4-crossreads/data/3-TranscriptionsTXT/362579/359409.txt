 [Page 169] 
 Suppose she is well. Give her my love   &amp;  tell her to write. So glad Edmund called to see you   &amp;  told you how well I am. I'm very strong   &amp;  have a number of good pals but shall never fall in love with anyone again. So can be quite happy    &amp;  do my work without worrying. Fondest love   &amp;  kisses to you all. 
 From Yours lovingly Edith 