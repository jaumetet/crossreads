 [Page 147] 
 3rd A. Gen. Hosp. B.E.F. France. 9.9.17 
 My dearest Mother   &amp;  Sisters., 
 Did I tell you that I got two letters from home this week. Belle's of June 3rd   &amp;     Alice's of    June 24th which I was delighted at getting. I know the family would be very pleased to see you back. So glad you had such   a happy time   &amp;  look better for your change. You will all miss Eddy very much, does he go all day   &amp;  come home for dinner? Who takes him   &amp;  brings  him home? Of course I'm very anxious to know all about the darling boy. 
 I wonder if there is anything 