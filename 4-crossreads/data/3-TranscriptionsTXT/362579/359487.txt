 [Page 247] 
 Not a handmade  very useful. Wish some letters from home would come before I leave. 
 Wonder if I shall see you before this reaches you. Hope Mother is quite better   &amp;  Ethel had a delightful holiday. Oh, I received one letter from Jane from Talbot. Trust Eddy is keeping well   &amp;  getting stronger. Fondest love   &amp;  kisses to you all. 
 From Yours lovingly Edith 