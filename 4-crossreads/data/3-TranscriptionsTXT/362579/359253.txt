 [Page 13] 
 about 200   &amp;  in the p.m. took in nearly 300, so to-day we could only take in about 50 patients. Then 29th started this p.m.   &amp;  have received quite a number. Can see us evacuating perhaps to-morrow. 
 Dorothy is very well   &amp;  seems to be liking the work here. 
 It is grand for us to be together again. We are leading a very simple, quiet life now, work   &amp;  bed seems to be the mode, anyhow it suits me at present. 
 Did I tell you that I received a nice letter from Edmund the other day, from England, have not answered it yet. Do you know I quite forgot to mention   &amp;  send wishes to dear old 