 [Page 180] 
 Mother has not to use her own birthday money. 
 Pleased she liked the present. 
 How awfully sad about Miss Cameron's death. Poor Mrs Trangmar   &amp;  Ada will find it very lonely. Am writing a few lines to Mrs Trangmar by this mail. 
 My special pal (man) wrote such a sad letter this week telling me of his brother's death &ndash; was killed in action. The dreadful thing is that he got him transfered. 