 [Page 9] 
 number of patients but not the very acute ones. 
 I have an orderly in each tent some have 26 beds (stretchers on tressels)   &amp;  others have stretchers on the floor. All the tents have wooden floors   &amp;  a stove in the centre. They really don't look too bad, have heaps of warm blankets, socks,   &amp;  mostly flannel pyjamas. 
 All are very happy   &amp;  jolly as soon as they are at all better. My orderlies are awfully good boys   &amp;  I get on very well with them. Last night I received a nice letter from Edmund &amp;  he is well   &amp;  in London. He may be there four months, so I may have the chance to see him. 
 I daresay he'll have a ripping time. The Padres have a very easy job in London, in fact, On Active Service, never had such good times. 
 I received darling Eddy's Xmas card the other day. He is getting on with his writing. Suppose he will be going to school soon. Hope the poor kiddy likes it   &amp;  is strong enough to stick to it. Fondest love   &amp;  kisses to you all. 
 From Yours lovingly Edith 