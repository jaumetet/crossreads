 [Page 49] 
 Sunday 28th Jan Cold conditions still continue. Nothing much doing &amp; have at last got recq. through for supplies. Plenty of strafing going on with Taubs over to see what it means. 
 Monday 29th Jan. Drugs arrived at last so all is well. Report through which explains the bombardment. We had an advance yielding over 350 prisoners very good indeed. Weather still cold. Albert &amp; surroundings heavily shelled today. Divisions going through to take over more of the French front. 
 Tuesday 30th Jan Cold still continues with addition of snow, papers today stating that these are the coldest conditions experienced for 20 years. Our Christmas parcels arrived today, belated but welcome. 
 Wednesday 31st Jan 
 Still very cold. Usual duties but a little trouble in keeping up supply of ointment for the scabies. Am 