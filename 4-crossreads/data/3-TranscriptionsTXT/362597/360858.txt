 [Page 22] 
 the bun before they finished. 
 I slept up on the front deck this night. All the deck space is covered with canvas now, &amp; although it keeps the sun out it does not keep out the rain. When the rain came down last night everybody scuttled for their quarters. I did not. I got in the alley way on the lee side &amp; it was quite cool &amp; dry. 
 At this time we are within 5 days sail of the line &amp; we have not had a hot day yet, of course it is hot down in our quarters &amp; stuffy too, but I do not stop there for more than 20 minutes 