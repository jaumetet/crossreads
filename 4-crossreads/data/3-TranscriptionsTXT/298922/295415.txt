 [Page 90] 
 Friday 30 March 1917 
 Very Hot. 
 [The following paragraph crossed through.]  Letters from wife, Cousins &amp; May Harrison up to Feb. 8th also Australasian from you - &amp; other Papers. Please tell MAY I can  not  write so often as I would wish, you tell her my news but with the Wife &amp; you &amp; my work more is impossible. In case we move out quickly I'll post this now. 
 The letters by Miss Hanbury Davies are hung up. No women allowed to leave Egypt at present. I do not know the Lady but Mrs. Cuth Fetherston Hough was good enough to hand them on.  Love to all, Son  
