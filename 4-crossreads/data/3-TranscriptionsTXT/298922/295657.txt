 [Page 332] 
 Monday 24 December 1917 
 The gods are good to me. I've got leave to spend Xmas Day with my little wife &amp; am off back to Cairo tonight, returning on the 26th early. Have engaged rooms for Dolly in Ismalia , as feel pretty certain of things now. Attended Lecture on Machine Guns use in Palestine, but as a sand storm was blowing could not hear much. The troops PORK was sent here  dead  &amp; has been lying half covered in sand on the Railway line since morning. Q.M.s fault of course. 
