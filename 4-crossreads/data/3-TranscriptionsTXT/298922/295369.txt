 [Page 44] 
 Monday 12 February 1917 
 TILUL  BIR-el-ABD  
 Still on the road &amp; raining hard &ndash; but all hands are happy. Passed several Catipillar Tractions pulling loads of 10 tons over the sand. Country gets more Desert like &amp; has edible bush as we go back. A man could make a do of 20,000 acres or less of Rafa country &ndash; all Bedouin Stock were fat there. 
 [The following crossed through] Letters from Mother 7/1/17 Joe 7/1/17 &amp; 1/1/17 Dolly 24/1/17 Laura 24/1/17 Papers "Australasian" &amp; papers from Mrs. Callow &amp; Punch from Mr. Ferrar. So far Mother's Housewife has  not  turned up. I am very 
