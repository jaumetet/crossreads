 [Page 133] 
 Saturday 12 May 1917 
 I have spoken before of the cleanliness of the TurksCamp all discipline. Here not a TIN or BOTTLE or Bag  can  to be seen on our arrival &amp; the whole WADI absolutely clean. All Camel &amp; Horse Manure burnt etc. &ndash; evidence of the fear of Disease &amp; German discipline I suppose. 
 [The remainder of the page crossed through.]  Also DIARY 15/1/17. Letter Mother JOE Day Mch. 25 &amp; April 1st &amp; also H.L.A. The latter asks me to bring my wife to Eli Elivah. I shall be glad to do so some day. Do you notice how  badly  Dolly  speaks  even after being at one of the best schools in England. Also Papers thank you. The old Home must be kept as well as ever. Am sorry TESS had to be shot. 
