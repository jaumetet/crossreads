 [Page 233] 
 Wednesday 5 September 1917 
 Still HOT in Cairo . 
  I am quite busy deciding what KIT to take &amp; what to leave behind. The days are Hot here but cold in England I expect.  
 Had lunch with Nowell at Camel Corps Mess &ndash; he is better &amp; goes out to his Regiment tomorrow. 
 We looked at many old photos of "Carrakoorte" &amp; other places. Nowell gave me photo of 2 young Lion Cubs he  is taking  took to England from Abyssinia the last time he went HOME. 
