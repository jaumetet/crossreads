 [Page 45] 
 Tuesday, 13 February 1917 
 BIR-el-ABD 
 Arrived here. You will remember me speaking of BIR-el-ABD last July &ndash; now a big military Camp, Pumping Station, Hospitable etc. &amp; a canteen both wet &amp; dry. 
 [The following is crossed through] 2 Officers came along &amp; offered 2 bottles of Beer (with patent costs) for some butter. After they had gone we found bottles refilled with  Water  - Assualt made &amp; Bivouacs wrecked &ndash; am afraid there will be many sore heads tomorrow, so many of these chaps overdo it when they reach a Canteen. 
 [continued from the end of previous page] sorry after all Mother's work but am afraid it went "down" a boat. 
