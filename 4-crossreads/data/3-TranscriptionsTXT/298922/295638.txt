 [Page 313] 
 Wednesday 5 December 1917 
 Another S.O.S. message came  from our rear  last night about 5 miles away, our BAR Steward is very nervous as he has been torpedoed twice  in PERSIA &amp; MOOLTAN.   We arrived however in  Malta  at 6 p.m. after going 96 miles past &amp; then circling back again &ndash; so tonight  Dolly &amp; I  we came ashore &amp; had dinner, but the Hotel tucker is not as good as on the boat.  The Papers I want to read.  
