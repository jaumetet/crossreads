 [Page 29] 
 Sunday 28 January 1917 
 Some Yeomanry relieve us today &amp; we go back to El Arish for a bit of a spell. Fortunately a Camel died so we can offer them some fresh meat!!! 
 The seasons are reversed here you know this &ndash; mid winter now &amp; cold at night but lovely days. Outposts are cold work but we like to think that the Light Horse are always in the front line &amp; have been for 1 year &amp; 9 months, beyond a months spell while I was away in England. 
 Bedouin implements are even more crude than Egyptians &ndash; Harrow &ndash; piece of wood with stone pegs &ndash; Wooden Plows etc. The well we are using is one of 7 between here &amp; Palestine put down by ? don't know how many years ago. 
