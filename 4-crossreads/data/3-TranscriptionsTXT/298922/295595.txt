 [Page 270] 
 Thursday 11th October 1917 
 On board Train again. Left  LYON  at 11.30 a.m. All money is paper here even down to 1 Franc piece = 8D or 9D. Each Municipality issues their own War notes. Here we saw a delightful smash along a picturesque canal with tow boats. Still in the train tonight. France is very pretty just now, apple trees loaded with fruit &amp; the Hawthorn hedges red with berries as we travel slowly on out of the way lines. We see the countryside mostly. 
