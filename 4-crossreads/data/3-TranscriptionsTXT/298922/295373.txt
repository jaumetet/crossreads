 [Page 48] 
 Friday 16 February 1917 
 *The New Chums" 
 The Yeomanry left a few decent Papers among them. I found a 'BULUWAYO' Times, mentioned many places I camped in in RHODESIA, so when I leave the 'Deniliquin Independent' &amp; Walgett Spectator the new comers from perhaps Canada, will find some news on Australian. Our men are always  giving  barracking *the New Chums about riding. As one fellow was leaving here with Saddlery badly put on, he was hailed with &ndash; "Put the Saddle further back, your reins are too long". 
 General -  J.B. Meredith  our old Colonel visited us &ndash; I am not changing I am too fond of the old Regiment. 
