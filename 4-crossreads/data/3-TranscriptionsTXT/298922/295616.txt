 [Page 291] 
 Tuesday 13th November 1917 
 Posted notes yesterday &ndash; telling you I'm here till 19th. Laura comes at 2.30 to see us, &amp; goes Hospital tonight. 
 Brace is coming to see her &amp; she says Connie is ill too but Laura is a bit of a Hypochondriac if that spells it correctly. 
 Foggy morning my cold better &amp; the wife very fit. A luxurious life at present. 
 In Palestine several of my friends have been killed. 
