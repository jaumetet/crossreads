 [Page 315] 
 Friday 7 December 1917 
 Still at  Malta  &amp; remaining till Saturday. We had trouble in getting here &amp; it looks as hard to get away. The Egyptians some years ago had a small NAVY &amp; they set out for Malta but could not find it!!! returning to Alexandria they said, Malta "mafish" which is  gypsy for  means "finish". Visited the Church of  Bones  2500 Skulls &amp; bones of the ancient defenders of Malta are arranged in Tiers in a vault, gruesome!! 
 [The remainder of the page crossed through.] More lace today so the wife should be busy in Cairo with sewing. We have our meals ashore as Dolly likes to get away from the Steamer. 
