 [Page 152] 
 Sunday 10 June 1917 
 Going out to  SHAM  fight our Scouts put up a Jackal, I let a Troop go &amp; we ran him down in great style &ndash; the same sort of animal we used to get in S. Africa. Coming home I noticed a Bedouin child cutting up young PRICKLY PEAR &amp; feeding 2 Camels on it. The SPINES were soft but quite strong enough to stick in our mouths &ndash; yet the Camel was fat. 
 Took the Squadron to the Beach &amp; paid them there, the swim did them all a world of good -  altho' I did not go to Church Parade I thought of you little Mother.  
