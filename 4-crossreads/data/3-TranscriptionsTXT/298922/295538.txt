 [Page 213] 
 Thursday 16 August 1917 
 [The following sentence crossed through.]  A parcel from Home &amp; one from Benduck Station have just arrived, they will keep till I rejoin. 
 I am not particularly fit yet, in Egypt if once one gets down at all, it is hard to pull up again &amp; this coastal humid heat is very enervating. My blood is out of order that is the whole matter. 
 [The following sentence crossed through.]  These notes are of interest to you only so please Censor heavily if you print them. 
