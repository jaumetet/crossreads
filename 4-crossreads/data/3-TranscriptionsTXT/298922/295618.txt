 [Page 293] 
 Thursday 15th November 1917 
  Kelman dinner  Took seats for Dolly &amp; self at Royal for Billeted. 
 Had lunch at the Club &amp; took rest of luggage to P. &amp; O. Office. 
 Laura doing well &amp; she has lots of friends who have taken her Flowers &amp; Fruit. We all hope she will be stronger after this. 
 Foggy days &amp; nights now. 
 Have read your letter of 23/9/17 to Laura &amp; also one of Aunt Lowes. I hope you will go to Melbourne at Xmas again. 
