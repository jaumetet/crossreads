 [Page 323] 
 Saturday 15 December 1917 
 Arrived Port Said &amp; caught train for Cairo arriving Heliopolis Hotel midnight,  Dolly knocked out, but safe &amp; all luggage safe too. Cabled England &amp; Australia safe arrival also wired Regiment, Mrs. Davis a friend waited up to see Dolly &amp; sent Hotel car all very kind.   Much Regimental news &ndash; many killed &amp; many decorated a certain amount of regret missing it all. 
