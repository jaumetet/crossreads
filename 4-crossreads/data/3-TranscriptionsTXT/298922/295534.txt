 [Page 209] 
 Sunday 12 August 1917 
 Did not go Church too Hot &amp; my feet are cut by the rocks while bathing so read &amp; wrote letters. I hope they reach their destination but one feels many don't these days. 
 Meetings are held to WIN the WAR here &ndash; but hot air a lot. A Soldier's life is now one of WORK, WORK  little praise  &amp; much toil, &amp; men who could will not enlist.  Many Englishmen are in good Government billets here. I went to the UNION Club for lunch, it was crowded with able men in mufti.  
