 [Page 269] 
 Wednesday 10th October 1917 
 The French are most enthusiastic &amp; friendly  not like the south of ITALY . I drank VIVE (a France in Vin Ordinaire with some French Officers at  Aix la Bains  a pretty spot near a Lake  Lac de Bourget  - it is autumn here &amp; the foliage is like Macedon. Some Boches were on a platform under guard. Every child woman &amp; man of France between 8 &amp; 80 years of age is working. Arrived  St. Germain  au Mont d'or on Rhone &amp; stayed the night at Hotel Restaurant du Sport &amp; it was "du Sport". 
