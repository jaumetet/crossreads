 [Page 263] 
 place called Grandsart. Firdie Kilier is here also. There were 9 of us in the van. Owing to the ravages of the influenza epidemic all leave has been cut out for the present. 
 1548 Wednesday Oct 30 1918. A fine day. The doctors decided to send men on to Abbieville today. I left at 2 &amp; arrived soon after 3. I went as a stretcher case. 
 Thursday Oct 31 1918. 1549. A quiet day in the 3rd Australian General hospital. 
 This is a large hospital &amp; there are many nurses. German prisoners scrubed out the floor of this ward today. 
 Friday Nov 1 1918. 1550. A fine day. I am too ill to write any letters yet or even to post away some Christmas P. Cards. Turkey has surrendered unconditionally. 
 Saturday Nov 2 1918. 1551. A fine day but cloudy. I feel much better today. German prisoners scrubed out our ward before breakfast this morning. 
 Sunday Nov 3 1918. 1552. A dull cloudy day. 