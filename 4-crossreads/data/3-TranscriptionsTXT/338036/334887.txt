 [Page 45] 
 HUNS SHELL PONT NIEPPE [20th Mar. contd] Warren.  It was bitterly cold &amp; raining all day. 
 Wednesday 21/3/17 The Hun sent a H E shell (5.9) into Pont de Nieppe just about dusk to night  killing 4 soldiers &amp; 3 civilians (inc 2 children)  Our cars &amp; boys brought them in.  Two lived for a couple of hours only.  I visited the scene &amp;  observed that windows in all the surrounding houses were broken &amp; a large hole was made in footpath. 
 Thursday 22/3/17 Was on guard all night.  Very cold &amp; snowing slightly.  More good news. 
 Friday 23/3/17 Still good news from front (Ancre &amp; Somme.  Guard duties continue all day. 
 Saturday 24/3/17 
 SOME SHRAPNEL 
 Very good incitement [?] this morning which however ended in one of our boys being a casualty.  As is customary every morn our Section does physical culture to keep fit &amp; this morning we were on this routine in the courtyard when a Fritz plane came across &amp; sighted us.  Consequently in about &frac12; hr (we were still at culture) whizzing over came a shell &amp; we were soon on our stomach.  Never worried much &ndash; thought it was a stray &amp; went on again why again &ndash; this time she burst in air not far away but we had taken every precaution &amp; no one was hurt.  We then received orders to get inside the billets &amp; the Hun kept us his shrapnel all the morning. 