 [Page 7] 
 Sunday 19th [Nov] When we awoke it was to see a beautiful white mantle over everything and it was a sight I shall always remember.  We made snow men and had snow fights until our hands &amp; feet were numb and wet thro.  During afternoon the snow eased off and Sam [Crook] and I went for a walk to Durrington. 
 Monday 20th Raining.  Snow almost thawed by now..  Orderly Room. 
 Tuesday 21st Route march in morning.  Telegram from Ernie. [His brother] To Amesbury at night &amp; posted letters (7).  Received gas &amp; trench helmets and goggles 
 Wednesday 22nd [indecipherable] 
 