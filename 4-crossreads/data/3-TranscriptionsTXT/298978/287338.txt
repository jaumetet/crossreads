 [Page 34] 
 Mar: 10 Manoeuvres.  For the first time after the many hard marches I have done, I fainted  - very amusing thing; I fell down just as is I had been shot; my companions, who were only a few yards ahead of me, within easy speaking distance, did not miss me until they had gone some considerable distance. I wonder how I'll get on when I am faced with the real hardships of War -  I casn only hope that I don't blow out before I have at least done a little. 
 Mar: 10 Studies at the Caf&eacute;. 
 Mar; 11 Flag drill &ndash; kit inspection &ndash; studies. 
 Mar; 12 Manoeuvres &ndash; studies. 
 Mar; 13 Visit Cairo with my friend Bell. 
 Mar; 14 Studies.  Visit friends in Cairo; they say they are French but they cannot speak one word of the language and in every way are obviously Egyptians. 
 Mar; 15 A great event!  I am given three days C.B. for refusing to attend that farcical absurdity Church Parade. Strike camp. Studies intermingle with my first experience of the "Angel's Whisper". 
 Mar: 16 Bivouac &ndash; manoeuvres &ndash; and C.B.  All the camp are delighted to hear that Silas has C.B! &ndash; the lads are immensely amused and come along to see me do it; if it had been pack drill I think they would have paid a pound a head for front seats. 
