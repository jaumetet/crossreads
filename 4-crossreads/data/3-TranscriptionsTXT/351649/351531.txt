 [Page 13] 
 are all attatched to the calvalry:  the latter are never in the lines except occasionally dismounted. 
 The batteries move up now and again to practice shooting:  I seem to be permanently attached to this one;  it has not been in action since I joined it;  more or less firing goes rumbling on every day, and when you are located close enough up to the lines you can ride over and visit any friend you know and watch them shooting:  the opposing sides see but little of each other, but incessantly annoy each other with bombs, bombing parties at night, mines, and a few shells dropped wherever you think they will catch someone changing guard &amp; so on. 
 Our turn will come when we break the German lines. 
 The procedure consists of, (1) wire cutting done by shrapnel fire from field batteries, (2) Infantry attack to take their trenches, (3) pushing as many troops through the gap as possible, headed by cavalry &amp; horse artillery:  this is the general idea of trench warfare attack. 
 A more monotonous lazy life than having a 'dug in' battery in action, I don't know.  Certainly as reserves, one has more comfort and more of interest to do. 
 Attacks now-a-days are ponderous and take months to prepare &ndash; so that the other side knows of it long before it comes off:  however lets hope the Huns are fairly rolled up this year. 
 The country here is gorgeous now:  two weeks hot weather has brought out all the leaves, &amp; the grass has grown tremendously. 
 Goodbye &ndash; with best luck to John &amp; Graham. 
 Ever yours sincerely Jock C. Ellis 