 [Page 30] 
 15th Sept 1916 Signalling. Cold day but sunny. Walked down to Durrington to see Perce, but did not see him. Received photo from Millie. 
 16th Sept 1916 Lectures Heliot Lamp. Received letters Rana &amp; Wyn. Wrote letters Aust. mail also to Millie &amp; Cousin Kate. Went down to see Perce. Met him &amp; went to Durrington. Had a beer at Plough Inn &amp; afterwards went to concert at YMCA. Came back &amp; had drink at canteen. Said Goodnight and came back with boys 9.45. 
 17th Sept 1916 Sunday today. Cooks fatigue. Did not go out. Wrote letters for mail. Perce came up to the hut. Talking for a long while then we went over to the canteen with the boys and had a [indecipherable], afterwards we soon went away to huts. 
 Here endeth this epistle of joys &amp; sorrows of some months passed from 11th May 1916 to 17th Sept 1916 Norman Edmondson serving with the A.I.E.F. Field Artillery 7th Brigade  18/9/16 
 [Transcribed by Lyn Williams, Adrian Bicknell for the State Library of New South Wales] 
