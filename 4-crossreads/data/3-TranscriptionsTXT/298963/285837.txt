 [Page 14] 
 to take any risks for a night's sleep. 
 Snow falls heavily &amp; all the whispering pines of the adjacent woods are covered with fairy flakes of snow, giving a picture of rare beauty.  All the fields for miles around are white. 
 12th Rested in same "posie" as previous day, rested &amp; enjoyed our rations. 
 13th Recd. orders to move down to R.A.P. of 3rd Batt.  Arrived there &ndash; "nothing doing" so spent day in "digging in" &amp; making a comfortable home &ndash; finished at dark &amp; were having tea when orders came to move off in 10 min.  Proceeded under cover of darkness to a chalk pit 200 yds. behind our front line, "dug in" &amp; spent a quiet night. 