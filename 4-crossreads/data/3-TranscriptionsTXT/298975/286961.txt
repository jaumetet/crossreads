 [Page 80] 
 spent the evening at Linfield Gardens. Had quite a good air raid or rather several 
 Sunday 30th Caught 9:15 to Harwich &amp; met Mab on the Station Cycled out a few miles &amp; had tea in evening. 
 Monday 1st Oct Went shopping in Harwich in morning Cycled out for tea again to a very pretty wood Have learnt quite a lot about Submarines Etc Air raid in evening. 
 Tuesday 2nd Caught 10 AM train to  Victoria  Liverpool St Messed around a bit &amp; caught 6 PM for Dinton in camp by 9-30 after a very pleasant 6 days. Fine wether every day we were on leave 