 [Page 59] 
 until 9 pm. 
 Nov 14th Tuesday 
 Misty &amp; cold day. Very glad am not in camp or at the front. Still waiting for a letter from home. A few games of billiards &amp; usual resting about. Another good concert by Welsh Guards, who are in camp near here. 
 Nov 15th Wednesday 
 Beautiful day, first sunshine. Very little to do by eat, sleep, &amp; be merry. No further news as to what is likely to happen. Horrible concert by some amateurs. 
 Nov 16th &ndash; Thursday 
 After so long waiting the long envelope arrives 