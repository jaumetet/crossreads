 [Page 30] 
 nerves and I was glad to get away from him. 
 Went ashore again after dinner and went to the Zoo and Gardens which are both very well laid out. Went to tea at the invitation of some gentleman in the tram and passed a very pleasant evening before going back on board. 
 Sunday 8th All up early, hoping that the loading apparatus would break down and delay the departure but no such thing occurred. 
 Thirty 
