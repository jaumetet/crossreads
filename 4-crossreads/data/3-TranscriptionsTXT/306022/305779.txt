 [Page 102] 
 Sunday 19th Snowed all day yesterday and last night. About 6 inches everywhere this morning, but nearly 12" on hill opposite. No church parade. 
 Went over to hill in afternoon with sheet of galvanised iron, and did some tobogganing. One chap fell over edge of quarry at bottom of hill and broke arm. Had great fun with snowballs on top of hill and then slid down to the bottom on our haunches at a 'hell' of a speed. Then 
