 [Page 69] 
 drifting past. 
 Wednesday 1st Nov Sea a bit calmer and during the morning we picked our escort up and passed dozens of destroyers on patrol duty. They told us there had been a submarine in the vicinity which was the reason for the anxiety yesterday. 
 On guard again, for the last time as we reach England tomorrow morning. 
 We were disappointed that land was not seen 
