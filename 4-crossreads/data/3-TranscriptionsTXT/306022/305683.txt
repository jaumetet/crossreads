 [Page 6] 
 Wednesday. Sept. 13th Left camp early to march to station through crowded streets, and got any amount of fruit etc from the people 
 Had a good trip down in the train though a bit crowded, with all the kits and arrived in Sydney about 3.30 pm and though very hot &amp; tired, we had to march to the barracks. As some AMC men were late we had to stand to attention in the sun for 25 mins, and all cursed the day they had enlisted. After inspection by Gen. Rammaciotti we were marched to the Show Ground &amp; shewn our quarters for the night. 
 Thursday 14th. Marched off very early. Met Mum at Darlinghurst. Went on 
