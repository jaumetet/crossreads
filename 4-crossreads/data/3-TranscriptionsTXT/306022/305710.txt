 [Page 33] 
 Tuesday 10th Land sighted again though only for a while. The speed was decreased today so that she won't get to Cape Town too early and have to anchor outside the bay all night. 
 Wednesday 11th Got outside Table Bay at about 1 am. Very gusty wind blowing which causes some movement (&amp; language) on the part of those sleeping on deck. As mine was slung up I chose to lay there and 
