 [Page 81] 
 a guide book however and found that the town is very ancient and has an important history with many antiquities well worth an inspection. 
 Saturday 4th No drill done yet, which is just as well for the food is rather light, though good. 
 Couldn't send a cable home as the wires to London were damaged by the bad weather. 
 Dull and wet today However after dinner my chums &amp; 
