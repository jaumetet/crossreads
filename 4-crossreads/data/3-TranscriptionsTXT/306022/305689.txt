 [Page 12] 
 to the sun. One old beggar in particular always says them with one eye on the meal and as the others get in for their whack, he gets quicker and quicker almost falling over himself when bowing down to the floor a few times in succession. 
 Though continually dipping their face, hands &amp; feet into water, I think that is as far as washing goes with them, for the stench from them and their quarters is absolutely unbearable at times, when the wind gets astern. 
 At certain intervals all their belongings are brought 
