 [Page 206] 
 There is really very little more news here except that the swim we get every day in the Canal is very acceptable. There is also an excellent place to swim the horses on the shores of Lake Timsah. One must have something in the way of a swim to counteract the effect of the "Khamseens" which are blowing here nearly every day and which are the worst we have met in Egypt. The sand here is feet deep and it does not take much to start a good old sand storm going. 
 Well Father must say goodbye for now. Hope it will not be long before I am writing you from "La France" &ndash; Anything to get away from this Sand &amp; niggers. 
 Love to all from Your affectionate Son Terence (over) 
