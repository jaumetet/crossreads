 [Page 304] 
 [The first line at the top of the page regarding the date is written in a different hand in ink, and initialled] Date evidently should be 7.5.17 - Inadvertently figure 4 used instead of 5. WMNG 
 France 7.4.1917 My dear Father, 
 Am snatching this opportunity during a lull in proceedings to reply to your delayed but still welcome letter of Feb 19th &ndash; readdressed from Exeter &ndash; 
 No doubt you will read of events happening here about this date, and consequently understand if my letters are rather short and scratchy &ndash; Am in  a cellar at present in  much battered village above which is my observation post for the scene of operations -  We have been battling for 4 days now for a small sector of the Hindenburg line, which being a vantage point for the Hun, is not easily taken. Our Batteries have all had 
