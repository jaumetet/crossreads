 [Page 267] 
 to France  till  about the 26th of this month &ndash; I am trying to arrange to spend Xmas here and will cable you with the glad news if I am successful. Sis is very much afraid they will send me back before Xmas and wants to hang the whole Army, but I think I will be with them for the festive day. Address me as usual 11th Bgde &ndash; Must stop now as its bed time &ndash; 
 With fondest love to yourselves and Pat Your loving Son Terence 
