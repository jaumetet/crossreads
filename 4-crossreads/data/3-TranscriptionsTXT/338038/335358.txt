 [Page 333] 
 "Cats" (continued)   2nd page 
 The popular belief about  of  our furry friends having nine lives seems to have a very solid foundation = 
 Our old war worn horses are revelling in this grass country after the Somme mud although the last month put a lot of condition on them &ndash; My own two look a picture &ndash; The little brown horse is shining like velvet in fact the men have christened him "Velvet" while the big bay is full of beans &amp; is not above getting his head between his forelegs if I am unwary &ndash; I have several times tried to get you a photo of the pair of them but as you know cameras are "tabooed" and even professional photographers 
