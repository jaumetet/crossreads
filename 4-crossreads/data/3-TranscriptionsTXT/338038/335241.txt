 [Page 216] 
 France 27.6.1916 My dear Father &amp; Mother, 
 This is my first letter from France so I have a fair amount to talk about of course but am afraid I must condense as one does not get a great deal of leisure in these parts. 
 We landed at Marseilles early in June &amp; entrained there straight away for Northern France. That 3 days train journey I shall never forget! The first day's run through Southern France was absolutely like Heaven after coming out of the sand. Everything of course is beautiful just now, green everywhere, &amp; the most beautiful shades imaginable. Every inch of the country 
