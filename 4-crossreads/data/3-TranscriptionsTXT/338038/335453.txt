 [Page 428] 
 [URGENT TELEGRAM -  NSW &ndash; dated 15 April] Vict Bks [indecipherable] Wilcoxon Greenwich Major T W Garling 10th Field Artillery Brigade late Divisional Ammunition column died of wounds &ndash; enquiring date please inform Mother Mrs W Garling Burns Bay Road Longueville. Colonel Sandford 
