 [Page 370] 
 If necessary for further enquiry regarding T.W.G's increased allotment &ndash; communicate with X District Paymaster Victoria Barracks Paddington 
 7th/11/17 Note &ndash; Staff sergeant Brown &ndash; seen by W.M.N.G. [Garling's Father] re matter of Terence's increased allotment &ndash; No advice received at date by military &ndash; they will notify Moore St (over) 
