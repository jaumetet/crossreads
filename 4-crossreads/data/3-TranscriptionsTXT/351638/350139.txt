 [Page 140] 
 [Handwritten notes continued]  (17)  Suggest that the Colonial Troops be taken off the   Peninsular &amp; thoroughly rested. How their morale will suffer if they are left at Anzac throughout the winter.  (18)  State at end why the letter has been written.  How critical it is the truth should be known &amp; how the staff are keeping back the truth [from?] the government. 
