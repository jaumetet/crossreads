 [Page 49] 
 Sunday 6 February 1916 
 Another beautiful morning. In the afternoon the breeze freshened a little &amp; the sky was overcast. There was a small amount of motion &amp; a shower of rain. 
 Attended Holy Communion in the morning instead of doing physical drill: it was a fine service, the last, I expect, before landing, &amp; there was a splendid attendance of about 120 soldiers. 
 After breakfast the usual Church Parade &amp; Commander's Divine Service were held together at 10.15, the organ being taken along to the end of the 2nd deck where all the concerts etc are held. The Commander &amp; chaplain read parts of the service between them, the Purser read the Scripture lessons &amp;  the chaplain gave a short address on 1 Sam. IV? "Be strong, quit you like men, &amp; fight", &amp; was well listened to by all. 
 The spare time during the day I spent in letter-writing. Towards evening the wind blew rather strongly from the N &amp; there was a good deal of motion, - sufficient to send some of the passengers down. 
 Ship's run to noon 360 mls. 
 Monday 7 February 1916 
 A clear fine day with a smooth sea. Passed a fairly large steamer in the distance during the morning. 
 Parades held as usual. In the evening the finals of the boxing tournament were fought &amp; attracted much interest. Several rounds of the chess tournament were also played &amp; proved interesting games. 