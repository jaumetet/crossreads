 [Page 20] 
 Monday July 2nd 
 Bapaume : cloudy : reveille 4 am : usual parades : we steered clear : after tea Alf &amp; self had a long walk along the Arras road &amp; back on the Cambrai road : watched an observation balloon being hauled down to change relief : the observers were both officers who have "done their bit" &amp; are unfit for general service : one had a wooden leg : gas alarm about 11.30 pm 
 Tuesday July 3rd 
 Bapaume : fair day : reveille 4 am : Alf &amp; I got passes to visit Pozieres : walked from the "Butte" overland through Martinpuich : still smells &amp; plenty of evidence as to the Hell that it was : passed over our old positions &amp; down to the Death gully cemetery : got one of the 17th Batt. Cross &amp; a couple of other things : All the different 