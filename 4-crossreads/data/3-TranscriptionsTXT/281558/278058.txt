 [Page 72] 
 welcome &amp; are real "cockies" : they travel little &amp; don't know much outside their local districts : tired so slept well 
 Thursday Nov 8th 
 Weston-S.M. : up about 9 am : showery : stroll in morn &amp; afternoon : walk after tea &amp; had a good time : 
 Friday Nov 9th 
 Weston-S.M. : caught 11 am to London &amp; went to Bank : money O.K. : watched the Lord Mayor's turnout &amp; it was pretty good : concert in the Aldwych after tea &amp; very good : the Y.M.C.A. have certainly made a home away from home there 
 Saturday Nov 10th 
 London : caught 9 am to Leicester &amp; went round to my friends : fine people &amp; made me very welcome : to cold &amp; damp to go out so spent pleasant evening talking by the 