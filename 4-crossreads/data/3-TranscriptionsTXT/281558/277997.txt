 [Page 11] 
 home about 1 pm : wrote in afternoon : "fun" night by the Padre after tea : 
 Sunday June 3rd 
 Havre : fine day : reveille 5.30 am : church parade 9 am : H.C. afterwards &amp; was very nice : about 70 present : band in afternoon &amp; plenty of French civilians as an audience : fine service etc after tea : lecture by Prof Paul. 
 Monday June 4th 
 Havre : good day : reveille 5.30 am : "bull ring" all day : open musketry &amp; live bomb throwing : lay down after tea 
 Tuesday June 5th 
 Havre : hot day : reveille 5.30 am : "struck" tents &amp; bath parade in morn : washing &amp; paid in afternoon : wrote in afternoon 
 Wednesday June 6th 
 Havre : reveille 5.30 am : "bull ring" all day : drill &amp; march past : company drill etc : 