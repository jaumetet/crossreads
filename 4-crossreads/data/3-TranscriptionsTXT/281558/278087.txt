 [Page 101] 
 Wheatley here : 
 Saturday Feb 16th 
 Harfleur : fatigue in morning : warned to leave tonight for the 35th coy : paraded about going to the 60th but done no good : left about 6 pm &amp; entrained at Le Havre : 
 Sunday Feb 17th 
 Rouen : arrived in morn : leave in afternoon from 1.30 pm &ndash; 5 pm : fine place &amp; we had a look through the Cathedral &amp; also went out to "Bon Secours " from which a glorious view of Rouen &amp; the surrounding country can be had : entrained at night : 
 Monday Feb 18th 
 Train : arrived Peronne in morn &amp; went into our camp : very homely &amp; almost civillian comfort in comparison to the infantry : 