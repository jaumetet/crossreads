 [Page 73] 
 fire : the people over here haven't much to live for simply work all the time &amp; little wages : Leicester a fine town : sent home some views : slept at the Y.M.C.A. 
 Sunday Nov 11th 
 Leicester : met my friends &amp; seen most things worth seeing in the town : went to church at St Marys which is a fine old church : old Roman wall &amp; other sights very interesting : walk to Western Park after dinner &amp; talk after tea : slept at Y.M.C.A. 
 Monday Nov 12th 
 Leicester : caught 1 pm train to London : strolled about &amp; went to the Coliseum at night : excellent programme &amp; Ellen Terry came in for a fine applause : marvellous actress in spite of her age 