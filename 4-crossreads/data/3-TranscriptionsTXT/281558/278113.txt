 [Page 127] 
 Saturday May 25th 
 Dunkirk : nice stroll in morn to Rosendale &amp; afterwards through the market square : worked 2 &ndash; 10 pm : 
 Sunday May 26th 
 Dunkirk : on from 6 &ndash; 2 &amp; then went for a stroll : met a couple of Frenchies from the depot so strolled round with them for a few hours : had a good time : 
 Monday May 27th 
 Dunkirk : on from 6 &ndash; 2 : Fritz' plane over in afternoon but very high up : stroll after tea : 
 Tuesday May 28th 
 Dunkirk : 
 Wednesday May 29th 
 Dunkirk : on duty in afternoon &amp; night : 
 Thursday May 30th 
 Dunkirk : on duty 6 &ndash; 2 : stroll to aerodrome after tea &amp; watched the "Handley 