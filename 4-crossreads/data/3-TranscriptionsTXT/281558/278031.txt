 [Page 45] 
 Sunday Sept 2nd 
 Monasterie : showery : reveille 7 am : bath parade to Arques in morning : wrote in afternoon : taube dropped bombs 
 Monday Sept 3rd 
 Monasterie : reveille 6.30 am : boys on parade : our A.M.C. day so we had a lecture : after dinner Morrie &amp; I went to St Omer &amp; had a good time : Catherdral very nice : we went &amp; seen the effects of last nights bombing &amp; the street is shook up a bit : pathetic to see the women with what furniture they still had left : more taubes over after tea : plenty on our 'planes up too : heavy gunfire up the line 
 Tuesday Sept 4th 
 Monasterie : reveille 6.30 am : fell in &amp; went for a wood  "stunt" in morn &amp; "encored" in afternoon : marched home in gas helmets for practice : went eel fishing after tea but only got 