 [Page 141] 
 Pal of Mine &ndash; continued 
 A rough hewn bit of limestone on the spot where Ginger lies Bears a proper soldiers tribute &ndash; "a brave man never dies" &amp; I hear the sentnel saying when the judgment story's told All's Well Pass little comrade to your dugout made of gold 
 Where the wailing croon of Shrapnel plays a requiem o'er you head In the slime of muddy Flanders where the bravest's  fought &amp; bled I'll go looking for you Ginger when I've crossed lifes firing line &amp; wether hell or heaven may I meet you Pal o mine. 
 Mdme Gressier Gressier &agrave;Westbecourt par Lumbres Pasd Cal 
 Monsieur Jules Devuldes Cultivateur Claimarais 
 Mr Albert Rowe Hollowell's Dairy Cricket St Thomas Nr Chard Somerset 