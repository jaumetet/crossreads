 [Page 118] 
 registered by the G.R.V. as permanent memorial graves : lorry back to Poperinghe &amp; strolled down the street then lorry to Abeele &amp; train &amp; home : got back about 6 pm pretty tired out. 
 Thursday April 11th 
 Audruicq : nothing much doing wrote at night 
 Friday April 12th 
 Audruicq : nothing doing at all, went to theatre after tea &amp; it was a very good show 
 Audruicq : Saturday April 13th 
 On fatigue during day : stroll at night &amp; watched the football match : our chaps won 5 &ndash; 3 : 
 Sunday April 14th 
 Audruicq : rotten day so I stayed in wet &amp; very cold 
 Monday April 15th 
 Audruicq : on gardening fatigue 