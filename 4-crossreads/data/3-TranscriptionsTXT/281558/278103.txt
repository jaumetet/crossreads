 [Page 117] 
 A.S.C. chap &amp; they are deeply dug in &amp; quite comfortable : stroll in afternoon along the canal &amp; it was very nice : leaves &amp; flowers are budding quickly &amp; in a couple of weeks time it will be glorious : band in the square at night &amp; very nice 
 Monday April 8th 
 Audruicq : nothing much doing : stroll after tea 
 Tuesday April 9th 
 Audruicq : got my pass, crosses, etc &amp; went on leave to Ypres : travelled all night 
 Wednesday April 10th 
 Audruicq : arrived near Ypres at daylight &amp; walked out to "Hell fire corner" : plenty of shells &amp; gas so I went back to Belgian Battery Cemetary &amp; erected the crosses there &amp; they are now 