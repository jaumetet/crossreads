 [Page 25] 
 [Card] Dymock's Building 68 Elizabeth Street SYDNEY 
 25/4/18 
 Dear Madam, 
 I have to acknowledge with many thanks receipt of your parcel, containing 46 prs of socks  13 prs socks  (from Mrs. Martin Smith)  3 Balaclavas  &amp; 1 muffler 
 Yours faithfully, "WAR CHEST" SUPPLY DEPOT, [Signed] J. M. Antill Hon. Superintendent Per  [indecipherable] 
 [Transcribed by Val Ridley for the State Library of New South Wales] 
 