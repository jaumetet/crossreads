 [Page 290] 
 Saturday 21 Sept. Morning as usual. Afternoon 11th Bde sports close handy. Attended &amp; enjoyed the fun. Good afternoon. 
 Evening &ndash; the whole tent visited the Cooees at Doingt. Had good time. Supper &amp; then to bed. 