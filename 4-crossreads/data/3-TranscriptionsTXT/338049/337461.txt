 [Page 277] 
 Sunday 8 Sept. Went out to the scene of yesterdays operations &amp; found Milligans tunic. Picked up Hendersons plyers &amp; knife. Henderson not too good according to reports.  Returned  Took private gear to Major. 
 Ordered back to Clery scrounging cable. Returned via Peronne. Made camp at old lines. Pleasant supper &amp; to peace peacefully. 