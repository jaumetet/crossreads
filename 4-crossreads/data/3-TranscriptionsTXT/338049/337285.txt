 [Page 101] 
 Saturday 16 March Moved off to Nielles for Boulogne but was advised that I was not to go to B. but was due for Blighty tomorrow. 
 Returned to Vaudringhem &amp; arranged things but on returning to Nielles was advised that I would not go until day after tomorrow. 
 Writing evening [Chanc?] Jack King in Boulogne. 