 [Page 267] 
 Thursday 29 August Had tea on arrival home. Slept in until about 10.30 am breakfast being brought into the tent. Had an easy morning. 
 Move ordered immediately after dinner for 2 pm. Move via Bray, Cappy to Suzanne &amp; camped near Chateau. Made ourselves comfortable. 
 Then I went in box car to Curlu to take cable forward to Page. Shelling heavy. Great ride. 