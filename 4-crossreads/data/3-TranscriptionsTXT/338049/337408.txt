 [Page 224] 
 Wednesday 17 July Slept in (picket last night). Stables as usual. 
 Difficulty about tents. Finally had to hand in 2 bell tents &amp; 2 covers. Acquired huge artillery cover and put up lean to. Had 6 chaps extra on a/c of rain. 