 [Page 301] 
 Wednesday 2 October  Morning easy  Morning out to try to get Bony at last. Got the wagon to 10th Bde then along road to near junction road &amp; Bony. Pulled cable up the hill to 11 Bde leaving wagon in cutting. Heavy harrassing fire. Finally finished &amp; returned via Hargicourt. 
 Afternoon easy. 