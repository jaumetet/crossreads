 [Page 72] 
 not seem to be able to think the same thing twice these days.  It is hard to say who is responsible for the conglomeration of orders which are received.  these days  
 The 3rd Aust. Div. went through our Div. last night and captured St. Denis Wood and the high ground around and beyond it. 
 10 A.M.  Our guns seen  rus  rushing up this hill early this morning. 
 Big columns of black smoke have been rising up over these hills for some days past.  The enemy is reported to be burning dumps of ammunition and other material which he considers he will be unable 