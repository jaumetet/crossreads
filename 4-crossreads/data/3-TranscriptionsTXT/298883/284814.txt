 [Page 58] 
 predominant, which is very inspiriting to troops moving up to the attack.  The enemy shelled our area with light and heavy guns without doing any harm.  His retreat is a very masterly and systematic one. 
 23 stalwart huns of the Alexander Regt. 2nd Div. gave themselves up this morning early.  They were very pleased at being out of the row. 
 Transport difficulties present themselves now that we are back in the Somme battle ground and  we  these difficulties will increase as &amp; when the bad weather begins. 
 3 p.m.  The enemy shelled us with tear gas shells and we had to move to higher ground. 