 [Page 173] 
 9th The papers all say that we are to have the dinkum oil about the armistice on Monday morning at 11 o'clock as General Foch has given the Germans 72 hours in which to decide as to whether they will accept our terms or not. 
 10th Great excitement in the village.  The fate of many lives is being decided by the hun slaughter mongers. 
 11th The Huns have been reasonable for once and have accepted fully the terms of the Allies for an armistice. 
 It is the end.  Flags were hung in 