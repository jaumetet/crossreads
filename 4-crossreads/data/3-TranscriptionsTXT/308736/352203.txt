 [Page 65] 
 Thursday April 3rd Up at 8 a.m.  Had breakfast and on the road 8-30, had to walk to Mettet (14 kilos).  A glorious morning but felt very tired on arrival.  Came through several small villages.  Got 7 letters, one from wife. Three weeks away from Battery.  Billited with some very fine people.  Had to dress for tea.  Had some music. 
 Friday April 4th Old cooker sent away.  14 new men in battery.  After tea music &amp; songs. 
 Saturday April 5th Another glorious day.  Did some washing first for a long time. 
 Sunday April 6th A small draft went away.  Wrote letters all afternoon, went to church. 
 Monday April 7th More washing.  Glorious morning.  Heard some French songs.  Rained during the night. 
 Tuesday April 8th Had the afternoon off, had my photo taken with Harry. 
 Wednesday April 9th After tea went for walk round Mettet and fields, saw a lot of burnt Aeroplanes (Fritz), got Photos. 
 Thursday April 10th Name read out on draft for Sunday, handed in pay book. 
 Friday April 11th Got my movement card and pay book, had to fill in a Q.I. form.  Could sell lots of gear if I had it.  Got letter from Kaffir. 
 Saturday April 12th Up early getting ready, put our kits on the square. The battery left for Acoz at 9-30 (Marched).  The draft didn't leave until 1-30, after reaching station had to wait 1&frac12; hours for train.  Reached Charleroi at 6.30 [?] marched into camp.  Came a gutzer, detailed as cook for the mob. Went into town and had a look round, went to picture show. 
 Sunday April 13th Up at 6 a.m. getting breakfast.  9-30 Parade, inspection by 