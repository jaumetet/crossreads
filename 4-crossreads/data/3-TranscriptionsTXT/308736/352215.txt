 [Page 77] 
 kept awake by Marine telling of his experiences in Africa. 
 Monday May 12th Arrived Kings Cross 7 a.m. had breakfast and clean up, then went to Finsbury St., by sub and saw about box of souvenirs then went to Waterloo by bus.  Got train to Salisbury 11 a.m. arrived in camp 2-30 and handed in pass.  Got blankets and kit and moved into new hut. 
 Tuesday May 13th A glorious day.  Parade 9-30, very few on parade.  Was put on Sanitary fatigue, but didn't have to do any thing, as no-one would own us.  Got a big bundle of letters (20, three from wife).  After tea went to Concert in Y.M.C.A. 
 Wednesday May 14th Parade 8-45 &amp; 10 oclock, Medical inspection and inoculation.  Got Parcel from Juniper Green.  Afternoon wrote letters and then heard lecture on the Eastern Question.  Evening went to Pictures in  Institute.  A very warm day, real summer. 
 Thursday May 15th Muster parade 8 a.m., 10 a.m.  Got our pay books back.  Put in an hour listening to the band. 
 Evening went to Band Concert at 7, and Vocal Concert at 8. 
 Friday May 16th My turn for Mess orderly.  10 oclock pay.  2 p.m. drew our leave ration money (1-4-6). 
 Sent away Note to Stockwell &amp; Co. 
 Saturday May 17th Glorious weather.  Nothing doing all day.  Evening went to Recital by Alexander Watson. 
 Sunday May 18th After breakfast went for a walk through Axford to the woods, spent a pleasant morning walking under the trees listening to the birds.  Evening, walked all round Australia.  Got letter 56 from Wife. 
 Monday May 19th Weather still warm.  Parade 9 a.m. dismissed for day.  Wrote letters all afternoon.  Went to Pictures, then to Debate.  Saw a humorous affair, a goat kissed an Aussie while he was reading the paper. 