 [Page 339] 
 [Tuesday 5 December 1916] 
 Inspecting our Mess supply this morning I noticed the Beef came from the ARGENTINE Word has just come from Regiment for me to go very close to Al Arish you will see it on Map.  I am writing &amp; posting this from Moascar near Ismalia- I am claiming 25/-/- from Egyptian Railways for lost Kit Am letting [indecipherable] tonight. 