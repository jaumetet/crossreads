 [Page 266] 
 [Saturday 23 September 1916] 
 Went to Ireland from Fleetwood by night boat- Packed up &amp; got room for Laura for Oct 2 our wedding day Ill send you a Cable when the deed is done- with the one word SPLICED &amp; date- Arrived 6.30 AM at Belfast calm voyage.  These are good fast boats &amp; have only been running a short time as the service was curtailed owing to submarines etc:- 