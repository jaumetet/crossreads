 [Page 364] 
 [Sunday 31 December] 
 Raining still Ring out the War Ring in Goodwill to all man kind!!!  I hope I &amp; my wife may be with you ere another year comes round. Did a Perish last night 5 miles from the Turkish frontier- Dug down 2 ft to the dry sand &amp; with my feet in a nosebag &amp; one blanket tried to sleep-   You will hear of another scrap in a few days- Am posting you some Stamps photos etc got from Magdhaba Turks bivouacs, with these Notes - &amp; all good wishes for the New Year-  We arrived back from stunt at sundown &amp; Iv'e shaved washed &amp; put on clean clothes for the New Year.  I handed most into Brigade but kept a few for you.  Someone may translate . 
 [Transcribed by Lynne Palmer and Trish Barrett for the State Library of New South Wales] 
 