 [Page 44] 
 Sunday 30 July 1916 Had charge parties revetting Bay Avenue.  Wiring Duck Walks, etc.  Things quiet. 
 Monday 31 July 1916 All kinds of work in hand.  News of big Russian successes.  Things quiet here. 
 Tuesday 1 August 1916 Things as usual.  Heavy bombardment in places along our line, also of Enemy positions. 
 Wednesday 2 August 1916 Some heavy Artillery actions on both sides.  Aereal activity also.  Things going well. 
 Thursday 3 August 1916 In charge parties revetting Avenues, etc.  Also setting out new dugouts.  Heavy shelling on Jay Post.  Things going well. 
 Friday 4 August 1916 Three Saprs. wounded today, Brice, Alta &amp; Brown.  Revetting trenches &ndash; putting in Dugouts, etc.  Things going well. 
 Saturday 5 August 1916 Things rather quiet.  A fair amount of shelling on both side.  Enemy brought down one of our aeroplanes, no serious damage. 
 Sunday 6 August 1916 Shells lobbed in the village this afternoon killing Sap. McDonald, &amp; two Infantrymen also wounding some.  Usual work revetting &amp; constructing &amp; generally [indecipherable]. 
 Monday 7 August 1916 Charge parties Revetting trenches, cleaning out, building concrete shelters.  Things going well.  One or two casualties each day. 
 Tuesday 8 August 1916 Same work as yesterday.  Sap. Smith wounded last night by M. Gun bullets supposed mortally.  Things as usual. 
 Wednesday 9 August 1916 Heavy shelling by enemy of positions at Fleur Baix.  Several casualties reported.  Progressive work as usual.  Nothing importance reported. 
 Thursday 10 August 1916 Work going on as usual, everything O.K.  A good deal of Artillery fire from both side. 
 Friday 11 August 1916 Cpl. Griggs wounded slightly, Sap. Gustephson more seriously being badly wounded in both legs, right shoulder &amp; hand. 
 Saturday 12 August 1916 Usual work, revetting, Dugouts &amp; general improvements.  Things going well. 