 [Page 42] 
 Sunday 16 July 1916 Heavy bombardment by our Guns.  Heavy reply from Germans.  We expect to charge tomorrow. 
 Monday 17 July 1916 Did not attack German trenches today.  Expect to do so tomorrow.  Bombarding enemy line effectively. 
 Tuesday 18 July 1916 Heavy bombardment of enemy line by our Artillery today.  Did not attack.  Making preparation to do so. 
 Wednesday 19 July 1916 Attacked enemy position &amp; advanced 900 yards No. 2 Sec. with 50 --- &amp; consolidated position. 
 Thursday 20 July 1916 Enemy outflanked our new position &amp; we had to retire into our old lines.  Very heavy Enemy bombardment. 
 Friday 21 July 1916 Worked last night revetting "Bromton Avenue".  Company resting today.  Casualties heavy  in yesterday's engagement. 
 Saturday 22 July 1916 Shifted Billet to Fleur Baix.  Nothing fresh going.  Things settling down. 
 Sunday 23 July 1916 Looking round firing line preparatory to taking over work here.  Things very quiet here. 
 Monday 24 July 1916 Started revetting communication trenches &amp; repairing and building dugout in Firing line.  Things going well. 
 Tuesday 25 July 1916 Had charge party revetting in "Bay Avenue".  Late in afternoon shells entered &amp; did considerable damage. 
 Wednesday 26 July 1916 Had Party Sappers &amp; Infantry revetting Abbots Rd. morning &amp; evening &amp; right on through night.  Little shelling. 
 Thursday 27 July 1916 In charge parties revetting Abbot Avenue.  A good deal of shelling in parts Lines.  Things going well. 
 Friday 28 July 1916 Revetting Bay Avenue, other Sections improving Firing Line.  Things going well. 
 Saturday 29 July 1916 Germans shelled Fleur Baix today doing considerable damage.  Destroyed old Cathedral said to be 300 yrs. old. 