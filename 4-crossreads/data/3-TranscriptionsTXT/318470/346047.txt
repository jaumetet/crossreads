 [Page 66] 
 Sunday 31 December 1916 Uneventful day.  Working on Road Construction.  Fair amount shelling going on.  Wet as usual. 
 Monday 1 January 1917 Plenty of work going on &ndash; roads going well.  Heavy shelling last night. 
 Tuesday 2 January 1917 Same as yesterday.  Uneventful day.  Fair amount shelling.  Overcast &amp; misty. 
 Wednesday 3 January 1917 Still working on Corpl. work &ndash; plenty of work &amp; things going well.  Uneventful. 
 Thursday 4 January 1917 Wet weather nothing fresh going.  Plenty of work going ahead on Roads. 
 Friday 5 January 1917 Same work supervising Roads &amp; drainage.  Cold &amp; damp. 
 Saturday 6 January 1917 Nothing fresh.  A good deal of shelling going on. 