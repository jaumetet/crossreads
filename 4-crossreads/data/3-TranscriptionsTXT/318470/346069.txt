 [Page 88] 
 26-3-17 but we still hold it.  Heavy shelling by evening, guns nearly all day. 
 27th Things going well everywhere.  Small skirmishes only reported.  Still on Well sinking. 
 28th A good deal of shelling from Fritz today.  Nothing of great importance reported.  Still Well sinking. 
 29th Good news continues to come in from most of the Fronts.  Small local skirmishes only.  Very heavy bombardment heard north where it is believed a heavy offensive has been started by the British. 
 30th Still Well sinking.  Nothing importance reported, only a little shelling on both sides.  Cpl. Morrison &amp; I visited Beaumetz to look for water, no luck. 
 31st Engaged Well sinking &amp; clearing old well out.  Nothing of importance going. 
 1.4.17 Engaged well sinking &amp; at night we demolished an old obelisk near Beaumetz.  It was about 70' high and 7' diam. at base &amp; it is thought that the Enemy were using it as a Ranging mark.   Early this  
 2nd Early this morning the 56th &amp; 58th Batt. advanced about 2000 yds. &amp; captured Louverval &amp; Boignies.  Our casualties were fairly heavy.  During the day Enemy shelling was very heavy on our new positions &amp; he counter attacked 3 times on Boignies but without success. 