 [Page 26] 
 Sunday 26 March 1916 Nothing special doing.  Went to service in Y.M.C.A. hut at night with N. Monaghan speaker-Dr. Werner. 
 Monday 27 March 1916 Rifle drill in morning.  In afternoon on Canal Bridging weld on Trestle and and Pontoon. 
 Tuesday 28 March 1916 Took party 6 men to Cairo to get kit bags.  Had very good trip.  Beautiful weather. 
 Wednesday 29 March 1916 Started Route march to Ferry Post "Ismailia" 14th Brigade 14th, 15th Engrs.  Very hot, many fell out. 
 Thursday 30 March 1916 Marched all day.  Many fell out owing to great strain marching in heat over sand with full pack.  Very little water! 
 Friday 31 March 1916 Arrived Ferry Post noon.  Troops very much fatigued.  Marched about 40 miles, a good many collapsed on march. 
 Saturday 1 April 1916 Started work today.  Boys still feeling effect of severe march. 
 Sunday 2 April 1916 Had charge Section for squad drill in morning.  Camp fatigues in afternoon &amp; swimming parade. 
 Monday 3 April 1916 Rifle drill, squad drill &amp; work about camp.  Nothing of importance going reported. 
 Tuesday 4 April 1916 Squad drill &amp; work fixing up the Camp.  Weather hot, rather trying for men drilling. 
 Wednesday 5 April 1916 Drill &amp; work on water supply.  Nothing of importance going. 
 Thursday 6 April 1916 Out with part of Section teaching them revetting trenching. 
 Friday 7 April 1916 Had Sec. out drilling also instructing in trench construction.  Getting very hot. 
 Saturday 8 April 1916 Had section out drilling also in trenches &amp; instructing in construction. 