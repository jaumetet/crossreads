 [Page 60] 
 Sunday 19 November 1916 Very wet, cold &amp; miserable.  Was out very little.  Had a look round in morning &amp; went to Stratford at night. 
 Monday 20 November 1916 Left Leytonstone about noon &amp; went to City.  At night Mr. &amp; Mrs. Smith and I went to "Palladium".  Good show. 
 Tuesday 21 November 1916 Had look City today.  Did not get anything of importance.  Did some business. 
 Wednesday 22 November 1916 Went to City in afternoon &amp; had a look over Tower London.  With [Mr.?] Smith to show Stratford night. 
 Thursday 23 November 1916 Left Leytonstone in afternoon &amp; missed troop train at Waterloo.  Went to Royal at night &amp; saw "Best of Luck". 
 Friday 24 November 1916 Left Leytonstone &amp; caught troop train in afternoon.  Left Southhampton about 9 p.m. for France. 
 Saturday 25 November 1916 Disembarked Havre about 7 a.m. &amp; went to Rest Camp.  Left for Rouen about 11 p.m. 
 Sunday 26 November 1916 Arrived Rouen about 7 a.m. &amp; went to Rest Camp.  Left for "Front" about 4 p.m.  Travelling all night.  Much delay.  Frosty. 
 Monday 27 November 1916 Left Rouen Camp about 1.30 p.m. and arrived in Albert about 7 p.m.  Very slow journey.  Much delayed. 
 Tuesday 28 November 1916 Stayed in Albert all day with Capt. Chappel &amp; Capt. McNab.  They left for Front about 10 p.m.  I stayed Albert. 
 Wednesday 29 November 1916 Left Albert about 9.30 a.m. for Front with Lt. Morgan 15th Trench Mortars.  Reached Coy. H.Q. about 2 p.m. 
 Thursday 30 November 1916 Taking things easy as I still have bad cold &amp; I do not want to get worse one.  Waterloo Farm. 
 Friday 1 December 1916 Went to Trenches today &amp; had a look around.  Sec. working in various places. 
 Saturday 2 December 1916 Working Needle Trench also Deep Dugouts &amp; laying Duck walk track. 