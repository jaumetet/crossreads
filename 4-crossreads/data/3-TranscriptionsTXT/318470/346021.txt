 [Page 40] 
 Sunday 2 July 1916 Arrived Thiennes about 8 a.m. &amp; marched 8 K. &amp; took up billet in farm house.  Good dry shelter, all troop billeted. 
 Monday 3 July 1916 Camped at Atoin [?] near town of Thiennes.  Ulahns were here last year.  British passed through, retreat Mons. 
 Tuesday 4 July 1916 Heavy rain today.  Having Gas Helmet drill &amp; other drill, map reading etc.  Reported good advance British. 
 Wednesday 5 July 1916 First pay in France.  Still very dull &amp; showery.  Nothing fresh going.  Could hear heavy bombardment last night. 
 Thursday 6 July 1916 Had 10 mile route march in morning.  Issued with Gas Helmets, goggles &amp; Steel Helmets in afternoon. 
 Friday 7 July 1916 Route march 10 miles in forenoon.  Gas Helmet drill in afternoon. 
 Saturday 8 July 1916 Marched in all about 15 miles.  Had Test in Gas with Gas Helmets only. 
 Sunday 9 July 1916 Started march to Firing line.  Camped Estaires.  Passed through Merville, fine town.  All going well. 
 Monday 10 July 1916 Left Estaires 0600, marched through Sailly to Fleurbaix &amp; camped in an old convent &ndash; very much knocked about by Artillery fire. 
 Tuesday 11 July 1916 Some of Coy started working trenches.  Shifted into new Billets.  Very little doing. 
 Wednesday 12 July 1916 Went into new Billets today.  Nothing of importance going.  Had a look around. 
 Thursday 13 July 1916 No. 2 Section got 5 days leave, two large shells lobbed near billets.  Things going quiet. 
 Friday 14 July 1916 About 40 shells lobbed near Village &amp; damaged Battery of Artillery.  Took party into Bac St. Maur tonight to [indecipherable]. 
 Saturday 15 July 1916 Very heavy bombardment by Germans.  Fairly heavy casualties.  Working day &amp; night. 