 [Page 58] 
 Sunday 5 November 1916 7th Brigade A.I.F. made attack in front Bapaume &amp; Tommies on left.  Only partial success. 
 Monday 6 November 1916 Coy. withdrew today from Front Line.  Resting &ndash; everybody exhausted.  Still wet &amp; very muddy everywhere. 
 Tuesday 7 November 1916 Packing &amp; loading carts.  Expect to leave today or tomorrow.  Very miserable weather.  Had two Saprs. killed.  5 Infantry in trenches last night. 
 Wednesday 8 November 1916 Left camp 3.30 a.m., marched 12 miles to Redemont [Ribemont?].  Left here 1.30 p.m. marched 2 miles &amp; took Motors to "Cardonette", 5 miles from "Amiens". 
 Thursday 9 November 1916 Resting today.  Many of men in Coy. sick &amp; exhausted.  Nice weather.  56th Batt. Billetted here. 
 Friday 10 November 1916 Had two hours Parade &amp; Drill in morning.  Resting in afternoon.  Nice weather!  Expect leave tomorrow for London. 
 Saturday 11 November 1916 Been waiting all day for Pass to go on leave to London.  Very cloudy, a little rain.  Nothing important. 
 Sunday 12 November 1916 Left for London on leave.  Went to Bertangles &amp; found it was wrong station.  Went to "Amiens" at Saint Roch.  Took train to Boulogne. 
 Monday 13 November 1916 Arrived  Rouen  Boulogne about midnight last night  about  after a tiring journey.  Stayed Rest Camp. 
 Tuesday 14 November 1916 Arrived London about 11 a.m. and took room Union Jack Club.  Very bad with influenza. 
 Wednesday 15 November 1916 Still bad with flue, cannot get about to see anything.  Only been about a little. 
 Thursday 16 November 1916 Had a fair look around City.  In evening went to Leytonstone &amp; stayed night with Mrs. Mr. Smith.  Nice people. 
 Friday 17 November 1916 Still got very bad cold.  Staying with same people but not feeling fit to go to City.  Looked around Leytonstone. 
 Saturday 18 November 1916 Had a run to the City but everything was to wet &amp; miserable.  Had a look at good show at Stratford at night. 