 a6922056.html 
 109 
 (9). 
 directed against us) must organise the whole Nation.&nbsp; We must reckon 
 afresh with the facts, get a new grasp of the manner of victory and 
 prepare ourselves to meet drastic sacrifices. 
 &nbsp;&nbsp;&nbsp;&nbsp; It is not a bit of use ASKING British people to make sacrifices. 
 They are not united enough to take much action until they actually FEEL 
 the pinch.&nbsp; Through no fault of their own they have led unordered 
 existences and have known no discipline in their Civil life.&nbsp; There is 
 no time now to start in and teach them, we must legislate in whatever 
 direction is required and put a policeman there to see that the law is 
 carried out.&nbsp; First the food supply must be assured.&nbsp; A good start has 
 been made limiting the price to be paid for meals in restaurantes.&nbsp; This 
 does not, on the face of it, strike one as being a remedy for conserving 
 the food supply, but to a certain extent it is.&nbsp; Without restaurantes 
 the &quot;a la carte&quot; menu was the most popular and incidentally the most 
 expensive.&nbsp; At many a London restaurante a man could, without being a 
 glutton, eat a meal which, exclusive of wines, would cost him say 25/-. 
 Now it is not only the food he eats that he is paying for but the food 
 which has been prepared for him to select from and which is wasted if he 
 does not choose it.&nbsp; It was not unusual to see 50 or more items on an 
 &quot;a la carte&quot; menu and at the end of the day perhaps only half of these 
 had been asked for and then possibly some of them only once or twice. 
 Some of the courses like egg dishes and grills would not be cooked until 
 ordered but still the waste must have been enormous.&nbsp; By limiting the 
 price for meals the proprietors of restaurantes will have to bring down 
 their selection lists to meet it.&nbsp; I fancy the price for dinner has 
 been fixed at 5/-, and supposing we take the case I have instanced above 
 of a 25/- meal chosen from a 50 dish menu; that will mean that the 
 proprietor will in future have to limit his customers&#39; selection to 
 10 items - a saving of 40 dishes on the country&#39;s food supply.&nbsp; All 
 this perhaps is only a small matter, but its a start.&nbsp; Meatless days 
