 a6922074.html 
 145 
 (5). 
 The Battalion settles down to get the hang of its new position, 
 to improve the existing cover and to do as much damage to Fritz as 
 possible. 
 &nbsp;&nbsp;&nbsp;&nbsp; During the winter months it is often impossible to 
 hold continuous lines of trenches owing to the swampy nature of the 
 country, and on these occasions the line is held by posts arranged 
 along the trench line or elsewhere, just where the ground is suit- 
 able.&nbsp; The intervening spaces are covered by machine guns and in 
 addition patrols watch the gaps by night.&nbsp; The enemy holds his 
 line in the same manner and for a time the raiding habit was 
 popular, each side trying to cut off one or more of his opponents 
 posts.&nbsp; At this type of warfare Fritz is no match for our men. 
 &nbsp;&nbsp;&nbsp;&nbsp; Food has to be carried out to the various posts by 
 night taking advantage of any clouds which may be about in the 
 event of the moon being bright.&nbsp; On the occasion which I have in 
 mind the moon was at its full, and this in conjunction with the 
 snow covered ground made movement very precarious as the enemy was 
 quite close.&nbsp; Of course we took fullx advantage of it to worry the 
 Boche, but that is poor comfort to you when you are crawling along 
 on your stomach through the ice and snow on your visits from Post 
 to Post.&nbsp; The available officers split the time up so that each 
 only has a few hours crawling to do each night.&nbsp; Accompanied by 
 your Patrol Corporal you set off to visit the posts.&nbsp; All is well 
 until you top a bit of a rise just behind the first line of posts: 
 now we are on a sort of plateau which is a salient into the enemies 
 lines.&nbsp; This makes it very awkward as Fritz&#39; snipers and machine 
 gunners on the left are able to &quot;moon&quot; you against his flares 
 (which are being sent up a mile or so to your right and rear. 
 Usually it is only necessary to drop down flat for flares in 
