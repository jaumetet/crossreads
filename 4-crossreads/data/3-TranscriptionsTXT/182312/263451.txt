 a6922048.html 
 93 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  10th INSTALMENT.  
  15th November, 1916. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; I had been told that the pea soup variety of 
 London fog had of late years become a rarity, but this year looks like 
 making up for lost time - or fogs.&nbsp;&nbsp; A clean fog is bad enough, but 
 these London affairs seem to have got mixed up with smoke from 
 thousands of chimneys until they assume a greenish black color.&nbsp; They 
 penetrate everywhere and completely disorganize London.&nbsp; For a while 
 the vehicles grope their way about and much tooting or horns, and head- 
 lights which at ordinary times would be dazzling but are now a blur in 
 the fog, visible perhaps fifteen or twenty feet only.&nbsp; After a whilex 
 they give it up and motorbuses, taxis etc. begin to crawl back to their 
 garages, the drivers finding it dangeroous to stay out longer.&nbsp; Since 
 the Zeppelin visits started these are the only occasions on which London 
 is allowed to light up and as the one I saw was the first bad fog of the 
 season, there was a hurried overhauling of lamps and fittings both on 
 vehicles and in buildings.&nbsp; A thousand-candle-power roaring petroleum 
 flare set up as a beacon in the Strand near Trafalgar Square as a guide 
 to stray vehicles, was invisible at 100 feet - just a hissing noise in 
 the fog.&nbsp; By five o&#39;clock in the evening practically all surface 
 traffic had stopped and it was said that footsteps could be heard in 
 Piccadilly -&nbsp; surely a record for this, the noisiest corner on earth. 
 The Tubes were the only unaffected means of travel and the scramble on 
 the platforms and in the subways reminded one of the rush for shelter 
 on Zeppelin nights to these excellent dugouts. 
 &nbsp;&nbsp;&nbsp;&nbsp; Extravagance is still the order of the day in London, 
 despite the fact that prices are soaring upwards.&nbsp; The Carlton, Princes, Romanos, The Ritz, The Savoy, in fact all the expensive restaurants, 
 are crowded nightly and unless a table is reserved before about 6.30, 
 xxxxxxxxxxxx accommodation cannot be guaranteed.&nbsp; Look into any 
 theatre and you will see a thousand people or more, rocking with 
