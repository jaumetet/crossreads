 a6922064.html 
 125 
 (6). 
 &nbsp;&nbsp;&nbsp;&nbsp; 7.&nbsp;&nbsp; Little boys learning the Hotel business should be kept from 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; being too officious.&nbsp; Having every mouthful one eats watched 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; or being continually run in front of just for the sake of 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; opening a door is very irritating to the quiet man. 
 &nbsp; 
 &nbsp;&nbsp;&nbsp;&nbsp; 8.&nbsp;&nbsp; Whiskey with BOTTLED soda should always be available. 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Whiskey should be served with the bottle and not in small 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; measures as though a gentleman would help himself to too 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; much. 
 &nbsp; 
 I could add a couple myself: 
 &nbsp; 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Head waiters should use tact in seating guests and never 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; put an Englishman at the same table with a rancour voiced, 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; gum chewing Yank orx a sword swallowing Southerner. 
 &nbsp; 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Don&#39;t serve macaroni, spaghetti etc. in pieces of longer 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; than about an inch and a half otherwise the Englishman will 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; get tangled up in the stuff. 
 &nbsp; 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Don&#39;t fry whitebait - boil it. 
 &nbsp; 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Provide spoons for the mustard and salt and a knife for the 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; butter, an Englishman hates having to wipe his knife on the 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; tablecloth and dive for the butter or condiments: besides 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; its bad for the tablecloth. 
 &nbsp; 
 Generally speaking, however, the better class hotels are well run and, 
 with the exception perhaps of the Adelphi Hotel at Liverpool (Eng.), I 
 Have not seen finer establishments than the Hotel de Paris at Monte Carlo 
 or the Hotel Ruhl at Nice. 
 &nbsp;&nbsp;&nbsp;&nbsp; This Southern shore of France was at one time a happy hunt- 
 ing ground for the Roving Nations in search of territory or more often 
 booty and one sees to-day plenty of evidence in the way of forts, castles 
 watch towers etc. to connect the Romans, Greeks Spaniards and Moors with 
 its early history.&nbsp; Even the Turks are said to have laid seige to Nice 
 at one time.&nbsp; The Roman&#39;s chain of beacon towers connecting up Northern 
 France with Rome and by which they were accustomed to signal their 
 victories, pass over the Southern Alps hereabouts.&nbsp; Some of the ancient 
 castles and forts are a regular gold mine to the district by reason of 
 the number of tourists they attract and they are not going to be allowed 
 to crumble away altogether if reinforced concrete and hidden iron bands 
 can save them.&nbsp; Villages perched on top of steep mountains are common 
 in fact it seems to have been a popular precaution against attack by 
