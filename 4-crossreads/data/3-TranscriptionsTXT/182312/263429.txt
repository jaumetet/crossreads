 a6922026.html 
 49 
 (9). 
 long been known as &quot;the Nursery of the Western Front&quot; - a picnic 
 compared to any other bit of the line!&nbsp; These are little things 
 which Australians at home ought to know and they should realize 
 that we have yet to show that we fitted to share the line with 
 such tried troops as the famous British Regiments and Canadians 
 now here in France.&nbsp; Every Australian soldier who has been in 
 the line here (I exclude Cairo &quot;Anzac Heroes&quot; and the heroes from 
 the pay camps in London and the like) will tell you the same thing. 
 They read it all with disgust - the don&#39;t want all this publicity, 
 all they want, all they hope to be is good soldiers equal to the 
 Britishers they have come to fight beside.&nbsp; The British Regiments 
 and the Canadians make raids on the Hun trenches nightly almost, 
 but the raiding parties don&#39;t get leave for England:&nbsp; they are not 
 driven round London in motor cars nor fed with strawberries by the 
 Lord Mayor nor paraded in the London streets to be cheered, no, its 
 their every day job to raid and they do it - just because it is 
 their job.&nbsp; I actually saw in the Daily Mail where&quot;Londoners 
 will to-day have an opportunity to cheer the party of Anzacs who 
 so gallantly raided the German trenches the other night in France. 
 The party will be in carriages and will pass such and such points 
 at such and such a time.&quot;&nbsp; Could anything be more idiotic than 
 this?&nbsp; Imagine, if you can for one moment, the feelings of British 
 and Canadian troops in the trenches when they read this! 
 &nbsp;&nbsp;&nbsp;&nbsp; I fear that I have been rather long winded on 
 this subject, but it is one that I, in common with practically all 
 other Australians xxxx - at least all I have come in contact with- 
 now in the line in France, feel very strongly on.&nbsp; Mind you, I 
 don&#39;t for one moment contend that the Australians did not do good 
 work on the Peninsular:&nbsp; everybody knows that they fought well and 
