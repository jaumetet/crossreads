 a6922002.html 
 &nbsp; &nbsp; I got a telephone message at Helouan Hospital (Egypt) at 12 
 Noon on Sunday 26th March 1916 that my unit ( 9/17 ) was to move out 
 at 7 a.m. next morning from Heliopolis and I had no hesitation in 
 telling them that I would be with them when they moved. &nbsp;Under ord- 
 inary circumstances I would have been discharged from the Convales- 
 cent Hospital within two days and would then have had to go to the 
 &quot;Overseas Base&quot; and from there to some strange unit and possibly 
 stay in Egypt for months. &nbsp;We were particularly lucky to get away 
 so soon as a lot of the earlier reinforcements to the 17th Batt. 
 were still in Egypt. &nbsp;Plenty of men had been in Egypt 5 months and 
 never had a move. &nbsp;Instead of making the 9th Reinforcement we will 
 probably form the 1st Reinforcement to the 17th Batt. &nbsp;I went over 
 to Heliopolis where the 9/17 were camped on Sunday afternoon to make 
 all arrangements and returned to Helouan in the evening to get my 
 discharge from Hospital, luggage etc. &nbsp;Coming back to Cairo about 11 
 p.m. I got a motor car from the Railway Station to take me right 
 out to the Camp (about 10 miles) but after it had gone a mile or so 
 in trying to avoid another car coming round a corner it ran over the&nbsp; 
 pavement and smacked a wall. &nbsp;The wall came off best so I had to 
 transfer my baggage and self&nbsp;to another car. &nbsp;The run out to Helio- 
 polis is very pretty even at night and the road is perfect - as a 
 matter of fact, Australia (and Particularly N.S.W.) is the only 
 place I have seen which has bad roads. &nbsp;Arriving at Camp. I found 
 my old orderly had made a bunk up for me in a tent so I turned in 
 for a couple of hours - the only occasion on which I had slept 
 in camp in Egypt. &nbsp;Reveille went at 2 a.m. breakfast at 3am. and 
 &nbsp; 
 &nbsp; 
 &nbsp; 
 &nbsp; 
 &nbsp; 
 &nbsp; 
