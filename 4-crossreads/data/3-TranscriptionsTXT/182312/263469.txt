 a6922066.html 
 129 
 (8). 
 side one might be excussed for remonstrating with the driver. 
 &nbsp;&nbsp;&nbsp;&nbsp; A lot of the mountain tops in the vicinity of Menton are con- 
 verted into forts which command the Italian frontier, whilst the Italians 
 on their side have made similar provisions.&nbsp; Happily both systems are 
 now united against the common enemy, although unless by an occasional 
 shot at a German submarine in the Mediterranean away below, it is unlike- 
 ly that they will be of any use.&nbsp; The forts are manned by the pictur- 
 esque Chasseurs Alpins with their baggy trousers and floppy tam o&#39; shante 
 caps.&nbsp; Artillery practice was in progress one day whilst we were up in 
 the mountains, the shells being directed at some mountain top a couple of 
 miles inland, and in the distance amachine gun could be heard tat-tat- 
 tatting away in its efforts to keep the cobwebs out of its barrel.&nbsp; This 
 mountain warfare would be a welcome change to the flat swampy conditions 
 of the North for one could at least retire round the corner of a crag 
 for a comfortable sleep now and then whilst gas attacks would be 
 impossible and one need not sit up all night with a gas helmet on just 
 because some fool miles away thought he smelt gas and started all the 
 alarm horns and bells going. 
 &nbsp;&nbsp;&nbsp;&nbsp; The town of Grasse about 15 miles inland from Cannes is 
 rather a famous Scent making centre and all about the mountain sides are 
 terraced not for growing olives this time, but roses and other highly 
 perfumed flowers.&nbsp; Long before one reaches the region of the Scent 
 factories one becomes aware of the industry of the place - the same might 
 be said of Botany. 
 &nbsp;&nbsp;&nbsp;&nbsp; The Principality of Monarco over which Prince Albert 
 reigns consists of the towns of Monarco and Monte Carlo whilst in between 
 is La Condamine where the greater part of the Prince&#39;s &quot;subjects&quot; live. 
 The Principality is 300 acres in extent and has a population of about 
 16,000 people, a toy harbour, a golf course, a museum and above all any 
 amount of facilities for the World&#39;s Rich to part with their money.&nbsp; As 
