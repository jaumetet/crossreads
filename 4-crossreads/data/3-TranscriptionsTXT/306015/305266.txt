 [Page 90] 
 those in places constantly visited by troops. 
 Our billet  was  is of the usual type, with plenty of straw on the floor.   In one compartment of this building the straw was heaped up to the height of about 8 feet &amp; on top of this Alan &amp; I made our nests.  The proprietor turned us off it once but directly he turned his back we were up again &amp; there remained to the end of our sojourn. 
 Aug.  3. The day being too wet for parade, lectures were given us by the officer &amp; N.C.O. 
 In the evening, we again sought our egg-house &amp; after that I had tea with the family there &amp; wrote for the rest of the time on a little round table which Mlle.. placed near the fire for me. 
 Aug 4. Wet again! lectures again in the forenoon.   After dinner we had a short route march.   The country crossed was grand.   The red roofs of the villages in the green 
