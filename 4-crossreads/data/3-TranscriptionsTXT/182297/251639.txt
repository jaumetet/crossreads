 [Page 8] 
 [Rear of postcard] 
 &quot;Kinnoul&quot; 
 Belmore 
 1.2.1915 
 Every thing getting on smothly in the Area been a felp. Changes in regards to Officers &amp; Staff. Olds gone over to Milita,&nbsp;I hope to be their shortly. Draper is going. And a S.S.M. Coleman been attached not the first one we had. 
 Every body O.k. here at home, hope when you write again that you are &quot;At Em! 
 &quot;Seasons Greetings to you all 
 L.E.Y. 
 Major J.G. Tedder 
 Transport Division 
 Australian Expeditionary Force 
 Cairo 
 Egypt. 