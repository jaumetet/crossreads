 [Page 49] 
 just detrained &amp; are in a rest camp overlooking the harbour The town in the distance looks  an insignificant  a very ordinary little place &ndash; before the war just a port of call for coastal trade but now it is an important base  for  both naval &amp; military for the Macedonian  Front  &amp; Turkish Campaign &amp; I don't suppose there was ever so much work &amp; money in the place before. 
 The country has been very monotonous these last two days &amp; we are all glad to have got this stage of the journey over &ndash; 8 days in the train 
 Oct 3.  S.S.  H.M.T. Ormonde Glasgow was on 
