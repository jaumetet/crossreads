 [Page 58] 
 to the Station  some  a mile away. There was a hospital train filled with men from the Palestine Front &ndash; British Australian &amp; N.Z. Indians all burnt so brown that colour was no distinction 
 They were mostly malaria cases &amp; very weak &amp; thin  they looked It made us realize that each front has its particular hardships &amp; the Winters of France are probably counter balanced by the flies &amp; heat of Palestine. 
 After the usual delay at the Station we  left  started at 9 p.m. in open 
