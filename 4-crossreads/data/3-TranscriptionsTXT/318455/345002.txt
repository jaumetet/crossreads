 [Page 5] 
 that goes on for lack of any other subject &amp; it has had the same effect in the old country to automatically settle all strikes &amp; trade disputes 
 The nation would have been false to its traditions if it had not been so 
 Ap 6/18 Am writing this in the train at Goedaersvelde in the truck with the horses having come on here today from La Clytte &amp; we are just waiting to start 6.45 p.m. destination unknown but at a guess The Somme again  where  
 Ap. 8 Bertangles (Near Amiens) detraine[d] at Amiens yesterday morning after an al[l] night journey &amp; arrived here about 2 p[m] We are billetted in a  squ  large group 
