 [Page 6] 
 of farm buildings with the usual 'rectangular smell' in the middle of Bairnsfather describes it.  
 Amiens nearly deserted, the houses &amp; shops all shut up &amp; the few people left all seem preparing to go &ndash; we saw lots of carts filled high with household goods &amp; groups of poor old women carrying  their  bundles  away  of clothes etc. Very wet last night I slept in an old Farm cart that was  pushed  under the Cart Shed so quite dry. 
 Ap 9 Have found out that these are the farm buildings of the Chateau of the Marquis de Clermont Tonnerre a Bertangles canton Villers Bocage 
