 [Page 57] 
 just a huge camp &amp; railhead bounded on one side by the canal &amp; on the other three sides by a waste of sand &amp; tufts of desert grass. 
 The Padre handed out a bottle of wine through his Cabin window to some of the men sleeping on the deck near him Said it was to break the good news that had just come through that Germany had agreed now to evacuate Belgium &amp; France before discussing peace. If it is true it looks like a complete climb down. 
 Oct 15 We got off the boat yesterday at 5 p.m. &amp; marched 
