 [Page 29] 
 &amp; that will ease the roads. 
  The ground is all  The country all round is pockmarked with shell holes most of which have fell harmlessly but I have just been to see the results of a chance shell which more than paid for itself. It was a direct hit on a battery of tanks just near here that were parked up some 15 of them &ndash;  ready for the advance next morning  loaded up with petrol &amp; trenching material ammunition etc ready for the advance next morning. The one hit was blown into halves &amp; fired 
