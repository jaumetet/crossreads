 [Page 237] 
 of the fa&ccedil;ade are grey stone and deep fading rose brickwork:  the steep tiled roofing carries the fantasy of Tartanaldean [?]. 
 So an ending of Diary VI, in a better content from today, being back to the Australian Corps, in a passing Hospital Train yesterday two strange shaped colours seen:  the oval of the 3rd and the high colours of the 5th, and tomorrow &ndash; tonight commence our first Corps stunt. 
