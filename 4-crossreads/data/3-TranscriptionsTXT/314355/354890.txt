 [Page 205] 
 July 11, 12 At 9 this morning four men went out along the Railway Embankment, and brought back with them some 30 or 40 prisoners.  They had come into the line last night, and apparently imagined this sector to be a quiet one:  this haluscination was immediately exploited by the 1st Bn., and later by the 4th. 
 At 2 p.m. the bag included two Company Commanders. 
 Still the guns are quiet, and they have been all day. 
 260. A definition of "On the grouter".  At 5.30 p.m. L/Sgt. Lockhart and L/Cpl. Farrel of 6th Batt. entered a trench, they saw an enemy post which they stealthily approached.  When almost on the post they were observed by an N.C.O. who sprang to a M.G.:  before the M.G. could be brought into action the post was rushed, and the garrison of eight captured and brought back to our lines.  Later the two Machine Guns were brought in, the Heavy M.G. by the 6th Bn. and the Light by the 4th. 
 July 12 261. Payed:  out in the early evening with Ken to the farmhouse of the woman whose arms are like dwarfed legs, then out to the Farmhouse billet of the Ulstermen for Coffee. 
 July 12 Mail, a letter from Aunt Louie, a bonser little personal note of her charm and generosity.  "The Elizabethean Playhouse" arrived safely &ndash; and, 
 "We were only saying this week it was a long time since you had sent any books." 
 Tomorrow will post away home the two books of James Joyce:  and later to Aunt Louie the first of "The Elizabethean Playhouse". 
 Still prisoners are being rounded up, and the A.D.S. report for the last 24 hours shows a nil return of wounded passing through. 
 262. 
 This from Borre, 
 Les Membres de la 1 Ambulance Australieme out beaucoup plaisir in demandant l'attendance de le Frank Molony, au Reception de Bal Artistique et Diner a la maison Quatre Filles d'Amour, Rue a Hazebrouck a six heures en sou 15 Juillet 1918, R.S.V.P. 
 Bought tonight P.O. for 8/- for Davis &amp; Oriole, but on consideration will wait till posting till the two books of July 8 arrive. 
