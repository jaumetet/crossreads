 [Page 243] 
 T.B.C. Notes &pound;. s. d. 114096 &ndash; 14.6.18 &ndash; "Resentment" &ndash; 3.11 S.184096 &ndash; 7.6.18 &ndash; Pencils, 6 &ndash; 2.3 184096 &ndash; 19.6.18 &ndash; Portrait of Artist &ndash; 5.1 185096 &ndash; 4.7.18 &ndash; 3 Sheldonian Series &ndash; 8.2 The Anchor &ndash; 6.10 Returned July 17 &ndash; Cuala Book wanted:  Darn them!  19.7.18 &ndash; Book of S. &amp; Wonders &ndash; 5.7  (P.13648) &ndash; 22.7.18 &ndash; Story Teller's Holiday &ndash; 2.4.8 24 (?) &ndash; Hauptmann &ndash; 5.16 (?) 
 Pot-Boilers, Clive Bell 5/-, C.U. 
 Book Notes "Rosas and Camilla", Masefield "The Somme Battles", Masefield "Counter Attack", Sassoon, 17.7.18 
 [The following ten lines crossed through.] "A Story-Teller's Holiday", George Moore "Secret Springs &ndash; Dublin Song", poetry L.L.R. "Resentment", Alec Waugh "Judgement of Valhalla", Gilbert Frankeau "The Crock of Gold", Stephens "Look!  We have Come Through", D.H. Lawrence, C.W. 5/- "The Oracle of Colour", Kidder "Memories of Childhood", Freeman "The Tale of Igor", C.L.B., 10/6 "Reincarnation", J. Stephens "The Gold Tree", J.C. Squire 
 "Faith of Our Fathers", Card. Gibbons 
  The Little School, S. Moore  
 Last Poems 2/6, Francis Ledwidge, H.J. 
 [The following two lines crossed through.] "Raptures", W.H. Davies, C.W.B., P.B. "The Dark Fire", 3/6/18, W.J. Turner, S.J. 3/6 
 "Folly", T. Maynard, E.M., 17/7/18 
 [The following seven lines crossed through.] Exiles, J. Joyce Georgian Poetry, 19.3.16 William Blake, Symons, 7.6.18 The Elizabethean Playhouse, 7.6.18 "Portrait of the Artist as a Young Man", James Joyce, 4.6.18 "New Paths", Beaumonts "The Anchor", M.T.H. Sadler, 13.6.18 
 "The Contemporary Drama of Ireland", 5/-, G.A. Boyd, Fishburn 
  S. George for England, M.R. Liston  
 Essays Irish &amp; American, J.B. Yeats, F.V., 4/6, 17.7.18 Poetry of R. Brooke, July 25.7.18 
