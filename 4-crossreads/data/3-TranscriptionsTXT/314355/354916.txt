 [Page 231] 
 August 4 295. Morning to the Ambulance, there dinner with A. Sub., in the afternoon to the Canal, and bathing, with green dark water, straight  lined  banked by steep terraced grass banks,  and  guarded by tall trees:  and a long springboard, and a hundred men bathing. 
 The carnival of tonight postponed, tomorrow at 10 to see its commencement. 
 Yesterday a letter &ndash; and an account "941" from the T.B.C.  Brooke's verse invoice dates July 31. 
 In the afternoon from Basil a book of the verse of Miss Forest:  its fly-page scribed &ndash; 
 "Glad the song o'er the land shall flow By peaceful farmhouse, and flower tall glades, But the bravest thing, (as all men know,) Was the way that the song was made. 
 From "A Song of Peace", M. Forest, [Mabel Forrest] Bulletin, Brisbane, April 13, 1918. 
 August 4, 5 296. Our Billet, the loft of a long barn by the Church, the Div. observers and 'Maps'.  Reading last night, and completed to the beating of the electric light engine, Hauptmann's "Vor Sonnenaufgang" [Before Sunrise] &ndash; the first of the plays of Vol. 1.  A splendid play, and good reading, its dialogue strangely natural, and the art of the love scene exceptionally good:  the conclusion perfect too, and well handled. 
 August 5 297. The canal, with the 1st Bde. Carnival in progress by de Chavannes.  A morning of grey, a haze of twilight low above the ground. 
 In a small house by the station, coffee:  refugees from La Motte, and one a girl of 17 &ndash; the colour of the sun's gold in her deep hair, and on her face and boyishly muscled neck and shoulders &ndash; drawn after Lord Leighton's "Juggling Girl". 
 The relay won by the 4th and some good swimming:  2nd came in a bad fourth. 
