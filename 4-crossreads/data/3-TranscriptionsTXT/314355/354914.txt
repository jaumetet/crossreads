 [Page 229] 
 August 1 292. T.L.S., July 25, a good review of James Joyce's Exiles ending.  Will not the stage society or the Pioneers let us and the author see this play, so that its shortcomings may become apparent to him, and its virtues be brought home to us with their full force?  Grant Richards, in his ad. letter quotes a review of Alec Waugh's Resentment.  Robert Lynd writes of him as, 
 ".. a writer critical, quick of feeling and zestful;  but, he cuts the corn while it is still green &hellip;" 
 - a phrase that seems perfectly one of the aspects of Waugh's verse. 
 Good announcements are of two books: an edition deluxe of Beerbohm's "The Happy Hypocrite", and by Rackham a book of Swinburne's "Child Poems". 
 Prisoner list to today lacks 15 to compleat the 1000 captured since taking as ours their sector. 
 Later:  now from a later addition through the A.P.M.'s list our surplus is 3, 1003.  Return them to their units?  One of this morning's prisoners stated over 30 of his crowd were waiting to desert, but our M.G. fire was too heavy for them. 
 August 2, 3 293. All day the rain.  A mail up in the afternoon &ndash; Hauptmann's Plays from T.B.C. 
 Packed for moving:  Hauptmann in the Instrument Box:  Moore with the Maps, Dieppe, Lens, Amiens and Abbeville:  Cathedrals with a mixed crowd.  To read in the train &ndash; Balzac's "The Tragedy of a Genius", and R. Brooke.  To move in the morning, and a long route-marching with packs up and blankets. 
 August 3 Wardrecques 294. With the A.P.M. board as a writing desk, out  side  in the ground of our quarters.  Soft deep grass about, an old yellow brick building, poplars bordering the high wall, and a great splendid chestnut by the shallow Becque through the  grounds  fields behind.  A day of silver, and clouds of a wounderful blue:  rain as we marched, but light rain, rare rain, that kept sullen heat from the air. 
 Two women are here, one who has just spoken to me of "taubes" a beautiful dame of near 60, good in her figure, and a Frenchwoman.   Amongst  Before the peasants we have been amongst, she stands as an home woman, and sets the mind dreaming and quietly recounting old faces. 
