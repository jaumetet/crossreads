 [Page 215] 
 July 20 275. Mac staying in:  and the morning so splendid with sun and colour decided to take today instead of tomorrow.  It was to be a day of bonser lazing, of reading and lying in the sun. 
 But a pass to St. Omer, dinner there with three Artillerymen and two Emma G's, back to Cassel, tea and an afternoon in the Y.M.'s Reading Room taking books from their shelves, opening them and reading a line, dipping into old English Reviews and back to the shelves again.  Midday saw heavy rain that lasted the afternoon through:  then at 5 a wounderful chequering of sunlight and shade over all the tapestried land about Cassel. 
 Past Bavinchove, a great company of Chinese, stripped to the waist, wearing bright dungaree bloomers &amp; wide hats, working on entrenchments through a field of yellowing wheat, under a golden sun in a blue burning sky. 
 French well advancing;  now 18000 prisoners and 300 guns. 
 July 21 276. A day of vagrant winds, still in the damned 5000 detail of the southern part of the sector. 
 Mail &ndash; only a P.C. noting from Smiths of Paris that "The Dark Fire" was O.O.P.  So tonight writing for McKenzie's "Poems" or "The Bubble" O.P. by Willoughby Weaving &ndash; with books from Blackwell's catalogue received last night. 
 Reading "The Anchor", but not with the enjoyment of "Hyssop", here Sadler does not  seriously  take Laddy in himself seriously, rather he attempts aloofness when writing of his real concerns. 
 Wire from the Aisne received midday contained, "The whole of the 6th French Army are advancing:  A.A.A. line now runs &hellip;"  Official prisoners to 6 last night number 20000:  the Boche claim an equal number of [indecipherable].  A quiet day here, and yesterday. 
 Later.  "The Anchor" completed:  disappointing and not particularly clever, though full of clever affectations.  "Hyssop" left an impression, "The Anchor" is merely an ordinary novel, with a sprinkling of chattered names. 
