 [Page 193] 
 July 3 fine dull blue bound edition. 
 244. At a Farmhouse coming back coffee with some Ulstermen, their speech freer from exaggeration than the average Tommies, and their expression changing and good, and their complexions burnt finely brown with richness about the cheek bones, one with the build and features of Jake whose voice had the faintest murmur of musick in its rich slowness. 
 Changes at La Kreule:  saw Willie L. and he now B. Sect. Supernumerary:  from Frank S. hear of he having Stretcher Bearer's Heart, if he can persuade Wilk from the Bearers they'll both go to B. Sect.  Will W. be persuaded ? then his love for Willie will be greater love than I thought. 
 News today of another Hospital ship sunk:  a Canadian ship travelling back from Canada. 
 And Heine, Goethe and Schiller and Wagner were of these peoples! 
 July 3, 4 245. From George borrowed copy of the March Tread for its review of "The Pen Drawings of Norman Lindsay".  Illustrations for Boccaccio, Rabelais and Casanova not yet published:  books of his illustrations include Hugh McCrae's "Satyrs", "Sunlight", L.E. Petronius (put at &pound;5.5.0) and Leon Gellert and Francois Villon. 
 July 4 245. The first entry of the morning since leaving Borre, but word comes through of the 3rd, 4th and 5th advancing in the Somme on a 4 mile front and a penetrating to an average depth of 1&frac12; miles.  The wire reads, 
 "We attacked at 3.10 this morning and captured Hamel, also Vaire and Hamel Wood, A.A.A.  We are consolidating, etc. etc.  Aust. Corps." 
 246. A long evening, after a walk alone, spent in an improvised estaminet at St. Sylvestre, with one of G. Staff and a 3rd Batt. man. 
 Today a home mail, letters to May 12th, and one of Mother's letters write of Burke's "London Lamps" and J.B. Yeats Letters (Cuala Press), and from T.B.C. T.L.S. of June 27, a dull number. 
