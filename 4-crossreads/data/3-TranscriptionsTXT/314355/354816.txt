 [Page 131] 
 May 25 165.  a.m. Still a wind, but thinner clouds about the sky:  the sun faint but of colour.  Gas shells over &ndash; from Pradelles to La Kreule all night, casualties amongst 1, 2, 3, 4th Battalions. 
 166.  p.m. Early down to Dressing Rooms talking to R., the Col. blew in .. 
 "Give us a bandage!  How's the Drawing getting along Moony?  Come outside, I've something to say to you!"  All in one breath, as none other could possibly manage.  The something, an order from the A.D.M.S. that I be "made available" for work on Maps, so tomorrow porter from A.D.S. to D.H.Q., and tomorrow Vin was coming across during the morning. 
 Mail, home letters and "Tale of Igor" and "The Little School" from Beaumonts.  Letters from Mother, March 17 and April 1, from Betty Mar. 28, from Grandma, April 1, and Geoff's letters to Mother of Feb. 3 and 16. 
 "How about this for an idea!  I address all my letters to you, you read them, and pass them on to Frank.  Then he'll get one a week from me, and you'll get one a week, and I'll only write one.  See the idea, you post them on.  If I start writing to Frank as well as to you, I'll have to buy my paper, so I'm really economising." 
 Seemingly the economy campaign has set in badly with Geoff &ndash; he's given up reading the Automobile, so 9d. a week saved, and sleeping on blankets! 
 Posting away to Mrs. H. "Tales of Igor" and "Memories of Childhood". 
 "Tales of Igor", a most unusual book, but certainly well arranged, though not up to the beauty of the Poet Series. 
 May 26 D.H.Q. St. Silvestre Cappel 167. After a morning waiting at La Kreule, an arriving here at D.H.Q. at 2 p.m. and a commencing right away at joining up maps from fragments, then on to a detailing of defences between Strazeele and Pradelles, Borre and Hazebruck. 
 It's a facinating work and full of interest:  but there's a strain on the eyes that will limit my working. 
 First impressions of the men on the job &ndash; mostly men of middle age, and of a good crowd, but I'll miss Les and the family &ndash; miss more the working with chaps of my own age. 
 A cold change, and one blanket on a hard hut floor!  All day long a heavy bombarding.  I wonder how they are at Borre, by the maps she's a citadel! 
