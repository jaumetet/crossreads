 [Page 59] 
 April 21, 22 Out after breakfast, with Bill F. grouting, a good bag of coffee, sugar &ndash; sweet lump stuff, and macaroni.  Supper &ndash; and candles brass sconced to heighten the illusion, cold chicken, champagne cocktails and liquers and &ndash; so to bed. 
 Tonight a most delicious soup &ndash; from the boiled chicken &ndash; with macaroni. 
 Mail, a letter from Elsie B. 
 April 22 77.  a.m. Talking of home mails, Eric G. brought out a newly arrived snapshot of three bonser girls.  Narne is inspired.. 
 "I'll show you some I just received", and a batch of half a dozen are intimately produced.  Groups of old fashioned, fashionably dressed queer people and places, most difficult to enthuse over &ndash; especially when they're produced with such outline sketches.  We all bit:  they were remnants of an old photo album from the Caestre Chateau! 
 78.  2 a.m. A great glow of fire behind Caestre, far back over us Boche shells, and a barrage that has passed us twice, travelling from the line noises of machine guns and about us our guns alive.  There's an expectation of a Boche attack within the next twelve hours. 
 79.  Evening Settling down to evening's passing, after composing a dish of macaroni cheese.  Today some good groutings &ndash; the lower part of the village afire, no fire engines to interrupt, so a splendid seeing.  Loot, a phonograph and a great collection of Masters Voice records:  about six doz. yellow apples, and macaroni and a big sandbag of young cauliflowers.  Our room is overloaded with unexpected comforts. 
 The A.D.S. is a rather pretentious three-storied box-house, shallow cellared, and practically intact &ndash; though standing alongside the rail, &amp; surrounded not remotely by guns.  Regularly we are shelled, now a commencing has opened.  Two cellars are our Dressing Stations, a third our cookhouse, and close alongside it a narrow space, our day sleeping quarters. 
 On the first floor, a room of dull green &ndash; a rich and good colour &ndash; and white, once it was a Mess, so the green curtains still are there. 
 We have furnished it and hung it with pictures &ndash; which include three Medici prints, and 
