 [Page 127] 
 May 22, 23 158. Frank K. today typed and completed 56 copies of central pages of the May "Ghutz".  Tomorrow must to H.Q. and get Frank D.'s cartoon of the Intelligence Bureau to compleat number. 
 Will give list to Wilk for B. Section copies &ndash; then the representative men will have the copies before the duds. 
 May 23 159. p.m. This morning to H.Q. with Les, but found Frank D. with R. arm in a sling:  got from him rough sketch of Flint &ndash; and tonight a roughing of it to a 'Night Idyll' &ndash; at Caestre Flint having a dog! 
 Mail.  T.L.S. May 16 and notice from Davies &amp; Oriole of "Georgian Poetry" being sold.  Les down with the Dog Disease!  Supper with the Padre, he receiving parcels from Vic.  The whole day cold and sunless, so a sleeping in till 9. 
 Wilk has been evacuated from H.Q., general debility and boils. 
 Talking to the Padre last night, he recommended me to have a year with Burdett, "he'll put you on to some good Fresh books". 
 Happy song I am too much alive to sing. I want to shout and leap and fling My laughing arms into the air And dance on tip-toe everywhere. With not a thought and not a dream, I want to skip beside a stream In Arcady, in Arcady, And woo the wood-gods all to me. 
 I want to feel them chase me down Among the grasses green and brown, With many a mad and merry cry Of Youth and Gladness ringing high, Till reeling frolic, drunk with mirth, Sinks to exhaustion on the earth. In Arcady, in Arcady! Tra-la-la-la, who catches me? 
 N.S.W. Zora Cross 
 May 24 160. a.m. How many of the A.I.F. carry somewhere or at sometime small snippings from the Bulletin!  Murphy at La Clytte shewed me from his pocket book Alex Allan's "White Bled White" and spoke of carrying some of Adams, at La Kreule, Burton produced a batch of M. Forrest's Bulletin pieces.  Tom Richard &ndash; new Lieut. In 1st Batt. carried collectings from Bulletin's verse. 
 I've felt a predujice [prejudice] against Zora Cross &ndash; they compared her to Swinburne when I adored him more passionately than I do now:  but after this bonser little song, I'll read more of her. 
 Tonight we have put over the Boche line over 1200 gas cylinders, now and earlier in the night he has shelled about the Brewary and Rail by the Road dull gas shells. 
 161. The Dressing Room is becoming as bad as the Cookhouse for hot arguing and impromptu war councils, now Bill and Harry and some of C. Bearers are the specialists &ndash; in Artillery! 
