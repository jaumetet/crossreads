 [Page 96] 
 March, 1917. Wednesday 7 Day cloudy &amp; cold strong S.E. wind. Set off about 8.30 with 3 blankets arrived at Tricourt at 12 &amp; in very draughty huts with broken bunks. Freezed again. Got billet mobile &amp; played bridge in evening until bedtime. Rumours of moving about 11th to Poperingle &amp; waiting here till division comes out. 
