 [Page 85] 
 February, 1917. Saturday 24 Foggy and slight frost last night. Usual routine. Quiet. No night party to-night and rumours of stunt or raid by "9th". 
