 [Page 201] 
 June, 1917. Wednesday 20 Day fair and some rain. On guard at 9 and at 9.30 took Snookes down to be tried. Case dismissed so stopped guard at 10. In aftn. made up raft for 5th division to float down to Corbie to-morrow Guard again at 8. Rugby match 2 &amp; 3 V 1 drawn. 
