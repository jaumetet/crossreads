 [Page 84] 
 February, 1917. Friday 23 Foggy. Usual wire routine. Fritz shelled our surroundings early this morning and the shells screamed close over the camp. 
