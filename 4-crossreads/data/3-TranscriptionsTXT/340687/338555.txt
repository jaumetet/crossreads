 [Page 281] 
 September, 1917. Saturday 8 Fine day, misty in evening. Guard all day. Men up at 4 and onto same job. I slept in till 7. Back at 11. Good guard and quiet with 4 men on. 
