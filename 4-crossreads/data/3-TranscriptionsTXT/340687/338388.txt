 [Page 114] 
 March, 1917. 5th in Lent. Sunday 25 Day cool but fine and sunny. Summer time established. Took engine to pieces in morning and finished job in afternoon and then filled the tanks. Cleaned up &amp; tea of peaches. Wrote to Melv in evening. Heavy guns firing rather heavily all day towards Bapaume. Tightened pump stuffing box and works better now. 
