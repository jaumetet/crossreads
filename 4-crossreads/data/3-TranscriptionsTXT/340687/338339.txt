 [Page 65] 
 February, 1917. Septuagesima. Sunday 4 Usual. Day sunny but cold. To go on guard to-morrow at 9 p.m. Parcel from home. Rumours of America severing diplomatic relations with Germany. Lined hut with hessian. 
