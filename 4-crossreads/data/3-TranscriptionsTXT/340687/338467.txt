 [Page 193] 
 June, 1917. Tuesday 12 9.55 It Dull &amp; Thundery. Practised bridge with 15 pontoons in morning &amp; in afternoon put up 15 span bridge in presence of Gens White [indecipherable]. Heavy &amp; long day. 
