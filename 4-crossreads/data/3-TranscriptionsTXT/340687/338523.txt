 [Page 249] 
 August, 1917. Tuesday 7 Fine day. Usual parades In morning. Semaphore and gas helmets. In afternoon reconnaissance. Phillips Little Christian Taylor towards Hazebrouck round home by Wallon Cappel. Wrote up results in evening and gave in at 9. 
