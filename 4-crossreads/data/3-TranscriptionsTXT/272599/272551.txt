 [Page 278] 
 December Saturday 1 1917 
 Received a big mail yesterday Xmas greeting from 18. Also parcel from Elsie Gollan, Emmie Gulliford, Nurse James &amp; Mrs. Wheatly. Wish I had plenty time to reply to all at length.  Our ward a Maturnity Home for the hosp cat today.  Heavy gales &amp; rough seas usual these days.  Met Geo Aldous yesterday. A patient in hosp. convalescent. 