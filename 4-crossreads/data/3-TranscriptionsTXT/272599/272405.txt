 [Page 132] 
 1917 Monday 14 May 
 Duties. In aft. Jock Heylin &amp; self walk out into the hills.   Wimille.  Hunchback nails me.  French lesson  Country lanes, hedges. Garcon &amp; wayside water spring. Column. A magnificent view. Cafe. Three in one. Home by easy stages. These rambles are fine. Boulogne &amp; its surroundings are very picturesque. 