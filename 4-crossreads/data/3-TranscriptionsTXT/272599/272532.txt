 [Page 259] 
 1917 Thursday 11 October 
 Afternoon off.   Jock Mac B Wilson &amp; self into B &ndash; walk   Jock late as usual.  Call at D.D.M.S. office &amp; find I am next for leave probably Sat or Sunday.  Buy Xmas cards. Payne, the gassed case evacuated in Blighty &ndash; a good save. 