 [Page 7] 
 diging afternoon, took few photos in camp after parade. Writing. raining heavy afternoon 2 showers. Parcel Mrs Landers Socks. 
 Feb 17th Thursday. 
 Signalling &amp; Musketry till dinner time. Dressed for Cairo left camp 3Pm. on foot to Road West of 353 &amp; caught motor transport from the 9 Kilom peg geting to Ferry Post at sunset. Had fast tea turned in 11pm. 
 Feb 18th. Friday   Up at 4am walked to Ismalia stn caught train 6am arriving Cairo 9.40 Much business, went to Heliopolis to see Derek &amp; Syd about 5 to 7pm. then back to Cairo, finished shopping &amp; went to my Room. Hotel Metropol 9pm. 
 Feb 19th Sat. Up 5.30am Cold bath dressed &amp; walked up to 
