 [Page 12] 
 hell itself &amp; perhaps a few hours afterwards when you think you are quite safe, well behind the lines &amp; out of range an isolated shell will get you as I have seen personally to happen. tis one thing I always (when things were hottest) consoled myself with was, well, if my time was up, it was up &amp; that was all about it, no use to dodge it. 
 Well [to get on with my narrative; nothing worthwhile happened] 
 