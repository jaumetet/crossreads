 [Page 1] 
 Confidential Copy 
 Board of Trade Timber Supplies Dept. Balfour Buildings, Haverfordwest. 1st. December 1917 
 List of Haverfordwest Administrative Staff other than Officer in charge as existing Nov. 30th with summarized description of duties. 
 Names &ndash; Duties &ndash; Remarks 
 Mr J. Richards &ndash; Accountant.  Records &amp; accounts of wages for Foreign &amp; British Labour, local bills &amp; payments, Banking record &amp; general acturial work. &ndash; Present Wages &pound;5.10.0 per week  To be given greater responsibility &amp; entire charge of bookkeeping with rise of 10/- bringing wages to &pound;3.0.0.  G.W. Lambert 
 Miss E.W. James &ndash; Correspondence Clerk &amp; Clerk over general records, also to assist with accounts. &ndash; Present Wages 25/- per week, recommended for rise.  G.W. Lambert 
 Miss V.A. Bland &ndash; Correspondence Clerk, Typist, also to assist with records of local bills &amp; payments etc. &ndash; Present Wages 20/- per week, recommended for rise.  G.W. Lambert 
 L. Lewis &ndash; Clerk over Transport, records of goods received by rail, also record of Dept. &amp; other haulage, messages, telegrams, &amp; general factotum. &ndash; Recommendation, that responsibility &amp; wages be increased if present progress maintain.  Rise recommended 5/-.  Present Wages 25/-. 