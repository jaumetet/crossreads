 [Page 54] 
 3/7/16 
 Poor food  -tea &amp; tea. Try to align key contacts &amp; break same. 
 4/7/16 Same old food. Buy salmon &ndash; bad. Heated argument over Melb &amp; Sydney 
 5/7/16 Terrible stew that makes us crook. Tea that is vile. Practice with calico aeroplanes ships. George Hamilton gets a cake from Australia. 
 6/7/16 More bad food. Aeroplane warning from Romani 
 7/7/16 Officer tearing about on horse 2am looking for patrol. 
 8/7/16 Artillery and machine gun guns etc out on an imagined attack firing live, shell overhead. Stoke guns also making big noise. TAT TAT TAT TAT  All over the place. 
 9/7/16 Rice for tea hailed with expectation &ndash; uneatable &ndash; bitter &amp; mushy &amp; as usual no milk or sugar. Catch a young snake. 
 10/7/16 George finds a chameleon that causes great amusement. No letters again. Plum duff for tea that is as heavy as lead. 
 11/7/16 Aeroplane message from Romani. Catch another chameleon. No letters again, terrible food. 
 12/7/16 Catch another chameleon which is called Hector. Cuthbert is others's name. (note. No 13/7/16) 
 14/7/16 Still bad food. Arthur's birthday. 
 15/7/116 Payday. Come to rescue of the fiend who missed a message from Ismailia &amp; generally does his block 
 16/7/16 have a great blow out &amp; get sick through eating. have some of Davies' bully beef with "hooks" in it or rather suspected of having such. 
 17/7/16 Bunny Brett leaves for Pt Said prepared for a big parcel of goods 
 18/7/16 War news begins from Ismailia. Message missed owing to jamming &amp; is not received till 9am 
 19/7/16 Bunny returns from Pt Said with photos goods etc. Receive three letters at last. 
 21/7/16 Taube up again flying over Kantara. Overhaul generator to get 16 strength from Ismailia. 
 22/7/16 Taube up over Kantara. 
 23/7/16 Aeroplanes up over Tantara 
 24/7/16 Rig up buzzer for the fiend &amp; send him war news. Have to get others 