 [Page 71] 
 -2- May the thought that he died a glorious death for his God &amp; his Country help to console you. We personally were very fond of dear Arthur, &amp; Pat, who is in France, felt it dearly, he was writing to you. My own dear eldest 
