 [Page 46] 
 his company, when a German 5.9 in High explosive shell came through the roof of the dugout. 
 If I can possibly find out any further particulars later, I will certainly let you have them. 
 All the men under his command were very fond of him, &amp; had the utmost confidence in him. 
 Please accept my heartfelt sympathy &amp; believe me to be 
 yours ever sincerely 
 W H Cullen 
