 [Page 67] 
 Pte J F Mant 3rd Div Cyclist Coy 
 26 July 1916 
 My dear Mrs Ferguson 
 Will you please accept my very deepest sympathy in your great loss. Arthur was one of the finest fellows I have had the honour of knowing &amp; it has left a blank in my life. 
 Believe me, Mrs Ferguson, 
 Yours very sincerely 
 John F Mant 
