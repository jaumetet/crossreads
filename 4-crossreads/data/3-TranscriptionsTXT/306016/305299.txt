 [Page 7] 
 France 11/5/17 
 Dear Mother &amp; Father 
 This is a group taken in Shrewton Village and all the lads shown here were in "Isolation" with me, and belong to our reinforcement, that is excepting the two guards. 
 From left to right:- 
 back Row standing: Corp. Stewart (guard) Ptes. Kelly, Urry (Guard),  Piggott , Brimblecombe,  Hayes , A. Cryer, Thompson, F. Cryer, O'Connell, L.Cpl. Pepper. 
 Middle Row Seated Ptes. Kearins,  Walker , McRae, Atkins, Mott,  Taylor , Williams. 
 Front Row, on ground Ptes.  Cavanagh ,  Lloyd , Williams (Snowy), Walsh. 
 My favorites are underlined, but most of the boys are nice in their own way, if once properly known.  As the photographer lost the  film  plate, this is the only copy I could get. 
 Yours affectionately, Duncan 