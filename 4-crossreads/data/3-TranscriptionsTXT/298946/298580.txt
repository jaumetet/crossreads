 [Page 42] 
 31 
 troops only. 
 This arrangement gives the troops a  maximum of at  the most space possible and does not give the women a fair chance.  However, it would be useless to further restrict the room. 
 Owing to the accidents which occurred in England the crew came to regard this as an interesting, if not mysterious ship.  Only one breakdown has occurred, and that was soon remedied, with the loss albeit of a few miles in the days running. 
 Coincidences have happened in &amp; around this ship. 
 We left England in on exactly the same day of the month I left Australia to come over.  That was only three days later in the month 