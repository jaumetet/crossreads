 [Page 50] 
  Saturday 16th Aug  
 For three hours this morning the boat was stopped; rest of the day she has crawled.  Another boat, a very slow one passed us in the afternoon.  I understand that we shall get back to normal at midnight tonight, once all repairs will be finished then.  After we should make a good speed. 
 Boxing contests tonight, a dance &amp; also Jazz band invitation down below.  The last enough to make a cat laugh. 
 Setchell told me some illuminating things concerning faith really practised among such as the Plymouth Brethren. Some instances he gave me of his own results were truly remarkable. 
 Parliament opens on Monday with [indecipherable] as leader of the Government &amp; myself as leader of the opposition. 