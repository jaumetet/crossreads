 [Page 95] 
 second man's death being assisted by Ricks. 
 Friday 16th June 
 Yesterday I was appointed dispenser for A.M.C. details.  Took sick parade &amp; fixed up medicines. Later in the day we moved back &amp; pitched tents in Miscellaneous Details again. Took sick parade again this morning. Told about a dispenser wanted in the 6th Light Horse, &amp; am debating on it now. Moved back to Miscell. Details. 
