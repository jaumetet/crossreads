 [Page 127] 
 Wednesday 2nd August 
 Arrived Alexandria at 4 in morning &amp; on board by reveille. Found 5th Div. &amp; Luna Pk. crowd already settled down.  Stuffy cabin in aft of lower deck, but lucky to get cabin instead of hammock.  "Franconia" is a big boat a Cunard I believe &amp; by the time we sail (in an hours time I believe &ndash; i.e. 6 p.m.), there will be 5,000 troops on board &ndash; a nice haul for a submarine. Lt. Col. Ramsey Webb is the P.M.O. I believe, so 
