 [Page 134] 
 Sat. 5th Aug. 
 for the second time our A.M.C. details changed for another boat. Now we are 18 D boat with B Company of the 2nd Div.  Had a passage of arms with Lt. Kerr over the fooling about from boat to boat.  Our own Sgt. (Cpl. Weir has been given an extra acting stripe for some obscure reason) lost himself as usual &amp; turned up when parade was nearly over. 
