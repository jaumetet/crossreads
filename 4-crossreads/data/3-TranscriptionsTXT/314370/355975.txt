 [Page 29] 
 February, 1916 Tuesday 1 After a beautiful sleep we get up &amp; have a look round the town  After dinner we go in a gharry round the docks &amp; have a look at the shipping Nearly all ships are armed w/-  light naval guns. Turn in at 2 am 
 Wednesday 2 Same as yesterday but spend afternoon in the park &amp; have a look at the Refugee camp where several thousand Armenians, Albanians &amp; Syrians are being looked after At night visit a couple of shows &amp; turn in at 2 am 
