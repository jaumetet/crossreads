 [Page 44] 
 March, 1916 Thursday 2 Caught the 12.45 train from Cairo out to Barrage.  This Barrage is situated on the Nile Delta &amp; consists of a number of Weirs &amp; Locks &amp; used to regulate the Flood Waters of the Nile in this particular region.  The whole is a very fine piece of Engineering &amp; looks very fine surrounded by magnificent gardens, 
 Friday 3 Having discovered that Jack Davidson was w/- the 1st T.B. I set out to find him &amp; was lucky to strike first try.  Very pleased to see each other &amp; had a lot to say about old times Played cards in the evening w/- Dad. 
