 [Page 42] 
 February, 1916 Sunday 27 After breakfast I wrote a letter home but forgot to post it After Dinner Dad &amp; I &amp; Dad's boy went for a run into Cairo &amp; out to Rod-el- farag the Nile docks. After Tea we went to the American Cosmograph 
 Monday 28 Monday morning was spent making the dugout more like a home &amp; less like a dump. I also received a welcome surprise got a parcel from home sox etc.  God knows how long it has been coming. Spent evening w/- dad 
