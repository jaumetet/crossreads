 [Page 27] 
 January, 1916 Friday 28 Final paper &amp; inspection of guns Am very glad it is over &amp; we have a week's holiday to look forward to. 
 Saturday 29 The men returning to Tel-el-Kebir march out at 10 am. Spend afternoon &amp; evening w/- dad. 
