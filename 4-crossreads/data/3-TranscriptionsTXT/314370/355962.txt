 [Page 16] 
 January, 1916 Thursday 6 I found out today that when my name was sent in to go away with the details to join Battn. that it was struck off the list by the adjutant but I could not find out why.  We have a very good concert in the Picture Show tonight 
 Friday 7 Draw P.T. 500 pay &amp; just afterwards get orders to report back to School of Inst as a M.G. Instructor.  9/13 get orders to march out on Saturday morning. 
