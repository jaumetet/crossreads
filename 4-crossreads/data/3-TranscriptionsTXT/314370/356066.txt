 [Page 120] 
 August, 1916 Monday 7 Made good time all day Making in direction of Marseilles, so our destination is now certain 
 Tuesday 8 In sight of the harbour about 2.30 pm. Pulled into wharf by tug at 5 pm. First to disembark when all were off we marched behind the band to rest camp about 2 miles from the town. got into tents about 10 pm damned tired 
