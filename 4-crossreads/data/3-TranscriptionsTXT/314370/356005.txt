 [Page 59] 
 April, 1916 Saturday 1 Soon after breakfast the men for the new school marched in. Keep coming in batches all day. After tea we went for a walk to Zeitoun 
 Sunday 2 Men continue to come in all day.  I was on duty from 12 to 4 pm. No word from dad. At 4.30 pm I went into Cairo for a final ramble before starting work tomorrow. When I returned about 9.30 pm, I found a note from the store to come &amp; get dad's bag as they were going to Tel-el-Kebir at 6 am tomorrow.  I got 'em. 
