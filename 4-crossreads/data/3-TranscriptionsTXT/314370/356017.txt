 [Page 71] 
 April, 1916 Tuesday 25 Caught the 4.15 express to Alexandria.  Arrived about 8 pm &amp; put up at the Windsor Hotel After dinner went  roaming round town till 12 mid.  Turned in very tired Anzac Day 
 Wednesday 26 had a lovely sail on the Harbor &amp; a swim off the breakwater. in afternoon went to see the Catacombes &amp; Pompey's Pillar Both are splendid pieces of work  one on top of a hill &amp; the other inside the hill. 
