 [Page 57] 
 since landing on the Island &amp; it caused great excitement as each mail call blew! 
 April 1st We had a day off on account of No 8 platoon being called out the night previous to assist the guard to guard the 30 odd prisoners who were somewhat roudy on a/c of fearing that they were to be removed to. Malta. Their behaviour entitles them to the just punishments which they were sentenced. The prisoners left this morning for Malta 
 April 2nd Good Friday church parade in morning &amp; rest of day off &amp; went for a swim in afternoon 
 April 3rd Went on a washing and bathing parade in morning &amp; had rest of day off. Two of the Cruisers which left the harbour yesterday were attacked were attacked by a hostile Aeroplane but received no damage. An English Aeroplane is in pursuit of the foreign one. 
 April 4th Easter Sunday church parade. Mounted guard at 3.30 P.M. on the pier. We were on 
