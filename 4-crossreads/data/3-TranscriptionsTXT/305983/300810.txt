 [Page 15] 
 4/9/17 cont Germans say that a telegram came into camp about the fall of Riga but they do not believe it. said  I expect they want another war loan.  Bread issue raised to day from 6 &frac12;  to loaf to 5 &frac14; per loaf the difference is hardly noticable.  We are off to another camp tomorrow at noon.  Where to? 
 5/9/17 
