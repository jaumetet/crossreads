 [Page 28] 
 17/10/17 nothing of interest doing other than England has stopped cable &amp; telegram messages within Holland 
 18/10/17 Nippy day. braces one up somewhat.  Germans take soup powders &amp; ox cubes out of packets &amp; put them in soup issued to us.?  Fleas troublesome again. 
 19/10/17 Nothing of interest today other than packets arrived.  I received the first one for a month today  a letter from Mrs Groves dated 18/5/17.  half of it cut away.  Germans did put ing  soup powders in soup. 
 20/10/17  Very  heavy fog to greet the eyes this morning &amp; did not lift until about 11 oclock.  it was cold too standing about.  Zeppelin passed in the distance sometimes.  football match Monday &amp; Wednesday 
 21/10/17 At roll call last night we fell in with german issue Bowls Spoon &amp; wash basin what a fine display well the Commandant he gave us a bit of German culture awful things.  The fog is still very heavy 
