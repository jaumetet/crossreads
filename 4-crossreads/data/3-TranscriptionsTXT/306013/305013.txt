 [Page 43] 
 [Brief notes of the diarist which appear to be the basis for diary pages 40, 42 and 43] Mon Jan 1 No drill boxing plum pudding reading writing 500 
 Tues. Jan 2 Slept on forc with Liverpool [indecipherable] a name="a3820040"  
 [Page 40] and sought the coolness of the forecastle head.  and    There the stars looked down and understood my feelings, the kindly moon  with her diffused, sympathetic beams, knew how seasons come &amp; go, and she could understand me; the cool breeze flowing from the peaceful ocean knew my thoughts &amp; understood, and to these &amp; to the ruler of the stars &amp; the winds I could command all things, &amp; to Him I can look for guidance in the Year which commences tomorrow. 
 "Ring out the old, Ring in the New" 
  1917  
  Jan. 1. New Year's Day . About 110 miles from Cape Town, on the Atlantic Ocean;. 
 The beginning of the day seemed so ordinary that it was very hard to realise that the day was a momentous one. However we were given a holiday from drill and in the morning witnessed some good boxing contests &amp; 