 [Page 27] 
 1st March Weather very dull. The S.S.Maloja reported sunk off Dover. The French fort of DOUAUMONT N.E. of Verdun has been recaptured by the French. 
 2nd Rumour persistent that we go to France. Sub-Lieut. two Petty Officers and six sailors come to stay with the Battalion. Severe sand storm. Lt Hoskins promoted Captain. Cpl.Jenkins promoted to 2/Lt. 
 4th Visited out-post and got lost. 
 5th Nothing 6th Move from KANIMBLA VALLEY to FERRY POST WEST. A hard and fatiguing march as the men had full pack and blankets to carry. 
 7th Left FERRY POST WEST 9am. The march through ISMALA was very tiring even the sight of the green foliage and habitation did not revive us. We continued to march for hours in the hot mid-day sun. En route we passed by various hospitals, among which was an Indian Hospital under the shade of the trees. A GHURKA stood guard with his drawn knife. Arrived at MOASKAR about 2 pm and very glad to find tents already pitched. 
 8th Battalion marched to the lake for a swim, band leading. Every one inoculated including myself. A general Officers Mess meeting held. Major Paul moved that all spirituous liquors be cut out of the mess. Mess showed a profit of 46 pounds A lecture was given to the Officers on the use of gas helmets. 
 9th Volunteers are asked for the Divisional Cyclist Corps. All clothes are fumigated. All temporary Majors are to revert to the rank of Captain. Weather is exceedingly hot. 
 10th) 11th) 12th) 13th) Nothing 
 14th We are refitting and expect to leave for France on the 17th. Ismalia is a very pretty little town, at present full of military hospitals. Reported that there are drums there, and our men are said to line up, 30 deep and await their turn. The club is a very fine building and is thrown open by the people of Ismalia for the use of all Officers. 
 15th) 16th) Nothing 