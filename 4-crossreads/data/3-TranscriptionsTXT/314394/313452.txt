 [Page 17] 
 Sept 19th Church parade 
 20th Sailing vessel sunk in sight after the enemy had fired 50 shells at it, a Greek ship laden with wood. 10090.0.0 pounds drawn for pay. Orders taken for winter clothing. 
 21st Another mail arrived. 
 22nd A man named Hill had his hand and arm blown off through his holding a bomb too long 
 23rd Nothing 24th Something big in the way of a Stunt expected. Demonstration by us with co-operation of Navy and Heavy Artillery. I was on duty in the trenches and  as the M.G. had not fired I sent word to find out why, they replied blown out  come and see" Lieut Richardson and a Private had their eyes badly damaged by earth which was blown into them. 
 25th Very quiet. 
 26th Pte W.V. Power was hit on the head in No 5 sap . I was observing with an Artillery Officer just before, I handed him the periscope and as I did so a bullet came through the bags and hit Power. He was in pain and would be unable to write for some time so I wrote to his Mother. 
 27th Nothing 28th Message received from Lord Kitchener. Things are going well in France, on Sept 27th 23000 prisoners and 40 guns with more to follow". This should liven things up in the East. Mine exploded on the beach and 8 men rushed in to see what had happened and were overcome by the fumes and died. 
 October 1st Clouds obscure sea and ship 
 2nd Nothing 3rd Sunday Church Parade 4th Nothing 
 5th Note thrown into trenches stating that a party of the enemy wished to surrender. A note was thrown back with instructions as to where and when to surrender. A number surrender at Poes Post. Queen Alexandra's, comforts issued, did not amount to much for each man. 
 6th Nothing  7th Nothing  8th Shell landed in Main Street killing one wounding four. 
 9th Severe storm at night. 
 10th Lieut Richardson went away ill. 
 11th New firing line opened at the Neck. Lieut Col. White and Adjutant of 8th Light Horse brought in from ground recovered. Bodies in very decayed condition. 
 12th,13th,14th Nothing 