 [Page 95] 
 [Ribbon in the colours of the 53rd Battalion's colour patch &ndash; green and black] 
 With the "Whale-oil Guards". 
 2453 J. Marshall 53rd Bn. A.I.F 
 13/12/16 to 20/7/19 
 [Transcriber's notes: Beaulencourt spelt Beaulaincourt Bertry spelt Bertre Bollezeele spelt Bolzeele Esquelbecq spelt Esquelebech Le Chatelet spelt Catelet Sainte-Radegonde spelt St Radegronde Villers Bocage spelt Villers Bacage Villers Brettoneux sometimes spelt Villers Brettonneux] 
 [Transcribed by Barbara Manchester and John Stephenson for the State Library of New South Wales] 
 