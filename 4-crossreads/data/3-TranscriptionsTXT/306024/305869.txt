 [Page 35] 
 [Transcriber's note: There appears to be a section missing here.] 
 leaving one of their own around lying nearby badly wounded and later we came across a 54th Bn chap with a smashed thigh, lying in the open with shells falling all around, so we determined to see that they shifted him. 
 I went scouting around to look for a stretcher and going into what I thought was Halle came on a ghastly sight. The cross roads had been the scene of some severe fighting and lying there, unable to lose consciousness was a 54th chap with the bare stump of his closest pal in front of his eyes. Just as I was wondering what to do along came an 
