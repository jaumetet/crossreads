 [Page 3] 
 [Transcriber's note: This page consists of a list of decorations awarded on 13 December 1918 and includes a had-written note:] 
 The Bn's decorations for the Peronne stunt. 
 [Following is a transcription of the typed list of decorations:] 
 Special Order. by Lieut-Colonel W. J. R. Cheeseman"DSO"MC" commanding 53rd Battalion A.I.F. December 13th 1918. 
 His Majesty the King has approved of the award of the following Decoration. 
 Victoria Cross 1584a. Pte. Currey.W. M. 
 Under Authority granted by His Majesty the King the Field Marshall Commanding in Chief has awarded the following Decorations:&ndash; 
 Distinguished Service Order. Major J.J.Murray. "MC". 
 Bar to the Military Medal. Captain W.S.Smith [indecipherable] Lieutenant W.Waite. "MC". 
 Military Cross. Captain W.F.Lindsay Lieutenant W.Bevan. Lieutenant J.Dexter. Lieutenant A.W.Cooper. Lieutenant A.J.Tofler. Lieutenant G.A.Young. 
 Distinguished Conduct Medal. 3467.T/CSM. Burns.C.R. 2962.T/CSM. Wood.J. 2153.Corpl. Crank.R. 2283.L/Cpl. Weatherby.C.J. 2247. Pte Smith.O.W. 2642. Pte. Cameron.R.C. 2768. Pte. Gilmore.H.J. 
 The Corps Commander has awarded the following Decorations:&ndash; 
 Bar to the Military Medal. 1753.L/Cpl. Willard.H.C."MM". 4875. Pte. Sullivan.J.S."MM". 
 Military Medal. 3498. Sergt. Croker.J.E.B. 3408.Sergt. Scully.V.J. 5406.Sergt. Lineham.C.C. 3256.Corpl. Baker.R.J. 1720.Corpl. Rayner.C.R. 5472.L/Cpl.Turner.A.J. 1142. L/Cpl. Brown.W.C. 5479. L/Cpl. Webb.A.G. 4852. L/Cpl. Smith.A.E.L. 3353. Sig. Alexander.A.J. 3427. Pte. Smith.E.J. 2444. Pte. Marsh.C.S. 2171. Pte. Greenhalgh.W.J. 5405. Pte. Lette.B.I. 1887. Pte. Clarke.G.F. 3259. Pte. Barron.E. 5390. Pte. Hopkins.A.J. 869a. Pte. Payne.A.C. 4662. Pte. Wilson.W.S. 
 The Corps Commander wishes to express his appreciation of the gallant services rendered by the undermentioned Officer who was "Mentioned" in the Australian Corps Routine Order No 48. of 17-10-18. Lieut-Colonel W.J.R.Cheeseman "DSO"MC". 
