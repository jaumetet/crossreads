 [Page 15] 
 [Fevrier 23 Dimanche] Had a trip on Nile in a Feluea with [indecipherable] &amp; two others had good time flying two sorties sent [indecipherable]. 
 Sgdn. goes on 7th [indecipherable] D.V. 
 [25 Mardi] Dinner at Shephard's with Mjr. Addings, Beaton, Glass, [indecipherable] &amp; [indecipherable] Champagne.  Sqdn leaves on Sunday for Aust. &amp; I leave with them. 