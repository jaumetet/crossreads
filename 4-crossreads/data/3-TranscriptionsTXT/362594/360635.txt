 [Page 5] 
 [Janvier 11 Samedi] Played Golf with Bedoss at Sporting Club in afternoon my handicap is 10.  Went round in 43 for 9 holes 
 [Janvier 12 Dimanche] Played billiards &amp; golf. 
 [Janvier 13 Lundi] Went solo after 9 h 40 in dual with instructor.  flew well as I have been fit to go off for some time.  I was inclined to look for the chap in front after flying 480 hours with a pilot.  Had absolute confidence in self but not so much in my engine. 
 At 400 ft engine cut out over rough country put up great fight for a clear patch missed by 10 ft crashed badly, machine total wreck, belt breaking round me.  My most marvellous escape in the war. 
 Went up again immediately &amp; got on well, confidence naturally varies, but I can beat such things with a small effort. 
  I am watched over by a guardian angel  