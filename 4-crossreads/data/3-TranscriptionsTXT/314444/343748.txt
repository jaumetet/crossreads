 [Page 469] 
 Commonwealth of Australia. 
 Dept. of Defence. Base Records Office, A.I.F. Melbourne. 20th May, 1918. 
 No.1825 
 Dear Sir, 
 I have much pleasure in forwarding hereunder copy of extract from Second Supplement, No.30424, to the London Gazette dated 11th December, 1917, relating to the conspicuous services rendered by your son, the late No.874, Sergt. T.H. Hill, 19th Battalion. 
 Awarded a Bar to Military Medal. "His Majesty the King has been graciously pleased to approve of the award of a Bar to the Military Medal to the undermentioned non-commissioned officer:- No.874 Sergeant T.H. Hill." 
 The above has been promulgated in Commonwealth of Australia Gazette, No.66 dated 2nd May, 1918. 
 Yours faithfully, (sgd) J. M'Lean. Major. Officer i/c Base Records. 
 Mr A.H. Hill, "Euterpe" Kurraba Road, Neutral Bay. N.S.W. 
