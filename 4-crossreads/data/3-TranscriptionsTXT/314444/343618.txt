 [Page 339] 
 On letterhead of Australian Imperial Force, Base Records Office, Victoria Barracks, Melbourne.] 8th February 1918 
 Dear Sir, With reference to the report of the regrettable loss of your son, the late No. 7247, Private J.P.G. Griffin, 13th Battalion, I am now in receipt of advice which shows that he died at 2nd Australian Casualty Clearing Station, Belgium, on 31st August 1917, of wounds received in action, and was buried at Military Cemetery, Trois Arbres. 
 These additional details are furnished by direction, it being the policy of the Department, to forward all information received in connection with the death of members of the Australian Imperial Force. 
 Yours faithfully, [Signed:] J M Lean Major. Officer i/c Base Records. 
 Mr. T. Griffin, Cathcart. N.S.W. 
