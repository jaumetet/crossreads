 [Page 225] 
 ever met Capt R.V. Pollak of my Regt, who was staff Capt to Ryne? he has told me a lot about the Dardanelles show, &amp; was I believe well liked by the Anzacs. 
 I suppose those kiddies of yours are as big as Father now, I hope you are all keeping fit &amp; doing well. 
 It is a long time since I heard from anyone in Sydney, except Vera Healey who writes me very cheery letters. Wymark hasnt written for a year &amp; a half, wonder if he got my letters, but the past is uncertain now. You might remind him, will you, &amp; when you get time write yourself &amp; give me all the news. Give my Regards to G.R, Wymark, Shenstone, McCure, Lewis, &amp; all the rest who are left there. 
 With Kindest Regards to yourself, Mrs Spencer &amp; the kiddies, Yours very sincerely, F. De Groot. Lt 15th Hussars. 
