 [Page 242] 
 devastating work of the Hun in this part one can easily see why all are in arms against an inhuman foe who has spread desolation over as fair a land as one could wish to see. 
 Let us hope that your share of sorrow has ended and that through the years yet to be many joys may be yours to minimise the effects of this. With this wish Believe me to be your ever sincere friend J. F. Garaty 
 [Sergeant-Major, later 2nd Lieutenant, John Francis Garaty, 35th Battalion] 
 