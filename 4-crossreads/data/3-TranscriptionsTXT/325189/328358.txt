 [Page 44] 
 have done a solid day' s graft to-day drilling on the desert.  It is hard work walking on the sand &amp; the dust absolutely clogs throat nose rifle &amp; everything else up.  The reinforcements drill together.  The others in our battalion are out to-night bivouacking somewhere in the desert. 
 16th March 1915 Tuesday 
 Heavy rain during the night &amp; morning. 
 19th March 1915 Friday 
 Looking in at one of the curio shops in the Camp last night &amp; saw some beautiful things.  There was a table in the construction 
