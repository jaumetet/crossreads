 [Page 67] 
 Received the 'dinkum oil'  &ndash; we start en route for the Dardenelles tomorrow. 
 4th April Sunday 1915 Easter Sunday 
 Church Parade.  Struck tents &amp; cleared up the Camp during the day.  Stan Cocking &amp; I picked to go as baggage guard.  7.30 p.m. 6th bn. marched off for Cairo.  We went at 10 p.m. &amp; reached the Central Rly. Stn. at 2 a.m.  This is 12 miles &amp; as we were carrying full packs, &amp; haversacks &amp; rifles it was not bad going.  Our feet are tender after walking on the soft sand.  Train left Cairo. 
