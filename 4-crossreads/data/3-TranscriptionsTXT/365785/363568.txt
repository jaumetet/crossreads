 [Page 91] 
 P.S.  How are the Owens nowadays.  Give Mrs. Owen &amp; Mifanwy my kind regards please.  A.M.W. 
 1st Dismounted Regiment Aust. Light Horse, Egypt 6.X.16 
 Dear Mrs. Read, Please excuse pencil but ink is just at present only sufficient to write addresses. I was very pleased indeed to receive your nice letter today, and hope you will favour me similarly again soon.  I shall appreciate it very much indeed if you do. Thanks so much for the news about Margaret, give her my love and the enclosed photo will you please.  This is my Dressing Station on about 4 miles of front somewhere in Egypt.  We have not been under fire here in the three months I've been here but we've 
