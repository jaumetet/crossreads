 [Page 134] 
 With love To All at "The Wurley" 
 From Mick 
 7th F.A. Brigade 
 Christmas Greetings 
 This Xmastide we're far away, Yet memories ever roam; A Merry Xmas &ndash; bright New Year To all at Home Sweet Home. 
 W.S.H. 
 1916 Lark Hill Camp Salisbury Plain England. 
 Gnr. George W. Bleach, 26th Bty. 7th F.A.B. 
 [Transcribed by Judy Gimbert, Betty Smith for the State Library of New South Wales] 
 