 [Page 31] 
 Monday 10th May Tired after digging all night, no ration this morning , mules run amock lost our rations poor us everytime. 
 11th May actually have a spell Roy Inwood cooks a good meal, shell burst on top of our trench nearly spoilt our meal, like to see that turk gunner, we sing Australia while Tune digs for shell, had a night's rest. 
 12th Back in firing line, pushing trenches forward towards Turks always digging though exhausted, Lieut Smyth give us cigarettes killed returning to beach chaps have a good time on rum fatigue one nearly buried for dead man 
 13 May spell day, had a good dinner, onions potatoes Bully Bacon tea sugar light horse expected lovely weather 
 Friday 14th Firing line again plenty schrap about no sleep all night, 
 Saturday on  water guard, shells keep us busy very tired hear Italy has joined Allies heavy Artillery fire on our trenches many wounded 3 killed. 
 Sunday on water Guard, shells as usual, had a bath washed my clothes, had a shave Cullen killed, tourist digging for shells Jack Tar comes up for Turk Rifle 