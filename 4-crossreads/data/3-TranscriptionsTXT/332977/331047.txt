 [Page 104] 
 1161 Pte Peter Miller B Company 2 Battalion AIF 26 Kinncaid Street Pulteneytown Wick Caithnessshire Scotland NB 
 Here lies the body of Pte Burke who lost his life while dodging work No3576 Pte G. Beadle No 5 Toulmin St Borough London SE Eng 
 Pte. S.A. George 65 Shephard St City Sydney 
 May the best you've ever seen be the worst you'll ever see May you always be as happy as I wish you for to be. GB Scott Gleniffer Goomboorian Via Gympie Queensland 