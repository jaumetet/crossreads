 [Page 91] 
 June 25 Hot as blazes in Red Sea 
 28 Passed Perium Hells Gates at 10-30 Aden 8,PM 
 29th Pay day out in mid ocean, very hot  &amp; developed Prickly Heat 
 30th very rough sick as a dog had no lunch all the lads are knocked out 14 days to WA cross the line on the second 
 2nd Cross the line at 4.30 very close still 
 July 4 th buried Pte Tucker 27 Batt, buglers play last post, firing party fire volley of three rounds, Dysentry 
 5 July Pte McDonald lost supposed to have gone overboard, man throws hot water over Sgt Gibbs 
 July 9th going good want men to handle coal 
 Saturday July 16th Arrived Fremantle Friday night anchored outside in roadstend pulled in to Wharf on Saturday morning troopships there bound for France, left WA at 12-30 for Melbourne. 
 Monday Good trip but very cold 