 [Page 7] 
 DATE &ndash; TIME &ndash; PLACE &ndash; REMARKS 
 13 &ndash; Serapeum &ndash; General Birdwood visits lines. 
 19 -  Bdge  Bde. Church parade march past G.O.C. 1st Anzac &amp; H.R. Highness Prince  ss  of Wales. 
 25 &ndash; Transport entrains for embarkation on "Maryland". 
 26/27 &ndash; Alexandria &ndash; Bgde. entrains for Alexandria.  9th &amp; 10th and Bgde. H.Q. embark Saxonia. 
 28 &ndash; 8.30 a.m. &ndash; Left Alexandria. 
 31 &ndash; Off Malta. 
 April 2 &ndash; Marseilles &ndash; 10th arrived at Marseilles (France). 
 2/3 &ndash; 1st train Bgde. H.Q. and 9th 2nd train 10th Batt. 
 3-4-5 &ndash; Through Orange, Montereau, Epluges, Abbeville, Hazebrouck. 
 5 &ndash; Godewaersvelde &ndash; Arrived and detrained.  Billited near Strazeel Moolenacker. 
 6/30 &ndash; Training, Gas helmet practice. 
 7 &ndash; Transport arrives. 
 20 &ndash; Sailly &ndash; 10th, 11th &amp; 12th march to billets at Sailly, divisional reserve. 
 25 &ndash; Anzac day Inspection by Gens. Plumer, Walker, &amp; McLagan. 
 27 &ndash; Inspection by Sir Douglas Haig.  Training continued. 
 May  12  12 &ndash; Gen. Birdwood visits Brigade. 