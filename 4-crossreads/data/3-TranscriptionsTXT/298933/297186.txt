 [Page 146] 
 Somme [September 1918] 12 Th Now in the vicinity of Peronne, the country is bare but it as not suffered much from shell fire 13 Fri Nothing much doing on this front, 2 hun planes brought down in flames.Brigade sports held 14 Sat Out on this sector we seem to be cut off from the world no news of anything and not even a cigarette 
