 [Page 181] 
 Sitting a well kept garden, recently evacuated before the huns advance the artillery is livening up right along the front, overhead the aeroplanes are soaring about in large numbers Before Amiens 6.4.18 
