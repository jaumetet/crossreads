 [Page 34] 
 The Salvation Army Divisional Headquarters 73 Goulburn St., Sydney 
 W. Bramwell Booth &ndash; General James Hay &ndash; Commissioner 
 Military Camp, Liverpool., N.S.W. 
 21st Decbr. 1914 
 Dear Mother, 
 Arrived this morning, and went right up and saw the Doctor. He passed me, though he tested my heart for a long time, and asked me if I had had any illness. I told him I had measles, mumps etc, but did not mention the diphtheria, because I thought he might have chucked me out. It is pretty miserable here in the rain, it has not stopped once all day, and the whole place is a mud-hole. 
 I'm afraid there is not much chance of my getting Christmas at home, as they have stopped all leave on account of a number of lads who get leave and do not turn up again. But you can be sure I will work it, if it is at all possible. 
 I saw Bob Newton this afternoon. He has joined, but will not be in camp till tomorrow morning. There is a pretty rough crowd here, but one hardly expects to find otherwise under the circumstances. There are 17 in our tent, which is supposed to hold 10, so you can imagine how cramped we are &ndash; and everything is wet &amp; slushy. Don't you think providing stationery etc. is a good idea?, but the nibs are awful. 
 With Love to you all Alan. 