 [Page 88] 
 Langwarrin Camp. Monday 31/5/15 
 Dear Mother, 
 I was very glad to get your letter today, and hear you were all well. It seems months since I have heard from anyone, and really it is about 7 weeks since I have had a letter. 
 It surprised me to hear of Dene's joining but I'm glad he has done so because I think anyone who can possibly join should do so now, its no time to be hanging back. And Uncle Jim is going too. My word I would like to be in his regiment; I'll bet he is a very popular Officer with the men. I saw in the Bulletin of Cairns getting his commission. 
 I am still doing Guard here, but hear that something is going to be done about us, at the end of the week, so perhaps  I may be home for Sunday dinner. We are being fed very well here and though it is cold, I am getting very fit, and quite ready to go back to the front and do  my little bit this time &ndash; and I'll take good care I don't get any more influenza. 
 I had a letter from Helen today also. Was so glad to get it, as I have not had a word 