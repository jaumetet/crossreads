 [Page 81] 
 Docks Rest Camp till 3.30 p.m. then boarded the "Viper" &amp; crossed the Channel to Southampton. 
 Mon. Nov. 27th: - Arrived at Southampton at 8 o'clock this morning (The "Viper" did not leave Havre till midnight). Entrained for London &amp; arrived at Waterloo Station at 10.30 A.M. put up at the Union Jack Club opposite station. 
 Thurs. Dec 7th: - My furlough &amp; good time is finished &amp;  left Waterloo Station at 4 P.M. this afternoon &amp;  aboard the "Viper" &amp;  away from Southampton tonight. 
 Fri. Dec. 8th: - Arrived at Le Havre early this morning &amp; put up again at the Docks rest Camp. 
 Sun. Dec 10th : - Left Le Havre by train at 6.30 this morning &ndash; on our way to Albert. Hear that my battalion has moved from Flesselles up to Mametz  Just passed through Rouen. 
 Mon. Dec. 11th: - Stop at Aust. Details Camp at Bellevue Farm just outside of Albert on the Albert-Bray Road from Midnight last night &amp; left at 10 A.M. along the Albert-Peronne Road to Manetz &amp; reported to Div. Hdqrs. Went back to Becourt where "C" 
