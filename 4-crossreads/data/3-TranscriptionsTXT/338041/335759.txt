 [Page 53] 
 Dec. 1st. Another raid on London &ndash; Unsuccessful &ndash; Fatigues &ndash; A trip to Picquigny and a visit to the "Duds" Vaudeville Show &ndash; A versatile comedian &amp; a "bon Orchestra &ndash; A meal earned by Patience but paid for by cash. 
 Dec. 2nd. Sabbath &ndash; Attend mass at village church &ndash; "A Knight of the Church (?) Waggon guard for 24 hours - [Indecipherable] Clarke's day-out. 
 Dec. 3rd. A free day granted us after strenuous (?) guard duties &ndash; Letter writing &ndash; Better weather &amp; the result on our boys &ndash; Assisting Q.M. Sergt. 
 Dec 4th &amp; 5th. General Duties &amp; Bad weather 