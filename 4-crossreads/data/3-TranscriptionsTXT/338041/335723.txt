 [Page 17] 
 Aug 2nd Contay in afternoon. We receive many more of our boys from the trenches. Vivid stories from the line. Day of vast preparation by British. Enormous shell supply goes to Albert 
 Aug 3rd More letters. Fierce night's bombardment of Somme Front &ndash; incessant 12 hours shelling by our Artillery. 
 Aug 4th Early morning (1a.m) celebration of Second Anniversary of Dec. of War. 
 Menu Mashed Potatoes Salad Salmon Cocoa Biscuits Fruit 
 Toasts "The King" "The Allies" "The Day" x "Our Fallen Comrades" 
 Second Division successful in a big objective &ndash; Fritz badly beaten. 