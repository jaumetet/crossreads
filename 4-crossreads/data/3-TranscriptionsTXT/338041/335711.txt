 [Page 5] 
 June (1916) 
 June 17th Nothing unusual happened during day 
 June 18th Frenchy Sunday &ndash;attend Mass at 5.30pm (6.30 by watch) &ndash; Artillery at Ypres silent &ndash; 2 Taubes brought down &ndash; I leave with party for Bois-Grenier 
 June 19th Day duty in Dressing Room &ndash; 
 June 20th Huns shell Armentieres &ndash; Church set on fire by their shells &ndash; Seen by us at Bois-Grenier &ndash; wilful destruction. Very few cases - Death of Von Moltke 