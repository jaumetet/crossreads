 [Page 59] 
 June 10th. German guns silent- Truth of Naval Engagement- Air fights &amp; pursuits. June. 11th. Verdun- Burial of lad from 20th Batt. Routine as usual. June 12th. Enemy guns silent- Ypres fighting severe- Artillery- Return to Ft. Rompu- A hasty march under wretched conditions June. 13th. Hospital duties- Mail- Quiet day. 
