 [Page 70] 
 July. 15th. Six months since sailing from Sydney.  Mail.  Easy day. July 16th. Sabbath.  Lecture on Gas by Major Anderson.  Mid-night trip to Amiens (N.Z. Hospital) July. 17th. Nothing unusual. July. 18th. Feet &amp; boot inspection- "The Ghost Walks." July. 19th. 
 [Transcribed by Lynne Palmer for the State Library of New South Wales] 
 