 [Page 50] 
 May 11th. Y.M.C.A &amp; Duties. May 12th. I witness my first Post Mortem Examination- Capt Culpin performs same.  Most interesting operation.  Y.M.C.A in Evening May 13th. Weather wet &amp; miserable- Artillery silent- Return of some of our "boys" from Bois-Grenier. May. 14th. Sabbath- Also "Mother's Day"- Attend Mass- Night duties.  Mail- Walk to Croix-du-Bac.  Quiet day. 
