 [Page 27] 
 Rumours of going to France- Active Service in reality- Inconvenience of washing etc- Sleeping out- Wet blankets 10.  Seventh Field Ambulance  Officially attached to Unit- our "14th. of 1st boys" in "B" Section under Major Brennan M.C- Impressions of Officers- Strict letter censoring- Announcement of Australians going to France- Issue of Respirators. 
