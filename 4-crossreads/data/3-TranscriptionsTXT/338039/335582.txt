 [Page 28] 
 Continuation of Diary. The voyage from Egypt to France. Mar. 13th. Inspection by O.C- Break Camp at Moascar- March with full kit to station.  Entrain &amp; leave Moascar. Mar. 14th. Monotonous train journey to Alexandria- Arrive at Port at 7 am.  Embark on A. 41- S.S. Minneapolis- Destination unknown- ship packed with men &amp; horses- Food poor- hammock issued. 
