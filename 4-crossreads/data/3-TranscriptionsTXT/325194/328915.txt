 [Page 67] 
 a few prisoners, 
 26/7/16 being releved to night  shelling not so bad but still plenty of it feel fairly tired no sleep etc. began to move out at 11.30 PM Fritz is still shelling the road but I think we will get through. 
 27.7.16 Got out alright marched althrough the night to Albert got there at 5.30 AM had a drink of tea and tried to sleep but was too tired 
 28.7.16 moved back another 9 miles slept in 