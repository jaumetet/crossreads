 [Page 61] 
 kicking around but cant carry em. 
 22.7.16 Our lads went over to night. We marched up in artillery formation hit a block in a communication trench Fritz put of three big ones wiped out about 20 , first blood for Fritz, We went over at about 11.45 not many casualties going over, charged over their first, 2nd and 3rd lines took the village had some fine stoush in the 