 [Page 50] 
 1.6.15 been here in billets about 8 days, the trenches were fairly quiet a few left their beanis on the parapets a few wounded  Capt Judge, one of the old boys got his. Some bombardment last night expected to go in but did not don't feel up to much myself, got that way I don't much care which way she blows Billy Hughes visited us 
 2.6.16 Nothing much doing went to tea at Sidonies wasn't very exciting 
 4.6.16 Heavy bombardment on the line thought we would be called out but was not 