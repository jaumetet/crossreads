 [Page 93] 
 July 26th Gov sent message thru telegram to Turkish commander stating that 700 British Troops here, actually 75 or so.  Turk tells him to wait &amp; see him drive the infidels away. 
 Col Bridges decides on going back as the Armenians are now 4 days overdue, I go along &amp; offer to take a patrol to the Lake 3 days off &amp; try to get in touch with the people.  Bridges says its impossible.  I point out it can be done by travelling at night &amp; following the river if needs be live on dry rice.  Wont hear of it.  I'm afraid he has got the wind up over the Turk telegram. 
