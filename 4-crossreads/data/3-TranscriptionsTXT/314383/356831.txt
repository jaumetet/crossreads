 [Page 14] 
 Go into Rest Station. 
 Jan 31 Entrained, off 2 pm.  The day clear &amp; fine.  Hitchock, Turner Stableberg and I share a 4th or 5th class carriage. 
 Feb 1st. Still travelling in this confounded dog-box &ndash; bitterly cold.  Able to get a few extras at various canteens en route which adds to the usual ration. 
 Feb 2 Arrived St. Germain 8 a.m.  Go into camp, Lyons not far but out of bounds, anyhow too tired to try any tricks. 
