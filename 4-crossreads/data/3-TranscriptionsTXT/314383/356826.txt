 [Page 9] 
 Jan 13.  Went to Church with Gracie, then Seven Kings at night.  All very much surprised to see me.  Stayed Seven Kings. 
 Jan 14. Reported A.I.F. H.Q.s.  Jogged about town all day.  Had lunch with Betty, caused quite a commotion on walking in on her and her pals at Wilkinsons for lunch. 
 Jan 15 Reported Tower of London (what a place).  There medically examined with remainder of force.  Canadian, S. African, New Zealander, Tommie and Aussies.  Very gratifying 
