 [Page 87] 
 &amp; proceed with Major Moore  &amp; escorted by the cavalry.  Beyond the names of the party &amp; the taking over of &pound;45,000 and &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;100,000 rounds of ammunition  &amp; 12 Lewis guns, no other information can be given until I am out on the road. 
 I remonstrated at the meagerness of the orders but nothing more would be told me.  I fancy it is not due to the secretness required but to the underhandedness of Seddon as he is proving himself to be a bit of a rotter.  The order is so [indecipherable] on a/c of its vagueness that I can afford to put it down in this old diary. 
 (over) 
