 [Page 42] 
 Sunday were practising the loading &amp; unloading of kits.  The mules are rather small but apparently very hardy.  Strapped to their backs are huge straw stuffed pack saddles.  The loads are attached by a long rope &amp; first by doubling    the length then half the  &amp; throwing the loop over the saddle to the further side half the load is then caught by drawing the loose ends around the load &amp; threading them thro other loop, the other side is then fixed in a similar manner, the whole being secured by a  sursingle. 
 April 29th Packing up ready to move tomorrow. 
 April 30th Muster at 4am, a hurried breakfast, then tents were downed 
