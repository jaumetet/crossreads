 [Page 5] 
 After my escape from Germany to Holland where I was forced to remain 5 weeks before getting to England where again I was kept 4 months before being shipped to Australia &amp; home. 
 I shall be glad if you will  purchase the  record for your archives. 
 Yours faithfully 
 Wesley P. Choat 
