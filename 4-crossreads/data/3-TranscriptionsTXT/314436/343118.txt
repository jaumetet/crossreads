 [Page 7] 
 [Pages 7 to 14 contain the shorthand notes made by Private F.J. Brewer and transcribed on Pages 2 to 4.] 
 [Transcriber's notes: 
 Minnen-werfer &ndash; usually spelt minenwerfer - German trench mortar minney or Minnie &ndash; colloquial term for the German minenwerfer] 
 [Transcribed by Judy Gimbert for the State Library of New South Wales] 
 