 [Page 54] 
 France 26.2.18 
 My dear Judge, I am still living very deep down under about 30 feet of earth but having quite a good time. 
 I received a very fine parcel two or three days ago but have no clue as to who sent it. The contents consisted of ginger, cigars, chicken, figs, and some small oddments all excellent. I wonder whether it was one of your contributions, if so thank you so much. 
 Every thing seems to go well over here: things are very quiet but one feels that  it is  there is a good deal happening during the lull 
 I am getting you some souvenir things made. I don't know whether you are keen on them or not, but most people are and these have a certain amount of sentimental value as they are all made by men in the Bn from material (mostly Bosche) picked up on the spot. I shall 
 [The following text is written along the left-hand margin] let you know when I send any of them off 
 Best Love Yours affectionately C R Lucas 
