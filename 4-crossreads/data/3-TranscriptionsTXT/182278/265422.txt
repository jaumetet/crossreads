 [Page 30] 
 Thursday 20.6.18&nbsp; Inspection by Wisdom&nbsp; very good turn out reconnoitring&nbsp; Wrote to Doff. 
 Friday 21.6.18&nbsp; [Jumping?] contest against RHA&nbsp; we won 
 Saturday 22.6.18&nbsp; Went for a long ride. Had a game of Bridge. 
 Sunday 23.6.18&nbsp; Letter from Doff&nbsp; played cricket, made 18 against men v officers 
 Monday 24.6.18&nbsp; Went to Bde during Wisdom&#39;s absence, to Concert Party at night after reconnoitring V.B. front. 
