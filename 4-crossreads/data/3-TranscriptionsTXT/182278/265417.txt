 [Page 25] 
 Raining heavily- men all working. 
 Saturday 25.5.18&nbsp; Letter from Doff&nbsp; Wrote to Doff- moving into line tomorrow night 
 Sunday 26.5.18&nbsp; Relieved Raper in Line&nbsp; had an amusing relief. no casualties. 
 Monday 27.5.18. Went around line, had a fight in No Mans Land- very warm 
 Tuesday 28.5.18&nbsp; Visited by G.S.O III Army showed him front- did a good nights digging. 
 Wednesday 29.5.18. Went around front with Lee- Bn working very well 
 &nbsp; 
