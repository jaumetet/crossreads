 [Page 21] 
 who has gone to Paris. 
 Thursday 2/5/18&nbsp; moved to CAMON&nbsp; I reconnoitred the front South of Somme arriving home tired after 20 mile ride. 
 Friday 3/5/18&nbsp; Men had a good rest&nbsp; Officers reconnoitring&nbsp; Huns bombed Amiens but did little damage 
 Saturday 4/5/18&nbsp; Reconnoitred area between Glisy &amp; Nicholas- moved to vicinity of Glisy. 
 Sunday 5/5/18&nbsp; Wisdom back- had dinner with 25 th . who celebrated 1000 days service. 
 &nbsp; 
 &nbsp; 
