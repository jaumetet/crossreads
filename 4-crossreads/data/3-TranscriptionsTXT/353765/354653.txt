 [Page 183] 
 July 1917 Expect to get it tonight. Personnel of  1/8th &ndash; C.B.F.A. splendid both in physique tone &amp; very keen &amp; well trained. Diary  returned  sent to Aust by Capt Logan who was returning with troops 
 [Transcribed by Donna Gallacher for the State Library of New South Wales] 
 