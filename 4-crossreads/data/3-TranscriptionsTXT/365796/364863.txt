 [Page 28] 
 1913 This is a retreat at the waterworks half way up Mt Wellington. We saw it on our way to Brown's River yesterday &amp; will see it again when we are up at Fern Tree. The whole country is very pretty &amp; the roads are splendid though very hilly. We were up 1600 ft yesterday. 
 Love from [indecipherable] 
 Alan Fry. Dene Gully. Northcote Rd. Lindfield Sydney. N.S.W. 
