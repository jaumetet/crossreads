 [Page 51] 
 11th May Friday Fine day, on Lewis Gun in morning, rest of day to ourselves, got paid in evening, had a very light day, rumours of Battalion moving up again, hope not, turned in early. 
 12th May Saturday In morning on Lewis Guns organizing Battn, in evening went and had a swim, afterwards went to Albert, had a good time, came back fairly late and turned in. 
 Jim and Evans, Mrs Hawkins, Miss Mosely, H T C Clarke, G Hank, M and D. 
 13th May In morning went on Church parade and had Brigade band with us, not bad, had nothing to do for the rest of day, still camped at Acid Drop Camp. 
 14th May Monday Still here, had some machine gun instruction in morning and evening, fine weather, had fairly light day, Capt Bond now over the Company. 
 15th May Tuesday In morning went on parade and done nothing much barring Lewis gun, had firing formation, in evening went to Albert, the town now very quiet, Bullecourt not all taken yet. 
 16th May Wednesday Still at Acid Drop Camp, Contalmaison, had a very light day owing to it raining, in evening had a bit of a lecture on huts, no letters lately, more home leave being given out. 