 [Page 136] 
 126 afternoon &amp; then return to billet to shift my quarters  &amp; when  to 72 Grande Rue &amp; when this is completed I speed off to the Dr for I have promised to dine with him. 
 After dinner which as usual is light &ndash; soup &ndash; eggs &ndash; ham &ndash; cheese &ndash; coffee &amp; liqueurs he receives a call from a patient near the big mill &amp; we all adjourn for a walk where the chestnut trees now bare arch overhead &amp; beside runs the eddying stream which serves the mill for power &amp; the fishermen for trout. 
 When visit is finished we return to house &amp; take our delayed coffee &amp; yarn over French system of Judicature of the Justices Tribunal &amp; Cour d'Appel. 
 I return to my new lodgings on explaining my lateness to the lady of the house she takes me into kitchen to her sister &amp; we yarn &amp; drink real good French brewed coffee till after midnight 
 26-4-19 (Sat) This morning I wake later &amp; decide that men can feed without my assistance. Before I can get out of bed the lady sends her little boy up with a cup of coffee for myself 
