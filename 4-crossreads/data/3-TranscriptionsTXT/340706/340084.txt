 [Page 140] 
 130 has been arrested 
 After this I visit the dentist &amp; with him the caf&eacute; &amp; Vasseur Reclerc, &amp; then to billet. I dine in camp &amp; then home where I write up notes of lectures &amp; letters. 
 29-4-19 (Tues) Rain &amp; snow with cold wind seems to have set in. However the snow yesterday all faded away by 9 am &amp; to day the fall during the night was not visible when I got up &ndash; so we are not doing too badly after all. London has had 1 ft of snow. 
 Visited the Saucets this afternoon 
 The relation of wife &amp; husband here different to our idea where the woman is as good as the husband. Here she acknowledges the [power] of protection that his strong frame &amp; character casts over her &amp; regards her life as belonging to him. His every  comfort  wish is forestalled &amp; every comfort anticipated. His paper is brought to him &amp; his shoes are put down alongside of him. Never have I seen the many little attentions that the wife lavishes on her husband exceeded or equalled among us. 
 The daughters seem content to stay at home receive visitor serve &amp; minister to their guests &amp; are quite happy in that [indecipherable] duty though they are for most part very intellectual &amp; deeply educated. This afternoon is 
