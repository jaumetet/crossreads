 [Page 194] 
 of our own chaps added a few selections from the vast collection of ditties* sung by the A.I.F. &ndash; mostly snaps from Music Hall s  songs, while others, written in the war zone, were in pidgin French, or a mixture of French and English. The following  is  is a stanza of a French song equivalent to our "Tipperary". &ndash; 
 "Apres la guerre est finie Soldats Anglais partis Mam'selle Francais Beaucoup pleurer, Apres la guerre est finie!" 
 Raucus voices, some of the owners being "well "vin-blanced"+, chanted the above popular verse very frequently this evening, or the following adaptation of it, wherein the ingenuity of the Australian in twisting and localizing things, is apparent:- 
 "Apres la guerre finie Soldat Australien depart Mam'selle Francais In the family way, Souvenir Australien." 
 The concert also included the interminable "parley vooing" of "Madamoiselle from Armentiers", 
 * Appendix 12 &ndash; a collection of some of them. + Intoxicated with white wine  or  (vin blanc). This wine, some of the A.I.F.  called "point blanc". 