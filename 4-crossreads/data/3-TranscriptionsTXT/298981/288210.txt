 [Page 61] 
 Suvla Bay 
 position the New Zealanders gained a valuable ridge, and captured many prisoners and a few machine guns. 
 Thus the fighting at Suvla Bay concluded except for occasional attacks of a small scale. 
 We then paid attention to consolidating our positions, and the preparation of our quarters on account of the rapidly approaching winter months.  Patrols were sent out every night to keep guard against surprise attacks, and artillery duels were of a frequent occurrence, but little more fighting occurred. 
 The New Zealand engineers greatly strengthened the firing stand recesses by means of wooden stakes which were bound together by wire. 
 They lined the tunnels, which were thirty or more feet below the earth and were very warm, with all necessary timber. 
 They strengthened our barbed wire barricades, and superintended the digging and laying out of all saps. 
