 [Page 11] 
 From Australia to Gallipoli 
 advancing to the attack giving the whole much the same appearance  as that seen a little further north at Gaba Tepe where only a few hours later it became our turn to reinforce our gallant heroes who were in great need of help if success was to be obtained. 
