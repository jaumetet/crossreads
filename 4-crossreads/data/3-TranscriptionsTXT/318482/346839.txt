 [Page 13] 
 Army Form B 295 
 Regiment &ndash; D.H.Q. 
 Pass 
 No. &ndash; (Rank) Capt. (Name) Lambert and Assistant has permission to be absent from headquarters, from 10000 on 20th July to completion of duty same date for the purpose of proceeding to Ismailia (Station) Moascar. 
 [indecipherable] Major, D.A.A.G. Commanding Australian Mounted Division 
 20.7.19. 
