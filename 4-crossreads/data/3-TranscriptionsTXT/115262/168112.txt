 [Page 338] 
 Tuesday 11 December 1917. 
 In a standing camp as it is termed. A few parades which bore everyone but most Corporals &amp; officers that always glory in same, after some weeks in the firing line. 
 On a fatigue that will last a day or so. All our sick camels have been taken to Beelah. 
 [&quot;Camel itch&quot; is being fought against now] 