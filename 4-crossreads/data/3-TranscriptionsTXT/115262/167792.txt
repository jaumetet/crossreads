 [Page 18] 
 Enlisted at Melbourne Town Hall Dep&ocirc;t Sat Feb. 19th 1916. &amp; was &quot;sworn in&quot; on Feb 21st 1916. Interviewed Major Grover on 24th Feb. 
 Entered Camp on March 1st &amp; was sent to Geelong Racecourse Camp. 
 Drafted into original 40th Battn &amp; on 9th March was transferred to A.M.C. in same camp. 
 Transferred to Royal Park A.M.C. on Oct 2nd 1916 &amp; on Oct 10th was transferred to A.M.C. Broadmeadows 
 On Nov 27th was as a C.S.M - contact transferred to Isolation at Ascot Vale, and on Dec 7th was released from Isolation and sent to A.M.C. at Seymour. 
 Volunteered &amp; was accepted for 1st&nbsp;Aust Camel Bgde Fd. Amb. on Jan 23rd 1917. Given final leave and expected 