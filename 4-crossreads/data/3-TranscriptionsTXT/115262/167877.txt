 [Page 103] 
 Friday 20 April 1917. 
 The march. Early breakfast. 
 Left camp 8.25. Trawool bridge 9.30 1st Hill reached summit &amp;&nbsp;was 4th on the highest peak at 10.15 &nbsp; Some climb too. &nbsp; 
 Trawool weir at 11.15 &nbsp; 
 Trawool falls 11.25. Dinner at T&#39;wool Hotel at 2.15 &nbsp; Rested 3/4 hr on the road. Arrived camp 4.30. Shower &amp;&nbsp;tea. Early to bed. 
 We carried water bottle &amp;&nbsp;haversack each. The scenery was  beautifu  l  &nbsp;in the gully below the Trawool falls. Best part near Seymour for scenery. &nbsp;  
 Volunteered for week-end duty. 