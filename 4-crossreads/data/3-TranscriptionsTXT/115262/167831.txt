 [Page 57] 
 Monday 5 March 1917. 
 Arrived back off leave. 
 News just come through that embarkation on Wednesday has been deferred till Friday. 
 Passed&nbsp;final medical exam for embarkation and had hair close cropped. 
  In afternoon our unit was taken for a swimming parade.  
  Interstate Artilelry, T.M.B. M. Grs, Cyclists, &amp; A.S.C, units marched into camp at Seyr [Seymour] for embarkation this week.  