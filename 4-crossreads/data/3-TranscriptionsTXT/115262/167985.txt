 [Page 211] 
 Monday 6 August 1917. 
 [Bank Holiday.] No holiday here though. 
 On the camels once more. We are getting a little used to them by now. 
 During afternoon went to 14 A.G.H. to see Ted Holden (Sgt) &amp; Tom de Gruchy. While there saw several of the other boys on the Staff &amp; as patients there. 
 After tea &amp; feeding parade went to Cairo with H. Flavell. 
 Back for &quot;tattoo&quot; roll call at 9.30 pm at camp. 
 Made a few purchases in  the  part of the &quot;Mouske&quot; 