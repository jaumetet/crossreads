 [Page 111] 
 Saturday 28 April 1917. 
 Up at 8.30 am. &amp; went into city. &nbsp; F.C M never turned u p. With&nbsp;Frank Pratt&nbsp; introduced me to  &nbsp; and&nbsp;L A. Bizley on the block. Had dinner at Y.M.C.A.&nbsp;Soldiers Club at 12.45 pm. &nbsp; 
 To Dandenong on 4.35 pm train from City. &nbsp; 
 Tea in Dandenong (Thomas St) &amp; caught 7.30 train to Lyndhurst. &nbsp; 
 Bed at 12 midnight. 
 &nbsp; 