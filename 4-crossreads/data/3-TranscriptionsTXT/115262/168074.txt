 [Page 300] 
 Saturday 3 November 1917. 
 Beersheebah watering and despatch&nbsp;to Hospital which imobile section of 3 LHFA are working in a Tksh building. 
 Saw Capt. Heath. 
 Taubes bombed Beersheba again today while we were watering. Thousands of animals are being watered and everything is busy to some orders. 
 Took snaps round flour mill destroyed by Turks before evacuation. 
 Some of the wells here are mined. Engineers took cases &amp; cases of explosive from one well alone. 
 Saw Railway line where the 4 th  A.L.H. had to make the final charge to route Jacko from Beersheba, across the open about 1000 yds. 