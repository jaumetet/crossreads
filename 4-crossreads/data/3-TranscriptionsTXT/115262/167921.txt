 [Page 147] 
 Sunday 3 June 1917. 
 Air in No 3 deck a little better in morning and worse toward eveng. 
 Detailed in charge of Isolation Guard and&nbsp; given &nbsp;made Corporal of the Guard&nbsp; for a week or so . Took charge at 9 am. 
 A rather crook day as regards the weather. Monsoonal rains or really storms every hr or so. Sky is clear then in 5 to 10 mins it is blowing a howling wind &amp; the rain coming down in deluges. 
 Expect to get into Colombo tomorrow morning. Several more of the soldiers &quot;tommies&quot; are now working in the stokehold. 3 boats now keeping close together. Speed varying from&nbsp;&frac12;,&nbsp;&frac34; to full speed.&nbsp; Birds (stormy petrel) seen in hundreds at 6 pm . 