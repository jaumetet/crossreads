 [Page 239] 
 Monday 3 September 1917. 
 Reveille&nbsp;5 a.m. Turned out and ready to groom by 5.15 am. Breakfast 6.30 a.m. (Smaller ration than at Malaleh Beach)&nbsp;Turned camels out to graze at 9.30 a.m. Feed up&nbsp; &amp; stables &nbsp;at 3.30 pm. 
 Issued with &amp; instructions in use of P.D Gas helmet respirator 4 till 5 pm.Tea, a wash &amp; turned in. 
 During today I dug in with L/Cpl Watson. Made a plain &amp; comfortable dugout too. 
 It is now a Military Standing Order in this country behind the firing line&nbsp; that every man must be quartered in a dugout.  
 &nbsp; 