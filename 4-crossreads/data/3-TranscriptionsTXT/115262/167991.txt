 [Page 217] 
 Sunday 12 August 1917. 
 Up at 6.45 am. Feed up parade at 7.45 am. Breakfast 8.15 a.m. 
 Watering camels 10.45 a.m. 
 Went on leave to Cairo and Citadel at 2 p.m. 
 Saw through the Sultan Hassan Mosque which was bombarded by Napoleon from Citadel in 1880 &amp; a cannon ball is still sticking in the wall. Where the other balls struck can be seen by marks on the wall. 
 Went through the Blue Mosque &amp; to the top of the minaret where we could see right over Cairo. Took 2 snaps. Then to the Citadel. Guard took our cameras from us at main entrance. Saw through the Mohammed Ali Mosque. Most beautiful Mosque in Egypt. 
 Saw Joseph&#39;s well 320 ft deep &amp; tree where an Egyptian Sgt Major was [Entry continued on previous page.] 