 [Page 76] 
 Saturday 24 March 1917. 
 Up at 5.15. a.m. Caught 7. a.m. train from Seyr. Got to &quot; the dear old Lewis &amp; Whitty&quot; once more  at 10 a.m. Spent morning in city &amp; in afternoon had a good time. Caught 6.30 pm tram to Lyndhurst. 
 8.30 home once more. Bed 10.35&nbsp;p.m. 