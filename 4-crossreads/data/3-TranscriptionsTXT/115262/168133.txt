 [Page 359] 
 Notes for 1918 
  Diary continued in new diary for 1918  
  Jan 1st . As usual in camp, and a sports meeting among A.C.F.A. held. 
  Jan 2nd . In camp. 
  Jan 3rd . March to Rafa station. Blankets etc. put through fumigating train. Saw Turkish prisoners just down from line being put through phenyle bath, etc. Prisoners of war also filling trenches. 
  Jan 4th . Printed photos &amp; wrote a letter. 
  Jan 5th . A C. Fd. Amb played Scottish M.G. Coy. A good game. Scores 2 to 4 goals. 
  Jan 6th . Nothing unusual. A Gas parade. 
  Jan 7th . A further advance reported. Rumored that Referendum has been defeated by 173,000. Mending attempted. 
  Jan 8th . (Officers back from leave) Wrote  a &nbsp;letter s . &nbsp;Party last night to M.G.&#39;s 
 &nbsp; 
 &nbsp; 