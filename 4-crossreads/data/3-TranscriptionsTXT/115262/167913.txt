 [Page 139] 
 Saturday 26 May 1917. 
 A little warmer still today &amp; sea not quite so&nbsp; rough &nbsp;smooth.&nbsp; 
 Swell as usual and surface a little disturbed. 
 Physical jerks for 1&frac12; hrs. 10 to 11.30 a.m. 
 Travelling 10 &frac12; knots an hr.today. 
 Flying fish (1 shoal) seen this afternoon at the bows of boat. 
 this morning &quot;Port Sydney&quot; practised with her maxim gun across the water. &nbsp; In afternoon from the stern from boat&nbsp;(A42)&nbsp;the C.O&nbsp;troops on board &amp;another officer practise with revolvers.  
 Cool on&nbsp;upper&nbsp;deck but on troop deck very close tonight. 