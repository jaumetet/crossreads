 [Page 128] 
 Tuesday&nbsp;15 May 1917. 
 A little more recovered from the sea sickness &amp; feeling hungry. Went up on deck at 6.30&nbsp;am&nbsp; 
 Rather a rough day today. 
 Boat drill during morning.&nbsp; 
 Our boat right close to the &quot;Encounter&quot; &amp; near the lead. 
 In the bight now. Back at Mess Orderly duties again with G.S.S.C. (who has acted&nbsp; a dinkum cobber&nbsp; to me while crook with seasickness.) [George Stuart Strathdee Cameron, No 16923, Camel Field Ambulance sailed on the Boorara with Culbert Cecil Fisher.] 
 Towards afternoon sea rougher 
 Ran into a storm later &amp; at night it is bitterly cold up on deck. &nbsp;Band playing down on No 4 deck cheering things up. 
 Everybody brighter today. Feeling better. Wrote 6 cards afternoon. 
 (Lost one of E M (?) mittens) 
 Are now well down in Sthn  &nbsp; Ocean 