 [Page 181] 
 artillery on the way to Moascar. George Wooten from Mosman was Capt. in charge of the pontoon bridge as we crossed the Canal. On arriving at Moascar we had to pitch camp. 
 29th May (Monday) Another march with packs up. We expect to be leaving for Alexandria shortly. All cameras have to be sent home &ndash; none are permitted in France. New orders have been issued stating that boots have to laced in one particular way. 
 30th May (Tuesday) Read out in orders that the 
