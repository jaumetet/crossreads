 [Page 81] 
 and stretchers just as though in action. I was on telephone duties again. 4 Battalions of Infantry arrived Serapeum also advance guard of the light horse. 
 2nd February (Wednesday) Warship H.M.S. Cornwallis passed in Suez direction. She has some big guns aboard also antiaircraft guns. This was a fine sight especially as it happened to be a beautiful day. Bluejackets cheering &amp; band on board playing "Tipperary". Our boys cheered themselves hoarse. 
 3rd February (Thursday) Orders to prepare for departure to 
