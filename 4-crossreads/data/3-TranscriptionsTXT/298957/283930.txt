 [Page 88] 
 passed in Port Said direction. Daily a number off native boys come round the camp with tomatoes &amp; oranges etc. for sale while occasionally we get a newspaper "The Egyptian Mail". 
 9th February (Wednesday) Showery. Fun with natives collecting rushes down the sweet water canal. After making these Arabs carry the bundles back we got them to show us how mats are made, so we had a good illustration. It was very interesting to note the use made of palm leaves for 
