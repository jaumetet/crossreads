 [Page 156] 
 at night. Shortages in equipment are now being made good in view of our prospective early departure for France. 
 20th April (Thursday) Mail Day. Carpentering at Mess Shed. Peaches &amp; cream before going to bed. 
 21st April (Friday) Good Friday. Glorious day. Parades cancelled except the Church Parade. Reading &amp; writing letters. 
 22nd April (Easter Saturday) Warm. On Cooks Fatigue &amp; writing. Route march for the remainder. In evening playing draughts. 
