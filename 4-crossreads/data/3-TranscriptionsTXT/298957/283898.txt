 [Page 56] 
 here making "bird cages" on the heights round the camp. Col. Tivey is a well known Melbourne man and is very popular. His men are known as "Tivey's Pets" or "Tivey's Tourists", our own unit included. 
 26th Dec. (Sunday) More pineapple &amp; salmon issued in addition to the very fine meals. Meningitis has broken out in two cases at Serapeum. Second mail arrived per S.S. Orontes. Indian Bikaner Camel Corps arrived and our chaps soon got busy on the camels riding them all round the camp. British aeroplane flew over at 9am 
