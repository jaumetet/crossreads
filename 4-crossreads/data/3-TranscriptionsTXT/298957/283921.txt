 [Page 79] 
 especially as our clothes are wet. 
 28th Jan (Friday) Still raining. Wood supply has runout due to poor management. No more warm meals until supply replenished. Troop trains arriving all last night. On the other side of the Canal there is now a huge camp much larger than Liverpool and Serapeum looks quite busy 
 29th Jan (Saturday) More troops arriving last night. It was too cold to sleep. Clearing Hospital arrived today. French mail boat "Amazone" passed. More of that strenuous training &ndash; carrying "wounded" (supposed) over sand. 
