 [Page 123] 
 Smith &amp; Cullen, the latter is a son of our Chief Justice. They both live in Mosman and were at school with me. Their Artillery Unit leaves today for France. Issued with helmets, khaki tunics long trousers &amp; puttees today also Plum Puddings. 
 22nd March (Wednesday) Still off duty. This was an eventful day inasmuch as the Prince of Wales accompanied by General Irvine, a brother to one of our officers, inspected the camp. An escort of Indian Lancers were in attendance. The whereabouts of the Prince while passing through the various lines could 
