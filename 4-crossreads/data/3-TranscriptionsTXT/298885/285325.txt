 [Page 297] 
 are rapidly becoming their former selves and indulging in Sports 
 May 17th &ndash; 19 Waking at 5.30AM and laying in bed on Hurricane deck we could see the snow capped mountains which run along the southern coast of Spain 
 It was a perfect picture as the sun was just emerging from over the horizon, throwing its rays over the stretch of blue water and further on to the snow-capped peaks. 
 May 18th &ndash; 19 At 4AM we passed Tunis 
