 [Page 292] 
 fully recognised 
 This recognition could not be mistaken during the whole of the march. After 4 hours of continuous marching we arrived back at Hyde Park and had tea. 
 Caching the train again at 8PM. we arrived back at Camp at 1AM feeling just about dead beat 
 But it was an experience I would not have missed. 
 May 6th &ndash; 19 Moved to Heytesbury and joined No 16 Quota 
 May 12th &ndash; 19 A little over 3 years ago I was waiting for a day 
