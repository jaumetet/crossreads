 [Page 270] 
 Salisbury Plains prior to embarking for those Sunny Shores 
 While at Salisbury we are supposed to get 14 days pre-embarkation leave and we trust it will be true for it will afford us an opportunity of bidding Au Revoir to friends made during our furlough in England. Nevertheless, at present I am putting forth double energy in mustering all the patience that is supposed to be in a man. 
 Even then the time seems to drag very slow 
 My satisfaction will 
