 [Page 311] 
 guide we were taken over the building which is small. The room is only about 25ft by 10ft 
 On entering the door one is struck by the ornamental work and colouring, the latter being very gorgeous. Besides three large statues there are many smaller ones representing priests etc who lived thousands of years ago. 
 The three large statues one in a lying position and two standing, are of solid alabaster and represented the Indian God "Buddha" 
 On the left of the door as you enter is the figure 
