 [Page 164] 
 the night and may have been still going if circumstances permitted. Also met Jack Dunlop and Jack Hense 
 Feb 2nd &ndash; 18 Today we said Au Revoir to our best camp to date and proceeded back to our rest billit in Bailleul which we acquired last Oct, all we have to show respect to here are the bombs and they have been pretty scarce of late 
 Feb 9th &ndash; 18 Spent 2 days searching per boot for my brother Stan but to no avail. But whilst out making enquiries as to Rube Leane's whereabouts 
