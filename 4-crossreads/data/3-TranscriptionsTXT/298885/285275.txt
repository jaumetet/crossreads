 [Page 247] 
 It was also a common sight to see refugees with hand carts, donkeys, oxen and various conveyances loaded with household goods as they slowly made their way to what was once their respective homes before the war. 
 Nov 25th Reveille 4.30AM. and still dark. Set out for Cartignes. 
 On this trip we came in contact with civilians who had stayed in their homes under german tyranny during the last 4 years. 
 One could [see] the far away vacant look in their 
