 [Page 92] 
 supply last night but instead of shells it was snow which lay about 8 inches deep and when looking all around on the far distant hills, one saw a very picturesque country although the larger shell holes and trenches showed very prominently 
 Jan 18th &ndash; 17 Left Pommiers Redoubt and proceeded to Albert for 19 day rest. 
 here we are billeted in a slightly damaged Chateau under the name of Chateau Lamont Hurtic [?] called after the recent tenant who was a great 
