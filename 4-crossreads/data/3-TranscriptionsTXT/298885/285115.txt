 [Page 87] 
 back at 4.10AM 
 Sun Dec 17th &ndash; 16 Anniversary of leaving Sydney 
 Mon Dec 18th &ndash; 16 There is a rumour that we go out for a spell but I very much doubt it as by what I heard we are here for 2 months 
 Tues Dec 19th &ndash; 16 Although the batteries have been going out in turn for a spell, we poor unfortunate blighters can't get out. 
 Wed Dec 20th &ndash; 16 The Infantry have just stabled their horses near our "Home" and of course next day must be fine and the taubes go over with the 
