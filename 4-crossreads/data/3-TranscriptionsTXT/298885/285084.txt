 [Page 56] 
 journey reached Bouzincourt where we stayed overnight. 
 Thur Aug 3rd &ndash; 16 Left Bouzincourt at 8.30AM and it was dusk when we reached St Leger where we are likely to stay 2 or 3 days. 
 Fri Aug 4th &ndash; 16 Although we are supposed to be out for a spell we were up again at 4AM to go out and practice manouveres. 
 Sat Aug 5th &ndash; 16 Inspection by General Walker. OC 1st Aust Div 
 Mon Aug 7th &ndash; 16 Up again at 5AM. for sham fight lasting all day. 
