 [Page 82] 
 would or would not have known all about it 
 Mon Nov 27th &ndash; 16 Today one of the linesmen being ill I went out with 2 others to patrol our lines &amp; a time we did have. 
 First was the cold wind and sleet, then we had our old friend "MUD". 
 Well we had to cross the roads in this mud and to do this we had to take in turns in pulling each other out from where he stood. 
 About 15 minutes later we had some excitement with "coal boxes" (iron-rations ) as were coming through Delville Wood.  We laid in shell holes for an hour and 
