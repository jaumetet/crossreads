 [Page 17] 
 Sun. Jan. 30th -16 Warned twice to be ready to quell native rising. 
 Mon. Jan. 31st -16 Spent most of day exercising horses with the result I am feeling pretty stiff. 
 Tues. Feb. 1st -16 Light Horse called out to quieten unruly natives in Cairo.  Several being killed besides a number wounded. 
 Wed. Feb. 2nd -16 On guard extremely cold at night.  Inspection of kit. 
 Thur. Feb. 3rd -16 Went into the desert with old howitzers and had gun-drill. 
