 [Page 255] 
 for the "spread" are completed and we are all looking forward to a good time 
 Dec 27th &ndash; 18 My entry of the 2nd Inst has evidently been cancelled as fresh orders state that we will not be going to the Rhine, but remain here in the vicinity of Charleroi, which town is the pivot of operations during the process of demobilisation. 
 Dec 29th &ndash; 18 On Xmas night we had an opportunity to celebrate a Xmas which will be remembered for years to come 
