 [Page 251] 
 journey we travelled over 2nd class roads, which, needless to say supplied us with more mud than we desired, with the consequence that the slippery track put us off our track several times in each mile 
 Nevertheless we arrived here at Morialme with half Belgium clinging to us. 
 Morialme is a fair sized town situated about 10 miles south of Charleroi 
 As it has been far out of the battle zone all these villiages have been spared the fate of those we had just left 
