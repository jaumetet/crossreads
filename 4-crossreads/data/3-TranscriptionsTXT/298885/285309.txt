 [Page 281] 
 informed that our next draft for Aussie would be leaving on the 5th Mar and we had to report to Div Qtrs tomorrow 
 Now fresh orders are out which cancel our move as the two previous drafts are still held up in France through unforseen circumstances brought about by the industrial disturbances and strikes in England. From our point of view it is very disappointing but still a chap can do nothing but wait. 
 Nevertheless we expect to be leaving about the 17th Mar., which notice I 
