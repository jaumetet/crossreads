 [Page 71] 
 Wed Oct 11th &ndash; 16 I was told to report to 1st Div Hd Qtrs to undergo 4 weeks training as an operator. I arrived here after walking 10 miles and losing my way 4 times. 
 Fri Oct 13th &ndash; 16 So far have not started with the instructors, have only been watching the switchboards. In fact they don't seem to know if they want me or not. 
 Sun Oct 16th &ndash; 16 Still nothing doing 
 Mon Oct 16th &ndash; 16 Still with Div Hd Qtrs. Aroused at 3.30AM and after a little toil left Hoogegrafht by motor lorry at 9AM which was a novelty for 
