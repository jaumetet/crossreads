 [Page 142] 
 of Clarmarris [?] Forest. This forest is said to hold good sport in the way of pheasants, deer etc. During our stay we took a stroll through this large thickly wooded enclosure with its dense undergrowth 
 It is a very pretty piece of scenery and the walk was very enjoyable, likewise the cool breeze, which blew gently thro the leave, we found very exhilarating. 
 These French woods with all their greenery have a picturesque view of their own, but as we made our way along 
