 [Page 238] 
 ceased to exist. 
 Only those boys who are well up in the front line will be able to understand more fully the meaning of the cessation of hostilities. 
 The guns and rifles cannot but help make themselves conspicuous by their silence at the given hour 
 It can be imagined that all the tumult and toil of war is going on as hard, if not harder than it has done since war began 
 Guns are booming, countless rifles are 
