 [Page 308] 
 one's feet on earth again 
 Our leave was limited to 6PM and in a short time from landing the town was thickly strewn with Aussies 
 There are a great number of Europeans here 
 The natives, like the Gippos, are everywhere selling their wares and do not blush when they tell you the price of an article at 4 times more than the regular price 
 Curios and silks were in great demand by the boys. 
 Also were the bananas pine apples and cocoanuts which can be had for next 
