 [Page 10] 
 1915 Dec. 17 to 31st 
 With a last longing look at Sydney Harbour we steamed through the heads at 3.30 p.m. on our way to what, and where, we knew not. 
 The Troopship S.S. Berrima (A35) rode the angry waves of a local squall nicely we struck just as we were a few hours out and after two days we had a very smooth run to Fremantle which we reached on 27th.  Entraining at once we arrived at Black Boy Hill Camp where we are told we will remain for 7 or 8 days during which time the boat will load wheat for England. 
 Exit 1915 
