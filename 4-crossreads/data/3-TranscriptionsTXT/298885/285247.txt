 [Page 219] 
 to the camp, where we stayed overnight, we were well and truly ready for tea and bed. 
 Aug 19th &ndash; 18 All on board at 1.45PM we sailed for Folkestone and an hour and a half later we felt a thrill when we were able to plant our beetle-crushers on Blighty soil. 
 Another 4 hours later and we were free to do as we pleased for 14 whole days 
 Aug 20th &ndash; 18 I have spent the last 3 days in London looking up old pals and visiting a few theatres etc 
