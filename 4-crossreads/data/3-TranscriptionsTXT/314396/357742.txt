 [Page 4] 
 in Commonwealth S. Bank . Banked 1&pound; 
 21st Oct 1914 
 Left Melbourne with transport for boat. Left Port Melbourne on  Osterley   Orvieto at 9 am. Very big crowd to see us off. Band played God Save the King  [indecipherable] 
 26th Oct 1914 Reached Albany at 8 am. A good many transport boats in beside Melbourne. Rained very heavy in the morning. Looked grand seeing land again after 5 days on water. Two big islands of  rock just before coming into port. A light house &amp; two houses on first one. Must be very lonely there. Mothers birthday today. 
 27th Oct 1914. New Zealand troops came into port in the afternoon 
 28th Oct 1914 Got inoculated for enterics in the morning &amp; had a   bad   sore arm by dinner time. 
 29 Oct. 1914. Strict censorship put on all letters post cards &amp; telegrams. Place, &amp;  any information likely to let the enemy know whereabouts of troopships is strictly censored. Arm inflamed &amp;  sore. 
 30th Oct. 1914 Nothing doing just ordinary life 