 [Page 73] 
 Septembre 9 Worked half a day on hutting. A good many of the boys went to St Omer on leave but I did not take leave. Did some washing instead. 
 Septembre 10 Working on Refugee Huts Ebblinghem. Nothing fresh to report. 105 of the Coy have gone to Aire for the purpose of training on Heavy Bridging &ndash; including all officers. 