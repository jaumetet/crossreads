 [Page 66] 
 Monday 26th March Etaples Final medical examination before going to the firing line. Concert at the Y.M.C.A Hut in the evening. Met Len. Georges of Malvern on Saturday evening (24th) at a concert in the Y.M.C.A. Hut. Greatly surprised to see John and I, came round to our tent the next evening Sunday 2/3/17. 
