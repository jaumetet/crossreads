 [Page 64] 
 Friday 23rd March 1917 Etaples Left camp about 8.30am and marched to Bull ring where we were given a lecture on the use of Box H.P.Gas helmets. After lunch went through the gas and tear trench. Arrived back in camp about 4pm. 
