 [Page 300] 
 Saturday 22nd December 1917 
 Near Steenwerch Grooming and harness cleaning half holiday 
 Sunday 23rd December 1917 Carting small arms ammunition from railway siding to our lines. Got home about 2pm. 
