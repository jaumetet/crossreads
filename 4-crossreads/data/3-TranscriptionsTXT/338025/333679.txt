 [Page  101] 
 Saturday 5th May 1917 Bapaume Visited the cemetery Reveille 7am First Parade 9am Lectures on harness cleaning and fitting by the S.M. After roll call at 2pm we were dismissed till 4pm. Unloaded a few waggons, which only took a few minutes. Then dismissed for the day. 
 Sunday 6th Reveille 6.30am 1st parade 7am Oiling waggons and cleaning up lines. 
