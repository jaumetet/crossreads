 [Page  241] 
 Monday 15th October 1917 Reninghelst Grooming and harness cleaning. Fritz over our lines about 8pm dropping bombs. The search light got on the first one then the guns opened out on him. They got that close to him that he dropped his load quick and lively and made back to his own lines. Lovely sight seeing about 15 or 20 search light sweeping the sky. 
