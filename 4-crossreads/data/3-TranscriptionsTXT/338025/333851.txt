 [Page  273] 
 Wednesday 21st November 1917 Bussiboom Harnessed and left lines at noon. Went to Taylor dump for Lieut. Hearn's and men's kits. Arrived back to lines at 5pm. Packed my kit as we are shifting our camp tomorrow morning. 
