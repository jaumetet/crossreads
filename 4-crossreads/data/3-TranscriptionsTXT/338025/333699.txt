 [Page  121] 
 Tuesday 29th May 1917 Favreuil Reveille 4am Took mules to water and fed up. Loaded our waggons with batteries baggage and moved out at 8.30 am. Picked up other 2nd Div. batteries on the road (who were also coming out) and by the time we reached La Boiselle we must have been over a mile long. Arr. at a camp about 2 mls. out of Albert at 1.30pm. Slept under waggon. 
