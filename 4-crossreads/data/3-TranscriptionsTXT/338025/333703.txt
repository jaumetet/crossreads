 [Page  125] 
 Saturday 2nd June 1917 Aveluy nr. Bouzincourt Reveille 5.30am. First parade 6am and water mules. Breakfast 7 till 9am then cleaning harness and water mules. Dinner 12 till 2pm. At 2pm we marched to the baths at Bouzincourt, where we had a good hot bath and a change of underclothing. 
 Sunday 3rd Rev. 5am 2nd Div. church parade at 8.30am and inspection by General Johnson. Dinner 12 till 3.30pm 
