 [Page 73] 
 29.12.16 
 Many thanks for parcel of Xmas gifts received today which I fully appreciate. Owing to the engagement we had with our old enemies the gifts were not distributed until today but altho' a little late for Xmas  they arrived at the time they were most needed. 
 Yours gratefully No.53  W J Bell.  1st LHFA 