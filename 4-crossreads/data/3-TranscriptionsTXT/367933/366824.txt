 [Page 36] 
 At Sea 22nd Nov.1917 
 The Secretary 1st L.H.Comforts Fund Sydney 
 Dear Madam i am requested by the men of the 33rd reinforcement 1st A.L.H. to convey to you their hearty thanks as an expresiion of appreciation for your kindness in doing so much for us. 
 The comforts were received safely and evey man of the reinforcement has been given one of each. 
 The voyage up to the present has been good, the men are in high spirits and think everybody on board is comfortable 
 Thanking you again for your kindness. wishing you every success in your work. I remain yours faithfully Sgt. R.H.Blackley 