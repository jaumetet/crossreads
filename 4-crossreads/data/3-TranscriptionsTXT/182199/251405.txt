 a5659015.html 
 Gallipoli Peninsula 
 19.5.15 
 My dear Judge 
 I reget to have to tell you that your son has just been killed in action. &nbsp;A battle started at 12 midnight and lasted till 10 a.m. &nbsp;It was a furious shell fire and our men stood it well. &nbsp;Your son was a gallant soldier. &nbsp;He has done splendid work and I have specially reported him. &nbsp;It is a great loss to the battalion and to me personally. &nbsp;I grieve for you and Mrs Street but you wil be proud to have given him in the cause in which he has done such splendid work. 
 Yours sincerely 
 R. H Owen Lt Col: 
 late 3rd Batt: ........ 1st Infantry Brigade 
 P.S. &nbsp;We gave them a good licking. &nbsp;They were in great strength and came boldly on at first. 
 &nbsp; 