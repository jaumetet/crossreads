 [Page 78] 
 I see him seeking glory with the throng, And the fury of unfettered hell defy, I see him dashing madly with a song - A crash!  My God, I see him fall and die. 
 Ah, boast thee death, of sorrow take your fill, Your victory lies underneath the sod, But, full of hope, the soul triumphant still, Is speeding to the Paradise of God 