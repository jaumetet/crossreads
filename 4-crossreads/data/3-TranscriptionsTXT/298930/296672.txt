 [Page 43] 
 the barracks.    Some of the lads thought they were justified in making use of them, and waiting their chance, would coax the fowl into the huts by means of bread crumbs, and once there they never came out alive.   One young fellow who had apparently 
 N.B. Carried forward on following page 