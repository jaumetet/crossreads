 [Page 93] 
 29 June, 1916 Thursday 
 Reveille at 0500 
 Mr Smith arrived back OK &amp; is looking splendid after his holiday, his wound has healed beautifully Jack Braithwaite was here at the time so a reunion was effected. Things exceptionally quiet today. The Russians seem to be still pushing ahead &amp; gaining more ground &amp; prisoners portion of the Austrian army has been cut off in Flanders, our fellows are doing well &amp; continue to capture prisoners &amp; positions. 
 30 June, 1916 Friday 
 Revielle as usual 
 The 2nd Bde went out on the stunt today, they are making for Bir-el-Abd &amp; more to the left again the plane has been reporting men &amp; camels there continually for the last few days so the 2nd are going to fix matters. 
 No mail on the skyline yet, its 8 wks now since one came to light so there ought to be a batch in any day now. 
 All hands have been going to bed early lately on account of our last stunt we can't seem to get over it &amp; catch up with the sleep. 