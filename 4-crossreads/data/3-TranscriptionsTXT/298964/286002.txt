 [Page 103] 
 12 Th April Captured one of the Turkish Lancers  usual Artillery bombardment  Fokkers very busy.  One of our Airmen killed in duel  Battle coming off in a few days anxiously waiting 
 13 Fri April Very quiet  Heavy artillery bombardment and Fokkers very busy  Received Gas helmets  we are going to use Gas shells 
 14 Sat April Turks bombarded our Hospital with 6 in gun for 2 hours  They gave notice to [indecipherable] result was they mean to shift it Heavy Artillery duel all day  Fokkers very busy dropping bombs  fine weather having plenty of sleep I will need it too 