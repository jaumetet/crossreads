 [Page 139] 
 16 Th August Taking line 600 yds  Jacko very warm laying out no cover  Lieut Tim Black our troop leader seriously wounded [indecipherable] 
 17 Fri Having a quiet day  Arrived back from Post 8 am  Tim Black died wounds at Rafa 
 18 Sat Leaving for Beach for a good spell  relieved by Tommies 