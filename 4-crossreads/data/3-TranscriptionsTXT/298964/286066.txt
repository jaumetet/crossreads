 [Page 167] 
 22 Th Our Regt is attached to 54 Div doing Patrols &amp; Recon  do not know how long we are likely to remain with them.  We are getting tired of it all  have not had a chance to write a letter since 20 of October and do not know how long will be before we have the pleasure. 
 23 Fri Doing patrols for 54 Div  heavy shell fire on our right do not know if Jerusalem has fallen  The [indecipherable] is charging 4f for Butter and 5f for honey [indecipherable]  paying with a smile 
 24 Sat Early morning Patrol 4 am  Jacko still in the Hills with M.G. opened on us heavy fire  We can shift him at any moment  Rumours he is going to counter attack  I hope he does  if so we [indecipherable] 
 