 [Page 90] 
 a Saxon building built on that site before St Melorus'. As each addition was put on, it was consecrated with a small cross,  The mason made 4 small holes as shown [sketch] and these were joined by the bishop. 
 13th  Saw W. Mayne 
 14th  Mr Andy Fisher, The High Commissioner for Australia visited us to-day with the Brigadier, and spoke a few stirring words about the works of the Australians and his expectations of our future, to the Brigade in mass 