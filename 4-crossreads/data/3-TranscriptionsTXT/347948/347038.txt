 [Page 7] 
 16th  Still very rough.  Passed Wilson's Promontory and Port Philip. 
 17th  Feeling better now.  Have kept up on deck to-day. 
 18th  Still very rough, doing at most 5 knots per hour 8 cases of measles on board quarantine feared.  We have a canteen on board, but it is only open occasionally and when it is you can't get to it without waiting about an hour.  Played a programme on deck, but owing to the men being ill and the boat rolling it was pretty Keystone. 