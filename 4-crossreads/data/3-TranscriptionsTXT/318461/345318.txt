 [Page 97] 
 but to us its dam funny don't know what to do tonight raining &amp; miserable will go to bed I think. 7 P.M. Great battle going on glad I am out of it so far. 
 Wednesday 1st August. 1917 Still raining curse it nice holiday got no coat so get wet every day. &amp; have bonzer cold. 
 Take good care of these Books for me &amp; I hope they will give you some sort of entertainment. I generally put down in it what is happening &amp; how it strikes me. Bert Love to all. 
