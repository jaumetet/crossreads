 [Page 80] 
 style with us. I think the distance you are away makes things appear gloomier than they really are. We're as right as reign &ndash; if you'll take a junior subs word for it. Everyone's as happy as larry and there are sports everywhere and the diggers were never in better fettle. 
 The worst feature is that there are a lot of pessimists knocking round Blighty but not on this side of the water. That's why [indecipherable] coves go to Scotland where everyone is confident as they ought to be. 
 Tom Street is now at an O.T.C. in Blighty. The lucky man. Six months at this time of year is not to be sneezed at. 
 Jock Russell is up with us now in the Field Amb and he gave me all the latest Sydney news. I'm 
