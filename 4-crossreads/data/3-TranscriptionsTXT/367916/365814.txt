 [Page 3] 
 2 full of gas which I think will probably be futile. 
 I've had one U.K. leave since being in France but leave is a thing of the past, until, I should say, the Winter comes along. Australia is a better place now altho' there are some merry souls there whom I'd just as soon fire a shell at, as at Fritz, I mean the merry Irish &amp; Laborites. Only thing for them is to pray a raider gets out &amp; shells some of the Australian capital cities. 
 My young brother is in the R.A.F. &amp; has had his Wings some time, in fact its going on for a year since he landed in England &amp; my other brother when last I heard was in Jerusalem. 
 Taking it all round things are pretty slow &amp; one gets fed up pretty soon. I've forgotten all the law I ever knew &amp; have a good mind to stick to the Army, altho' I have done in several Colonels while acting as Prisoner's Friend on a D.C.M. The trouble is, if you tie up the President, he roars like an enraged bull &amp; openly asserts his need of lunch. 
 I have no news of any interest as the Censor would delete any thing I could say. 
 Please remember me to Mrs. Ferguson &amp; the family. 
 Yours sincerely [P.L. Summers] 2/Lt. 
