 [Page 66] 
 Sunday 26 November 1916 Go to Church.  Ellis goes out.  Corp. Lawrence &amp; Rex Potter visit me.  Receive letter from George.  Write to Dad, Mum, George &amp; Auntie Annie. 
 Monday 27 November 1916 Recd. letters Bess (2), Laura, Nell, Hilda, Gwen, Eil. Stirling, Eil. Burke, Otto, Mrs. Stephen, Jess, Wally, Marion, Jean, M.R.L. 4 from Home, George.  Went to see "When love creeps into your heart". 
 Tuesday 28 November 1916 Letter recd. from Bess.  Canteen Corporal.  Wrote to Mrs. Clarke &amp; Laura.  Hard frost in morning.  All Taps frozen up.  P.O.W. patients go to Pictures.  I get Room Warmers to work with in Test Hut.  Cold in head coming on. 
 Wednesday 29 November 1916 Wrote N. Cuneo, short note to M.R.L. (5).  Very sick to-day with cough.  No good for anything. 
 Thursday 30 November 1916 Cold much better.  Recd. 2 letters from Home, Oct. 21, one from Aunt Annie, letter from Home dated Sep. 3.  Wrote Dad, Mum &amp; George, P.C. each, Bess card &amp; 2 pages to M.R.L. 
 Friday 1 December 1916 Wrote to Mum &amp; Aunt A.  Nothing doing.  I am pretty sick.  I read &amp; loaf all day.  I am also on milk diet. 
 Saturday 2 December 1916 Ellis goes to London with Dave Williams one to Richmond &amp; one to Windsor.  I go to see Dave who is sick in bed.  I write to Marg. Millar. 
