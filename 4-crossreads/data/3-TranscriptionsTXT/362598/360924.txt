 [Page 37] 
 Sunday 7 May 1916 Welsh Fusiliers Band in Ezbekieh Gardens.  Late pass till 12 p.m.  Billy, Harrie, Dave, myself. 
 Monday 8 May 1916 Wrote to Julie &amp; sent 2 P.C's Home (30).  Cards to J. Mather.  Wrote to Dad (31).  2 P.C. to George (32), Dave, Vernon, Ted Clarke, Miss Flaherty (Aust.).  156 letters to here. 
 Tuesday 9 May 1916 Received mail from H. Hey &amp; Bess.  Ellis on the Canteen.  Dave &amp; I played Quoits.  Aeroplanes chase away at Reveille to Port Said.  MacLean leaves Cairo to-morrow.  We go in a fortnight's time? 
 Wednesday 10 May 1916 Night Ward Master.  Earn &amp; I dress murderer in the cells &amp; admit man at 11.15 p.m. to No. 6 Ward from Anzac Picket.  Received letters from Hilda &amp; Bessie. 
 Thursday 11 May 1916 Canteen Keeper.  Recd. letter from Jennie.  Horrie Norton 18 years old.  Wrote to H. Hey, Bess, Jennie, Dad, 33, 2 cards. 
 Friday 12 May 1916 Wrote to Roy, George &amp; Mum, 34 &amp; 35 &amp; studied French with Eric.  Another Hot day 110 in the shade.  Dave saw Eric Jones &amp; his wife on the way back to Australia. 
 Saturday 13 May 1916 Very Hot.  Went to French Lesson with Eric Marshman.  Billy &amp; Dave go to Alex. 
