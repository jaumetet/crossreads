 [Page 36] 
 Sunday 30 April 1916 Excused Church.  Frank Cartright &amp; Les Nettleton from Tel-el-Kebir to dinner, Kasr-el-Nil, Gehzira [also spelt Gezira] Gardens.  Tea at Anzac &amp; heard Welsh Choir.  Met Percy Stewart. 
 Monday 1 May 1916 Received Parcel from B. Cuneo, chocs. &amp; socks.  Wrote Home (28).  Started Prophylactic Treatment. 
 Tuesday 2 May 1916 Ellis leave day &amp; we had to shift tents.  Pay day &amp; nearly all the mob were boozed.  Percy Stewart on Transport work &amp; also came over to see us at night. 
 Wednesday 3 May 1916 Wrote to Hilda, Hannah, Nettie, George (29), Mem 9 pages, Kit Paine, Roy Tredrea.  Received letters from Mem, Jess M., Bess, Kit Paine, Julie &amp; Roy Tred. 
 Thursday 4 May 1916 Official opening of Canteen.  Half shica.  Read books nearly all the time.  Earn has fight with Picket.  Ellis Orderly Corporal.  Wrote Bess  Julie  &amp; Jess Mather. 
 Saturday 6 May 1916 Ellis &amp; I whole day leave.  Looked for McGoodye.  Lay down in Park all afternoon.  Went to tea with Mrs. Wright &amp; met Traill &amp; Miss Hindmarch &amp; Brother.  Just Home in time.  Saw Roly Hale, who told us that Eric Davis had left the Hospital.  Summer starts in real earnest.  Will note in Future only cool breezy days. 
