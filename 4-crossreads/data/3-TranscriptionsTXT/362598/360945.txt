 [Page 58] 
 Sunday 1 October 1916 Church Parade &amp; General inspection by Gen. Moore.  D., B., E. &amp; I go to Shipton but fail to get to Salisbury.  We motor to Tidsworth for Supper &amp; walk Home.  We tour village for grub. 
 Monday 2 October 1916 No parade morning &amp; route march in afternoon.  Wrote Dad, Mum &amp; George, Marg., Maynie, Marie, Jennie.  I get a change of Boots.  Recd. letter Marie 17/9/16. 
 Tuesday 3 October 1916 We route march to Cholderton &amp; see a 50 Dennis Motor Transport.  Heaps of drill all afternoon.  Go to bed very early as we are all tres fatigue. 
 Wednesday 4 October 1916 I am on Y.M.C.A. Fatigue.  I meet Dougall Carroll &amp; mud-guts.  Route march in afternoon, go to bed early.  Write to Bess. 
 Thursday 5 October 1916 3 route marches.  We get leave.  I wash clothes.  See Tocooloo Roberts at Cholderton &amp; visit old church.  Wrote Jennie &amp; Roy. 
 Friday 6 October 1916 No route march after 9 till 2 p.m.  Very rainy. 
 Saturday 7 October 1916 Ern goes on Guard.  D., B., E. &amp; I go to Shipton &amp; return to Y.M.C.A.  We have supper on bacon &amp; eggs.  Mick Barr goes to France &amp; bids Bill Good-Bye. 
