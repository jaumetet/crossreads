 [Page 38] 
 Sunday 14 May 1916 Ellis, Bill Bagnall &amp; I go to Kasr-el-Nil &amp;c. at Gippo's dinner.  Feed on Hot Green apricots without after-effects.  Tea at Anzac &amp; Home to Bed at 6 p.m. 
 Monday 15 May 1916 No leave.  No water till 8 p.m.  Dave &amp; I go to Helio. to eat Ice Cream &amp; buy Lemon Squash.  Niness gets crook on peanuts. 
 Tuesday 16 May 1916 Rise at 5 a.m., treatment alters.  Played cards from 2 till 4 &amp; then started treatment.  New W.O. arrives.  Open Bessie's parcel which arrives on the 15th inst. 
 Wednesday 17 May 1916 Treatment at 5.45 a.m. and at 9 &amp; 3.30 p.m.  Took Len Val's turn at Ward Master.  Bed 11 p.m. 
 Thursday 18 May 1916 Wrote to Francis &amp; Eileen.  Cool day.  Staff Burr's Presentation in Rec. Room. 
 Friday 19 May 1916 Tennis court opens.  Night Ward Master.  Dress the Drunken Guard's Head &amp; 12 minuit.  Nice cool weather. 
 Saturday 20 May 1916 Nice cool day.  Ellis &amp; I go to Heliopolis to the Pictures.  Walk home with H. Wooly.  Walked along to the Catholic Church &amp; it was O.K., a beautiful place.  Beautiful Cedar flowers Red &amp; salmon colour in Heliopolis.  Received a letter from Marie .  Hear of Johnny Norton's Death. 
