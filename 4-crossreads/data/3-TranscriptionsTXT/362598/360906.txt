 [Page 19] 
 Leo. Baker 24th Coy. A.S.C. No. 3 Company 3rd Div. Train A.I.F. 
 Cpl. 77511 John Lawton R.E. Sig. Co. Poona India 
 Prof. Barr Barr &amp; Stroud Anniesland Glasgow 
 L/C Hazelwood L/C Gosling Holmgren Garratt 
 Anzac Provost Corp. that visited our Hospital, 23-11-16 
 No. 4794 S.M. Swallow A.C.C. Chisledon Nr. Swindon 
 Mark letter Confidential 
 11 De Beauvoir Rd. De Beauvoir Town Kingsland London N. 
 Saturday 1 January 1916 Sea Smooth.  A.D.H. held Boxing Match.  Ship's staff gave concert in F Ward.  Nurses provided chocolates. 
 Mrs. C.P. Browne 37 Thurlley Rd. Wembley Middlesex England 
