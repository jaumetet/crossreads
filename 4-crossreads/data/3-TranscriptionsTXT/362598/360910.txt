 [Page 23] 
 Sunday 30 January 1916 Ward work &amp; no spell even for a shower. 
 Monday 31 January 1916 Continue in ward work &amp; Operation Theatre 
 Tuesday 1 February 1916 Still continue.  Wrote to Marg Millar. 
 Wednesday 2 February 1916 Wrote Home &amp; sent a few cards.  Wrote to Cohen's. 
 Thursday 3 February 1916 Ellis &amp; I with Staff Moore, L.C. Winn &amp; Sergt. McIvor went to Pyramids followed by tea at Y.M.C.A. 
 Friday 4 February 1916 Harold Filshie, Roland Hale &amp; Eric Davis came up to see us. 
 Saturday 4 February 1916 Had a letter from Roy Tredrea. 
