 [Page 49] 
 Sunday 30 July 1916 Ellis &amp; I go to Ferra Sudan for Dinner.  Mrs. Cougi plays Le Piano &amp; we dine on stuffed fowl &amp; Chicken Soup with Grapes as Desert.  We spend the evening outside Saults.  I am Matron &amp; forget all about it but nothing happens.  Good Luck. 
 Monday 31 July 1916 Send Photo to Mum &amp; Mr. Westy &amp; letter.  Photo to Lyd. Goddard, George (60), Photo to Bess, Marj., Mel, Alice Meth.  Papers Received. 
 Tuesday 1 August 1916 Dave &amp; I go on Donks through the Dead City &amp; Khalif on to Nap's Fort.  We dine at Y.M.C.A. &amp; meet E.B. Old &amp; Matt to play Billiards.  We had to hurry back at 5 p.m.  We had a very good day.  Letter from Marie.  Wrote Home (61) &amp; Marie. 
 Wednesday 2 August 1916 Supposed to have written to Maisie Carter on the 1st August. 
 Thursday 3 August 1916 I begin to play Football.  We go to Luna Park &amp; have a very nice time. 
 Friday 4 August 1916 Nettie's Natal Day.  Commen. of start of war.  Church at 9.45 a.m.  Downes leaves us. 
 Saturday 5 August 1916 Wrote Home (62), Pictures of a quiet drink in Cairo.  Photo to Nettie.  I practise Hymns for Church.  We all go to Cairo but D.E. &amp; I go to Anzac &amp; take supper. 
