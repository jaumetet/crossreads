 [Page 16] 
 Harry Goddard 8th Rein., 3rd Batt. 
 Mrs. Melville Capshaw Woodbury St. M'Ville 
 Francis Bruke Cahermore 50 Darling St. Balmain E. 
 Kit Green Caloola Adelaide Parade Woollahra 
 1957 H.A. Hickson 
 Jack Tredrea 113 How. Battery 13 F.A.B 5 Div. Artillery B.E.F. France 
 Alice Methven C/o W.B. Gibb Greta St. Oamaru, N.Z. 
 Marie's Aunt "Oakhurst" 4 Waterloo Rd. Birkdale Southport England 
 Maisie Carter "Edgecote" Northbrook St. Rockdale 
 Dulcie Butell Duncan St. A'cliffe 
