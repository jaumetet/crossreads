 [Page 12] 
 11 Sept. Rum issued, wounded in the neck with glass while looking through a perescope.  Stew, biscuits &amp; tea for breakfast &amp; dinner &amp; 1 pancake 4" dia. for tea.  The doctor puts me off duty for a few days on account of my wound.  Receive 2 letters from mother dated July 14 &amp; July 23rd. 
 12 Sept. Go on duty at 6 PM. Write home. 
 13 Sept. Off duty &ndash; Sick.  Stevenson wounded with bomb.  Go on duty at 6 PM in the bomb trench. 
 14 Sept. On duty. 
 15 Sept. Have some rain which makes it very unpleasant. 
 16 Sept. Mail in.  Receive letter from mother.  Lieut. Sheppard wounded by a bomb thrown from the Turks trench.  The Turks throw a packet of tobacco and cigarette papers.  I enjoy a very pleasant smoke out of it.  Heavy demonstration at 10 PM.  Craig wounded with a bomb.  Garland Gun explodes.  Killed one and wounded two more. 
 17 Sept. Have a day out of the trench and write home.  2nd Rein. of the 17th arrive by the  Kingtonian  B.15 Kingstonian from Egypt. 
 18 Sept. The Turks send us plenty of bombs and give us a  lively time .  Turks open heavy fire on Courtanays &amp; Popes Hill at 5 PM.  Captain Lonsdale wounded with a Turks bombs. 
 19 Sept. The Turks send in plenty of bombs.  Pte. Hugo wounded.  Mail in. 
 21 Sept. "B" Coy. relieve "A" Coy. in the trenches. 
 22 Sept. Wounded &amp; sent to the base. 
 23 Sept. Taken aboard Hospital ship.  Capt. Colwell comes down to Anzac to see me off. 
 25 Sept. Sail from Anzac at 9 AM for Lemnos arriving there at 8 PM. 