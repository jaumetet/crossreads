 [Page 104] 
 rations for them as they were off our strength. "Beachy Bill" our old friend sent a lot of shells over towards the beach to-day. 
 Tuesday 14th I went to Ordnance to get officers clothing etc also to arrange about mules for drawing indent goods on the following day. Received note from Jack Fitzgerald our Field Cashier 
 15th Sept Wednesday It rained. Had to get up to catch the drops through my roof etc. Mud everywhere &ndash; sticks like glue much worse than our black earth etc. Fairly cold etc. Fine big mail arrived. 
 16th Sept Thursday A fine day. Felt tired after going to Ordnance yesterday to 
