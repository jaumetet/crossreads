 [Page 129] 
 to have a fresh lot of ammunition now as their guns are more active. 
 Wednesday 27th Oct It is windy to-day &amp; dusty. There was a big fusilade of shelling before midday. The Turks opened up on us from most of their Batteries. 
 Thursday 28th Oct Had a quiet day. Captn Crip informed me I was chosen by Officers Commanding 3rd Brigade F.A. to take charge of Brigade Canteen it was decided to form. This would mean my being attached to Headquarters. 
 Last night one of the old limbers which had been broken up by shells was erected up in one of our Gun Pits as a Dummy to draw fire. 
 Friday 29th Had a meeting of Canteen 
