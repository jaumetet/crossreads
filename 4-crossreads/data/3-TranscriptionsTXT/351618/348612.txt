 [Page 99] 
 Monday 30th August It is nice &amp; cool early this morning. I got a few more letters &amp; Papers which had been amongst the bags on the sunken barge. Went to Ordnance to get Bags Boots &amp; Tunics etc. Saw Olly Gunderson. 
 Tuesday 31st August There is a tinge of Winter in the air &amp; it is lovely after the hot days. Our chaps are commencing to cover themselves up a little more. There is not that absence of clothing on their backs that I spoke of some time ago. 
 Wednesday 1st September 1915 There was a decided change last night &amp; I felt the chill of the air as I had to unceremoniously leave my bed for the rear. It was about midnight at the time &amp; I had five others to keep me company when I arrived 
