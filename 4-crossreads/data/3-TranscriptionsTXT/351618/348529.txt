 [Page 16] 
 last found our departure was postponed till tomorrow. Alas another night on board the "Southland" 
 Saturday 19th Mustupha is a place of much concern as all wounded soldiers to their sorrow soon learn. A few weeks there and back your turn to where the bullets fly and make you squirm. 
 Scotty and I met in this place of renown. It wasn't all comfort or feather down. We were there a few weeks on bread and jam and good hot tea that made you very down. 
 Now we are off where the bullets fly and hope to meet again in the Street bye and bye. 
 Limerick to Cecil Thomas Scott (RMLI) 58 Alexandra St, Warsop, Notts. 
 We were all day waiting on board 
