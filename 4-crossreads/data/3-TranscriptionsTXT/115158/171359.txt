 1916. 
 light cruisers &amp; destroyers, our expedition only lasted some 48 hours, having failed to disclose anything.&nbsp; The next few weeks were quiet, after which there was a period during which we were kept at short notice for full steam. 
 Sept. 30. 
 Towars the end of the month all the battle cruisers, accompanied by light cruisers, &amp; destroyers left for Scapa Flow.&nbsp; One of our naval airships, which had been recently completed, preceded the Fleet for some hours to reconnoitre for hostile submarines.&nbsp; Recently, Fritz has been very active in this neighbourhood, but on this day nothing was seen of him, and on darkness approaching the airship left us,&nbsp;&amp; we were left to our own devices. 
 Oct. 1. 
 The following morning we arrived at Scapa Flow. 
 Oct. 7. 
 During the following week the Battle cruiser fleet carried out torpedo &amp; gunnery exercises, on the completion of which, we carried out a sweep in the North Sea, before returning to our base in the Firth of Forth.&nbsp; Nothing was seen in the course of this sweep, &amp; our return to harbour was made in the teeth of a westerly gale. 
 Oct. 9. 
 On returning to harbour, we completed with coal &amp; oil fuel. 
 Oct. 10. 
 The next day was taken up by the men voting on the question of conscription in Australia.&nbsp; Every Australian on board was allowed to record his vote &amp; the issue was clearly explained to the men before voting by the officers supervising the voting.&nbsp; The work of adding 1&quot; nickel steel plates to the lower deck over the magazines had been 