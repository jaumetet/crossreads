 [Page 91] 
 1 st =  76 K 76,000   [indecipherable] 924 154 2464 
 76000  - 2464 ozs 
 Becourt to Senlecques. Senlecques (3 AM) to station about km &amp; thence through Bailieul to rail head.  Marched from rail to Locre where we established 1st Div Rest Station.  Locre is behind Kemmel &amp; Messines. 
 [Spelling corrections p.55 &amp; 60  -  Monte Katz  =  Mont des Cats p.68  -  Whytchaete = Wytschaete p.68,69,72 -  Boechepe = Boeschepe p.81 - Reniscure = Renescure p.81 &ndash; Wardreques = Wardrecques p.81 &ndash; Quistede = Quiestede p.85 &ndash; Longvillers = Longvilliers] 
 [Transcribed by Peter Mayo, Trish Barrett and Adrian Bicknell for the State Library of New South Wales] 
 