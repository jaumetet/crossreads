 [Page 3] 
 [Transcriber's notes: p1. &ndash; p.19 Repeats details of previous six weeks as his diary for that period was lost. p.20 - Parkhouse camp. Medical problems p.24 - Application for O.T.C. approved. On draft for France. p.28 - Leaves Parkhouse for Southampton p.36 - In camp at Ruelles near Le Havre p.46 &ndash; Leaves for front via Rouen p.53 &ndash; In rest camp at Caestre near Belgium border p.81 &ndash; Left Caestre for front. p.84 &ndash; Camp at Cormont near Estaples pp. 87,89 -  Move towards front &ndash; Becourt/Locre] 
 Diary No. 5 
 O.L.Holt 4231  3rd Fld Amb. A.A.M.C. 
 c/o  20, Ellison Rd., Barnes London S.W.13. 
