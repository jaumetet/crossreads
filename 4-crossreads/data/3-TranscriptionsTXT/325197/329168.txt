 [Page 17] 
 Mon. 2nd Sept Down again with French Fever 
 Wed 4th September Up again, and feel A.1. 
 Sat 7th Sept Once again I am into bed, tried to stay up, and after a few words with the Sister, I was shoved in. 
 Sun 8th Sept. Visit from Miss Golding, who came to get some particulars of how Bombd. [Bombardier] Fox was killed. As Fox was in the same "Dug out" as myself in France and was hit by a piece of the shell that got me, I was able to give her full particulars 
 Tues 10th. Sept As I feel "Good O'" and out of bed, I have been warned for 14 days leave. It is hard to take. I dont think. 
 As the piece of Shrapnel in my arm is very small, and giving 
