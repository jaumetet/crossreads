 [Page 39] 
 wagons antiaircraft guns with their  nervous crews .  Wireless aerials, troops, regimental cookhouses, smoking, betokening a hot meal for somebody, and above it all the hum of many aeroplanes tearing through a blue clear sky. 
 1st Oct. Pioneer Camp 
 We left Half Way House yesterday evening at 5.30 and reached here about 10 p.m.  The scene when we arrived 