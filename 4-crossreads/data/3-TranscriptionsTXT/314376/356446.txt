 [Page 135] 
 135 into the German's line I can't say. The fact remained I have heard nothing further from that day to this. 
 In taking over this trench for the first time we came upon rations that were intended for another Battn but had never been issued to them. We were not short of bread and cheese I am[?] for some time. 
 We had, as I say just made ourselves comfortable for the night when word came through that we would have to move along to another trench. 
 We had to pack all our things together again and under the guidance of a runner we reached our new quarters, which were not very far away. The Germans 
