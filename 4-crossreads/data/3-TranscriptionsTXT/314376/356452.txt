 [Page 141] 
 141 they determined to try and get back to the line again and I believe most were successful. 
 Corpl Macgregor was killed here. Later on in the day there occurred a regrettable incident. 
 One of our number, passing a tree under which a German was lying was accosted by the man. 
 He told us that he felt inclined to go up and bash his brains out but did not. However, later on he passed the man again and this time he did attack the man, wounding him very badly. 
 It appears that the chaps in the trench had left him there for a time nearer darkness when they could take him down without fear of being sniped by a German marksman who 
