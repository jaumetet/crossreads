 [Page 36] 
 36 town itself are not very distinct, there seemed to be a distinctly old world tone about it and the streets well watered, smooth and cool. 
 The heat of the sun did not come into it but was shaded away by the big buildings on either side. 
 There was a story told to me of the fact that there was an old woman who used to sell milk round the town. His [Her?] method was not that which our milkman adopt when selling milk but she used to drive the cow around and as the customer would require milk she proceeded to milk the animal. 
 We eventually reached No 3 [Hospital] which is situated next the prison called Abbassis Bcks. Arriving in the courtyard we were admitted by the M.O. and then sent off, I think, to F Ward where the charge sister spoke to us and 
