 [Page 194] 
 194 by us. 
 In the excitement of battle when men are apt to get a slightly distorted view of things, mistakes are apt to occur. In this instance our Machine Gunner turned his gun on to them without however causing much damage as far as I could see. 
 Another time, a little German, probably lost, wandered into our lines to give himself up. The same gun was again fired and a couple of bombs were also thrown, with the direct result that the little chap was so badly wounded he died soon after. 
 I took a risk that afternoon too. It came on to rain 
