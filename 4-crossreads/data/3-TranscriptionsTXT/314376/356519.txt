 [Page 208] 
 208 as I was under the impression it was a shell from one of the enemies batteries that had been discharged. Setting off again we traversed part of the way and then suddenly "Krupp Krupp" Krupp about 3 shells came along simultaneously. We made a rush for shelter in a trench alongside to be sharply reminded to get a move on or words to that effect so we did move on. A lot of the men had dropped out. The shells came along. Fritz's flares were going up all round us. 
 We stuck to it however (Frank Butler and I) eventually reaching our 
