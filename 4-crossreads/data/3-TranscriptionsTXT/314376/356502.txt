 [Page 191] 
 191 they had cooked stew and made tea but were unable to get it down to the boys. 
 After leaving we went back and got into a dugout where we slept the night. 
 Next morning the Germans were shelling very heavily. I did not like facing out for a start. However, getting directions from someone else we went forward in an endeavour to find the front line. There was a very shallow communications trench hereabouts and it was filled with men of the second division, who had been relieved by our fellows. We went forward again and in amongst the others I found my brother Bert who was looking worn. We clasped hands and parted. 
 Some of our chaps were trying to bring in the wounded and 
