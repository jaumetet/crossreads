 [Page 89] 
 89 to a trench that was originally designed for a cavalry track. 
 We marched up this eventually coming to "Switch" trench. 
 It was raining pretty consistently all the time. As usual upon arrival at a new "bevy" there was a rush for what were called the best "possies" or dugouts as they were termed, of course, the first of the men were able to procure these. I was one of the last and, of course, a new comer myself and three others found a place, which after some patching up, we decided would suit us for the stay in the trenches. 
 We had a meal and afterwards settled down for the night. About midnight the 
