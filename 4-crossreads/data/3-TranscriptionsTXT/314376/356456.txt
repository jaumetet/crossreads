 [Page 145] 
 145 had just scooped a square out of the side of a trench covering the bottom with straw and putting a piece of iron over the top and with their blankets and waterproofs made themselves as comfortable as possible under the circumstances. 
 It is really surprising however I think when a man is living under exposed conditions nature works overtime to combat the more exposed conditions. More blood is formed, I think that though a man loses the comfort of a nice home and a fire yet it is made up to him in other ways &ndash; nature providing other means as above. 
 Souvenir hunting was a feature of the troops here. It was the first village 
