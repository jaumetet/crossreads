 [Page 56] 
 56 the enemy. In this work the boys were very much in earnest. 
 The instructors were mostly chaps who had come over with the draft reinforcements being taken out and going to schools &ndash; had qualified as Instructional Sergeants. Mixed amongst them were a leavening of Imperial (Sergts) instructors who taught us Physical culture and Bayonet exercises. After about 3 weeks here I was drafted for overseas. 
 After examination as regards my physical condition following and being passed, was with others marched to the Station one morning headed by the depot band and there started new experience. 
 Entrained we started off for Folkestone which we reached about 1 o'clock in the afternoon. 
 It is a practise here that the troops &ndash; before going abroad the troop 
