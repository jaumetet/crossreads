 [Page 55] 
 55 necessities that he might require. 
 One could buy the usual cup of tea and biscuits. Writing paper was provided. The rule at the Mission Tent was to provide a free tea of a Sunday afternoon. I cannot say whether this applied here or not. 
 Concert parties would come along as well to entertain us and of a Sunday morning it was possible to attend a Communion Service. 
 We had plenty instructions here. Bayonet fighting (once I nearly got prodded), physical exercises, plenty of "doubling" and musketry. 
 I passed here as a 2nd grade marksman. Where we could practice "hopping over". This consisted of scaling trenches and hopping into same. We also had to bayonet bags got up represent 
