 [Page 172] 
 172 line in the muddy soil. 
 Our respirators and dixies were shot through early in the day and it was not long before there was one or two casualties, one fellow getting his ear shot off. 
 To speak truthfully we never thought we would get out of this trench at all. At night orders were given to evacuate the trench and dig in further back. We waited till it was quite dark and there was only a few star shells illuminating the scene and then cautiously made our way to the rear digging in with our entrenching tools. 
 We had only just started to do this and I may say with great care, when the 
