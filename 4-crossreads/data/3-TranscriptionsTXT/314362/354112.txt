 [Page 30] 
 7 Sunday  [Oct] Cold  &amp; Wet &amp; fed up!!! Take me back to dear old Blighty. 
 8 Monday shifted camp from Reninghelst to Ondernoom [?]. Rained of course &amp; we got washed out.  What a life!! 
 9 Tuesday In Evan's court martial as Prisoner's Friend. [Three lines deleted] 
 10 Wednesday Evan court  M finished &amp; he got off. Don't know whether it was due to me tho !!! 
 11 Thursday Had quite a good feed in Reninghelst with Pennie. Raining muchly. 
 12 Friday Hear that we are move tomorrow upwards!!   Wrote lng  lre to Rup. 
 13 Saturday Packed &amp; moved up to Westhoek Ridge at night. up to our necks in mud but still happy !!!! 
 14 Sunday Am on Sigs job &amp; with HQ. in a Boche pill box. Gas shells at night &amp; lost my sleep !! 
 15 Monday Wrote a short note to Rup. Received quite a number of lres from everyone. Plenty of gas shells at night. 
 16 Tuesday Heavy shells all day &amp; we had some narrow escapes. More Gas damn it !! 
 17 Wednesday Moved up to the line at 4 oc &amp; had rough time!  Boche plane chased us on the duckboards. 
 18 Thursday Our phones lines are dis. on &amp; off but we get along somehow.   Buzza [indecipherable] &amp; D M blown out. 
 19 Friday Lines going well especially the one to Bde. Heavy shelling all day 
 20 Saturday Saw  Woolrych in morning.  Getting fed up &amp; was gassed by the beastly tear stuff. 