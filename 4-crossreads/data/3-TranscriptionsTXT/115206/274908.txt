 [Page 370] 
 December  1918. Wednesday 4 
 Wembley On furlough. Received a letter from Smithy, so he did not go to Ireland.Gives Leeds a good name. Weather wet. Went to Palladium with Charlie, Vic and Olive. Splendid show too. Visited Cooks and made inquiries for sailing. Weather still wet at midnight, I wonder does it ever cease to rain. 