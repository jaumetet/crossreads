 [Page 162] 
 May 1918 Friday 10 
 Bussy-la-Daours Glorious weather. Had the usual journey at 4 pm, to Lailly Le Sec, via Vaux sur Somme and Corbie. During afternoon drifted out avec Muller, to Querrie Pont Noyelle, and La Neuvelle, as Mounted Orderley. Wrote a French epistle to Adrienne, aided by Lemon, at Madame Isobel. Harry came in from Aubigny. 