 [Page 137] 
 April 1918 Monday 15 
 Bussy &ndash; la &ndash; Daours.   D.R. Spent a very quiet morning during the afternoon rode to Bonnay via Querrie, Pont Noyelle, La Ne&mdash;and received a fair quantity of mail. Borrowed a few francs from Ken Gunn. 