 [Page 371] 
 December  1918. Thursday 5 
 Wembley On furlough. Received a wire from Smithie to meet him at Y.M. at 11 am tomorrow. Wired to Birmingham instructions. Spent quiet evening at home visiting Mr Harrison near by. 