 [Page 364] 
 November 1918. Thursday 28 
 Wembley . On furlough Went up on early train with Mr.Jones and Vic to Victoria. Met "O" at 1 pm on Marlebone Station and received smithy's wire. Met Smithy at 2.25 pm at Kings Cross. Went down to Baker St and met Margeurite Vaugnat also Miss Westport. Made arrangements for tomorrow. Left cakes in the carriage. 