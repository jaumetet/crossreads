 [Page 230] 
 July 1918 Wednesday 17 
 Bresle. Bty Position 
 Weather very unsettled  and close. Wrote Adey and enclosed two letters. Wrote Ken Gunn and enclosed some letters. Had some very heavy rain during day. Salvaging wire with Tom and prospecting new line. Which we shall endeavour to salvage tomorrow  Heavy artillery fire somewhere by Viller Brettoneaux, at 9 pm also Fritz S.O.S. signals. Bosche attack down by Chateau Thierry, official news , on an 88 kilo front. 