 [Page 159] 
 May 1918 Tuesday 7 
 Bussy-la&mdash;Daours Weather wet, heavy rain all day. Had a run to Bonnay via Corbie and Daours. Chestnut gassed in both legs, shot as a result of injuries. Ken Gunn and Pea paid me a visit during evening. Borrowed a large reel of D.3, and sent same to Batterie. Had a couple of eufs avc du pang, du beure et coffee, at Le Maison des Madame Isobel. 