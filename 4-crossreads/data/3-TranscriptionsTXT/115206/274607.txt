 [Page 69] 
 February 1918 Wednesday 6 
 Messines Weather fine and clear Had a look around the vicinity and went across to Anti Tank position and saw Gunn and Russell. Garford on O.P. Received small amount of mail. Had the 10 to 12 watch. Played cards until ten 