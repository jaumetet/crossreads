 [Page 365] 
 November 1918. Friday 29 
 Wembley. On Furlough Went up to town at 11am. Met Smithy and Percy Grayling in the Aldwych Y.M. Went out to Lyons for lunch and then booked seats for "You never know Yknow" at the Criterion Theatre. Made up a neat little party and had a very enjoyable evening. Expect to leave tomorrow for Bristol to see Ken Gunn. 