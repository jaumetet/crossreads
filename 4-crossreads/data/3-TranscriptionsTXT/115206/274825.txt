 [Page 287] 
 September 1918. Thursday 12 
 In Reserve. Hebecourt Weather very wet and fairly miserable. Walked across to Frise to see old Yank Watson but had no luck.  Wrote three letters, and cleaned the telephones. Spent a very quiet night .Met Fisher of 10th A.L. [indecipherable] he looks well. Charlie Adey returned to Bty from school and Blighty leave. 