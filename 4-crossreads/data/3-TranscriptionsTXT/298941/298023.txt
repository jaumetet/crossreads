 [Page 61] 
 12 years, was sobbing loudly despite the Colonel's attempts to soothe her with sympathetic tut! tut!s Apparently he (the boy) had been too ill to start on the long journey away from home, &amp; the danger of keeping him in the village must have broken the family up. Even after the Col. Solved the difficulty, their own future remained in doubt. We evacuated all civilian wounded 
