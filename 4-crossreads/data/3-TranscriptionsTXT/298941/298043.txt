 [Page 81] 
 manifest. So far even I have been impressed how when one understands the meaning of "in God's hands",  what  a very real and world transcending help can be obtained. Particularly if I am to be spared, I want the effect of what I have learned, to flow when peace time comfort comes again, &amp; not as is more natural, decrease. I had a chat to F.O. Grey today (the 
