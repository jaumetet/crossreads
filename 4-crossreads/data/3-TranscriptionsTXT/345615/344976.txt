 [Page 48] 
 "Ah God one sniff of England To greet my flesh &amp; blood To see the hansoms slurring Once more through London Mud" 
 Jan 20 &ndash; (I think) Hurrah, have got out of that Postal orderly job and am back again for duty with my Company.  It was a bit difficult getting away &amp;  It means harder work &amp; none of the "sweets of office" in the way of soft living but it is more satisfactory to be back with the Company with which I started with. 
 This diary seems to be dying a natural death, but nothing seems to have happened worth recording. We got some new arrivals last night in the shape of 180 men &amp; 5 officers from the South African 
