 [Page 23] 
 post &amp; come in with Capt Munro. 
 He may have a bit of a fight for it, as I learn that the road is closed behind us  the other  
 Dec 15/15  Matrugh  reached here 4 p.m yesterday &amp; am sure the horses are not sorry after the rough time they have had this last few days 
  People  Everyone says  there  we  has  are lucky to have got through without a scrap &amp; wild rumours were about the camp that we had been cut off. 
 Every few miles along the track we have seen signs of habitation &ndash; mud  huts  &amp; stone huts &ndash; with mud walled yards 
