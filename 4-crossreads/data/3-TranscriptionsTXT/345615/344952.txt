 [Page 24] 
 evidently  small  natives with small flocks of sheep &amp; goats which graze off the "salt bush" which covers the desert But all the huts are deserted &amp; the flocks driven away 
 The people have either joined the Bedouins or fled for protection. 
  Cap  Have just been speaking to one of Capt Munro's men  who was with his flying column which got safely back here at 5 oclock today after having covered the 60 miles including all stoppings in  2 days  36 hours. He tells me that as soon as they had evacuated the waterhole &amp; got the Sikh garrison 
