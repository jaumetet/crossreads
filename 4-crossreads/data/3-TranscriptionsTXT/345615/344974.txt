 [Page 46] 
  Unit  Regt which will possibly be coming to take their place 
 It hurt to see my Company  be lef  move off &amp; be left behind in Camp, their is nothing finer than the joy of being on the trek however hard it is. And I had the 'pip' all morning. 
 I put in a written application to the O.C. asking to be allowed to rejoin my company for ordinary duty but so far have had no reply. 
 Jack Stanton the L.H. Postal orderly also was 'fed up' as we have had nothing to do all day, so this afternoon we went up to the L.H Lines &amp; borrowed a couple of horses &amp; have been for a good ride &amp; gallop round the Camp and on the beach 
