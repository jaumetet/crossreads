 [Page 50] 
 46) 
 [Indecipherable] S.S. Minnewaska 
 12/11/15 
 Lemnos 
 We are now on Board Ship and the Distance from here to Gallipoli is 60 miles. We will reach there Some time tonight and join up with our Battalion tomorrow morning&nbsp; We were all on Ship yesterday and at the last moment we were told to get back to the Minnewaska&nbsp; the weather was to rough to land us at Anzac. we may not get away to day. But there is a Slight Chance of Doing So Soon. any how I hope that we reach there tonight. So that I will See Some of my old pals again. 
 finished. 