 [Page 23] 
 sides of the ship numerous loves of bread many of them untouched can be seen floating by besides bully beef &amp; potatoes. Too much is issued, &amp; what is not eaten is thrown overboard instead of being returned when it could be made into pudding. This time last year many could not get enough to eat and they may yet be glad of the bread they are not wasting. 
 The two prizes I won in the Literary Competition were paid to me today. 1/6 first prize for the short story, and 5/- second prize for the Reminiscence. 
 Gambling amongst those who have skill some money left still goes on. Sunday makes little difference. 
 Monday April 28 1919 5th sail from Cape Town. [line in shorthand] It is very stormy this morning, the sea is rough, and the ship is rolling a lot. The decks are wet with spray. 
 The Shropshire is a large liner that has been made into a troop ship. She is a twin screw steamship, built and engined by John Brown &amp; Co. Ltd. Clyde Bank in 1911. Length 525 ft. 