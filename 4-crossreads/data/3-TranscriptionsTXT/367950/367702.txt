 [Page 75] 
 Some of them are still in the air buzzing like angry bees. Cpl Smith &amp; Mather have returned from Zoabatia where they have been on outpost duty with B Squadron for some time. Dorricot the third man has gone to the Field Ambulance (Hospital) suffering from Dysentry. 
 4th December 1916 GERERAT Monday 
 packed up and vacated our previous camp at 10.30 am. Had to carry our blankets on the saddle and there was some fearsome looking bundles too. I borrowed a Kodak and took a photo of my horse. I don't think I could have found room for anything else I even had a shovel. Trakked across the desert and came into this camp at 10 00. There is nothing to write about at this camp &ndash; just a camp among the undulating sand dunes. We have a long way to water rather too far I think. No 1 outpost is 150 &deg; }   (bearings taken&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;   ) No 2 outpost is 209 &deg; }   (of the outpost positions) to camp 280 &deg; 
 5th December 1916 GERERAT Tuesday 
 patrols going out all day. There is a patrol of 20 men and 2 Signallers (Wilson and Mercer) under the command of Captain Brown 
