 [Page 283] 
 France 13/2/18 
 Dear Little Friend just a line thanking you so much for the lollies you sent they were very nice but not so nice as you for thinking of me. Well as you see by the date, I did not receive the parcel for Xmas but I was in Paris for Xmas and fairly enjoyed myself but I am wishing I could have one in dear old Sydney that would be much better never mind this war will soon be over and we shall all return again to the friends who have been so good to us. Well I must close with my best thanks to you and all at home 
 From your friend Will 
