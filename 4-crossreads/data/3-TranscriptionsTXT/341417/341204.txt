 [Page 399] 
 [Lieutenant, later Captain, Charles Edward (Chas) Viner, an accountant of Unley, South Australia, was born on 16th July 1852 in South Australia. He joined the Army on 16 June 1915 and embarked from Adelaide on HMAT A13 Katuna on 11 November 1915. He served with the 32nd Battalion, 8th Brigade on the Suez Canal Defences and later in France, where he was promoted to Captain, with the 5th Divisional Salvage Corps. He was mentioned in despatches and returned to Australia on 30 July 1919. 
 Charles Viner's letter to his sister Myrtle describes life in Egypt in December 1915.] [Address, written on a small piece of paper:] 
 Miss M. Viner c/o Mrs Mcgarry Cosmo 22 Charles St. Petersham. 
