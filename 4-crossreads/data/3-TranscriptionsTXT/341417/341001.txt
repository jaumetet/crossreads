 [Page 196] 
 [On letterhead of Pratten Bros., Jamieson Street, Sydney.] 7th Mar. 1916. 
 Mr. W. H. Ifould, Lane Cove Road, Turramurra. 
 Dear Mr. Ifould, 
 Enclosed please find a letter for your collection. It is from my Brother-in-law who at the time of writing evidently thought his last call had come, but fortunately is now much better. 
 Yours faithfully, F. G. Pratten 
