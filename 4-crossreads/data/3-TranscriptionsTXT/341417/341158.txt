 [Page 353] 
 via Honolulu and the Auckland Exhibition. Time has passed like lightening. 
 Coming back to Melbourne made one feel like a cat that had led eight out of its nine lives.  Bye the Bye, there are two of us now, only two. The tailor who pressed my green bow tie and glaring cheque suit stared at me with a grin of recognition. "Aren't you the bloke who drew me down the bay on the Hygea for a bob?" The old skipper was eighty four years old when I left Port Melbourne. He's still eighty four. [dash]? 
 With pride the proprietor of  the  a leading seaside resort  pointed  points out to me the great improvements at his hotel. A wash basin, a couple of roller towels and some soap  had  have been erected during my absence. 
 It's great coming home! 
 Right in the  camp  military camp at Enogerra, in Queensland I meet men whom I've caricatured years ago.  One who worked 
