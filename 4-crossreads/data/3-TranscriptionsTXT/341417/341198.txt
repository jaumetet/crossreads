 [Page 393] 
 [Several coloured sketches by Vasco, with descriptive comment about each one. See image for details.] [Sketch of ships at sea from the deck of a ship where a soldier stands disguised as a ship's funnel. With comment:] Submarine Picquet. One of the Ned Kelly Type a suggestion for the War Office 
 [Sketch of a soldier in a hammock, with comment:] In the Sappers' Boudoir. Time. Before reveille. Sapper Vasco has one more look at his wife's photograph. 
 [Sketch of a destroyer, with comment:] One of the five little torpedo-boat destroyers which dash out to meet our convoy, of nine within a couple of days of "the old Dart." 
 [Sketch of a cruiser, with comment:] The armored cruiser "Almanzora" our escort and guiding star from Sierra Leone. taking her morning dip. 
 [Page signed:] Vasco Jan. 1917. 
