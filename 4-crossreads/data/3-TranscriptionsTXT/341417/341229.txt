 [Page 424] 
 for rifle fire. Each side letting the other know that they are awake &amp; trying to bluff them sometimes as regard to their numbers. 
 One of my men got the D.C.M. the other day for bravery under shell fire. 
 We have had a unusual proportion of casualties. I mean our branch has. The same applies to the A.M.C. fatigue parties &amp; others of whom it was usually thought had a safe job. A number of clergymen who  done good work lost their lives, poor chaps. 
 A Battalion I am camped with had a "bathing" parade to-day. The sea is too cold. The cooks warmed 4 buckets of water &amp; then the men paired of to wash each other with about a pint of water apiece. The best method, owing to the water ration, is washing with a shaving brush. It is impossible to wash every day, when away from the beach for water is carried here on the backs of mules. 
 The Indians manage them &amp; have wonderful patience (they want it too) I have never seen them use a whip, just make a soothing noise or whistle by sucking in their breath. It is the only way. 
