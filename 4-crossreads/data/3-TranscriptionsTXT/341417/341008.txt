 [Page 203] 
 Preface Some account of the Battles during December 1916 and January 1917 on the borders of Egypt and Palestine by Clement Ranford, 3rd Australian Light Horse, and son of Mr. and Mrs. Joseph Ranford, Semaphore, South Australia. Clem Ranford is not yet 19, but served in Egypt, then at Gallipoli, and again in Egypt where he now, March 1917, still is. 
 His elder brother Joseph Marmion Ranford was killed at the Battle of Romani, Egypt, 4th August 1916, after serving in Egypt, through Gallipoli and again in Egypt. He was shot through the breast and died in about forty minutes, his brother Clement being with him when he died. 
 A.T.S. 
