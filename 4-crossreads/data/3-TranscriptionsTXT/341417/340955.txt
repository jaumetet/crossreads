 [Page 150] 
 [On letterhead of the British Red Cross and Order of St. John.] 13/7/18 
 Dear Miss King 
 Am dropping you these few lines letting you know I am still in the land of the living. I think I wrote before and told you I were in Hospital with gas burns but have now been discharged to a convalesent camp. 
 It is run by the BRCS [British Red Cross Society] and it is a fine 
