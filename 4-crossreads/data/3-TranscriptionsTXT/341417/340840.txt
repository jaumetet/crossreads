 [Page 35] 
 [On letterhead of Shepheard's Hotel, Cairo] C/O A.M.O. Cairo Egypt 30 October 1915 
 Dear MacNamara 
 On reaching here from Galipoli your letter bearing date 8th Septbr awaited me. For it please accept my thanks. 
 Glad to learn that your piece of the Country is looking prosperous &amp; hope that ere now you have been favoured with a copious rainfall. 
 The Norton Griffiths business is unsound in principal, in my opinion, and on that account has never appealed to me as being for the good of the Country, though its acceptance might have been for my gain financially it would not have received my support. 
 Sorry to learn about your Nephew Laurie, but please tell his Mother that she should be proud to be Mother to a boy amongst those who fought at Galipoli. 
 All accounts point to some loss of prestige in favour of Holman 'mongst his own people, he is a clever man who like the Prime Minister William has a knack for pulling the chestnuts out of the fire. 
 My earnest hope is that the Referendum proposals may be defeated this time, nothing could be more fraught with disaster to the stability of the Commonwealth than they. What will be the 
 [The bottom few lines from page 73 show at the bottom of this page. Not transcribed here; see page 73 for details.] 
 