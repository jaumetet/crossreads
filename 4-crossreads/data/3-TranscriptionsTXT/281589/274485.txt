 [Page 41] 
 was shunted down the Docks, the Dockyard was interesting, torpedo sheds etc. embarked about 8AM, had tea to drink, a light dinner but good tea &amp; plenty of it, Lovely day.  We left the wharf in afternoon &amp; anchored in the Sound. 
 Tuesday showery, good food, there are two other troop transports &amp; 3 bigger transports. 
 Wednesday showery good food. 
 Thursday 8th Nov threatening weather we left Plymouth at 10AM, there were 8 ships in the convoy, 3 with Aust, 2 Niggers (Africans) a Yankee patrol boat, a French steamer &amp; a Auxiliary cruiser, also an escort of 7 destroyers, made slow progress passing Eddystone Lighthouse, running into bad weather. 
 Friday threatening weather, choppy sea head on, a good number are sick. Our boat is named "Themistocles". Rough sea tonight. 
 Saturday 10th Nov calming down a little, was put on Submarine guard this afternoon, served with Ball cartridges. 