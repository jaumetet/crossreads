 [Page 34] 
 tonight they were not too bad, mail day. 
 Thursday 4th raining hard, fine afternoon head bad today, went on special sick parade, more mail. For want of something to do, the Military have nailed wire netting over one of the doorways of the hut, thus only allowing us the use of one door. 
 Friday cold showery weather still have bad head, more mail today. 
 Saturday 6th October, cold showery weather went to football match (Rugby) N.Z. v Canada, N.Z.won easily, Canadians didn't know much about the game. 
 Sunday cold showery weather, church parade. 
 Monday dull cold day, we are still taken for our morning &amp; afternoon walks. 
 Tuesday 9th Oct, cold &amp;amp miserable, better in afternoon, came on to rain at dusk. 
 Wednesday same weather as yesterday, was put on a boat roll this morning &amp;amp issued with more clothing. 
 Thursday 11th Oct, cold, frosty morning.  