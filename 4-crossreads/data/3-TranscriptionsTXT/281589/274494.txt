 [Page 50] 
 the Coronel bet one of the men that we would sail at the time he stated roll was called &amp; absentees names were taken. At 3.30 PM we had to go to our Messes  &amp; the roll was called again to see if any were missing but only two were missing. We sailed at 5PM, Miss Campbell was there to bid us fare well, she is a real good sort, the Men think a lot of Her. We were all sorry to leave Durban as it is a very good place. The Promenade on the Ocean Beach is very nice, though it was knocked about through a very violent storm about 6 weeks ago. The Public buildings, Town Hall, G.P.O, etc were very nice buildings, The streets are fairly wide &amp; straight though not a great deal of traffic run along them. Rickshaw men are very picturesque with their horns &amp; plumes. 
 Tuesday 11 Dec. raining all day, very miserable.  Wednesday fine though sea a bit choppy. 
 Thursday fine, a bit choppy, head bad tonight. 
 Friday 14th lovely day, calm, head bad. Started the games (Euchre, Crib, Bridge &amp; Draughts) tournament today, lost the Draughts and Crib. 
 Saturday lovely day head bad, on sick parade. 
 Sunday lovely day, bad head. Church 