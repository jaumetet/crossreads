 [Page 42] 
 Sunday 11th Nov. very calm, church parade, cloudy morning but turned out sunny after dinner, On the permanent submarine guard 6AM till 3PM next day 3PM till 6Pm, no night guard. 
 Monday fine day, calm, doing 3 guard shifts issued with a Red Cross parcel of clothing, 2 suits of pyjamas, 1 singlet, underpants, 1 shirt, 2 pairs of socks, 8 handkerchiefs, 1 towel &amp; a pair of shoes. 
 Tuesday fine &amp; calm day, getting quite warm now, only one shift of guard today 5PM to 6PM. 
 Wednesday 14th Nov fine, getting very warm, still on guard. Thursday same, but a concert given. 
 Friday 16th Nov. hot day, raining a little tonight 
 Saturday hot day, sports held on deck, a couple of whales were seen 
 Sunday 18th Nov. church parade hot day 
 Monday Hot day, still on Sub. Guard. 
 Tuesday Hot day, very close down stairs, nice breeze on deck of a night. 
 Wednesday sighted land breakfast time and ran into the Bay at  