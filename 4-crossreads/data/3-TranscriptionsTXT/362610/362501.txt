 [Page 218] 
 Thursday 12 October 1916 Fine but dull with a strong wind blowing that was decidedly cool. In the morning went down to the school at Oxalaere to run through a few tests &amp; after lunch went down again to the village. Trying to get souvenirs to send home, but did not meet with much success. 
