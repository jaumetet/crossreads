 [Page 193] 
 Sunday 17 Sept. 1916 A beautiful day. Took things easy not attempting any helmet-fitting with units. Felt inclined to go out for a stroll but thought that Leo might come along during my absence &amp; so decided to remain in. Got a little letter-writing done but was not in the mood for much. 
