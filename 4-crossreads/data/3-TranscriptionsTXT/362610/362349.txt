 [Page 66] 
 Saturday 13 May 1916 A wet cold day. Was orderly officer for the battn. &amp; kept going during the day, having the final equipping of the men to attend to as well. On the go until 11 o'clock getting my own pack &amp;c. ready. 
