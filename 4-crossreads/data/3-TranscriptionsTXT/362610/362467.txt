 [Page 184] 
 Friday 8 Sept. 1916 Fine but misty in early morning &amp; at night. Busy during the day fixing alarms, visiting the line &amp;c. 
