 [Page 13] 
 [Page 13. The image for this page was photographed out of order, transcribed in correct order] 
 both be all right. 
 What do you think of the bag I sent you they are a novelty, Lady Barron has one just like yours. Sir Harry Barron and Lady Barron are on the same boat as us. 
 Well we hope to get to some port tomorrow and I expect we will be there one day I sincerely hope we are allowed to go on shore for a bit of a stroll around. We crossed the equator yesterday and they dip nearly every one aboard who has not crossed it before but there was to many they could not dip us all. I missed getting dipped. We passed a boat on Sunday morning but it was about 20 miles off. 
 We do one hours work befor dinner and one hour in the afternoon that is all. I can tell you I do not want any more sea trips till we are coming home again. I think we will be on board about two weeks yet. It does seem funny we do not know a word about the war now. I suppose we will hear some war news tomorrow. 
 Well no more news for this time. So I conclude with sending my very best love to you and best wishes to the boss. 
 I remain Your ever loving Son 
 It rain here nearly every night. Nell told me that she intends to go and stay with you some time. Give me all the news when you write. WW 
 My Address Trooper W. Weaber No. 3272 26/9 Light Horse A.I.F. Egypt 
 See Over 