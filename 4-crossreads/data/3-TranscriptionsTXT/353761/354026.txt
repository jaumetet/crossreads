 [Page 118] 
 16 gnawed by a rat Thus in the field of war where there is death Nature insists with her eternal will that life shall still abide. 