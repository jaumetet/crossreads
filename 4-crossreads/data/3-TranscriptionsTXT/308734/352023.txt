 [Page 91] 
 Tuesday Feby 19th another frost, a very clear moonlight night.  10 [indecipherable] shining very nice.  had to scrub the tables in the mess hut.  read all afternoon.  10 PM went to bed.  10.30, 6 that went on leave came home and woke us all up. 
 Wednesday Feby 20th still morning, no frost, didn't have to do much work in mess hut, as the old ones were back, a draft went away.  1.45 had to go on parade, started to rain.  2.30 medical inspection, then back to mess hut to clean up. 
 11 PM some more came back that went on leave. 
 Thursday Feby 21st A clear morning, not so cold.  10 AM cleaned out hut, then printed some photos.  wrote all afternoon. 
 Friday Feby 22nd A big draft went away to France.  Got a new job, in the cook house now, 3rd cook.  but don't do any cooking. 
 Saturday Feby 23rd up at 6.30, weather mild, looked after fires and boilers all morning, put in an application to get into the Saddlers.  An aeroplane flew over the camp, very low down.  a very close evening. 
 Sunday Feby 24th 6.45 on parade, in the cook house all morning.  got 8 letters from Australia.  3 from home.  very pleased.  10 Aeroplanes flew over the camp during afternoon at a very high speed in [see text for inverted V] formation.  A bright moonlight night wrote letters all night. 
 Monday Feby 25th up at 6.30 had been raining heavy during night.  All the old mess orderlies lost their jobs, we all had to go on parade at 9 AM 
