 [Page 10] 
 Re Sgt GREEN  File with 1 LH Regt, AIF  
 38 Martin Place, SYDNEY 26 th October1926 
  Copy  
 The Chairman, Repatriation Commission,  Melbourne  
 Dear Sir,  WIDOW OF THE LATE JAMES GREEN  
 This pension case has come to my notice recently, and I thought a letter to you attached to the League's application for restoration of the pension would be acceptable to you and justifiable on its merits, if you will do the honour of considering it. 
 I knew Green in the South African War and later he joined the 1st L.H. (N.S.W, Lancers) and was one of our most regular attendants at drill, and took great pride in his turn out and soldierly manner for many years: he was one of the old type of militia who vied with Regulars in the preparation for drills and attention to all loyal matters. 
 At the outbreak of war, as a natural soldier, he enlisted in the 1st L.H., A.I.F: his baby son was christened at a parade before we left and his other sons were practically brought up as soldiers: his character was exemplary, and he was killed in a charge in front of Pope's Post on Aug. 8th 1915, thus ending a career, which he devoted to the Empire on the only 2 occasions of war. 
 Although he did not rise above Sgt., yet this makes his services and death none the less worthy: his widow deserves assistance by her country and the children should not be neglected by it: I understand Mrs. Green had the pension, but owing to circumstances  to  which you know, and which were only temporary these children are not receiving the des s erts their father built up. 
 Having known Mrs. Green for some 20 years or more, I know she is of the character and type of mother that should receive you full consideration: and I beg to stress that continued withholding of the pension is unnecessarily severe, when considering the loyal qualities of the late James Green. 
 I am, Yours faithfully 
 Late C. O. 1 st. L.H. 