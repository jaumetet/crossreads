 [Page 142] 
 Wednesday 25 September 
 Thursday 26 September Move from Biaches, marching to La Chapelette &amp; after awful delay, by train via Brie, Chaulnes, Rosieres &amp; Amiens to Longpre where we detrained. An awful journey partly in open trucks. 