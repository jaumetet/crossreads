 [Page 134] 
 Monday 9 September Move this afternoon to Buire where we are to establish a M.D.S. in bow huts which our people built in 1917 &amp; had to evacuate again. About nine kilos from the line. 
 Tuesday 10 September Working about M.D.S. fixing things up generally for work. The division takes over to-night at midnight &amp; our bearers are already up, with the battalions. 