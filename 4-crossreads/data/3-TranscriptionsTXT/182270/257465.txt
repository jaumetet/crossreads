 a6578169.html 
 MONDAY, JUNE 2, 1919. 
 Fine &amp; warm. Sister went down 
 by day train &amp; Father &amp; Mother &amp; Peter 
 went in in the big car. I was round 
 the bottom end in morning, tried 
 to fix the engine onto Middle 
 Bore, but had a job with the 
 pipe. Gave delivery of 30 bullocks 
 to T Considine and 1 bull for 
 him to deliver to Watt&nbsp;&amp; Abbot. 
 Went for a drive round the 
 Hill paddock in afternoon 
 with Linda. 
 &nbsp; 