 a6578048.html 
 983 
 SATURDAY, FEBRUARY 1, 1919. 
 Fine, but cold with an odd 
 snowflake dropping now &amp; 
 then. Went out to the Bank 
 in the morning. Alan came 
 down &amp; we both had lunch 
 with Jack Bridges at the Bath 
 club. Went to &quot;Oh Joy&quot; at the 
 Kingsway theatre after &amp; it 
 was the best show I have 
 seen over this side. I enjoyed 
 every minute of it. 