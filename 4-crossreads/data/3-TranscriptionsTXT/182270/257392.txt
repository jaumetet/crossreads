 a6578096.html 
 1031 
 FRIDAY, MARCH 21, 1919. 
 21 
 Fine &amp; warm. Shore leave was 
 granted. We got into the wharf 
 at 6.30 AM and started coaling 
 right away. Got off at 10.30 AM 
 and went up town with L t &nbsp;Morcombe 
 met L ts &nbsp;Jeffries &amp; Smith &amp; went out 
 to Haupt Bay &amp; back round 
 Table Mountain &amp; Groote Shuur. 
 Called in to get a feed of grapes 
 on the way home and struck 
 some jolly fine prople - the Betrams 
 &amp; spent the afternoon with them. 
 Had dinner at the Grand Hotel 
 &amp; went to &quot;The French Maid&quot; after 
 but could only sit out the first 
 act &amp; then came home to bed. 