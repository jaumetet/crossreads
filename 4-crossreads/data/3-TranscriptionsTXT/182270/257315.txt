 a6578019.html 
 954 
 FRIDAY, JANUARY 3, 1919. 
 Fine, &amp; very cold. Went down 
 &amp; fixed up with Sir Charles Wade 
 about getting back &amp; then to 
 the Bank &amp; sent a cable. 
 Knocking round town the 
 rest of the day. All had dinner at 
 the Pic at night, &amp; we had a 
 bit of a party at the flat at 
 night. 