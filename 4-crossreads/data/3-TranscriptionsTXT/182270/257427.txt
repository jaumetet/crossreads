 a6578131.html 
 FRIDAY, APRIL 25, 1919. 
 Fine &amp; hot. Went round the 
 bottom end of the run in 
 the morning and round 
 Hill &amp; Ben Ledi in the 
 afternoon. Things have not 
 changed very much since 
 I left. But from what I can 
 see things are in a very 
 big mix up here, &amp; I can 
 see a lot of work in front 
 of me. 