 [Page 216] 
 H.M.S. Chester, when the boy Jack Cornwall received the "Victoria Cross" - The Captain was talking for 1&frac12; hours and it was intensely interesting. 
 July 11th at 3 PM a funeral service throughout the fleet took place for the victims of the HMS. "Vanguard" &ndash; several bodies had been picked up in the harbour since the explosion and were interred in the naval cemetery close by, and for several days, wreckage and oil was floating about the harbour in very considerable quantities, pieces of furniture caps shirts, and in fact, all manner of articles could be seen in masses. 
 July 16th  the morning the fleet [negative?] "Barham", and the 2nd Light Cruiser Squadron which included the "Birmingham" "Southampton" "Melbourne" "Sydney" and "Dublin" went to sea for 48 hours exercises &ndash; During P.M. we carried out firing exercises on firing ground 
 July 17th during a.m. Squadron exercises were carried out, and PM> firing exercises 
