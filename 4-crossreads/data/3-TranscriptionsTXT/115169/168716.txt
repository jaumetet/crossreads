 [Page 24] 
 Aug 28th  Should have been no day, but this change in time did not effect us because we were returning again on the morrow probably. 
 Aug 29th  Jubilation was caused because we were informed by wireless that H.M.S. "Highflyer" had met and sank the Kaiser Wilhelm Der Grosse off W coast of Africa 
 The weather became heavy and everything was got in readiness for battle in the morning, as we all seriously expected much opposition, and a possible meeting of the German Squadron. 
 Aug 30th At 5.30 AM we stealthily arrived off the Southernmost point of Samoa and steaming rather close in land we headed for Apia, and off which place we arrived at 10 A.M. - very quickly we discovered that no enemy warships were in the vicinity, altho nothing was left to chance, all guns being manned and every man at his battle station. 
 Apia had every appearance of being a very nice 
