 a6491017.html 
 Jan 10  th  &nbsp; The rain chased me off the deck at 2.A.M. but it cleared up at 10 A.M. &amp; the rest of the day was beautiful - we kept in sight of land all day - read all day &amp; played euchre in the evening. 
 &nbsp; 
 Jan 11  th  &nbsp; We were out of sight of land for a few hours, but came in sight again at about 4.30 P.M. We played crib in the evening. We slept on deck but it wasn&#39;t too good, it was windy &amp; cold. 
 &nbsp; 
 Jan 12  th  &nbsp; We got into Cape Town first thing. all the troops went ashore at 12.30 except a fatigue party (&amp; I made one) who were kept on board to load stores &amp; worked like horses till 7.30 P.M. with the promise that we should go ashore the following day - We went ashore after we had finished our work &amp; had a hot bath &amp; a feed &amp; went aboard again at 10.30 P.M.&nbsp; We shipped a gun &amp; gun crew - 
 &nbsp; 
 Jan 13  th  &nbsp; We pulled out into the bay at 7 A.M. &amp; dropped anchor - we were not allowed ashore as promised, and it was a lovely day wasted. 
 &nbsp; 
 Jan 14  th  &nbsp; Sunday &amp; we spent a lazy day laying about all day - lovely day. 
