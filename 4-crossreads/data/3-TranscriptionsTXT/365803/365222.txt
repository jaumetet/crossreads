 [Page 78] 
 British Expeditionary Force France 17-7-16 
 Dear Mrs Ferguson 
 It was with very deep sorrow that I heard of poor old Arthur's death, I feel it was the loss of one of the finest men, &amp; undoubtedly the best friend I have ever known. I wish to express to you all my very deepest sympathy with you in your sorrow. It is so hard to write anything that it is perhaps best for me to say no more than what I have said. 
 Yours most sincerely, 
 R.A.V. Connor 
