 [Page 46] 
 Tuesday Morning 
 Dear Judge 
 Thank you for thoughts of Myles and acting for him &ndash; your love reaches a long way. 
 And Hebrew's letter. As I read it a phrase [indecipherable] much in my mind &ndash; I  [indecipherable] my concordance &ndash; bible, and here it is. 
 They that serve in tears shall reap in joy. He that goeth forth and wasteth, bearing precious seed, shall doubtless come again with rejoicing,    bringing his sheaves with him  Ps CXXVI last two verses 
 Yours affectionately 
 [indecipherable] 
 