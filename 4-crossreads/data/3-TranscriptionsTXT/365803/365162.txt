 [Page 18] 
 9th July 1916 Sunday Night 
 Dear Judge Ferguson 
 I  [indecipherable] your offer of a seat with you but Pat   &amp;  Pixie went with me. 
 It was a fine service, and a fine service &ndash; indeed, the distinction between the "dreams" and the "Vision", was a lesson not to be forgotten 
 Yours 
 Dick 
