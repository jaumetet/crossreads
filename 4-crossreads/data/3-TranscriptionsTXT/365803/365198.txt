 [Page 54] 
 Div. Headquarters 16.7.16 
 Dear Dorothy, 
 Your nice long letter which arrived yesterday has helped me to make up my mind at last. I have refrained from writing simply because I hadn't the heart to. At first this was all the more so and in addition the pressure of work way very heavy. Since then I have been wondering how to make a beginning. You know exactly how I feel about Arthur &amp; I know how you feel. I shant say anything 
