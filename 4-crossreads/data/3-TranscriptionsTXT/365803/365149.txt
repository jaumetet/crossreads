 [Page 5] 
 Sydney Grammar School Sydney 
 July 3. 1916 
 Dear Mr Justice Ferguson, 
 I feel so keenly the loss of each of our boys that I can scarcely bring myself to write &ndash; such loss can I forbear. 
 When I think how you write to remembering the boy whose unassuming courteous nature endeared him to all, I seem to live again the days of my own trouble and know once more what it means to the father on whom the heart broken mother leans more than ever. Sweet soundness in life honour in death &ndash; seem to round a complete life &ndash; and yet!&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ! 
 Yours my trust 
 Arthur Miles 
