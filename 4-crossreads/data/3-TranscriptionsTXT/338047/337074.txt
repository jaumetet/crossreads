 [Page 358] 
 1919 July 28 "Floorwalker".  Sat in the Anzac lounge for a while &amp; then back to the boat about 10.30 pm. 
 July 29 On 29 July we bid farewell to Capetown, pulling out from the wharf about 7.30 am. We left about 8 am but did not lose sight of landtill nightfall, as we hugged the coast.  The Education classes were moved downstairs on account of the weather which now became very cold.  We ran into frequent fogs for four days &amp; [Five photographs around Cape Town] 
 