 [Page 37] 
 9: Skating at Rue Rosette, had a good time &amp; had supper with American naval men who were in Alex. 
 10:  Driving around town in gharry,sight seeing Went toSultans Palace but could not get inside. 
 11:  Went to Alhambra to see an English company; had a good night. 
 12:  Left Sid Bishr 9 a.m., reaching Kantara  5 p.m., after a good holiday. 
 17:  English ladies visited camp, serving fruit salad to the boys.  Funny to see them Marching up with dixies, etc., &amp; we gave the ladies some rousing cheers for their generosity. 
 27:  Rugby v. 1st regt., lost by 6 nil, this being our first defeat. 
 November 
 2:  Left Kantara 6 a.m. halting at Duedar for the night.  Moved on at 9 a.m. the next morning, arriving at Romani in pouring rain at 4 p.m., carrying one blanket only &ndash; no oil sheets.  Left Romani on the 4th at noon; camping at Hod Anders for the night.  This hod was named after a Hun officer, whose carcass 
