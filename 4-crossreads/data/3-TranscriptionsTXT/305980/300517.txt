 [Page 20] 
 20:  Went to Wardan to try to get back with my unit as I am fed up with this ambulance.  Made arrangements to get back within a week. 
 21:  Fined 5/- for overstaying leave for two hours, owing to the train not connecting with Cairo line to time. 
 28:  Left Kebir at 7 a.m., rejoining my unit at Beni Salama, where 1st Bde. is under canvas.  Received 50 odd letters; first mail since leaving Sydney.  Parcels had gone to Gallipoli &amp; given to the boys, so someone benefited from them. 
 February 
 1:  Getting rough again regarding rations, half pound of bread; 2 ozs dates &amp; tea for 24 hours.  Canteen doing good trade as we buy our own food here. 
 9:  Rugby 2nd. regt. won by 12 &ndash;nil. 
 12:  Orders to move at midnight.  Boys visited a Tommy camp where they had a lad tied to a crucifix in the sun, for taking a tin of milk from the dumps.  Guard turned out, but our boys took them in hand a chopped down the cross with an axe, 
