 [Page 75] 
 for 3 reasons; 1stly for our mail, 2ndly to learn where our destination was to be and 3rdly, if leave was to be granted us. Slept on deck, night being glorious. 
 September 19th: - Fine all day. Divine service in morning and at 11 am, I, along with Reg, Bob, Nick &amp; Jack went ashore on leave until 5 pm. Spent a pleasant day, though I think Alexandria a dirty &amp; uninviting place. 
 A large camp occupied by the Australian Ammunition Column &amp; another, where an Infantry brigade was encamped were paid a flying visit. 
 Such a dreary waste of desert surrounds the town; the smells arising from the native quarters and the dirty customs of the race make the place in my opinion loathsome, although there are fine gardens in the European quarters which are fairly extensive. 
 Visited the famous Catacombs which contain mummies dating thousands of years back, but I think that this is the only place of interest throughout. 
 At 5-30 pm upon our return, and inspection of the ship was made by the chief medical officer then in the town who expressed appreciation at the appearance of our boat. Slept on deck again. 
 September 20th: - A happy day was to-day for we received our first mail from Australia. Owing to the letters 