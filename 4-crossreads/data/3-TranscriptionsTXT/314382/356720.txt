 [Page 37] 
 saluted as we passed. 
 During the afternoon, 300 Territorials who were recovering from fevers &amp; wounds were taken aboard, some being admitted to the ship's hospital immediately. These chaps told some stirring tales of incidents at Gallipoli and of the conflict on the continent, for 70&percnt; had fought in both places. 
 On a glorious evening at 5 pm we left and turned into the Mediterranean which was of a most glorious blue colour, and the last lap of our journey began. I was in fine spirits and perfect health, &amp; so had good cause to be, as I indeed was, very happy. 
 August 4th: - A glorious day and one which passed without an incident of note occurring. 
 August 15th: - To-day, a day as perfect as day can be. Divine Service held at 10 am and a parade at 3 pm. Slept on deck with Reg, Nick &amp; Bob. 
 August 16th: - Our unit completed hospital duty to-day it being taken over by No 2 ship. The weather still continues to be perfect and we have lazy days before us for a week or so. Slept on deck again. 
 August 17th: - At 6 am, Sicily was sighted, objects on the shore being plainly discernible. Until 4 pm 