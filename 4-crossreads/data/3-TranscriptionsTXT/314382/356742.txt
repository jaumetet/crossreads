 [Page 59] 
 September 5th: - Another glorious day. 
 In camp all the morning and caught the 1 pm train to Bournemouth which lies about 30 miles away. As this place is a favourite resort we were fortunate in visiting it on a day when the attractions and crowd were both numerous and brilliant, for the fine promenade and pier were thronged with a host of remarkably distinguished and well-dressed people. 
 The beach which is a pebble-strewn one looked picturesque with it's numerous coloured bathing machines, though, strange to say, considering the perfect day that it was, there were few bathers. To reach the beach, one passes through a small but delightful park which contained dozens of plots of brilliantly hued flowers, and a number of small ponds which are stocked with gold and silver fish. 
 Left at 6 pm &amp; arrived at Southampton at 7 pm when we went to a concert on the pier. 
 September 6th: - Went for an early morning swim this morning and later for a walk through the portion of the Common which so far we had not seen. 
 On guard from 2 until 4 pm so was unable to leave camp. 
 After tea we visited a couple of Soldier's rest houses and went to bed early, sleeping under the trees. 