 [Page 31] 
 their clothing in front of them to the bank. They then dressed &amp; darted off.  The CO troops (Major Jacobs) as soon as he heard of the matter placed a strong patrol on the Canal bank and also ordered a boat patrol to prevent further escapes. 
 He also ordered strong guards to be placed all over the vessel to prevent attempts to escape. 
 A good deal of difficulty was experienced in trying to get the men for duty in my company as most of them were by this time either in bed or already on duty. The fall in was sounded and a good deal of confusion resulted. 
