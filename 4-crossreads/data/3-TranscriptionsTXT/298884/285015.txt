 [Page 85] 
 A medical officer came on board &amp; inspected the hands &amp; arms of everybody on board to satisfy himself that no small pox existed on board. Despite the fact that we have so far a very clean health bill it was reported in the W.A  newspaper  Times that a case of small pox had broken out on our boat soon after leaving Colombo. 
 After an exasperatingly long wait in the sun the Western Australians, two nurses &amp; five others were allowed off.  They have to do three days in quaran- 
