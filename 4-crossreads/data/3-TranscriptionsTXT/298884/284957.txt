 [Page 27] 
 popular forms of amusement on board amongst the diggers. Of course the two-up kings have their little game thought the game is not played nearly so promiscuously amongst these chaps as is generally the rule with a crowd of our men. We seem to have a specially select group of men. It is a pleasure to talk to any of them. Their language of course continues to be rather livid but it is a habit which is hard to drop. 
  13th  The Mediterranean Sea has been as calm as a mill pond all day. We anchored at Port Said about 5.30pm.  We 
