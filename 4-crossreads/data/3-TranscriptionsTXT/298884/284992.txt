 [Page 62] 
 a trial too if the language of the chief mate &amp; the Chief Engineer is anything to judge by. 
 The ship  has  only   doing  did about 7 knots a hour this morning due to bad coal and inexperienced firemen. 
 But she picked up her old speed of 13 knots this afternoon. 
 This afternoon was the balmiest we have had since leaving England.  Everybody slept or lolled around on deck too tired to move hardly. If only I could have found a few tin cans I should have been delighted to  have  
