 [Page 44] 
 We parted with our natives at the hotel for a very small amount about half a rupee each (ninepence in English money). We had a cool drink each a splendid cup of tea with cakes and some  bananas &amp; paw-paws. This latter fruit I had never tasted before &amp; I must say that with the lime &amp; a little sugar they were very nice indeed. 
 We then wandered into a silk shop in the hotel premises. After being pestered with business cards &amp; natives trying to cajole us into buying some 
