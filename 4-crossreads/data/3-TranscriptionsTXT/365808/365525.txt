 [Page 39] 
 The Bulletin 214 George Street, Sydney 10th August 1917 
 Dear Judge Ferguson, I have this morning a reply from Lindsay with regard to the Bear cartoon. I cannot do better than quote his own words, "Would you let Judge Ferguson know that he can have the Bear page for &pound; 5 [indecipherable]. He is such an amiable man, that I would not like to charge more" With best regards Sincerely yours   [M?] Macleod  
