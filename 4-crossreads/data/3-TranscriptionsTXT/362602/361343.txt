 [Page 16] 
 2nd Day after Sailing 
 (1915) At Sea On same ship as we left Port by. 
 Dear Dad, Mother &amp; George, 
 We are having glorious weather &amp; the Pacific (Peaceful) Ocean is calmer than Sydney Harbour.  We have been able to see the coast all the time &amp; some of it is very nice but most of the coast line is rugged hills &amp; cliffs.  There are numerous light houses all along the coast &amp; this morning we sighted an island on the Port bow about 10 miles out to sea.  We have passed a number of ships passing northward but although this is a very slow packet nothing has passed us yet.  I suppose the Suevic will soon catch us up.  There were numerous birds flying round the ship last night &amp; many gulls rested 