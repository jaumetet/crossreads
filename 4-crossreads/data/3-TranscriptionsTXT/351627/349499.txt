 [Page 16] 
 I have had only one letter from mother in the last  four  three months did mother send you one of my photos she said that she received them. 
 This is a large place three storeys high it used to belong to the Sultan of Egypt but lately I believe it was an hotel &amp; now it is a hospital &amp; a good one too 
 Well dear Aunty I must close now as there is no news. 
 With love from Your affec nephew Norman Sherwin 
 Tell Aunty Evie I will write to her in a few days 
