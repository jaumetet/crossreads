 [Page 8] 
 weeks as the usual routine of camp life goes on, the weeks pass on to August 21st when we have some range shooting. Mounted gaurd at 5pm on till 5pm. Thompson; Gallaher; Day went on their final leave, so the day looks near when we shall say good, bye to Sydney town for a while &amp; plenty  for ever. 
 23rd/ Easy day , fatigue work, it looks like being wet for my final leave. 
 25th/ Went on final leave to Taree with my mate Harry Milner 
 26th/ Arrived at Taree. 
 27th/ Wired for three days extra Reply, granted to 31st. 