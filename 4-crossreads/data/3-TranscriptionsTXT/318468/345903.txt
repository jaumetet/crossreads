 [Page 41] 
 to see whether they could get anything usful for the Coy to use.   A few tools were procured.   A few nights since some of our fellows got away from Camp &amp; proceed to the wreck &amp; it was rather amusing to see some of the things that were brought back on the quiet.  Included in the collection were &ndash; Steering wheel, about 60 large candles 2"shells,cantas &ndash; Knives forks &amp; spoons &ndash; part Machine Gun belt &amp; shells &ndash; skin Rum &amp; various other articles.   They were coming in from about 12 to 4 a.m.    This was out tents doings.   Started work again to-day was with a party on Reservoir 
 2-4-15.     Usual no    Holiday for those who are not on important work. 
 3-4-15   Usual work &ndash; nothing fresh &ndash; cool change set in again. 
 Sunday 4th.    Work only for few today.   First all have not had to work on Sunday 
