 [Page 20] 
 properly yet, and that is the reason I am here.  My wounds are healed.  Also my right side is numb, at least it was, but it is getting better now.  That is another reason I am here.  So there you have it, &amp; in a month or    two I'll be as right as a bank. 
 Maj Skirving was very good &amp;    tried to get over to France to see me.  I wired to him when I got here &amp; he came to see me yesterday. 
 Give my love to father &amp; Dorothy and Mr. O'Reilly also Catherine and anybody else who wd be glad    to know that I am bright &amp; cheerful. 
 Your loving Son. Keith. P.S.  Glad you enjoyed tour N.Z. trip P.S.2 How wd I do for signing cheques. KF. 
