 [Page 283] 
 1917 September 
 23 Sunday 
 Weather perfect, but feeling very depressed.  Not able to write owing to the diversions of pea nut stand &amp; head thick from quinine or something.  Ears aching &amp; dull of hearing from the same cause. 