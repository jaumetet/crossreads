 [Page 195] 
 1917 June 
 27 Wednesday 
 Lunch with Mrs Holman, Mrs. H.G. Wells &amp; Agnes Platt at Rubens Hotel. Went to Am.Ex. for money &amp; then on to Caf&eacute;. Miss Hodge &amp; I went shopping &amp; to tea &ndash; I came home &amp; washed head. Nell &amp; Kath came to wish me goodbye. 