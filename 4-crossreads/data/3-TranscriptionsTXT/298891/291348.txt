 [Page 282] 
 1917 September 
 22 Saturday 
 Miserable day paralysed.  Feeling very depressed.  The breezes send down a shower of gold from the elm trees.  Sister Moyan came into sick tent from Scotchivin. 