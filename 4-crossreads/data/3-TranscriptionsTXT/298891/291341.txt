 [Page 275] 
 1917 September 
 15 Saturday 
 Waked at six.  Very frail so lay in till 9 a.m. &amp; then ran till 8 p.m. with but 1 hour's interruption exclusive of meals. Feeling so exhausted that I crept to  Matron  bed to escape Matron but she came to know what I had done with her lamp &amp; I did not have it at all. 