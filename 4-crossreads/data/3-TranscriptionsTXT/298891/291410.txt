 [Page 344] 
 23 Friday 
 Dr. De Garis took me with her to V. to have automobile parts supplied.  Little Capt. Alex. with arm wounded gave us lunch in his room.  2 women came in &amp; greeted us.  Lovely day.  View superb. 