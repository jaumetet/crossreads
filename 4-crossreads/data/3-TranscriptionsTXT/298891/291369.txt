 [Page 303] 
 1917 October 
 13 Saturday 
 Heavy rain at intervals.  Allowed to leave sick tent &amp; go to my own &amp; to mess tent for meals.  Sisters Davis &amp; Maxwell took me for a little walk in late afternoon to try my stiff leg.  Pelting night. 