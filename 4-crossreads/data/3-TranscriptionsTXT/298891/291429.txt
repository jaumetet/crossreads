 [Page 363] 
 12 Wednesday 
 Mild drizzle. Went to Vodena with Sisters D. &amp; Urquhart. Had tea with Dr Petrovietch. Got home about 8.30. Travelled back with Serb. Capt. Just back from Russia via Archangel &amp; Liverpool &amp; Winchester. Very tired of the war in all its aspects. Leaves still on the big plane tree in Vodena main street. 