 [Page 240] 
 1917 August 
 11 Saturday 
 Busier day because Brown was off after lunch. In evening patients were entertained at simple games. How very simple the male of the species is. With him running the planet little wonder it is being run off its axis. Went swimming with Read &amp; Freece. 