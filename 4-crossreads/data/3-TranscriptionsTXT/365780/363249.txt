 [Page 21] 
 untill their painful breath at last began to fail Upon there way to death let pity draw the veil They would not mind the blow but cut of sound &amp; sight Of comrades [indecipherable] fire they passed to endless night Deep down on Oceans floor far from the wind &amp; sun They rest for ever more.  Goodbye to AEI. 
 No 3 A harder fate was theirs than mens  who fight &amp; die But still Australia care &amp; will not pass them by. When. Honors lists are read there names will surley be Among the gallant dead who fought to keep us free There winding sheet is that there Sepulchre is wide The sea birds scream and wheel when silently they die There is a monument of History began. When down to death  they went  Goodbye AEI 
 The End 
