 [Page 96] 
 16/2/16 Serapeum SERAPEUM on the EAST side of the Canal about 12 miles South of the Bitter Lakes. 
 Still put address on letters without mentioning "Serapeum" 
 1st Field Amb, 1 Aus Brigade, Aus Intermediate Base Egypt 
 This is a ,u>damnable Camp &amp; we I believe only to be allowed a water bottle of water per diem so I fear we will get very dirty. We are on our 