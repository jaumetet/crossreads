 [Page 49] 
 [Postmark] ARMY POST OFFICE 24 De 15 FIELD SERVICES POST CARD 
 The address only to be written on this side. If anything else is added, the post card will be destroyed. 
 Mrs W.H. Read "The Wurley" Cleveland St Wahroonga Sydney NSW Australia 