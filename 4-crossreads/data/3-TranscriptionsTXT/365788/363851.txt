 [Page 70] 
 should be able to become a Sergeant. Very much love to you all dear. Ever your loving brother. Fred 
 P.S. You dog! I have never seen as fine a horse as Charlie since I left. If he is dead, he should be embalmed. F.J.P.  Please don't send any cigarette papers as I have plenty now. It is very good of Will to offer to give me a new suit &amp; I am very grateful. 