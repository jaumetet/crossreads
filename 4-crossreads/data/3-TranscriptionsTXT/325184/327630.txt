 [Page 50] 
 Friday 25th July 
 In office all day 
 Saturday 26th  July With Nell and Chris left for London by car at 7.15 am. At 10 am proceeded to Buckingham Palace to attend an investiture. Received the honour of Knighthood at the hands of the King and was invested with the Insignia of KCB, CMG and DSO. Returned to Tidworth at 7pm.  Gen Marshall of Mesopotania also invested. 
 Sunday 27th July In office all morning and part of afternoon. Took a turn round the golf links after tea. 
 Monday 28th July Left for London by car at 7.30 accompanied by Nell and Mr Denning. Took route via Grateley, Winchester, Farnham, Guilford and Tooting. Attended Conference at Gen Monash's office. Left London for Tidworth at 5pm and arrived 7.45pm. 
 Tuesday 29th July In Office all day. 
 Wednesday 30th July In office during morning. In afternoon interviewed [indecipherable] Gibson Comptroller QMAAC Southern Command on the question of retention of QMAAC personnel in our Messes. After some discussion it was agreed they should remain until we close here, and that the girls should be accommodated in Bhurtpore Buildings. 
 Thursday 31st July With Nell Chris  and Mr Denning  left Tidworth by car at 7 am for Liverpool via Swindon, Worcester and Chester. Spent the night at Midland Adelphi Hotel. Met T.H Kelly of Sydney. Police on strike in Liverpool. 
 Friday 1st August Inspected S.S. Argyllshire. (Lt Col Clarke OC Troops) at Riverside Docks and found everything satisfactory. 
