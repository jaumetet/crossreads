 [Page 21] 
 re delay in sailing of Wyreema. Went on to Warminster returning to Tidworth at 5pm.  In evening attended dinner at Regimental Institute. 
 Thursday 27th March With Col Williams &amp;pm left Tidworth 7 am for Lewes via Portsmouth. Took Alvord with me for a day's run. Addressed the Staff and inmates at Lewes and interviewed several NCOs and men. Returned to Tidworth at 9.15 bringing Alvord with me for week end. 
 Friday 28th March Left Tidworth at 9.15 for Cardiff via Swindon arriving at 12.40. Inspected SS. Port Macquarie. OC Troops Lt Col Robinson - and lunched on board talking with the troops afterwards. Accommodation and arrangements good. Left Cardiff at 5pm and arrived Tidworth 8.15pm 
 Saturday 29th March Left at 9 am for Codford. Addressed No18 Quota. At 12.30 called on GOC Southern Command returning to Tidworth 1.30 In office all afternoon. 
 Sunday 30th March Left 8.45 am by car for London. Had an interview with Gen Griffiths. Afterwards met Gen Foote and with him called on Gen Monash. Left at 11.45pm for Liverpool. 
 Monday 31th March Arrived Liverpool 6.30 am. Inspected SS Khyber OC Troops Major Hoskings. Ship was clean and accommodation good. I spoke to NCOs and men as they came aboard. Left Liverpool at 2.10pm for Bristol arriving at 8.55pm and remaining the night there. 
 Tuesday 1st April Left for Avonmouth and inspected SS Shropshire &ndash; Major Matthews OC Troops. Ship well fitted out. Talked to men as they came aboard. 
