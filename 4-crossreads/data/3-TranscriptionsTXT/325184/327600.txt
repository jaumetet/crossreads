 [Page 20] 
 left for Whale Island (HMS Excellent) Portsmouth where Middies are to remain till posted to ships. Returned to Tidworth at 8.15PM 
 Thursday 20th March Remained at HQrs all day 
 Friday 21st March Motored to Swindon catching train for Devonport to inspect S.S. Kildonan Castle &ndash; OC Troops Lt Col Brazenor. Ship well fitted out and arrangements good. Troops quite contented. Returned to Tidworth 9.30pm. 
 Saturday 22nd March In office during the morning.   In afternoon with A &amp; C left for Andover arriving Waterloo 6.35pm.  Left Euston for Liverpool at 11.45pm. 
 Sunday 23rd March Arrived 6.15am at Liverpool. After lunch visited SS. Czar. OC Troops Lt Col Imlay. Ship in good order and clean, all on board well satisfied. Left at 10pm for London. 
 Monday 24th March Arrived Euston 4 am. Left Waterloo 6.15 arriving Tidworth 9.30 am. In office during morning. In afternoon addressed Nos 14 and 15 quotas at Heytesbury and visited No 15 Camp Codford re ordnance. Left for London arriving Waterloo 6.35pm.  Left Euston 11.45 for Liverpool. 
 Tuesday 25th March Arrived Liverpool 6 am. Left Hotel at noon for wharf. Inspected SS. Port Denison. OC Troops Lt Col Duffan. Troops quarters very good. Lunched on board and conversed with troops. Left Liverpool at 11.45 for Euston. 
 Wednesday 26th March Arrived Euston 5.50am and reached Tidworth 11.45 am. In office till lunch. In afternoon addressed a deputation of No 1 Quota at Codford 
