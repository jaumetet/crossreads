 [Page 40] 
 [Telegram.] E.T. No 1. Commonwealth Of Australia Postmaster-General's Department, New South Wales. 
 Telegram This message has been received subject to the Post and Telegraph Act and Regulations.   All complaints to be addressed in writing to the Deputy Postmaster-General."] [Office Date Stamp "Crow's Nest N.S.W. [indecipherable]"] Station from, No. of words, check, and time lodged.   Remarks. 
 Melbourne 29   2/1 6 p  15th Mrs A. I. Alley 400 Miller St NSW Copy to C Nest Now reported son Sergeant Geo. U.F. Alley still progressing favourably.    Will furnish further progress report when received.   Base Records 
