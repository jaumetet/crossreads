 [Page 34] 
 S.S. Port Darwin At Sea, 3.12.18 I am an extraordinarily unlucky individual.  However I am hoping all may yet be well.  And I will explain why I make moan. 
 On the night of the 14th we got into open Trucks again &ndash; a scandalous proceeding that side of the Canal &ndash; about 11 p.m. &amp; started for Suez.  It was the coldest ride I have ever experienced I think.  We we [were] shunted about the docks at Suez for a long time &amp; did not get opposite our boat the "Port Darwin" until daylight.  We then got on as our names were called &amp; ticked off etc., etc. 
 We started after dark that same evening. 
 Now this is where my bad luck becomes prominent.  Bear in mind 