 [Page 12] 
 killed 
 25-7-1917 Raining, The railways both heavy and light very busy. Tons of ammunition coming through.  Drawing a number of  canv-as back, ready for an advance. All ammunition to be packed from old to new position. 
 26-7-1918 Rumoured that we move wagon line forward in a day or two, up to Dickiebusche. Three miles from here. Smoke shells going to Bty. Today is fine. Still no shoes. Horse Master amongst our horses. Last night was the first night since we have been in action, that gas shells were not used. Huns evacuated their first line, fell back to their second, 150 prisoners captured, got the wind up the mongerels. Our baloons and planes up in hundreds. This village very seldom get shelled, my opinion is, that it is full of spies. 
 27-7-1917 