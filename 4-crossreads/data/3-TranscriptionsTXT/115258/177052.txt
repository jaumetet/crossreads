 [Page 282] 
 A Cry from the Wilderness 
 To Sister Susie 
 Dear Susie, you&#39;ve lived in Tasmania&#39;s fair isle 
 Midst peaceful surroundings in nature&#39;s fond smile 
 Whilst we have been fighting in dust and in heat 
 Or storming great mountains: The Hun foes to beat. 
 Of course for the empire you&#39;ve well done your share 
 Growing apples in Tassy so rosy and rare 
 Still - I know you&#39;ll relent when you hear me complain 
 Of the ills you have done us &nbsp;- Here let me explain. 
 &nbsp; 
 Apple jelly is sweet and was prized by the crowd 
 Of men over here, and at first they felt proud 
 That Tasmania had sent it; but changed when they found 
 They were doomed to endure it, the dreary year&#39;s round. 
 A poor soul that&#39;s been damned and cast out into space 
 For ever in darkniss, its lone paths to trace 
 I blest beside us, if Jones continues to send 
 Apple Jelly unceasing, till this wicked war&#39;s end. 
 &nbsp; 
 Since the subs have been sinking our shipping so free 
 Your apple exporting&#39;s gone all up a tree 
 But it&#39;s we have to suffer out here in war&#39;s zones 
 When you send all your apples via Hobart to Jones 
 Well the cure for al this I can print out to you 
 Your apples for export - just let the pig&#39;s chew 
 Dont keep them for jelly. Turn &#39;em all into ham 
 And in future tell Jones to send changes of jam. 
 Tpr. Willie. 