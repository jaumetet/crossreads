 [Page 7] 
 9th. M.G. Coy No III Intelligence Report from 6 am 2nd to 6 am 3rd:- Right Sector:- Four shells dropped on suspected enemy M.G. emplacement at 10.45 p.m. Reported destroyed. Fired from No 5 gun on I 12 Central to I 17 d 0/7 No 6 gun I 17 b 5/8. One gun has been removed from Post C. and is to be put in position near White Farm. Left Sector:- Sniping at Machine Gun positions was carried out by enemy throughout the night. 
 BJ. Carey Capt 9th M.G. Coy 3-12-16 
