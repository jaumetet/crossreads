 [Page 23] 
 9th M.G. Coy No 14 Right Sector:- No 6 fired from I 10 b 26/11 on defensive works at I 18 b 3/3 from 10 &ndash; 11.45 pm. Hostile bombardment during afternoon near No 5 night firing position, and No 1 gun. Left Sector: No 7 fired from I 4 d 9/9 to I 12 a 4/9. trenches &amp; buildings round RETALIATION FARM. Unobserved. fired from 7 &ndash; 10 pm. Light good- Wind favourable to enemy 
 B J Carey Capt 9th M.G. Coy 14.12.16 
