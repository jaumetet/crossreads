 [Page 30] 
 F 17 Sun &amp;hot. Shower at 6 pm. I go up to Hall &amp; sweep &amp; clean &amp; wash up. D &amp; I go for a walk round through milstone. We are hot &amp; tired  &amp; I am thirsty. Wrote P.C. to Dad. Mum. George. Marie &amp; Aunt Annie 
 F 18 Mon Lovely Day. I go up to Hall again for final clean up. Play Hockey against Waac's Have nice time. Do Night Part of orderly Corporal. Wrote Jennie. Capt. Robinson. Reg. Roy. Bess. Ada 
 F &amp; W 19 Tues. Catch first Train to London. Go H'ferry Rd Round Lamberth Bridge up through St.Pauls &amp; Holborn F. 
 London Bridge. Tea at Aldwych Theatre &amp; supper in Strand. Go to Drury Lane see Samson &amp; Dealilah [Delilah] &amp; sn tho. Buches orchestra play at 11 Aesb St. 
 F 20 Wed Glorious Hot Day. Go to London Bridge, Kingsway. Catch 11pm Train arr home 2pm. Recd. Letters from Home Marg &amp; Nell. Go to bed at 7.30 &amp; read. Go for walk round block with [indecipherable] meet Bainty [indecipherable]. 
 F 21 Th Lovely hot Day. Recd letters from MRL. Jennie &amp; Doris. Arr Ord Corp. Go to merry-go-round with Dick. It is fine. The laughing Jackass is there again Wrote to MRL. 
 F &amp; Hot 22 Fri It is a real summers Day. I go for ride round B'ford after Tea. Then write all night. Wrote Marg. Dot (8) Laura. Norm Upton. Dulce. Go to bed at lights out &amp;  read for a little. 
 F 23 Sat Glorious Hot Day. Recd letter from Aldington Hunt. I play Hockey against N Z's over at Sling. We lose 6 goals to nil but give them a real rattling game. Col. comes over to see us &amp;  we all have one photos took. Dick &amp; I after Tea have bath &amp;  go walk round Block. I sit  &amp;  write all night. Nettie. Lottie. Muriel. Alice. Mrs White. Go to bed at lights out  &amp;  read.    Recd. Letter from DAD  
