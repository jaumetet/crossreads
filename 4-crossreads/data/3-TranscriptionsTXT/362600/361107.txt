 [Page 60] 
 F 13 Sun Glorious Day. Recd letter &amp; photo Peg. Letters from Mum 3 Nette. Ken Pine. Ruby G. Reg. Park. Tap. E &amp; I go up to [Boscon Le Down?]. See scores of Planes. One dives into the earth. We play cards all night. We have Poultry for Dinner. 
 W 14 Mon Recd letter from Auntie Annie with sweet nothings. Jim Robby comes over. We play till 8.30 in office &amp; I go down to play with Dick &amp; Ellis afterwards Write Peg. Nette. Ruk. 
 F 15 Tues. Recd letters from Home 5. George. Clare. Lily P. Bess. Pay day 
 Wrote Bess. Etta. Ken. Pine. Lily Porman. Clare. Jennie 
 F 16 Wed Lovely Day. Ride to Sarum. Go to both Homes &amp; do gardening. Meet Jim O'Brien. Take Girls back to [Blooms?]. Recd. Letter from Connie. Stay at d013 Have very nice time &ndash; Dream of [Jen Mather?] swimming in river. 
 F 17 Th Dream I fall the [indecipherable] right in untidy underwear on 17, 10.18. Lovely Day. Moonlight night glorious ride Home. Hang on Motors Tailboards. Go to Dance. Home [indecipherable] time. Keith comes over. [indecipherable] M. &amp; I chat E. is bored. Recd parcel from Home with cake, chocs &amp; Sweets. 
 F 18 Fri Dull. Recd. Letter from M.R.L. go up with Tam for Hockey. Very strenuous. Go to Pictures with Jim Robby. See [His appointed Love?] Wrote to M.R.L in Slumbering Fires [indecipherable] [indecipherable] go walk towards P House. Lovely moonlight. 
 F 19 Sat Meet HR &amp; T in Town. Play Hockey with T.W. Ride to Village with Dope. Go to Dance. Dance without stop until 9 pm. I am very tired physically &amp; glad to get to bed at 9.30 pm. Pick all good Dancers &amp; have Royal times 
 Wrote H.R. Auntie Annie. Ethel. [indecipherable] 
 