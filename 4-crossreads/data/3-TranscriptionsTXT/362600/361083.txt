 [Page 36] 
 28 Sun Dull  &amp; Rainy Shower. D &amp; I have on French Lesson with [more?]. Wrote Aunt Annie. Jennie. Nancy Dewan. Connie. Mrs Edwards. Esme. Wrote PC of London &amp; Photos to Dad. Mum &amp; George. 
 F 29 Mon Played cricket for first time. Do pretty well. Come home &amp; J &amp; I sit &amp; talk. Wrote Mrs Stephen &amp; White. 
 F 30 Tues. Recd. Letter from Mum &amp; Marion Clarke &amp; Dad. Pay Day. D &amp; I have French Lesson. We sit &amp; yarn after with Ellis Mr Buchanan from D.J's comes over to see us. 
 F1 May Wed Go to get Hilarty &amp;  Charlie chap in [Correct?] dress. George Slatter goes with me. Recd. 2 letters from Doris Preddey. Col. Raffon comes down. 
 F 2 Th Chat with George R. Recd. Letter from Norm Upton. Sew all night &amp; pressing also. Ellis goes on leave at midday Play cricket. 
 F 3 Fri Wet. Tropical Rain Storm comes over at 7pm. Recd letters from Masie &amp;  Photo. letters &amp;  lots of photos from George. Letter from Bess. Letter Edie. [Taubin?] Sew, press &amp; generally tidy up. Play Cricket. 
 F 4 Sat Glorious Day. On Bays. Play Cricket &amp; win by 8 &ndash; 1 36 to 42.ov. I go &amp;  have long yarn in French to Mac. Come Home &amp; generally mess about   &amp;  read etc. 
