 [Page 3] 
 the pavement. Please think of me on Tuesday, duty officer, first day at sea messing about down in the 'tween decks holding sick men's heads. 
 Well Babs I've nothing to say except goodbye. Give my love to old John &amp; the lad &amp; also say goodbye for me to Mr Mac. Wilkie &amp; everyone else 
 Yours ever Sam. 