 [Page 96] 
 May-June 1915 There was hardly a shot fired on either side. 
 Monday 31st Came out for 24 hrs in the rest trenches at 6.30am.  Did a bit of digging on the morning &amp; a good sleep in the afternoon.  Alf Outridge came round for a yarn.  I heard that John Kirby was missing &amp; that Arthur Wright was wounded. 
 Tuesday 1st June We had a quiet morning in the trenches.  Things are getting too quiet altogether &amp; it is hard to realize you are taking part in a war at all.  The Turks shelled the trenches just before we went in but did no damage except for a few sand bags.  The sun gets quite warm in the middle of the day &amp; the flies are getting fairly plentiful. 
 Wednesday 2nd Came out at 5am &amp; 