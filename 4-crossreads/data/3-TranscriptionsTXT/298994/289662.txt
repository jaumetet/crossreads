 [Page 14] 
 the morning "dailies" I did not attend the match myself. 
 Lance Corporal Ringwood and myself had a fine afternoon's engagement. We took the tram and had a nine miles'  right  ride round the fine suburbs of Durban. A commanding view of the harbour could be obtained. We visited the native portion of the town later in the afternoon; this being set apart for them, some two miles out of the main portion of Durban. 
 Views of Durban, and its environs were eagerly bought by the men, to send home to Australia. The streets of Durban are very spacious, likewise are the buildings particularly fine. 
 Durban has a fine coaling machine it is a treat to watch it at work! 
 I also visited the whaling station, which proved interesting. The whaling grounds are situated some sixty miles out of Durban. 
 I must give volumes of praise to the 