 [Page 29] 
 Now when this war is over, and we've captured Kaiser Billy To Shoot him would be merciful and absolutely silly Just  when  send him down to Codford, there among the Rats and clay And I'll bet it won't be long before he droops and fades away! 
 Codford is not so bad as this, we soldiers so exaggerate our troubles, a soldier has the privilege to growl, he is a good soldier if he does, we must therefore all be good soldiers. 
 Pack up all your troubles in your old kit bag and smile, smile, smile says the song, but a hardened pessimistic soldier said in reply, when asked to pack his troubles in the said bag, said he didn't have enough bags! 
 The camp was early astir on the morning of the 8th Jan 1917. We left Codford, making our way to the station, we were going to London on furlough How sweet this sounded to me! 
 It was quite dark when we left. We entrained about 8 and after a three hour ride in the train, we arrived at Paddington 