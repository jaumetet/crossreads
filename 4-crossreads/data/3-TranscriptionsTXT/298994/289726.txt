 [Page 78] 
 ready to leave this camp again. 
 We had a very early breakfast the next morning (25/3/18) about 2 A.M. The machine gunners of A Coy left this camp before the rest. We marched to Loere, here the whole of the 4th division were conveyed in motor lorries to a new "home". 
 We passed through large numbers of French towns on our journey such as Hazelbroach, St Pol, Lillers; a good number were deserted by its inhabitants 
 After a tedious journey, we finally reached Gouy at 5.30 on the evening of 25/3/18 Gouy was an interesting little town, on the 26/3/18 we had an inspection of rifles etc, we were wanted to go up to the line any time, so we were not allowed to leave our billets on any consideration We had a false alarm just at dinner time, we moved out into an open field to await further orders, but nothing eventuated. 
 At 9 o'clock in the night of 26/3/18 we moved out of our billets and proceeded towards the line. 