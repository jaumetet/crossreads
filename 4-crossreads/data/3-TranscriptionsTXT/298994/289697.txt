 [Page 49] 
 We were relieved by a regiment of Tommies on that night. 
 Another 5 miles of hard going and we were comparatively safe again We stayed the night at a camp, known as Wheal Camp. We did look pretty objects the next morning with the slush &amp; mud! 
 In the afternoon, we had orders to be ready to move off from our camp at 1.30. We embarked in motor buses to a "new home" or billets This was Sept 1st 1917. 
 Passing through Bailleul again, we passed through more or less large towns, until we finally came to a dead stop; a mile or two from Hazelbroach We were billeted in a farm house, about 50 of us occupying a hay shed. 
 We had a church parade the next day, the sights of the countryside were all unfamiliar to us (the new ones who had joined up) 