 [Page 66] 
 We all  it  did I think! 
 No one seemed to know our future movement, Rumour gave it out that we were out of the line for a four months spell, not the 4th Divy's bleeding luck! 
 The leaves were falling fast at this time of the year, they were indeed beautiful to behold. 
 We did not do extra much drill, while here, about 4&frac12; hrs daily. 
 The Y.M.CA have erected a marquee and supplied it with Aussy papers etc A canteen is now established right against our billets. Things are not too bad! 
 Nov 1st 1917 is known as "All Saints Day" in France. The bell across the way has been ringing continuously Large numbers of people have been going to and fro to service. It is some day! 
 I went to Vinelay today, a small hamlet about 3 miles from Lugy. 
 A terrible bombardment must be going on on the front, we have heard 