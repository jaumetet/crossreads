 [Page 9] 
 Wednesday 7th July 1915 
 Washing day &ndash; a unique &amp; novel experience.  Sunshine &amp; rain during the day.  Slept on deck atmosphere too close below &ndash; pleasant dreams of home. 
 [Paragraph crossed out] 
 Thursday 8th July 1915 
 Warmer weather &ndash; becoming more pronounced every day. 
 [Paragraph crossed out] 
 Keeping in great trim &ndash; boxing &amp; physical exercises practiced daily.  Feel a little tight across chest but nothing serious. 
