 [Page 3] 
 The World War 
 Rough Diary of my Service in A.I.E.F. 
 Enlisted 26/4/15 at Victoria Barracks.  Entered camp at Liverpool 21/4/15.  Transferred from Inf. to Signalling Depot 27/4/15?.  Subsequently attached as Signaller, 20th Battalion, 5th B'de. 
 26th June 1915 
 Battalion left Liverpool Camp at 8.30 a.m. &amp; entrained for Woolloomooloo Bay where we boarded troopship "Berrima" for port "unknown" (rumours said Egypt).  Wife, Father, Mother &amp; Sister met procession from Central Railway Stn. at Goulburn &amp; Elizabeth Streets &amp; accompanied me to steamer,  except Dad who having Mab in his arms had to break off at Oxford St.   Had a great send off.  A crowd of small motor boats loaded with friends of troops, accompanying Berrima to Heads.  A rough night out, many sick myself O.K. 
 [Indecipherable] Liverpool 9-5 a.m. &ndash; left Heads about [indecipherable] 
