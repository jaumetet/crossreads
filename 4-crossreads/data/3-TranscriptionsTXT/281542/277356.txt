 [Page 43] 
 or drowned rats.  Our Sappers complain bitterly of the stench &amp; many of them have given up sick at heart &amp; physically overpowered.  The swollen bodies at the least touch burst with the report of a rifle &amp; the puss  &amp; stench  squirts forward like water from a hose.  Many of the bodies are maggoty &amp; eaten away by rats &amp; other vermin &amp; the nauseating odor floats through the trenches with the evening breeze in stifling intensity. 
 This afternoon first issue of tobacco &ndash; 1 oz. each man, or packet of cigs.  Matches also issued. 
 Friday 27th Aug. 1915 
 There has been fierce fighting on the left flank  toward  in the vicinity of Chocolate Hill &amp; WW Hills.  The attack was made at 4 p.m. &amp; was covered by heavy Naval artillery.  It raged 
