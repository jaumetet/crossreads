 [Page 55] 
 of fighting ships in it. 
 31-5-18 It was a beautiful morning here just like Sydney Climate. It makes one feel like jumping out of his skin. The boat we are on is the worst boat to live upon I could get the Decks are of Iron &amp; one slips all over the place, the food is awful. We left Lemnos Island the harbour I mean 7.30 PM. for the Dardanelles the lads a joyful at going. We passed the Shropshire as we going out the harbour I am told that she has been to England since we left her &amp; brought more troops to Lemnos Island for the Dardanelles. 
