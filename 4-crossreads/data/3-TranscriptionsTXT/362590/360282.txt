 [Page 22] 
 R.M.S. "Osterley." 19/1/16  Confidential  
 My dear Judge 
 I regret very much that I was unable to accept your kind invitation to dine with you last night but the ship was taken into the stream early in the evening &amp; I felt that my place was on board with the men, I had a telephone message sent with my apology which I trust you received. 
 I may mention that the men are very indignant at the disparaging statement in the paper re NSW men, as you know I put them on their honor with regard 
