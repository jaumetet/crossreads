 [Page 75] 
 C/o Miss Colquon "Glenfruin" Katoomba 
 Feb 10th 1916 Dear Judge Ferguson 
 I thought &ndash; you, &amp; Mrs Ferguson might &ndash; be interested in the last news of Evan &ndash; I am enclosing a letter of Dr Bean's &amp; one of Evan's also a copy of a letter I have just had from the Hon Mrs Edwards &ndash; Evan is still the guest of Mrs Edwards, &amp; her only daughter the Countess Gleichen whose husband is a General in the Guards! At present at the front in France. When Evan gets his official leave many homes are open to him in different branches of the Wendoves family &ndash; My relations &ndash; Mrs Edwards says "I cannot 
