 [Page 85] 
 6. past, unless it is of how a well liked mate went out. Touching tales of just before the evacuation, how men would for a second stand bare headed silent beside some grave, of cold, snow, &amp; Jonny Turk, some of our Infantry understood the turk, &amp; left little things for him, Phonographs left ready to play some Turkish air, to please him, Watches hung that when lifted 
