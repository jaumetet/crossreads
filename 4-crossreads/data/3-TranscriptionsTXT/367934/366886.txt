 [Page 4] 
 Somewhere in England! June 30th 1918 
 Ma Cherie 
 J'ai des nouvelles vertigineuse!  No more France for Muh!  I'm not writing too long a letter.  Too excited! 
 Brave little Woman.  [line of kisses]  I just pray that you're well and happy.  The old Victoria tune is ringing in my ears.  They're wonderfully good in the hospital and there's a glorious garden with real thrushes as my Irish actor friend says dropping their "nhotes from the trays".  Each day one feels the benefit of a complete rest.  I have absolutely everything and people are wonderfully good.  At present I am in A Ward F Block, Maudsley Hospital, Denmark Hill, London S.E. 5.  We get an absolute rest here and I am told afterwards either home service or perhaps Australia.  Don't say anything to relations or build too much on it but there's absolutely no need for further anxiety.  We have every reason as "Hosky" says to keep smiling. 
 Send all letters to 11th Field Coy. Aust. Engrs., France or Abroad.  They forward my mail immediately.  Hospital addresses change all the time. 
 Love to Jen, Jack and all at home (not forgetting the dorg). 
 Your Vasco forever and ever and ever.  [line of kisses] 
 