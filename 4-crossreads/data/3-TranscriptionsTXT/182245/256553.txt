 a6576064.html 
 272 
 1917 FEBRUARY 
 7 WEDNESDAY 
 Fine &amp; Sunny, &amp; fairly warm. The 
 snow started to thaw a bit to 
 day &amp; about time too. I was on 
 stableman all day &amp; had a 
 good day on my harness &amp; 
 got it all in A1 order. The 
 whole battery was called out 
 &amp; had to take amunition to 
 the pits at night. We only 
 sent up 4 horse teams so 
 I did not have to go, thank 
 the Lord. 
