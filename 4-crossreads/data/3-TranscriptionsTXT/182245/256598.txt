 a6576109.html 
 317 
 MARCH 1917 
 24 SATURDAY 
 Fine &amp; Sunny but with a very 
 nippy wind. Rode down to 
 Steenwerck in morning with Rohu 
 for rations, from there on to 
 Erquingham &amp; Armenti&egrave;res 
 &amp; had dinner there. Went on 
 to the battery position after. Old 
 Fritz lobbed a 5.9&quot; shell about 
 15 yds off me as I was going past 
 &quot;Le [?on]&quot; dump. He shelled out a 
 battery of NZ guns about quarter 
 of a mile from our position today. 
 I went &amp; had a look at their pits 
 &amp; there&#39;s no doubt their shooting 
 is marvellous. Had tea 
 with Joe Clonan at his billet 
 &amp; then came on home. 
