 a6576153.html 
 361 
 MAY 1917 
 7 MONDAY 
 Fine &amp; beautifully warm. We 
 picketed our horses out in 
 the sun &amp; as I was stableman 
 I had a good old sunbake. I 
 cleaned my saddle in morning 
 &amp; then read the rest of the day. 
 There was an exercising parade 
 in evening. Big bombardment 
 by old Fritz all morning all 
 round Bac S t  Maur &amp; he got 
 a lot of civilians. Very big 
 bombardment round Houplines 
 at night. I got home letters &amp; 
 one from Linda 19 th  March. 
