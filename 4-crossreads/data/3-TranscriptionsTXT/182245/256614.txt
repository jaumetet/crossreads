 a6576125.html 
 333 
 APRIL 1917 
 9 MONDAY 
 Raining in the morning &amp; 
 blowing very cold in 
 evening. Cleaning harness 
 act all morning&nbsp; ^  two teams &nbsp;&amp; went 
 up to the battery in evening 
 We have always to take 
 six horse teams now, so 
 one man does not get a 
 spell every now &amp; then. 
 We had a progress &quot;500&quot; evening 
 &amp; had some good games. Bill 
 Constable &amp; self were a tie 
 for first place with two other 
 couples &amp; got second place 
 in a show poker hand for 
 places. 
 &nbsp; 
