 a6576133.html 
 341 
 APRIL 1917 
 17 TUESDAY 
 Rain &amp; snow in morning &amp; a very 
 cold gale with it. We moved out 
 from Wallen Chappel about 8.30 AM 
 &amp; passed through Hazebruck 
 &amp; camped at Longuenesse on 
 the edge of S t  Omer about 1 pm. We 
 camped in a barn there, &amp; after stables 
 we got leave to S t  Omer. It is a 
 fair sized place, but very dirty. It 
 has a big church there with a 
 beautiful organ &amp; pulpit, but 
 it has a cheap look about all the 
 place. Anyway it was good to 
 get a bit of leave &amp; be free for a bit. 
