 a6576176.html 
 384 
 1917 MAY 
 30 WEDNESDAY 
 Fine &amp; warm. Building the pit 
 up all day &amp; making it 
 shipshape. Bill Hunter &amp; I 
 started a dug out of our 
 own in evening &amp; got it 
 going all right. We were 
 paid in afternoon. Old Fritz 
 sent a lot over in morning 
 behind us, but nobody 
 hurt much at all. We sent 
 over a lot all day 
