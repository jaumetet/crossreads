 a6576093.html 
 301 
 MARCH 1917 
 8 THURSDAY 
 Snowing most of the day 
 with a very cold wind 
 blowing. I was on stableman 
 all day. Hear the Germans 
 put a few shells into 
 the factory near the baths 
 in Point de Neippe &amp; killed 
 a few civies. Heard from Poss Moison 
