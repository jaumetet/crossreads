 a6576035.html 
 243 
 JANUARY&nbsp; 1917 
 9 TUESDAY&nbsp; 
 Dull &amp; misty rain all day &amp; very&nbsp; 
 cold. Took the horses out for 
 exercise in the morning and 
 grooming all the afternoon. 
 there was a big bombardment 
 going all day at the front. 
 Saw little bunches of wooden 
 crosses hanging on trees 
 along the road. The French 
 peasants make them &amp; anybody 
 killed near there they have 
 a cross handy to put on 
 his grave. 
