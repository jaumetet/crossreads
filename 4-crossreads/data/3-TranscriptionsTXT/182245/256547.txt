 a6576058.html 
 266 
 1917 FEBRUARY 
 1 THURSDAY 
 Fine &amp; Sunny. About stables all 
 morning &amp; went up to Armenti&egrave;res 
 with a G.S waggon in afternoon 
 Things were very quiet up there 
 though, hardly any shelling at 
 all. Got a letter from Mother. 
 
 from PAGE 157&nbsp;&nbsp; 6.6.17 
 tonight God keep you sweetheart 
 &amp; remember I always loved 
 you &amp; that I tried to do 
 my bit for the good old 
 homeland &amp; you Linda dearest 
 Heard Alan was safe, but was 
 a prisoner in Germany &amp; had 
 lost his left arm. Poor Alan 
 but its great to know he is 
 safe. 
