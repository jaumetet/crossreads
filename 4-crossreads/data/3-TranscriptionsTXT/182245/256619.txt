 a6576130.html 
 338 
 1917 APRIL 
 14 SATURDAY 
 Fine &amp; fairly warm. Cleaning 
 harness all day, &amp; I got mine 
 all clean again. We were to 
 go up to the battery position 
 to draw amunition at 
 night but that was cancelled 
 We went down the road to 
 an Estaminet after tea &amp; spent 
 the evening there. All our 
 guns came down at night. 
