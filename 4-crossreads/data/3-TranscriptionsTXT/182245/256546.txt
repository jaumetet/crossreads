 a6576057.html 
 265 
 JANUARY 1917 
 31 WEDNESDAY 
 Snowing all day off an on, but 
 the sun also shone a good part 
 of the day. Had an inspection 
 of horses in morning. by some 
 [ret? vet?] major &amp; exercising 
 horses in afternoon. Got a 
 couple of letters from Linda, &amp; one 
 from M rs  LLoyd &amp; father in morning 
