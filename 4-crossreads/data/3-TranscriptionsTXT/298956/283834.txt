 [Page 39] 
 put into barracks inside Germany's largest test flying station to await transport to Denmark. 
 We stayed at Warnemunde until 12th December, and enjoyed our stay there as there were lots of things of interest there watching the Flying machines and Sea planes.  Well on the 12th a 150 of us we put on board a small steamer named "Niels Ebbesen" a Danish steamer, and that night we steamed outside the breakwater and anchored until morning. 
 Very early next morning we got under way bound for 
