 [Page 89] 
 83 November 2nd French to return as the French were not prepared to send the German officers,  and second  as they were afraid the Germans would convey back important news as regards the movement of troops from France to Italy &ndash; 
 November 3rd Slept in till noon &ndash; Recd letters from Aunt Min, Father, Mrs Gowing &amp; Mr Asher &ndash; all dated about 10th August. &ndash; 
 Played Bridge again today &ndash; Walk held this mng &ndash; I did not go &ndash; Had my foot bandaged &ndash; 
 Recd my money today 160 marks, also paid for month of November 48 marks &ndash; Not feeling very well today &ndash; 
 November 4th (Sunday) No church service this mng owing to the indisposition of the German Padre &ndash; extremely cold today &ndash; 
 November 5th Slept in till noon &ndash; Played Bridge. Cinema tonight, some very good films were screened. Had another big guest evening &ndash; 
 All flying Corps officers were paraded after roll call, rumour says that they are being sent to a camp specially constructed for Flying officers &ndash; 
 Recd letter from Army &amp; Navy 
 November 6th Slept in again &ndash;  God it was a sad one  No debate tonight &ndash; 
 Special farewell concert tonight to say Goodbye to Flying Corps who are leaving at 3 am 7/11/17 &ndash; Commdt bade them goodbye &amp; a safe journey &amp; hoped that the time was not far distant when we could all go home to Blighty &ndash; The Concert was a very enjoyable one at the conclusion we sang Auld Lang Syne &amp; God Save the King &ndash; All hands busy saying good bye, some to their best pals but such is the life of a Kriegsgefangener 