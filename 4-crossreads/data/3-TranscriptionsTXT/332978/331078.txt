 [Page 5] 
 were issued to the contrary, I kept my diary at a great risk to myself. 
 I concealed it always, in different "hiding places", as we were invariably subjected to periodical surprise searches, and the discovery of my diary would have involved punishment, in the form of confinement to cells. 
 Luckily the diary was never detected. 
 When warned that I would soon be leaving Germany for Switzerland, I was at a loss to know how to conceal my diary. Acting on the advice of one of the 1914 prisoners, I got the camp bootmaker to remove the soul of my right boot, and conceal as much of the diary as was possible to do, between the inner and outer souls. This he did by 