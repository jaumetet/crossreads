 [Page 20] 
 Jan.30th. British Air Raids on German Towns &ndash; During the day I meet Madame's daughter (age, about 45) and other of Madame's friends &ndash; Political Unrest in Germany &amp; Austria. 
 Jan 31st. Change of Programme &ndash; "Fuzzy Wuzzy" &ndash; strikes in Germany- War rationing in England &ndash; In a leisure half hour I run across a gold mine in the shape of a shop containing Shakespeare &ndash; Homer. Dickens &ndash; Tennyson &amp; Kipling etc &ndash; Weather colder. 
