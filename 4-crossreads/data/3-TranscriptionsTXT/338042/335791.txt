 [Page 15] 
 Afternoon spent with George Martin and evening (including supper) with Harry &ndash; I write a sketch (or is it an apology) entitled "Failed to Start" 
 Jan 20th Sabbath &ndash; Pay day &ndash; Reading &amp; Writing &ndash; We journey in the evening to Steenge (?) where we give a show to 27th Batt. (3rd Div. Artillery) arriving back at Ballieul late in the night. 
 Jan.21st. Lloyd George says "Fight On! &ndash; Change of Programme &ndash; Letter to M.B. "Les Miserables"- Fred Reade in Paris. 
