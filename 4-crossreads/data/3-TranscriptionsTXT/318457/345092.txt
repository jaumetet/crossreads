 [Page 12] 
 [9 August 1914] 
 As on the previous Sunday, prayers only was read, since the war routine was continued daily. 
 The rendezvous was reached Sunday morning 9th Aug, at 10am. The Flagship was accompanied by the "Parramatta". The other two destroyers "Warrego" and "Yarra" had followed us up to Thurs. Id. when they met us and proceeded with us to the rendezvous. 
 After calls had been made, and our new surgeon Dr Percival Provost of Sydney, had been transferred to us, the fleet sailed on a course N80E. Eventually we found we were to proceed with the Flagship to Rabaul, capital of German possessions in New Britain. Special order was sent to us to make a destroyer attack upon this place, our ship supporting them, on Tues 11 Aug. 
 During Monday afternoon the "Emden" was visible for some hours on the horizon. She joined up on Tuesday night. 
 At 5pm we parted company with the Flag and the three destroyers with us, started off at 22 knots op St Georges Chan between New Britain and New Ireland. The course is shown on the map. The night was clear but fairly dark. The moon would rise about 9.45 so the work had to be done before that. After we had entered the Chan. speed was reduced about 9 Knots. The destroyers, all black, followed by our ship, stole up 
