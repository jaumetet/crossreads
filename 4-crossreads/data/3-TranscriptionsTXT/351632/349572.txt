 [Page 27] 
 just halved, as many wounded must have drowned.  Thus we had an abnormal list in the enemy ship added to our own.  The ship was overcrowded and most unsuitable at any time as a hospital ship;  we were delayed 48 hours round the scene of action &amp; we were 4 days steaming from the nearest hospital at 18 knots. 
 The services of Surgeon Todd, R.A.N. were invaluable, &amp; he was at the disadvantage of having served afloat only 14 days, &amp; those immediately preceding the action;  also he was in indifferent health at the time. 
 I specially recommend the services of T. Mullins, S.B.S. whose endurance &amp; energy were wonderful. 
 The work done by the first aid party &amp; volunteer party was most useful, unstinted &amp; remarkably intelligent.  I might mention in this respect the names of &ndash; 
 Francis Holley, M.A.A., R.A.N. William Sweetland, 2nd S.S., R.A.N. Frederick Tilbrook, Off. Std., 2 cl., R.A.N. John Donnelly, P.O., R.N. Herbert Holmes, A.B., R.N. Jas Hill, A.B., R.A.N. John Fulton, Cks. Mte., R.A.N. 
 Below is a list of stores used during the period:- 
 Chloroform 2 lbs. Drainage tubing 1 yd. Sal. Alembroth wool, 12 lbs. Absorbent wool, 18 lbs. Surgeon's lint, 16 lbs. Boric lint, 10 lbs. Bandages 4 inch, 10 doz. Bandages 3 in. 10 doz. Bandages 2 in. 15 doz. Cyanide gauge 12 yds. ea. 6 tins Moist gauze 6 yds. ea. 1 jar Iodoform gauze 6 yds. ea. 1 carton Triangular bandages 24 Iodine, 1 pint 
