 [Page 23] 
 him up soon as possible. 
 Glad Mrs. Hine hasn't forgotten you, we were to have had a good time in England but passport troubles cut the trip right out;  anyway it could have cost about &pound;300 which is too much to spend on spec, or am I getting mean? 
 I sent your kiddy's photo home as I couldn't carry it too well.  A platoon Sgt. has generally quite sufficient papers &amp; books to carry. 
 Best wishes Yours sincerely A. Hine, Sgt. 
