 [Page 178] 
 12 Sept, 1916 For when you've come to weigh the good 'an bad &ndash; The gladness wiv the sadness you 'ave 'ad &ndash; Then 'im 'oo's faith in 'uman goodness fails Forgits to put 'is liver in the scales. 
