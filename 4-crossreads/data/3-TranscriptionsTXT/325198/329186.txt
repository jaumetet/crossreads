 [Page 2] 
 [Joseph George Burgess served in Gallipoli and returned to Egypt in December, 1915. His rank of Lance Corporal was restored following demotion at Gallipoli.  He was in hospital with a septic hand through most of July and August and so missed the action at Romani and was not engaged directly in any action during the period of this diary. He served in Egypt and Sinai for the remainder of the war. Where the writer has apparently omitted a full stop, a dash has been inserted where appropriate, to make for easier reading] 
 [Diary cover] N236&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Corporal Joseph George Burgess A Troop B Squadron 6th Light Horse Egypt 
 [Pages 3 to 26 are printed information pages, not transcribed] 
 