 [Page 55] 
 same, I only wish I could describe the bald headed, shifty eyed, weak-kneed, chinless, yellow fanged, &amp; heavens only knows what else of a man, what a character he would have been for Charles Dickens to picture &amp; describe. 
 After tea tonight 4 of us went up with the elevator &amp; pinched the piano, &amp; so this evening we are having a little music &amp; singing by some of the lads &amp; nurses, &amp; very good some of them are. 
 2nd. Have been expecting Len all day but he never turned up must have been stopped somehow for he said he would be here this morning in his letter of yesterday. 
 Have been getting tons of mail since coming over here &amp; plenty of parcels I get parcels of flowers now &amp; then from different people including Mrs Duke &amp; Miss Sparkes. 
