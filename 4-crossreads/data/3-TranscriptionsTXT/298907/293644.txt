 [Page 59] 
 this afternoon &amp; I quite enjoyed myself, I have got all my stripes &amp; etc up &amp; I look like a zebra. 
 Got a splendid parcel this afternoon from Mr Duke strawberries apples &amp; oranges, the strawberries must have cost a pretty penny for they are a distinct rarity over here at this time of year so the nurses say, he's a bonza old chap is Mr Duke &amp; it's a real pleasure to know him, something like a friend. 
 This afternoon I tried a good dozen shops trying to get some chocolates you wouldn't believe how scarce &amp; dear they are, you get about a dozen for a shilling, from what I can understand the shops are rationed &amp; only allowed a certain amount each week, there seems to be plenty of food though in Birmingham judging 
