 [Page 62] 
 &amp; then into Heliopolis A.G.H.  Arrived back at 11-30 pm &amp; turned in very tired. 
 3.2.16 Had rifles issued to the whole Company &amp; have been doing Rifle Drill all day &amp; in consequence of same am feeling far from lively. Still no mail for me &amp; am getting sick &amp; tired of waiting for some. 
 For reference sake I put the following item down.  Had said  "Through no fault of mine I was to-day severely admonished in front of several of the rank &amp; file during the episode several obscene words were used which I objected to.  I made enquiries  re being  seeking redress, &amp; was told that I had a strong case, but probably on account of rank I would be the sufferer.  But I was told to keep this matter in memory for any future happenings of the same kind. Witness SQMS &ndash; All this happened on the 2/2/16 Line blocked from Port Said do not know cause. 
 4.2.16 Another hard days Rifle Drill with route march in the afternoon. 
