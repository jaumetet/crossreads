 [Page 22] 
 early &amp; later rec'd our accumulated mails, 35 letters &amp;, 6 parcels, 1 Journal &amp; 1 Paper.  After dinner had good  [indecipherable] bath &amp; change.  Had long walk round Camp roads &amp; turned in about 9 p.m. 
 Jan. 11th Tues. Began full routine, Physical D. 7 to 7-30 a.m., Bkfst . 8 a.m., Morning P. 9-30 to 11-30, Dinner 12-00.  Afternoon Parade 1-30-4.30, tea 5 p.m, Lights out 10 p.m.  Reveille 6-15 a.m.  Wounded return letter from K. 
 Jan. 12th Usual camp routine, another wounded return [This entry continued on Page 24.] 