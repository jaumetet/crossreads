 [Page 37] 
 June 1st  N.N.W.  M.301 Drill as usual.  Afternoon writing, slept over tea time, awoke at 'Sunset'.  Study, Band, Bath, Bed 8-30.  Weather fine &amp; clear &amp; hot.  Position noon N. of Laccadive Island.  Payday. &pound;1 received. 
 June 2nd  West.  M.314 Usual drill.  Physical, Musketry, Morning  C.S.  Reading midday.  Afternoon visual T. by section Cmrs. &amp; Burial Service at 3-30 to 4-15pm.  Manning C. Coy.  Writing &amp; Study &amp; Reading B. 
 June 3rd  West.  323M. Physical Drill.  Parade Bt. Deck at 8am.  Celebration. 