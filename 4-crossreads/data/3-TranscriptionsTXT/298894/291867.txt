 [Page 5] 
 Owner H.E.Gissing Elizabeth Street Ashfield. Sydney N.S.W Australia 
 Next of kin F.W.Gissing of above address Sworn in as private in the A.A.M.C Reinforcements 17th December 1914 Chosen with 5 others on 15th January 1915 as 2nd reinforcements to the No 1. Field Ambulance A.I.F.. Sailed 11th February  Seang Choom via Thursday Island &amp; Colombo. Arrived in Cairo 24th March. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;    Lemnos 7th April &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  "    at  Dardanelles  25th April 
