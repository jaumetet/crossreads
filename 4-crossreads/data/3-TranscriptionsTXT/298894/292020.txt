 [Page 158] 
 No vow of service to abuse, No pledge to king &amp; country due; But he had something dear to lose, And he has lost it &ndash; thanks to you. 
  No grip like their bite.  properly describes the fighting of the French in the early part of the war &amp; to a lesser extent now.   26.11.15  
