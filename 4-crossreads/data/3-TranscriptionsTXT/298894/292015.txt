 [Page 153] 
 "Scoring Well" How often from the tented slopes In summer we've acclaimed The stalwart drive that topped the ropes, The wrist stroke neatly aimed! How long the bowlers trained each nerve, Before a wicket fell; While waiting wires received the news, Australia scoring well. 
 How oft in winter's day gone by, We've heard the crows acclaim, The swift low pass, the brilliant trip, The hard but sporting game. The while across the world there leapt Brief messages to tell Another triumph for all Blacks; New Zealand scoring well. 
 And now when greater games are played And greater deeds are done, Where sing the bullet &amp; the blade Beneath the Eastern Sun. More welcome to the Homeland flash The messages that tell Australians &amp; New Zealanders Continue  scoring well.  
