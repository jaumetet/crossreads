 [Page 19] 
 2nd. Sunday. July Church parade at Bttn Hqrs also Band out. 
 Football match between B &amp; C Coys. after lunch. 
 3rd. Monday. Physical &amp; Coy drill all day with lecture on Gass. 
 4th. Tuesday. Parades cut short about midday by heavy rain. 
 5th. Wednesday. Very dull weather. Foot ball match between 7 &amp; 8 platoon postponed owing to bad weather. Gass alarm. 
 6th Thursday. Physical jerks. Drill contest between Coy's of 17th. Mail in. 
 7th. Friday. Very dull weather. Parade stopped by rain. 
