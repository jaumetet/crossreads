 [Page 60] 
 far as can be seen for rise in ground. Enemy certainly has pick of position &amp; overlooks &amp; enfilades us properly. We all keep out of sight as much as possible by day. no other than Gas guards done. 
 11th. Monday &ndash; Sept &ndash;  Day rather dull but not bad. Letter from Mrs Walters day quiet easily the quietest front we have yet been on. very thankful too after Pozieres hope we stay its some home. 
 12th. Tuesday. &ndash; Sept &ndash; Dull day with showers. little more shelling today only one on the trench not casualties &amp; slight damage to parapet.fixed up later after dark.  16th month away "Ausy" 
