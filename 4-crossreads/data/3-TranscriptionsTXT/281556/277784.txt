 [Page 58] 
 11.30pm &ndash; Sept 8th 
 9th. Saturday. Guards relieved by D. Coy 10am. Poor Bfst. went for stroll with Little Sgt, round ruined city. got several snaps of Cathedral Cloth tower &amp; other ruins. Damage done is shocking large &amp; small places alike have suffered took 7 pictures in all. 
 Returned to Barracks at 11am. Got 7 letters on 8th lovely one from K. acknowledged receipt of 75 snaps. English ones &amp; my photo &amp; letters up to 11th. June. 
 expect to move into the trenches tonight. YMCA appear to be the only selling concern in Ypres. 10th. Sunday. Took over trenches 1st line 
