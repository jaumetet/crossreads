 [Page 52] 
 was snowing, then turned to sleet, later the weather cleared, but still bitterly cold.  Supposed to be here 6 days. 
 14-1-1917 Snowing, very cold.  Horses out in the open, on brick standings, shoeing horses on the line.  Sgt. Gibson posted to Bty.  He was invalided to Australia, hit in head. 
 15-1-1917 Still in rest camp, under canvas, a rotten hole.  B subsection team gone to Meault work shops for gun, guns getting repaired.  5 horses sent to Mobile, mostly necrotic sores.  Freezing all day. 
 16-1-1917 Supposed to move from here on the 19th inst.  Recieved a letter from Vance in England also one from home.  Major General Walker inspecting lines.  Major Matson A.D.V.S. inspecting horses. 
 17-1-1917 Snow 6 inches deep, not so cold.  As far as you can see is white.  This is the first 