 [Page 26] 
 It is becoming increasingly difficult to get real Australian uniforms &amp; equipment now. There is no doubt that the men in the A.I.F. leaving Australia are the best equipped troops in the war. But now everything is being made in England and the quality is nothing like the same. Our boots &amp; riding breeches are of "Tommy" pattern and the hats &amp; tunics are of poor quality. Those chaps who have real Australian stuff hang on to it like grim death. We are very proud of our distinctive uniform. The Canadians, South Africans &amp; Newfoundlanders wear the same uniforms as the "tommies" with their own badges. 
 My exchange with a Sergt. Of the 14th Field Amb. Has been approved and on Wednesday afternoon I went through the gas course with box helmet &amp; P.H. prior to going up the line. 
 We had a lecture about gas &amp; helmets, helmet drill and about 10 minutes in the tir gas itself. 
 Sunday 17th June, 1917 Said farewell to No 2 G.H. and Himmend[?] 
 