 [Page 122] 
 account from time to time 
 From the start I took the call to "give up all and follow me" as the best means of obeying the command. Why should not this be so? 
 It was not long after leaving Colombo that we crossed the line. I tried very hard to get the men to put the lights out to the right time and allow me sleep without success. I asked the Orderly Officer to do this too then I had a look for the M.O. The sunsets hereabout were beautiful to watch and were I an (officer) artist I might try and describe the intense beauty of it all. The color changed very rapidly. 
