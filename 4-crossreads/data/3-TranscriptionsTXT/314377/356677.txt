 [Page 129] 
 then the birds, who keep close to the lands and habours, is that the one is strong and graceful the other in comparison small and futile. Is there not a lesson to be learnt by us from this? 
 We approached Sydney Harbour one Saturday afternoon at 3/30. The Pilot Steamer "Captain Cook" approached and we followed her inside the Sydney Heads for our moorings for quarantine. The vessels all saluting us with calls on the ships sirens. This was continued for some time. 
 When the Health Officer drew alongside the vessel he was wearing a mask. The men resented this and bombarded the launch with potatoes and also kept talking. I didn't agree with this at 
