 [Page 57] 
 We left the shores of Dover in a mist escorted by destroyers. The sea, I take it was moderate &ndash; As on the trip over the Australians were sent below decks and were ordered to put on life belts as in case of a submarine attack 
 We disembarked from Calais and were sent from No 2 Camp for the night being issued with bread jam tea and 2 blankets making ourselves comfortable for the night. 
 Next morning setting off for the Railway Station to rejoin our Battalion I found mine just in time to participate in a further trip into the line which was to be my last experience. 
 We were encamped in under 
