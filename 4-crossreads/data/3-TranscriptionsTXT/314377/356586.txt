 [Page 38] 
 usually presented to the recipients at a special time appointed by the King at Buckingham Palace. 
 The other day I heard the Military Medal described as a duckboard. This I take it because of the fact that the ribbon supplied was similar in appearance to one of these very useful articles. 
 We did more practice work here in the way of Battn manoeuvres and they were all successful. 
 I have said before when we required water for drinking purposes it was the usual thing to obtain some from the village well. At this place we obtained 
