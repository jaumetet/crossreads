 [Page 126] 
 position shorewards that the flag was dipped as the vessel passed 
 We entered the harbour at Williamstown and proceeded to take in water. One thing to note is the young man who pumped the water wore a suit worth quite &pound;6. It would have been far better for him to have worn a cheaper and kept the better 
 Our vessel was overdue. The captain of the water boat wanted to get more water. Our captain angrily refused to wait any longer and the water boat steamed away. The captain and others aboard cooeeing to the angry chief 
 We passed the Heads at Port Phillip in the midst of an angry sea. The waves were 
