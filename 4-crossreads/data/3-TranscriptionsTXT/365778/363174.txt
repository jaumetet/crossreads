 [Page 15] 
 Had sinner with Wisdom and went to a concert in Y.M.C.A. hut at night. 
 Sunday 11/2/17 
 Church Parade, re-allotment of Officers to Coy's, sore throat &ndash; wrote to Doff &ndash; had parcel from Doff Billies cake. 
 Monday 12/2/17 
 Meeting of C.O's at Bde H.Q. had letters from Doff &amp; Nellie. 
 Tuesday 13/2/17 
 Orders received to move to SEVERN ELMS as Support Battn took over from 23rd relief finished at 8.30 pm. 
 Wednesday 14/2/17 
 Quiet day generally &ndash; had a game of cards at night &ndash; Battalion mostly on fatigues. 
