 [Page 21] 
 Saturday 10/3/17 
 Training and re-organising &ndash; another letter from Doff. 
 Sunday 11/3/17 
 Church Parade. Training move to line cancelled now going to make an attack in a few days. 
 Monday 12/3/17 
 Training and re-organising moved from Acid Drop to Becourt Camp, wrote letters to Doff. 
 Tuesday 13/3/17 
 Grevellers Line evacuated &ndash; meeting of C.O's at Bde H.Q/ 
 Wednesday 14/3/17 
 Battalion Training Platoon &amp; Coy Schemes. 
 Thursday 15/3/17 
 More Training saw The General who is looking well. 
