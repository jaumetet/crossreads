 [Page 23] 
 Tuesday 20/3/17 
 Took over line from 23rd . Hdqrs in Vaux-Vraucourt, adjusted line of ourposts. 
 Wednesday 21/3/17 
 Handed over &frac12; line to 27th Bn. Great air fight. Prince Frederick Charles of Prussia forced to land &amp; taken prisoner by us wounded. 
 Thursday 22/3/17 
 Made arrangements for attack on Lagnicourt, visited Right Battalion. 
 Friday 23/3/17 
 Attack on Lagnicourt postponed, saw the General and was sniped at in his company with 77 mm shells, feeling very tired. 
