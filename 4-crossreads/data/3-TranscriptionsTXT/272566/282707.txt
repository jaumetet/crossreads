 [Page 267] 
 Tuesday 20 November 
 Fine. Put in for leave to Paris applying for 4 days as from the 24th inst The usual 6 a.m. and 8 p.m. wires in connection with casualties I took to 14th Brigade Sigs. The A.D.M.S. run today had to be continued on to 1st Anzac Corps.  I have to do the run for 8 days then the other ambulances take their turns.  Leaving Dranoutre I passed through Bailleul &amp; Meteren and Flete and dropped the returns at D.D.M.S. Offices.  Came back by same route. 
