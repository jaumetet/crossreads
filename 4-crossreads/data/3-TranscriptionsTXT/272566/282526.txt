 [Page 86] 
 Saturday 24 March 
 Bitterly Cold Again. Aeroplane dropped a note in Bapaume asking how the Prince is.  (The one who was captured by us.)  He has since died.  Have just finished building a new fire place and chimney in this "shanty" of ours.  Rats are pretty numerous. 
