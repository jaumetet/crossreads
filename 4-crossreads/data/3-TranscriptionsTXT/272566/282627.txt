 [Page 187] 
 Sunday 5 August 
 Raining fairly heavily.  In addition to the usual paper run and the trips to Brigade &amp; Divisional Hd. Q. I had another run to Hazebrouck this afternoon passing through Blaringhem Steenbecque &amp; Morbecque on the way there &amp; returning through Morbecque, Steenbecque, St. Venant, Boesingen, Aire &amp; Wittel.  Had a look round Aire plus a first class tea.  In Morbecque I visited a few friends made during our stay there last year. Hazebrouck was shelled on August 1st with long range guns. 
