 [Page 219] 
 Notes 
 These notes are those with which I armed myself for the Court of Enquiry re  accident  Routine Orders 396 of 28th July 1917 for 3rd Army Area 10 miles per Hr through towns or villages and 25 m.p.h. elsewhere. When passing lorry parks or stationary transport reduce speed to 10 miles per hour. Routine Order 410 of 12th August. 1.066 Headlights &ndash; Lights to be extinguished if enemy aircraft are in vicinity or when held up at level crossings.  These lights may be relit about &frac12; mile from the crossing 
