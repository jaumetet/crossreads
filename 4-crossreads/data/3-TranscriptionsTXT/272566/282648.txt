 [Page 208] 
 Thursday 20 September 
 To A.D.M.S. Reininghelst twice A long run to Belgian Battery Corner near Ypres. To K. Supply Column and Anzac Hd. Q. Our Bearers left for the line today.  Things are pretty hot and traffic enormous near Belgian Battery Corner where I was today 
