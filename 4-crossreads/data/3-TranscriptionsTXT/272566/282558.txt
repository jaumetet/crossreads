 [Page 118] 
 Monday 23 April 
 Fine Day. Incinerator. "Anzac Coves" concert in Albert after tea.  I was particularly impressed by one item &ndash; a song sung by a chap in evening dress.  He was singing and pretending to be writing the subject of the song in letter form. "The Mountains of Mourn". 
