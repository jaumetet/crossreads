 [Page 113] 
 Wednesday 18 April 
 Showery and snowy.  Route march before breakfast.  24 Batteries of howitzers parked nearby pending transfer to positions in the line.  These consist mainly of 6 inch and they came down from the famous action at Vimy Ridge near Arras.  The thunder of guns continues incessantly. 
