 [Page 184] 
 Monday 30 July 
 At mid-day we moved off, but I struck trouble less than &frac12; a mile from camp &ndash; water in the petrol.  Was delayed some time but picked the others up before they reached the entraining point  arrived . Left Senlis late at night, the bike having been tied to the wheels of a G.S. Waggon on one of the trucks.  We travelled up in Cattle Trucks passing Amiens, Boulogne &amp; Calais, reaching 
 From Senlis we entrained at Aveloy Railway Siding &ndash; through Bouzincourt. 
