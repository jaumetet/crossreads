 [Page 150] 
 Sunday 27 May 
 Fine and cloudless day. Church parades in the Church Army Hut two doors away. Despatch Riding duties continue smoothly. Leave to Amiens has reopened &ndash; parties of 8 may go daily. No word yet about leaving this part of the front. 
