 [Page 62] 
 Wednesday 28 February 
 Today we witnessed a very fine sight.  Large bodies of cavalry passed on their way to the line.  The 1st Dragoon Guards, Mounted Machine Gun Corps, and Indian Cavalry were among the number, Lancers too were in the line. 
