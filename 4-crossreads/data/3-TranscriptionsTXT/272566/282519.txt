 [Page 79] 
 Saturday 17 March 
 The bombardment continued all night.  Bapaume fell at 4 a.m.  The 30th Batt. were the first Australian troops to enter the town.  The 8th Brigade &amp; the 29th Tommy Division took it.  Le Transloy has also fallen.  Concert in our camp tonight. List of 30 names read out including mine.  These have to leave for the line at 6 a.m. tomorrow. Bapaume is in flames so also are the surrounding villages. 
