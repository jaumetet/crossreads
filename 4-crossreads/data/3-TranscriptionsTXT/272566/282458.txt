 [Page 18] 
 Monday 15 January 
 We spent a very cold night.  Not moving off today.  A party of us walked to a local Mobile Workshop &amp; watched the "Tommies" preparing to move a 15" gun.  In the afternoon we went out for another walk to keep warm &amp; after tea visited an amusement hall in Mericourt which is financed by the Aust. Comforts Fund.  The performance consisted of a picture show, concert &amp; Drama combined.  "Some" Drama too. 
