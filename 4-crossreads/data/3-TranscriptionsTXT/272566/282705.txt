 [Page 265] 
 Thursday 15 November 
 Misty &ndash; Roads greasy. In the morning I had a run back to Remy Siding via Kemmel, La Clytte, Reninghelst and Poperinghe, returning by the same road. To Lindenhoek. At 8 p.m. to A.D.M.S. in Dranoutre via Lindenhoek and Daylight Corner.  Travelling without a light on greasy roads is by no means easy work Canteen opened today. Guns last night were carrying out a terrific bombardment. Amentieres can be seen from a hill close by. 
