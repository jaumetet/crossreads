 [Page 66] 
 Sunday 4 March 
 Very cold again today.  Caught for a job &ndash; cleaning out a gutter.  News to hand that Lt. Col. Williams died last night at 9 p.m.  Party to go from this unit tomorrow to be present at his funeral.   This afternoon I was burning off rubbish. Church service in the grounds of the camp at 3 p.m.  The 5th Div. Band attended. 
