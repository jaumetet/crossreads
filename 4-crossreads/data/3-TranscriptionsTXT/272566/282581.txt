 [Page 141] 
 Wednesday 16 May 
 Showery all day. Tommy Ambulance arrived to assist on this part of the front.  The Daily papers are full of talk about the Bullecourt fighting where the Australians are still repulsing all German attacks and are also smashing The German Divisions up. This is said to be the heaviest fighting of the war to date. Despatch riding. Casualties in the Field Ambulances are growing, our unit being the luckiest with about 15 (none killed), i.e. at the present front Bullecourt. 
