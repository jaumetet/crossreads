 [Page 292] 
 Tuesday 18 December 
 Roads covered with snow.  Rest of our unit arrived at about 4 a.m. Run to Desvres thence to Samer and return via Questrecques. Another run to 5th D.H.A. later in the day. Tonight I sleep on a mattrass which I have taken off the bed, there having been two on the bed previously. This mattrass plus 5 blankets, a thick "Tommy Warmer" coat and the water proof motor coat are sufficient to make a warm bed. 
