 [Page 107] 
 Thursday 12 April 
 Very cold.  During the night the snow has frozen.  Trouble about the missing horse.  It came back riderless during the morning.  Someone has apparently taken it in order to ride as far as Mericourt, and there catch the train, leaving the horse to return by itself. 
