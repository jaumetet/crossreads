 [Page 132] 
 2.2.15 
 so they were not known much by the L.H. 
 Tue 2nd Mounted parade &amp; were out all day on the desert &amp; had a few glorious gallops, we arrived back in camp at 3.30 &amp; wanted to go to Heliopolis, but had to stay in for lamp practice;  we paraded at 6.30 PM &amp; were told that they were going to have lamp practice to-morrow night instead; so I guess I will not have a chance of getting in this week.  I intend to kick against going out tomorrow night 
  Wed 3rd  Stan was in the orderly room all day &amp; Nelson &amp; I walked C Sq  