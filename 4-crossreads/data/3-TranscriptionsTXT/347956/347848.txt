 [Page 104] 
 7-1-15 
 right time one morning &amp; being late on parade   Helio work after dinner &amp; lamp practice after tea from 6 till 8.30 PM. 
 Thur 7th Last night was cold &amp; windy &amp; put in rather an uncomfortable night;  This morning the S.M. told us on parade that we had to start the signalling over from scratch again as they are introducing new rules etc.  the lads are getting full of the S.M. &amp; I am a bit disgusted with him myself. 
 It has been windy all day &amp; the dust is very nasty on the eyesight.  started a letter home &amp; volunteered to help feed up the horses at 9 PM.  The S.M. drafted us into classes to-day &amp; he has a nice idea of who 