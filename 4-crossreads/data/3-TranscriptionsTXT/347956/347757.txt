 [Page 13] 
 Oct. 1 &ndash; 8 
 Thur 1st Station work all day had trouble to get pass.  Jessie, Nowland &amp; Hayley came out.  went to Sargeant's for tea, &amp; then to pictures saw Jessie &amp; Nowland to the Sydney &amp; Hayley to Moorcliff  hospitals. 
 Fri 2nd Raining a little &amp; very little work done stayed in camp evening for a change 
 Sat 3rd Went to Randwick for the day signalling, went to Coogee &amp; Felicia evening 
 Sun 4th Signalling morning got passes from 4 PM till 9 PM.  went to Felicia &amp; to Church got back at midnight.  met Jess at Felicia 
 Mon 5th Sig. at Maroubra had dinner on the beach;  Jessie &amp; Pens came out to camp; Stan Jess &amp; I went to Griffiths &amp; saw Aunt Agnes there  was paraded before Colonel for coming home late the night before, &amp; had our passes stopped 
 Tue 6th Paraded the city with all the N.S.W. troops camp fire evening 
 Wed 7th Went to Maroubra signalling.  went to Edwards for the evening 