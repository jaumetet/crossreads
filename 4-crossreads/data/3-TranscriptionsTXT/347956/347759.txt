 [Page 15] 
 Oct 17 &ndash; 21 
 Sat 17th Raining all day.  done nothing &amp; got wet doing it, went to stadium &amp; saw fight between Kay &amp; Griffiths, Griffiths won 
 Sun 18th Raining in heavy showers.  doing nothing  Stan went to Felicia &amp;met Jess &amp; Nowland; I had no idea they would be there. 
 Mon 19th Called out early in the rain to leave camp embarked on board at 10 AM  Saw Jess &amp; Pen's for a minute or two at the gate, also saw them on the wharf for a few minutes before we left at noon,  went to midstream &amp; was anchored there  till 6 am next day  
 Tue 20th Left Sydney at 6 AM.  rather rough sea outside the heads, most of the lads seasick before we were out half an hour, was sick myself at 4 PM.  calmed down at night &amp; had a good nights sleep 
 Wed 21st A calm sea &amp; the boat running along smoothly.  the 