 [Page 111] 
 12-1-15 
 room for the Light Horse on its way from Australia  The lad who had his legs cut off by the train last night died a 6.15 this morning;  we were doing the same work as we were on yesterday principally nothing; we are to have musketry to-morrow morning 
 I believe the signallers are working to get armed with automatic pistols I believe in place of the rifle, as we have too big a load to carry &amp; only carry the rifle in cases of emergency.   doing signalling close to camp all together. 