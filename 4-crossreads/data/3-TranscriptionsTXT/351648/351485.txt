 [Page 25] 
 12.5.17 
 My dear Mrs. Jack 
 Now let me see &ndash; I don't remember quite when I wrote to you last &ndash; I know it wasn't very long ago &ndash; as we've been going some lately, anyhow I think I owe you a letter, if I don't perhaps this will bring forth an extra one from you &amp; so I shall be amply repaid. 
 Its wonderful how little this life full of movement &amp; "things doing" can produce fit for sending in a letter that must be bright, newsy, scintillating &amp; smart &ndash; what! but the routine becomes quite monotonous &amp; the little there is to write about is generally of local or merely of technical interest, for surely you don't want me to describe any 
