 [Page 21] 
 14th Field Ambulance A.I.F. France 
 23-9-16 
 My dear Mrs. John 
 I have only just got yours of June 4th sent C/o D.M.S. &amp; though I wrote you only a couple of weeks ago I intend inflicting another on you &ndash; I am quite bucked up at getting it I can tell you, its good to think one isn't quite forgotten. 
 Am awfully sorry to hear of the bad times you were having but surely by this everything is O.K. as I hear from various parts what wonderful rains have fallen &amp; you surely must have benefited by it.  So I write not in consoling mood, but anticipating finding you in good humour from the prospects of a  good  splendid season with the shadow of the "Poor House" spirited entirely away. 
 I am really awfully sorry to hear that you have been ill, am awfully glad Dr. Barrington was charming, but I think you'd make the merest mortal of a medico, in his endeavours to be &amp; appear charming to you, a far nobler creature than he really is (especially if you go falling in love with him as you say you did with Mr. F-B).  You were wise in your choice he is a fine chap &amp; an extremely able Surgeon, I like what I know &amp; have seen of him extremely. 
 Have just got back from relieving the M.O. of the Batn. who has been on leave, they are up in the trenches, Lt. Col. Humphrey Scott is in charge, he is a fine chap &amp; a very good soldier, I hope some day when doing Batallion work to get into his  lot  Batn., he has a very fine lot of officers most of whom I 
 [Comment in margin.] Again, it gives me the greatest pleasure to hear from you, and thank you most awfully for the Cardigan you said you'd send, it will be most useful, the weather is getting most decidedly cold, I don't know what it will be in winter.  Bon Sante et Bon Chance. 
 Yours very sincerely Bill 
