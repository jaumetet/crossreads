 [Page 33] 
 Fishmongers Hall Hospital, London Bridge, E.C. 4. 
 15.11.17 
 My dear Babs R., 
 I haven't written  for  to you for some time, I hope you'll forgive me, and although I have been on the point of doing so several times &amp; wanted to do so just to talk with you &amp; pour out my woes I have never brought myself up to scratch, for though I have simply longed for you to talk to &amp; have your sympathy as I feel sure I should have  done  had, its much harder to put it on paper &ndash; I know you are the sort that understands &amp; would make me feel ever so much better for the telling to you, even as you seemed to think in one of your letters that I too could understand &amp; sympathise. 
