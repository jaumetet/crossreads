 [Page 64] 
 Miss Jean. Wallace. C/- Mrs Burns. 457 Laurimarket Edinburgh 
 1900 C.E. Lee. 114th How Battery. A.I.F. 5th Div. 
 No 1995 Btn W.R. Furnis 349 High St Kirk-caldy. Scotland. 
 266199. Pte Geo Wallace. "A" Coy 7th Scotland Rifles 52nd Lowland Div. Attach 156 Bgde. H.Q. J Bennett. Block A.2. Ward 58. Kitchener Hospital Brighton 