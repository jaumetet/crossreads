 [Page 13] 
 left ship at 9AM &amp; returned MIDNIGHT.  Had good look round City &ndash; entertained by Red Cross &amp; went to MAJESTIC THEATRE at night. 
 5th Left Adelaide about 7 AM 
 6th Arrived outside heads Melbourne &amp; waited till dawn 
 7th under quarantine &ndash; during mist pulled inside about midday &ndash; restriction lifted &ndash; stopped opp Williamstown &ndash; 
 8th Pulled into New Wharf Port Melbourne about 8 AM &amp; received leave about 11 AM.  Had good look round city &amp; St Kilda &ndash; stopped in town at night 
 9th &ndash; round city &amp; St Kilda &amp; returned to ship about mid night 
 10 &ndash; left Port Melbourne about 6 AM &ndash; 
 [Transcriber's note: P.7 The French towns spelt Wimereau and Harfluer should correctly be Wimereux and Harfleur P.11 The mountains referred to are probably the Sierra Nevada P.12 The point sighted is Cape Guardafui] 
 [Transcribed by Darren Blumberg, Ros Bean for the State Library of New South Wales] 
