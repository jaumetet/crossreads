 [Page 14] 
 23rd Followed coast along it  being in sight from daybreak &ndash; Arriving &amp; entering Albany harbour about 5PM &ndash; took water &ndash; 
 24th  Weighed anchor &amp; left about 2PM &amp; followed coast for a couple of hours &amp; then out into the Bight &ndash; 
 28th &ndash; Arrived &amp; weighed anchor off Brighton outside outer harbour Adelaide under Strict quarantine 
 31  troops catch shark &ndash; dissatisfaction aboard troops riot &amp; practically take charge &ndash; ship attempt to put to sea but detained &ndash; 
  1st Feb  troops get ashore at Quarantine Station Torrents Island by tugs &ndash; passing through Outer Harbour &amp; up Torrents River &ndash; 
 2nd DITTO &ndash; 
 3rd DITTO  &amp; weighed anchor &amp; pulled into No 1 Wharf Outer Harbour at 7 PM &amp; at night dinner &amp; concert &amp; cinema  film by Red Cross. 
 4th Leave to Adelaide 
