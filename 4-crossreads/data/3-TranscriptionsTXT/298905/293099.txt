 [Page 2] 
 Diary of A.A. Barwick. 
 10th May 1916 
 This will be my third attempt to keep a daily record of our doings, &amp; I hope it will be more successful than my previous attempts one of which lasted 5 days &amp; the other 21 days, some going alright. 
 The weather the last 5 days has been very windy &amp; cold with occasional showers of sleet &amp; hail, which needless to say made us shiver &amp; pass uncomplimentary remarks about "Flanders" &amp; the war in particular. At present we are billeted at a farm &amp; I &amp; a few others are sleeping outside in an old shed without sides &amp; the roof is like a sieve thank's to German shrapnel, we have 
