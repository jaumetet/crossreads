 Administrative Headquarters A.I.F. 130 Horseferry Road London  S.W. 
 30th October 1916 
 Lieutenant K. Ferguson 
 Australian Training Camp Rollestone, Salisbury Plain. 
 My dear Ferguson 
 I congratulate you on your letter of the 28th instant, it shows exactly the right spirit. I shall see how matters stand about your going to the front, but remember this, that if you are kept for training purposes, it will only be because you are suited better than other people would, &amp; it is an exceedingly important job. I fear in the past we have rather suffered sometimes for want of the right men doing this work, which we are now realising is of such great importance. Do not, therefore, chafe if you sind that you are kept behind; there will be good reason for it, &amp; it will have nothing to do with the personal influence to which you refer. 
 With best wishes, 
 Truly yours, 
 R. McC. A. 