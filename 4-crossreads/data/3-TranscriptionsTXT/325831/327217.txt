 4. small excitement. 
 Another reason for our delay was the alleged activity of the Hun Untersee Boats German [U Boats &ndash; Unterseeboots] in the region of the Canaries. 
 It was stated that quite a number of Transports were kept at Durban &amp; Cape Town, pending the provision of an Escort. But no one knows the real reason, &amp; Rumour was ever a lying jade. 
 Durban is a pretty place &ndash; &amp; I went for several train drives round about. The trams are Municipal, &amp; soldiers were allowed free &ndash; a grateful concession, which was much appreciated. I saw the Zoo, Museum &amp; Art Gallery. The latter is nothing like up to the Sydney one. 
 The Esplanade is well laid out, with a fine Swimming Bath, &amp; wired in 