 Yaralla Chambers 109 Pitt Street, Sydney 22nd February, 1917 
 His Honor Mr Justice Ferguson Sydney 
 Dear Mt Justice Ferguson 
 You said one evening when I had the pleasure of meeting you at the Hospital at which our mutual friend Blunden was lying up that you would like to see any letters that came to me from Brigadier-General, now Major-General Holmes &amp; as I have received a letter from him within the last few days I am sending you a copy of it. I think that the letter is of a rather personal &amp; private character to myself, but some parts of it might feel interest in reading. 
 Yours faithfully, 
 H.S. Williams 