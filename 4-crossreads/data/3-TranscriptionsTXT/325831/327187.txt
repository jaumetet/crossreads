 5th T. B. Rollestone 26.10.16 
 Col Anderson 
 Dear Sir 
 While thanking you most sincerely for your kindness in forwarding this correspondence &amp; giving me a chance to decide for myself, I think that for me to accept the course which is offered would hardly be playing the game. It seems to me that my duty lies in the firing line, &amp; I am sure that Mother &amp; Father are of the same opinion. 
 You asked me to remind you to obtain a photo of my brother's grave. His full name was Arthur Gard&egrave;re Ferguson, &amp; he was a captain in the 20th Battalion. 
 Thank you again for your kindness. 
 Yours respectfully 
 K. Ferguson 