 [Page 42] 
 here we saw thousands of Tommys &ndash; Indian &ndash; Australians &ndash; Horse &ndash; Camels &ndash; Mud Houses &ndash; Asses etc &ndash; At 8 pm we just fell out at Zeitoun &amp; after getting all L.H.F.A. Baggage etc, we left the L. Horse for our own Camp &ndash; which we reached after 2 miles walk &ndash; were issued &amp; Blankets each &amp; camped in a [indecipherable] Hut, it was that cold all night I couldn't sleep &ndash; I used to think I knew just a little bit, but am quite resigned to admit as a Queenslander that I didn't know I was alive. 
 It took my breath away at the magnitude of the Military Camps &ndash; from one end of the Canal to the other to just Camps &amp; soldiers &ndash; Quite a common thing now for me to see Aeroplanes. 
 Wednesday 8/3/16 
 A nice cool day &ndash; cold at night. 
 Had a day off &ndash; Disappeared with the Boys to Heliopolis &ndash; saw most of the sights &ndash; Visited Luna Park which is an amusement Place 
