 [Page 17] 
 Saturday 12/2/16 
 the military term (Spring to it) was not in it. There were few that was not on the alert &amp; thought of submarines etc. I'll admit I got a start myself, &amp; it woke up all the patients. Have often  heard of cutting the air with a knife, one can pretty well do this by going down the hold of a troop ship of any night, it nearly blocks your approach. Very monotonous on the water, there being no sign of land. Tucker still keeping very good &ndash; Feeling tip top but can't sleep too well during day. 
 Sunday 13/2/16 
 Very hot &ndash; sea very calm. Very few patients in Hospital. About 150 men call at Dispensary twice daily for medicine &amp; dressings.  Two services held, great chanting &amp; singing of Hymns &ndash; finished up with a huge bun fight at night. The boys had as much cake etc as they could eat.  Thousands  of flying 
