 [Page 168] 
 Sent by Mother 
 August 1916 Once in the days of long ago, Days &ndash; of my whole life the best &ndash; When the time for sleep had come, And the house was hushed to rest, It was such a happy thought, Used to make my heart so light, We were all beneath one roof When I barred the door at night. 
 Let the wind moan as it would, Let the rain-drops patter fast, They were near me, nestled warm From the midnight, &amp; the blast; Not one lingering out of reach, Not one banished far aloof - It's a woman's heaven to have All she loves beneath one roof. 
 Now, when bedtime comes at length To me, sitting here alone; And the ticking of the clock Tells how still the house has grown, Oh, how heavy is the heart That was once so light of yore; Now &ndash; I seem to bar them out When at night I bar the door. 
 [Transcribed by Alison O'Sullivan for the State Library of New South Wales] 
 