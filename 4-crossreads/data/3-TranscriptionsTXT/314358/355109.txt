 [Page 77] 
 13 This is true, for I have heard them, and I have seen their solemn faces and noted the expressions in their eyes, which cannot be described, except I like it to the trouble of a fearful thought. Had Germany known! Brush away the memory it is unthinkable. 
 The first things I picked up in Belgium were some acorns beneath a fine old oak. I have them yet hoping to send them to you to plant in Australia. 
 At last the day came when I was sent forward to join up with the 7th Field Coy I then merely walked into the ruined city of which I have hinted 
 How I did wish to have time to note details of this 