 [Page 155] 
 14 Waggons, and what not; to keep those unending supply columns moving forever along the rutted shell battered miles of communication lines. 
 Many are reinforcements going into the wide field of action to replace noble old animals who have done their duty and passed west with mans cursed judgement upon them that they have no souls. 
 They have done their duty without complaint for man; and for this, as far as i am concerned, they shall, in the narrow sphere of my own thoughts, have souls for remembrance 
 In the calm existence of peace the poor old horse has almost lost his noble place with man. 
 But now it is war, he too must fight and take a risk with man as a companion, in the same old noble way. 
 As well as horses there are mules here also 