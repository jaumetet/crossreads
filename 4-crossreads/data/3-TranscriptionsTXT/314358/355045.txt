 [Page 13] 
 5 In orders I find this will be the last mail from here to be in time for Xmas. Often have I thought of writing a book "My Xmas Days" Mine have been spent in strange concerns under various conditions "Books which never were written" will be perhaps my only book and that an empty cover. 
 Don't lose my old writings for I want to rewrite everything which is good when I come back dear 
 Xmas greetings to you all my wife 
 After next pay I shall send some Egyptian things to you all 
 At present my love is all that I can send before Xmas. 
 I am in the land where the first Xmas began The sunset tonight was a Bible picture. 
 There is so much to speak of. To me this place is still the edge of a vast dream 