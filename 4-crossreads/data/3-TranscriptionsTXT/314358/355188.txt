 [Page 156] 
 15 Many mules Strange useful animals these 
 They seem to be without conscience, without sentiment or feeling without fear 
 Just mechanical things awarded with the miracle of life 
 They dont seem to care whether they eat or sleep, work or rest, live or die 
 They are like the uncared for discarded children of a false and nourished love, who have never known sympathy and seek it not. 