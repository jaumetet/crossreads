 [Page 167] 
 26 It, and yet was hollow and unsatisfied as a leviathan feeding on sprats. 
 It is now the 27 June and we have crossed the English Channel. 
 Are now almost in the harbour of Havre 
 It is a gorgeous sunny day, with picture clouds 
 Silver-green seas 
 Faint pearl haze 
 We can see the cliffs and their colors, and the grass and trees of the land. 
 Buildings are quite plain. Windows and doors appear like square dots, if you can imagine such shapes. 
 Everything is nearer every minute and planier 
 Love to you dear wife and to our children And to you dear mother Am in splendid spirits and well.  All is well and will remain well with me.  God Bless you all 
 Geoff 