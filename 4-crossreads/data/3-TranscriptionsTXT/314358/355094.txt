 [Page 62] 
 4 those old world people somewhat rare in these days 
 She maintains her splendid property for simple old sentimental reasons quite regardless of commercial gain or loss, and the simple old lady thinks she is poor in spite of the fact that her servants number up to quite a dozen 
 Her house is considered to be the best remaining example of moated mansion in England and is historically famous on account of hacing sheltered royalty during the troublesome times of bye gone ages For this reason and also on account of its picturesque appearance the subject of many a notable artists canvas, and pictures of it have been hung in the Royal Academy twice 
 Saints aunt knows most of the worth knowing in England she entertains of her place just those whom one thinks he would like to meet 
 For instance Saint tells me his aunt would introduce me to the president of the Royal Academy or perhaps the editor 