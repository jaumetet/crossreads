 [Page 141] 
 Ordered to be brought, not a single one above that number even should more be taken (The suggestion is clearly understood by all listeners to the lecture) 
 The officer continuing describes how he knew of a New Zealand railway party being told to bring back 10 men 
 They took a number of prisoners in a raid but in the uncertain light of early dawn they miscounted them 
 On a count being made when they returned 11 was found to be the exact tally. To fulfill the order they killed one 
 This is a fact 
 The lecturer goes on to strongly advise this course for it is difficult to keep an excessive number of prisoners 
 Our own wounded are to be brought in 
 A half glance over my left shoulder permist me a view of our piled rifles on the brow of a grassy slope.  Beyond is the sky-line &ndash; a distant rise of the downs all is grass 
 A plan in coloured chalk on an oil sheet hangs on the hedge 
 Now and then I catch the lecturers words and meaning, but for the most part I am intent with thoughts quite away from military matters 
 The beauty of natures simple leaves and grasses, the sterling sunlight, the clear sky, the snowy clouds floating on their own pale shade or silver-grey.  All this is never sordid, never gross. 
 A battalion band can be heard far 