 [Page 125] 
 4 Soon through the [indecipherable] arch of a viaduct which carries the permanent way, and our on the other side of its long embankment. 
 Now the farm and the mill are shut off from our view 
 Still we are in a meadow flat, where there is some marsh land 
 There are rushed here, but the heels of winter have cast them of their green. 
 A [plover?] startles upward with a plaintive call, flys a swift doubtful course and settles afar off. 
 The road of flint winds on 
 Soon there is gorse in scattered tangles, and soon there are masses of tangles of gorse. 
 Then there are hedges; and gates and little square meadows paled in with hedges. 
 These meadows are fields, but there are meadows for all that. 
 So small are some of them that if a giant had spread his smallest handkerchief of green it would be much the same. 
 The road winds on, but on rising ground and soon there 