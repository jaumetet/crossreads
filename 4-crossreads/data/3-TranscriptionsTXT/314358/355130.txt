 [Page 98] 
 9 As usual the rear portion of the column slowly understands 
 The whole line is crushed up and close packed. 
 Mules raise their heads to avoid baggage in front of them. 
 Lumbers and houses become a jumble. 
 Then cramp up and wedge together to give the transports room and to avoid danger from restless animals 
 The officer leading is doubtful of the way 
 Who knows? 
 "Where do you want to get to" This query comes from us "Road leading to the Lylle gate, comes the answer through darkness "You are almost on it. Turn to the left and you're in it" "Good &ndash; Thanks lad" comes answer through the darkness again 
 The column moves once more, turns and slowly assumes its easy links of space ridding itself of cramped wrinkles 