 [Page 228] 
 Somewhere in France 1st July 1917 Dear Bee &amp;Our children 
 This daddy is off to the front any day now, but dont worry for all will be well 
 We hope for a fine victory. 
 And wish for Peace 
 Your loving Geoff and Dad 