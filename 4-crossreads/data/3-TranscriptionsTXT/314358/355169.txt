 [Page 137] 
 Be this done or let there be that done, it was so 
 Did Haig ever command a miracle? Yes! 
 That's where the great soldier comes in.  For only the great soldier has faith.  Faith in his army; faith so abundant that it naturally demands the supreme effort 
 More than once has Haig said let there be such a thing done or such another thing, and it was so 
 Men and mules and horses have heard and have marvelled at the word of Haig 
 Marvelled because that word has seemed to them their death sentence 
 But they had ceased to marvel in their brave toiling to perform it. 
 Man with his nerves like quick-silver, his brain like fire, his finger tips iced with the touch of Death had gone forth in the drab of mind and with glintless buttons to do the will of Haig; the will of himself; the will of his country of righteousness 
 And sometimes though fearing his heart has been joyous in the intense moment of action 