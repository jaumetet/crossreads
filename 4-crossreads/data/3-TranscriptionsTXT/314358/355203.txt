 [Page 171] 
 4 That which will happen even if it he that which one least cares to meet, that which he most dreads, will not be lessened nor diverted by fear 
 I have referred to moments under shell fire, but what I have written cannot be taken as general to human nature, nor even as standing truth regarding myself, for my experience has been exceedingly small, limited and brief, when compared to experiences of others. 
 So much so indeed that I dare not assume on first impressions a staple mental quality for similar future moments. 
 In fact from what I have seen and learnt of shell and shrapnel fire a time comes to almost every one 