 [Page 26] 
 10 It is impossible to give a correct impression of this camp in such hurried moment as I have 
 Officers and men are all alert and watchful, little things are not our concern at present 
 It may not happen of course but we are on the eve of important doings 
 All is well with us and will be well I can assure you. 
 Transport is busy throughout the camps. 
 Affairs are such that anything may happen. 
 Now my darling I have no time to write to everyone as I would like to but you can let Mother and the other see this indifferent letter 
 I could write letters if there was more freedom to do so but you must just accept these poor attempts as coming from one who has quite heaps of work to get through 
 Let Mother know I am getting more rest now except at [indecipherable] 
 Health is good, I am hardy as a [indecipherable] 
 