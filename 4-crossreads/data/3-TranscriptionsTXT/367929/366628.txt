 [Page 72] 
 May 1915 Wore our equipment. Few Australians and NZ all past recognition and removal. 
 25 Tuesday Down to Anzac Cove with Clark for the swim I have daily. Had b 
 reakfast after coming out, on the beach. HMS Triumph blown up by a torpedo off the shore. 
 26 Wed Oceassional stands to arms. Swim under bullet fire. Had 1 killed and 2 wounded by shots down Monash Valley. 