 [Page 76] 
 June 1915 12 Sat In valley bivouac. Penetrated gully up hill found a night outpost of the Turks &ndash; brought back some equipment curios. 
 13 Sun In valley bivouac. Padre Merrington and also padre Green held communion services opposite Regt Hdqu dugout. Latter was the 1st  had seen here &ndash; about 43 communicants. 