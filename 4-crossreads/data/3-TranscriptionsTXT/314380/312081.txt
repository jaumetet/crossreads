 [Page 6] 
 Mr J. Green 260    Lib  Liverpool Darlinghurst 
 Pte J Green No 2658 6th Reinf 17 Batt Liverpool Camp Sydney 
 Enlisted 14th Aug 8.15 age 28 yrs. Training was brief for one thing, not suffice rifles, and equipment, on guard to day rifle's sup being without bolt and bayonet being held on by a bit of string to day Oct 3rd being supld with pick and shovels marched to trenches, outside of camp   &amp; instructed by Eng Lieut in the art of trench warfare, other training consisted of rifle practice &amp; ceramonial drill, just having the rough edges taken off us so to speak 