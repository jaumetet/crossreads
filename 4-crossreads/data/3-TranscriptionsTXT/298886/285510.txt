 [Page 161] 
 10th  April 1915. May [referring again to Gallipoli, the year before] 
 Our first night on the SS Devanha and outward bound for Gallipoli We had very little news as to the true state of affairs then existing. 
 11th  April May. 1916. Thursday. Sohag. The weather has been somewhat lenient today no dust blowing about and the temperature only 106&deg; in the tent so we are let off lucky. We leave for KANTARA in the Canal Zone on Tuesday 14th getting there next day. So the orders say. 
 11th  April  May 191[indecipherable] 1915. 
 All day sailing through groups of Islands in the Aegean Sea which lie off Gallipoli. About 2pm the Dardenelles in Sight faintly discernable in the distance and as we draw closer small black dots can be seen which are the warships of the Allied Fleet. Coming closer again one can faintly hear the larger guns exploding and faintly see the shells bursting on land. After about an hours steam we drop anchor close to a couple of formidable looking battleships which are pouring in broadsides into the 
