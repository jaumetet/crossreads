 [Page 151] 
 on. Rumour says that we are bound for the Canal Zone in a day or two perhaps less. Things happen with much suddenness these times 
 I have given my saddle a much needed clean and made it in good condition for a possible move. I don't think the next Camp can be much worse than this one At least I hope not. O'Halloran one of my Signallers came back with the last batch of reinforcements and he takes Jones's place, who by the, has been granted a transfer to Colonel Glasgow's Brigade at Tel.El.Kebir. as a Signaller 
 7 bags of parcels last night to distribute and 2 more bags of mail tonight 
 - 3rd May  Tuesday Wednesday 1916.Sohag. 
 Turned out for an inspection this afternoon in full marching order preparatory to leaving the Camp Heavy dust storm this evening. but rather cool. 
 4th May Wednesday  Thursday. 1916. 
 Same routine as usual. everything points towards a departure from this dust hole and the sooner the better. Although it will be 
