 [Page 245] 
 has been very high. today. Gibbs of A Squadron seriously wounded up to now. The artillery (RHA) have just gone out with four guns to pound "Jacko" 
 6.45 pm. Our Guns opened fire on Oghratina &amp; cannot see with what effect. They sound very familiar. The latest order is that the Brigade moves at dusk and its nearly that now. Enemy's Taube came over. 
 12 midnight. Same date. Home again! 9 oclock before we started from QATIA. So ends one of the longest days we have had.12 midnight when we got home. 
 22. July Romani 
 Out of the blankets at 2.30 A.M. and Stood to arms until 5. Men &amp; horses dead tired Enemy's Taube came over about 7.30 we turned out and scattered as usual. Our Bristol Bi plane which now has a landing near here came out and gave chase by the other fellow flew away as fast as he could for home. The anti-air-craft gun fired several shots after him to help him on his way. 
 9 A.M. Just received the very pleasing intelligence that we are going out again to night! Oh Hell!! 
 This sort of thing cannot go on 
