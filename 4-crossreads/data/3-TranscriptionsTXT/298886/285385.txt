 [Page 36] 
 nomenclature. The old name would have been handy for writing on returned mail. The wind has been blowing very cold all day and now to-night it has become much colder. Heaps of drift sand are banked up against the tents &amp; huts. No wonder cities were buried in this old country. I should imagine that it would not take many centuries to completely bury a city with these sand storms. A memo from Regimental office last night wanted to know how many of the original regiment were left a complete list had to be forwarded by 8.AM.this morning. This has given rise to some extraordinary rumours. Plenty firmly believe we are going back to Australia for a spell Personally I don't care which way it goes 
 Rumour No1 today a good on I thought. 
 No decoration for Gallipoli! What price a row in Australia if one were not given? Rumour No2. Also as good as the last one. 
 They are taking the names of all the old hands to arm us with swords! 
 What price some of the poor horse's ears? 
 A game  was  of football was played today but the going was extremely heavy in the soft. sand. 
