 [Page 204] 
 does that portend? 
 6th June TUES. 1916   Romani. 
 Much the Same routine as Yesterday  Lent to 1st Signal Troop one Heliograph and stand. 5 new Heliographs and 5 stands came to day. 
 - 7th June  Wed. 1916 Romani &ndash; 
 Nothing exciting happened today Returned two telephones and all spare wire to Brigade Signal Troop  Only kept two phones in hand for night patrol work. 
 8th June Thurs. 1916 Romani 
 Rather hot today though Sea breezes very acceptable. Training Supernumeries. Return of men who have been inoculated against para typhoid. Signallers for Night outpost 2 Extra horses came from C Squadron to complete my establishment. Telegram received this morning States "Lord Kitchener and all his staff went down off the Orkney Island on board the Cruiser "Hampshire" None were saved". Latest rumour. The Germans have succeeded in taking Verdun and also that the German High Seas Fleet heavily reinforced by Zeppelins came out again and to use a Colonializm "Towelled the British fleet up"!  Another rumour we heard was that the Turks are making. 
