 Dec 21 st  
 a cold night spent on Kantard Station which we left @ 6 30 am in a train [made?] up of all sorts of army stores &amp; [riggers?]&nbsp; 
 Almost immediately after leaving Kantard / which by the way is an important sailhead being used as a launching depot for ships coming up by way of the Suez Canal ) the line runs through dismal desert covered with a sparse shrub. The outlook however across the desert [if scenic? if scenery?] was --- remarkably beautiful on account of the fine sunrise &amp; although the night was extremely cold it began [to] warm up almost immediately with the rising sun.&nbsp; 
 The road to ElArish is monotonous [&amp;] uninteresting being practically&nbsp; [paractically?] desert the entire way The 
