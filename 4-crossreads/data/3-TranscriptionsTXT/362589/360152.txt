 [Page 20] 
 INFORMATION BUREAU. AUSTRALIAN RED CROSS SOCIETY N.S.W. DIVISION 
 WOODSTOCK CHAMBERS, 88 PITT STREET, SYDNEY. 
 23rd September, 1915. 
  Lieutenant A. G. Ferguson, 20th Battalion.  
 My dear Judge, 
 We have just received the following cable from the Ghezireh Hospital :- "Ferguson, convalescent, walking. A.W. Campbell, Major". 
 Yours sincerely [indecipherable] 
 