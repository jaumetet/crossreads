 [Page 5] 
 At the request of the Director of the Australian National War Memorial dated 17th June 1930, a copy of the following has been forwarded to Melbourne and will be deposited in the Australian War Memorial at Canberra, which is at present being erected. 
 Copy. 
 Australian War Memorial, Exhibition Buildings, Melbourne. 
 February 11th 1931 
 V.G. Schwinghammer Esq., South Grafton, N.S.W. 
 Dear Sir, 
 I have to thank you for your letter of 1st inst, and to acknowledge receipt of the copy of the narrative of your experiences which you compiled from your war diaries and which at our request you have so kindly presented for preservation in the Australian War Memorial. 
 This document is of much interest and great value as an historical record and will make a welcome addition to the National collection. 
 Those responsible for the Australian War Memorial greatly appreciate your action in responding so generously to the appeal for the records of your war service. 
 Yours faithfully,, (Signed) J.L. Treloar, Director. 
