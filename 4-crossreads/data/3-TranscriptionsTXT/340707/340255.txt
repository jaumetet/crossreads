 [Page 140] 
 129 
 Chateau de 17 Rue Campagne 
 [A sketched floor plan of chateau] 
 1. Bookcase 2. Marble topped lavabo &ndash; disguised as a [indecipherable] &amp; cupboard for clothes 3. Fireplace &amp; case of books &amp; seat in front 4. Divan &amp; cushions 5. Heavy table for work 6. Light table for refreshments 7. Bed &ndash; above shelf 8. Stove place &ndash; shelf above to left &amp; cupboard iron doors below 9. Two boxes of Marie Le Breton serving as table 10. Washstand &ndash; Portmanteau in other room 
