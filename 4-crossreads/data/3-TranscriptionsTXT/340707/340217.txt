 [Page 102] 
 87 of Convention &amp; the scene of a dinner party to General Bonaparte. The frescoes are excellent though some are showing signs of wear The altar &amp; pulpit are best described by reference to pc which I have forwarded. There is a meridan line in this church but I forgot to notice it. 
 After this I returned to Rue Delambre a pied The day was rather too much for me 
 9-8-19 Repose all day headache &ndash; fever, glanular swellings &ndash; 
 10-8-19 Toss about all day. Brise makes many trips per day &amp; suggest that I rest at her home where I can be looked after. It will save her an hours walk 2 or 3 times per day &amp; she will call in her doctor. If I am no better I have decided to this 
 Tonight we went to Odeon one of the 4 theatres subsided by Govt entirely (Opera, Comedie Francaise &amp; Palais Royal being the other). Drama was L'Arlesienne. The music by Bezet was excellent. Orchestra had 14 violins alone The acting was good but at times a little tedious. It needed a Pauline Frederic to play one part &amp; she was not in cast. 
 After theatre as I am still ill, I take taxi home to her place &amp; put in a miserable night. Brise pleading to [indecipherable] for me 
 11-8-19 Dr came to day &amp; so expect relief tonight All the day is spend in bed. I have a tonic to take &amp; poultices &amp; hot compress 
