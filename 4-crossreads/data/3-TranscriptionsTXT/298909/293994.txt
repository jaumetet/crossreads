 [Page 69] 
 to visit Ismailia &amp; had a very pleasant evening in the shade of the gardens, quite a treat to be able to get a nice cold drink etc. 
 Bombs dropped on Serapeum Camp by enemy Aeroplanes &ndash; several West Indians injured. 
 15/6/16  Thursday 
 Very hot &ndash; about  110  - Had Kit inspection &amp; drill etc with pack up.  8th Brigade moved out from Sundown till midnight &ndash; about 5 train loads &ndash; Last night the Tommys who are replacing us &ndash; gave us Australians a tip top farewell open air Concert, we all appreciated the great efforts they made to entertain us. 
 16/6/16  Friday 
 Very Hot &ndash; Getting things ready to move out, clearing out most of the sick &amp; wounded from our hospital Tents, have sent in a lot to Cairo Hospitals &ndash; More of the Division moved out. 
 17/6/16  Saturday 
 Very hot  115  - Bivouacking in the heat is cruel. Last night I never saw such heavy dew, my Blanket was ringing wet when I awoke, also my head &amp; face. Troops moving out again after sundown. 