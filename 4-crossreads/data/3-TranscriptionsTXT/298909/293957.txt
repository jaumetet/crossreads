 [Page 32] 
 16/4/16  Sunday 
 Sergt McDonald promoted to S.M. very popular move. From an A.M.C. mans point of view this camp from a Sanitary point of view is disgraceful &amp; reflects greatly on the Heads &amp; more especially the Medical Men. 
 The authoritys do not hesitate to inoculate us against all epidemics etc &amp; yet they cater for disease by ignoring sanitary arrangements &amp; endeavouring to keep down  to keep down  the cursed flies. It is all the more serious since the Medical Men think that every fellow that parades sick is a malingerer &ndash; they are now putting into to force this fact &ndash; Fit A Class, be ready to move off to the front. Class B fit to be Classified. Class &ndash; C unfit &ndash; go back to Australia: means Fit or Unfit. 
 17/4/16  Monday 
 Very Hot &ndash; Corporal of a Picquet all day. Went over to 14/2nd L.H. &amp; paraded myself to Lt. Tricket, stated my self  case to him 