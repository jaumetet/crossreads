 [Page 212] 
 Dec. 18 Saturday 1915 Got up &amp; went to Mena House as a convoy was expected with a lot of wounded. Commenced to evacuate Anzac. The men were taken off in small launches at night. All the mules that had done such good work for us were shot &amp; much of the stores &amp; other things were blown up before the men left 
 Dec. 19 Sunday 1915 In bed all day 6th Regiment L Horse left Anzac for Alexandra. Lieu Col Fuller was last to leave the trenches with 12 men 
 Dec. 20 Monday 1915 In bed all day Our chaps did not like leaving the "Dead"comrades behind them. We all feel the evacuation was necessary, but it is hard to have to crawl back, after hanging on so long. 
