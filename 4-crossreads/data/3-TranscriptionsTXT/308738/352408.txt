 [Page 50] 
 March 13 Saturday 1915 Mounted Parade in morning Went into Cairo for Major Fuller conveying an important letter to the Australian &amp; N Zealand HeadQ Shepherds Hotel. Colonel Cox &amp; party of  Qld Lancers left this evening to take Dinner at the Continental Hotel 7. 45 
 March 14 Sunday 1915 Colonel Cox major Fuller &amp; party went out for the day returned 6. 30 Church parade this morning I rode into Cairo with Haigh today Measles broken out in camp. 
 March 15 Monday 1915 Mounted Parade in morning Major Fuller went into Cairo in connection with Court Martial Case. Received 2 letters from M. one dated 24th Jan &amp; other dated 15th Feb. Posted letters P. 6 to M &amp; mother. 
