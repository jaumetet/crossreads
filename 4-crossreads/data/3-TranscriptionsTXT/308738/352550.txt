 [Page 192] 
 Nov. 13 Saturday 1915 Work early this morning. Not feeling too well. Took washing around went with Dick Pladell for long walk to see Sick &amp; Wounded being taken off Hospital Ship. 
 Six charges against Cpl Grice (1) Disposing of Gov property (2) Pilfering (3) Attempting to Bribe Sorters (4) Unlawfully removing Postal Matter (5) Selling papers (6) Concealing Veneral Disease. 
 Nov. 14 Sunday 1915 Received orders to go to Mustapha tomorrow. Went to work from 6 AM to 8 AM then to church, this afternoon I went round to Hosp to see Lieu Drummond. He is leaving soon for England. To Church tonight, met Sergt Pro Police. 
 Nov. 15 Monday 1915 Went to work early. After breakfast went with 12 others in motor car to Mustapha as witnesses in connection with Cpl Grices case. There is six charges against him. Case was adjourned till 3 PM. Again adjourned till next day 10 AM. Lieu Cunningham was the 1st witness. Grice has a lawyer friend to assist him. 
