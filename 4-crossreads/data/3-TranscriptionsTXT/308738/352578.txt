 [Page 220] 
   List of articles to be worn by men, or hung on saddle  
 Horse, Saddle, &amp; bridle, Head rope, Heel Rope, Heel peg, bucket, nose bag, spare horse shoes, wire clippers, mess tin, Overcoat, Blanket, gun, bandoler, belt, bayonet, water bottle, haversack, 
 Abdomal Belt,Boots, brases, Breeches, cap, drawers, disc, jacket, jersey, clasp knife, legging, spurs, flannel shirt, singlet, socks. 
 Remainder of articles belonging  to men to be carried in Kit Bag which is taken to nearest Base. 
 All suplus luggage to be kept at Abbassia. 
   In Haversack  Shaving brush, tooth, comb, Fork, housewife, holdall, knife, 1 pair spare laces, razor, soap, spoon, towel. 
   In Great Coat  comforter, spare socks, jersey, 
 Oil sheets, &amp; horse rugs go on transports. 
 [A list of prices of odd items headed "Colombo"not transcribed. Also a cure for German measles not transcribed] 
 