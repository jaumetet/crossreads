 [Page 22] 
 Jan. 23 Saturday 1915 Sighted Land at 7 AM today   dropped anchor 9 am outside Aden  Small town principally Lascars Large Garrison of Artillery, &amp; Infantry stationed here. Small boats around selling tobacco, matches, etc, very expensive. Horse died making 17 to date. Very rugged scenery around here fine sight to see all the Fleet anchored here, Have picked up the Ceranic", Persic, &amp; Ayrshire. S. M. Gordon was sent over to flagship for letters &amp; orders.   Weighed anchor 5 PM  
 C. Whitlock J. W. Weekly N C. Ridley  E G Jones, C. R Weekly put off at Aden medically unfit.  Syphilisss. 
 Jan. 24 Sunday 1915 sighted the Arabian Coast at sun rise on the right also the African Coast on left. Cool breeze Blowing Sea choppy 
 (Fleet stopped at noon to bury man from off "Ceramic") 
 passed Danish Motor Boat going east. Kit inspection. Passed by Islands called 12 Apostiles 3 PM doing 12&frac12; knots per hour 
 Jan. 25 Monday 1915 Another beautiful day Wireless message received 3. 30 PM Naval battle Yesterday in North Sea German Dreadnought "Blutcher"sink 2 others seriously damaged. doing 10 knots per hour. Large Oil Tank Steamer passed by today. Pecular construction. no Land in Sight since early morning 
