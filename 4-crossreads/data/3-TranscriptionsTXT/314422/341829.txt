 [Page 14] 
 Some American officers of high rank have been inspecting this front of late and probably we will be removed to some other sector. 
 Tuesday Jan 8. 1918. 1254 
 Very cold. About 8.am. it started to snow and at 9.am about 2 inches had fallen. Snow fell at intervals during the day. Colonel Knox Knight inspected the Transport this morning, accompanied by Mr Hunter and Mr Hazeldine the assistant adjt. 
 Wednesday Jan 9. 1918. 1255 
 Cold &amp; frost but clear. We took all the horses &amp; mules out today for exercise. L/C A Borthwick, Pte 
