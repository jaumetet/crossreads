 [Page 110] 
 the education scheme that is going to fill in that period. 
 The wood facing our dug out, but on the opposite side of this valley, is Carpet Copse, &amp; with the glasses today I had a good view of the enemy wire round it. To the right of this wood is another, beyond which the plane came down last night, &amp; the name of this  wood  is Hervilly Wood,  named  after our village. 
