 [Page 36] 
 us: &ndash; apparently between us &amp; our last camp. 
 The stunt was due to start at 5 the following &ndash; Friday &ndash; morning, &amp; just about that time a terrific barrage opened out &amp; lasted for about an hour. 
 Our tent Sub had breakfast &amp; went on duty at 8 oclock, about which time the first car loads of wounded began to arrive. To make things rather more interesting, we had a blood transfusion section, with a surgeon major, for attending to bad cases &amp; operating; these occupied the third hut. 
 About five of A &amp; B sectors combined did dressings, &amp; the rest of us loaded &amp; unloaded the cars as they arrived one after the other in quick succession. For five hours we worked solidly without a break, dust collecting on our faces, &amp; sweat pouring down, for it was a hot day. Still, one  found  felt that good work was being 
