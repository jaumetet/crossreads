 [Page 3] 
 18th Sept. 1916 Signalling tests.  Cold day.  Return of Capt. Campbell to our Battery.  Lamp test.  Football match between 27th &amp; 25th.  Win for 27th, 6 to Nil. 
 19th Sept. 1916 Very cold day with winds.  Brigade of heavy howitzers firing here today.  Lectures &amp; buzzer.  Did not go out anywhere. 
 20th Sept. 1916 Cold day.  Got C.B. [Confined to barracks] 2 days today for coming late on parade.  Hard luck.  Signalling &amp; reading.  Received letters from Florrie &amp; Millie. 
 21st Sept. 1916 Signalling &amp; out on Helio.  Finished up the C.B. tonight.  Wrote letters. 
 22nd Sept. 1916 Signalling &amp; telephones.  Received letter from Wyn also snaps of the Sunday stunt at Winchester.  Did not go out but fed horses which arrived today. 