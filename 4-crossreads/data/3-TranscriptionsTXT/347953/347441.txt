 [Page 100] 
 of all the fatigues especially as our bodies were weak from exhaustion  &amp; insufficient food.  Nevertheless the battlefield is remarkable for the tremendous amount of trenches and their wonderful skilful execution and will always prove of interest to those who may be fortunate to visit this historic battlefield when peace once more shows its glorious &amp; shining light. 
 [Sketch of Rising Sun badge and battalion colour patch] 
 