 [Page 119] 
 with comforts from friends who always gave so liberally to the Australian funds.  The Medical officers &amp; Sisters were always obliging and carried out their duties most efficiently and with the best of good heart. 
 Nearly every Australian Soldier has at some time or other passed through the hospitals as it will be seen that at least 200,000 men have passed through their hands.  This is remarkable work, but the overtaxed hospitals were always under competent hands, so the comings &amp; goings of the men were always arranged efficiently 
