 [Page 60] 
 were all visited by me many pleasant hours were spent at these various places.  London was full of soldiers especially Anzacs who seemed to have all the money.  The weather was dull during November but the rain kept well off.  The plan of this wonderful city is rather complicated  but as I possessed maps and excellent information I was easily able to find my destinations even during the fogs &amp; the much darkened streets. 
 Weymouth.  A seaside health resort with an excellent harbour, and comfortable hotels &amp; private residences 
