 [Page 36] 
 The Imperial Hotel Russel Square London . W.C, 10.9.16 
 Dear father, The lack of correspondence lately, has been due to our state of unsettled anticipation; but at last things have come to a head and we are once more back into the old groove.; 
 The 3rd Div cyclist boy after a valiant struggle to keep the old title, has at last  succumbed to overwhelming odds and is no more.  We are scattered to the  four winds of heaven and if there is a 
 Yelled out in a gleeful voice  'Ah! Burnt to death the ----! 
 I have written to MacCallum but as yet have received no reply. This is not very surprising, as I suppose most of our mail will go astray now. 
 I have not been able to find out anything about Arthur's belongings here. If they can tell me nothing at A.I.F. headquarters at Horseferry Rd. tomorrow, I will write to the 20th Bn and find out what has been done with them. 
 Give my love to mother Dorothy and Catherine &ndash; and remember me to Mr. O'Reilly when you see him. 
 Your loving son Keith 
 Ps. I got in touch with Mr Pring the other day, but have not seen him yet. 