 [Page 12] 
 Fritz a lively time tonight.  A few more shells dropped in town principally at 11.30 square &amp; Houplines.  Each evening we visit some French friends and sit talking to them till 8.30. 
 27th Sept. Wednesday 
 Incinerator.  More shells in the evening.  Numbers killed &amp; wounded though none are hurt in the trenches.  1000 British drove 40,000 Germans out of Armentiers two years back.  Spies are pretty numerous.  German balloon broke loose &amp; 5 of our planes 
