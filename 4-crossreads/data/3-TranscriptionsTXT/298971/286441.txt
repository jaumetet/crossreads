 [Page 49] 
 27th October (Friday) 
 Stayed in bed all the morning.  Cold, cloudy &amp; raining outside.  At 4 pm we moved off to relieve the 14th F. Amb. at Runner's Post (the second).  It was raining heavily at the time and we lost our way.  We went miles out of our course through Longueval &amp; Delville Woods.  In these places there is good evidence of bitter fighting.  Absolutely wet through we duly relieved the 14th and spent a busy evening with patients as a 
