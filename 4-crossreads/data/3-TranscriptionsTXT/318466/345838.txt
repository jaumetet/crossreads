 [Page 102] 
 Sunday 27 October 1918 Mater's Birthday.  Went out for a walk with Muriel for first time since illness, feelg. weak. 
 Monday 28 October 1918 Into Town &ndash; U.S.A. Consul &amp; A.I.F. &amp; one or two other places, was glad to get back Home. 
 Tuesday 29 October 1918 Bought a cabin trunk at Highgate. 
 Wednesday 30 October 1918 Lunched at Pinoli's with Rup.  Then to A.I.F. &amp; altered allotment.  Tead with Muriel &amp; Geoff Atkinson at the Waldorf.  Home for dinner. 
 Thursday 31 October 1918 Turkey surrendered. Into American Consul for my vise on Passport.  Lunched at the Club with Mundon of the old school.  On to A.I.F.  Bought suit civvies. 
 Friday 1 November 1918 Into town in afternoon for passport to be amended at U.S.A. Consul.  To A.I.F. about paybook.  Civvies came but did not fit so am changing. 
 Saturday 2 November 1918 Into Town in morng. &amp; changed suit at Phillips.  Out to Palmers Green in afternoon &amp; tead with Mother &amp; Ede &amp; the Mills.  Home for dinner. 