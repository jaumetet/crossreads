 [Page 72] 
 Sunday 14 July 1918 Had a lazy day &amp; read &amp; wrote.  Good Y.M.C.A. run by English ladies, this used to be the Vicarage. 
 Monday 15 July 1918 Paraded sick about the teeth.  Wandsworth didn't finish.  They tried to put me on draft conductg. but I got out of it!!! 
 Tuesday 16 July 1918 Plenty of rain.  Usual parades etc.  Fixed up with Dentist for next Monday.  Revolver shootg. 
 Wednesday 17 July 1918 Think Sutton Veny is one of the best camps I have been in for accommodation. 
 Thursday 18 July 1918 Went to Salisbury &amp; Fovant.  Saw Matron Lamming.  Major Simpson was out so left a note. 
 Friday 19 July 1918 Rained all day &amp; we did live bombing in the middle of it!!  Got off early. 
 Saturday 20 July 1918 Advance Guard stunt in morng.  Went shootg. in afternoon &amp; got ptridge.  marker. Sister Birt Ede 