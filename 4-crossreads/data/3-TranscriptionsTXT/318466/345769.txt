 [Page 32] 
 Sunday 24 February 1918 Went into Kemmel for a bath but they were closed.  Met Bentley &amp; Browne.  Parcel from Rup. 
 Monday 25 February 1918 Had anor. trip into Kemmel on the Club Committee stunt.  Decided to pay us 1000 frs.  My applon. for staff job did not go thro' so I hear today.  Borrowed records from L. Timbs. 
 Tuesday 26 February 1918 Went down to Bde. &amp; reported as to yesterday's meeting at the Club.  Fixed up the gramophone.  Feelg. fed &amp; longing to be home.  Things v. quiet. 
 Wednesday 27 February 1918 Salvaged a good deal of wire in morning.  Wrote to Miss Eva, the donor of the mess tin of biscuits!!!  No mail from Rup, hence the wet weather.  Paid 100 fr.  Recommended Blackman for M.M. 
 Thursday 28 February 1918 Salvaged more wire.  Lovely weather in morning but snowed in afternoon.  Feel the most miserable man on earth tho' I don't know why!!! 
 Friday 1 March 1918 Anor.  day  month gone, thank goodness!!  Went into Kemmel for a lecture &amp; for a bath after.  The latter was damned chilly!!  Home on the rations train.  Bit of a strafe on our sector at night.  Hear the C.O. is going. 
 Saturday 2 March 1918 Snowing like Hell &amp; had to go to a bally G.C.M. at the 15th Bde. H.Q. as prosecutor.  Raised a blister on my heel &amp; cursed all the way home.  Col. Simpson left. 