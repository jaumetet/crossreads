 [Page 68] 
 Sunday 30 June 1918 Went out to Findon in morng.  Tead at Perivale in the afternoon. 
 Monday 1 July 1918 One of the Yankees crashed on the beach this morng.  Propeller was smashed but pilot not hurt. 
 Tuesday 2 July 1918 My eyes are still giving me trouble.  Am wearg. dark glasses occasionally. 
 Wednesday 3 July 1918 Bathed in sea afternoon, not too warm.  Breakers not so good as the Manly ones!! 
 Thursday 4 July 1918  The water was top hole &amp; warm.   Took Rup &amp; Muriel to Salvington Mill in a trap &amp; very small pony!!  Tennis with Chicken later. 
 Friday 5 July 1918 Tead at the Atkinson's &amp; beaucoup Tennis afterwards. 
 Saturday 6 July 1918 Leave up to Town by the 2 p.m.  Stayed at Highgate. 