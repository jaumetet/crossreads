 [Page 62] 
 Sunday 9 June 1918 Had tea &amp; comp. Tennis at the Hotel. 
 Monday 10 June 1918 Arrived in Town in the evening from the River &amp; heard that I was for a board on the 11th. 
 Tuesday 11 June 1918 Went up to the Board with Bull &amp; got two weeks convalescence. 
 Wednesday 12 May 1918 Toddled down to St. Neots with Rup &amp; stayed at the X Keys Hotel. 
 Thursday 13 June 1918 Market Day &amp; looked out for the Magers but did not see them.  On the river most of the day. 
 Friday 14 June 1918 Drove thro' Hunts &amp; called on the Magers later.  Had our lunch on the roadside near Offord. 
 Saturday 15 June 1918 St. Neots is very sleepy and I don't think they know there is a war on at all. 