 [Page 85] 
 Slept in all morn. Heavy bombdmt down coast. Turks said to be driven this way. QM Fatigue noon. Saw Tite. Quiet well. Into supports at 7. Good sleep. S.T.A. at 3. Into look out at 5 with Seymour. Posted PCs to Mtr &amp; Mrs D. &amp; notes to Mrs F &amp; KMW per Vincent. Big gun duel in morn &amp; our night got it pretty hot. 
 Mon 14th Trenches. Observation Pst Fairly quiet 
 Tues 15th Supports. Bombdmt on coast Quiet our way. Enemy shelling 
 Wed 16th Relieved from Trenches about 9.30 am. Removed to Gully by Hdqtrs Staff. Severed partnshp with Seymour. Tea with Tite &amp; Swainey. Met Cooper. This are worse than the bullets Far more annoying Godly supposed to have some 
