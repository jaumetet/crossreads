 [Page 65] 
 were permitted to return to their trenches &amp; at once a hot fire commenced. Such are the methods of our amateur officers. 
 I don't expect to see dawn as we are a mere skeleton army here &amp; no reserves to reinforce 
 This spells finish here if they charge &amp; are massing at the 
