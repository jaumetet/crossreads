 [Page 106] 
  May  
 in constructing this canal, attempted to cut the Panama-Canal through but failed, went bankrupt and died a pauper. 
 The speed at which ships are allowed to travel through the Canal is not to exceed five knots per hour. I was unable to see much of what existed along the banks of the canal, owing to it being dark when we passed through the grater part of it. 
 Along the Arabian side there are military camps for a long distance, The Australian Light Horse are in the majority. 
 After passing Lake Bitter I saw a lot of trenches and barbed wire entanglements along both banks, which were put there to form defence works to protect Suez Township and canal from enemy attack. The majority of the country on both banks is flat desert land with a fertile spot visible every now and again. I cannot say what Suez township would be like if inspected closely I could not form any idea by 
