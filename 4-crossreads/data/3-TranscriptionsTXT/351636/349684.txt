 [Page 37] 
 18.1.16 
 Dear Mr. Layton, Mrs. Bethel the bearer of this note wife of T. S. Bethel returned wounded &amp; now discharged is at present receiving &pound;2-0-0 per week from your fund. Her husband was told at the Australia Day Fund Office  that  to day that this would cease on Friday next. Her husband pension is expected to take about two months to fix up &amp; until this has been done the allotment from your Fund is all they have to live on. Should it cease they will have nothing. In this case would you continue the amount until the pension is available. Private Bethel's discharge dates from the 6th January &ndash; 
 Yours faithfully B. Booth 
