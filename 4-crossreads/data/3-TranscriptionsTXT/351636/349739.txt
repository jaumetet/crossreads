 [Page 92] 
 Centre for Soldiers' Wives &amp; Mothers 92B Pitt St. 7.6.17. 
 Dear Mr. Horsfield I am sending Mrs. Shirtley with a friend up to you to see if you will pay off her machine her only debt &amp; the son only support has been killed if this debt were cleared for her her daughter who is learning tailoring could assist her &ndash; 
 Yours sincerely E. H. Bennie &ndash; 
