 [Page 17] 
 The Centre Soldiers Wives &amp; Mothers Pitt St. 29th September 1915 
 Dear Mr. Layton You kindly gave Mrs. Horsnell &pound;2 on the 20th to tide her over until her son should receive his pay. He is not expecting any till Friday or Saturday. As she has no money left do you think you could give her 5/- or 10/- to help for the next day or two? 
 Believe me Yours sincerely E. M. Wallack 
