 [Page 27] 
 The Centre November 16th. 1915 
 Dear Mr. Layton We are sending Mrs. Tallehawk for a little immediate assistance. Her husband Archibald James Tallehawk enlisted under the name of Hamilton 198 [indecipherable] Co 30th Battalion but is supposed to have deserted &amp; gone with 4th R. 20th B. 
 Pending enquiries by the military authorities she is receiving no pay at all 
 We think you may be willing to assist 
 Yours faithfully B. Booth 
