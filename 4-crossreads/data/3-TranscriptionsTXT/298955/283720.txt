 [Page 5] 
 "Canberra" Esplanade Perth Western Australia 4th January 1920 
 The Principal Librarian Mitchell Library Macquarie Street Sydney N.S.W. 
 Dear Sir, I have noticed by advertisement your desire to purchase the diaries of Australian Soldiers, who served abroad on the European War, for the State Archives. My father's diary I am forwarding under separate cover, and should like it preserved in the archives of the Mitchell Library, so trust therefore you will accept it. I am Yours faithfully Phillip  E.C. deMouncey 