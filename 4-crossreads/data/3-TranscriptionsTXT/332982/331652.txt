 [Page  17] 
 Meningitis on board, but the ships Doctor said that it was nothing of the kind, any way the ship was isolated. The general opinion on board was that it was a tale put up by the shore officials to keep the Boys on the ship, as some of the previous ship's with Australians had played up and had destroyed a few places (hotels). In the afternoon we were allowed to go for a route march. 
