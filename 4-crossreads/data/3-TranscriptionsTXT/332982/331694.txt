 [Page  59] 
 To the Doctor this morning. He was sent into the Hospital with measles, and all the Boys in the Hut (No24) were isolated. We were taken off the Parade ground and sent into the hut, and not allowed to mix with the other men. I don't think the Boys mind, as they will get out of the long route marches. 
