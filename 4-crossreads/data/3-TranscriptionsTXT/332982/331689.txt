 [Page  54] 
 Tuesday 9th Jan 1917 Arrived in Plymouth at 4am .Disembarked at 2pm. Taken ashore by the S.S.Sir Walter Raleigh. Left Plymouth at 4.15pm by corridor train (G.W.R.) John and I and four others had a lovely first class compartment. At Exeter we were provided with Tea and buns by the ladies. All blinds have to be kept down on case of Zep. Raids, Very little 
