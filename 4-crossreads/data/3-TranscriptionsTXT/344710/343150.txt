 [Page 3] 
 15.11.17 Awakened 5.30 after a strenuous evening. I was billeting Sergeant and our Mess was a British Canteen. Ordered at a moment's notice to proceed to Messines village with 20 men (11 platoon, not my own) &amp; report to an 18 pr. Battery 400 yds. Left from there. Half an hour Later we got away and toiled up the first ridge. Passing Brigade we split into parties of 6.  A Long weary march.  The ridge has been very heavily shelled, the craters being the largest we have seen even on the Somme. Got well roared up for exposing our party on the top of the ridge.  Also got splendidly Lost at Last.  Orders were wrong as regards the Battery. Finally met an Officer with a map, &amp; at a Late hour we were dug in to an old trench, wet but fairly comfy. 
 16.11.17 No sign of the R.E. Officers to whom I report.  I hear our job is to build an O.P. for the battery, just near the support lines.  Arranged for our rations with the Battery. 
 17.11.17 Still no orders.  A quiet day. 