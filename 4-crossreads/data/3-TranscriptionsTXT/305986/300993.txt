 [Page 62] 
 "Our Billets" 
 Our billet is "airy" Our Billet is "sweet" You can smell the dam place as you come up the street, A manure heap in front, a manure heap behind, Why, man, you're in clover, that is - of a kind 
 The place is quite "Holey", and oft unawares The wind comes whistling with various airs, It is a bit draughty, but, then, you are told To live in fresh air, keeps one clear of a cold. 
 The straw that you lie on was laid on the floor, When France was at war 