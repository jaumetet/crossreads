 [Page 112] 
 Or spread the oil that's issued us To keep our steelwork clean, Upon a slice of issue bread, And save the margarine. Or pass it to the HMC Instead of number mines To cure some sole from agony When he unwisely dines. 
 It must take quite a lot of iron, To make those ordnance razors, But what about the biscuit tins, Allowed to go to blazes. I think theres still a lot to learn Of various ways of saving, And after twenty years of war They'll know that I'm not raving 
 F----------------S 