 [Page 8] 
 4/1/17 Franvillers for night 
 5/1/17 Flesselles. Jam tarts milk &amp; eggs. 
 14/1/17 Buire. buckshee tin of fruit 
 16/1/17 Fricourt. Cold weather water in sandbags. 
 23/1/17 Meault for bath. 
 24/1/17 Albert. 
 26/1/17 Bernafay camp in Trones Wood 
 28/1/17 Front line before le Transloy. Also in Lesbouefs. 
 1/8/17 C camp. 
 3/2/17 Montauban 
 6/2/17 Blighty Trench 
 9/2/17 Front line 
 9 &ndash; 10 &amp; 11th Patrolled No mans Land &amp; went to Le Transloy. 