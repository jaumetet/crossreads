 [Page 37] 
 33 to a place called Warloy about 12 miles in a car it was great. Saw some old trenches on the way &amp; several aeroplanes being peppered with shrapnel. 
 Arrived back about 10 o'c &amp; had a good yarn to the folk here including the "petite fille", who is very nice. Had a "du Vin Rouge" &amp; turned in. We are utilising a folding bed which we found here it is great lovely &amp; soft but find it very hard to rise in the morning. 
 The weather now is perfect, yesterday was quite hot. There was a letter from Doss when I got back last night, there must be more about somewhere for me. 
 31/7/16 Had a lovely quiet day yesterday the weather was beautiful. We spent most of 
