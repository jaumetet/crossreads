 [Page 40] 
 36 billeted in. 
 Every move we creep up closer to the line &amp; last night I strolled up to the top of the hill behind us &amp; saw a sight which I'll never forget. A fierce Artillery battle was raging a few miles off &amp; the roar of the guns was deafening. It was a wonderful sight, the sky was illuminated with bursting shrapnel &amp; star shells, a fascinating sight but one which made one think. 
 I will very likely see more of it as I hear the HQrs. are moving to Albert a town that is shelled pretty constantly. We passed numerous Ambulance wagons on our way here full of wounded but the chaps were all smiles. 
 11/8/16. We have decided to stay 
