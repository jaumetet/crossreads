 [Page 18] 
 at El Burj. But as we left over a mile of Turkish wire which is much superior to our D3 wire I intend going out tomorrow to reel the Turkish wire in. 
 3-2-17 Sat 3rd Leask, Wride &amp; I left here at 9 oclock this morning &amp; had a very enjoyable ride to El Burj  Went to the 5th Mtd Bde Head Qrs &amp; after a good deal of arguing recovered all the Turkish wire. Had dinner there &amp; returned, The Tommies are getting the wire road down in good style also the railway. 
 An enemy plane came over El Arish this morning but was out of range A mine was washed up on the beach today 