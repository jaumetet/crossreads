 [Page 147] 
 14th November&nbsp; 4 
 Captain Anderson, Master, S.S. &quot;Messina&quot;, Rabaul. 
 Sir, It was my intention on arrival at Rabaul with the German Prisoners from Nauru, to have transferred the latter to the S.S. &quot;Matunga&quot; for transport to Sydney, so that your Ship might return direct to Nauru to load phosphate for the Pacific Phosphate Company, but unfortunately the &quot;Matunga&quot; sailed from here on the 11th instant. 
 As there will be no opportunity of sending the Prisoners now on board your Ship to Australia&nbsp; for at least 3 weeks, and I consider it highly undesirable that they should be taken on shore here, I have to request you to put to sea at 4-0 p.m. to-day, and proceed direct to Sydney and convey the Prisoners to that Port under a Military Guard that will be placed on board. 
 I have the honor to be, Sir, Your obedient servant, 
 Colonel. Administrator. 
 &nbsp; 