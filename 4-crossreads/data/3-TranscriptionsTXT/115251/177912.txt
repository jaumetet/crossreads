 [Page 21] 
 [Printed letterhead] H.M.A.S Australia. 
 To the Admiral of the Australian Fleet 
 In the name of the officials taken prisoner I have honour to protest against our being kept prisoner, in opposition to the common law of nations. 
 1&nbsp; We do not belong to the armed forces&nbsp; We were forbidden by the Governor to  employ  offer any armed resistance. 
 2&nbsp; We offered no resistance of any sort 
 3&nbsp; We were ordered to go to the Government, went there and were immediately seized and taken prisoners 
 4&nbsp; There is every chance of losses of private property.&nbsp; Yesterday as it is several things were stolen 