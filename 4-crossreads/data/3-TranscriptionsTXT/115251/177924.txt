 [Page 33] 
 [Translation of&nbsp; letter written in German on pages 31 and 32, from Dr E. Haber, Acting Governor of German New Guinea to Colonel Holmes] 
 In the field, sept. 14th 1914. 
 Sir! I have the honour to conferin receipt of your favour of this afternoon and will try to reach Herbertsh&ouml;he to morrow, tuesday, about 11 o&#39;clock.&nbsp; In consequense of the long distance it may happen that I will be a little late.&nbsp; In this case I beg leave to ask your pardon in advance. 
 I have give ordre to stop the hos tilities from our part until further notice and beg to kindly take care that the same ordre be given on the british side. 
 I beg furthermore to point out that I have given my consent to appear in Herbertsh&ouml;he with the understanding, that I will have safe conduct back for my A.D.C. and myself in 