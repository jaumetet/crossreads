 [Page 168] 
  Receipt.  
 Received from&nbsp;Colonel W. Holmes, V.D., D.S.O., the original of the terms of capitulation for the surrender of German New Guinea from Dr. Haber. 
 Signature. &nbsp;JG Legge &nbsp; &nbsp; &nbsp;Colonel. Chief of the General Staff. 
 Victoria Barracks, Melbourne. 25th January, 1915. 