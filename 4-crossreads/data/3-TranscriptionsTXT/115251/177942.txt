 [Page 51] 
 [See page 50] 
 LIST OF PRISONERS: 
 
 
 
  Name  
  Occupation  
  Remarks  
 
 
 Weber Gustav 
 Judge (White &amp; Japanese) 
 Declines to carry on Judgeship. 
 
 
 Grundler Eugen 
 Secretary to Govt. Accountant&#39;s Dept. 
 &nbsp; 
 
 
 Heysik Edward 
 Bureau Assistant. 
 &nbsp; 
 
 
 Shuppert Gustav 
 Native Court Secty. 
 &nbsp; 
 
 
 Kleffling Paul 
 Assistant Native Court. 
 &nbsp; 
 
 
 Schultz Antm 
 Treasury Assistant. 
 &nbsp; 
 
 
 Gottschalck Kurt 
 Bureau Assistant. 
 &nbsp; 
 
 
 Schmidt Jacob 
 Secretary Bureau. 
 &nbsp; 
 
 
 Mahler John 
 Govt. Store-keeper. 
 &nbsp; 
 
 
 Hamnner Joseph 
 Builder Govt. 
 &nbsp; 
 
 
 Tolke Ewin 
 Chief of Administration Burgomeister. 
 &nbsp; 
 
 
 Weller Karl 
 Post Secretary. 
 &nbsp; 
 
 
 Grumbach Franz 
 Secty. Judge&#39;s Court. 
 &nbsp; 
 
 
 Berghausen Ernst 
 Government Resident Administrator. 
 &nbsp; 
 
 
 Muller Heinrich 
 Merchant. 
 &nbsp; 
 
 
 Bredemann Gustav 
 Curator Botanical Gardens. 
 &nbsp; 
 
 
 Kerler August 
 Secretary to Govt. 
 &nbsp; 
 
 
 Jahn Bruno 
 Police Master. 
 &nbsp; 
 
 
 Kleppek Akois 
 Telegraph Operator. 
 &nbsp; 
 
 
 Blumenstengler Karl 
 Carpenter. 
 &nbsp; 
 
 
 Broedner Albert 
 Land Surveyor. 
 &nbsp; 
 
 
 Scale Rudolph 
 Police Master. 
 &nbsp; 
 
 
 Frank Otto 
 Bureau Assistant. 
 &nbsp; 
 
 
 
 WH 14 9 4 