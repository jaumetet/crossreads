 [Page 50] 
 H.M.A.S. &quot;Berrima&quot;, Rabaul, 14th September 14. 
 The Rear-Admiral Commanding Australian Fleet. 
 Sir; Herewith I forward list of Prisoners of War now on this Ship, whom it is in my opinion most desirable in the interests of the good administration of this territory should be deported.&nbsp; 
 In accordance with arrangements already made, I have instructed that these Prisoners shall be transferred to the Flagship at 6-0 p.m. to-day. 
 I have the honor to be, Sir; Your obedient servant, WH Colonel. Commanding Aust. N.&amp; M. Expeditionary Force. 