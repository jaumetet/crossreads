 [Page 51] 
 Monday June 30th at Sea 
 Calm day : boys getting very excited : we ought to get to Sydney about midnight : plenty of shore lights after tea &amp; speculation as to our position : some waited up to see the boat into the harbour but bed was my fancy 
 Tuesday July 1st Sydney &amp; Home at last : 
 Dropped anchor in early hours of morn in Watson's Bay : it was great to look round &amp; see the old familiar places again : 
