 [Page 4] 
 [Extract from letters to the Editor, Egyptian Gazette] RED CROSS &amp; Y.M.C.A. 
 To the Editor, "Egyptian Gazette." 
 Sir, - Re Australian Red Cross versus Egyptian Y.M.C.A. controversy, I venture to say that most, if not all, Australians think as "Australian Woman" does, especially the wounded. I contend that we should never have had to depend on outside kindness for the supply of comforts, which our Red Cross Fund was intended to provide, &amp; well could. 
 I am, Sir, "A 2 months-bread-and jam-fed (up) Convalescent." 
 Alexandria, July 16. 
