 [Page 18] 
 3. are terrified that we are going to be torpedoed &amp; they sleep in their clothes to be ready for the worst. I suggested that they should also put their lifebelts on, but they think the matter too serious to joke about. 
 This is Monday &amp; we expect to reach Plymouth on Friday morning &amp; if they give us a special train we should be in London in time for dinner. 
 Hoping you are all well &amp; with kindest regards to all from 
 Irene V. Read 
