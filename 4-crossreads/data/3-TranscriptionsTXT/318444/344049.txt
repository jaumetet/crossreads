 [Page 77] 
 Quota to C Company at another part of camp. Awaiting the result of my application for a course at the Motor School in London. That No 10 Quota will never leave for Australia. 
 Mon Ap 7th:- Nothing to do except sleep &amp; eat our rations. Nothing doing to the accompaniment of fine weather. 
 Thurs Apr 10th:- Shifted from C Coy Heytesbury over to No 1 Command Depot Sutton Veny. 
 Good Friday April 18th:- At Sutton Veny camp eating shark (underdone) 
 Sat Apr 19th:- Left camp at got the 8 am train from Warminster, via Reading to London. Arrived Paddington Station at 10.45 am. 
 Easter Mon April 21st:- By 'bus to Richmond &amp; then by steamer down to Hampton Court. Around the palace &amp; survived the struggle home. 
 Wed Apr 23rd:- Left by midnight train to Swindon. 
