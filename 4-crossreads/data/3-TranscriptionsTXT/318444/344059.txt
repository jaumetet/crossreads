 [Page 87] 
 Sun Nov 9th:- At midday we were 272 miles out &amp; just past Ushant into the Bay of Biscay. Average speed 12.5 knots. 
 Mon Nov 10th:- Rough night but sunny morning. Another 276 miles up till midday &amp; we passed Finnisterre this afternoon. 
 Tues Nov 11th:- Good weather &amp; 292 miles completed. Armistice day 1st anniversary. 6009 miles to Durban. 
 Wed Nov 12th:- 287 miles by midday 
 Thurs Nov 13th:- 266 miles today's count. A strong headwind blowing &amp; heavy seas running. 
 Fri Nov 14th:- 270 miles up today. Passed Teneriffe at 11.30 last night. Sea has calmed down 
 Sat Nov 15th:- 273 miles &ndash; just on 11.3 knots nearing Cape Blanc &amp; just getting in the tropics. 
 Sun Nov 16th:- 289 miles &ndash; 12 knots. Getting sultry. 
