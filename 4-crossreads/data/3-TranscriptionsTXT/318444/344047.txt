 [Page 75] 
 Thurs Mar 13th:- Left Consett by train from Blackhill Station at 9.2 am for Newcastle &amp; got the 10.5 am train to London. Had a good time though still snowing. Reached London at 5 pm. 
 Sat Mar 15th:- Still around London &amp; to the Coliseum tonight 
 Tues mar 18th:- Left London by the 6.10am from Waterloo Station to Tidworth &amp; sneaked into the Park house Camp a day late. Rumour says that we will be on the move to Sutton Veny in a few days time. Snow is starting &amp; making things miserable here especially after being off the chain for a fortnight. 
 Thurs Mar 20th:- Left Park house Camp &amp; went by train from Tidworth to Codford &amp; walked to Heytesbury depot. Made out an application for non-military employment as it looks like never getting a boat home &amp; these camps are "eyesores  &amp; earaches". All you can do is sit down &amp; "mope". Snow is added to our troubles. 
 Fri April 4th 1919:- Shifted from No 10 
