 [Page 81] 
 Sat June 28th 1919:- Peace signed. Around Trafalgar Square in the rumpus. 
 Fri July 4th:- Dinner to Billee Hughes at the Horseferry Road War Chest Club. No luck &ndash; The 1/- feed did not choke him. 
 Thurs July 17th:- By train from waterloo Station to Windsor &amp; Eton. Over the castle &amp; then by train to Taplow &amp; Slough. Got launch back on river from Clievdon Woods. 
 Sat July 19th:- Peace procession of the Allied Forces through London. Saw it along Vauxhall Bridge Road. 
 Mon July 21st:- Left London from Paddington Station by the 5.5 pm train to Warminister. Just missed the roll for the "Argyllshire" to leave on Saturday. 
 Tues Aug 5th 1919:- Off to London again. One gets "fed up" of the dirt-box of a camp. Left by the 10.20 am train from Warminister to Paddington. 
