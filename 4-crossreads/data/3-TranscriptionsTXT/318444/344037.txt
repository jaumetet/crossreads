 [Page 65] 
 Thurs Jan 2nd:- Out to Acton for the day. 
 Fri Jan 10th:- Empire Theatre at night. 
 Sun Jan 12th:- Left London from Victoria Station &amp; crossed to Calais. Spent night at Rest camp. 
 Mon Jan 13th:- Entrained at Calais &amp; today passed through Etaples, St Pol &amp; Arras. 
 Tues Jan 14th:- Passed through Douai, Mons &amp; St Ghislain &amp; reached Charleroi at 11pm put up at 1st Div Rfts billets. 
 Wed Jan 15th:- Left Charleroi by tram to Chatelet &amp; by legs to Bn at Acoz. 
 Thurs Jan 16th:- Aussie Xmas pcls arrive. 
 Sun Jan 19th:- Our first draft leaves us en route to England to embark for Australia. They 
