 [Page 187] 
 Elsie"s address Allesley Dalley Road Naremburn 
 Greg Thorpe 6245 14 F. Amb. 
 V.R. Nile &ndash; B. Adams 9th F. Amb. 
 John R. Thorpe 85 Finlay St. Fulham London, S.W. 
 Yet Again, 5/- Xmas Garland &ndash; 5/- [indecipherable] &ndash; 4/6 Happy Hypocrite &ndash; 1/- - O.P. Poets Corner &ndash; 5/- - O.P. 
 Max Beerbohm 
 Jim Healey 25 Westfield Park, Redland Bristol 
 Mr. A. M. Parsons 23 Milford St. Thornton Heath London 
 Wimereux &ndash; misspelt as Wimereax &ndash; P. 1 Etarps &ndash; possibly Etaples &ndash; P. 22 Branguin &ndash; also spelt Brangwyn &ndash; P. 24 Hamel &ndash; misspelt as Hammel &ndash; P. 68 Becordel &ndash; misspelt as Becaudles &ndash; P. 73 Bazentin le Petit &ndash; misspelt as Byzantin &ndash; P. 73 Bapaume &ndash; misspelt as Baupaume &ndash; P. 75 Veulx &ndash; possibly Vaulx &ndash; P. 86 E.F.C. &ndash; Expeditionary Force Canteen R.A.M.C. &ndash; Royal Army Medical Corps A.A.V.C. &ndash; Australian Army Veterinary Corps A.D.M.S. &ndash; Assistant Director of Medical Services M.D.S. &ndash; Main Dressing Station "Maleish" &ndash; possibly Mafeesh &ndash; nothing, all gone from the Arabic 
 [Transcribed by Judy Gimbert for the State Library of New South Wales] 
 