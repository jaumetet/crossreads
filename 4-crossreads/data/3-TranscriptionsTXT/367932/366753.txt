 [Page 55] 
 [Letter cont. (2).] [Reference to the outbreak of influenza and the death of Lieut. I. Dalton.  Comments are made on the condition of previous Anzac trenches and the impact of disuse.   A visit was also made to the location of "Beachy Bill" and the Turkish lines.] 
 