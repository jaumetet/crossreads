 [Page 82] 
 Alex Guthrie is writing &amp; Tony Horden lying down.  Two of the best mates you could ever wish for, &amp;.  We are going away &ndash; call it clearing out, running away, evacuating, it all comes to the same thing, &ndash; but we are not beaten.  We'll meet Abdul again someday, &amp; I hope we have the higher ground then. 
 Goodnight Mother and Father when you get this, if you ever do, it will be all over, one way or the other 
 Your loving son Lionel 
 This short letter came in the same envelope 
 Cairo 31 Dec. 
 My dear Mother 
 You might like to see that letter, so Ill post it, reading it now, I can laugh, at it &ndash; but it was no laughing matter then &ndash; but you can see how we felt about clearing out.  I was wild that night.  Well its all over now &ndash; &amp; one way thank goodness not the other 
 Your loving son Lionel Bigg 
