 [Page 54] 
 (1). 
 Visit of 7th LH to Gallipoli Dec. 1918 
 [Letter to Headquarters of 2nd A.L.H. Brigade from J.D. Richardson, Lt. Col., Commanding 7th Light Horse Regiment, A.I.F.] [Report on return to Gallipoli after the end of the war.  Reference to Lieut. Colonel Findlay, C.B. D.S.O.] 
 