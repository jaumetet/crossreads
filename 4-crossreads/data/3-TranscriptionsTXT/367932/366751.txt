 [Page 53] 
 7th L.H. Regt. 30/1/19. 
 Dear Colonel Arnott 
 We were not able actually to identify either Morris' or Colonel Harris' grave, as all the crosses and headstones had been removed in the Shell green cemetery except two &ndash; Trooper Ronald of 6th L.H. and Tpr D . . . ton 7th L.H.  The Graves Registration Unit should be able to identify these easily however, as they have all the graves plotted out onto a proper plan, and with a few cross bearings should be able to restore the crosses above every grave. 
 We are waiting up here at Rafa for our demobilisation, and hope that it may not be too long delayed.  I hope to get leave before long and will try and call in and see you, 
 Yours very sincerely J.D. Richardson 
 [Colonel John McLean Arnott CGM and Colonel Dalyell Richardson, both Commanding Officers of 7th Light Horse.] 
 