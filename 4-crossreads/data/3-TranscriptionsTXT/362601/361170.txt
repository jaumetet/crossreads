 [Page 40] 
 8 March, Saturday 
 F. G.O.C. [General Officer Commanding] makes rounds accompanied by very much Belying. I go to Y.W.C.A. &amp; play for Dance all night. It is a very nice evening. I am feeling off &amp; cannot get to sleep thinking about M.O.I. 
 Wrote Con &amp; Marion. 
 9 March, Sunday 
 W. I get my Breeches from the Tailor. I pack up for I.O.M.  [Isle of Man]. I go to Y.W.C.A. &amp; sit alone all night, reading music. Go home &amp; straight to Bed. I wait on the corner for Nothing. 
 Wrote Nette, Dad, Mum. 