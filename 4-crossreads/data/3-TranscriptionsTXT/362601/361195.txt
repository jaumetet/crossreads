 [Page 65] 
 27 April, Sunday 
 W &amp; Snow 
 I go to church &amp; the parson talks at me &amp; tells me to get an idea. We all sir around fire all day &amp; I sort my bags over again. I play cards &amp; go to bed early. 
 We get no dinner. 
 28 April, Monday 
 F. I go to Bulford for Teeth. Tea at mess. Go to Sarum &amp; spend [?] Home at No 13. I run all the way to train have to wait 30 minutes. Recd letters from Dick, Doris &amp; Dad. 