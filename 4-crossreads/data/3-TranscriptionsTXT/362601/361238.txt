 [Page 108] 
 17 July, Thursday 
 F. 
 Lovely Day. We sail at 6 am. We have a big clearance in Hospital. I expect to go overland from Melbourne so I pack all my bags &amp; bring 'em up to Hospital. 
 18 July, Friday 
 We get news at 3 pm that we go ashore at Melbourne &amp; overland. All excited &amp; tons of work. I sleep in Hospital on top story. 
 We sight the heads at 8 pm. 