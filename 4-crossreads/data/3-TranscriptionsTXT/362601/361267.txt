 [Page 137] 
 15 September, Monday 
 Bread strike begins so procession march along George St. I talk outside shop with sympathiser. I nearly buy a fight on tram Home. 
 Write to Ida Dunn. 
 16 September, Tuesday 
 Go to Welcome Home. Awful pianist, awful songs. "The Irish Washerwoman" nearly kills me &amp; brings me out in a sweat. I respond &amp; play Auld Lang Syne. Meet Tom Proud. 