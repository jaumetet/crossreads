 [Page 76] 
 19 May, Monday 
 F. Billy &amp; I carry swag to Dinton &amp; have refreshments there. After dinner we go for sleep etc. after tea get films. Sat &amp; see Picture on "Life of Nelson". 7 reels very good. 
 2 Boat rolls I miss. 
 Wrote M.R.L. &amp; Home. 
 20 May, Tuesday 
 F. Print photos all day. Very long job. Go for a walk with Billy after tea, all around Dinton. We view some magnificent views, some magnificent views from 'Hill Top'. Meet Ted "Ryde", Lyall Walker. 
 Wrote Lottie, Miss Chapman, Mrs McKinnon 