 [Page 120] 
 10 August, Sunday 
 Chop wood in morning. Geo &amp; I go to Narrabeen see Livo. Come Home &amp; stay. Moonlight &amp; I think of Island. I play piano until Dad &amp; Mum go to bed. Mum is melancholy. 
 M 19 
 11 August, Monday 
 Mum &amp; I go to Manly shopping. Visit Mrs Stephen, Dobbies &amp; Whiteheads &amp; Creightons. We buy Ellis's presents &amp; soda syphon. 
 Wrote to Mr Paul. 