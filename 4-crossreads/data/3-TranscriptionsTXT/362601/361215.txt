 [Page 85] 
 6 June, Friday 
 F. I go upstairs to wash &amp; have bit. I am so weak will that I cannot do anything. I sit in chair for a couple of hours. Then come below &amp; lie about till dinner time. No brekkie or dinner, not even a drink until 5 pm, go to bed after dinner. Better at Tea &amp; eat a little. Headache worst in my life. Meet Jack Craven. See schools of porpoises &amp; whales. 
 7 June, Saturday 
 F. Lovely Day. We see some ships but in the distance. I rub the C.O's arm. Work all day, am booked for the voyage as Nurse. We have music in afternoon. Sea calm but nice breeze in afternoon. I have a chase over a deck chair. I am much better to-day. I go to the nether regions for ice. 