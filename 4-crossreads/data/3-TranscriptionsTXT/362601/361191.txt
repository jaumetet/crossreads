 [Page 61] 
 19 April, Saturday 
 F &amp; Hot 
 Meet Hassells &amp; Les Luker also Len O'Brien. Meet Bill Allinors. I go to Bulford &amp; meet the boys in Mess. It is a lovely hot sultry day &amp; we cut out the fire. We all go to Boozer &amp; celebrate, but I have a row &amp; buzz off. I am very put out but eventually go to bed easy in my mind. 
 I see H. Hey &amp; M. taking off. C.H.H. has Sheperds Plain &amp; M.C.R.[?] 20 April, Sunday 
 F but windy. 
 I go to Tidworth all morning with a victim of sand-bagging. E.T. is drunk horrible. Recd letter from Jennie. I go to T'worth Delhi after Tea with a Patient. It is a lovely ride in the cool of the evening. 
 Wrote Mum, Mr Edmonds, Con, Marg, M. McKinnon, Aunt Annie. 