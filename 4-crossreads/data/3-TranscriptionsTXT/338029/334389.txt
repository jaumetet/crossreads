 [Page 30] 
 Thursday 2nd Nov. Pay day, first lot went on leave, I am endeavouring to get away this day week, very cold at night now, one must wear flannels.  Letter from Lionel all well. 
 Friday 3rd Nov. Continued boxing to-day.  I had gloves on, sadly out of condition, only went two rounds.  Double Squadrons left to-day for camels,  I paid back all borrowed money and showed a  &pound;4 profit with the mess this week. 
 Saturday 4th Nov. Boxing at Wesh  in p.m., big mail came in I rec. my letters all containing good news, put me in a happy frame of mind, recited that night at Y.M.C.A. Concert, was doubly encored. 
 Sunday 5th Nov. There is a big move on somewhere, troops arriving in thousands at Moascar would not be surprised if we were fighting at Xmas.  Spent day answering letters. 
 Monday 6th Nov. We have a sweep on Melb. Cup, and it was drawn to-day,  I as usual drew outsiders.  Woloroi won the Derby. 
 Teusday 7th Cup Day the third I have been away from, it 