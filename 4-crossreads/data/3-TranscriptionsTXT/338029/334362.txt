 [Page 3] 
 If found, please return, Sergt. A.P.K. Morris 12th A.L.H. Regt. or Mrs. A.P. Morris Denbigh Road Armadale Melbourne 
 My Diary on Active service being continuation of fourth book commencing on Fri. 15th Sept. 1916 
 Word came through that 6 Squadron had to journey to Megeibrah, one of our advanced posts near Ogratina, a distance of 50 miles across heavy sand.  The rest of the regt. will follow in a few days time.  Incidentally M. was visited by Taubes to-day and bombed. 
 Sat. 16th Sept. C Squad marched out at 6 a.m.  Great news from all fronts.  Aust's Casualty list now published, size is terrific, such a lot of my mates have gone under. 