 [Page 53] 
 Monday 5th There are two many senior offs. &amp; N.C.O's on this boat and they are all clashing it is highly amusing.  Last night we struck a tropical cloud burst, the hatches were open and when I went on duty in a.m. the wards had water 3" deep, the patients were drenched. 
 Teusday 6th Spent day cleaning up, no one worse for it, change in duty roster now.  Had enjoyable concert at night, recited four times.  The W.O. and I as partners are yet undefeated at whist, we beat all comers. 
 Wednesday 7th Usual kind of day, I rather pride myself now as a ward Sgt.  Everything going smoothly.  In the same waters as we got the Emden on 9th Nov.  Of all the patients on board there are only about two who were there. 
 Thursday 8th Sea very calm, big swell on, boat pitched a bit, it makes one very squeamy to stay long below.  Sister Lyons pinched all my jelly to-day &ndash; I'll get hers to-morrow. 
 Friday 9th Choppy sea, a few seasick, hope to reach Freemantle  on Teusday next. 