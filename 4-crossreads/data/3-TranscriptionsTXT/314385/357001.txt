 [Page 41] 
 Sat Oct 28th Had a good nights rest the first for seven days &amp; we are just resting nothing more. There is nothing as to our movements. 
 Had another look at the mill today it is worked by a horizental Turbine with a six foot fall of water. 
 This country tho somewhat flat is picturesque. 
 We pass the time playing cards yarning etc. 
 I went to see my little French madame &amp; madamoselle &amp; wrote some letters there. 
 Our transport landed here last night the weather is wet &amp; cold. 
 Sun Oct 29th The day again broke rainy and watery. 
 We are paid to day the first for a month. 
 Am writing to catch X mail. 
 Mon Oct 30th Weather improving. 
 R.S. &amp; myself walked 6 miles to a nice town called [Corly?] very ancient we found the church dated back to 640 AD a well built structure larger than St Pauls Melb also a monastry connected but it is used as 