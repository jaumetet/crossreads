 [Page 30] 
 1916 
 he is on a visit to the trenches from Australia &amp; was there to deliver a message from the people of Australia. 
 Mon Oct 16 This is Election day at the front &amp; we are to record our vots as to wether there is to be conscription in Australia or not. 
 News is to hand that this is to be our last day here we are striking the tents &amp; loading them on the waggons &amp; are warned to parade in marching order in the morning. There is plenty of excitement &amp; speculation. 
 Tues Oct 17 Revalli at 5 30, breakfast parade in marching order at 7 &amp; at 7.30 our Col gave the order up packs, form fours, left turn, quick march and thus the ambulance moved off in colum of route. 
 We marched by the way of Reninghelst and reached the France-Belgium frontier at 12 pm. after a strenuous march where we halted for dinner. 
 It seemed interesting to be on the frontier. There were two armed guards. 