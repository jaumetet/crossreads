 [Page 29] 
 1916 
 Wed Oct 11th All very tranquil at the front tho there are gun flashes to be seen in the direction of Ypres but we could not hear the reports. 
 Thur Oct 12th Another quiet day. 
 No more heard about us leaving yet. 
 Football match this afternoon. 
 Fri Oct 13th Same old routine. 
 Weather fine, mail to day, letter &amp; photo from Paris. 
 Sat Oct 14th Things as usual. 
 In the afternoon a football match was played here between the 2nd D.A.C. &amp; ourselves we won. 
 Sun Oct 15th Duties same. 
 In morning a church parade was held &amp; we marched to an adjacent paddock where a drumhead service was held. Bishop Riley of  the  WA who is Chaplain General preached a very impressive adress 