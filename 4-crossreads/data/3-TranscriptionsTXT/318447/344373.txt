 [Page 56] 
 This evening we had a dozen big bottles which we found very difficult to get rid of, in fact I think some were still left, as everyone refused to look at it tonight. 
 The weather during the last week has been vile, a high wind &amp; gusty with oceans of rain. I have managed to keep fairly dry but it is most depressing to paddle through sloppy mud to one's own little oasis. 
 A new establishment has come 
