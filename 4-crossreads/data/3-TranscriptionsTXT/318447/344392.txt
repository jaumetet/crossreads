 [Page 75] 
 pleasant sight after the weary precipitous &amp; rock envisaged journey from Latron. 
 17 March After spending two extremely miserable days out in the open amidst the pouring rain &amp; apparently unfathomable mud we went into billets this morning. I, with my gear &amp; two men were told off to a room, but at lunchtime we were peremptorily told to shift, &amp; no one could give us anywhere else to go So after worrying our Captain for a 
