 [Page 78] 
 each meal we shall have to tip them handsomely when we leave Yulia, the daughter of the house a strapping bosseyed wench who goes about habitually in clogs &amp; what appears to be a petticoat, cooked up some bullybeef for us for lunch &amp; tea into the most delicious rissoles. Our only common language is arabic so my knowledge of it has come in very useful after all. 
