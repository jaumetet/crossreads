 [Page 88] 
 These are my Aust. friends who picked me up out of the gutter when I was on leave for the first time in England and for the first time in the A.I.F. for that matter. 
 25th Had a good look round the city today with  this  the ladies.  It is a beautifully laid out city.  Clean wide streets.  The old Cable tram system is still in use.  It does not look out of place though.  The Castle is a fine looking building.  It is perched on a large piece of rock in the centre of the town. 
 26th Went to Empire Palace Theatre. 