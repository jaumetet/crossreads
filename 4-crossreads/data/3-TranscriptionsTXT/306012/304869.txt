 [Page 49] 
 was cleared out and the majority of the patients sent to C.C.S. 
 23rd  April  March 1918 (Sat.) We occupied several old houses on the outskirts of the village about 3 or 4 hundred yards from the old spot. To-day we evacuated the remainder of the patients and removed equipment from the shelled camp. 
 24th March 1918 (Sund.) Fritz kept putting stuff over but none on us, although it meant work for all. News has come through that the Germans have commenced a big attack on the Somme and are forcing the British back.  We are getting ready for an early move. 