 [Page 34] 
 17th Sep. '15 On board No. 1 A.H.S. Mediterranean 
 My Dear Ma 
 Just before leaving Southampton, while assisting in adjusting the x-ray app. I dropped, or rather fell with a big 16 in coil in my arms and injured my wrist. So I had to get James to drop a line to you. The doctor diagnosed it as a fracture but when the x-ray was put on it next morning they found it was not so! One carple bone is displaced 