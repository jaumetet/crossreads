 [Page 96] 
 4 as dawn was breaking. This was a wretched spectacle and many children clung like glue to their fathers. Embarkation was finished at 8.30 &amp; we pulled out. The crowd were let on to the wharf &amp; hundreds of streamers were thrown. Stuart was down on the wharf on duty, and Biden was Sergeant of the guard on the wharf, which was very nice. 
 Well Father I wont be able to write for a long time now. I will send any cables to Marion who will send them on. With very best love to all, &amp; hoping grandma has improved 
 Yours affectionately 
 Dene 