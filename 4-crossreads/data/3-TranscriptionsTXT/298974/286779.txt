 [Page 103] 
 4th Oct. 1916 
 My dear Mother Here we are at an out of the way place, &amp; I post this on chance only. The trip from the Cape has been smooth the whole way &amp; I'm feeling awfully well. I had an absys on my tooth that troubled me with neuralgia but that's all right now. We expect to arrive in England Oct 11th Best love to all, &amp; tell Hazel I will probably try to see Olly in the old dart but am not sure. Dene 
 Turn the card &amp; its] [indecipherable] Washington's face below cross. 