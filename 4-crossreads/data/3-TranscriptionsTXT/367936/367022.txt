 [Page 128] 
 Thursday 10 August 1916 Retired a little further back.  Resting all day.  Saw Colin. Bit short drinking water.  Not allowed to take off Boots or any gear of course. 
 Friday 11 August 1916 Still in reserve. On outpost at night.  Horses getting very tucked up owing to only getting handfuls of oats sometimes 3 times a day. Men not much better. 