 [Page 108] 
 Saturday 1 July 1916 Were shifted from tents into ward but still in isolation.  Getting very good tucker. Printed photos in afternoon. Wrote Mother (No. 7). 
 Sunday 2 July 1916 Took film up to Alex to be developed. 