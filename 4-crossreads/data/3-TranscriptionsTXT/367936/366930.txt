 [Page 36] 
 Tuesday 8 February 1916 Mother and Father both came down. Father went back same night. Dr Mason says with care will stop pneumonia.  Tempreture still over 103. 
 Wednesday 9 February 1916 Having very bad nights, no sleep and half delirious.  Living on ice and milk.  Nice having Mother to look after me instead of being in hospital in camp. 