 [Page 107] 
 Thursday 29 June 1916 Went into Alexandria.  Sent parcel home registered containing table clothe, sleeve links &amp; studs. Got films of Nouzha gardens, etc. 
 Friday 30 June 1916 Went into Alex on my own.  Got receipt for registered parcel. Met Nelson came home in Garry. 