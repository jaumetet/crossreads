 [Page 169] 
 Tuesday 31 October 1916 Got tooth pulled out. Inspected by General Murray the G.O.C. in the afternoon. Sent registered parcel of photos home. 
 1st November 1916 Carried on as per usual. 
 Wednesday [1] 2 November 1916 All hands up early and saddled up to start for desert.  Lot of busters as owing to remounts being flash. Crossed canal.  Saw lot of Bedouin prisoners in Kantara E. Hill 70 for dinner. Camped at Doudiah [Dueidar]. Camels carrying overcoats and blankets. 