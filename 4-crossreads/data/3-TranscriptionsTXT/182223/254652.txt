 [Page 55] 
 Thursday 8&nbsp;April 1915 Acting O. C. today&nbsp;Colonel Martin at Alexandria &nbsp; Letters from Mrs Reardon of Lock Gur, Kilmallock Co. Limerick Ireland 
 Friday 9&nbsp;April 1915 Colonel Martin returned at 12.30 pm. He took over duties of O.C.&nbsp; Riding between&nbsp;5 &amp;&nbsp;6-30 pm. Letter from College of Surgeons &amp; George Reid. 
 Saturday 10&nbsp;April 1915 Colonel Martin away most of the day, acting O.C. for him&nbsp;&nbsp; Not out during the day&nbsp; Posted letters to Girls, Mollie &amp; Doffie 
 Sunday 11&nbsp;April 1915 Mass at 7-30 a.m. A padre from S.A., Adelaide diocese,&nbsp;a Tipperary man, officiated.&nbsp; Wrote David Storey M.L.A. Randwick&nbsp; Riding on dessert during the evening 1 hr 5-30 to 6-30 o&#39;ck 