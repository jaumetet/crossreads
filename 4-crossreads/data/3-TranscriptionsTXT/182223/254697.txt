 [Page 100] 
 Monday 13&nbsp;September 1915&nbsp; Prof Watson called about 6 pm&nbsp;&nbsp; Went with him to Sheppheards hotel &amp; dined with Barrett at 8.30 p.m. Home at 11-30 p.m.&nbsp; 
 Tuesday 14&nbsp;September 1915&nbsp; In Cairo this morning for two hours.&nbsp; Letters from all at home&nbsp; Answd these letters by 2 A.M. 15-9-15&nbsp; 
 Wednesday 15&nbsp;September 1915&nbsp; About the hospital all day&nbsp; Work not being done to my satisfaction by my medical staff.&nbsp; 