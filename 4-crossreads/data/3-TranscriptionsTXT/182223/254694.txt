 [Page 97] 
 Thursday 2&nbsp;September 1915&nbsp; Not out all day.&nbsp; Major Purdy took tea with me at 8 p.m.&nbsp; Letters from home to 25 th  July&nbsp; Car, Joseph, Buddie, Kitty &amp; a lot of friends. Reports good in all ways.&nbsp; 
 Friday 3&nbsp;September 1915&nbsp; In Cairo for one hour during the morning.&nbsp; About hospital rest of the day.&nbsp; 
 Saturday 4&nbsp;September 1915&nbsp; Not out all day&nbsp; Father King from Melbourne partook of tea with me a 7-30 pm&nbsp; He looked after the Roman Catholics during the afternoon.&nbsp; 
 Sunday 5&nbsp;September 1915&nbsp; Mass here during the morning, I served.&nbsp;23. Communicants.&nbsp; At Convent Zeitoun, between 6 &amp; 8 pm.&nbsp; 