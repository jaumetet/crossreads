 [Page 76] 
 April 1917 Saturday 21 Dug trenches again farther out to form a better line. Expect the Jacks to attack but he did not. 
 Sunday 22 Relieved by Yeomanry . We retired back a couple of miles. Heat and dust very bad 
