 [Page 63] 
 [Pages 54 to 62 blank] March 1917 Monday 26 Very foggy morning did not lift until 9am First operations at Gaza 10th LH in reserve &ndash; going in- Standing to arms all night moved at 2 am .10th as rearguard to the brigade. Major Dangar does some good work 
 Tuesday 27 Retired from Gaza shelled by the Turks also fired upon with rifles. One of our planes came down in flames. Almost surrounded and captured. Got back safely thanks to Genl Roystons leadership. Total British losses &ndash; 13000 and Gaza not taken. 
