 [Page 26] 
 January 1917 January 10 (cont) back in old camp El Arish, dead weary and hungry. Had no sleep for 58 hours and no food. Big casualty list. all the camp fast asleep. 
 Jan.11 Still very tired  - Had an easy day &ndash;overhauled camel saddles and camels &ndash; some have sore backs. 
 Friday 12 Quiet day 
