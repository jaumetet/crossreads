 [Page 197] 
 December 1917 Wednesday 19 Suffra.  Showery day and cold. Went on night outpost. Rained during night and got wet thro Blankets all wet. No shelter 
 Thursday 20 Suffra. Cold day with occasional showers. Tried  to dry blankets by fire. Cold damp blankets at night. Still well. No colds. Big mail I got 34 letters. 
