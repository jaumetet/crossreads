 [Page 84] 
 May 1917 
 Monday 7 Went to Aust Gen Hosp saw Dave, Fr Goodman and host of the boys also Vic Holland Vic Masters. Dave looking well. Was at Hospital from 2.30 till 8.15 pm. Col McIntosh (12 LH) died of wounds received at Gaza 
 Tuesday 8 Went in to National with Jack Gratham to see Olsson 
