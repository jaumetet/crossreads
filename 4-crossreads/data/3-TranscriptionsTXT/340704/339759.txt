 [Page 70] 
 April 1917 Monday 9 Moved out to Tel el Jimmi as screen for 9th LH who were digging reservoirs by night; in preparation for the next big offensive on Gaza 
 Tuesday 10 Movved out to Tel el Jimmi we dug the reservoirs tonight to hold water for the big push. 
