 [Page 73] 
 April 1917 Sunday 15 Moved out to night from our old camp. camped for night 
 Monday 16 at 2 pm 3rd Bgde moved out to take up outpost line prior to the big push which we are to hop into tomorrow at daybreak. Saw cousin Bob Fell out here  He is with 4th Signal troop 4th Brigade &ndash; (in our division) Moved out for the big fight at 7pm. Reinforcements arrived in darkness. 
