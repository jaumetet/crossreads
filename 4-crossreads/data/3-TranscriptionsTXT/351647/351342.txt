 [Page 80] 
 The sun has had a little warmth in its beams the last day or so &amp; it surely can't go on being cold for ever. 
 Everything is frozen, ice inches thick, ground like iron &amp; everything white. 
 All the papers have been talking of the intense cold, it has been down to 19&deg; Fahrenheit.  Bed is the best place really but its an awful wrench arising. 
 We have been doing some more prancing round the country.  Left the little village where we rested (?) and ambled to another village where we stayed a couple of days.  Nothing startling there only work.  Then on Sunday we ambled on &amp; arrived here (back in the line) in the afternoon.  Moving isn't so bad for me now I have got out of that Brigade Clerk job &amp; I have a bike all to myself &amp; can choose my own pace &amp; route. 
 Here I have a fairly decent dug out &amp; a fire place in it &amp; as there is a shell damaged wood close handy with plenty of fallen timber I have a fire going &amp; I am getting along very 
