 [Page 162] 
 Brian Mac has the M. Cross.  I'm so pleased.  He does deserve it. 
 Field 9 September 
 Dear Mrs. Jack, 
 I think I told you I had received your cable, thanks for sending it, I'm glad you told me, although it was a rotten shock.  Poor old Jim I hope he didn't suffer long.  It must be an awful shock for Isobel, poor girl.  I somehow can't realise it yet, &amp; yet I know it is only too true.  I find myself still thinking him &amp; getting rotten shocks when I realise I won't see the dear old chap again. 
 However I hope Isobel will be left enough to live on, I'm glad she has the little kiddie &amp; Miss Redford will look after her too. 
 Poor old Bringagee, I shan't take much interest in it now. 
 By the way, I accidentally bumped into Broad of Wilga (Kooba) the other day.  It was a shock to me as I didn't know he had enlisted.  Edgar Read (late of Kerarbury) is I believe in the same battery.  I like Broad although he was a mad headed coot but I never knew he was connected with Andrew Broad before.  I hope to see him again soon at present things are mixed, also I'm off to Paris.  I don't 
