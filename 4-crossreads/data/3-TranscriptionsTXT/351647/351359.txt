 [Page 97] 
 France again 4 December 
 My dear Mrs. John, 
 Fancy I got about three letters from you at once &amp; one from the dear old Boss &amp; I was so jolly pleased &amp; yet so homesick.  I still try &amp; not think about things too much but sometimes you can't help it.  I love to hear you say in my mind, "Don't be such a fool Wilkie, you  are  an idiot" like you did sometimes when I made one of my elephantine efforts to be humourous.  I certainly think you are changing but I'm hanged if I could explain the why &amp; how on paper. 
 You simply must not get too much of that that predestined air, "it was to be so what's the use of worrying" sort of mood. 
 I'm glad you're back in Tog &amp; settling down on a new book and I like the title although it does sound sensational.  I'm all agog to read it now. 
 I wonder if Lerida has been with you, she is a dear soul but I'm like you, sometimes feel that I love her a little less strenuous or if she must have these strenuous fits on that she'd have them away out of it.  Say up at the tool shed. 
 I simply love your letters Mrs. John they are so you.  Yes it is dull to be good till you've  old  ceased to be young then its much more comfortable.  Then you cuss anyone who stirs you up &amp; makes you wish to be anything else.  I don't think its any easier for a man but perhaps I'm wrong there.  I loved the photos of Graham, he does look 
