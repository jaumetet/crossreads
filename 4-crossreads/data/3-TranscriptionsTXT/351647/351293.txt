 [Page 31] 
 12-3-17 
 Just a note to say the air cushion &amp; the Havelock tobacco have just arrived &amp; I'm very very grateful.  They both fill a long felt want.  The pillow is most swanky &amp; in our colours too.  I shall be able to flaunt it. 
 Judy Morrice &amp; Duncan Macfarlane came today, they are Lieutenants in the R.F.A. [Royal Field Artillery] but I don't think are altogether enraptured with it.  Our fellows &amp; Tommies don't mingle somehow, they don't understand one another.  Their minds are totally different. We have far more in common with the Scotchies somehow. 
 Got quite homesick today, it was a nice spring day, warm &amp; bright &amp; a heavenly south wind &amp; in the distance a band would persist in playing "Rag Time Violin", "That Mysterious Rag", "Rendezvous" &amp; "In the Shadows".  I was up in the wood above &amp; came upon a Australian Infantry man &amp; we talked of the dear old land.  He was a rough diamond but a good sort 
