 [Page 59] 
 mind to do likewise but I'm wedded to Australia &amp; even the prospect of unlimited grand opera doesn't detach me. 
 Home is where the heart is. 
 Well &amp; how are you &amp; Graham &amp; the Boss &amp; Mac.  I expect Graham is getting huge &amp; is quite a man.  I would like to see him. 
 I hope the Boss is well, tell him I saw Claude Nowland yesterday he is all right again &amp; back here. 
 I haven't been to Paris yet hope to get in September D.V. 
 What sort of a winter had you &amp; how is the garden.  Is the diminutive jackeroo still there &amp; is Sarah still tripping about.  The dear old soul, she was good to me.  And Ma Gibbons &amp; Paddy how are they?  You don't know how I'm simply hungering for all Tog &amp; its works. 
 And the things I'm dying to tell you &amp; can't on paper. 
 Well I must flee, I'm getting busier every day. 
 Good luck &amp; best love to you all &amp; specially yourself. 
 Yours as ever, Wilkie 
