 [Page 70] 
 are there it hurts to go. 
 One of my rather special camarades got killed the other day but I simply can't write about it yet. 
 I forgot to thank you for the socks, I created quite a sensation in them the other morning.  I had to fly across to the telephone early (its across the road &amp; I hadn't my leggings or puttees on, just the purpuel socks.  Coming back I struck a whole company, couldn't turn &amp; flee so there stood your James, in shirt, pants &amp; purple socks &amp; slippers half way down the roadside bank while the company, officers &amp; all, went marching by.  You should have  heard  the remarks, they would have pleased your ear. 
 Tell the Boss I nearly shot a pal with the little squirt the other day.  I have used it for firing at Boche airmen.  I couldn't conscientiously 
 [See pages 197 and 198 for continuation of this letter.] 
 