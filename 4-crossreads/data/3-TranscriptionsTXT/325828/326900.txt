 [Page 77] 
 In train en route Alexandria. 22.3.16. 
 Dear Father, Have just received this this morning and can only hope that the Censor will pass it through for you will be so glad of the news.  Have kept a copy in case you do not get this &amp; next time will convey the news to you if possible in a less direct form. Had another telegram from Rid first thing this morning &amp; am catching early train to Alexandria to say good-bye. Much love. Olive. 
