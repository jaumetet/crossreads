 [Page 106] 
 Sunday 27 May 
 On Thursday night we had a little of alleged messages, &amp; a little hypnotism. 
 Scotty Haine, who again held most of the conversation, is a gruff Scotsman with swarthy face &amp; iron grey hair (although still young), &amp; rather penetrating eyes.  By appearance he seems of rather a commanding personality until one gets to know him &amp; then &ndash; one gets the impression that his personality is not large enough to fill the body made for it.  He seems a little worn out, &amp; apart from that, not of a very brilliant intelligence 
