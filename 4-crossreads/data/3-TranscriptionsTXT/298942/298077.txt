 [Page 15] 
 old shop &amp; coffee room could be seen lying about. 
 A direct hit had been obtained on the old candle stick  church  church spire. 
 After Caestre we separated into parties &amp; single file followed the road from Caestre to Fletre past shell holes, dead horses, smashed farm machinery, &amp; fallen trees to Fletre. 
 But what a joke! 
 The clock still told 12 oclock as of old, for it has never been in working order since I have known it.  But what a wreck Fritz has made of the village.  The Caestre end is not so bad 
