 [Page 20] 
 which was one of three or four buildings. Others occupied by French soldiers. 
 Have had one case already, &amp; two walkers,  one a 1st lieutenant. 
 Wednesday 1st May 
 All last night the guns around here were barking out so that I found it impossible to sleep until about dawn. 
 A few German shells came near, &amp; a great many passed over.  These, I found this morning were directed at [indecipherable] des Cats for this morning to my astonishment, I found 
