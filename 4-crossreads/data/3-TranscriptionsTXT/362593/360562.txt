 [Page 70] 
 Tuesday 11 June 1918 Played G.H.Q. cricket, we were beaten by 12 runs, they have a splendid team, made 4 runs. 
 Wednesday 12 June 1918 H.A.P.  Pilot Capt. Addison, in air 3 h., no excitement. 