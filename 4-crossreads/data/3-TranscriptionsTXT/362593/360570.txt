 [Page 78] 
 Thursday 27 June 1918 Photos.  Pilot Ltd. Headlam.  In air 1 h. 45 m., successful. Allan Brown &amp; Fin get a Hun.  Oxenham &amp; Smith get a Hun but crash alongside at Katiani.  Excellent work by them all.  Feared Oxenham &amp; Smith killed. 
 Friday 28 June 1918 Photos.  Pilot Capt. Addison, in air 1 h. 20 m., returned owing to clouds, in air 1 h. 50 m., successful. 