 [Page 48] 
 Sunday 14 April 1918 Test Flight.  Pilot Capt. Addison.  In air 20 m.  Tried new piston in gun, went well. 
 Monday 15 April 1918 Photos.  Pilot Capt. Addison, in air 2 h. 55 m., good results obtained. 