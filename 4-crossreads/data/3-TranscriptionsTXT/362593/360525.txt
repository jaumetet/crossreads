 [Page 33] 
 Saturday 9 March 1916 Recco.  Es Salt-Nablus, in air 2 h. 50 m. Pilot Lt. Headlam.  B.F.  A good recco but very cloudy. Got an anti-aircraft shell extremely close but no damage done. 
 Sunday 10 March 1918 Recco Es Salt, Nablus. Pilot Capt. Addison.  In air 2 h. 45 m.  A good trip. Reported cavalry in Jordan valley. It is a pleasure to be flying with Addie again. 