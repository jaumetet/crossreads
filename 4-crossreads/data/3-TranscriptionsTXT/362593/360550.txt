 [Page 58] 
 Monday 6 May 1918 Put in for leave from the 12th. 
 Tuesday 7 May 1918 Paul drives down a Hun &amp; crushes him.  Tonkin gets 2. 