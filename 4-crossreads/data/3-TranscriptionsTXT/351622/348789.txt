 [Page 47] 
 1 March 1916 Wed Twelve months to-day since I joined this unit. Arthur Preston while in Cairo calls to see Randolph for me. A lot of the boys suffering from mumps. Big debate in tent 8 pm. Pommies verses Australians. We have 5 Poms in the tent. 
 2 March 1916 Thur A most awful day. Sand blowing about in sheets. Sailors from the Euraylus come to spend a few days in the trenches. There are now eleven hundred camels here employed carrying water &amp; stores from Ferby Post. A most awful stormy night it is a wonder the tents pegs are holding. 
