 [Page 107] 
 25 June 1916 Sun To day is the anniversary of our departure from Sydney. Most of the old boys celebrated the day finishing up with a sing song in the estaminet close by. The cooks made up 4 or 5 big plum duff which were sufficient to feed 200 of us. Most of the corps full 
 [Text continued in margin] by bed time. 
 26 June 1916 Mon Pick &amp; shovel work all day. Felt very sick on awakening after the big night. Saw Mr. Minter A fine cove. Great excitement at 8 p.m. Two German (aeroplanes) observation balloons brought down. You could hear the cheers for miles. Several German planes up peppered with shrapnel also star shells lit up the skies 
 [Note in margin] Hardwick transferred 
