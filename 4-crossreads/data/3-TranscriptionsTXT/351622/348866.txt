 [Page 124] 
 telling me he has obtained a commission in the English Army. Many an old familiar face. 
 29 July 1916 Sat Just after midnight last night the wounded began to pour in. They came in hundreds. Terrible wounds. Feet blown off &amp; faces. My hands were red with human blood. Jimmy Fay, Ronny &amp; McKenzie are fighting like lions. I have just heard the great news that Jim &amp; Mc have been recommended for the V.C. I do hope they survive this terrible ordeal. Letter from Randolph [text continued at top of page] 
 30 July 1916 Sun Up all night dressing wounds. 4 p.m. left the Chateau for the Chalk Pits passing the Gordon Dump. We arrived at our post after about an hour journey. It is awful shelling something terrific. How one can live through this I do not know. Without any  imag  exaggeration there is not one yard of ground untouched by shell. No wonder men are demoralised 
 [Text continued in margin] by this perfect hell. German dug-outs. 
