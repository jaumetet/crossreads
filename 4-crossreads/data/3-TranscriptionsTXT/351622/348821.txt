 [Page 79] 
 attack. All hands stand by their helmets. 
 30 April 1916 Low Sun Half-past two early this morning there was an exceptionally heavy bombardment. Two shells fell a little way up the road about 8 oclock. Started to work with Albert Doust in the receiving room. Just as I had turned in the signals went up that the Germans were making a gas- [text continued at top of page] 
 1 May 1916 Mon Bill Fordyce's case comes off &amp; he gets 7 days Field Punishment. A very busy day &amp; news that General Townshead has surrended so Kut has fallen. A few more big German shells fell out in the fields again to-day. Washing very cheap charge more mending washing pair of trousers 2 &frac12;. 
