 [Page 94] 
 life as hard as ever &amp; their barn is rocking some. 
 30 May 1916 Tues I am writing this in an old barn which by the way is infested with rats for there is a big cove who comes nibbling round my head at night time. I can hear the B.A.C. waggons rumbling along with their iron rations for the Germans. To night the artillery are at going for their [text continued at top of page] 
 31 May 1916 Wed Arose fairly early &amp; passed the morning dodging the heads. After got the bike &amp; went in search of Vere. On the way I met Roy King attached to the DAC. Surprised to see each other out here. I at last arrived at C. Company 3rd Batt &amp; saw my old pal Vere. He looks well &amp; we had a great old yarn together. On leaving him I met Aggo who is as wild 
 [Text continued in margin] as ever. 
