 [Page 96] 
 loss of the Queen Mary. 
 3 June 1916 Sat Spent the morning in reading to Mr. Minter whose eyes have failed. After dinner walked through the fields of crops to Steenwerck. Two cricket teams played the 8th C.C.S. at Balliel &amp; won by 8 runs. Davis receiving 8 wickets for 17. News of the great naval engagement &amp; [text continued at top of page] 
 4 June 1916 Sun Had an early dinner &amp; with Carl Warburton got the horses &amp; rode to Sailly for the purpose of seeing Hubert &amp; Cockie. I found them billeted in an old flower mill. Hubert is not the old stout gentleman now he is much thinner &amp; Cock as usual looks pretty good. To see them again was just grand. Sports meeting held &amp; Ken &amp; Cpl Brown win the cock-fight 
 [Note in margin] Captain Needham arrives. 
