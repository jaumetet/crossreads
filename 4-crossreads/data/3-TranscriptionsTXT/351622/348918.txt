 [Page 176] 
 great guns at night Zeppelins aeroplanes set fire to big French Ammunition dump. 
 6 November 1916 Mon Gave Anti Tetanus injection to big Prussian Guard of one of the Bavarian Regiments. Leave to England has been started. Sid Colemans dugout two Sgt Majors Royal Irish Rifles big night ending in Sid being put under arrest. Dick Powter came home sick in his tin helmet 
 [Note in left margin] magnificent sight 
 [Note in right margin] Mr. [O'Connor] C.C.S. [indecipherable] 
 7 November 1916 Tues To day is Melbourne Cup day &amp; here we are up to our neck in work in sodden fields of France. I feel now as though I dont care if ever I see Australia again with Hubert &amp; Cockie gone. Letter from Jay [indecipherable] &amp; Snowy Milham &ndash; the [indecipherable] [Joe Barry] [indecipherable] passes on his to the C.C.S. George [indecipherable] wounded also. 
