 [Page 121] 
 rumble of the big guns around Albert. British gain ground at Pozieres. 
 23 July 1916 Sun Loaded up the waggons over at the transport lines till dinner. The parade was dismissed at 2 pm. Slept from then on as I was on gas &ndash; picquet in the village from 7.p.m. It was very quiet walking up &amp; down that road save for the [text continued at top of page] 
 [Note in margin] 1st Division in [indecipherable] Division support 
 24 July 1916 Mon Spent the morning in bandaging practice with Major Gibson. Route march of 12 miles arriving afternoon passing through Varennes after tea went round to C. Company Pioneer Batt to see Paddy Burnham. We adjourned to a billet where we passed away an hour with a couple of Heidsicks. Paddy was well 
 [Text continued in margin] oiled &amp; is the same as ever 
