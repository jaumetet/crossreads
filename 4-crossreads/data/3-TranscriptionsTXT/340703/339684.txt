 [Page 111] 
 Memoranda 
 Jan 5th 1917 Rained again in showers. Wind very cold. Met a lot of old friends. Lt Lamb, Lt Dawson 5th LH Lt Bell 2 LH all in 11th Coy ICC 
 Jan 6 Nothing of note today. 
 Jan 7th Work very hard today all bustle whilst issuing rations at 8  pm moonlight our camp was bombed by Taubs 2 bombs dropped 30 yards away. No damage. Moving out to Rafa for battle tomorrow. 
