 [Page 102] 
 December 1916 Saturday 16 Started on our long trek 7am.Passed several graves of L.H. on the journey saw lots of dead horses. Arrived at Duedar and camped for the night. There was a battle here last Easter Sunday between LH and the Turk.  The LH held out ; numerous graves. 
 Sunday 17 Left very early. Passed thro Katia and then 5 miles south of Romani. Still seeing graves. 
