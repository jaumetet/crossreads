 [Page 45] 
 told to fall in as we were nearly to our destination, and next day we would very likely be into the war. From Hazebruick we marched by road to the little village of Saille where we were put into an old Brick Foundry to stay the night. 
 [Photograph of H.M.A.T. "Nestor."] 
 Everything was dead quiet and war seemed miles away so we just strolled into the village itself to have a look round as we had not yet had the experience of being in a french village to purchase anything and become aquainted with the French manners &amp; customs. 
 As luck would have it I fell in with one of the boys I had known earlier in the game &amp; who had 