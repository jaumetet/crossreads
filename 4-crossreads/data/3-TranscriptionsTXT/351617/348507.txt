 [Page 231] 
 of the good old Southern Cross away ahead and it just seemed to change the very atmosphere 
 31st 256 miles now very hot and a fair beam roll on the sea 
 Sunday June 1st 256 miles Good sea no wind 
 2nd 236 Very hot Canvas covers put over the deck to keep the sun off. 
 3rd 222 miles. Heat unbearable sleeping in hammocks without blankets 
 4th 245 miles crossed the Equator (no line Visable) 
 5th 236 good sea no wind 
 6th 221 ship stopped for a spell, into the Gulf of Guinea 
 7th 230 nights getting shorter and cooler 
 Sunday 8th 226 sea still good 2 big sharks seen close to ship 
 9th 231 nothing out of ordinary happened 
 10th 217 Currents head on now all the way to Cape Town 
 11th 211 miles Loosing ground every day ship must have stopped for another spell during the night 
 12th June Rough Sea and heavy weather from port side ship rolling found it hard to sleep for swinging of hammock 
 13th weather moderate but sea 