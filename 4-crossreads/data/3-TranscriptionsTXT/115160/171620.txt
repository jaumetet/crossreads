 Page 105 
 the Australians drive the Boche back victoriously. 
 Have you received your postcards? 
 Send my letter, for I am in a hurry. 
 I trust you in good health and expect your letters patiently 
 Yours faithfully&nbsp; 
 Jean Watelet 
