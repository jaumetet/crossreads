 Page 132 
 2 
 
 
 
 Fresh vegetables (when obtainable) 
 8 ozs 
 
 
 or Dried vegetables 
 2 &quot; 
 
 
 Jam - now 2 oz 
  3 &quot;  
 
 
 Tea 
 &frac12; &quot; 
 
 
 Sugar 
 3 &quot; 
 
 
 or Sugar (when Sweetened Cond milk issued) 
 2&frac12; &quot; 
 
 
 Condensed milk 
 1 &quot; 
 
 
 Salt 
 &frac14; &quot; 
 
 
 Pepper 
 1/100 &quot; 
 
 
 Mustard 
 1/100 &quot; 
 
 
 Pickles (thrice weekly) 
 1 &quot; 
 
 
 Tobacco or Cigarettes (once a week) 
 2 OZS 
 
 
 Matches (thrice fortnightly) 
 1 box 
 
 
 Lime Juice (when authorised) 
 1/160 gall 
 
 
 Rum (when authorised) 
 1/64 &quot; 
 
 
 (b) Equivalents: 
 &nbsp; 
 
 
 Frozen Meat 1 lb = P Meat &nbsp; 
 9 ozs 
 
 
 which includes Veg Rn or M &amp; V 
 1 tin 
 
 
 or P &amp; B 
 2 2/9 tins 
 
 
 
 &nbsp; 
