 Page 57 
 Bohain&nbsp; 
 8 Decbr 1918 
 Darling Mither, 
 The week has slipped in again and here it is Sunday., &nbsp; You will notice I am still here, and we have become detached again. &nbsp;The rest of the Section went back whence they came and left us here to box on, which is not hard to do. &nbsp;Things are very easy and we have no complaints. 
 The weather has been mild with a bit of rain. Things are dull in the town, and the scarcity of our mails makes it more so. &nbsp;I suppose they&#39;ll come in a bundle, as they&#39;ve beeing doing for the last couple of months. &nbsp;But I received another of your cakes which was in excellent condition. &nbsp;Lock is becoming quite a tinker with his soldering iron. 
 Two of us are in a billet with some very nice people who have had to put up with the Huns since the beginning, and some of&nbsp; 
