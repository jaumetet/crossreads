 Page 124 
 9 
 I&#39;m told that the Authorities are cutting out soldiers&#39; parcels, after the Christmas ones. &nbsp;Although the Lads will feel it generally, perhaps there are a few in particular who&nbsp;will feel it most, I&#39;m sure. 
 Well, Dad, this is Thursday, I&#39;ll keep it and it will form my weekly budget. &nbsp;Good night. 
 16th Nov. 
 Gee whiz, you should hear the drum fire at the moment. &nbsp;It&#39;s simply appalling. 
 This envelope looks tampered with, but it&#39;s just one that I opened a week or so ago, and took out the cigarette packets, for it was too bulky. &nbsp;And I can&#39;t afford to waste one, they&nbsp;are so scarce. 
 Sunday 18th 
 Yesterday I received Lockhart&#39;s of 24th Sept readdressed from A.A.S.C. Parkhouse. &nbsp;I&#39;m mighty glad to hear that Mum is keeping well. &nbsp;I see that the shop was able to keep its end up during the strike, and it&#39;s nice to notice the cheerful tone of it. 
 I had a letter from Mrs. Boydon telling me that Alex McDonald was wounded &amp; missing. 
 Well, Dad, this is not a bad attempt &amp; will keep you going for a little. &nbsp;There&#39;s not much more to mention, except that it&#39;s dull and sombre but quite mild. &nbsp;I&#39;m OK and hope are are well. 
 Fondest love to all, 
 Your loving Son 
 James 
