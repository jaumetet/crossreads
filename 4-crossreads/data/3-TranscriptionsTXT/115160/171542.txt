 Page 27 
 29/10/18 
 A few lines on the way as we are on the move back. &nbsp;I&#39;ve a letter half written somewhere but I&#39;m blowed if I could find it. &nbsp;Well Sunday slipped on to me and I didn&#39;t get my usual letter off, but this will keep you from swearing. 
 I had a letter from Auntie Maggie &amp; one from Aunty May in &nbsp;[indecipherable]&nbsp;and from the latter it appears Aunt Maggie has been laid up with the flue. &nbsp;By the bye, I sent off my photos to you and hope you get them safetly. 
 Don&#39;t forget what I have written on another page &amp; lay hold of that fiver. &nbsp;I see too that another [indecipherable]&nbsp;has come to hand for which I am very pleased. &nbsp;I am well up to &quot;sugar&quot; now and holding a fair bit on this side. &nbsp;I will write further on this matter as soon as we get settled.&nbsp; 
 I&#39;m quite OK, the fiddle is good Oh &amp; we are having it a bit easier now. &nbsp;But we&#39;ve had a rough spin as far as work has gone. 
 Fondest love&nbsp; 
 Your loving son 
 James 
