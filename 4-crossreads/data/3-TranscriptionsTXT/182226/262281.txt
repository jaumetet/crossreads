 [Page 286] 
 Thursday 16 September, 1915 
 Letters from home. Mater, Dot &amp; a very nice one from Beryl. 
 Am right once more &amp; will go on duty next watch. 
 The watches have been changed from 18 to 24 hrs. 
 The chaps in the big shed started to mend the roof &amp; had half of it off when the rain came on &amp; gave them a bad time of it. 
 Had a sharp shower which however did not last long. 
 The Turks turned their attention to our post last night, one shell which failed to explode landed very close to our quarters. About a dozen others landed on the crest of the hill. 
 Stray bullets occasionally fall about our humpy one came very close during the night &amp; made us feel a bit uncomfortable. 
