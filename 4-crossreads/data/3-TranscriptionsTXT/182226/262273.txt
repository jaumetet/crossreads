 [Page 278] 
 Wednesday 8 September, 1915 
 Have made a seperate dugout for ourselves. Mac, I &amp; Bernie Norris. Mac is pretty crook in the stomach but seems to be improvong. The 21 st  Batt relieved the 24 th . They are very raw men &amp; allowed a Turk to get into our trenches early in the Morning. 
 21 st  Batt were torpedoed outside Lemnos &amp; lost the Brigadier &amp; about 40 men. 
