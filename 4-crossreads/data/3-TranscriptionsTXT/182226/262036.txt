 [Page 41] 
 Friday 14 January, 1916 
 Got some more letters today, I am in luck. 
 The dainties out of the parcels are going fast &amp; are highly appreciated. 
 We do our work on the desert wich is much harder that that round Cairo &amp; all covered with stones. 
 The country here is very flat looking away towards the canal &amp; all hummocky towards the desert. We get it cold here in the morning but it is lovely &amp; warm later on. 
