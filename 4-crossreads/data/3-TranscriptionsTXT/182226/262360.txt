 [Page 365] 
 Saturday 4 December, 1915 
 The weather here is great. In the morning everything is fresh, just like spring. 
 There is nothing much to do here except play cards. Just at present I have gone broke on the game. 
 I had some good games of handball today, the first time I have tried it, my hands are pretty sore. 
 This place is a veritable gambling hell, wherever you go you see little crowds round a bed &amp; hear the clink of coins. 
 I played till about 1 a.m. 
