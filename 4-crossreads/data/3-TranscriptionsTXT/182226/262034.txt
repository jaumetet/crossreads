 [Page 39] 
 Wednesday 12 January, 1916 
 Still at a loose end nobody loves us &amp; nobody wants us. 
 The camp adjutant has turned out a brick &amp; is trying to get us bedding &amp; money. 
 We got 8/- each &amp;&nbsp;our bedding so we are all right. No leave has been given but we have taken a look round Alex, we have no time to do the thing thoughly but from what I can see it is a much finer place than Cairo though not as large. 
 We got unspecified orders to be ready to move off at 11 p.m., most of our&nbsp;boys were the worse for the 8/- but we got away all right, 130 of us with no one in charge &amp; are now on our way to Cairo. 
 Hooray, I will soon be back with the boys. 
