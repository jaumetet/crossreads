 [Page 376] 
 Wednesday 15 December, 1915 
 Still cold &amp; wet. I have a bit of a cold &amp; am not feeling too bright. 
 The weather was too rough for the boat to come in, it is the first time it has missed since I have been here. 
