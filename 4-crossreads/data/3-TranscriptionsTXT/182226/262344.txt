 [Page 349] 
 Thursday 18 November, 1915 
 Had to come back to camp for medical injection. Was put in &quot;C&quot; Section. Went straight into Valletta after that. Went to Pieta at 2.30. Took the girls photo dressed in our clothes. We put on their skirts meanwhile. 
 We stayed till 10.30. I had to take &quot;Mem&quot; into Valletta as she was disappointed by her two friends. 
 Got to Hotel De France 12 midnight. We should have got to camp that night but we risked it &amp; stayed in all night. 
