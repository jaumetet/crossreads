 [Page 360] 
 Monday 29 November, 1915 
 The weather is much warmer now. We opened our tent up &amp; pulled our beds out to give them an airing in the sun. 
 Sid has been floating round Valletta on his own account for the last couple of days. He landed up in the guard tent this morning. 
 The Maltese&nbsp;Malitia [Militia]&nbsp;set on some of our boys &amp; nearly killed one, he was hit on the head with an iron chair. 
