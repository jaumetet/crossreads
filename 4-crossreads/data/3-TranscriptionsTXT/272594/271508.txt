 [Page 102] 
 &amp; wheeling about. 
 Last night some of the 5th Bde machine gunners brought a big heavy machine down, he was very cheeky &amp; came down too low with disastrous results. 
 There has been some very heavy fighting around Bullecourt, the Tommies got through all right but failed to hold it, the result is that they have left the Australians in a very nasty position &amp; both our flanks are in the air &amp; that means enfilading fire the worst of all to stand up to &amp; tonight 