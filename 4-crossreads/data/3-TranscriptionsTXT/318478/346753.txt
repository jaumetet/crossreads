 [Page 75] 
 Continental Hotel June 2nd 1919 
 To Colonel Croll 
 (Second Light Horse Field Ambulance Memorial Fund) 
 I beg to thank you for your letter of May 29th in which you offer me, formally, a commission to paint a picture recording an incident in the history of the 2nd Light Horse Field Ambulance. 
 I think the subject which you mentioned in our conversation at the 14th A.G.H. most suitable and most interesting. 
 I quite agree to your terms which I hereunder re-capitulate &ndash; 
 That the price be not less than &pound;100.0.0.  That a deposit of &amp;pound:50.0.0 be paid on a/c.  The balance to be paid on completion of the picture.  That the commission be carried out, that is to say the work will be undertaken, as soon as my present contract with the Australian Govt. is completed.  Also I give permission to the 2nd L.H. Field Ambulance Memorial Fund 
