 [Page 92] 
 Moascar July 1st 1919 
 Dear Mick 
 As you see by above address I am in my first  Palestine  possy when I joined up about 18 months or more back.  Remounts are still here and many of my old pals but a lot of Desert has come back and a lot of tents have been folded and a lot of good &amp; tried men have left for their home in Australia.  It is not nice to be a left over but I like this place and am feeling fit and hopeful.  This is practically my last month of service.  It is hard to believe, but I am going on steadily and keeping the jumps down as low as possible. 
 Yours Ever G.W. Lambert 
