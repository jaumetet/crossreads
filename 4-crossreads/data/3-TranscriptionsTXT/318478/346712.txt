 [Page 34] 
 Near Anzac Gallipoli Peninsular March 1st 1919 
 Personal 
 Dear Colonel Fulton 
 By the above address you will notice that I am still wandering round the old battle fields on behalf of the Australian Govt. collecting material for pictures yet to be painted. 
 Since last I had the pleasure of seeing you &amp; Mrs. Fulton my experiences have not been of the best but checks are to be reckoned with in every job and now it seems the only bar to my work is the weather which is really most unkind to the "Australian Historical Mission" of which I am an humble part. 
 Presently we go to Cairo and it is my sincere hope that you will be there and that you will again give me the great pleasure of a meeting &amp; some conversation on and about the many subjects in which, without boasting, I believe we both take a common interest. 
 Yours truly G.W. Lambert (Hon. Capt. A.I.F. War Records Section) 
