 [Page 77] 
 Cairo June 3rd 1919 
 My dear wife &amp; twice Mother by the gifts of God and humble co-operation. 
 We are again to become Heroes &ndash; I use the plural advisedly, but not in the same sense as used by Royalty nor by the little boy with a worm but in the sense that I am with you or rather what is left of me is with you and the boys included.  The sense  of duty w  of duty is a personality in itself and I am really going up with it to see that it does its best. 
 Two hefty henchmen, tried &amp; hardbitten scouts, go with me so I am alright &amp; possibly will regain my health &amp; optimism. 
 I would like to rub my beard against your very soft cheek by way of benediction but bully &amp; biscuit will have to do. 
 I leave Cairo within two hours returning from the blue within two weeks. 
 Yours Ever G.W. Lambert 
