 [Page 55] 
 March 11 &amp; 12th, 1919 
 The Turks have flat backs to their skulls.  This has nothing to do with the diary of the last two days barring that the Turks living that I have now seen in Constantinople conform in type of skull to the bones that I studied at Anzac.  The difference between the British type, that is Irish, English, Scotch, etc., or to give them one name, the Northern type is most strongly marked.  Herewith a chart. [Drawing] It is only fair however to give the front view of Jacko which is as a rule full and not brainless. [Drawings] 
 