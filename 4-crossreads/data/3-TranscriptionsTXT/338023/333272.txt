 [Page 79] 
 Penham Down Feb.12./17 
 Dear Brave little Mother 
 Up to the present I have little or nothing in the way of news to tell you. A little Australian mail has dribbled through to us &ndash; but, so far, nothing of a later date than Dec 12. I had one from Betty dated about then, also your letter card. Poor old mother, I'm afraid you've had a bad time of it &ndash; but I hope, by the time you get this, you will be about again &amp; better than ever. Perhaps now that the job is done you will feel the real benefit. 
 As I told you in my last, I saw Chas &amp; spent the night in his hutment at Wareham. They have a far better camp than ours: also it is better situated 