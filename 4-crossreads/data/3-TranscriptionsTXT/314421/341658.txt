 [Page 33] 
 [Shorthand transcribed as follows] I sent a French post card to my dear wife today showing a photo of a typical French soldier.  I said I was sending some more souvenirs which I have made out of the driving bands of German shells.  [indecipherable]. [End of shorthand] Monday September 24, 1917. (1148.) 
 A fine day. 
 We were issued with tobacco &amp; cigarettes this morning. Our iron rations, Bully beef, &amp; a tin of tea &amp; sugar was issued to every man of the Transport section this morning to replace the supply that was called in a few weeks ago. 
 I again paraded sick this morning &amp; was given treatment &amp; no duties. 
 I received a letter from my brother Willie today, describing briefly his trip to Ireland. He had 8 days leave and had a good look around Dublin, Belfast and other Northern towns, and visited a lot of our relations there. 
 I sent a packet of souvenirs to my wife this evening in a registered parcel. It cost 1 franc 3d for registration. The packet contained two souvenirs, serviette rings made from the driving bands of 5.9 German shells, &amp; one ring made out of the stay wires of an enemy aeroplane. 
 Lieut McNicol lent me a book to night, a guide to Paris. He has been there lately. 
 Tuesday Sept 25. 1917. (1149.) 
 A beautiful morning. 
 Reveille was at 5.30. 
