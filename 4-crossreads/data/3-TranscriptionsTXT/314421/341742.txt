 [Page 117] 
 The Italians have lost Udine and are retreating to a new line where plans have been prepared for Allied help. Things are in a bad way in Germany. Count Hertling has been elected Imperial Chancellor in place of Dr. Michaelis. Germany is faced with Bankruptcy. Britain's war debt is &pound; 3,000,000,000, &amp; Germany's war debt &pound; 4,000,000,000. Austria is bankrupt &amp; neither she nor Germany can pay interest on loans. 
 I am on 4th picquet to night or rather to morrow morning from 3 to 6. 
 I received a letter from Willie this evening dated Oct. 20. He is in the 42. Battery, in the horse lines when writing, but expected to be sent up to his Battery. His letter was very much censored. The number of his division being obliterated every time and also reference to other units, though his remarks were left intact. 
  Thursday  Friday Nov. 2. 1917.  1187. 
 Showery to day. I was on usual routine duties to day. 
 [Shorthand transcribed as follows] I wrote to my wife tonight, letter 98.  I also sent to my dear wife a parcel containing 1 table centre and 4 serviette rings made out of the driving bands of German shells.  I also ordered through the Battalion a new book called "Australia in the Great War" a copy to be sent to my wife and also my sister, Mrs. Kittson.  [indecipherable] There is a furphy afloat 
