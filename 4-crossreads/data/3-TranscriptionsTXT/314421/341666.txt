 [Page 41] 
 there are only farm houses about and no shops. At dinner time today I bought some views of Blendecques. 
 Thursday Sept 27, 1917. (1151.) 
 Revallie 15 past 6. 
 Breakfast 6. 
 We got away about 8. Oclock. The morning was damp and threatening rain, a little fell during the night. The morning soon cleared up and it turned out warm &amp; very close. A lot of troops fell out during the march &amp; we carried their packs &amp; equipment in the limbers. We passed through Lynde, passed through  the commune of Wallen Capelle, but did not enter the town. Here we were only 3 &frac12; kilometres from Staple where we camped one night when leaving the lines for Belquin. At Hondeghem we stopped for dinner. The 37. Battalion were forbidden to enter the Estaminet, which caused a lot of high language. We had about an hour for dinner. I rode  a lot this morning and also yesterday evening. We travelled fully 8 miles this morning in the heat, and the Battalion is very tired, &amp; a great many of them fell out. I consider myself lucky to be with the Transports as I have not to carry my pack 
