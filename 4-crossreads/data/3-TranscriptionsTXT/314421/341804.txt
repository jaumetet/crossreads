 [Page 179] 
 French Pot Hook or Kitchen hook. [Sketch follows] A. rod on which the serrated bar slides up &amp; down. B. Slotted bar with hook on lower end. C. Slot hinged on rod A &amp; through which serated bar B passes. D. Hinged end of rod A. E. E Guide for rod A. riveted to B. F. F Swivel with hook to hang on chimney bar. 
 Blequin Nov. 4 1917 
 Roulers, Menin, Langemark, Poelcapelle, Zonnebeke, St Julien, Hollebeke, Spret, Hannebeck, Houthulst Forest, Westroosebeck, Staden, Polygon Wood, Glencorte Wood, Polderhoek, Gheluvelt, Broodseinde, Passhendaele, Becekaere, Reutelbeck, Dadizelle, Oosthoek, Koybergmolen, Comines, Wervicq, Warnetou, Dixmude. 
