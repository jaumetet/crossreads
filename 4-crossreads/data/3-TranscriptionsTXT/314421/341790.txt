 [Page 165] 
  and is  He was in the 6 Division while in England. When it was broke up he was sent back to France with reinforcements. 
 Sunday Dec. 16. 1917.  1231 
 Cold &amp; frosty this morning with a sharp cold wind from the North. 
 Some enemy planes came over at midday but were quickly driven off. 
 I started to walk to Dranoutre to see Sylvester but I heard that his Battalion had moved into the lines so I turned back. 
 Monday Dec. 17. 1917.  1232. 
 The ground and everything this morning was enveloped in a mantle of snow. Light flakes of snow continued all the morning. 
 It was so cold we took all the horses &amp; mules out for exercise. 
 Yesterday I saw Sgt. Lear of Portland. He is in the 23 Battalion, &amp; is one of the original men. George Thomson of Bridgewater is in the same Battalion. 
 Tuesday Dec. 18. 1917.  1233. 
 Cold &amp; frosty. The ground is frozen. We took the horses out for exercise this morning. Fritz attempted to raid the lines yesterday evening, but was repulsed. We took some prisoners. We could hear the machine guns at the stables. I am on 1st picquet tonight, from 6 to 9. 
 Four of us were paraded today before Mr Boylan by the Sargeant 
