 [Page 189] 
 camouflaged = disguised reciprocal = Fulminate of Mercury. Boyland. 
 [Shorthand transcribed as follows] 2 letter posted in England No. 78, May 17 and number 80 May 28, 1917 
 Rifle No. 4/5088 
 October 26 letter 97, posted in England Sept. 24, sent a packet of serviette rings to my dear wife today. November 2, parcel and November 5,another parcel to my wife. December, a German water bottle from Fromelles.  [end of shorthand] [Transcribed by Gail Gormley, Judy Gimbert, Judy MacFarlan for the State Library of New South Wales] 
 