 [Page 167] 
 for being a few minutes late on first parade this morning. The charge was dismissed. 
 Wednesday Dec. 19. 1917.  1234. 
 Cold &amp; frosty this morning. 
 For some time I have been very itchey. I paraded to the Dr Collins this morning. He examined me &amp; said I had the scaveys and sent me to the Steinwerck hospital. I am going in in a limbre. 
 I went to the hospital after dinner to Steinwerck. Remained there for tea when I was sent away in an ambulance to a hospital  at  near Steinwerck and was put in the scabies ward. I had another tea &amp; was given 5 blankets and a hot drink at 7. p.m. 
 I had a letter from Willie yesterday dated Dec. 13. He is still at Norfolk hospital. 
 Thursday Dec. 20. 1917.  1235 
 There was a heavy fall of snow last night. It was bitterly cold &amp; frosty all day, and the whole landscape remained covered with snow all day. The snow froze on the branches of the trees. The food in this hospital is excellent, and well cooked. We get 3 good meals a day, arroroot for breakfast &amp; roast beef for dinner, sago for tea. In addition we get a cup of cocoa or coffee about 11 a.m. &amp; again at 7. p.m. I had a hot bath this evening. The doctor says that I have only a very slight attack of scavey. 
 [Shorthand transcribed as follows] I wrote to the Postal Orderly telling him not to send my mail away as I will be back soon.  [end of shorthand] Friday Dec. 21. 1917.  1236 
 A nice day. The ground is 
