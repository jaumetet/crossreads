 a6666002.html 
 (1) 
 From Surafend to Portsaid 
 Sept 17 th  - Nov 12 th  
  with   the   5 th    Signal   Troop  
 Surafend 
 17 th  Sept. 1918.&nbsp; Time 6 pm.&nbsp; A beautiful bright Palestine moon shines on a troop of dismounted horseman.&nbsp; They are hastily adjusting their accoutrements before moving out on  what proved to be  that memorable advance against the Turks when three Turkish Armies where anihilated in less than two weeks and which ended the  war  great war as far as Turkey was concerned. 
 &quot; Line   up   here   Sig   Troop &quot;!&nbsp; We lined up, about 40 strong.&nbsp; Four  ^  Tele  operators, farrier, limber drivers, visual signallers etc including one officer one sergeant &amp; 2 corporals. 
 &quot; Get   mounted &quot;! &nbsp; I place my foot in the stirrup and give a mighty heave.&nbsp; My foot strikes  it  my rifle butt on the other side and I assume a very unsoldierlike &amp; undignified position across the old mares rump.&nbsp; She doesnt take 