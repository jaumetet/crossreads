 [Page 7] 
 Dear old Mick, 
 Many thanks for your letter which I duly received but which I had not time to answer before I left [indecipherable].  I must also thank you for the Varsity results and also for the swastika.  I appreciate it all the more, knowing it to be a gift from our dear little Mater. 
 Remember me to Decent &amp; Miss Cohen &amp; explain to them the impossibility of me writing to them, but give all my friends my best wishes &amp; ask them to write to me. 
 Have not received a "Torchbearer" as yet.  Will you also try &amp; get me a "Hermes" &amp; send it along. 
 Best love to the Jenkos, Greigs &amp; Jones, also to Callie &amp; Nan.  Will write to them as soon as I can. 
 Ever your affectionate brother Eric 