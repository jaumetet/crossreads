 [Page 34] 
 April. Saturday, 1. Noyables, Pompe &agrave; Incendie, Fort Mahan, Steenbecque, some of the stations we passed through. We disentrained at Thiennes. Major Dean in charge. Rode on gun limber. Had to go four miles. After fixing up horses had tea. Got to bed in an old farm---hay loft---at 10.30pm. 
 Sunday, 2. First morning in Flanders. We purchased seven loaves of bread and some butter for 13 francs. 
 Left about 11.am, but could not find B.A.C. lines until 12.30. Country pretty. Quaint estaminets. 
 Monday, 3. Was roused at by Lt. Bryan the O.C. for coming back late from 18th Battery. No room provided for us here. Told to sleep under a gun. Everything sloppy. Picturesque village of Blaringhem. Roused at by Bryan for being too reserved and quiet and not bullying the men more etc. The rotter! Let him wait! 
 Orderly Sergeant. Slept in Saddler's "Shop". Worked with Lt. McDonald at 8.pm. 
 Tuesday, 4. Reveille 6.am. Fall in 6.30. Boot and saddle 8.45. Marching order parade---all teams out 9.am. Dismissed at 12.30. Boot and saddle 1.45. Marching order parade 2.pm. Dismissed 5.30pm. Worked with Lt. McDonald 8.pm. First Green Envelope issue. 
 Wednesday, 5. Usual routine. Went out with teams this morning. I have a nice little bay pony for my hack. Findley, Wise, Jackson, Quarrel and I sampled the champagne at one of the estaminets. Water and non-intoxicants practically non obtainable. 
