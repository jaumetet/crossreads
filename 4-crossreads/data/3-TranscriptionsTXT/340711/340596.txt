 [Page 9] 
 lying prone, with a plate near their mouth and getting it down somehow. I found some satisfaction in anathematising the Kaiser each time I found it expedient to visit the side of the ship. My impression at the end of the day was that I had "Gott strafed der Kaiser" about 84 times. Anyhow it kept me cheerful---and one or two of  les malades  around me. 
 Sunday, 30th: Feeling better, though still groggy. Eating well. Whales spouting. Cloudy skies; port holes closed. Church Parade---but did not risk moving from saloon deck. Brought my hammock up on deck and slept there---7 p.m. 
 Monday 31st: Read London's "Martin Eden". Holiday---Foundation of Aust. Anniversary. Rough weather crossing the Bight. Big liner (supposed to be Troopship "Wiltshire" inward bound) sighted. Bed 7.30 p.m. 
 February. Tuesday 1st: Everyone Feeling groggy. Boat pitching heavily. Do not feel too good, can scarcely write a line. I spend most of my time with Basil Urban, who was next me in hospital at Maribyrnong. He is a fine youngster---I always seem to feel older than he somehow. We seem to hit it without any effort. 
 Concert in evening. Sang "My Ain Folk". Bed 8.30 p.m. 
 Wednesday, 2nd: Land sighted starboard, 20 miles away. 
