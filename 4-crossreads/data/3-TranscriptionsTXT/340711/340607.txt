 [Page 20] 
 Tuesday, 22nd: Drill (Signalling). Entered Hell's Gate during early hours of morning. Passed several steamers during same period. 
 9 a.m. We passed the rocks known as "The Twelve Apostles". By 6 p.m. we had overhauled 4 steamers. We were doing about 15 or 16 knots. 
 Lecture at 3.30 p.m. by Senior M.O. on V.D. and caution re Egyptian women. Did a copy of "Entrance to Aden". It took just an hour. Saw six vessels at same time all going south. Correspondence. 
 Wednesday, 23rd: Received 300 copies of "Boys of Australia's Clime" from printer. Sgt Fenton and I sold them all within an hour. Could not get any more from printer---a rotter he was too---charged 20/ and we provided the paper. I offered to give the whole of the profits to the ship's band too. 
 Passed the "Ceramic", going south, at 8 p.m. She looked very fine with all her lights; we were very close to her too. Saw 8 or 9 vessels during day. 
 Played "500" in evening with Basil, Bozzy and Tommy Mower and then wrote letters. 
 Have seen no land all day. 
 Thursday, 24th: Correspondence in morning. Land in sight both sides, within short distance. Concert in evening. 
