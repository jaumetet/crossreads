 [Page 17] 
 a glass of Indian pale ale with the rest of the fellows---but did not particularly fancy it. 
 We marched back to ship by a different route and arrived there at 7.15 p.m. The sea was very rough, and it was a matter of danger as well as difficulty to get from the barge to the gangway. The rise and fall of the sea even at the vessel's side was five feet---with little more than a second between the maximum rise and the swift rush of the fall. It took until 8.30 p.m. to get the last man off. Many were absolutely prostrate when they got on board, while several were nearly drowned and one of the Officers almost had his leg crushed to pulp. 
 Monday 14th: Steamed out of Colombo at 7 p.m. Things quiet all day. Land soon out of sight. Slept on boat deck. 
 Tuesday, 15th: Heavy tropical rain at 5.30 a.m. We had to rush for cover. No land in sight. Heading due N.W. 
 Wednesday, 16th: Second inoculation or rather the completion of the first. It was a double dose this time, but the after effects were not anywhere near as severe as those of the first dose. No land in sight. 
 Thursday, 17th: Genuine alarm in the afternoon. We had already had one practice lifeboat alarm. This time there was a cause however, and there were no rumours round beforehand. A vessel was sighted on the horizon right 
