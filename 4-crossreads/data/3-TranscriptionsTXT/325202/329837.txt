 [Page 76] 
 for a great push against Bapaume as soon as we have a spell of dry weather. 
 I am sleeping in a canvas hut &ndash; some of the men are living in  an  old German Dugouts on the door of one of which is still hanging the sign "Gott strafe England" 
 Nov 8/16 Have been taking rations up each day to the dump at Longueval &amp; Montauban &ndash; a good deal of shell fire at the cross Roads near the Dump. Roads blocked with traffic &amp; in bad condition 
 Yesterday left at 8 a.m. &amp; got back at 5 p.m. on the road all the time. 9 hours doing about 6 miles. Owing to them shelling 