 [Page 113] 
 gas helmets but the gas did not reach us. 
 Aug 6 (Hondeghem near Hazebrouck) came here two days ago to join up with the Div Train Headquarters about 16 miles on the road raining the whole way 
 Aug 9 Vieux Berquin I reported last evening to D.H.Q.  to move  at Hazebrouck to move off with them this morning We were awakened by a visiting Hun dropping Bombs on the town and by our anti-aircraft guns 
 This morning as we were getting ready to move off The Germans started shelling the town with 