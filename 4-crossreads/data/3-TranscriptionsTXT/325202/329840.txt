 [Page 79] 
 to the winter at all. 
 Nov 26 (Sunday) Went into Amiens 10 miles away yesterday &ndash; A very beautiful Cathedral but the facade &amp; the interior woodwork is all sandbagged up as a precaution against air-raids nothing else of interest. 
 Just a good provincial town of 100,000 inhabitants at present filled with British soldiers &ndash; specially officers &ndash; Prices exhorbitant was charged 1 franc for cup of cafe au lait at a middle class confectioners. 
 Dec 5 Ribemont we arrived back here on Dec 1. a terribly cold 