 [Page 134] 
 apples &amp; cider  &amp; as  
 It has been a pleasure to give them an occasional hand with the farm work which like all farms in France suffers from want of labour. The farmer is of military age but by one of those practical French Laws &ndash; because he has six young children he is exempt from military service 
 The Division is going into the line again &amp; I leave tomorrow morning for the trek 
 Dec 18/17 Dranoutre arrived today after 4 days hard trek from Samer &ndash; bitterly cold with sleet &amp; freezing hard. The mild 