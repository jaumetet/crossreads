 [Page 104] 
 a bit of a cheer. 
 The people in these little village have never had such a shaking up as this war. It is like some irresistible force to them   to have some  for a sleepy hollow of a place  to ha  with perhaps a couple of hundred inhabitants to suddenly have 1500 rollicking soldiery billetted on them. They live half way between fright &amp; laughter 
 The other night I saw a 'fresh' soldier in the crowded room of an estaminet juggling with 2 empty beer bottles &amp; 2 glass tumblers the faces of Madame &amp; her daughter were a picture 