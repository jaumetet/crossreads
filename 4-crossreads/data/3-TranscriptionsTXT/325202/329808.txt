 [Page 47] 
 parts of the building had the date 1747 on it. But the main building must have been much older 
 In all the sights it has survived there cannot have been a more undreamt of one than to be surrounded by Australian Soldiers horse, guns &amp; foot billetted in the cottages of the chateau 
 July 18 (Tuesday) Varennes arrived here this afternoon at 4 p.m having come today from Val de Maison a distance of 11 miles where we had stopped 2 days when it rained the whole time. Our waggons are pulled in alongside the village street &ndash; The horses in a paddock 