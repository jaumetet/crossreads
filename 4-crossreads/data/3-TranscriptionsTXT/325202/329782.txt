 [Page 21] 
 had been &amp; took up my share of coal on the right aisle &amp; the wood from the left aisle 
 While in there waiting to be loaded saw an exciting aeroplane action nearly overhead. A German plane which our "anti-guns" were firing at for all they were worth &amp; at the same time two of our planes pursuing it. 
 We heard a machine gun rattle overhead but couldn't see the finish as they got in between us &amp; the sun. 
 A lovely day &ndash; one could almost see the crops &amp; the hedges growing in the warm sunshine after all the rain we have had 