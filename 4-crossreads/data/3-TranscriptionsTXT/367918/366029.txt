 [Page 65] 
 At Sea 5.8.19 
 My dear Judge, The job is very nearly over. We are due at Melbourne tomorrow afternoon. How long we shall be there I do not know but we should leave at the latest on Thursday morning and should arrive in Sydney well within the 48 hours. 
 The trip has been a very good one but frightfully tedious and I am dying to have it finished and get home again. 
 Best Love Yours affectionately C.R. Lucas 