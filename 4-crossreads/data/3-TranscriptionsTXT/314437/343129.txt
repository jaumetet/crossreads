 [Page 5] 
  about  the same time with Clery as objective.  The Australians ran into him at the same time.  The prisoner said his was the best regiment in the German Army, having served on all fronts.  He also said he had been wounded by machine gun fire, by his own men.  He added this, too, "I never met any men to come up to the Australians for initiative.  The French are next, but they lack the dash of the Australians*". 
 This Fritz had obtained leave to go to Berlin on 1st September, and was very angry about his bad luck. 
 F.J. Brewer 
 * Corporal Edwards assured me these were the exact words.  F.J.B. 
 [Transcribed by Judy Gimbert for the State Library of New South Wales] 
 