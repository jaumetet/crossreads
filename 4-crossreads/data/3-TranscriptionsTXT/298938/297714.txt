 [Page 23] 
 work with of that, more anon. 
 Saturday 7th October Have had another week-end in London since I wrote last in this book. Saw Rushy at the School on the Monday morning and chatted with several masters, including Nixon, McNair, Hope &amp; Kingdom. Paid 7/-, the subscription for 2 years Elizabethan membership. Was not exceedingly pleased with reception which was hardly enthusiastic. Possibly because I have not risen above a Pte, or for other reasons. Since my return have worked 
