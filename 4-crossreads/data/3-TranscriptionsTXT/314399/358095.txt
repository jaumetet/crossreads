 [Page 124] 
 Tuesday 8 April 1919 Missed parade again this morning. Am on special patrol today &amp; no job tomorrow. Met Garry &amp; took him round to see different places. Bill came in &amp; we had dinner at a little french cafe. Had lobster. Goodo. 
 Wednesday 9 April 1919 Bill &amp; I visited St Addresse [Adresse]. Got the train home OK. Got my negs &amp; prints. Quite good. 