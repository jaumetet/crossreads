 [Page 78] 
  To Cairo Tramways Co  Please pass 1 military passengers over trams from Kasr-el-Nil to Heliopolis on military duty free. G. Masefield 2 Lt. Adjutant Kasr-el-Nil Garrison 21st Battalion AIF 14th August 1915 
 [Transcribed by Eric Hetherington for the State Library of New South Wales] 
 