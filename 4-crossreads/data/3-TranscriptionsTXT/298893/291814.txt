 [Page 37] 
 19th Batt Popes Post 2-12-15 
 Capt. Hale, Will you please arrange for 19th Batt to draw the following from Anzac tonight. 144 Garland Bombs complete with Powder friction tubes etc. 144  6 second English Improved Match headed C.B. Bombs. 50  5 or 6 second fuses. 50  No 8 or No 6 Detonators A Gray RSM 
