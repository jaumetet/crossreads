 [Page 25] 
 a new style of paybook in which both our debit &amp; credit accounts are shown to date. Y.M.C.A. hut in evening. 
 Feb 27 &ndash; Mch 1 
 Usual drill. We were also issued with razor, Jack-knife &amp; lanyard, flannel belts etc. which now makes our equipment complete. 
 Mch 2 
 I was detailed to-day with a party to do a few hours pick &amp; shovel work &amp; at this strenuous occupation filled in most of the day. 
 Mch 3 
 Usual Saturday's route march after which we underwent a feet inspection by our officers. 
 Mch 4 
 I was detailed as a member of a fatigue party this morning &amp; instead of attending Church Parade we were marched to head quarters to receive instructions &amp; to our gratification found that there was nothing to do. Alan &amp; I then went for a walk to the A.I.F. 