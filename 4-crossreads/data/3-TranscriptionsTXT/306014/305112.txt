 [Page 35] 
 Mch 14 
 "Bull Ring" again especially bayonet exercises &amp; open order work as under shell &amp; musketry fire. 
 Mch 15-16 
 "Bull Ring" &amp; guard 
 Mch 17 
 Usual Saturday's route march in morning. Walked with Les Dimond &amp; his friend to Orcheston in afternoon &amp; there we bought a couple of bags of fancy biscuits &amp; sat down on the roadside &amp; shared them with three wee boys whom we met there. We had tea in Shrewton &amp; strolled back early in the evening. 
 Mch 18 
 Church Parade in the morning. After dinner I went alone for a stroll across the hills. The afternoon was glorious. The sun forgot his bashfulness, and ventured once more to brighten the hills with his brightness. Gloominess was ushered from the landscape &amp; the very grass of the fields &amp; wayside &amp; the trees of the valleys showed their gladness in their faces. How could 