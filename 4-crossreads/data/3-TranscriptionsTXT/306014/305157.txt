 [Page 80] 
 our march, but a good tea was soon ready for us &amp; we were in bed in an hour or so. 
 This camp is a big one, and is very compact. We are all under canvas to the extent of 16 per tent &amp; find this sort of home fairly comfortable although all our feet are in a heap at night just like a bunch of turnips, or something like that. 
 April 27 
 We pararded for our service rifles and bayonets this morning and were then inspected by the Camp Officers. We also got an issue of two helmets, one for gas-attacks, the other as a precaution against shrapnel. This latter is made of steel &amp; is worn here on all parades &amp; at all times in the firing-line. 
 A dental &amp; medical parade occupied the rest of the time. 
 The training as a rule covers a period of ten working days and is styled "intensive" and is to a large extent recapitulatory. 