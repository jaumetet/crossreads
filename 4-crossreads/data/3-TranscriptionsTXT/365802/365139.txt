 [Page 50] 
 My dear old dad, It does not need the eye of a son to read the pain that underlies every word of your letter, and I too wish I knew how to say half the things that are in my heart. The sacrifice that you and mother have made so bravely has given me something to try and live up to, and I realize to the full what that means. Wherever I may be, you and mother will be with me always in thought, and your example govern my actions. 
 There is no need to tell you that I have always looked up to you as my ideal, and nothing in the world can ever change that 
 Your affectionate son Keith. 