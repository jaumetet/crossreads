 [Page 258] 
 The Mayor of the town who is now with me has formally handed over the town of Raboul which includes the mountaneous country on the north to &quot;Tonanumbatir&quot;(1708 ft). I am in touch with my advance Guard 1 mile North East of town and 1/2 Coy&nbsp;along the coast road north of native hospital. Guards have been posted at Post Office Govt. Buildings (2) mayors house and New Guinea Cos. Stores where the main Body&nbsp;is in bivouac. also a section on South Side of town. 
  Signature  J Paton 
 OC Raboul Garrison 
  From  Gov Office 
  Place  300yds NE of Wharf 
 7.55pm 
 To Brigadier 
