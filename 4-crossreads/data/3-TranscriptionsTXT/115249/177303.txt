 [Page 6] 
 &nbsp; 
 11 9 4&nbsp; 
 Brigadier to O.C. Infantry Herbertshoe 
 If your operations do not  end clear up  conclude the action  situation  before dark. The Ridge between Herbertshoe and Kaba Kaul will be shelled at daylight by fleet the remainder of Berrima Force will land at Kaba Kaul soon after daylight tomorrow and combined attack made from Kaba Kaul and Herbertshoe when shelling ceases. &nbsp;Reports to Berrima tonight. 
 Brigadier&nbsp; 
