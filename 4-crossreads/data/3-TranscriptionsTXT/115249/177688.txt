 From Commander Beresford, per &quot;Warrego&quot; 
 To - Flagship. 
  12th Septr. 14  
 (4-55 p.m.) 
 I have received information from Lieut. Bond that he has captured and is in possession of the wireless station, the machinery of which is in perfect order, including duplicate reserve sets, but the towers have been destroyed, the angle irons having broken in falling. 
