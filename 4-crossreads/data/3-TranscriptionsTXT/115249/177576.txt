 [Page 279] 
 W.T. signal 
 Receiving Ship: Berrima 
 Transmitting Ship: Protector 
 Date: 14 Sep 
 Time: 5.47 
 Colonel Watson to Brigadier and Encounter 
 Began minor march on Toma at five am with four Companies and machine gun (stop) Land your machine gun by four am complete (stop) Sketch captured shows enemy occupying position his right on MATAWTA to bend of road near NATORA with post at GIRE GIRE (stop) 
 Will you by vigorously&nbsp;shelling ridge and plantations sweep positions on map at daylight (stop) My troops will not move South from Coast road until shelling ceases. 
