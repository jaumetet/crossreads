 [Page 104] 
 Naval Signal 
 From - Berrima 
 To - Flag 
 Date - 13 9 4 
 Time 6 20 am 
 From Brigadier - As you have no doubt reported successful occupation of Herbertshohe and Rabaul and the other steps taken to date and furnished list of killed and wounded nothing further is I think required to be sent by wireless except that all are well no sickness whatever. &nbsp;I will send despatch by mail today. 
