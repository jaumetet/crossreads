 [Page 187] 
 Naval Signal 
 From - VA 
 To - Administrator 
 Date - 23/9/14 
 Time - 8.50 
 I should like to see you on important business regarding a message received last night. &nbsp;Will send boat for you when squadron stops at 9.30 pm. 
 0831 
 Brigade Major 
