 [Page 158] 
 Naval Signal 
 From - Berrima 
 To - Flag 
 Date - 13 9 4 
 Reply from Brigadier to R A C 
 My cyclist&nbsp;orderly just returned from Herbertshohe states he heard some firing on his right while returning. &nbsp;He was also informed that party had been sent by Beresford towards WT station&nbsp;to bring in a motor lorry &amp; that  the party returned reporting that it had been fired upon&nbsp; &nbsp;a messenger returning from this party reporting they had been fired on no other reason for troops being at Kabakaul 
