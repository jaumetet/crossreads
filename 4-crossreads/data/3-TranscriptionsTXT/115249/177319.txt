 [Page 22] 
 W.T.Signal 
 Receiving Ship - Berrima 
 Transmitting Ship - Warrego 
 Date - 11.9.14 
 Time of Despatch - [illegible, possibly 5.32pm] 
 Comdr to Brigadier 
 My information was previous to Comdr Beresford. 
 50 men with a flag of truce conducted by a captured German have proceeded to the WT station with the view&nbsp;for negotiation with the officers there for an unconditional surrender. 
 I am sending about 30 black prisoners&nbsp;&amp; 2 white under guard to Australia now. 
 Comdr Beresford proposes camping about three quarters of a mile inland from the pier awaiting result of negotiations @ 1715 
