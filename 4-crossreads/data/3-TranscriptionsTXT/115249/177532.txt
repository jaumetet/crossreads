 [Page 235] 
 Naval Signal 
 From - Flag 
 To - Berrima&nbsp; 
 Date - 12-9-14 
 Time - 4.25 
 Signalling was observed last night from mount Towanunbati the high peak to North of Rabaul. &nbsp;Arms and ammunition found in the lighters in port have been removed. &nbsp;The lighters containing coal are moored between the piers. &nbsp;A kite apparently fitted for wireless purposes was sent up this morning close to sheds flying Japanese flags bearing North. &nbsp;The kite in question appeared to be 50 feet long and 8 men were employed in handling it (1618)&nbsp; 
