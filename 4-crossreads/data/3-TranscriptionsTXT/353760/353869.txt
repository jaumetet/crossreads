 [Page 103] 
 March 3 198. Mails in yesterday:  early directly after breakfast two Muston letters, one long dear chronicle of home from Mother of Dec. 16, and one from Billy, same date. 
 Betty's reminded me of the paragraph of Guy &amp; Pauline &ndash; "Pauline put the letter in its crackling envelope with a sigh for the unformed hand in which it was written.  Nothing brought home to her so nearly as this hand-writing of hers the muddle she was always apt to make of things.  How it sprawled across the page, so unlike Monica's that was small and neat and exquisitely formed or Margaret's that was decorated with fantastic and beautiful affectations of manner." 
 The lace Handkerchief from Poperinghe and "The Letters of a Soldier" arrived safely &ndash; and already Mother has read and loves them.  They are lovelier than all Russkin's prose, and as lovely as parts of Pater. 
 From Davis &amp; Oriole "The Father of The Forest", W. Watson's &ndash; with one lovely Lyric.  Later in the day arrived letter from T.B.C. &ndash; note on Drinkwater's 1908-14 being despatched and Stephen's "Reincarnations" not being yet published.  From Poetry Bookshop word of "Trees" being despatched and report of Chapbooks &ndash; 
 "The Old Ships", 4/- Spring Morning, 2/- Farmer's Bride, 2/- Children of Love, 1/- Antwerp, 3/- Strange Meetings, 1/- 
 Davis &amp; Oriole saving me "Cadences" &ndash; 2/6. 
 March 4 199. Up today, saw Sheppherd drawing for G.H.Q., good drawing but hard. 
 200. Printed out pages 4, 5, 6 and 7 of 3rd number of Gheitz:  under "Digger", Capt. Barbour sent in good account of Rouen Team's trip and comments.\ 
 Mail.  Letter of Jan. 3rd, Betty, from Bank and T.B.C. of renewal of T.L.S. subscription.  Later arrived Munro's "Trees", a beautiful book with some fine woodcut drawings, bound in black and well printed. 
 Harold L. in today. 
 Rain. 
