 [Page 121] 
 March 19 Kemmel M.D.S. 232. Arrived Bailleul at 5 and back to M.D.S. &ndash; being shell'd H.V. about 7 p.m. 
 For some days the Boche has been shelling Kemmel, and yesterday morning blew up our Pack Store, only wounding Johnno, who was in it, &amp; trussing Capt. Barbour, also in it.  Library blown to the four winds. 
 As we arrived last night at intervals shells were falling, super whiz bangs. 
 Bailleul has suffered too, we seeing as we passed through several houses completely demolished.  Last night's nearest shell was by the corner of the old Mess Hut. 
 Big batch of "Gheitz" Musick has arrived while I was away:  some fine stuff amongst it, and a book of English Folk Songs by Vaughan Williams. 
 Mail awaiting &ndash; two home letters of Christmas day, one splendid note from Mother, and a parcel of Davis &amp; Oriole books:  A.C. Benson's "Walter Pater", Flint's "Cadences", Verhaeren's "Belgium's Agony" and a book of Aubrey de Vere's Poetry, also packet of sweets and gloves from Mat. 
 233. Reconstruction of Sunday.  Up at 3.30 a.m. and to Place de la Concorde, left there at 7, to Gare d'Lyons and train to Fontainebleau.  Arrived there at 10, to the Palace. 
 A splendour of Architectural Ceilings and room designing far lovelier than Versailles.  The Palace buildings and design appealing before Versailles.  The afternoon through the Bois on wagons, and an enjoyed drive. 
 Back to Paris by 6 to the Hotel, and a settling of bills and tips, and to the Canadian Y.M. hut for supper.  Train from Gare du Nord at 11.40 and a long travelling to Calais arriving there at Midday.  Train to Hazebrouck and changing on to Bailleul. 
 So home, to a hot corner and sleeping in cellars, a llistening to sudden tearing sways and krumpings. 
