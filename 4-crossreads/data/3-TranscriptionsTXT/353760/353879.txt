 [Page 113] 
 March 13, 14 215. 8.30 a.m.  The talk at the Opera last night of big casualties during the air raid, 60 are said to have been killed at one place, and a rumour too, of two spys having been caught giving signals to the raiders by the Office of the Minister for the Interior, where several bombs dropped.  At the Madelaine and the St. Agustine, heavy black and silver drapings were over doors and altars.  We were in the Madelaine and great mourning clothes covered half the church, all the altars' candles glimmered, all lights were glowing and in a forest of steady tapers high on its pedestal draped was a coffin, leaving a minute later another coffin was being carried up the steps, covered and accompanied by great massed grouping of violets, roses and tulips.  Were they air raid victims? 
 [Comment in margin.]  Over 100 killed in raid, and a big number wounded. 
 March 14 216. Yesterday:  To Versailles, a good trip, but disappointment.  The legend of Versailles broken, the imagined beauties of interiors lost in an orgie of gold and painted ceilings. 
 Half of the Palaces closed, and the bronzes of the fountains and walks sandbagged against raids, and tapestries leaving bare dull walls carrying overwhelming ceilings.  No time for the seeing of the Petit Trianon:  by photos the loveliest gem of all.  Sunday we are for Fountainbleu &ndash; and I'm looking forward to an appreciation I did not feel for Versailles.  There almost a burst of gold, and a coldness of unnatural affectation left only the impression of a wounder at the period that had produced it. 
 The Louvre remains the loveliest building of my love. 
 Night to the Alhambra &ndash; an empty theatre and a charming drawing by Poulbot at the back of the programme. 
 217. Today for Paris No. 2 Trip. 
 Back at Hotel after a second trip, to Notre Dame, Musee de Cluny, and the Parthenon, three wounderful experiences but spoilt by shortness of time. 
 The Notre Dame a beautiful cathedral, less full and 
