 [Page 96] 
 few days you can guess we might easily have more comfortable quarters. 
 Still we are not doing too badly all things considered. Please give my warmest congratulations to Hazel and tell her I will write as soon as I can. I went a few weeks ago to see Olly but he had just gone away sick &amp; so I missed him. 
 Meantime, kindest regards to all at home and love to Aunt Carrie. 
 Yours sincerely Harold W. Fry. 