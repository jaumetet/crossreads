 [Page 167] 
 Waterloo Belgium 4th April 19. 
 Dear Mr Fry, Have just been on a flying visit to Brussels, Antwerp and Cologne and stopped at the field of Waterloo on the way back to Charleroi. Thought you might be interested in this card with the Waterloo postal stamp. Will write soon and hope to be back in Australia shortly now. My love to all, 
 Yours ever Ellis Troughton 
 Mr Arthur Fry "Denegully" Northcote Road Lindfield New South Wales Australia 