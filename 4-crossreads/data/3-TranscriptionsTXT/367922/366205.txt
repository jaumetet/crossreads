 [Page 88] 
 Replied to 5-5-17. 
 "Arcadia" O'Brien Street Bondi 1st May, 1917. 
 Dear Mr. Fry, It is with very genuine sorrow that I have learnt of your deep trouble. The weight of it must be almost beyond endurance, especially after the grief you have already had to bear. One can only trust that time will soften the pain, and that you will be able to find comfort in the thought of your boys' nobility both in life and death and the knowledge that they have 