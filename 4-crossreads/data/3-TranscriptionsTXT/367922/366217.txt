 [Page 100] 
 I derive considerable pleasure from the fact that it was partly due to my constant advice, which finally led him to consider doing the full University course 
 Words cannot express my sorrow that one, so full of brilliance and promise was not spared to carry on his work. It may be some comfort to you and your family &ndash; in this time of great sorrow, to know that he will live on at least in our memory. 
 With deepest sympathy with you and your family 
 I am Yours very sincerely Sydney George No [indecipherable] 
 Mr. George's letter re Dene 