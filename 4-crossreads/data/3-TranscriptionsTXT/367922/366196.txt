 [Page 79] 
 But now I can only offer you &amp; his Mother the sympathy of another father and another who knows what you are suffering and feel for you very deeply and sincerely. 
 Yours very truly David Ferguson. 