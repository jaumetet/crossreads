 [Page 25] 
 not have any reason to complain of baldness, occasioned by fellows passing over your head &ndash; 
 I shall always be glad to hear from you and if there is any thing you think I can do for you in England let me know, and I'll promise you I'll try. 
 Give  my  our united love to your Mother Father your Brothers &amp; Sisters and accept the same yourself from 
 Your ever aff. Uncle John Hall 
 Aunt would have written your Mother, but her hands &amp; shoulders are so painful she cannot, she sends her love to Edith, and begs me to say she is very glad that she is not going to be a Fijian, my own opinion is, that  Loviers  are like wasps, for whenever you kill one, another comes buzzing about 