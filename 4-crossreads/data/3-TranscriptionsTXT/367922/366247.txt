 [Page 130] 
 5/3/18 
 Dear Sir, Could you give me a few details about your son Dene Fry for inclusion in our Presidential Address which is to be read on the 25th inst? What I should most like would be detail of his scientific work, and also details of his record after enlistment, and any other details you may think fit. If you could give me this I would be able to compile something suitable for our President. 
 Thanking you in anticipation 
 I am, Yours faithfully, A. B. Walkom, Hon Secretary 
 Mr. A. Fry, Lindfield 
 (Answered 9 March &amp; list of work sent) 
 [Page 131] "Denegully", Northcote Road, Lindfield, 9th. March, 1918. 
 Dear Sir, In reply to your letter of the 5th. instant, I beg to enclose a memorandum showing the principal facts of Dene's career, from which I hope you will get the information which you require. I thought it best to make it rather full, leaving it to yourself to select such portions as may suit your purpose. Please use it as you deem fit, and if there is any further detail you would like to have I shall be glad to do anything I can. 
 I enclose list of his scientific publications so far as there is any trace here, in his home. Possibly, there may be others known of at the Museum, and if so I will make inquiry and let you know. 
 He wrote some beautiful letters to his family, but of course I cannot give them all, and he himself told us that he was precluded by the Censor from telling us much that he would have liked. He said : "It must wait till I come home." Dear lad, we can never hear those things now. The world as well as ourselves is loser, for he looked at facts very perspicaciously and his views on many things would have been valuable. 
 I send you under separate cover a copy of the Sydney University Magazine "Hermes," on which page 229 you will find a notice of him by Dr. S. J. Johnston who was acting Professor of Zoology in Dene's time, and has now succeeded Professor Haswell in the permanent position. 
 A. B. Walkom, Esq., Hon Secretary Royal Society of Queensland, Brsbane. 