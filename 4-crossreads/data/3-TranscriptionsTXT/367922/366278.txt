 [Page 161] 
 Always use military address &ndash; 17836 4th Austr. Field Ambulance A.I.F. 
 Florennes near Charleroi Belgium 10th Feb. 1919. 
 To Arthur Fry, Esq. "Denegully" Northcote Rd., Lindfield, N. S. Wales. 
 My dear Friend, Such a fine, welcome letter from you, dated 26th Oct., reached me at the beginning of the month, bringing with it much interest and the comfort of the kind thoughts and good wishes of all at Denegully. 
 You are most generous to write my undeserving self, dull and bad correspondent that I am, such helpful letters and then say "not to feel obliged to reply". But replying is a great pleasure indeed, the only doubt being as to whether I can write a  worthy  letter worthy of your interesting ones. 
 With reference to the signing of the Armistice, which must have been an intense relief to you, it has left a kind of reaction behind it which makes the slowness of repatriation, unavoidable no doubt, very irksome. 
 Recently, on Church Parade, some prayers issued by the Chaplain-General were read and one, with beautiful understanding, was for those "for whom bereavement had dimmed the joy of victory" and my thoughts were with you dear friends at once, as they have been so often, before and since. 
 Though so thankful for everybodies sake, the signing of 