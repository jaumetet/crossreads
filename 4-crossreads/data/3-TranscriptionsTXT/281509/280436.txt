 [Page 226] 
 1916 August 1 Tuesday 
 Up in time for breakfast. Get a hair cut &amp; go on route march this afternoon. battle continues on front but not with former intensity. Dawdle around village in evening. These evenings are beautifully mild &amp; balmy. Summer has been ushered in in earnest. The wheat fields are rapidly ripening the scenes from the hills are both varied &amp; lovely. If only the grim spectre of war did not continually overhang all what a delightful time it would be 
