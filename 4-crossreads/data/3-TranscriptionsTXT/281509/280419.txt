 [Page 209] 
 15 July 1916 Saturday 
 Another good night on hay despite the old lady &amp; orders not to use it for bedding. Start route march with light pack up, in morning. Still the flowers. They are everywhere. the beauty of them is beyond description. Bob up for short while before dinner. I take him down a letter after dinner &amp; later after tea take him some more. His transfer still in abeyance. We get new issue of gas helmets. 
