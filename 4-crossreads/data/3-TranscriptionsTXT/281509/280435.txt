 [Page 225] 
 31 July 1916 Monday 
 Rest all day &amp; write some letters. Receive old letters in bunch dated Feb &amp; March. Post my home letter dealing with our late fight also note to A.G. Bob down to see us.   paid today.   Lash out with tinned fruit, chocs. &amp; biscuits.   He is sending home joint cable. 
 Hostile Tauabes pass overhead at lunch time. Nose caps from anti-aircraft guns drop perilously close. 
