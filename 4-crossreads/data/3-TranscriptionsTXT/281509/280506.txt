 [Page 296] 
 1916 October 10 Tuesday 
 Ditto.  Rumour confirmed about us moving. Leaving at end of week for St. Omer &ndash; for short course of training. 1st Div. &amp; then into action somewhere (Somme probably) 
