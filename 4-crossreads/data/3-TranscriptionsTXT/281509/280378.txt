 [Page 168] 
 1916 June 4 Sunday 
 Our squad to Bull Ring in morning for instruction in Goat First Aid.  Went for walk up to Headquarters P.O.  Nearly mulcted of 10 francs only for Bob's intervention . Meet Cartwright &amp; Nettleton. &amp; we stroll thro' Tommies' Camp  Well appointed. Anzacs barred from all refreshment &amp; recreation rooms.  Back, to tea at our own Y.M.C.A  Church after.  Hospital train &amp; wounded 
