 [Page 72] 
 
 From December 1917 to April 1918 inclusive, an area of 1,500 square miles of country was completely photographed for mapping purposes, which work was carried out practically entirely by the Australian Squadron and constitutes a remarkable performance. 
 Throughout the period of my command, I have had the pleasure of conveying to the Squadron special messages of congratulation too numerous to mention in detail, including messages from the G.O.C., C-in-C, G.O.C. Middle East, R.F.C. and the G.O.'s C. the formations with which the Squadron has from time to time been co-operating and I am extremely proud of having had under my command No. 1 Squadron, A.F.C., whose record is one of magnificent achievement and worthy of the very highest praise. 
 I have the honour to be, Sir, Your obedient servant, (Signed) A.E. Borton, Brigadier General, Commanding Palestine Brigade, Royal Air Force. 
 In the Field 16/5/1918 