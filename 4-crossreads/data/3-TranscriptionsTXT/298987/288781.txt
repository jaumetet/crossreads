 [Page 131] 
 Friday 12th Paraded in morning to give in Overseas Kit &amp; draw UK Kit afternoon C.O. inspection etc 
 Saturday 13th Messed about all morning Got a weekend Pass Went to Sutton V &ndash; picked up Hall &amp; went to Pictures. Had Poached Eggs on toast at Weslean Hut Stayed in Training Camp for night 
 Sunday 14th Stayed at Training Camp all day a good deal of rain 
 Monday 15th Went on parade as usual but an Officer took about 20 of us to see the Draft that leaves tomorrow make a sham attack on a trench line &ndash; A Platoon in attack it is called &amp; is the latest idea for advancing with  a  few casulities as possible There are 2 Sections of 6 men &amp; 2 NCOs each with rifles &amp; some of the men have rifle grenades, &amp; 2 Lewis Gun teams of 9 