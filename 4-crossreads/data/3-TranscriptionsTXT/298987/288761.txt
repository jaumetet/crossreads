 [Page 111] 
 Thursday 9th Sun 12th May '18 Hand dressed every day doing nothing but reading &amp; loafing about. Think I forgot to mention that Quex Park was run entirly by women The Dr visited 3 or 4 times per week The only man was a deff &amp; dumb Belgan (been gassed early in the war) he got through a lot of work. The out of bed patients helped a good deal. 
 Monday 13th Nothing as usual wet day for a change 
 Tuesday 14 Was marked B.A.  to  as my hand is quite healed up Walked to Fovant &amp; back about 4 miles the longest walk I have had for some time 
 Wednesday 15 Dental inspection Had a tooth stopped 
 Thursday 16th Moved from Camp 5 No 1 Co to Camp 7 No 5 Co only a few hundred yds 
 Friday 17th Am on Massage &amp; R.C. Treatment (exersising 