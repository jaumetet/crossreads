 [Page 64] 
 hut &amp; 2 men are detailed each day as Mess Orderlies I am one of the unfortunates today &amp; it is not much of a job as I like haveing things reasonably clean &amp; there is no proper way to do it The table &amp; dishes are decent anyhow 
 Sunday 18th Detailed to go to Forrent on Working Party about 20 of us all told 
 Monday 19 Earlie Breckfest &amp; with less messing about than usual got away. Put in 2 hours at Wilton where we had to change trains arrived Forrent about 3 PM &amp; were soon fixed up with Hut blankets etc. A few men out of the 11th Reinforcement started for France today Pat Buchman one of the list went wish I was with him 
 Tuesday 20 Our job is cleaning &amp; makeing drains in this Camp what a comedown for fighting men 
 Wednesday 21st to Saturday 24th 