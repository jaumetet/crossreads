 [Page 173] 
 Friday, 27th November, 1914 Fearfully hot day, typical Red Sea, muggy &amp; close.  All engineers &amp; firemen knocked up. 
 Passed islands known as twelve apostles. 
 Men complaining of headaches, it is just stifling working 'tween decks.  On guard again, worst yet experienced, owing to heat. 
 Engineers absolutely beaten temperature below 135 in engine room. 
 Saturday, 28th November, 1914 Like a clap of thunder came the shock of the voyage to us.  We heard the sudden news that we were to disembark on Tuesday next at Alexandria, Egypt &amp; there complete our training at Cairo.  The men were dumbfounded as every man expected to be going home for Xmas.  Although it upsets all my plans I am rather glad we are going to Egypt it is a country I always wanted to see.  The col. made a speech at concert at night. 