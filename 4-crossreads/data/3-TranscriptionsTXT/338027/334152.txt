 [Page 48] 
 Sunday, 21 March, 1915 On guard all day on the front of the camp, wrote letters home &amp; to friends. 
 A swarm of locusts invaded the camp to-day it was a great sight they come in millions and are a terrible pest. 
 I think this is our last week here as we must move before long. 
 Monday, 22 March, 1915 Big review of the 1st N.Z &amp; A. division, a splendid turn out, we were inspected by The high commissioner Sir H. McMahon &amp; General Godley and his staff including French officers from the front.  After an inspection we all marched past in review order to salute.  It was a splendid sight &amp; did credit to our army.  The Col complimented us on our return to camp on our behaviour &amp; appearance we are considered the crack ambulance corp. 
 Half holiday in the p.m.  Went to O.'Rs again at night. 
 Four days bivouac &amp; march to Helouan tomorrow. 