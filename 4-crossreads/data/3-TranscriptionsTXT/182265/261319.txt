 [Page 5] 
 The Governemnt, led by Lieut Nelson (15th) contending that &quot;The White Australia Policy should be altered after the war.&quot; The Government was beaten by about 300 to 20, the leader of the opposition (Cpl. Champion, 13th Bn (a N.S.W teacher) becoming Premier for next week. He is to argue that &quot;State Governors &amp; Parliaments should be Abolished.&quot; 
 Some of last nights speeches were very interesting &amp; well delivered. There were speakers from every state. 
 This evening our Camp Pierrots give their first entertainment in YMCA Hall. 
 Entry Sunday  20/5/17  
 Raining this afternoon. I arranged to motor to Bath with Capt MacPherson (16). Lt Towers (16) &amp; Lt Thorpe (15) but car did not turn up. So we remained in mess &amp; spent an 