 [Page 31] 
 fine promenade pier, beautiful walks &amp; fine bathing facilities. 
 I spent the Sunday evening walking through the Gardens along the beach walks &amp; on the pier. 
 Yesterday morning (Monday) I walked the streets - bought a few small articles - then did the &quot;Circular Tally Ho&quot; drive - which takes one through Bournemouth into the country, along the River Stour &amp; back again in about 2 hours for 1/-. It was a beautiful drive &amp; thoroughly enjoyable. 
 Leaving Bournmouth at 2pm it began to rain heavily &amp; is still (Tuesday evening) very unsettled &amp; showery. 
 Arriving at Lyndhurst Road, an ambulance wagon brought in several officers - to the Camp. 
 Here we are in tents.&nbsp;Very bleak &amp; damp last night.&nbsp; 
 It promises to be wet again this evening. Am writing to Jess &amp;  home &nbsp; 
 Read &quot;The Drums of War&quot; last evening. - fairly 