 [Page 26] 
 The C.O. Capt Cox gave a lecture this evening to Officers &amp; NCOs - a very poor lecture but nice &amp; short.&nbsp; 
 I attended the Group Bomb Officers&#39; Conference at 5.15 this evening. 
 Two sergeants from 13th who were wounded at Bullecourt &amp; who are now convalescing at No4 Command Depot (Commanded by Col McGlynn) visited Marper &amp; myself this evening.&nbsp;I posted letters to Jess &amp; Merle.&nbsp; 
  Entry 16th July  Returned from Bristol this morning, getting up at before 5 to catch the 6am train. 
  12th July  - Arranged Ladies&#39; evening at Mess, for the wives of 4 officers - Capt. Parker, (16) Capt Smith(15) Lt [indecipherable]&nbsp;Fish (15), Lt Whiting (16). We all attended a concert of men by Lena Ashwell&#39;s London Party afterwards in YMCA Hall. 
 14th July - Obtained leave to visit Bristol to purchase material for our Mess. Left Codford 10.36 and arrived at Bristol about 12.30 just in 