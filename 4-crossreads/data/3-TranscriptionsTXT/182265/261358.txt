 [Page 44] 
 Entry  21/8/17  -  2.30  pm&nbsp; 
 In Owl Trench - X or Reserve Line between Messines &amp; Gapard Farm. Arrived here last night about 9.30 from resting at Drouancourt near Belgian Border. Coming up we had hot cocoa &amp; milk at Bde Hqrs. Met Gen Brand here. 
 McKillop, Cleland, Westwood Merifield &amp; myself. Sniped at coming in but no casualties. Trench fairly dry, but sloppy in places. 
 In charge carrying parties last night carrying from Mule Dump to Reserve &amp; Front Lines beyond Gapaard&nbsp;Farm. A quiet night comparatively here but tremendous straffing on either side, especially North.&nbsp; 
  Entry  23rd /8/17  -  6pm  
 Several aeroplanes overhead - our &amp; enemy. One enemy plane came repeatedly over us this morning - Very gamely indeed 