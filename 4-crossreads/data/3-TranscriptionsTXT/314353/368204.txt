 [Page 67] 
 October 19 
 Casualties Continued Corps Order No. 775, 9.10.17 Wounded:  Major T.J. Frizzel was evacuated to hosp. wounded 8th inst.  Major N.E.B. Kirkwood was wounded on 8th inst., and remained on duty.  No. 220 Pte. Laver was evacuated to hosp. wounded 8th inst.  No. 223 Cpl. Havenstein, D.D., was sent to D.R.S. wounded on 8th inst. 
 Corps Order No. 778, 12.10.17 Wounded:  No. 165 Pte. Watts, R.C., was evacuated to hospital wounded on 4th inst. 
 Corps Order No. 785, 19.10.17 Wounded:  No. 17622 Pte. Hambly, W.J., attached to 1st Aust. F.A. Bgde. was evacuated to hospital wounded on 17th inst. 
 Ends Casualties to date of stunting since 18 Sept. 1917 
 Killed in action &ndash; 10 Died of wounds &ndash; 5 Wounded &ndash; 39 Casualties &ndash; 54 
 October 19 70. Good old Horatio! Mr. Bottomly!! John Bull!!! 
 So there will be no more warnings:  watch Russia!... and we will read no more ex cathedra prophecies.  You tell us of 'discarding the mantle of the Prophet'. 
 "Not because I have found my data wrong, but there were so many people &ndash; Politicians, I think they are called &ndash; possessing and exercising the power to interfere in the conduct of the war, that everybodies calculations (including those of the Generals in the field) were being so constantly upset, that I came to the conclusion, that all prophesy &ndash; whilst that state of things endured &ndash; was in the nature of guess work, and decided to abandon it." 
 Oct. 6, 17 But, dear John, we readily forgive you, you have made Doug live for us:  you tell us he &ndash; 
 " is essentially a military organiser and strategist&hellip;  He is a man of mystery- never in a hurry, never ruffled. 
 I was much impressed, whilst at Headquarters, with the implicit faith which every member of the staff has in him.  In the middle of lunch he quietly left the table, and went into his office &ndash; where he remained a quarter of an hour, quite alone.  "The Chief has thought of something", said one of the A.D.C.s, " something important, to leave us 