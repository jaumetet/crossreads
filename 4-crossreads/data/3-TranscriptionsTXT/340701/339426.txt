 [Page 93] 
 which seriously annoyed him.  He replied with the heaviest stuff he could send. 
 Saturday 31 September 1916 Met Shannon in the secret safe[?] today he is Pioneer Sergeant of the 20th 
 Sunday 1 October 1916 Clock put back one hour today (day light saving).  Lieut Armitage &amp; Segt Nevin ? wounded today.  Our Artillery bombarded last night Hill 60 I believe.  Fritz Shelling town with Whizz Bangs today in spite.  An Aeroplane brought down a Baloon today. 
 Friday 6-10-16 L/M Everything quiet.  Still working on Vince St. trench (Secret Trench) 
 Sunday 8-10-16 Sick parade today.  Boil on face. 
 Friday 13-10-16 Had 3 days light duties over Boil.  28th Batt had a raid last night.  Moderately successfull only a couple of wounded.  Got 2 prisoners.  The 28th nearly bombed the 20th when they were changing over. 
 14-10 Applied for leave to Poperinghe yesterday.  Got shelled today in trench.  4" &amp; Whizz Bangs from different angles cross &amp; enfilading.  Fritz got a machine gun possy &amp; wounded one of the crew. 