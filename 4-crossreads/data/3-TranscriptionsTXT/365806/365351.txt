 [Page 65] 
 4 Wagga this time.  Do you remember Maj. Edwards being hopelessly out of direction when we were up there?  Did you see the butter-factory this time?  If I remember rightly it was a German who owned it. 
 I am going to France to-night with another draft, so will have to stop now.  This is only an excuse, because I have no news &amp; my pen is running out. 
 Please remember me to Mr. O'Reilly. 
 Your loving son Keith 
