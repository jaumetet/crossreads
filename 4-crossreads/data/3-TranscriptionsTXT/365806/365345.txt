 [Page 59] 
 France 30.11.16 The Hon. Mr. Justice Ferguson Sydney. 
 Sydney 
 Permit me to offer yourself and family my deepest sympathy in the lass of your son Capt. A. S. Ferguson. 
 Unfortunately I had no opportunity of seeing him before I learnt of his untimely death so I was unable to tell him that I had travelled with you on the "Osterley". 
 I am sure that the circumstances under which he died will in some measure console you all. 
 With kind regards, I am yours sincerely M.C. Moors [indecipherable] 
 