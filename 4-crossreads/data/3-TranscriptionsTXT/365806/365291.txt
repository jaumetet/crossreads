 [Page 5] 
 H.Q. 3rd Aust. Division 
 Dear Judge, 
 Card enclosed is the production of H.Q. staff clerks.  The oval on the back is H.Q. colours &ndash; if black and white are colours. 
 I received a message the other day from two ladies at Amesbury asking for Lieut. Ferguson's whereabouts, and was only able to tell them that he had left for France with the 
