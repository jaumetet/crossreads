 [Page 8] 
 4 effect were taken by the majority as mere exaggerations made for political purposes. 
 I had a slight illness the other day, had to go into hospital, came before a medical Board, &amp; was offered a commission.  However, an interview with General Monash &amp; General Howse put the thing right, &amp; I go to France.  Leslie Williams, who was doing my work while I was away, gets the commission &ndash; under Lance Addison.  Lockhart has been in England on leave very fit &amp; well. 
 Yours sincerely E M B 
