 [Page 36] 
 20th Up very early to look at a captive observation balloon.  At 2 p.m. we went ashore, and had a long weary walk up Shrapnel Gully, with packs on to which we are unaccustomed.  We dug a few temporary dug-outs for ourselves.  All over us bullets were cracking and singing, and now &amp; then shrapnel bursts.  The doubtful honour of having the first casualty of the 6th fell to our troop Bergelin being hit in the eye by a stray bullet. 
 21st Busy at work completing our dug-outs.  Colonel Cox wounded slightly in the leg.  Four great Black Jack Johnson shells burst close to us.  They looked awful, but did no damage. 
 22nd Was warned to go to Lemnos with Turkish prisoners, but as a G-submarine turned up, we had to wait. 
 23rd Went into trenches for 28 days.  Saw with a periscope hundreds of dead Turks lying just outside our trenches.  On observation duty through the night. 
