 [Page 43] 
 20th Mess orderly.  Cossack outpost night. 
 21st Inoculated again.  Supports night. 
 22nd Firing line. 
 23rd Firing line. 
 24th Mess orderly.  Sergeant Lamborn hit again, &amp; killed. 
 25th Firing line.  Fatigue at night. 
 26th Water guard. 
 27th In reserve.  Cossack outpost reserve at night. 
 28th Digging fatigue.  Firing line night. 
 29th Warned to be ready to go on a bodyguard to-morrow for General Sir Ian Hamilton. 
 30th Inspected by Colonel Cox in morning.  In evening news came through of a victory for us in Asia Minor, leaving the way clear to Bagdad.  At 11 p.m. the bodyguard (12 from each squadron) fell in and marched down to the wharf, where we 
