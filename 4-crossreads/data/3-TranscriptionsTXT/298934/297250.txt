 [Page 12] 
 would mean another night without sleep.  Heavy naval guns going off all last night away to the south where the Tommies &amp; French are. 
 4th May Tuesday 1915 
 Resting &amp; fatigue work.  Pulled out of bed 10 p.m. to 1 a.m. digging a trench. 
 5th May Wednesday 1915 
 Went into trenches at 10 a.m. &amp; got relieved at 2.30 p.m.  Occupied dugouts till 3.30 a.m. next morn after receiving 2 days iron rations. 
 6th May Thursday 1915 
 Embarked per horse boats aboard gun boats etc. the whole of 2nd Brigade &amp; sailed down the D's to where the French &amp; Tommies were.  Saw many results of heavy bombarding forts guns graves, entanglements hors de combat.  Helped to get some 60 lb guns into position. 
