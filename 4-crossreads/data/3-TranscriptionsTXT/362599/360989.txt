 [Page 26] 
 ICE [21/1/17 Sun] Go to Soccer match on Tap Ground. Write to Chris France, Mrs McL. &amp; Roy. Billy goes to Lark Hill for Duty. 
 F [22/1/17 Mon] Wrote to Jennie &amp;Home Sent Photos. Stayed in bed all day. 
 F [23/1/17 Tues] Ice. Recd. letter from M.R.L. Sgt McKenzie gets knocked by motor. Walked to Durrington Milstone. I had tea out &amp; met another Melbourne Blatherskite of a woman V.A.D. 
 F [24/1/17 Wed] Ice Wrote to M.R.L. &amp; []. L. Robson Lottie Watson &amp; Jessie Henderson Sewed shirts for Dave. Took from 11.30pm to 5am to write to M.R.L. Dreamt of I and [Indecipherable] with Marg Millar &amp; Shipwrecks. Going to Douglas. 
 ICE [25/1/17 Thurs] Recd. letter from Kingsway re watch. Wrote in return. Wrote to Mary Lindsay &amp; Hilda, Charlie Bates came here. Slept nearly all day. Had no food from 10.30pm yesterday till 7pm this day. 
 ICE [26/1/17 Fri] Recd. letters from Marge. Bess Dave Vernon. Nettie &amp; Jess Mather. Slept during morning thats all. 
 ICE [27/1/17] Sat Recd. letters from M.R.L &amp; Jennie wrote to M.R.L. Goaled at the Footy match &amp; concert in Palladium. Poor Montgomery dies at 5.45p.m. Sent Postal Note for my watch. Billy Ali comes over for the week end 
