 [Page 151] 
 Memoranda 7th Jan 1917 About 5 pm the Kent left us and all the transports went in different directions. 5. 30 10 destroyers met us but only one remained. 
 Mond. 8 Jan.  very rough spray coming on to the boat deck.  Passed destroyer this morning and sometimes the waves went right over her. (45) 
 [Pages 152 to 192 not transcribed] 
 [Transcribed by Rex Minter for the State Library of New South Wales] 
 