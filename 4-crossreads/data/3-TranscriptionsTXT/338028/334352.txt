 [Page 43] 
 Sat 2nd Sept Doc went on leave to Cairo, he is a character, just says "carry on" and cleared out. I am acting Doctor. 
 I am very worried over my brother as a big casualty list has just come through, I have not yet seen it. 
 Sunday 3rd Sept Rode into R'head after I did sick parade for drugs, my mare is so well I cannot hold her, she bolted yesterday with the groom. 
 Monday 4th Sept Heard the new unpopular R.S.M. counted out this morning, I was sorry even though he is a cad. 
 It seems definite that we are getting camels, every man is cursing. 
 Tuesday 5th Sept I wrote a rather good poem on Conscription to-day. That ought to be appeal, Gardeyn, by telegrams, it seems certain to take place which will be a blessing, we are to get the vote and make a certainty of it. Doc arrived back with great news of naval battle [?] Wednesday 6th Sept Camels seem a certainty now. Put in for leave to Cairo, on duty.  This pay mortgaged, draw on next pay. 
 Mail in, rec 6 letters, none from my brother, 60,000 casualties in France making me very unsecure. 