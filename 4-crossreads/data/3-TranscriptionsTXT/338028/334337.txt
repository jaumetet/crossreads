 [Page 28] 
 Friday 14th July 
 Very hot day, the men go about naked and are as brown as berries. The date palms are all heavy laden with ripening fruit, but we do not understand them like the Bedouins. They are just falling to the ground for the want of a little knowledge. 
 Wrote Norn. 
 Saturday 15th July 
 At last we are going in for our horses expect to leave here on Monday. Great news from all fronts, so far. The big offensive is doing well 
 Sunday 16th 
 Hot day. No mail as expected, we leave here to-morrow for Kantara are being relieved by Scottish Horse. Advance party went to-day. 
 Monday 17th July 
 Scottish Horse took over redoubts in morning, busy packing and pulling down tents, packed on camels left for Gilban, loaded train, unloaded again at Kantara at 11 pm. Regt moved out to camp I stayed behind with goods all night. 
 Tuesday 18th July 
 Transports came for our stuff and greatly surprised when we arrived to find camp in order. Our horses arrived, they were in fine lot my old mare looks particularly well. Busy all day, we are off again shortly so erected bell tent only. 
 Big mail in I got 6 letters including letter from Grand. 