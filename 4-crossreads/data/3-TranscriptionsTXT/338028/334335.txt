 [Page 26] 
 Friday 7th July 
 Caught a goods train 10 am arrived in the miserable town of Port Said, had a spruce up, then tis out to enjoy ourselves, but had to make our own fun, surfing was the only really good thing. Everything terribly expensive, we stayed at the best hotel, and our ex's were &pound;1 a day, hotel alone. 
 Saturday 8th July 
 Spent the day enjoyably all over harbour, wins over warships. We saw latest aerial machine gun which fires shrapnel and has self adjusting time fuse. Gave some letters to passengers on the Mooltan to post in Melb. 
 At night we fell foul of Red caps, I hit one right out expect crime to come to regs, but it was worth it. 
 News of big success came through and French civilian shouted us three bottles of champagne. 
 Sunday 9th July 
 Nearly missed train, arrived Kantara to find train did not go until 4 pm, went to Gloucester Hussars Sgts Mess where we had a good time and finally arrived back in camp at 7 pm (?) 
 Monday 10th July 
 A good deal of amusement was caused when the crime sheets came through from Port Said, so we expect 