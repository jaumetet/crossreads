 [Page 29] 
 Wednesday 19th July 
 Big sick parade in a.m., many casualties during day, etc, kicks, falls and one man who diving in Suez Canal hit the bottom and fractured his spine, I had to take a stretcher party to the poor boy, he was a fine lad, and is now totally paralized, saddler, parades etc etc all day long. This is not a very systemated Reg. 
 Spent evening and spare time with my mate Sullie over at the 1st L.H.F.A. Transport camp, he has had a great time. 
 Thursday 20th July 
 Great rumours in that a force of 10,000 Aust[rian]. Germ[man], Turkish troops have entrenched 3 miles from Bir el Abb. We are making great packs. I was surprised at the number of heavy guns that went through, if it does realize I hope we can get into it mounted. 
 Friday 21st July 
 Rode down on old Zigi to Scottish Field Amb to see the poor lad who fractured his spine. The renowned Surgeon Professor Weecks took me to him, I found him in a low condition totally paralized and dying. The poor boy was very cheerful and he died at 5 p.m. He was one of my pets, and the finest lad in the regt. 
 I now have very big marquee up doing big sick parade. All our horses are bunched into the shade of some lovely trees during the heat of the day. 