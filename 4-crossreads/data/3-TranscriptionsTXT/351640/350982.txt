 [Page 536] 
 5.9.15. Sunday. Good night my dears. God bless you. I cannot add more before sending the envelope to the post, but a post script may follow if there be aught of interest or of business that will relate to you or your affairs. 
 I have an appointment with General Ford at 10 a.m. tomorrow or rather to day, because the hands of the clock have reached beyond midnight. 
 Good night my dears. May the goddess Fortune keep deep in love with each of you, and may all that you desire be with you. 
 Good night. Good night. Good night. Heaps of love and loads of kisses for each of you. To all my friends my best wishes and kind regards. I am Your loving and affectionate father John B Nash 
 The Misses Nash 2ic [219] Macquarie Street Sydney New South Wales. 