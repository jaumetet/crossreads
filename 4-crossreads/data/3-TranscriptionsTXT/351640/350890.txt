 [Page 444] 
 [Pages 444 to 455 are a letter to Dr Nash from Blanche Sutton (see page 525 for confirmation of her surname and more about her role), a Sister at The Queen of Belgium's Hospital, La Panne, Belgium, dated 22 June 1915. The pages are out of order and the letter is transcribed here in the page order in which it should be read.] 
 Written sideways at the top of the fist page:] Faithfully Blanche [indecipherable] 
 Queen of Belgium's Field Hospital La Panne Belgium 22.6.15. 
 Dear Dr Nash Thank you so much for letter and views.   So very good of you to write me when you are so busy.   I appreciate the letter very much.   Yes! I am proud of our country men I think their work has been magnificent, I scan the papers keenly for news of you all. 