 [Page 10] 
 337 Royal Manchester Eye Hospital Manchester 6th July 1915 
 Dearest Joker and Bobbie Many thanks to you both for your kind letters &ndash; Perky will have given you all my news &ndash; I was awfully pleased to see her. We had a great time together &ndash; I enjoyed my lunch last Sunday more than I can ever remember  having enjoyed a meal before &ndash; 