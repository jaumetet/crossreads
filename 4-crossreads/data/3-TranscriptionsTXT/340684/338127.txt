 [Page 73] 
 435 2 by an explosion of a shell &ndash;  it has been a jolly dull week for me stuck in bed all the time &ndash; The weather has been beautiful and yesterday morning there was quite a heavy snow storm. The first one many of the fellows have ever seen 
 J and R came up last Saturday looking splendid as usual &ndash; its jolly good of them to give up their Saturday afternoon to come up here, especially now that I can't get out. 
 I haven't heard from Sir G.Reid since I wrote last &ndash; I will send him along a reminder in a day or two but there 