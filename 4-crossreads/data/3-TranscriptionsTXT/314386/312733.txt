 [Page 41] 
 15 Thurs. [Mar.] Had a trip to London to escort an A.W.L. prisoner Back  But only had 2 hour's there was up at 4 a.m. &amp; in Bed 1- 40 a.m. next day. 
 16 Friday.  Was allowed to sleep till Breakfast, then attend Court with our Bird.  He got 14 days Field Punishment  20 days of his pay Robbed. 
 17 Sat.  Cricket begin's.  it is day light now till 8 p.m.  It seems funny to see Daylight so late.  Am  beginning to like England now it is getting Warmer and our Camp is in Glorious Country. 
 18 Sun.  Church parade.  A trip to Broad Water in the Afternoon  Finished at night Poaching Rabbits 
 19 Mon.  Beautiful Weather now it makes a person glad to be alive.   Men are getting very fit and lusty looking for fight now  Another Draft Picked 
 20 Tues.  All ordered a special clean up.  Full Pack Etc. to be ready.  We wondered But was not told any thing 
 21 Wednesday  Up at 5 Breakfast at 5.30 quick march at 6. Going a Route March to Bulford.  King's review 
 21 Wed.  Continued.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;to be held there on Thursday.  It Poured Rain all the march.  Went through Wilton &amp; Salisbury.  Camped at Rolliston.  No Blankets and Dry Ration's we boiled Tea. 
 22 Thurs.  Fall in we are all Wet &amp; Cold Hardly any tucker.  March on to Bulford  Still Pouring Rain.  It was not "God save the King sung.  We got to Bulford on time and we had a great feed.  We fell in for the Review.  The Duke of Connaught represented the King.  Sir N. Moore and General Mackay with him  Moore spoke &frac12; an Hour  Mackay also Connaught  "Fine body of men I'm sure. Owing to Wet dismiss.  Hoo Ray 
 23 Friday  Those who could Pay fare's were told they could go back by train  So not relishing 19 mile more of it, I lit out for the Station and had a good day in Bunk. 
 24 Saturday.  Glorious day.  Men not Back yet only about 40 of us in Camp so get a day off.  I went to Dinton.  Got adopted there and did A.1 
