 [Page 70] 
 the front of Camp, Sound the Regimental Call, Play "Sons of the Brave" the Regimental march, then play "Advance Australia" through once at 6 a.m. 
 We have orders to pull the white ribbon, numerals, and infantry badges off our shoulder-straps. 
 Issued with another blanket. 
 18th  Our Brigade work has now commenced with a review and inspection of troops by Brig Gen'l Jobson and Gen'l Monash on the common by the Stonehenge.  There were two march pasts in close column of Companies while the mass bands played, also a General Salute. 
