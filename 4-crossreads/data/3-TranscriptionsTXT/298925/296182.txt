 [Page 105] 
 O how the Spring of love resembleth The uncertain glory of an April day Which now shows all the glory of the Sun And by and by a cloud takes all away 
 O Say not woman's false as fair That like the bee she ranges Still sucking flowers more sweet &amp; rare As fickle fancy changes Ah No The love that first can warm Will leave her bosom never No second passion ere can charm She loves &ndash; and loves for ever 
 If she be not fair for me What care I how fair she be 
