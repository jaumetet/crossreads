 [Page 78] 
 At Westbury, a few miles from Lavington they have a big white horse on the slope of the downs made by stripping the turf from the hillside showing the chalk underneath.  It is supposed to be a representation of the horse ridden by Alfred the Great in his battle against the Danes. 
 22nd  Practice for Divisional Review to be held next week. 
 23rd  Brigade sports  to be  held on the downs near Rifle Range.  Mule races etc were held. 
 The band are absolutely exasperated with the major who stopped our weekend leave after it was granted from Brigade Hq'rs. and we were 
