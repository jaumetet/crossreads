 [Page 77] 
 a strategic flanking movement.  The other Battalions all had their bands with instruments. But our Major with his characteristic eccentricity ordered us to take stretchers instead.  The Brigadier roused on the major and despatched a motor transport back to camp for our instruments and music, so we played a programme on the square.  The inhabitants were delighted as they had never heard a band like ours before. 
 21st   At 10 a.m we left for home and arrived at camp 4 pm.  We took the same route back which is about 17 miles each way. 
