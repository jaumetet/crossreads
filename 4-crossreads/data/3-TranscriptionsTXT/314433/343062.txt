 [Page 13] 
 Stretcher bearers, also S.M. Simpson proceeded to the dugout where they were left. On reaching it there were six Huns there, and S.M. Simpson lined them up into single file at the point of the pistol. While they investigated further, the wounded men were attended to." 
 F.J. Brewer 
 Address of Deponent P Morris Helensburg South Coast New South Wales 
 [Transcribed by June Pettit for the State Library of New South Wales] 
 