 defeat the Officers (affirmative). 
 Messages (per wireless) sent to Australia.&nbsp; News from Sydney &amp; Melbourne.&nbsp; Sporting results and a threatening strike. 
  July 8th  
 A ships&#39;concert on &quot;D Deck&quot;. 
 A message of welcome to the &quot;Rio-Pardo&quot; and her troops, is received by W.A. Holman. 
 Some of the boys also receive personal messages from Australia. 
