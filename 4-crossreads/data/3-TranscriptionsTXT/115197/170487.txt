 from Horseferry Rd. for duty at Aust. C.R.E.&nbsp; Further news of R. Carlisle. 
  Mar. 12th  
 Rain &amp; wind - wind &amp; rain - Scarcity of letters.&nbsp; &quot;The growing Faith&quot;.&nbsp; The absence of melancholia. 
 Usual duties. 
  Mar. 13th  
 Third anniversary of leaving Egypt for France on the &quot;Minneapolis&quot;.&nbsp; A visit to the Ambulance - Old faces &amp; old memories. 
 -&nbsp; An endeavour to return to 7th for purpose 
