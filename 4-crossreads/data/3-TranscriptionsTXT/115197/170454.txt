 my present Unit - &quot;Australian Corps Engineers Workshops&quot; - New faces - Duties practically nil. 
  Jan. 18th  
 Rehearsal - Reading - Writing. 
  Jan. 19th  
 Liberal terms regarding &quot;Demobilisation&quot; - Expecting to leave France in April.&nbsp; Rupert&#39;s news regarding position at Drury Lane.&nbsp; The Peace spirit throughout France &amp; Belgium.&nbsp; Soldiers entertain &amp; are entertained by civilians. 
