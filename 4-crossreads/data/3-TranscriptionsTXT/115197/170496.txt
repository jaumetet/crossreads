  Mar. 26th  
 Fatigue duties - Another Poem - A parcel from Home. 
 The stir caused by Sydney and &quot;Our Harbour&quot;- Reading writing &amp; walking. 
 An evening with my new friends - Monsieur as a linguist - French.&nbsp; German.&nbsp; Spanish.&nbsp; Italian.&nbsp; English.&nbsp; Dutch.&nbsp; Flemish.&nbsp; etc. 
  Mar. 27th  
 Official news of departure to &quot;England&quot;&amp; thence &quot;Australia&quot; -&nbsp; Hurrah!!! 
