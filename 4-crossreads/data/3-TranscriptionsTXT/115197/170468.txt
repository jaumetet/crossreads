 a farce with &quot;C.H.&quot; the predominating feature. 
  Feb. 7th  
 Writing &amp; Reading.&nbsp; Mr &amp; Mrs Scales return from Wanstead.&nbsp; Dinner with Mr &amp; Mrs Harold at &quot;Westcott&quot;. 
  Feb. 8th  
 Morning, afternoon &amp; evening at &quot;Elm Bank&quot;.&nbsp; Such a way of spending the day holds a greater appeal and more charm than jostling along the busy London like so many aimlessly do. 
 &nbsp; 
