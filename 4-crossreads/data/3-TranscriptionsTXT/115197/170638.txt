  June 5th  
 Tropical weather does not appeal to the men after leaving the pleasant clime of England.&nbsp; A growing friendship gives much enjoyment and relieves the days of monotony. 
 Dolphins &amp; porpoises racing with the boat and leaping from the water and flying fishes skimming above the waves provide a little diversion. 
 Morning parades &amp; inspection 
