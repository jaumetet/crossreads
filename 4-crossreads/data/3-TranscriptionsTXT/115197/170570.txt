 are in the forms of newspapers and a public telephone, around which the men swarm about fifty strong and await their turn. 
 On Sunday evening we have our final Service and the Padre gives good advice. 
 On Monday morning, much to our great relief a number of our party leave the &quot;Station&quot; for our Homes. 
 We travel to Woolloo- 
