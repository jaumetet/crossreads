 colder and the sea rougher. 
  June 14th  
 A day spent chiefly with book and pen.&nbsp; A chapter by Donald Haukey.&nbsp; 
 A new book &quot;Quiet talks about the Tempter&quot; (Gordon).&nbsp; Owing to certain throat troubles all the troops undergo a throat spraying operation. 
 &quot;B&quot; Decks adds points to their aggregate. 
