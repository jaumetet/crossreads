 anchor in the stream. 
  May 27th  
 Plymouth Harbour.&nbsp; Natural beauty.&nbsp; Not unlike Farm Cove in parts.&nbsp; The Rio Padro passes up stream and is moored to a wharf in the naval docks yard .&nbsp; The troops for the Pardo arrive per train.&nbsp; They consist s  chiefly of 3rd Div. Infantry. 
 Band selections by 39th Battalion.&nbsp; A last letter! 
