  June 16th  
 Read &quot;The Pocket Thackeray&quot;, which consists of popular excerpts from the works of the novelists - Note book entries - The &quot;Rio Negro&quot; is now reported to be 350 miles behind our ship.&nbsp; A nasty cold causes discomfort.&nbsp; A &quot;home remedy&quot; is brought to light. 
  June 17th  
 Nearing the &quot;Cape&quot; - four days sail to Cape Town.&nbsp; Will we be allowed ashore? 
 &quot;Quiet Talks about the Tempter&quot;.&nbsp; (S.D. Gordon.) 
 &nbsp; 
