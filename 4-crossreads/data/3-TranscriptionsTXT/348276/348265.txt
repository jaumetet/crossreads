 [Page 66] 
 War Diary 1916  18th March Capt. G.F.G. Weick promd. Major and seconded as Bde Major to 15th Infy. Bde. Major W.P. Farr to be D.A.A. and Q.M.G. Anzac Mounted Division. Capt. C.C. Dangar 13th Hussars to be Brigade Major 
 19th March Church Parade 1/1st. Ayshire Battery moved to Rail Head on 17th inst also 1/1st. Lowland Mounted Bde. Ammunition Column. 
 20th March Making arrangements for reconnaissance to Moiya Harab tomorrow.  G.O.C. round right of line. 
 21st March Brig. Gen. (G.H.Q.) selecting gun positions on front line   Reconnaissance to Moiya Harab under Capt. Wearnes 8th L.H. 
 [In right margin] Lt. Col. Arnott to Command 9th L.H. d/17/3/16 
 22nd March In wireless touch with above reconnaissance; work proceeding steadily on front line. 
 23rd March Reconnaissance returned at 0330, both men and horses stood the 80 miles in 37 hours well &ndash; G.O.C. to Ismalia to confer with Gen. White re promotions and reinforcements coming to Serapeum. 
 24th March Lt. Col. Downes appd. A.D.M.S. of Anzac Mtd. Divn. on 3rd L.H. Bde. train disbanding, Capt. Dunningham and 2nd Lt. Dickenson retained in Brigade. 
 25th march Gen Birdwood visited the Brigade to say "Good-bye" on his leaving Egypt &ndash; He inspected works 1 &ndash; 9 on front line. 
 26th March Church Parade at Rail Head &ndash; 10th L.H. 
 27th March Fresh instructions received re new distribution of troops in No. 2 Section &ndash; Bde. will be much scattered &ndash; 1 Regt in outpost line of "B" sub-section and 1 in outpost line of "A" sub-section, the remainder at Rail Head. 
 28th March Trench work on whole front going on very satisfactorily.  Gen. Cox (Comdg. 4th Div.) and Staff visited Rail Head 
 29th March Brigade Major visited left of front line;  three weeks on No. 9 almost completed. 
 30th March 3rd Light Horse Brigade train disbanded, all horses and wagons being retained in the Mounted Division &ndash; a number 
