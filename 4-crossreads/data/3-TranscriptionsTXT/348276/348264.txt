 [Page 65] 
 War Diary 1916  4th March Inspection of 9th L.H. by G.O.C.  &ndash; Lines clean, men comfortable. 
 5th March Church Parade &ndash; two Squadrons 10th L.H. under Lt. Col Todd D.S.O. took over the Reconne duties from N.Z.M.R. Squadrons, that Bde. taking over certain position of 2nd Divn. line. 
 6th March Regts. settled down to training, all movements being done at the walk. 
 [In the right margin] Major Nicholas 10th L.H. to 51st Battn. d/5/3/16 
 7th March Received orders re taking over of that portion of front line at present held by first Division &ndash; G.O.C. inspecting line with Gen. Chauvel. 
 8th March One Sqdn. 10th L.H. took over Works 1 &ndash; 2 &ndash; 3 and 4 &ndash; 5 &ndash; 6 the following day. 
 9th March Remaining two Sqdns. of 10th L.H. took over posts 7 &ndash; 8 &ndash; 9  - This Regiment being responsible for some six miles of front originally held by our Infy. Bde. 
 10th march No. 3 Troop of 10th L.H. taking part in reconnaissance to locate aeroplane which failed to return;  Recce cut short, pilot and observer returning on foot to 9th Corps; 9th L.H. took over that position of front line held by 2nd Infy. Bde. 
 [In right margin] Capt. A. McAllister from 31st Battn. to 8th L.H.  Lt. Wearne to be Capt.  Capt. Worthington (V.C.) [?] to 8th Mob. O. Sectn. 
 11th March Move of 8th L.H. and H.Q. postponed on account of shortage of water at Rail Head. 
 12th march Regts on front line settling down 
 13th March G.O.C. Met O.C. units at Rail Head to discuss matters in connection with holding front line. 
 14th March G.O.C. took Gen. Godley to the left of line and inspected No. 9 Post 
 15th March B. Major went round the right section of line &ndash; orders received to move B.H.Q. and 8th L.H. (less 1 Squadn.) to Rail Head, the squadn. to Work 62. 
 16th March No work could be carried out &ndash; high wind blowing filling in trenches. 
 17th March Trench work being carried out on front line. 
