 [Page 67] 
 War Diary 1916  30th March of men transferring to the new R.A. formations. 
 31st March G.O.C. took Gen. Cox (Comdg. 4th Aust. Div.) and Gen. Chauvel round the right section of H sub-section. 
 1st April Cleaning up lines &ndash; inspection of lines by G.O.C. &ndash; a great difference to when the B.M. Took over. 
 2nd April Church Parade 
 3rd April G.O.C. took Gen. Cox round the left of the front line -  Orders issued for the new distribution on front line. 
 4th April Redistribution on front line took place.   8th L.H. moved into "B" sub-section of No. 2 Section &ndash; 10th L.H. to Rail Head &ndash; 9th L.H. remaining on front line. 
 5th April Regiments settling into new quarters.  Signal Troop broken up and brought down to new establishment 
 6th April G.O.C. to conference with G.O.C. 4th Australian Division &ndash; a reconnaissance E to be carried out. 
 7th April General Staff Officers visiting front line &ndash; making of horse shelters in progress 
 8th April Swimming Parade 
 9th April Church Parade 
 10th April Camel Transport escorted by Bikanir Camel Corps and 30 armed Lighthorsemen as drivers moved out to Wadi Um Muksheib to be picked up by reconnaissance moving out 11/4/16. 
 11th April Column under Major Scott 9th L.H. moved out, Jifjafa being the objective &ndash; men and horses well turned out &ndash; all looking fit and hard. 
 Major Scott's Jifjafa Column The object of the reconnaissance was mainly to destroy gyns erected over wells at Jifjafa, capture the post, and report generally on the country etc. in the vicinity.  The whole enterprise was a complete success in every way. 
 11th April Column moved out at 1400 making bivouac in Wadi Um Muksheib at 2230 travelling good, some 6 miles 
