 [Page 68] 
 War Diary 1916 ahead of front line. 
 April 12th Having picked up camel transport at above, the column moved on at 0730 to water, arriving 1120 &ndash; At dusk moved to point 1340 arriving 0230 
 April 13th Under cover of a khanpseen [khamsin, a dust storm] Hill 1080 was reached and from this point, the attack was made &ndash; The attack was a complete surprise, the enemy bolting to the hills, and after a sharp engagement surrendered &ndash; Result, 6 enemy killed, 4 wounded, 29 captured, which included an Austrian engineer officer; our only casualty being Cpl. S.F. Monacham [Monaghan] (976) 8th L.H.  Killed. 
 During attack our engineers destroyed gyns and a very up-to-date boring plant and all gear etc. of any value -  The return journey was made without opposition &ndash; During the whole operations, communication with Rail Head was maintained by wireless, which worked most satisfactorily, with the exception of 13th inst. when khampsan was at its height. Congratulatory telegrams received from C in CE.E.F. Generals Godley, Cox, Chauvel.  Major Scott decorated with D.S.O. 
 Composition of Column 
 Commanding &ndash; Major W.H. Scott 9th L.H. Staff Officer &ndash; Capt. H.E. Wearne 8th L.H. Staff Attache &ndash; Capt. MacCaulay G.S.O. 3., Capt. Ayres. 9th L.H. &ndash; 117 all ranks 8th L.H. -  12 all ranks Wireless Secn &ndash; 9 all ranks Engineers &ndash; 10 all ranks R.F.C. &ndash; 4 all ranks A.M.C. &ndash; 9 all ranks A.S.C. &ndash; 1 all ranks Bikanir C.C. &ndash; 25 all ranks Guides &amp; Intr. &ndash; 3 all ranks Camel T.C. &ndash; 127 all ranks. (includes 30 L.H.) 
 Totals Men &ndash; 320 Horses &ndash; 175 Camels - 261 
 16th April Jifjafa prisoners sent to Ismailia &ndash; Church Parade - 
 17th April Detailed in connection with Jifjafa Raid finally settled.  Remainder of 3rd. L.H. Field Ambulance joined the Section 
