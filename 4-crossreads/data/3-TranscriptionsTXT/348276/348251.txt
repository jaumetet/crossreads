 [Page 52] 
 War Diary Daily State    27th November. 1915 
 Brig. H.Qrs.   At Mudros &ndash; Off. nil; O.R. 3 Effective St'h. &ndash; Off. 4; O.R. 14; Total  18 Killed &ndash; Off. nil; O.R  nil Wounded &ndash; Off. 1; OR. 1 Sick &ndash; Off. 2; OR  21 Missing. &ndash; nil On Duty - nil Trans - 1 Total &ndash; 26 
 Signal Tp.  At Mudros &ndash; Off. nil; O.R. 5 Effective St'h. &ndash; Off. nil; Ors 19; Total  19 Killed &ndash; Off. nil; OR  nil Wounded &ndash; Off. 1; OR. nil Sick &ndash; Off. nil; OR  26 Missing. &ndash; nil On Duty - nil Trans - nil Total &ndash; 27 
 8th L.H.  At Mudros &ndash; Off. 1; O.R. 50 Effective St'h. &ndash; Off. 8; Ors 125; Total  133 Killed &ndash; Off. 16; OR  192 Wounded &ndash; Off. 3; OR. 123 Sick &ndash; Off. 13; OR  258 Missing. &ndash; nil On Duty - 4 Trans - 6 Total &ndash; 615 
 9th L.H.  At Mudros -  Off. 6 O.R. 116 Effective St'h. &ndash; Off. 11; Ors 270; Total  281 Killed &ndash; Off. 6; OR  57 Wounded &ndash; Off. 3; OR. 145 Sick &ndash; Off. 15; OR  332 Missing. &ndash; 17 On Duty - 1 Trans - 6 Total &ndash; 582 
 10th L.H.  At Mudros  -  Off. 5 O.R. 96 Effective St'h. &ndash; Off. 9; Ors 205; Total  214 Killed &ndash; Off. 12; OR  111 Wounded &ndash; Off. 12; OR. 142 Sick &ndash; Off. 9; OR  259 Missing. &ndash; 12 On Duty - nil Trans - 8 Total &ndash; 570 
 Total landed to date -  2767 Including Re-enforcements) 
 Effective strength &ndash; 665 Rest at Mudros - 282 [Total]  947 [Total]  1820 
 Summary:- Killed 394, Away wounded  431, Away sick 940 (Away 1371), Missing  29, ,  On command 5,  Transferred. 21 [Total] 1820 
 J.M. Antill  Col. O.C. 3rd L.H. Bde. 30/11/15 
