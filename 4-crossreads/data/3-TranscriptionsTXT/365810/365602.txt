 [Page 2] 
 1st Nov. '17 
 Dear Judge, 
 I owe you a letter as I have yours of 7th September.  I say thanks awfully for those cable forms.  I carried out your instructions and sent one straightaway saying that I was off to a signalling school for seven weeks.  This is not exactly hard to take as a school is a school and such things as good huts &amp; beds with fires are not to be despised these days.   Winter is pretty well on us now.  The trees have only few leaves and Mud is starting its reign or rather it has started.  The wind is 