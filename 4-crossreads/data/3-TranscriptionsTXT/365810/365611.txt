 [Page 11] 
 Commonwealth Military Forces 2nd Military District 
 District Pay Office, Victoria Barracks, Sydney 
 20th November, 1917 
 Dear Sir, 
 I beg to enclose herewith a Statement showing the balance of Military Pay at date of death, &pound;78.1.5 &amp; cash in effects &pound;8.7.10 which is due to the estate of your late son, No. Rank Capt., Name, A.J. Ferguson, Unit, 20th Battalion. 
 The above amount will be available at the Victoria Barracks within the next few days. 
 Mr. David Gilbert Ferguson, Supreme Court, Sydney. 
 Yours faithfully, W. Percival Minell, Lieut.-Col., District Paymaster, 2nd M.D. 
 Copy of Statement enclosed is to be kept by you. 