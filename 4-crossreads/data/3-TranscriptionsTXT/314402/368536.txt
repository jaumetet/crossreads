 [Page 18] 
 REPORT NIGHT SEPT 18/19 1915 D. Coy 12th BATT 
 Events About 1700 Enemy opened with heavy rifle &amp; gun fire. No movements of enemy seen to our front 
 All quiet at 1800. Few bombs on LONE PINR 
 1945 Very quiet. Keen observation to be kept [signed] PH Weston 2030 
 Message from 3rd Inf Bde recd 2140 G.O.C. Division special [Indecipherable] as against possible attack tonight following today's demonstration AAA Please take special steps for period moonset to daylight AAA review your bomb arrangements messengers &amp; means of quick support. 
 Instructions Between Mo[o]nset &amp; daylight officers &amp; N.C.Os on duty to constantly patrol line &amp; constantly check field of vision of sentries. 
 Events 2030-2400. everything quiet, bombs thrown toward LONE PINE. 
 [Signed] K C Jacob 0100 
