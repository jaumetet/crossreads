 [Page 80] 
 NOTHING is to be written on this side except the date and signature of the sender.  Sentences not required may be erased.  If anything else is added the post card will be destroyed. 
 (Postage must be prepaid on any letter or post card addressed to the sender of this card.) 
 I am quite well. 
  I have been admitted into hospital sick/wounded and am going on well/and hope to be discharged soon.  I am being sent down to the base.  
 I have received your letter dated Aug. 23.  telegram/parcel  
 Letter follows at first opportunity.  I have received no letter from you lately/for a long time.  
 Signature only &ndash; G. Horan Date &ndash; 23.11.17 