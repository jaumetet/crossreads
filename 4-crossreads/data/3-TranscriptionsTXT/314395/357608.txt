 [Page 46] 
 we will suffer the sun of Bondi or Manly when we return, we will just about go black.  Of course we have always been much browner than the Englishmen, even in winter we looked like half-castes, beside them. 
 This draft consists entirely of N.C.O's and signallers, so it seems as if the latter have suffered some casualties. 
 Nothing exciting to report, so I reckon I'll quit, and turn in, as my next few nights will be almost devoid of rest on account of the sardine method of travelling employed by the military.  Will write again as soon as I get back. 
 Your affect. Son, George. 