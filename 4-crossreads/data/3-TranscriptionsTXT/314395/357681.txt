 [Page 119] 
 Sgt. Horan 
 Australian General Base Depot. 
 You will return to England to-day 18-1-18 reporting to M.L.O., Quai D'Escale at 4-30 p.m. 
 24 hours rations will be carried which may be drawn from Depot Ration Store. 
 [indecipherable] Captain, Adjutant Australian General Base Depot. 
 Havre, 18-1-18 