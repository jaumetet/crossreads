 [Page 99] 
 -2- 14th October 1916. 
 Glad you got some news of the boys through Corp Barker.  It's a lot better than letters isn't it. 
 Hope you are busy getting Xmas Billies ready, which reminds me that I'll soon have to start scattering Xmas Greetings about. 
 Nothing more to add.  Am expecting a mail in.  Your last was August 13th. 
 Hope all are well at home, 
 With love from Roy. 
