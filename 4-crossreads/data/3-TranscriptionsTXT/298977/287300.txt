 [Page 130] 
 Tommies.  My position is isolated and we are living like rabbits.  Not allowed to go out of the dug-out in day time. 
 Saturday 9th September 1916  So far it has been very quiet we have to watch all night and sleep in day time.  Our rations are brought up at night and that is the only time we see anyone.  A bit of excitement tonight &ndash; the dug-out caught fire, burning one of the boys packs and several waterproofs. 
 Sunday 10th September 1916.  A mail came in tonight and a small parcel for me. 
 Wednesday 13th September 1916  About 11.30 to-night we were relieved suddenly by the 6th M.G. Coy.  After carrying ammunition to Tillebincke Village, came back to Coy. H. Quarters.  Fine row of dugouts.  Issued with a blanket and got to bed about 4.30 a.m. 
 Thursday 14th September 1916  Pay today.  Moving up again tonight &ndash; Only 2 guns &ndash; Left at 9.30 p.m. and came up and took over a rotten position.  Only my gun stayed, the position too precarious for another gun. 
 Friday 15th September 1916  The 19th Div. carried out a stunt on our left using gas.  There are only 2 of us at the gun, the other 3 are behind.  We have to divide the all-day watch between us. 
 Saturday 16th September 1916  Another stunt on the right this time tonight.  So far Fritz replies to the bombardment have been very weak, and rumours are flying around that he is rushing from Ypres front. 
 Sunday 17th September 1916  Great sadness tonight &ndash; Nothing to eat!! &ndash; something went wrong with arrangements. 
 Monday 18th September 1916  Nearly flooded out, raining all day.  Issued with rubber boots. 
 Tuesday 19th September 1916  Issued with a Primus Stove for cooking.  A big mail in tonight, 14 letters and a parcel of tobacco from Mr. Pauch. 
 Sunday 20th September 1916  Relieved tonight and came back to B---.  2 parcels. 
 Wednesday 27th September 1916  Left B--- at 8 on leave to Blighty.  Went down as far as transport in limber, and walked to Paperingha, about 2&frac12; miles. 
