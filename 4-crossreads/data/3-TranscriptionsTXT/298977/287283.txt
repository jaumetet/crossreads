 [Page 113] 
 France 16th February 1917. 
 Dear Mrs. Richards, 
 My deepest sympathy goes out to you and all in the loss you have had through the death of your dear Son Roy.  As you know, I had known him for a considerable number of years, and the last time I saw him was in France, near the front line trenches.  It was about eight months ago. 
 We were at Hayfield, and the Sydney Grammar School at the same time. 
 One wonders when this awful business will be over.  I think and hope by the end of this year. 
 With kindest regards to you and all. 
 Yours very sincerely, (Sgd) Bernard Rose 
