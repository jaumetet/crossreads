 [Page 58] 
 -2- 22nd May 19816. 
 Think I told you that Jim Stafford, the pal of mine, whom Charlie knew, was killed on my gun, the night we saw life at its worst. 
 So Billy has got a unit at last.  He should be over this way shortly now.  Mother sent me one of his Photos.  He is a worthy representative of the family.  Only got one lot of his tobacco. 
 My furlough to England is still on the hang on but am still hopeful of its coming off shortly.  Hope the trip to Harwood did you good, the place must be looking well now.  Glad all my girls round there are O.K.  The free trip out with the mail is certainly an inducement. 
 Nothing fresh.  Things going along in the same old way.  Hope things at the Office are O.K. and everybody the same. 
 Love to all at home. Your loving Son, Roy. 
 5th Aust Machine Gun Coy., A.I.F.  France.  
