 Squadron of Turkish cavalery &amp; drove them back about a mile to their entrenched positions.&nbsp; On &quot;Listening Post&quot; last night about a mile out in front of the barbed-wire entanglements. 
  May 28th :&nbsp; Our Division with drawn to Khan Yunis today where we expect to be more or less &quot;resting&quot; for awhile.&nbsp; Relieved by the 2nd Division (Imperial). 
  June 3rd :&nbsp; Went through gas drill today.&nbsp; First of all we attacked through heavy clouds 
