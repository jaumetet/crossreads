 [Page 5] 
 [Pages 1-4 are covers and library information]
 Original Diary of Ashmead Bartlett kept during the Gallipoli Campaign 
 10 &frac12;  156 [indecipherable] H 11,  H 12a  advanced up  86  H 12 87th Brigade S.W.B [South Wales Borders] K.O.S.B. [Kings Own Scottish Borderers] Borderers took J9, J10 J11, J11A &amp; the Boomerang Trenches [Turkish redoubt] east of [indecipherable] Marine as far as J 11 
 [Numbers refer to trenches marked on British maps] 
 