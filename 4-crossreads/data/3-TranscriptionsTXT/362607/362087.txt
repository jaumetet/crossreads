 [Page 205] 
 Mr T Nicholls Corry Rd P.O. North Manly Sydney australia 
 London  24/3/19 
 Dear Dad &amp; Mother, 
 I am just returning from 14 days leave &amp; I catch the train for Sarum where I will stay the night. I had a lovely time although very quiet &amp; I will tell you all about it when I feel settled down in two or three days time.   Best Love  Harry 