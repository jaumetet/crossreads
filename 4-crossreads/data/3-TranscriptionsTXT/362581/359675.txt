 [Page 72] 
 2.12.17 
 My darling Eddy., 
 On Thursday I received a letter from Auntie Ethel with some cuttings from you. They are very pretty   &amp;  think you are getting a very clever boy. Hope you are quite well   &amp;  strong this term   &amp;  shall be able to go every day to school. When I come home you must have a few days holiday   &amp;  will will have a picnic. Hope Miss Tolhurst is still at the school   &amp;   that she is well. 
 Fondest love   &amp;  kisses XOXOX From XOXOXOX  Auntie Eadle 
