 [Page 37] 
 do not expect to see the pretty little village again. 
 Aug. 19th An Entertainment given in the camp where we are billeted (No. 4 Con. Camp) &ndash; Read "The Little White Bird" &ndash; Rehearsal. 
 Aug. 20th Return shows (two) given to Officers &amp; Men of No. 39 General Hospital &ndash; Rehearsal &ndash; Letter writing Book Purchases &ndash; 
 Aug. 21st Rehearsal &ndash; Letters &ndash; Diary &ndash; Return show at 