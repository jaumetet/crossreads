 [Page 50] 
 Our Engineer Dvr. Smith leaves for Long Service Furlough in Australia &ndash; After the show I write a new dramatic sketch for what is called "The Burglar". 
 Sept. 15th Sabbath &ndash; Further decoration to the Blue Room. &ndash; Wash clothes &ndash; write letters. Weather warm &amp; oppressive. 
 Sept. 16th A never to be forgotten storm &ndash; Thunder &amp; lightening as I have never seen before heard or witnessed it. Show as usual. 