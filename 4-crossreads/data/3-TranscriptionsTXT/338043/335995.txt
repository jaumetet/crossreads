 [Page 72] 
 Betterton. Garrick. Kean. Kemble. Macready.  Coquelen &amp; Irving. Tonight we have given a capital show to a capital audience and the "Comforts Fund" capital is thereby increased. 
 Oct. 20th (Sabbath) A very busy day &ndash; During the morning we move to another hall in town. In the afternoon I am busy making arrangements for my production which is coming off to-morrow night. 