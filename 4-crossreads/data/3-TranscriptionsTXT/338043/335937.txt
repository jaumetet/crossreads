 [Page 15] 
 At the Army Bakery (North) &ndash; many W.A.A.C's present &ndash; Return to camp "Champ des Courses" 
 July 31st Rehearsal of "Grafters" &ndash; "The Arabian Nights Entertainments" &ndash; Ward shows &ndash; Night performance at the Army Bakery (South) &ndash; We are afterwards treated in a good hospitable, colonial fashion. 