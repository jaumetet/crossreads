 [Page 2] 
 [Letterhead of Young Men's Christian Association of Sydney N.S.W. Field Service Department] 
 Light Horse Camp Liverpool 
 Dear Auntie I suppose you will be wondering what has become of me. I left Holdsworthy nearly a fortnight ago and am back here in the Light Horse. I expect to be down to see you on Sunday. 
 I was camped out at Roseberry Park all last week but couldnt get out to see you as I had forgotten the address and didnt have the letter with me. I had plenty of time off while I was down there too. I expect to be going on final leave next week as we will be sailing on or about the 23rd. 
 Well Auntie, I will close now hoping to see you all on Sunday. 
 With best love from Your affectionate Nephew Frank 