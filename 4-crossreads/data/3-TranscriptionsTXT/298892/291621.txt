 [Page 161] 
 is there are fine roads and  ramps  paths all through the camp  leading to the baths, messrooms &amp;c. While in London I called at the bank and received the &pound;10 you sent. It was very welcome and will tide me over another trip if I can secure the requisite leave. Trusting all at home are well Your loving son Eugene. 
