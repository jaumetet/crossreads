 [Page 171] 
 Saturday  (date blanked out by censor) Somewhere in France. 
 My Dear Parents:- We have arrived here at last and are now settled in billets some miles from the front. We left (censored) on Tuesday morning early and arrived at  (censored) .ampton before dinner after an uneventful journey. We loafed about the wharf all day until night when we packed aboard a transport and when we awoke in the morning we were in harbour. We all had to carry two blankets and a waterproof in addition to our ordinary pack containing our 
