 [Page 220] 
 France Monday 12th. February '17. 
 My dear Parents/. Since last writing I have received five very interesting letters from home, one from father dated 15th. Decr. and four from mother rangeing from the 12th. to the 17th. Decr. So Douglas has been home for another vacation. He must brighten things up a great deal for you now that the family is so small. From all accounts though Kit seems to keep up a fair round of entertainment. 
