 [Page 21] 
 Less casualties daily as we are doing well with our sniper posts &ndash; &ndash; &ndash; &ndash; &ndash; 
 Swimming every day practically &ndash; though a long hot walk through crowds of soldiers &amp; flies. 
 Bud well &ndash; also Gibbs &ndash; Dowling &ndash; Hordern &ndash; Howard. No letter from you for a fortnight but hoping for some soon &ndash; 
