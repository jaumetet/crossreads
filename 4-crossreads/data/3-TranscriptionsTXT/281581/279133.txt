 [Page 133] 
 a brand new machine being sold for &pound;100. Decent glasses are unbuyable without an order and then the prices are beyond reason. 
 Boots run at 2/5/. - 2/15/- and upwards!  Leather goods are luxuries.  Afternoon to Ponting's flat selecting lantern slides. 
 27th July /18 
 Have extremely little spare time at present. The preparation of the expedition films and slides needs my own personal, selection, care and attention. 
 Left Hotel at 8 am and to Raines &amp; Coy. at Ealing. Put in hand Shackleton slides.  Afternoon to Sir Douglas Mawson's and with him through the expeclition films &amp;slides. Sir Douglas and his lady are the 
