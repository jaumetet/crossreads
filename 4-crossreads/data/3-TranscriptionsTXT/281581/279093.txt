 [Page 93] 
 [No entries from 8th March until 5th May, 1918] Safe at last.  Dropped anchor in the Downs, 6 pm. Wretched fog all day with rain. 
 6th May 
 At high tide steamed up the Thames and by means of lighter landed at TilBury 5 pm. 
 Put up at the Imperial Hotel Russel Sq. Evening went to Daily Chronicle and saw Perris. He informed me the rights of the Shackleton Film have been sold on the Continent for &pound;5,000 and the British for another &pound;5,000. The film is now free of all encumberances and any further sales of rights will be distributed. 
