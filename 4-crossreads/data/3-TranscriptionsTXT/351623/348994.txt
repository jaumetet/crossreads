 [Page 32] 
 4 Oct 16 Left Devonport, exact destination unknown, doing 23 knots 
 5 Oct 16 Passed Isle of Man (where the cats have no tails) &amp; Ireland &amp; through Firth 
 6 Oct 16 Arrived at Scapay flow, Scotland. We are now anchored with the First Battle Squadron of British Navy. Coaled ship from Collier Cedar Tree. 
 7 Oct 16 With fleet. Done sub-calibre firing. 
 8 Oct 16 ) 9 Oct 16 )  With fleet. Weather rough &amp; cold 10 Oct 16 ) 
 11 Oct 16 Done sub-calibre firing. 
 12 Oct 16 Anchored with fleet 
 13 Oct 16 Done 6 inch firing in afternoon &amp; sub-calibre firing at night. 