 [Page 52] 
 Princess Royal, Repulse and Renown arrived at 6 pm tonight. 
 23 Aug 17 Sydney left for Glascow &amp; dock tonight, weather foggy. Having moving pictures on board tonight. 
 24 Aug 17 In Scapa at anchor. 
 25 Aug 17 Lifted anchor &amp; started to sea with squadron &amp; 6 destroyers. 
 26 Aug 17 At sea patrolling &amp; looking for German raiders. Steaming North along the Norwegian coast at 8 am heavy fog came on at 7.30 pm tonight. 
 27 Aug 17 Weather very rough &amp; getting worse. Steaming for home. Arrived Scapa Flow &amp; anchored at 7.30 pm. Started to coal at 10.30 pm took in 500 tons finished at 5 am raining all night. Battle cruisers still here. 