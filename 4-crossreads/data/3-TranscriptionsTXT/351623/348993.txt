 [Page 31] 
 went to Union Jack Club for the night. 
 9 Sept 16 Three mates &amp; myself took a room off Tottenham Court Road. Went to Lyceum Theatre play was Women &amp; wine. 
 10 Sept 16 Went for a walk to Chalk Farm. It is very dark in London at night, all lights being out in case of Zeps. 
 11 Sept 16 To 29 Sept 16 [Gillies Birthday 22 &amp; never been kissed] In London. Zeppelins came over London while I was there &amp; two were brought down. 
 30 Sept 16 Returned on board ship. 
 1 Oct 16 ) 2 Oct 16 ) 3 Oct 16 )  Lying alongside dockyard pier. We have a New Captain &amp; also a new Engine Commander. We now have an antiaircraft gun mounted on board &amp; several other additions to make her an up to date fighting ship 