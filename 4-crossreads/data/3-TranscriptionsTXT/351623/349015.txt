 [Page 53] 
 28 Aug 17 In Scapa Flow, weather heavy &amp; blowing. Battle cruisers left for sea at 3 pm. 
 29 Aug 17 In Scapa Flow 
 30 Aug 17 Doing Squadron torpedo practice today. 2nd Battle Cruiser Squadron. Australia New Zealand. Inflexible &amp; Indomitable came in today. 
 1 Sept 17 Anchored off North Shore Scapa Flow. Airships going to sea patrolling. 
 2 Sept to 6th Lying in Scapa Flow 
 7th Sept 17 Draft left for Australia today. Battle cruisers left for Rosyth 