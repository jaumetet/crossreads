 [Page 91] 
 should say that is the particular spot where I was. As the New Year came in they fired a barrage of guns &amp; the boy scouts played the All Clear, the same as they used to do after the air raiders were gone from over London. I saw a few theatres &amp; had a good time in general while on leave. 
 4 January 1819 Went back to Devonport Barracks. Arrived at 10 oclock in forenoon &amp; went out again at 12.30 in afternoon for weekend leave till 7 oclock on Monday morning 
 5 January 19 Ashore in Devonport. 
 6 Jan 19 Came into barracks again at 7 oclock &amp; went ashore again for afternoon &amp; all night leave. 
 7 Jan 19 Drafts from Melbourne, Sydney &amp; Australia left Devonport for 