 [Page 51] 
 now that they have gone up Tomorrow will see the opening of the 3rd Stage of the push here &amp; our battalion takes the first objective 
 Thursday 4th Oct The advance began this morning at Daybreak. A detailed account will be added later. I went to several hospitals during the day. Prisoners were coming down in hundreds &amp; wounded were packing the hospitals. I hear the objectives were all taken. M Clark died today Called at 35th Transport &amp; saw some of the Old Batt. 
 Friday Today I went up to the hospital on the Menin Rd I passed through Ypres 