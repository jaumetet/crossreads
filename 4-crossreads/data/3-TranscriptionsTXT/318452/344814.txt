 [Page 101] 
 After dinner, saw Rev. now Corporal Bistock Jones, who is here recovering also. It was a very bitter day &amp; slight snow fell &amp; a keen east wind blew. 
 Wednesday Jan 9th The weather today has again been very cold the ground is frozen hard &amp; the wind is very cutting. I visited the hospital this afternoon &amp; met several fellows I knew there 
 Monday Jan 14th The weather towards the latter end of last week &amp; yesterday was fine cold &amp; clear there is little of interest. I 