 [Page 29] 
 then return home and help to erect our billet, and take possession of same.  Retire 9 p.m.  Receive letter from Nan. 
 Friday, May 19th: Rise 6 a.m.  Fine day.  I stay home to-day and work on our billet, building bunks and mess table.  The boys go out to Fleurbaix and dismantle two huts.  Retire 9 p.m. 
 Saturday, May 20th: Rise 5 a.m.  Ride on waggon to A.S.C. Sailley and erect sleeping hut, size 24 x 16.  Ankle a bit weak, but that's nothing.  Fine day.  Receive news from Nan.  Retire 9 p.m. 
 Sunday, May 21st: A fine day.  We are given a welcome spell.  Rise about 8 a.m.  Granted leave to visit Sailley.  Write to Mother, Uncle and Nan.  Walk in to Sailley in afternoon.  Spend a quiet, enjoyable day.  The weather is perfect.  Retire about 8 p.m. 
 Monday, May 22nd: Rise 5 a.m.  Ride out to A.S.C. Sailley at 6 a.m.  Work all day and return from work at 5 p.m.  First portion of day fine.  Showery towards evening.  Retire 9 p.m. 
 Tuesday, May 23rd: Rise 5.30 a.m.  Ride out on transports to erect old huts near Fleurbaix, and make souvenir in the form of transparent square.  The huts have been under fire.  Return to Billet at 