 [Page 51] 
 Situation Report 12-10-17 
 Situation generally O.K. Casualties Nil S.A.A. expended Nil Change of Personnel Nil Plenty of Rations , water &amp; bulk S.A.A. Desultory shelling by Hostile Artillery generally 5.9. Spirits of men in fair order. They are anxious to personally encourage. Heavy artillery situated behind YPRES. Have sent Mitchell on to Mr Cory, he attended to three here no room to keep his stores dry here. MacKey turned up. 
 PHS Weston Lt  4pm. 