 [Page 10] 
 Morning Report 24/9/17 Casualties NIL Situation front quiet. bombardment by enemy severe about 4-5am 
 Round fired Nil 
 Part of rations were lost during last night's bombardment, please send extra tonight if possible. 
 Will co-ordinate positions later also make alternative [Indecipherable] 
 [signed] 740am CHP Weston 