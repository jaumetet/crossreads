 [Page 32] 
 been an air raid in London &amp; about 50 killed &amp; 70 wounded, there have been several raids lately &amp; we are always hearing reports of Australian mail steamers being sunk which I think are more or less true.  I will try &amp; write you a more interesting letter next time.  Give my love &amp; kisses to every one &amp; much to yourself. 
 From Douglas 