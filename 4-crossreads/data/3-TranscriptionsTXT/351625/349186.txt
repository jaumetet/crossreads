 [Page 83] 
 The Salvation Army 
 The Base France 6 Dec. 1917 
 Dear Mother, 
 We are in France at last, left England about two days ago &amp; will most likely be going to the 2 Batt. in a day or two, they are not in the line at present.  I am afraid our going to the 17 Batt. has fallen through as by some carelessness or other our transfer papers which came through a day or two before we left, we left behind, there is still a chance they may send them on but we can't lay much hope on it.  Our trip across from England was a very rough sort of affair as far as the accomodation went.  Although the sea was quite calm the boat was a dirty smelly 