 [Page 6] 
 the delay. 
 In expressing the sentiments of the men in my platoon I must abuse the French language "Oh! trez bon" but verbal description does not adequately describe their appreciation of your kindness. 
 Thanking you, I am, Yours truly 894 Gordon Grange, Sgt. 17th Batt. 