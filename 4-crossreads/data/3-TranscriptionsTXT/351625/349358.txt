 [Page 255] 
 Hospital 27 Sept. 18 
 Dear Mum, 
 I suppose you will be glad to hear that I am safe &amp; sound in hospital in Blighty.  Its the greatest bit of luck out as I have nothing worse than a septic finger.  At the time I was sent away from the Batt. they happened to be clearing out all the hospitals etc. in France for a big stunt that was to come off any day so I came sailing through here.  Stourbridge is the name of the place near Birmingham.  We have been having a very rough time over there 