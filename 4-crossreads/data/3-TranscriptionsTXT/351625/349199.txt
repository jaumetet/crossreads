 [Page 96] 
 France 19 Dec. 1917 
 Dear Mother, 
 We are at last soldiering properly &amp; have had some very strenous  marching lately.  While I am writing this I can here the constant banging &amp; thundering of the guns, it goes on day &amp; night but none have lobbed about this district.  To get here from the billets well back where we joined the Batt. we had four days marching 