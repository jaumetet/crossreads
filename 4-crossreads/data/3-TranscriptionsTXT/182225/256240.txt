 a6575104.html 
 29 th &nbsp;June 
 MARCH &nbsp; SATURDAY 25 &nbsp; 1916 
 which was sunk at the Dardenelles was waiting in the anchorage &amp; she is to accompany us the rest of the way. We steamed out of the harbour about 6 pm &amp; explored outside. The other boats all went in in turn. We got some postcards of Dakar &amp; sent them home. Dakar is the capital of the French Senegal, but is practically only a naval port and only officials live there. 
 We had an impromptu concert at night and it was a treat to have the lights again. 