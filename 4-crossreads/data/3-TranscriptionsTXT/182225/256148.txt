 a6575012.html 
  RATES OF POSTAGE - contd.  
 Newspapers (each), United Kingdom, 8oz. 1d.; 
 8 oz. to 10 oz. 2&frac12;d.; all other places, 4 oz. 1d.; each additional 2 oz. &frac12;d. 
 Printed papers (not newspapers), New Zealand, Fiji, New Hebrides, Bris. Solomon Islands, 4 oz. 1d.; each additional 2oz. &frac12;d.; all other places, 1d. per 2 oz. (limit 5 lbs.). 
 Patterns, Samples, every 2oz., 1d. (Limit to United Kingdom, 5lbs.; New Zealand and Fiji, 1lb.; other places, 12 oz.) 
 Commercial Papers, New Zealand and Fijji, every 2oz. up to 5lbs. 1d.; other places, 2oz. or under, 3d.; every additional 2oz. up to 10oz., &frac12;d. ; every additional 2oz. up to 5lbs., 1d. 
 Parcels, to United Kingdom, 1 lb. 1s. ; every addtional lb. (limite 11lbs.), 6d. 
 Magazine Post, to New Zealand, Fiji, New Hebrides and Solomon Islands (Brit. Prot.), 8 oz. 1d., and &frac12;d. each additional 4 oz. All other places 1d. per 2oz. 
 Registration Fee for letters, etc., 3d. 
 Money Orders payable in United Kingdom and British Possessions, &pound;2, 6d. and 3d. for each additional &frac12;1 ; New Zealand and Fiji, &pound;2, 6d.; &pound;5, 1/-; &pound;7, 1/6 ; Papua, &pound;5, 9d.; &pound;10, 1/6; &amp;c. Limit &pound;20. 
  Eclipses, 1916.  
 
 January 20th. - Partial Eclipse of  Moon , partly visible in Australia. 
 February 4th. - Total Eclipse of Sun, invisible in Australia. 
 July 15th. - Partial Eclipse of  Moon , invisible in Australia. 
 July 30th. - Annular Eclipse of  Sun , visible in Australia. The central line of Annular Eclipse passes along the southern portion of Australia. At Adelaide the Eclipse will be Annular. 
 Dec. 25th. - Partial Eclipse of  Sun , invisible in Australia. 
 
 &nbsp; 