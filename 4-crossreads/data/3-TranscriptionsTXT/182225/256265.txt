 a6575129.html 
 17 
 1916&nbsp;&nbsp; SATURDAY 27&nbsp;&nbsp; MAY 
 No sun yet, but there was no wind today which was something to be thankful for. The 27 th  Battery put 4 teams against the 35 th  Bat. 9 th  Brigade Queensland and we each won two pulls. In the afternoon there was boxing tournaments in the afternoon. Just 3 round &quot;no decision&quot; bouts; but there were some tip top goes. Its Linda&#39;s birthday to day - her 21 st . I would give anything to be with her for the day, but hope to be there next year, although I think I will be lucky to be home for her 23 rd . 