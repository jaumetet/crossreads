 a6575273.html 
 160 
 OCTOBER &nbsp; TUESDAY 17 &nbsp; 1916 
 Raining all day. Went out with the battery on the staff as a horse holder. We were inspected by the C.O. &amp; then took up a position &amp; I believe they did not do to well. We got back late &amp; we were cleaning harness all the afternoon. I went to the pictures with Jack Hunter at night. Got a letter from Linda &amp; two papers. 