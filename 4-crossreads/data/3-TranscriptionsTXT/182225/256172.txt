 a6575036.html 
 Jan. Thursday 13 1916 
 A Toast to the &quot;Reliable&quot; Man. 
 Here&#39;s to the steadfast reliable man 
 The man with the tongue that&#39;s true 
 Who wont promise to do any more than he can 
 But who&#39;ll do what he says he&#39;ll do 
 &nbsp; 
 He may not be clever; he&#39;s often quite blunt 
 Without either polish or air; 
 But though its not in him to &quot;put up a front&quot;, 
 When you need him he&#39;s always there 
 &nbsp; 
 So here&#39;s to the man on whome one can rely, 
 And here&#39;s to his lasting success 
 May his species continue to multiply 
 And his shaddow never grow less 
 &nbsp; 