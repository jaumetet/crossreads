 a6575276.html 
 163 
 1916 &nbsp; FRIDAY 20 &nbsp; OCTOBER 
 Fine, but cold. I was on stable picket all day. All the men had to go through a riding school in the morning. It was decided that&nbsp; all &nbsp; each of &nbsp;the batteries was to have different coloured horses so they drew for the colours in the evening &amp; the 27 th &nbsp;drew the black horses,&nbsp;much to everybodies delight, though personally I would rather have the greys. 