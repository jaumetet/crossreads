 a6575179.html 
 66 
 JULY &nbsp; SATURDAY 15 &nbsp; 1916 
 A warm day for this part of the world, but we were glad to wear our tunics. Had some flag drill with M r &nbsp;Neylan in morning. The battery went on duty in afternoon, and as I was not on&nbsp; ^  duty &nbsp;I went down to Bournemouth with Bill Richards, Dick Capel &amp; Harry Camp in a car. We had one of the prettiest drives I have ever had. We had dinner at the &quot;Bath &amp; East Cliff&quot; hotel, &amp; came back after. My word the officers did look when we common gunners walked into their inner sanctum. 
 Poor Billy Hunter got bad news, he heard his brother Clive had been killed in France. I hope its not true. I had a letter from Linda the 2 nd &nbsp; first ​&nbsp;she has written, but none from father. 