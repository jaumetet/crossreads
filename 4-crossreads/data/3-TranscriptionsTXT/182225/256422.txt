 a6575286.html 
 173 
 1916 &nbsp; MONDAY 30 &nbsp; OCTOBER 
 Fine. Got a car into Salisbury &amp; got an early train to London. Went straight to the&nbsp; Regents Palace &nbsp;Grovosner Court &amp; saw Auntie Florence &amp; M rs &nbsp;Angus. Went round London &amp; did some business in morning &amp; saw the &quot;Battle of the Somme&quot; on the pictures at the &quot;Scala&quot; theatre in the afternoon. We all went to the Opera &quot;Samson et Delilah&quot; at night &amp; it was beautiful. I got the 11.15 pm train to Edinburgh from Euston afterwards. 