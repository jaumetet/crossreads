 [Page 6] 
 [Previous cover pages not transcribed] 
 Continued from old diary. 
 28/9/16 A good deal of artillery activity round here lately 
 30/9/16 Seventh and Eighth bns. are having a raid tonight in front of our lines. 
 1/10/16 The raid was successfully carried out last night. We hear the raiders killed about 40 germans. Our casualties were 2 killed. 