 [Page 95] 
 failed to hit anyone. 
 We expect to move further up tonight and do the hop over in the morning. 
 Our people are putting up heavy half hour barrages at intervals of about two hours all day. Fritz reply seems very weak but he seems to have a bit of a set on this position and some of his 5.9 are landing too close to be comfortable. 
 The german concrete dugouts round 