 [Page 16] 
 made. 
 3/11/16 Shelled the village heavilly but our dugouts were too secure for him to do any damage to us. Dozens of men going to hospital this last few days with bad feet owing to weather conditions 
 4/11/16 We expect to go over tonight 6 sigs are detailed to go up with the coys. It is a small position and the object of the stunt is to straighten 