 [Page 85] 
 the line since Sunday and have had a fair number of casualties with gas and shellfire. One of our sigs was killed yesterday afternoon and I hear another was wounded. 
 There are a large number of tanks in the forward area. 
 I hear the stunt takes place tonight. The 2nd and 3rd Bdes attack, and our bde hold the line. It has been read out to the troops that the success of the allies is depending on this 