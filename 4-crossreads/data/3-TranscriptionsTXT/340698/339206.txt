 [Page 67] 
 about midnight we travelled via Domart, Doullens, St Pol, and Lillers and de-trained at Bavinschove about 5 am. Marched from Bavenshove to Wallon Cappelle, and went into billets surrounding the town. 
 We hear that most of the towns we were billeted in on our previous garrison of this front have since been blown to pieces. 
 Fritzs' aircraft are being active round here and pay nightly  billets  visits to 