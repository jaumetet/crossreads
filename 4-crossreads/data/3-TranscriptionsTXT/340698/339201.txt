 [Page 62] 
 2/7/17 Shifted camp from Englebermer to Mailley. Had to shift from that town to let Tommy artillery in. 
 3/7/17 Had a big field day today for the purpose of training staff officers. 
 5/7/17 Had an all night manouvre last night and finished about 8 oclock this morning. Most of the work was 