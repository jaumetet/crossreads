 [Page 69] 
 motor buses today. They moved in the direction of St Omer but their destination is unknown. 
 31/7/17 Laid lines to all coys. 
 Great rumours of big advance something is doing up north as there has been a very heavy bombardment going on for some time. 
 1/8/17 "Fritz" shelled Hazebrouck with heavy long range guns yesterday afternoon. 