 [Page 87] 
 easily and had very few casualties. We captured  200 machine guns and a good  200 prisoners and a good number of machine guns. 
 22/9/17 Went up to Q.M. Stores yesterday. 
 The attack the day before was a great success. The 18 pounders have moved forward fifteen hundred yards and cannot get in touch with "Fritz" so he must be well on the move. 
 Our battn. is in reserve and up to the 