 [Page 46] 
 have to move forward as expected. 
 15/4/17 Great excitement this morning. "Fritz" attacked and captured a few of Fourth bns' posts. We hear the Fourth lost a few prisoners and had a fair number of casualties. 
 Our bn was called up and are now in position in front line with fourth. We expect them to make another attack shortly. They 