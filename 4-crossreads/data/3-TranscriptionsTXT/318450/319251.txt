 [Page 105] 
 our marque caught fire, and how I do not know, anyway every case was a walking one so we all got out without any injury. 
 I remained at this place for about a week and then I was marked for Blighty. 
 We left the hospital about one pm and we were off in the train for  Cala Calais where we embarked for Blighty, it was not long before we got a move on, but the boat I do not remember the name as I was too sick to look. 
 It only took about an 1 hou  hour &amp; a half to run across the channel but I will never forget it as it was very choppy. 
 I was never sick all the way from Australia to Egypt, but by Jove I was sick coming across the channel this time. 