 a6680107.html 
 November&nbsp;&nbsp; 1918 
 3 SUN&nbsp; Dull day. Went up 
 the village at night. 
 Evening after 
 Rumours out re going into 
 action shortly. Quiet 
 4 MON&nbsp; Fine day. Preparing for 
 the revue. New pay books 
 issued. Right half bty dinner 
 at night. Tr&egrave;s bon evening 
 Out with J. 
 5 TUES&nbsp; Raining heavy. Review 
 cancelled. 4 men went on leave 
 Banthrop arr. back as Lt in AIF 
 Educ. scheme. Went up village 
 at night. 
 6 WED&nbsp; Raining heavy all day. 
 10. men went on leave. Caf&eacute;s 
 started 12-2. &amp; 6 to 8 PM. Went 
 up village at night. 