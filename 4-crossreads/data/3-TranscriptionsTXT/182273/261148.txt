 a6680077.html 
 July &nbsp; 1918 
 21 SUN &nbsp;Fine day. cool. very 
 quiet. Forward wagon 
 line est: guns 
 to move forward. Major came 
 down. Meteren taken yes- 
 terday. Very quiet at night 
 Swim. 
 22 MON &nbsp;Fine day. blowing hard 
 Amm. going up. Preparing&nbsp; 
 new gun positions. French 
 offensive going well. 
 Quiet at night. 
 23 TUES &nbsp;Raining all day. 
 Amm. going up. Quiet in 
 day. Quick Dick active 
 at night. Bombs at night 
 24 WED &nbsp;Fine day. blowing. Rec. 
 letter from Julie. very quiet 
 Preparing for stunt. Sent 
 pad up to J.W.C. Posted 
 Service Cards to. J.M.S. &amp; G.E.B. 
 Bombs. 