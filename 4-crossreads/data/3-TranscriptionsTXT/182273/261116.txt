 a6680045.html 
 March &amp; April &nbsp; 1918 
 31 EASTER DAY &nbsp; 
 Rain in morning, but fine 
 in aft. Guns 
 fairly active. Writing. 
 Battery in action nearly all 
 day &amp; night. Very quiet. 
 1 APRIL EASTER MON &nbsp;Fine day. 
 Rec letter from Amy&nbsp; &amp; photo . P.C. Julie. 
 Posted letter to Amy&nbsp; &amp; 10/- &nbsp;(&amp; encs. Else P. 
 &amp; Gladys). Bty in action all day. 
 &amp; night. Very quiet. Raid at night 
 by .8.  th   Bn 
 2 EASTER TUES &nbsp;Fine day. very 
 quiet. Rec. letter from Celie. 
 Major J.C.S. went to D.A.C. &nbsp;washing 
 Very quiet. 
 3 WED &nbsp;Dull day. Scottish troops 
 relieving our Infantry. Very 
 quiet. Orders out re moving 
 out tomorrow. Writing.&nbsp; 
 Right Section moved out 
 &nbsp; 