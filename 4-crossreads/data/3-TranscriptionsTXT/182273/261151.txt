 a6680080.html 
 1918 &nbsp; August 
 1 THUR &nbsp;Fine day. one section 
 pulled out. relieved by&nbsp; 
 29 th  Division 
 Posted &#39;Blighty&#39; to May. quiet. 
 Bombs at night. very 
 close. Tom Magee arrived back. 
 2 FRI &nbsp;Raining hard all day. 
 remaining guns pulled out 
 Moved off 9 P.M. for Pont [Anspin / Auspin ?] 
 arr: midnight. rough accomodation 
 Rec. letter from Amy. Finished&nbsp; 
 4.30 am. 
 3 SAT &nbsp;Raining on &amp; off all day 
 Preparing to entrain tomorrow. 
 Walked to Comapgne at 
 night with J.W.C. &nbsp;JW &amp; WJC. 
 had a pleasant evening. 
 Rec. letter &amp; photo from 
 Amy. letters from Celie &amp;&nbsp; 
 J.M.S. 