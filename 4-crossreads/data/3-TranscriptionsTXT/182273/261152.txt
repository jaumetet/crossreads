 a6680081.html 
 August &nbsp; 1918 
 4 SUN &nbsp;Fine day. Went into 
 St. Omer in morning. had a&nbsp; 
 glorious aft &amp; 
 evening. arrived home after 
 midnight. Said goodbye to 
 all French friends. Rec. letter 
 from Reg Clarke. 
 5 MON &nbsp;Fine day. Went on ahead 
 of Bty on byke 
 said Goodbye to all. Very hard. 
 Entrained at St. Omer moved off&nbsp; 
 5 P.M. raining. good accomodation 
 Met Billy Williams. 
 Raining on &amp; off all day. 
 6 TUES &nbsp;Arrived Longpr&egrave; 4 am. Moved 
 off to outskirts of 
 Ailly sur somme &amp; camped. 
 Moved off at 10.P.M. for Longueau 
 arr: 4.am. guns went into line 1000 yds 
 behind front line. 3 men wdd &amp; horses. Passed 
 through Amiens. town quiet. Fixed up camp 
 7 WED&nbsp; Fine day. Warm. great prepar- 
 ations for big stunt tomorrow 4.am. 
 Armoured cars, tanks of all descriptions 
 cavalry etc. going up. offensive on 50 
 mile front. Divisional waves. Huns 
 reported preparing retirement. Every 
 body confident all officers up top 