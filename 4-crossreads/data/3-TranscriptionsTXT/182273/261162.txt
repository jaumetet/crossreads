 a6680091.html 
 September&nbsp;&nbsp; 1918 
 8 SUN&nbsp; Posted letters to Amy &amp; May W. 
 Dull day. raining on &amp; off. 
 Fixing up camp. Grazing 
 horses. Officers 
 counted out by the boys. 
 Orders out re moving tomorrow 
 Very quiet. W Flintoff paid 
 me a visit. Concert at night. 
 9 MON&nbsp; Fine day. raining at 
 night. Moved off 9 am. through 
 Suzanne &amp; bivouacked near Hem. 
 in the open. Camped in trench 
 Fixing camp. &#39;Fritz&#39; still going back. 
 Rec. letter from Gladys. C. Quiet. 
 10 TUES&nbsp; Cold raining on &amp; 
 off all day. Grazing horses. 
 easy day resting. Orders out 
 re moving up tomorrow. 
 Writing at night. Quiet. 
 11 WED&nbsp; Cool. raining on &amp; off. Moved 
 off 9 am. via Peronne &amp; Courcelles Buire, 
 bivouacked near Tincourt. Guns 
 went into action at 4.P.M. for con- 
 centrated fire. Inf. checked during 
 the morning. &#39;Huns&#39; holding strong. 
 Country clean &amp; free from shell holes. 
 3  rd   Div. moved out. Quiet night. 