 [Page 55] 
 in future. The leave we get is poor enough as it is and this will not serve to make things any better. Heavy punishment is being dealt out to offenders and in some cases it is well deserved. Those who go raising Cain in Cairo can do with all they can get. 
 Got this song from a friend 
 Till the sands of the desert grow cold Till the infinite numbers are told God gave thou to me and mine thou shalt be For ever, to have and to hold. Till the story of judgement is told Till the mystery of heaven unfold I'll turn  unto  love to thee my shrine thou shalt be Till the sands of the desert grow cold 
 The hot winds that come to thee Oer desert sands all blow from thee I bid them to tell thee that I love thee Speeding my soul to thee Hot sands burning, fire my veins with passion bold Love I love thee, till desert sands grow cold Love me! I'll love thee 
 The desert a burning sea A barrier stands 'tween thee and me  All  The love fast as light hasten to thee Quenching my thirst in the Noon suns finds me, far beyond the caravan Death there warns me how vain is the strength of man Love me! I'll love thee. 
