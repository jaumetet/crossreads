 [Page 11] 
 Tuesday 6/1/17. Rode to Reninghelst and had tea with Lloyd. 
 Wednesday 7/11/17. Raining heavily all day- O&#39;Hara Wood came along for game of cards. 
 Thursday 8/11/17&nbsp; Orders received to move&nbsp; saw Bas had letters from Hetty &amp; Pattie. 
 Friday 9/11/17. Battalion moved by bus to Steenvoorde&nbsp; I rode with Little. Had dinner to celebrate Billie&#39;s Birthday with Robinson. 
 Saturday 10/11/17&nbsp; Quiet day- a bit off color- presumably Billie is the cause. 