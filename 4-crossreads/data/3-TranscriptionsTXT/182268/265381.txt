 [Page 25] 
 Sunday 13/1/18&nbsp; Church Parade, Letter from Doff- Finished report on Lagnicourt. 
 Monday 14/1/18&nbsp; Snowing heavily all day- went to Steenwerck 
 Tuesday 15/1/18&nbsp; Had lunch at Brigade H.Qrs.&nbsp; still snowing&nbsp; wrote to Doff 
 Wednesday 16/1/18&nbsp; Going on leave tomorrow, had a talk with Robinson &amp; Lloyd 
 Thursday 17/1/18&nbsp; Caught Light Railway to Steenwerck &amp; there by train to Calais. No boats running- Got a room at Commerce Hotel- Went to dine with a Miss Martin Y.M.C.A. 