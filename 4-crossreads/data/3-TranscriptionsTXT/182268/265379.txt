 [Page 23] 
  Thursday   4/1/18 &nbsp; awarded bar to D.S.O. Bas &amp; D.S.O Martin [Carly?] 
 Friday 4/1/18. Birdwood visited line &amp; congratulated me on D.S.O 
 Saturday 5/1/18&nbsp; New Huns opposite getting very active&nbsp; had good old artillery duel- no casualties. 
 Sunday 6/1/18. More good patrol work- also good wiring work done 
 Monday 7/1/18&nbsp; Quiet Day. releived by Currie- moved by light Railway to [Romarin?]. 