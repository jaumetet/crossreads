 [Page 27] 
 C/o The Palace Bangalore S. India 
 Dear Mrs. Ferguson 
 I feel I must send you a line. Mr. Duncan [Irvine?] sent me a copy of a letter he had received from Mr. Ferguson. It is such a nice letter and it was so kind of him to write. Frank's people appreciated it very much &amp; so do I, and it is one of the letters which will be kept for Ian, he is a big loser 