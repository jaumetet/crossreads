 [Page 147] 
 Thursday Feb 21 Raining again. Davis transferred to 14th A .G.H. Friday Feb 22 Wet day. Anzac Anzac mounted Div arrived Jericho this morning Casualties light. Saturday Feb 23 Showery day usual routine. Sunday Feb 24 1st Brigade &amp; N.Z returning tomorrow. Scottish met with heavy casualties. Monday Feb 25 Jacko over today. Dropped few bombs. 
