 2234 Pte Christopher John ROBINSON 
 Son of Terence and Fanny Robinson, 
 Short St., 
  Mudgee . N.S.W. 
 Shop assistant. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Single Roman Catholic &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Previous experience in 41 Infy., Citizens Forces. 
 enl. 6/4/16 emb. 22/8/16 with 4th reinfs., 45 Bn., on H.M.A.T. 18, &quot;Wiltshire&quot;, &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; from Sydney. &rarr;&nbsp;England. 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; stated age then 19 
 Died of Wound 3/4/18, aged 20.&nbsp; Buried. France 62. Dowellens Communal Cemetery, Extension No. 1. 
 Possibly wounded at Lernancourt, 28/3/18. 