 [Page 19] 
 We are all just longing to complete this pleasant trip and see the good "old '[indecipherable]'. 
 Tuesday 11th Fine day sea a trifle rougher than the pretty well absolute calm which we have had since Cape Town. "Demosthenes" disappeared to the N.E. evidently going to the Canary Is or Gibraltar. The chaps on board her are from Q.land &amp; have been on board about a fortnight longer than we. 
 Wednesday 12th Fine day but sea a little rougher than usual. Buzzer test in morning. Ship preparing for port. 
 Thursday 13th Another fine day but nothing unusual. 
 Sailing ship in full sail passed about 6 p.m. Notification given to all ships from Laconia that a derelict schooner was sighted July 1st in a certain longtitude &amp; latitude through which we pass about Saturday. 
 The English twilight is now being experienced. I read in it until about 7.30 pm, which equals 7-50 p.m. in relation to the sun in Melbourne (Melbourne time being 20 mins. fast. 
 Friday 14th Great day. Universal kit bags bought up from hold. Quite pleased to see the old bags again. Marching order equipment extracted and as much of the contents of the sea kit bags placed in. 
 Passing ships becoming a little more common. Owing to booms being painted could not swing hammock so slept on the deck in open. 