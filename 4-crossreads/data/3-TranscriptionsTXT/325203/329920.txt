 [Page 19] 
 A WISH. I wish that I had lived in days gone by, With Thestylis and Corydon to play; To pipe my oaten flute and sing my lay, With brother shepherds in my songs to vie. 
 To be the sweetest singer would I try; In lilting lines describe the happy day. And when my song was finished, hie away To tend my flocks upon the uplands high. 
 For in these modern days we think too much Of worldly pleasures, selfish needs and fame, We only strive for riches till we die, And though there are a few who look on such As meagre aims, and try to play the game, I'd rather I had lived in days gone by. 
 B.J. ROBERTS. 20/5/19. 
 [Transcribed by June Pettit for the State Library of New South Wales] 
 