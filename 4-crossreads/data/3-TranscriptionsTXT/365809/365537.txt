 [Page 4] 
 New South Wales Premier's Department  F.J./E.J. 7th September 1917 P.O. No. A 17/5645 
 Dear Judge Ferguson Referring to the Premier's letter of the 23rd January last, the Acting Premier desires me to inform you that he has now been advised by the Agent General to the effect that he duly communicated with the Mayor of Boulogne and received in reply a communication, of which the attached is a copy 
 Yours faithfully Fbf. Tremlett Acting Secretary. 
 The Honorable Mr. Justice Ferguson Supreme Court. SYDNEY 
