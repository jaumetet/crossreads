 [Page 87] 
 [21 Th] Mounted Guard 5pm  6-30pm. taken off guard, &amp; [warned?] for Blighty leave 7-15pm  Went to H.Q. for medical inspection 
  London  
 [22 Fri] Left camp by G.S. waggon at 8-30am. Arr. LUMBRES 10am. Left by train 10-30am arr. Calais 3-30pm. Left Calais 5-30pm  Arr. Dover 7-30pm, left Dover 15 min. later, arr. London about 10pm  Marched to H.F. Rd. Tea. Paid &amp; clothes. Into bed about 1am at War Chest club. 
 [23 Sat] Breakfast 8am.  then went to Commonwealth Bank re. money. Spent the rest of the day in London. Left Kings X 10-30pm for Edinburgh 
