 [Page 136] 
  Clisy  
 BAYON-VILLES 
 [11 Sun]  2nd D.A.H.Q.  
 About 4pm got hit with Piece of Shrap in left forearm, taken to No. 53. CC.S 
 [12 Mon] 12 noon taken by red &amp; train to ROUEN "The best of attention from the Red + staff. 
 [13 Tues] Arr.  ROUEN  at 2am. then taken from station to No. 5 General Hospital by motor. (about 3 miles from station) Into bed just on day break. 
 [14 Wed] Had Xrays on arm  Piece of Shrap still in Fritz over Rouen bombing  Had to get out of Marquee &amp; get into sand [baged?] Hut. Raid over in an hour 
