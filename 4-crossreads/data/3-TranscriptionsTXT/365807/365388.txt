 [Page 5] 
 His case is one of great interest because functional paralysis is rare with an actual head injury &amp; because the paralysis corresponded to the hemisphere which had been concussed. How much real sensory loss &ndash; I mean that is due to actual pathological changes &ndash; can be known only after the psychical phenomena have cleared up but as I said we do not think there will be much. He has improved considerably since he came in. 
 I hope you will come &amp; see him often. 
 With kind regards Yours sincerely George Riddock 