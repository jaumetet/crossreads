 a5763198.html 
 157 
 -3- 
 portion of the ribbon. 
 &nbsp;&nbsp; The awards for the 29th of September to 2nd Oct-ober stunt are not all out yet, only the lowest, the M.M&#39;s are out, and my boys have scored four of them, I&#39;m hoping for another D.C.M. for a Sergt. and a Military Cross for an officer whom I recommended.  Later  for this battle I recd  Bar  to M.C. 
 &nbsp;&nbsp; Up to now there is, I think, no more news, so I&#39;ll cut off to bed now. 
 &nbsp; 
 &nbsp;&nbsp; Best wishes from 
 &nbsp; 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Harold. 
 &nbsp; 
 C.H.Peters. 
