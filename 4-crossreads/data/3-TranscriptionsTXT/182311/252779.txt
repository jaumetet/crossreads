 a5763059.html 
 47 
 -4- 
 of course we did so, breaking our journey for four hours in Portsmouth, seeing (only at a distance) the Brittanic, the largest vessel in the world, now employed as a hospital ship; the City Walls, Tudor House, Bargate, Norman House, all in the few hours, and as well a call of about half an hour&#39;s duration on Miss Bound, a friend of Miss Hayes.&nbsp; Then back to camp and to hard work again.&nbsp; Ah well - - 
 &nbsp;&nbsp; With love. 
 &nbsp; 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Harold. 
  Charles H Peters&nbsp; [signature]  
  Capt  
  then Lieut  
  38 th  Bn&nbsp; A.I.F.  
 C.H.Peters. 
