 a5763174.html 
 137. 
 -3- 
 intense training and organizing, but &#39;tis well back from even the sound of guns and is a very pretty spot, the weather is most propitious, an early springtime. 
 &nbsp;&nbsp; Well, cheerio for the present, more later, 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sincerely yours, 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; HAROLD. 
  Charles H Peters&nbsp; [signature]  
  Capt  
  then Lieut  
  38 th  Bn A.I.F.  
