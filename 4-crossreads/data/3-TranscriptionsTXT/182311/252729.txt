 a5763009.html 
 iv 
 
 
 
 &nbsp; 
  In England . 
 &nbsp; 
 
 
 November 4.1916. 
 
 From Lark Hill. 
 (Final Leave; London) 
 
 
 60. 
 ....... 
 
 
 
 November 19.1916 
 
 From Lark Hill. 
 (Training for War.) 
 
 
 65. 
 ....... 
 
 
 
 &nbsp; 
 
  THE WAR . 
 &nbsp; 
 
 &nbsp; 
 
 
 December 6th1916. 
 
 Arrival in France. 
 (from an unposted letter) 
 
 
 69 
 ....... 
 
 
 
 December 8th1916. 
 
 First Impressions and 
 Experiences of Trenches. 
 
 
 70 
 ....... 
 
 
 
 December 12.1916. 
 Moving up to the Trenches. 
 
 74 &nbsp; 
 ....... 
 
 
 
  1917 . 
 &nbsp; 
 &nbsp; 
 
 
 January 6th 1917. 
 Early Scouting &amp; Raiding. 
 
 77 
 ....... 
 
 
 
 January 30.1917. 
 A Raid and Scouting. 
 
 79 
 ....... 
 
 
 
 February 3.1917. 
 Billets and Camps. 
 
 81 
 ....... 
 
 
 
 February 2.1917. 
 Training for &quot;The Big Raid&quot; 
 
 84 
 ....... 
 
 
 
 February.29.1917. 
 The Raid on Pont Ballot. 
 
 86 
 ....... 
 
 
 
 March 5th 1917. 
 
 Investiture with M.C.Ribbon 
 by General Sir Reginald 
 Plumer. 
 
 
 90 
 ....... 
 
 
 
 March 18th 1917. 
 
 Attached to H.Q.of 47th 
 Divn.13.E.F. as Liaison 
 Officer for  Training . RAIDING  
 
 
 92 
 ....... 
 
 
 
 April 15th 1917. 
 A Chimney O.P. at Houplines. 
 
 97 
 ....... 
 
 
 
 April 31st 1917. 
 
 Winter and the Coming of 
 Spring. 
 
 
 99 
 ....... 
 
 
 
 June 15th - 1917. 
 
 Investiture by H.M. the King 
 George 5th with the Military 
 Cross.&nbsp; Travelling in France - 
 Duty Leave. 
 
 
 102. 
 ....... 
 
 
 
 July 15th 1917. 
 Messines: After the Battle. 
 
 106 
 ....... 
 
 
 
 August 14th 1917. 
 &quot;Lorry-Hopping&quot; to Les. 
 
 110 
 ....... 
 
 
 
 Sept. 6th. 1917. 
 
 Central Training School 
 (Le Havre. The Bull Ring) 
 
 
 112. 
 ....... 
 
 
 
 
 &nbsp; 
