 a5763195.html 
  154  
 -6- 
 you immediately revert to Lieut, goodness knows I&#39;ve been one long enough already. 
 &nbsp;&nbsp; While on leave both in London and in Edinburgh I stayed at Officers Clubs, conducted by the Y.M.C.A.,  ^  Staffed  by ladies of leisure who give it the touch of a refined home and it is a very pleasant change afte r&nbsp; camp and hotel, and though the tariff is reasonable, almost inexpensive, still it is not by any means charity, but rather goes to show that even the titled ladies of England are shrewd and careful managers or manageresses in these days of economy and artifice. 
 &nbsp;&nbsp; This sleepy village (Vismes-Au-Val) is now the scene of much military activity for though resting, we are having plenty of training and hardening for future battles though all hope that we have seen the last. 
 &nbsp;&nbsp; That is all tonight, I&#39;m tired and it is eleven p.m.&nbsp; tomorrow a big parade and then the Brigade Sports meeting. 
 &nbsp;&nbsp; With best wishes, 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Harold. 
  Charles H Peters&nbsp; [signature]  
  Capt  
  38 th  Bn A.I.F.  
 C.H.Peters 
