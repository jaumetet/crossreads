 a5763157.html 
 Christmas Dinner at the Chateau d&#39;Rose at Armentieres&nbsp; While we were out on the 23rd November we celebrated the first anniversary of our arrival in France; we were in hutments, chose a big hut for Officers&#39; Mess, hired a funny old piano from a nearby Estaminet, and had a special dinner and evening and with song and story passed a memorable night into the &quot;Book of Memories.&quot; 
 &nbsp;&nbsp; I&#39;m feeling quite fit and as I&#39;m getting more exercise even the rheumatism has left me pretty much alone, and I&#39;m like a giant refreshed or a young lamb skipping for joy or some other silly thing. 
 &nbsp;&nbsp; Our Batmen have just brought in a coke fire in a brazier made from an oil drum, so all the world is bright and gay for we are delightfully warm.&nbsp; Birdy is writing too, so there are no interruptions.&nbsp; &quot;Elections&quot; in a couple of days time, or rather the Referenda; of course officially I cannot do anything to influence the voting, but personally I am doing what I can to stem the &quot;No&quot; idea which is I feel, very strong.&nbsp; I&#39;ve ordered a copy of the A.I.F.Christmas Book to be sent to you direct from the publisher as soon as it is ready, it will be a fine Christmas Card, please accept with all the Best Wishes for Christmas and the New Year, 
 From 
 Yours Sincerely, 
 HAROLD. 
  Charles H Peters&nbsp; [signature]  
  Capt  
  then Lt  
  38 th  Bn A.I.F.  
 C.H.Peters. 
