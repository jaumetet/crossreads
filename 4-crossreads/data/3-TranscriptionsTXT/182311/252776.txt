 a5763056.html 
 44 
 No.6. Camp. 
  Lark Hill. Sept. 9th 1916 . 
 My Dear Ones, 
 &nbsp;&nbsp; I think when I finished off my last letter, it was up to the stage when we toddled up to bed, quite weary at &quot;The Esplanade&quot; Ryde, Isle of Wight. 
 &nbsp;&nbsp; The morning found us up quite early at9o&#39;clock we were in a car with a guide who was also driver and on our way as our American cousins say to &quot;do the Island.&quot; It is a great pity that we had to &quot;do&quot; it for the Isle is a pretty place, even the Kaiser loves it, they tell a story on the island that the Kaiser has promised not to ravage it but keep it intact and hand it over to his son as a birthday present,  when  he gets it, of course. 
 &nbsp;&nbsp; Fashionable Germans for years past have spent their honeymoons on the isle and since the war many of them have been interned.&nbsp; The proprietor of &quot;The Esplanade&quot; was an Austrian who though not interned had to leave the Island, his s little daughter, Dorothy, a pleasant capable girl of 22, carries on very successfully; she treated us all very well and hearing that I had made unavailing efforts to buy Guide Books presented me with their own copy.&nbsp; I will post the guide home, and you can if you wish, read fully of the places visited in our helter-skelter over the twenty-three miles by thirteen miles at its longest and widest points.&nbsp; One thing too that pleased me greatly was the number of literary men who had lived or stayed upon the island, the better known 
