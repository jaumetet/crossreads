 a5763211.html 
 170 
 E 
  COPY . 
 &nbsp; 
 10th AUSTRALIAN INFANTRY BRIGADE. 
 3rd Division, 
  A.I.F . 
 &nbsp; 
 General Staff, 
 57th&nbsp; (W.LANCS.)&nbsp; Division, 
 9/4/17. 
 &nbsp; 
 No. G. 139/3. 
 &nbsp; 
 Headquarters, 10th Australian Infantry Brigade. 
 &nbsp; 
 Reference Lieut.C.H.Peters, M.C., 
 38th Battalion.&nbsp; This Division would be very greatly obliged if this Officer might remain with us another eight or nine days, as he is now training the third party and will by that period have given assistance to one party from each Brigade. 
 &nbsp; 
 &nbsp;&nbsp; He has been invaluable to us and has done most excellent work, and a request for a few further days of his services would not be asked for from his Unit after the courteous way they have placed his services at our disposal, if it was not felt that it would be a pity from our point of view if all three brigades did not get the benefit of his experiences.&nbsp; The final Brigade training will be over by the 20th at the very latest, and probably two ro three days earlier, when Lieut.Peters will be given instructions to rejoin, if you would be kind enough to allow us to have his services for these few extra days. 
 &nbsp; 
 (SIGNED.)&nbsp;&nbsp; X&nbsp;&nbsp; R.Y.BROADWOOD, 
 &nbsp; 
 Lieut-General, 
 Commanding 57th Division. 
 &nbsp; 
 x&nbsp;&nbsp; see other letter. 
 &nbsp; 
 D.H.Q. 
 9/4/17. 
 &nbsp; 
