 a5763081.html 
 2&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2 
 &nbsp;&nbsp; We (5 Officers of A Coy) were very glad to see his pictures &amp; his grounds too were glorious for birch &amp; elm, maple &amp; beech were either burned or bleached with the fire of autumn.&nbsp; 
 &nbsp;&nbsp; After this visit we went on to Salisbury had a quiet civilized dinner &amp; then  went  after making a few purchases back to our Hut on Lark Hill. 
 &nbsp;&nbsp; From Friday the 10  th   to Monday the 14 th  November has been leave; &quot;Indulgence Leave &#39;twas called &amp; &#39;tis our final leave for embarkation approaches very closely.&nbsp; 
 &nbsp;&nbsp; &quot;Up to London to look at a King&quot;, &amp; for other purposes too I went, other sights, purchases of Kit to make, friends to be met, &amp; shows to be seen. 
 &nbsp;&nbsp; &quot;The City of the World&quot;, London, so huge, so wonderful &amp; how important the great clearing house of the world&#39;s commerce, the heart of thr Empire &amp; since &quot;mud built London by the Thames&quot; it has been the cradle of our race. 
 &nbsp;&nbsp; It really does  thrill  give one a thrill 
