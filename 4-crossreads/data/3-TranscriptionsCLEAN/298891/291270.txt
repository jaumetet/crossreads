  
 1917 July 
 6 Friday 
 Arrived in Rome at 10. Clear bright weather. Kindly arranged for by Mr Beaumont of B.R.C. Thos. Scott Alexander, American, took us around Rome in afternoon. Very tired. Saw Dr. Jamie McLaren off for Taranto. We waited over on acct. of luggage. Noted Italian telegraph poles like miniature Eiffel Towers decorated by large necklaces. Struck by majesty also deathly barrenness of the Alps. 