  
 21 Friday 
 Came on to drizzle in afternoon. Sister Saunders & S had tea in Serbian camp with Prof. & he gave me a Serbian Badge. Dancing in evening in mess tent. During forenoon an A.J.C. boy was brought in with both hips dislocated. Pollard & I had Matron's tent till 3 p.m. she copied music for me. 