
1917 July
12 Thursday
Dropped anchor in Navarino about 9 o'clock a.m. I stayed in my bunk to escape parade & was tired as Russell & Lindsay kept me up late as they played cards & sister smoked incessantly. Left at usual hour in evening. Tired of Capt. Sutherland & Dr Waldron at our table. Ground swell made me sick for a while. Miss Scott Russell also sick. Had a wild headache but it passed.