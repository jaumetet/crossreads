  
 CASH MEMORANDA 
 Date.          Particulars            Received 
 Feb          For Richmond article           in S.M.H.                17/9. 
 Sat. 5th 
 Terribly, terribly cold 
 Sunday 6th 
 Did odds & ends in tent 
 Monday 7th 
 Serb Christmas. Saw wards & had cake with Treasurer then Dr De Garis. Reid & I went to Salonique Very warm as we got down. Australian dinner at Wht Tower. Staying at 52nd 
 Tues 8th 
 Ran around to S.W.H. near camp & back to 52 for lunch & afternoon tea. Concert & refreshment after at night at M.T.O. officers' mess 