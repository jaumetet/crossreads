  
 May 7th Tues. Grand Concert by Aussies in City Hall. Have been going all over the country seeing everything worth looking at. Went one day w/- large party to town of Stellenboseh & had a fine time. 
 Sun. 19th May Orders to pack up. 
 May 20th Monday Embarked on TSS Tofua. 
 May 21st Tue. Pulled out at dawn & sailed about 11 a.m., very rough & stormy. 
 May 22 & 23, Wed., Thur. Still very rough & rolling, tucker & quarters pretty good. 