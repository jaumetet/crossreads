  
 About the 20th I was allowed out of bed.  From then until now (Jan. 10th) I have been getting steadily stronger.  I am able to take fair walks without undue fatigue.  Have received letters from home and am feeling at peace with the world. 
 On 12/1/18 was sent to Military Hosp. at Shorncliffe (Sandgate).  Spent several slow & rather miserable days waiting to be sent to the Aust. Base Hosp. for a medical board. 
 16-1-18 arrived in No. 3 A.A.H. at Dartford, examined by M.O. & marked for Board. 
 Jan. 25th Board marked me C2 & am now waiting to see the Major. On leaving Dartford I proceeded to No. 2 Command depot at Weymouth. Went on Furlough from 14-2-18 to 1-3-18 