  
 Several of the officers are old friends of mine.  The village consists of a farmhouse & a large heap of Bricks. 
 May 8th, Tuesday Started at 9 p.m. for the Front line.  After a strenuous march we reached our positions near Bullecourt.  Mine was with a Gun in a strong position, one of the forward positions on the Hindenburg Line.  Heavy shelling all night. 
 Wed. 9th Still in Trenches 
 Thur. 10th Still in Trenches 
 Friday 11th Fritz started shelling at 6 p.m. & kept it up until 6 a.m. on 12th, in all, a 12 hour bombardment. The Huns evidently intended making an attack but our division (5th) took the job out of their hands, hopped over the top at 4 a.m., succeeded after 3 attempts to take trenches & over 300 prisoners.  We stood by our gun all night despite the heavy shell fire & only I got a crack on my tin hat. 