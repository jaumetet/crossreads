  
 out to the wreck and after about 1/4 of an hour, return to the cruiser. The cruiser then fired 8 shells (presumably 6?) into the wreck and we saw it sinking as we passed out of sight of it. Whether anyone was rescued we do not know. 
 6th  Inspection Full Dress with webbing Equipment.  From this morning till end of trip lifejackets or belts are worn continuously night and day. 
 7th  The Ship's Mascot a bull dog named "Wowser" Was Killed to-day by a fall from the boat deck on to 