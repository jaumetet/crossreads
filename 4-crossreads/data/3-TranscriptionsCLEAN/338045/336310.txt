  
 1916  3rd September  3rd October - Tuesday A fine day & not cold.  Nothing further about leaving.  Very little work.  Study French most of the day.  A walk & visit to the barber & learn French at home.  To bed at 10-30 p.m.  No news of importance from the front. 
 Expenses - Hair Cut & shampoo - 1 fr. 75 
 4th  Sept.  October - Wednesday Cloudy & a little rain.  An unpleasant day & plenty of mud, but not cold.  No further news of leaving.  Take a walk after tea for my watch.  Diptheria Cases (2) & we are all examined.  Leave starting.  Not much work.  Fighting at the front 