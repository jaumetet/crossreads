  
 Nov 7th - Tuesday 
 Raining & cold again to-day. Do just as I wish in hospital. Out at 6-30 am & spend the day in various ways, reading, writing & walking around. Am here for rest & get it. Concert in theatre to-day at 3 pm & another at 8 pm. 
 Nov 8th - Wednesday 
 Cold to-day, but clear. Really a nice afternoon. Usual resting around the wards. Obtain my greatcoat from the store in a beautiful condition. No concert. Have a game of billiards. Bed 9 pm. 
 Nov 9th - Thursday 
 A grand day. Awake 6-30 am 