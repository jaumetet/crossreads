  
 walk to Pont Corneille & take tea in the Restaurant Patisserie in Rue de la Republique.  Return home & to bed after a chat.  During the day we observed some Chinese who had arrived for munition work, some weeks previously in the native clothes, & now in European dress, really smart, soon civilised.  Also some South African natives of the contingent for wharf work, who speak English well. 
 Expenses - 2 f. for tea. 
 2nd October - Monday Cold & raining all the morning, but fine in the afternoon.  Some of us of A.I.F. Headqrs. (four) are taken 