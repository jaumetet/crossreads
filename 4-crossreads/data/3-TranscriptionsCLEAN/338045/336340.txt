  
 as he is called away just as he comes to me.  Very cold day. 
 Oct. 30th - Monday Blowing like seven furies.  Cold & rain.  Great weather.  Seen by the doctor this morning & after a short examination marked for England with heart trouble & debility.  Probably a weak heart for all I know;  a nice ending to my trying to hang on whilst feeling knocked up for so long in anticipation of getting to the front.  Feeling the after effects now & stay in bed all day.  Expect to leave for England to-night. 
 Oct. 31st - Tuesday Blowing a gale all night & our tent almost over.  Raining 