  
 first view of snow falling as it continues until about 11 am. Some fun in the ward with snowballs. A drizzling cold rain & sleet follows the snow, but having my leave pass determine to go out & set forth at 2 pm well wrapped up. Bus to St Mary Cray, meet my friend Miss Schwerer & go home. Formal introductions & a warm fire & have tea. Some music & yarns in the warm room until 8.30 pm & set out home, arriving late. Very tired & soon in bed. 
 Nov 19th - Sunday 
 Awake at 6.30 am but too tired to get out until 