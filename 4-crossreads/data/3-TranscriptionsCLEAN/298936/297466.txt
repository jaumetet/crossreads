  
 November, 1915 
 Monday 8 
 Beach dressing station. 
 Working all day & part of the night, although I felt very ill. 
 Move on the left side in progress. 
 Beachy landed shell (from howitzer) clean down the hold of barge at pier opposite one dressing station. 
 One killed & two  wounded one simply cut to pieces. 
