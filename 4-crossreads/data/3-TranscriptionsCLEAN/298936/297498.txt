  
 Notes For 1916 
 A Soldier's Prayer 
 Sing me to sleep where bullets fall Let me forget the war & all Lousy my clothes are & dirty my feet Nothing but bully & biscuit to eat Sing me to sleep in some old trench My only cobbers ground lice & stench Stretched out on my old waterproof Dodging the raindrops from the roof 
