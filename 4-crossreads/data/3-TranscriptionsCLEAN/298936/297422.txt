  
 September 1915 
 Thursday 23 
 Put on fatigue work fixing up tents. 
 It is very healthy here but the rough wind makes the sand a nuisance at times. 
 A collection of Turkish prisoners is here, and plenty of Ghurkhas French & Tommies. 
 The island evidently belongs to Greece for there are a few Greeks in the mud hovels of the village. 
 The hospital seems to be arranged on good lines for all the marquees are in orderly lines & paths are edged with white stones. 
