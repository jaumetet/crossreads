  
 September 1915 
 Sunday 19 
 Holy communion at 6 a.m. Did a little more work today. 
 On lifebuoy watch for 24 hours in charge of three men taking 8 pm to 11 pm watch. 
 Arrived at Lemnos at 4 a.m. slowly steaming up Mudros harbour between 2 rows of torpedo nets & mines. 
 Destroyer ? vanguard all day, both her & our own boat zig zagged like fun  for danger of torpedoes very great indeed. 
 Mudros harbour full of cruisers, hospital ships & transports. 
