  
 October 1915 
 Saturday 2 
 Heavy duty in Artillery Lane which was no different to the fatigue of yesterday with difference of staying all night. 
 No cases to [indecipherable]. 
 Getting quite used to Beachy Bill now. 
 Last night there was a little doing on the left for at sunset there was a heavy artillery with mostly the firing being from our own guns. 
 After dark there was heavy artillery fire which of course sounds like a rifle range only heavier with the addition of the sharp repeated cracks of the machine guns. 
