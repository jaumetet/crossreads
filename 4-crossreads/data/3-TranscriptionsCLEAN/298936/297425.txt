  
 September 1915 
 Sunday 26 
 Rose at 5 oclock & went to Holy Communion at 6. 
 There were only 3 there but I felt the benefit of it. 
 Had a curious dream last night.  Mr. & Mrs. Hart, Bobby & Rita were washed out to sea & I saved Rita & Bobby but could not reach Mr. & Mrs. Hart. 
 Rita must have been thinking of me a lot today for I feel she is close to me. 
 Left camp at 7.30 carrying pack on back, blankets & waterproof & also black kit bag, marched 4 miles to jetty, only to find we were here a day too soon so had to march back again ( left kit behind and went to village in evening 
