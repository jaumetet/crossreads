  
 July 1915 
 Friday 2 
 Worked in office during the day with thoughts ever of Rita. 
 Not much work but a lot of fooling. 
 Feel discontented, [a very wicked feeling] chiefly over not being promoted, but secondly over doubt whether I shall see active service. 
 Hospital work is however [indecipherable] .I must steel myself to disappointment & put a brave face on work, whatever it is.  I am lucky to be able to serve my country even in a Hospital. 
 Cannot get over fact that Sgt Christie passed me over when I thought he had faith in me. A wound to my cursed pride. 
