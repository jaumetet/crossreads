  
 September 1915 
 Saturday 18 
  Thursday 16  
 Spent a rough night on deck in the rain but overcame the difficulty by wrapping myself in a waterproof. 
 The nights are getting much colder now, & the days cooler. 
 Usual parades & drill with the addition of allotments of places & duties by lifeboat & a lot climbing around discovering falls & man ropes. 
 If a torpedo hit us I am willing to wager that only half the boats would get away, for the difficulties of a list would require far more organisation than we have. 
