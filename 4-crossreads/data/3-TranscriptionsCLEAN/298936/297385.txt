  
 July 1915 
 Friday 30 
 Arrived in Melbourne at 1.30 & berthed at Port Melbourne 2.30. 
 Mails revealed nothing for me. No leave granted. Men annoyed at first. Talk of breaking leave & at that Capt Hordern attempted to procure it from Commandant but got no reply.  Men broke out in a body at 4 p.m. but repulsed by guard. 
 Fresh guard brought up & fixed bayonets ready. 
 Another attempt at dark ending in bloodshed 
 Several men injured in the charge made by 
