  
 Sunday 9.1.16 (18) I dodged Church Parade this morning. We had to don Breeches & Leggings & it is a terrible strain after a week without socks & in shorts all the time. I tried to write letters this afternoon but half-wrote 2 & gave it up. 
 We had a sacred concert on the Prom deck from 8 till 9. Dave Niness & I got right down the back with our nurse girls & we had chocolate & almond rock & plenty of talk & fun & of course did not hear a bit of the concert. 
 Last night it rained again but I found a dry spot. Ellis slept on deck too. 