  
 places being taken very soon by ships coming through from Port said. 
 Tuesday 25.1.16 (33) The crew put all the boats in the water this morning & tried to get the launch to kick for about 2 hours. Like my old bike she went for about a mile & for the last 11/2 hours she has been on the drift. She must be down near the canal entrance by now. 
 A P & O liner from Australia came in while we on Roll Call this morning. 
 We went alongside at 5 pm & 72 of us fell in & were issued with 16 biscuits & 1 lb of bully beef per man. After the 