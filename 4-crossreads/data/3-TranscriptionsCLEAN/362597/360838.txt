  
 My Diary 22.12.15 We embarked at 11 am at No 2 W"loo wharf & put out into the stream at 3 pm & set sail at 5.50 pm. Sea very calm & we slept well. 
 Thursday 23.12.15 (1) Sea still calm & lovely breeze blowing. Had my hair lopped. Full Dress parade & boat drill. 
 Dinner & Tea was a huge scramble. Plum pudding for dinner very good. Passed Gabo [Island] & can see the shore all the while. Very rugged & uninteresting. Boat very slow but very steady. Plenty of fellows sick but 
 the jig-jig of the boat does that. Saw a sea-lion while on parade. 
 Friday X'mas Eve (2) Sea still smooth just like a harbour trip. Washed some togs & took them out on to the f'k'slehead [forecastle head] & sat alongside till they dried. 
 Saturday X"mas Day (3) Sea still very calm we are between Melbourne & Adelaide & have not sighted land since yesterday at noon. Spent the morning in the fore-hold & looking for our piano but was unable to find out. 
 Broached a tin of preserved fruit & one of prunes. Had a bonza dinner & read magazines all the afternoon. We held a concert in F 