  
 26th April Thursday To day I have been exerting myself in the role of cart horse, on blanket fatigue with the Q.M.Sgt. Yesterday was Anzac Day, but being our duty week we worked (A Section), & were promised a holiday next week. As a matter of fact I managed by luck to get a couple of hours of after dinner and in the evening I went to a show at the Empire with Alec Nunan, whom I have dug 
