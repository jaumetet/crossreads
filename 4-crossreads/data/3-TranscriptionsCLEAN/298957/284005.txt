  
 been set as follows 
 (Small sketch) 
 Wounded are to be carried from the sand hill positions, occupied by infantry, to the dug-out on stretchers. From there they will be taken on camels to our camp and on to the road by sand carts drawn by mules. At the road Ambulance waggons will be waiting. The sand carts have arrived already amidst cheers from the boys. 
