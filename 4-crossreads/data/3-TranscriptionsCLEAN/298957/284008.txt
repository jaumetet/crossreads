  
 is to be prepared and left standing for the unit who will take it over from us shortly. 
 9th May (Tuesday)  Hot. Carpentering all day. The Water tanks at Rail head are being covered all over with sandbags for protection against aircraft. Today I was informed that I can go to Cairo on Thursday. 
 10th May (Wednesday) Still warm. Continued carpentering. Left for Cairo in party of 5 at 6.45 pm. Went as far as Ferry Post in light service waggon. Forms had been fixed in it for seats but they all broke before we had gone 100 yards. The idea of sending us 
