  
 Rumours that 8th Brigade are to be used as reserves in Ismailia whilst all the other troops form the 1st line extending as far as Serapeum. 
 22nd Jan (Saturday) Colonel Shepherd arrived to take charge of our unit. Stretcher carrying. Very striking scene as the H.M.S. "Implacable" came back up the Canal. With bands playing and bluejackets cheering from on board this magnificent ship, and the troops ashore giving deafening cheers, the incident was most impressive. 
 23rd Jan (Sunday) Church Parade. Sergeant from 29th 
