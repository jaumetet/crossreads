  
 18th Jan (Tuesday) Long route march along the fresh water canal from Einghosien to the Bitter Lakes then back along the Canal. First Aid lecture in the afternoon. Engineers are rapidly constructing elaborate trenches on the other side of the Canal. Today General Godley carried out an inspection of the camp. A monitor passed in Suez direction, also a hospital ship and big P. & O. Indian Liner. 
 Wednesday 19th Jan. Stretcher drill and lecture on application of bandages. "Kitchener's Vultures", big birds for eating up rubbish, such as 
