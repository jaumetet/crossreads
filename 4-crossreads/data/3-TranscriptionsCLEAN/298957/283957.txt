  
 that passed us at Serapeum. A few French warships were also at anchor. Ismailia is indeed a very pretty little French town. On my way back to Tel-el-Kebir the train guard, who speaks English very well, related the full history of the Tel-el-Kebir battle to me. 
 9th March (Thursday) Making mud bricks and ovens for the cooks. About 40 new A.M.C. detail men arrived and are being temporarily attached to us. 
 10th March (Friday) Mail day. On water fatigue for the cook house. Took 4 horses down 
