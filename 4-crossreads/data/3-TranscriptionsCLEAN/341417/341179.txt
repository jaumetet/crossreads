  
 [Coloured postcard of Castle Street, Ludgershall, with note by Vasco.] Right here in the doorway on Sunday in about a foot of snow Portraits while you wait amongst roars of laughter from rosy cheeks and Khaki Australians. 
