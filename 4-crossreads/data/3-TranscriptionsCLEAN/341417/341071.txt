  
 shall have to conclude now, thanking you once again. 
 I remain, Your Obedient Servant (Sgd.) Pte. W. Rowley. 
 P.S. Excuse writing as there is no ink in the firm. Remember me to all the rest of the firm, also their wives and families. 
 A Merry Christmas and a Happy New Year. 
