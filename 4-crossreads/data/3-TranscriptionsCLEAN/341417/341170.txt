  
 Gwen Dear, 
 Just a few dashed off lines. Had a glorious time "up" in London. British Australasian took four pages of sketches. Have sent through their London agent a big batch of sketches for the Sydney Mail to chose from. I feel sure the Lone Hand (Sydney) the "Queenslander" or some of the other illustrated Papers could use some. Keep all money  for yourself . Don't ever send any thing here for me as arrangements are  rotten . So far (3 weeks) have received not a word. Nothing since leaving Sydney, things in a rush. Head too scrambled to think. Address Sapper Vasco c/o British Australasian, High Holborn London. 
 Love for ever and ever from your Vasco. Miriads of Kisses from (who do you think) Same as usual to Jack Jen and Max. Not forgetting Hughie, the children and Peter. 
 [Written sideways in the left-hand margin:] Have just received three letters from the dear little mandolin player. The kisses were awfu' good! 
