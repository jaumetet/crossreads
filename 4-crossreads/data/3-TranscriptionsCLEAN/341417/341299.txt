  
 [The individual pages of this two-page spread are not in chronological order. Transcribed as they should be read; see image for details.] No. 3 Aus. Gen. Hosp A.I.F Lemnos 13/10/15 
 My dearest Amy 
 Many thanks indeed for your chatty & cheery weekly letters, it is jolly to know you write weekly but quite a lot miscarry, however I'm hoping they will eventually catch me up & anyway the ones I do get are very charming & it's sweet & kind of you. The last I recd. yesterday enclosed Nudges with the address of Arties' boys in & I'm very glad to get [indecipherable] as it's hopeless trying to find them out without full particulars. 
 My dear I'm being both  good  and careful - So don't worry: & one really has to be both for theres quite an embarassment of men round:  Stacks of them over in the rest camp those who were in first landing & who escaped more or less unscathed but who - the brave dear things they are - go back cheerily knowing that its a hundred chances against their ever coming back again. It's fairly heartbreaking at times. The stories one hears told so casually, in little terse sentences leave one gasping at times & glad 
 [Letter continues on the next page.] [The end of this letter is written sideways in the left-hand margin of the first page:] pitiful for them not to get a few homely comforts - & we could help there with things sent to us. 
 With much love to you & Prince. I've written him at Foster as I suppose he's there a good deal. 
 Keep on going ahead Amy. it's good to hear of your improvement. 
 Much love From Dun 
