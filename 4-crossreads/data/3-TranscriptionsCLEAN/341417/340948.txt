  
 Well Miss King I have only a few minutes to spare before I will be away and doing so will have to cut my letter short. 
 I have not received the parcel you sent but hope to do so any day now. By the way Miss King I was very amazed indeed to read the cutting you sent me of what Fritz would do if he got  it  to Australia  and all I can say is If? and that he has the AIF to beat before he will be able to carry out his programme. Well Miss King I will conclude with best wishes and Kind Regards to self and all War Chest workers from Yours Sincere Friend Lance Cpl R. Otto. 2nd Btn AIF France 
