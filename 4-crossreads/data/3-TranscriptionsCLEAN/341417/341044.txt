  
 [Message side of the postcard in the previous image.] Don Dunlop 9.8.32 
 P.S. Will write you later. 
 Belgium Apl 16. 1917 
 Dear Norman Just to say that "All's Well" with me, and like "Johnnie Walker" am "going strong". Weather has been intensely cold, - 30 of frost but Spring is trying hard to pop its nose above ground. Kind regards to Mrs. Norman & all your folk. Sincerely yours J A Reid. 
