  
 [On letterhead of Angus & Robertson Ltd., "Booksellers to the University of Sydney, and the Public Library of N.S.W."] 5/10/16. 
 Dear Mr. Ifould 
 You already have a Rowley letter or two in the Mitchell. Here is another. I enclose a copy of some verses by the King who was shot at Rowley's side. King's brother (George) is on our staff and is at the front. 
 Yours faithfully, George Robertson 
 [Signaller George Edward King, No 507, 19th Battalion, killed in action 4 December 1916 in France. George King's letters are in the State Library's collection, in A 2660 Volume 1, letters written on active service, A-L, 1914-1919.] 
 