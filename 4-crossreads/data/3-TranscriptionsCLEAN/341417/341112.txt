  
 also Eddie. Well Dear Mother I will now have to end my letter till I have time to send you another later on. I ask God to Bless & keep you Dear Mother & also Rube in Good health & happiness 
 Till we meet again Your loving Son Will xxxxxxx 
 [Note in red at the bottom of the page:] From Sgt. Major W. B. Schaeffer, Killed in the trenches at Anzac, 20th May, 1915 
