  
 when he was at Malta & besides writing to say he was having splendid attention, sent a late note to say he was going to England The trip was fully earned, anyone in my estimation, who had done a couple of months trenches being at least entitled to a trip round the world. 
 Young ? Concannon wishes to be remembered to Uncle William & sends kindest regards. He is our cook. 
 Hoping all are in the best of health & enjoying life, I remain Your's Sincerely Robt D Marrott 
 P.S. What's happened Easts & Souths this season? They say here Easts want "Dally" 
 [Private, later Lance Sergeant, John Concannon, 1st and 14th Field Ambulance.] 
 