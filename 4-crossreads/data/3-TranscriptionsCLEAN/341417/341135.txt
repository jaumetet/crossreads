  
 28th May 1915 
 Dear Mr Thompson 
 Thank you very much for your letter and congratulations. It was nice of you to write to me, and I wish I had more time to reply. However we have been in action for some time, and are still very busy - We have got along very well with hardly any casualties, but were gassed rather badly on Whit monday - I have been in H Battery for a fortnight as my O.C. went sick and there was a general changing round of officers - Now he is returned and I am back with the column and rather bored - With very best wishes to Mrs Thompson and yourself 
 from yours affectionately B G C. Simpson 
