  
 leave its mark on the Battalion and its impression on all who know him for no memory of the 4th Battalion in the first two years of its history will be complete without his figure in it whether as Quartermaster in the early days or latterly as Commander of B. Coy. 
 Such of you husband's papers as were not wholly destroyed and his other personal effects have been secured by the Quartermaster & forwarded to the Base in the usual way and I hope that you will receive them in due course. 
 Once more let me tell you of the sense of loss which we all feel and assure you for myself & the rest of the Battalion that you have our very deep sympathy 
 Yours sincerely J. G. MacKay Lieut Colonel commanding 4th Battalion 
