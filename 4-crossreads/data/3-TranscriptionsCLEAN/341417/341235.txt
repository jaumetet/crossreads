  
 capes & other articles  were in most demand. Who wanted ordinary soldiers' equipment when we were clearing out? 
 Some amusing scenes I witnessed included men stripping all their underclothing & uniforms off & getting into new clean clothes, right on the spot. I threw my old hat away & got a new cap. 
 As the men were carring the goods back to the trenches, afterwards only those who had a written order to procure goods were admitted to the stores. 
 I might here state Anzac Cove where all the waterside traffic had been carried on from the date of our original landing till a few months ago, had not been used. Besides being enfiladed by a gun called Beachy Bill the sea in rough weather would come right up to the base of the hills. The jettys here too were washed away some time ago. 
 Again, Anzac Cove was under observation from Gaba Tepe so all over  water  sea traffic was carried on at Walkers & Williams piers situated a few hundred yds north of Anzac & around a short point. 
 A tramp steamer was purposely sunk here about 4 months ago to form a breakwater for the jetty. It was fairly effective in the rough weather we had 
