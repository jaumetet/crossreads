  
 Lord Roberts, as our leave expired at 9 P.M. we had to catch a train at 6 P.M. it was with great reluctance that we did so. 
 Thursday was spent working at the Docks loading our equipment into Railway trucks. in the evening a party of us visited a Lodge meeting there we met with a reception fit for Royalty. 
 Friday was also spent at the Dock, we were entertained at tea by the Y.M.C.A. after tea we had some music & all had a most enjoyable time. 
 Saturday. we completed our work at the Dock spent the afternoon & evening in the City. 
 Sunday. Church Parade, Afternoon visited Hythe (which is on the other side of the bay) had tea at a quaint old English Hotel. it reminded me very much of our trip to Narrabeen. the last boat returns at 7.15 so we had to return to the City we then visited 
