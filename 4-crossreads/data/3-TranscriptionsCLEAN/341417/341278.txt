  
 [This four-page letter is undated but is written from Egypt prior to Clem Walsh's departure for Gallipoli, and so predates the letter beginning on page 399, written from Gallipoli on 20 June 1915.] 4th Coy. Divisional Train 4th Infantry Brigade N.Z. & A Division On Active Service, c/o Victoria Barracks Sydney. 
 Dear Drs 
 I am still at Heliopolis which is about 7 miles from Cairo, to which it is connected by electric tram, equal to the suburban railways of Australia. 
 The Totalisator is used on the racecourses here but the prize money is small compared with at home. Most of the horses are fine symettrical animals but are only about 131/2  to 141/2  hands high & although seeming to have better stamina, they have not the speed of our English racehorses. 
 Cairo is worth a visit, en route to Europe, if ever you go abroad and can easily be worked in by leaving the steamer at Suez, then a train journey of about 100 miles to Cairo. After a week in that city antother run of 120 miles to Alexandria of 50 to Port Said. 
 I understand some bad reports appeared in the Australian Press concerning the Australian troops in Egypt & I will try & give you the facts. 
 At the time of the "Stinking Fish" Press articles, appearing in Australia, nothing had happened here to warrant 
 [Written sideways in the left-hand margin:] Don Dunlop 9.8.32 
