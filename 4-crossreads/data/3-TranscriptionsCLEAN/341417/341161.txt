  
 Copy from the Sydney Bulletin Round the World with a Pencil 
 April 18th 1907 
 Vasco Loureiro writes from Melbourne. 
 Next Tuesday (16th) I leave for London by the Medic and start the first chapter of my illustrated diary "Round the World with a Pencil". Last year I tried my luck as a Shilling Portrait Sketch Artist in the Hobson's Bay excursion steamers, working the various picnics. As a business it panned out splendidly. When the Melbourne season was over I sought the nimble shilling in Sydney on the Manly Ferry boats & if passengers were scarce I studied navigation & took a turn at the wheel. Then to Brisbane in August, Cains tourists on the Wyandra and the wonderful me at the Queensland shows gave me plenty of work. I was always earning a living. At the Brisbane exhibition my neighboring fellow-artists were a man who sold "your fortune with a lump of butter scotch" for 3d and the Gippsland fat girl who exhibited her bulk for 6d in a tent. One day an 
 [Continued on page 350.] 
 