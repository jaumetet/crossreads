  
 April 1 1918 Easter Monday Bank Holiday Left Glasgow and went to Edinburgh. Saw most of what is worth seeing in the Royal Mile - in the old part of the City In the afternoon Cpl Holm & I went down to the big Forth Bridge which is said to be the greatest bit of steel bridge building in the world. Spent an enjoyable day. 
 1918 April 2 Tuesday I had a further look around the city in the morning and went into a kind of Social in the afternoon given to a party from the Club by Mrs Martin a few miles out from Edinburgh. We spent a very pleasant afternoon. 
