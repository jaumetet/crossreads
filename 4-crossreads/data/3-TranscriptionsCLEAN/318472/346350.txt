  
 5 May 1918 Rogation Sunday Nothing special doing today. In the afternoon I went for a walk out onto the Dinton Rd - then to Barford and from there back to camp. There is a very beautiful avenue on the Dinton Rd called I believe Dinton Avenue 
 1918 May 6 Monday Just the same old Camp routine. I am still looking after No 8 Canteen. Have not heard anything about my Discharge 
