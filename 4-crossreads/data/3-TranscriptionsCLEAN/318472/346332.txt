  
 17 April 1918 Wednesday It is still raining & very cold. The news from the French Front continues to be bad. Baillene has been lost & the situation in the North is grave. 
 1918 April 18 Thursday Weather still cold and miserable. Things appear to be going not too well at the Front. 
 Nothing of interest to report in this Camp. 
