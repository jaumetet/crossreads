  
 16 July 1918 Tuesday Chief excitement was the returned of the boys who were on the Torpedoed D16. I was one of the Conducting N.C.O.s & we had them all in camp about 9.20 P.M. looking rather rough in all kind of clothing 
 1918 July 17 Wednesday Was drafted onto Boat Roll D18 today & transferred into No 3 Company. Expect to leave about Sunday A wet squally day Went to the Palace & Montys Flapper played Fairly good. 
