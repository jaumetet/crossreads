  
 28 September 1918 Saturday Went again before the M.O. in the morning Have to go to Randwick for eye test & boots  Delivered parcel Neutral Bay for Mrs McDonald from W.O. Smith Blighty and France. Went home with Sgt Brindley & had Lunch. Came back home and after tea Ruby & I went into the City* Arrived back about 10 P.M. after pleasant Evening 
 * Went to Watsons Bay & then to city by Boat. 
 1918 September 29 Sunday 18 After Trinity 
 Went to the Domain this morning to see some more of the boys come home. In the afternoon went with Ruby to the Zoo "Taronga" & spent a very pleasant few hours. The New Zoo is a very fine turnout all the animals being allowed freedom almost in the native state in large enclosures Arrived back about - 6 P.M. had tea & put in the rest of the evening writing etc: mostly the latter. Met Cpl Turnley - saw Anderson 
