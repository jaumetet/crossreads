  
 1917 Raided Signal school cookhouse & secured four tins M & V rations.  Bought two loaves of bread, I alone having any money - 7 francs!  Slept in barn. 
 Nov. 16 Left p.m. per lorry for Ramillies-Werquin where we rejoined battalion. 
 Nov. 17 Spell at Ramillies-Werquin.  I left after tea per lorry for Blequin.  Slept in empty house.  No rations or money to buy food. 
 Nov. 18 Battalion arrd. p.m. 
 Nov. 19 Mess orderly.  Left per boat for Tingry nr. Samer, a distance of 221/2 kilometres.  Billetted in large empty house with fire-places - tres bon.  Hope to stay several days. 
 Nov. 20 Kit inspection a.m.  Pay p.m. 
 Nov. 21 Formal parade for 5 minutes. 
 Nov. 22 Ditto.  Settling down, Samer. 
 Nov. 23 On billeting scheme as interpreter with Lt. Marr. 
 Nov. 24 Still engaged on scheme. 
 Nov. 25 Communion. 
 Nov. 26 Parade. 
 Nov 27 Escort to Heningen for court martial prisoners. 
 Nov. 28 Parade a.m.  Borrowed 100 frs. 
