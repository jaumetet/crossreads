  
 Wednesday 11 [10] April 1917 [1918] Ones own woes are so trivial when this awful war is raging - how many brave men lay dead these last few weeks & still they call for more. 