  
 Saturday 29 September 1917 Head washed.  Weather hot & moist.  Ocean perfect, millions of tiny sparkling diamonds & long rollers. Fancy dress ball for Monday next. Our little band - Miss E. Ralston ret'd sister (charge), R.C. Q. Miss Rowe, Meth., Q. Miss Dowie, Meth., Q. Miss Lamb, Doonar, McLoughlin, R.C's, Q. Miss Henry, O'Brien, Gill, Finn, R.C's, Q. Miss Dean, Ramsay, Homewood, C of E, Q. Miss McDonald, Presby., Q. 
 South Australia Miss Dart, Wylie, Roach, Whitfield, Rogerson 
 N.S.W. Miss Holloway, Moreton, Macdonald, Lancer, Lee, Musgrave, Watson. 