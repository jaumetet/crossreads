  
 DATE - TIME - PLACE - REMARKS 
 March 1/2 - Hollebeke - B. took over line raid at time of relief.  Mjr. Henwood Killed. 
 9/10 -  Hollebeke  Crater Dugouts - Releived by 12th proceeded to Crater dugouts. 
 24/25 - Line Hollebeke - Releived 12th in line.  Casualties for March 141 other ranks. 
 April 3/4 - Hollebeke - Bn. releived by British Div. moved to Dezon Camp strength 39-764. 
 4 - Dezon Camp - Bn. left Duzon camp for Caestre by buss. 
 5/6 - Caestre - Bn. entrained at Caestre and left for Somme. 
 6 - Doulanville - Detrained at St. Rock (Amiens), moved to Doulenville, First Fire. 
 7 - Resting Second Fire. 
 8 - Resting third Fire. 
 9 - Vignacourt - Marched to Vignacourt. 
 10 - Rainneville - Marched back to  Raie  Rainneville. 
 12 - Bivouacked outside Amiens. 
 12/13 - 9 A.M. - St. Roch - Marched to Station in morning 13th, entrained for the North. 
 13/14 - 11 P.M. - Hondeghem - Detrained at Hondeghem about 11 P.M. very dark and Germans near Strazeele. 
 14 - Borre - Batt. marched to near Borre in supports.  T.L. Morbecque. 
 19 - Meteren Sector - Bn. releived support Bn. 33 Div. near Besace Farm South. 
 23/24 - Line - Bn. releived 11th Bn. in line. 
 24/25 - Line - Metren Stunt.  Total casualties for April 169 other Rnks. 