  
 scathless. 
 Seems impossible 
 13th May. At 11.30 about 16 of us from "C" Squdn went trench digging on left near flank (Communication trench) Maxwell & self worked together. A pretty lively fire overhead whole time. We had to run for it going & coming back as a sniper was at work on about 150 yds of main track & one man "Irish" was killed & another badly wounded. 
 Godley missed it by a few seconds going down hill with 
