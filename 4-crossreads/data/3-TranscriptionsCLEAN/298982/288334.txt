  
 of any special mention Pretty busy & much to see to 
 Sun 15th Made a biggish round this day. Got Merrington's rag out re tins & waste Poor devil Bathe in evening & a sharp dose of shrapnel as a finale  Camm QM  Many rumours but no news 
 Mon 16th . Busy day. Rumours sunken troop ship with Hampshires on bd Also captured Turkish submarine in nets at Lemnos. Farr Comes from Devon stock. Busy day. New sanitary matters to see to. Visited Courtney's V.B. Yarn we all go to left. Muir return to his Batt. A scheming dog. 
