  
 lively night. 
 We stood by all day Saturday as supports & it rained in the morning 
 Butcher & self remained in firing trench until 9 30 am & the rain turned everything into mud & we were in the hell of a mess Snatched about 2 hours sleep at our spell time in a small hole 
 In the supports again at 7 pm & I was on guard at corner (left.) A very quiet night & we stood to arms at 3.30 At 5 we dismissed Whit Sunday & I was 
