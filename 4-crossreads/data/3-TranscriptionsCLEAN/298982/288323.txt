  
 Wed 21st July I took on cooking for the SP did not go out till noon when went see Tite. The charge is to come soon it appears All preparing for an attack or conter ditto. Fine bathe in evening with Bill Jenkins Tea late & most unusually pleasant free fr m flies 
 Thur 22 Another quiet day Aeroplane making great reconnaissance. Shrapnel vainly trying to bring him down. More yarns of attack. One dead Turk in Mortuary. Shot while trying to cut our wire entanglements. Some shelling in evening by our guns S.P. Corpl slept with his Sqdn who were cutting a trench on side of dead man's gully 
