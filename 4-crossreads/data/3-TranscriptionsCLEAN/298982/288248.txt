  
 Colne about 9 pm. Landing stage Bullelts spitting in the water around now & again on the way. One or two hit the boat. We proceeded up the track towards the trenches & near the top bivouacked for the night to the music (?) of the rifle fire & the sing of the bullets I slept some Our chaps took the whole piece so far jolly quietly & did not show much excitement. 
