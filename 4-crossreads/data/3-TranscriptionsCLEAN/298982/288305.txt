  
 Sun 6th June Road making up Dead Mans Gully morn & in sap 4 - 8 pm on Popes hill noon. Quiet. 
 Called on Tite in evening. 
 Mon 7th. Quiet day. No fatigues. Tom B came up in morning for yarn. Seems well, Bathe first parade noon. Elfick. No shrapnel. Met Cumine later. Tuft hair present fad. Up to Viney's Folly at dark under McCloud. Fine night. Some heavy firing & star shells. Visited Tite & Alexander at 7 - 8 Had short letter from Mater with enclosure. Posted Mtr in F L.H.P. 
 Tues 8th A quiet day in Vineys Folly. Heat unpleasant & no shade. Relieved at 8 15 Some tucker & to bed. [indecipherable] picquet. 
