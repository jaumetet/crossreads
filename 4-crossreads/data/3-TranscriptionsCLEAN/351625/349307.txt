  
 P.S.  Please excuse any mistakes as I never read my letters over.  Doug. 
 France 10 July 18 
 Dear Mum, 
 I suppose it is up to me to write you another letter after getting 3 from you a few days ago amongst a bundle of 13.  I was jolly glad to hear how well & fat you had got during your holiday & only hope you keep so, if you dont be sure & take another such holiday.  You must have had a glorious time at Crookwell & Woodville & all the different picnics you were telling me of must have been great.  They make my mouth water but still they wont compare 