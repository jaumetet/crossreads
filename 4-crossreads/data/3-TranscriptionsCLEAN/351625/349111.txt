  
 The Australian Y.M.C.A. 
 No. 7451, 25 Rein., 2nd Batt. 
 At Sea 
 Dear Mum, 
 It is a week today since we left Cape Town & we have been making a quick voyage in good weather since.  In a few days we expect to reach our next port the name of which you had better try & find out yourself.  We have been keeping very well since we left Cape Town where we stayed about a fortnight & had a tip top time as my letters from there will tell.  One bit of hard luck, my mate Smith was put off the boat the day we left there with meningitis & we have not heard whether he survived or not but think he will pull through as he was a very strong chap & I believe they operated very successfully before they put him off.  We have had only one case since then & he has pulled through.  I think the hot weather will kill it out.  I am working at a job in the orderly room which will take about 10 days.  I don't like the job as it means 