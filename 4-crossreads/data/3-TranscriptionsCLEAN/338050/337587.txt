  
 No. 6517 Pt. G.B. King c/o 2 M. Store] 12th TB 15 th Btn Codford Wilts. Eng 
 Miss. G. Bentley 54 Renny St Paddington Sydney 
 Mrs. Duncan Duncraig Charmouth Dorset 
 Mrs. Barratt Hine Charmouth Dorset 
 21st December '16 Left Hurdcott at 9p.m. last night & marched to Wilton where we entrained for Folkstone arriving at 6a.m. where we breakfasted. Crossed the channel to Boulogne at 11a.m. & went into rest camp for the rest of the day. 22nd Dec We left Boulogne by train at 11a.m. & travelled 15 miles to Etaples (pronounced Etaps) where we are now in the "base depot". 23rd. Dec. Did nothing all day. We are beside the Black Watch here, they are great fellows. These cats are practically the only ones of the British forces that 
