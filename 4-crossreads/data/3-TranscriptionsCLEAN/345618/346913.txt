  
 Whether you be war records [indecipherable] Or in the ice-cold ocean swimming, Or killing Turks, or kissing women In sultry climes We'll often fill the bumper brimming & drink Old Times. 
 Old chaps like us are never fit, But you can shew you've got some grit, And if by shell or bullet hit You chance to lose a limb You can come back & buck a bit About Jerusalem. 
 You will come back of course - but still - You probably have made your will - Unless your balance stands at nil - It seems a rum thing To ask but in a codicil Leave the Club something! 
