
War Diary1916  19th Feb.Packing of vehicles.  Overhauling of Harness and saddlery after route march.  Kit inspection for making up final requirements.
20th Feb.Church Parade and March Past.  Major Barlow proceeded to take charge of 2nd L.H. Regt.
[This is the end of the 3rd L.H. Brigade War Diary entries written by Gen. Antill. The following entries are by another hand - unsigned]
26th Feb.Brigade preparing to leave Heliopolis for Serapeum.  Advance party under Capt. Dangar left to make arrangements for arrival of Brigade.
27th Feb.First Squadron of 8th L.H. entrained followed by remainder of Regt.   L.H. Field Ambulc. moved out.
28th Feb.Hd. Qrs. 8th L.H. and 2 squadrons 9th L.H. entrained and despatched - The Brigade train moving all baggage to station - Regiments instructed to take10 per cent over plus in men.
29th Feb.Last of 9th L.H. and advance Squadron 10th moved out - Wire received asking for Major Farr's services in connection with the Mounted Division.  Regiments settling in at Serapeum.
1st MarchSignal Troop - Brigade train and Bde. Hd. Qrs. arrived at Serapeum completing the move of the Bde. - no casualties amongst men or horses.
2nd MarchCamp situated between railway and Suez Canal, within 1/4 a mile of the latter;  Hd. Qrs. on the canal bank.  Regts. doing little more than exercise.
3rd March8th L.H. doing troop movements at the walk, sand making heavy going:Major Scott's name (9th L.H.) sent forward for promotion to Lt. Col.A.I.F. Orders d/29th  February received with names of those mentioned in Gen. Sir Ian Hamilton despatch of Dec. 11th 1915 - Published in B.O. d/3/3/16Major Charley Supy List transferred to 3rd L.H. Bde. to command details at Heliopolis.
4th MarchG.O.C. visited N.Z.M.R. lines - Major Powles, B. Maj. pointed out training areas.
