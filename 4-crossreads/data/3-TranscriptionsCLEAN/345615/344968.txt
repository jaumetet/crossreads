
about 10 o'clock heard a great yelling of - natives by the sound & from the direction it was coming - thought possibly it was a party of the enemy surrendering & making the noise to prevent our outposts firing didn't get out of my blankets to see - But was told It was a most thrilling noise - but was told this morning the Indians had buried their killed & they were wailing for their dead
Just heard to day a rumour that Anzac has been evacuated expected it - but it will come as a great shock to Australia & will sorely try the temper of their patriotism - There is hardly a home that has not
