  
 Royal Herbert Hospital Woolwich  19 June 1917 
 Dear Mr Ferguson 
 Your two letters duly arrived & I am glad to hear you are all well at your end. Am sorry to say my work in France came to rather a premature end & I am now at above military hospital. My work at the Kitchener War Hospital I suppose used up my reserves, I got a chill at Havre where the weather was very bleak, & what with marching & roughing it in side camps on the way up, & so on I was in no wise able to hold out against a 