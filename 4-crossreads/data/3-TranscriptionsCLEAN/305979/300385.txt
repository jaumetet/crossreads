  
 1916 Oct. 5 
 Offered self at Victoria Barracks for third time. Accepted as fit except for teeth and referred to Major Donald Smith re same and passes as 'fit' by him. 
 Oct 6    Vaccinated 
 Oct 15  left by 7.25 P.M. Melbourne express and proceeded to Hobart. 
 Oct 20   Acted as 'best man' at Bob Williamson's marriage. 
 Oct 23   arrived home after a rough trip on the 'Morraki' 
 Oct 24   Examined in English 111 A 
 Oct 25   Examined in History 11 
 Oct 27   Examined in English 111 B 