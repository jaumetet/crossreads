  
 had a box with the gloves. A boxing tournament is beginning this afternoon, and nearly all the lads will be there, but I am not over fond of such things yet, and prefer to stay down in our quarters & write etc. No doubt there will be plenty of time and opportunity for all of us to exercise our pugilistic instincts when we come in touch with the Germans. 
 Our ship is a twin screw and her engines are arranged somewhat differently from those of other ships which I have see, so I add a plan of the cylinders. 
 [Small diagram of cylinders showing valves etc.] 
 