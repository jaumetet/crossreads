
97 December 12thI am glad OByrne passed; I am extremely sorry that old Leckler is not with us & as soon as I am exchanged, I will certainly do all in my power to have him repatriated as soon as possible. It is a crying shame, when one knows that far worse cases than our own are being overlooked, & still our govt will not interest themselves unnecessarily as regards Prisoners of war -
December 13thThere is a Cinema show here twice a week. Officers also go out for a walk twice a week. There are about 600 officers here French & English, & the Camp is run very systematically. The German staff here is equally as good as at Freiburg - very polite, perhaps it is because this is purely a concentration camp for people eligible for repatriation -
Very cold indeed our mess is going along O.K. I play plenty of Bridge nowadays, especially with Tremlott & Stones & Gracie
December 14th130 French officers, who passed the Swiss Comm. June last, left early this morning -
Had long yarn with Matthews & Bernard, Australians of 5 Div captured last August,. They came from Strohen & have related to me some most horrifying incidents - I was glad to learn thro them that Gardiner & all 4 Bgde officers there are O.K. & well. Several attempts to escape have been made & quite a lot succeeded. Mott was successful in his first attempt -
Played Bridge -
December 15thThe cooking arrangements at this camp cannot compare with those at Freiburg, otherwise the camp is quite a good one -
Had a glorious bath today, the bathing arrangements are simply perfect, that is where Freiburg goal failed.
Received my first parcel today it was forwarded on from Freiburg -
French concert tonight The orchestra here is really splendid, simply marvellous for a Kriegsgefangener orchestra -
December 16th (Sunday)The Senior officer here is a naval Commander - He officiates at the English Service which is held every Sunday - we have a very nice chapel here.
The arrangements for Drawing parcels is more complete than at Freiburg, whilst the tin drawing system could not be better -