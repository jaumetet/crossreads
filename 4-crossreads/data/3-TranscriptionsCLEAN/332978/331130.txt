
[This page is partially obscured by what looks like the imprint of another page]
September 1st 51concludes that the letter was not sent.
Impromptu concert held tonight when some good choruses were rendered. Played Bridge -
September 2nd (Sunday)Church service held today, the first opportunity I have had of attending service since March.
Foot bandaged today - Played Bridge.
Lieut Hume who escaped a few days ago was captured about 40 Kilos from the Border He travelled by day & [indecipherable] walked through 2 towns in uniform He was caught by a Postman.
September 3rdBath today. Foot bandaged - Dr informed me that my photograph had arrived from Karlsruhe, & it showed several small splinters still in my foot. He said that if an operation was performed he would have to make sausage meat of my foot so that I would have to wait until the pieces forced their own way out [indecipherable] as soon as they did my foot would gradually become more elastic as he is of the opinion that the splinters are hanging round the bone thus preventing its movement at the joint - obtained permission from adjt to purchase a pair of shoes, as I am practically bare footed -
It was suggested that "bath-chairs" be provided for