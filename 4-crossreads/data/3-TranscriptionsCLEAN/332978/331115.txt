  
 August 1st 36 Raining again. Sale of wine stopped for 3 days on account of last nights Hubbub. 
 Tried my hand again at Sardine Rissoles & treacle pudding both being a wonderful success. Played cards again. 
 My foot is very sore, another piece of bone is trying to force its way out. 
 A new Draft of officers arrived today including a Lt Col. 
 My tinned stuff from Karlsruhe arrived today I have over 50 tins of food now. Paid today for month of August I received 46.30 marks. 
 August 2nd Raining again. Had hot shower 
 To Dr with my foot, it is very painful. Had very little sleep with it last night. 
 It was announced on roll call this mng that all officers who are sick & wounded are to parade to Dr tomorrow to be examined, with a view to being drafted to Switzerland to recuperate. Eng & German Govts have both agreed on this point. (Wrote Bert a PC no 5) 
 August 3rd Raining again, very miserable day. All Communiques are read out every day at 4.30 pm by an officer who has been a prisoner 