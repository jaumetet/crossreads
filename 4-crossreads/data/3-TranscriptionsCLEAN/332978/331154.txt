  
 75 October 15th & point out our grievances - 
 Meantime Lewis is going to write a letter & point out our grievances to the Commdt. Lewis has accordingly asked for "Peace" tonight. 
 Ralph was arrested for being rude to the Commdt & using abusive language to him - 
 Played Cards - Recd letters from army & navy 
 October 16ht Had bath this morning - 2 Recd parcel today from Aust Red Cross - Played Bridge - 
 Debate held tonight, subject being on Home Rule question. It was unanimously decided that Ireland should not have home rule - 
 A meeting of Senior Officers from all rooms held today when Commr Lewis read a letter which is being handed to the Commdt. It was a very well composed letter & very much to the point - In it He suggested that the present parol card system be abolished & that the Commdt introduce a new card, authorising us to walk independently anywhere within a radius of 5 mile between certain hours - This card to be signed by the Commdt. This is the system adhered to in England - 
 He also said that this camp was obviously a reprisal camp placed in the middle of a town & as such It was the Commdts duty to assist us in every way possible. His greatest concern should be to ensure that the camp is well guarded. Also that we should be allowed our sticks as it was recognised as part of the Btsh Dress - 
 October 17th Very miserable & cold today did not go for a walk. We have fires in the rooms now but one stove to a big 