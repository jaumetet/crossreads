  
 August, 1916 Saturday 19 The last stage of the journey completed Arrive at Havre in afternoon & after long march made camp again obtained some rations & TEA spent very cold night 
 Sunday 20 mooched about all morning After dinner were moved back to the docks & embarked on the "Queen" for Blighty. spent an awful night men were sick all round me but I escaped 
