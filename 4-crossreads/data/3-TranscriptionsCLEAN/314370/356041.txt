  
 June, 1916 Sunday 18 wake up in our new home, completely disgusted No rations yet. At 4 pm tents arrive & are erected 14 men per tent 
 Monday 19 Went to Tel-el-Kebir Rifle Range w/- a company of reinforcements. shooting was simply awful men have never had any proper instruction. splendid walk out & on the way back I sniped a large bird in a tree at 60 yds w/- my pistol 
