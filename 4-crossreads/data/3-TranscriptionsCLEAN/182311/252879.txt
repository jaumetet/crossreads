 a5763159.html 
 130 
 -2- 
    Why is it too, that there is no tobacco like our own, it cannot be usuage for we have been here long enough to acquire a new taste, but nearly everyone has by now arranged for a regular supply of Australian tobacco and cigarettes from home.  But the best thing sent from Australia is as everyone agrees, the Australian Mail, and the news of some being lost when the vessel was torpedoed has called forth many threats of vengeance. 
    Yes, we are all "Good Australians" now if never before, even the beauty of Midsummer England cannot surpass our memories of home, and as for France and Belgium, it is sometimes said by the boys - "Oh, we will give them to Fritz and apologize for them" but that is excusable for it is usually a  after a trying time in knee-deep mud or in watery trenches deeper still. 
    Yes! things are coming our way now, everywhere the Allies are advancing, biting off here and there large chunks of enemy territory, then consolidating those gains and advancing again.  Russia is still a puzzle, and she can help us very much if she will.  But we will win, we know that now, but when?  That is another matter. 
    Good Luck, Tom, and every good wish to your family, our good friends and prosperity to our A.N.A.,please give my greetings to the members. 
    Very Sincerely Yours. 
    HAROLD PETERS. 
  Charles H Peters  [signature]  
  Capt  
  then Lt  
  38 th  Bn A.I.F.  
