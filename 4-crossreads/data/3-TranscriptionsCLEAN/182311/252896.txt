a5763176.html
April 15th 1918
France.
My Dear Ones,
   It must be nearly a month since I wrote you anything like a long letter although several times I've sat down & scrawled to you the wish that all are well & the fact that I am well too.
   Yes it has been a very busy time for us all for we have been accomplishing quite a big task since the 23rd of March when in some haste we left our pleasant rest billets in a happy sheltered valley far from the war & started out to stop the advance of Brother Boche.
   It seems the fate of our Division that a period of rest is never completed but is always curtailed & the division moves off to some scene of warlike activity.
   To the lads the move came as no surprise for they were expecting a stunt, had been ever since a few days before  they had been served with a ration of Australian Bunny.
   By some strange coincidence the Rabbit Ration quite uncommon as a soldiers food, has always been issued before a "stunt", it came before Messines, before Passchendael, it came before some of our raids & just as soon as we saw it while in rest, well we all knew what to expect.
   And we were ready too for the weeks in rest had been spent in organization, in
