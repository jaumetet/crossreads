 a5763213.html 
 172. 
 G 
  COPY . 
   
  THIRD AUSTRALIAN DIVISION.   
   
 Divisional Headquarters, 
 10th April, 1917.  G.42/175. 
   
 Headquarters. 
    57th Division. 
   
   
    Reference your G.139/3 of 9th instant.  There is no objection to your retention of Lieut. C.H.Peters, M.C., 38th Battalion, for the purpose mentioned. 
   
    A copy of your letter has been forwarded to the G.O.C., 10th Aust. Inf. Bde. 
   
    (SIGNED)  J.H.F. PAIN MAJOR - G.S. 
    for G.O.C. 3rd.Aust.Division. 
   
 Lieut  C.H.Peters, M.C. 
 38th Battalion, A.I.F., 
   
 Forwarded for your information. 
   
 X  (SIGNED)  CECIL ALLANSON  LT.-COLONEL. 
   
 General Staff, 57th Division. 
   
 X   A.  C.S.I. man later I heard Knighted  K.C.S.I.  last news he was at the War Office in charge of Standardization of Arms. 
   
 10.4.17. 
