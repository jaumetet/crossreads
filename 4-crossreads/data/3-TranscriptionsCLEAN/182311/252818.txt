 a5763098.html 
 76. 
 -3- 
 the blue with puffs of white smoke all round indicating shrapnel exploding, but none were brought down. 
    These farmhouses are all built upon one plan, around a big central cess-pool where all refuse from house and cattle is thrown and where all the drainage is directed; it is very foul and should an Australian sun ever reach here the whole land would reek with typhoid.  From these billets we made a day's march along cobbled highways into a war-stricken town right in the rear of the trenches we were to occupy.  It is really pathetic to look upon a town, almost deserted by its inhabitants, with shell-blasted walls and broken windows, streets torn up by the passage of shells, others blocked with barbed wire and other obstacles and with soldiers billetting in its palaces, its schools and university and its factories and churches, horses too stabled in what was once a comfortable middle class home.  Really it forces one conclusion upon a man and that very strongly "That it is a moral right of every man to do his bit to smash the Hun." 
    This is the pitiful side of war, out in the trenches it is the Great Adventure. 
    With best love and every good greeting. 
    Harold. 
  Charles H Peters [signature]  
  Capt  
  38 th  Bn A.I.F.   
  then Lieut.  
 C.H.Peters. 
