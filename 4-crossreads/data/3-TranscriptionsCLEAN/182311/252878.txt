 a5763158.html 
 129 
 " A FRAGMENT FROM FRANCE ." 
   
 (All that remains of a letter written to the Secretary of the Albert Park Lodge of the Australian Natives Association, about 1917) 
   
 . . . . . . . . "parties though excitement is furnished when he picks up a party and shells it.  This afternoon the yarns in our tent turned towards Australia and the excellence of things Australian.  At one time the boys used to sing of "Dear Old Blighty" (England) now after many have returned after a wild "Blighty Leave" or perhaps after being wounded, from furlough they sing: 
 "Blighty is a failure, 
 "Take me to Ausralia." 
 And so with everything, for although the British Army Supplies and rations are quite good and sufficient, those articles coming from Australka are always much, much better.  When we lose our original Australian blankets they are replaced at times by others good enough but not nearly as comfy or as warm.  The news that there is a supply of "Ossy" boots sends everyone to the Q.M's Store, neat boots, comfortable and really waterproof are our Australian issue. 
    When I returned from my Investiture trip to London with a felt hat which I had brought from Australia and left in my trunk at Cox's, London, well, everyone made friendly attempts to exchange it or purloin it, for its the "Dinkum" article. 
    Clothing too, is just the same and when one ompares the rations it results in a win for, say, Australian jam, or indeed, everything else from home. 
