  SOME WAR RECORDS . 
   
 From June 1916 
 To - Aug. 1919 
   
 by 
   
  Captain Charles Harold Peters  
 M.C. and Bar. 
   
 of the 
   
 38th (Bendigo) Battalion 
   
  AUSTRALIAN IMPERIAL FORCES . 
   
   
 Melbourne 1930. 
