 a5763167.html 
 (3). 
 Just now I'm living underground in a big underground city, I call it a city for it at present contains nearly two thousand men, all in comfortable bunks along the sides of shafts & galleries driven into the side of a big Belgian hill.  Tis a wonderful place all the streets are named  some with familiar names Perth Road, Melbourne Road, Plumer Road after a famous General & I live in a cage in Crowther Street so named after the Staff Captain of our Brigade. 
    I share a cubicle with Capt Treb & we are divided from the passage & from  each  other officers by chicken wire & so these little cabins are called "The Cages" & would be humourists pass along the passage & poke nuts & biscuits into the cages. 
    Of course a lot of water seeps in but tis kept under by pumps & drains but the big luxury of all is electric lights & plenty of them too. No, I'm not joking 
