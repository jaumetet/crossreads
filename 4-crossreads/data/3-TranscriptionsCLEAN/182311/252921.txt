 a5763201.html 
 160. 
 -3- 
    Our first call was on the Colonel, who after giving us the New Year Greetings joined us like the fine old sport he is, then the "Doc" who seemed to be waiting for us.  Then here and there at many billets we called, some officers joining up, others refusing, these last we subjected to some trifling torture. 
    At midnight the bells from the church pealed, the Battalion band turned out and soon the muddy streets of this dull old village were alive.  Candles, flares, pocket torches provided light and everyone in a happy frame giving each other "Happy New Year" and supplementing it with the very popular phrase, "We will get Home this year." 
    Soon after we were in bed and on New Year's Day started the year well with the unaccustomed luxury of a "lie-in" until nine o'clock. 
    Next New Year's Day at Home - it is good to think on isn't it" 
    With best wishes, 
   
    Harold. 
  Charles H Peters    [signature]  
  Capt  
  38 th  Bn  
  A.I.F.  
 C.H.Peters. 
