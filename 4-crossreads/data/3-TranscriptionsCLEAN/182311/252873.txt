 a5763153.html 
 124. 
 -3- 
 wide and gay with innumerable striped umbrella tents, the Casino hotels are enormous, cafes numerous, and side-shows everywhere, the sands were crowded, but there were very few bathers, most people seemed to content themsellves wading and [shriping ?] in the shallow water.  The trip both ways was made in excellent weather by a small steam-boat across the estuary of the Seine. 
    Etretat is a pretty little resort, small and select set above a shingly beach.  I went there by motor with the same Major, Jack Harris (Lieut of 29th A.I.F.) and we had a glorious day, cliff climbing, pelting shingles at one another, investigating smugglers caves and lying in the sun on cliff-tops. 
    Octeville I reached by cycle through the picturesque undulating country: Lieut.Cheall (Royal Sussex Regt.) was my companion on the trip which was thoroughly enjoyed, and on the sands near the M.G. range lay a melancholy object, half a hospital ship which had struck a floating mine and which the French were slowly dismantling. 
    These few notable trips and many little journeys on foot through the woods and villages surrounding camp and many trips to Havre which is a fine town, prosperous, lively and interesting, all helped to make my trip to Base a very happy and memorable one.  Now here/I am back at the war and feeling very fit and ready for anything. 
    I am finishing this off before going up again to the trenches, this sector is fairly quiet but very muddy - Ugh! 
 C.H.Peters.)     HAROLD. 
  Charles H Peters  [signature]  
  Capt  
  38 th  Bn A.I.F.  
