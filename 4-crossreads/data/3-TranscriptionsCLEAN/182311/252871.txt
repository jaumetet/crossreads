 a5763151.html 
 122 
  FRANCE . 
 12-12-17. 
 My Dear People, 
    Here I am back at the big war again but able to look back on a very good time at Base; the six weeks I spent there were perhaps the best since I left Australia and certainly since I came to France. 
    Living conditions and mess at the C.T.S. were of the best and quite a reasonable amount ofliberty after school hours was allowed.  This gave one time to make little tours to Rouen, that famous cathedral town, to Trouville, one of the most famous of seaside resorts for Parisians, to Etretat, a small and highly select resort among picturesque cliffs and crags, and to Octeville again at the seaside where there was an area which we used, a French machine gun range. 
    All these little journeys I made a fter school hours or at the week-end, they took me to or through the most interesting county and my oftenest expressed wish was - "Oh, but I wish I had my camera!" 
    Major Blake, A.I.F., Capt.Claude Furze, London Rifles, and myself secured week-wnd leave, a formidable looking pass-port, hired a car and one Saturday at five o'clock set off for Rouen; we travelled first alongside a canal running at the foot of cliffs and it was there that we had our first check, a blow-out occasioning our stopping for twenty minutes, and being eaten at by an enthusiastic horde of mosquitoes. 
