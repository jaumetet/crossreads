  
 Military Camp. Tel-el-Kebr. 
 Arrived here two days ago, left Lemnos on the 3rd, going aboard the Manitou, old cable boat they crowded us on pretty thick too and the accommodation was a bit off. What should have been a sergeant's mess was taken up by prisoners of war, - odds and ends from Salonika going to be imprisoned somewhere. 
 Submarines are very active in the Aegean, we were escorted the whole way, travelled without lights, a most uncomfortable business. 
 Luckily for me we had fine weather the whole way to Alexandria stopped there on board one day 
