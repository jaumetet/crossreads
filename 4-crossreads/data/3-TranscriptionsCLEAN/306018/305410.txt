  
 March 1917 Answered a couple of letters 
 Sunday 18th Took some sick camels over to Railhead.  In the afternoon took the sick camels with a line of coons to get washed 
 Monday 19th Was detailed orderly to day, but was relieved of my job by Sergeant Berry & reported back to my unit.  Sgt Fitzgerald was detailed for duty at Moascar & left at 6 pm. 
 Tuesday 20th We were lined up in the afternoon & lectured by the doctor on diseases we are likely to encounter in Syria & means for their prevention.  Mounted Regt orderly Sgt at night 
 Wednesday 21st Being orderly Sgt could not go to the races to be held today  It was a big meeting & lasted all day.  Australian Horses did very well 
 Thursday 22nd We were expecting to move out today & got ready for it  However we didn't go. 
 Friday 23rd Still in the same camp  We are getting a bit impatient as orders are expected hourly to go.  Other troops are in contact with the Turks in front & we don't know what it means  Were lectured by the Squadron O.C. 