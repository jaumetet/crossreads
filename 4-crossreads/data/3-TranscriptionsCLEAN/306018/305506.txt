  
 March 1918 Saturday Usual work in the morning.  Went to the Races in the afternoon.  Won 30/-.  Was pretty tired after tea & went to bed early.  Got 3 or 4 casualties in.  The Regt have been in action again. 
 Sunday 31st Worked in the morning although it was my morning off as I want to go to Alex next week end.  Got more casualty lists in.  7 officers & 89 O/Ranks which is the biggest list I have yet had.  Several of the good old fellows have gone including a few of my particular friends in the Regiment.  Went out with 
 [The 6th LH Regiment was involved in the attack at Amman] 
 