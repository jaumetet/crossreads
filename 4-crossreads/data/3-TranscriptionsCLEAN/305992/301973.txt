  
 Single file as per orders [sketch of convoy formation] 
 27 [Apr] pm. Continued discharging guns, carriages & ammunation for (Palastine) 
 29. Coaling ship & loading guns, carriages & ammunition. Air Lieut. Worrell flew today over here from Port Said so called on Indarra & told us that he was in a hydroplane bombing the Goeben at the Dardenelles & had several direct hits. She was badly knocked about on the upper structure. On her side she had a large hole where the mine struck her whilst steaming back to the Dardenelles after the raid. Rough weather prevailed & prevented our airmachines from bombing for several days. So in the meantime she got off & was seen by Worrell whilst flying over Constantinople in dock from one of the British machines 
