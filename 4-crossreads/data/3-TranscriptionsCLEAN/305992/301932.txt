  
 Convey was formed outside at 2pm & then we proceeded. Zig zag during the day & straight course during the night. Covering twice the usual distance from Port to Port. The destroyers who were in command of British Commander & the sloop did splendid. 
 Feb 6 Arrived at Alexandria 2.30pm. Everyone on the elert when we passed the remains of the Aragon, Osmania & Attack. 
 7 Moored with anchors & shore. crew granted shore leave. The Jippo boatman was one of Lord Kitchener's soldiers formally & posessed 2 medals from the Soudaneese Campaign. A very interesting person who could speak splendid English for a Egyptian. He told his he had 3 wifes & 16 children according to the custom of the country. We told him he was a brave man for a Britisher could only manage one (wives in Egypt are little better off than slaves) 
 18 Boat drill all boats lowered & pulled around the harbor. 
 19 Shifted from our moorings to the wharf to embark English officers & troops. 
 20 Left wharf for anchorage with 2000 troops. 
 21 Orders to proceed to see 2 Japanese destroyers escorting us. Noon. orders cancelled. Orders to remain in port until furter notice. 2pm. Took aboard from a lighter 68 troops going to England on compassionate leave. 
