  
 the direction of the inner ships. No news so far re. the survivors. 7 am The Ormond joined us with 3 extra Japaneese destroyers. Fine weather prevailed. 
 May 28 One of the Jap destroyers returned & sent a message that all troops were saved on Leasowe Castle. Zig zag continued night & day. Now the escort composes of 8 Jap. destroyers. Since we found that when the roll was called 5 of the crew 92 troops were missing. 
 May 27 0.15 am Position of convoy when Leasowe Castle was torpedoed. [sketch of convoy formation] 
 May 29 Sighted at sub. conning tower above water at 11 pm. Sent word through phone to gunlayers & morsed the destroyers. It appeared on surface for 2 minutes & disappeared. It appeared as if we had ran over it & rammed same. Officially reported 
