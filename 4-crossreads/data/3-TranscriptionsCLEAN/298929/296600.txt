  
 The German Pilot got a bullet in his leg and was forced to land, the machine being captured intact.  This was carried out by Australian airmen. 
 30th Apr  The batt'n has been broken up and the band has been placed on the strength of 33rd, 34th & 35th batt'ns for administration purposes 8 to each.  I am allotted to 34th. 
 1st May  We have been transferred to Brigade Hqrs as Brigade band and to do other duties that may be required of us 