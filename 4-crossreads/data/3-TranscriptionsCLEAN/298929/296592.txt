
18th  In addition to band-work, the band has been detailed for duties which include, running, observing batmen, driving, bombing (work on supply dumps)
19th  Played at Amiens Ecole de Natation" or Swimming School where the 9th Brigade held a swimming carnival.                           Fritz is systematically strafing the town with shells and bombs.
20th  Went in charge of Reserve Bombing Dump just behind Villers Bret.