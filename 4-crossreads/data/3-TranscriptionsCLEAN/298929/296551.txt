  
 stronger affection for some than others.                                                 The French authorities presented the 3rd Pioneers with 2 medals of the Legion d'Honor.  They tossed up for them, and the cook's won.  A health to out gallant comrades. 
 28th  Our chaps are preparing for a big raid.  We have received our Xmas parcels 3 months late, but very acceptable. 
 3rd March  The 9th Brig raiders went over towards Warneton and did good work captur- 