  
 Leave Buxton    Matlock  Haddon   Manchester Sutton Veny    Warminster Bath  Drs.   Batt    Weak etc    Piano   Armistice   MPs. [indecipherable]  No 4 carrier  Mamari Xmas  Cape St Vincent Spanish mane     Armada   Band Gibralta  Algeria & Tripoli Port Said   Suez  Aden  Fremantle Perth  masks    Port Phillip  Whirlpool Melbourne    Nestor ---    Mamari Argylshire    Shindy Cricket Grds    Discharge 
 [End of sixth and last diary] 
 [Spelling corrections - place names] p12  Steenworck = Steenwerck p13/14  Eblington = Ebblinghem p33  Gentilles = Gentelles p64  Warfusie = Warfusee. Proyat = Proyart p69  Wallen Capelle = Wallon Cappel p70 Acroche = Accroche] 
 [Acronyms p13  CCS = Casualty Clearing Station] 
 [Transcribed by Peter Mayo for the State Library of New South Wales] 
 