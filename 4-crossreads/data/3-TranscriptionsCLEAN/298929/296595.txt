  
 railroads leading to the line, and reserve trenches have been dug for miles back. A chap who had outgrown his hair removed his "steel bun" when a wag advised him to put in on again or the water would boil. Fritz wants a lot of room for his fireworks, one would think round our little humpy was the only place he could give a display. 
 8th  Battalion went in again.  Fritz came over sending 