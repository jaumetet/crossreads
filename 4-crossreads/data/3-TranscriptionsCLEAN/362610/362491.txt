  
 Monday 2 October 1916 A very dull morning later turning to rain which continued throughout the day. The morning spent in going round the line, again being late home for lunch, & in the afternoon, after changing into dry things took things easy. 
