  
 Sunday 7 May 1916 A wet day with occasional bursts of sun. The Church Parades were all cancelled on a/c of the rain. Stayed inside writing as well as interruptions would allow, getting very little done in reality. 
