  
 Saturday 16 Sept. 1916 A gloriously bright morning but overcast later, then bright again in the afternoon & evening. At the Div. Gas School in the morning & then walked to the Div. Baths at Hoppoutre where I enjoyed a good wash. In the afterward busy with the fitting of new respirators in the 1st Bn., & in the evening writing home. 
