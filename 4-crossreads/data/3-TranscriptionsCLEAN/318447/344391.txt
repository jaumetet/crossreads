  
 mile of the modern Jerusalem composed of hotels shops & the more or less European quarter 
 Leaving the Jaffa Gate on our left and turning S. along the Hebron Road for about 3/4 mile we came to this spot to camp 
 Jerusalem is considerably larger than I expected though of course it is necessarily very scattered. It reminds one faintly of Mata with its precipitate streets & abrupt corners. Anyway it is a very 
