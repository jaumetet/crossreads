  
 in the valley. At about 1pm we reached Kureyat el Enab one of the highest points from which one gets a stupendous view of the plains of Philistea, Rabuleh, the white sandhills & the glittering Mediterranean in the far distance. 
 As 5 oclock struck from the clock in the tower above it we reached Jaffa Gate of the ancient Jerusalem having already traversed about a 
