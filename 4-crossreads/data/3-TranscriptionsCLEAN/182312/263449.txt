a6922046.html
89
(8).
larger craft.  Its their swift movements that appeal to one.
     Several new vessels of different classes were a little
apart from the fleet doing evolutions and thoroughly proving them-
selves before joining up with the units to which they were intended.
That long ship of a thing xxxx tearing through the water over there
is the "Naughty" the latest thing in destroyers, steaming - knots,
and that dense cloud of smoke further to the right is from the
"Awful" one of the "Push" ships which is one day going to prove a
surprise to Germany.  And so one could go on, always something new;
the Navy must be kept absolutely up to date.
     I had received an invitation to lunch with Admiral Sturdee
on his flagship and a very genial man I found him.  He had been in
Australia as Captain of one of the small vessels of the old Navy and
had received a blue ticket to Lord Beauchamps famous party and
admitted to turning his searchlights on to the couples at Lady
Macquaries chair.  Like most officers of the Navy whocame out in
those days he had the time of his life in Australia.  Melbourne
always needed defending about the first Tuesday in November.
Hobart at Christmas time, Sydney at Easter and Spring Meeting time
and so on.  He pointed out of the port hole to a vessel a couple
of miles away which he said I ought to know.  She was our old
friend the "Royal Arthur" now a sort of mother ship for some of the
small craft.  The Admiral reminded me of the part she played in the
inauguration of the Commonwealth when as Flagship on the Australian
station she made a triumphant entry into Sydney Harbour bearing our
first Govenor General who on landing midst much pomp and ceremony
proceeded to Centennial Park where the swearing in and inauguration
of the Commonwealth took place.  Altogether I spent a very
pleaasant couple of hours with the Admiral, the man whose photos
