 a6922014.html 
 25 
 -13- 
 as we had just come from Egypt where the weather was getting hot. 
 For three weeks we saw the sun but three times.  We are almost 
 within sight of the English coast (we certainly hear the British 
 anti-air-craft guns firing at the Zeppelins at nights) and to be so 
 near and yet be unable to get over there is rather tantalizing to 
 the boys.  The British soldiers here get parcels daily from their re 
 relations and friends in England.  Stuff cooked one afternoon reaches 
 them here at about midday following.  Some of the British Officers 
 at the front practically live on stuff sent over from England - 
 roast chickens etc. - I can tell you, if there is one man who knows 
 how to look after himself it is the British Officer.  All this re- 
 ceiving of parcels by the British troops started our chaps looking 
 for English relatives.  They raked their brains for long forgotten 
 Uncles and Aunts and to these they wrote as "your loving Nephew 
 Tom" or whatever their names were.  Some drew prizes, some drew 
 blanks whilst others found that their "Dear Aunt" had been dead ten 
 years!  My tent mate struck a prize in an Uncle down in Cornwall 
 who asked for a list of anything he might want.  He wasn't long in 
 getting it.  We asked him for a case of beer (because the water 
 here was very bad and we were afraid of an epidemic of typhoid and 
 as the constant rains had wet all the wood about we were unable to 
 light a fire to boil the water)( a few bottles of whisky, (all the 
 water lying about had caused the snakes to be very bad), a case of fruit (we feared an outbreak of scurvy through living on tinned 
 meat only, no vegetables being available).  We are now waiting for 
 him to send along the stuff.  A favourite stunt in writing to, say 
 an Aunt was to say "Mother used to be always talking about you, 
