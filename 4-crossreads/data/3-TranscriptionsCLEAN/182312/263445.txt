 a6922042.html 
 81 
 (4). 
 adventures, and considers that if the boot was on the other foot, 
 that is if we were in German's place, these little enterprises would 
 be nightly affairs and I have seen enough of the men who run our Naval 
 small craft to know that this would be no idle boast. 
      As regards the ships themselves, the big ships of the 
 Battle fleet do not at once impress a layman with their power.  They 
 are not very big and they do not bristle with guns but to thoroughly 
 appreciate their worth you must take a tape measure to the guns for 
 here is the overwhelming force of Britains sea power.  The ships 
 themselves do not look enormously big and their number is not very 
 great.  It is not as if you see the entire fleet steaming by as in 
 review;  you see it piecemeal, you see it at anchor and you see it 
 apparently idle and can not at once appreciate its worth.  But this 
 worth has nothing to do with numbers, or with huge bulk, or with 
 lightning speed.  Instead of counting the ships, measuring their 
 length and enquiring about their horse-power, look to their guns, for 
 it is here that the shattering power of the Grand  Fleet resides and 
 it is xxxxxxxxxxxx this that keeps the German Navy locked in their 
 bases. 
      What surprises one about the Fleet is the enormous 
 number of auxiliary vessels xxxxx required to keep fighting ships in 
 fighting trim.  Over there is a fleet of fast colliers and store 
 ships;  that long low vessel with the funnel well aft is an oil fuel 
 carrier for the oil burning destroyers etc., and that big tramp looking 
 steamer is a repair ship crammed full of delicate machinery and 
 capable dealing with all kinds of fleet repairs.  Those graceful 
 looking white liners with the green stripe are the fleet hospital 
 ships and that old "wooden wall" surrounded by a swarm of small craft 
 is the depot ship for the Fleet.  Here all mails for the ships are 
