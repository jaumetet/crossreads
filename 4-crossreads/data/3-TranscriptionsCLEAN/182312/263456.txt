 a6922053.html 
 103 
 (6). 
 What time does the train go?  Where do I change?  All this information 
 is supplied from a little office on the wharf and within an hour or two 
 the crowd has melted away, some to catch trains at once for their 
 destination, others to wait over until morning.  I am to wait over, 
 and am soon in bed at the Hotel Louvre - every French town of any size 
 has a "Hotel Louvre" so the Censor can't accuse me of disclosing our 
 landing place.  How typical of France are the sounds which drift up to 
 me from the street.  The rattling trams with their motor horns in lieu 
 of warning gongs, the carts bumping over the cobbled streets, 
 occasionally a horse with bells round its collar, the railway engines 
 with their peculiar way of blowing off steam, the shunters running about 
 blowing horns, the arguments in high pitched voices on the pavement 
 below, all are so inseparable from the French town.  In the morning I 
 take train for the Base at which we assembled on arriving from Egypt 
 back in March. 
  1st. December, 1916.        Let us turn for a moment to the situation in 
 this world war as it appears to-day.  For the present mud seems to be 
 the outstanding feature on all fronts and whilst this lasts nothing 
 much in the way of progress can be hoped for.  The re-organization 
 of the Government of Great Britain and the appointment of a War Council 
 of five has met with the approval of all classes.  It was the only 
 rational thing to do as the people had lost faith in the old Governments 
 ability to run such a war as this.  We can now at least hope for a 
 forceful businesslike management of the war. 
      I had hopes that the Somme offensive this 
 year was going to mark the turning point in the war:  that after being 
 battered for two years we were going to take the lead and start the 
 unceasing pressure on the enemy's lines that was to eventually break 
 downthe German military power.  That this was going to take a long 
