 a6922079.html 
 155 
 (10). 
 will bring up the old joke about an army moving on its stomach, we 
 at last wriggle into No.5 post.   "Good night, Sir", says the 
 Sergeant.   "A lovely night for a harbour picnic":  "and a lovely 
 night for a sniper" adds my Corporal with feeling.   "There is one 
 sniper who appears to be only about 50 yards out from here", says 
 the Sergeant.   "I've swept all the ground round about there with 
 the Lewis Gun but I fancy he must be in a shell hole and just 
 sticks his head up to fire".   "Well get your rifle grenades ready, 
 keep a careful watch out to locate his flash next time he fires 
 and then pour a couple of dozen grenades into the spot".  (On a 
 night when the moon was not so troublesome, such a sniper would 
 easily be dealt with by two or three men wriggling out and 
 settling Mr. Sniper with hand grenades.) 
      By now the enemy's artillery has opened up in 
 reply to the disturbance created down at xxxx Nos. 2 and 3 posts 
 and things are merry for awhile.   Soon our own artillery joins 
 in and the Boche quietens down again. 
      Our return journey to No.2 is just a repetition 
 of the outward trip, and here the relieving patrol should meet us. 
 We wait half an hour but as no sign of the patrol appears we set 
 off to look for them.  No.1 reports that the patrol had been 
 there and left again an hour ago.  Soon a messenger turns up and 
 tells us that the patrol corporal had been sniped through the 
 head between Nos.1 ad 2 and that the officer not having been over 
 the ground before had been compelled to return to Company Head 
 Quarters for a fresh guide.  He turns up about half an hour 
 later and we are both held up in No.2 whilst Fritz puts about 
 50  minewerfers (large trench mortar shells making a tremendous 
 noise in exploding and having great tearing effect) over.   One 
