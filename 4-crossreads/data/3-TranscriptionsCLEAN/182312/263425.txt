 a6922022.html 
 41 
 (5). 
 drill.  As soon as we come out of the trenches we start "right 
 turn by numbers", and "form fours" and all this sort of 
 elementary stuff.  It seems absurd but there is nothing like it 
 to smarten the men up after their cramped life in the trenches. 
 This is really a very smart Battalion and after they have been 
 out of the trenches about two days and have had a chance to 
 smarten up they drill better than any Australian soldiers I have 
 seen drilling in Australia.  This from men who have been at 
 trench warefare for about 12 months - Gallipoli and here - is very 
 good.  Our Church Parade (I couldn't get out of it) last Sunday 
 was a particularly fine turn out and was held under a big chestnut 
 tree.  Every officer and man was there and it was the first time 
 I had seen the Battalion all together.  I couldn't help thinking 
 that the Huns would have had a big bag had they put a couple of 
 shells into us.  Fortunately none of the planes were about at 
 the time or we might have got strafed - it does not take the 
 planes long to put their artillery on to a target.  Better than 
 planes for general artillery observation are these Kite bal ^  l oons 
 commonly known as "sausages" owing to their likeness to this 
 favourite article of German diet.  Both sides use them and each 
 side tries to destroy each others bal ^  l oons by aeroplanes dropping 
 incendiary bombs on them.  Only last week I saw our aeroplane 
 destroy three of Fritz's sausages.  It was about seven or 
 eight o'clock in the evening that he sent up four bal ^  l oons 
 evidently intending to do a bit of shelling in the twilight with 
 the aid of the observers in the baskets attached to the bal ^  l oons. 
 They had not been up a quarter of an hour before our planes were 
 over and destroyed three out of the four - they just managed to 
 haul the other one down in time.  We had a very good view of the 
