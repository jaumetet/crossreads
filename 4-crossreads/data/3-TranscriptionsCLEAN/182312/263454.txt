 a6922051.html 
 99 
 (4). 
 press and elsewhere, generally in more or less harrowing style. 
 Artists have painted it and poets have immortalized it.  And yet 
 there is nothing spectacular about it, just a little drama which is 
 being enacted every morning month in and month out, Sundays included. 
 Perhaps our women have grown braver or perhaps the more emotionally 
 inclined have been presuaded to say their good byes at home, the 
 early hour being a helpful excuse for insisting on the latter course. 
 Be that as it may, the seeker after pathos would find no gratification 
 in the scene as it takes place nowadays.  Officers returning from 
 leave are not a very cheery lot and what conversation they do carry on 
 consists mainly of grunts.  The man in the corner who took the tender 
 farewell of the pretty girl in the blue hat on the platform, sits 
 vacantly staring out of thewindow and inwardly cursing the war.  Next 
 to him is a chap who has caught this train four times during the past 
 two years.  He reads one newspaper after another and finally bundles 
 the whole lot up and throws them out of the window through which he 
 continues to gaze with one of Bainsfeather's "fed up" expressions.  In 
 the other corner  an unshaven subaltern  smokes cigarette after cigarette 
  [Note handwritten in left hand margin:   
  Poor old C (vide supra)]  
 in an effort to overcome the "nasty brown taste" which his last night 
 of leave has left him.  The next compartment contains a party of 
 nurses who are returning from their well earned leave after a 
 strenuous five months devoted to looking after the wounded from the 
 "1916 push".  They chatter as is their way, and appear quite happy. 
 The Tommies who fill up most of the train are like a lot of schoolboys 
 returning after the holidays.  They are not the same jovial spirits 
 who arrived at Victoria a week or so again.  Banter which then would 
 have been met by counter banter, now threatens to lead to blows. 
 However the feeling will work off as the day xxxx wears on. 
      The run to Folkestone is a very pretty one passing 
