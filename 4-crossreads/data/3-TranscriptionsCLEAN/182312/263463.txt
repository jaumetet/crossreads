 a6922060.html 
 117 
 (2). 
 Corbie to Abbeville some four months back.  The trees on either bank 
 of the Somme, then so dense and brilliantly green, were the only things 
 I could see through the open roof of the barge.  Now they are but an 
 avenue of stocks through which the river runs.  Although unlike the 
 poplar in many respects, I fancy they must be one of that family: for 
 the purpose of bordering a river I have never seen a more suitable tree 
 so tall and straight do they grow. 
      Amiens is the next town of any size andnits tall 
 Cathedral visible for miles around seems like an old friend.  I well 
 remember how on our treck from Amiens to Albert - we marched for three 
 days before we lost sight of that steeple and then almost immediately 
 we picked up the partially ruined spier at Albert.  This latter land- 
 mark surmounted by the partially tumbled figure peeping over the hill 
 near Becourt Wood was visible to us in the trenches (i.e. on the 
 Eastern side of Albert) if not at Pozieres, certainly as far as 
 Contalmaison.  Why it was not pulled down I can't understand, for it 
 must have been an excellent direction mark for the German gunners in 
 their search for our battery positions round about Albert.  Sentiment 
 (one of the Allies worst enemies) was probably the reason. 
      From Amiens another couple of hours brings us to Paris 
 where we arrive at seven o'clock in the evening.  The train for the 
 South leaves from the other side of Paris at 8.15 p.m. and the time 
 occupied in crossing Paris by car from the Gare de Nord to the Gare de 
 Lyon is about 20 minutes.  It is apparently the intention of the 
 British Army that Paris shall remain a sealed book to its officers. 
 From the time we arrived at the Gare de Nord until the train left for 
 the South a Major from the R.T.O.'s office never had us out of his 
 sight.  Whilst we had dinner, he sat on the bottom step of the 
 Restaurante and I'm sure he didn't breathe freely until the Riviera 
 train started.  One cannot see much of Paris in an hour and a quarter 
