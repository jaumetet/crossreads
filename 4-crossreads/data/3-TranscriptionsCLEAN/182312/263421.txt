23
                                     4th INSTALMENT.
5th July 1916.     The Billets I last wrote from now consist of one
chimney and  ^a heap of charred bricks.  It was a big homestead and
accom^modated the whole Company.  The Hun apparently thought it
concealed a gun for he poured shells in there in salvos until he
was sure there was nothing left standing.  Fortunately everybody
was able to get out in time.  Since then I have done another turn
in the front line where things have livened up considerably.
"The great push" which we have heard about for twenty months past
has at last started and this little strip of country from the
Belgian Coast down through that country and France to Switzerland
has become an inferno of bursting high explosive.  The constant
row of the guns firing and the shells exploding is now so loud
and continuous that it robs us of the warning shriek of an
approaching shell and the first thing you know now is the explosion
and that quantities of earth clods are falling on you.  If they
land much closer to you than that - well, you don't know anything
about it and that's the end of it.  That warning shriek of the
shell is always looked upon as a comfort;  it only gives you
about two seconds, but it is just time to duck, not that ducking
is any use for it is quite an understood thing in the army that if
a shell is meant for you (some say your number is engraved on the
nosecap) it will find you no matter where you be.  You will often
hear a chap say as a shell xxxxx sails harmlessly over head, "I
hope that was "my" shell".  Shell shock is a very real complaint
and quite a frequent one possibly growing more frequent as the
size and effect of the shells increase.  I had always imagined
that shell shock was a sort of climax to a gradual wearing of the
nerves through constnt exposure to shell fire.  Such is not the
