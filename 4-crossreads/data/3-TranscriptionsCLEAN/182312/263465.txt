 a6922062.html 
 121 
 (4). 
 waters edge on Cap Martin it commands a view both up and down the coast 
 from San Remo (the principal town of the Italian Riviera) on the left, 
 down past Monte Carlo and Monarco to Nice on the right.  The sea is a 
 deep blue, the towns consists of white houses with red roofs and rising 
 up half a mile back from the beach the snow capped Alps finish a picture 
 of wonderful beauty.  The highly colored views one sometimes sees of the 
 Riviera are by no means exaggerated.  The Landscape Gardener has worked 
 wonders all along this coast and has introduced plants, shrubs, and trees 
 from all parts of the world and frequently their native soil has to be 
 brought with them to get them started.  The tendency is towards 
 tropical rather than Northern foliage and results have been obtained 
 superior to the same plants or trees in their own native countries. 
 This specially applies to tree ferns and the more heavy varieties of 
 palms, but there are also Australian trees the splendid condition of 
 which would make Mr. Maiden green with envy.  Eucalyptus and other 
 species of the Gum tree are here in great quantities:  one road near Nice 
 through which I drove was bordered for two miles with Gums whose 
 tendency to sprawl out had been checked by liberal pruning each year 
 with the result that they have developed into fine shapely trees.  The 
 Currajong is another tree which does well here, and one sees both the 
 big fellow which makes our Western sheep farmers glad and the smaller 
 symmetrical chap used mostly for ornamental purposes.  It is rather 
 worthy of note that the Prince of Monarco has made use of this latter 
 tree in his wonderful Casino Garden at Monte Carlo.  This garden, 
 probably the most famous thing of its kind in the World consists of 
 an oblong strip running from the main street of Monte Carlo down a 
 slight decline to the main entrance of the Casino, probably about 
 half an acre in extent.  It is bordered by alternate Currajongs and 
 a species of tree palms the trunks of which match those of the 
 Currajong in so much as they are about the same thickness and taper 
