 a6922021.html 
 39 
 (4). 
 loss.  We are still having raids on the German trenches and 
 giving him no peace - keeping him wondering where we are coming 
 over next.  This keeps him on the jump and must seriously affect 
 his morale.  Some of these raids have their humorous side. During 
 our own Brigade raid a week or two back a party had been told off 
 to bring back to our trenches any Germans which the attacking party had been able to capture.  Crawling back across No Man's Land 
 under fire one of our officers noticed that one man and his 
 prisoner were not making progress, so he crawled back through the 
 grass to see what was the matter.  There he found one chap trying 
 to make his prisoner crawl whith his hands up in the air!  Naturally 
 the Hun could make practically no progress without the aid of his 
 hands to help him crawl but our brilliant escort couldn't see this 
 and was jabbing him from behind with a pair of wire clippers in his 
 efforts to make him move faster!  The only unwounded Hun brought 
 into our trenches that night was a subject of great interest. 
 Everybody poked an electric torch into his face to see what he 
 looked like, some drilled him at putting his hands up and down 
 quickly, whilst others invited him to sing us the Hymn of Hate! In 
 connection with that raid, the next night we put a notice board up 
 in No Man's Land, where the Hun could read it in the morning with 
 his glasses, giving him the condition of the prisoners we had taken, 
 some having been wounded.  The next night the Germans put out a notice which we were able to read in the morning, "Thanks for notice, 
 your man wounded has every hope of recovery".  This latter referred to 
 a man we had to leave behind in the German trench, wounded. 
      The billets we are in now are further back than 
 our previous position but still reachable by shells.  The 
 country is more open and we have a field or two in which to 
