 a6922038.html 
 73 
 (11). 
 was in no way due to the War (Oh no!) but was caused through the 
 failure of the wheat crop in some out-of-the-way province in Canada! 
      At Waterloo and in fact at all the terminal stations, 
 hundreds of women collect and pelt flowers at the wounded as they are 
 carried into the waiting ambulances.  That they cannot find some 
 more tangible way of helping the war along seems a pity as I feel 
 sure they only come out of idle curiosity, the same as would attract 
 them round a street accident. 
      The last stage of my journey was by ambulance to Wandsworth 
 - No.3 General Hospital - a distance of about 7 miles out, where I 
 was to cease my wanderings for a while. 
      I forgot to mention that whilst at Southampton I picked 
 up a "London Daily Chronicle" of 1st. August which had this one and 
 only piece of cable news from Australia: 
      "with tremendous whiskers and hair hanging down 
     his back another "wild white man" has been 
      captured east of Lindenow (Victoria) where he 
      had been a terror to the women and children of 
      the neighbourhood". 
 and I make no comment on it, except that it looks very much like the 
 Sydney Bulletin's style. 
