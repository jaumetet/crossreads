 a6922011.html 
 19 
 -10- 
 wayside stations, particularly the larger towns like Orange, Valence 
 etc.  where girls sprang up from nowhere with baskets of oranges, 
 cakes, sweets etc.  French soldiers are everywhere.  Down in the 
 South they are mainly on guard duty and are dressed just like the 
 French soldiers we see in  the old Comic Operas - bright red trouser 
 very loose and tucked into their boots, blue coats and a blue cap 
 with a red band.  The fighting French soldier is dressed in a sky 
 blue uniform which is certainly an improvement on the old Comic 
 Opera affair but is still very conspicuous.     The way the French 
 railway people handle the traffic is an eye-opener.  The Railways 
 are privately owned but at present of course, are under national 
 control.  All the Companies interchange their rolling stock so that 
 no delays occur in changing over from one Company's Lines to another 
 Our journey was 650 miles and with the train of 350 yards they did 
 the journey in 48 hours which for a troop train is very good travel- 
 ling.  They use the lines to the utmost; ahead of us was always a 
 train 2 miles away and the same astern, and yet with all this traffic 
 we were never blocked for longer than a quarter of an hour.  Their 
 expresses fly, everything gets out of their way.  We passed through 
 Lyon, Dijon, Tonnerre, and other big places south of Paris, but 
 Paris itself was not touched at, the train making a detour which took 
 us round the outskirts of the city.  North of Paris we began to see 
 places which came under our notice during the Germans dash on Paris 
 back in August 1914, St. Denis, Beauwaix, also Amiens, Abbeville, etc 
 We crossed the Seine, the Marne, the Oise, the Somme rivers whose 
 names we laboriously learnt in our school days, little dreaming that 
