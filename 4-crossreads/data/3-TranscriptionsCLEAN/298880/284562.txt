
14th Dec.
The Col. inspected all huts at 11 AM this morning. He gave us a cheery little lecture enquired how we all felt & passed on.
15th Dec.
We came back here to Switch trench again yesterday. The coming was not quite so bad as before as the engineers had laid duckboards through the mud to walk on, It snowed a little the day before yesterday at Adelaide camp Montaubau where we were camped in huts. The sun tries very hard to burst through the thick clouds and mist. He looks so cheery & bright when he succeeds. I received some Xmas cards a few days ago. They are so disappointing too as they contain absolutely no news. A good long