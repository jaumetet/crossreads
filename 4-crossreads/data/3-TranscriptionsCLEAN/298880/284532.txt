  
 Sept 2nd. 
 We did station work today (or were supposed to) to finish up our course. I went about 10 miles out near Fort Rouge with another chap. We didn't do a tap all day as there was no sun and we were too far away to be seen with the flags. It was the first time I have been on a station and did not succeed in getting communication. 
 3rd Sept. 
 We left School of Instruction today reached Cassell Station about 8 AM got aboard and reached Hazelbrouck about 9 AM. Billeted all day got on train again 6.30 PM and reached Steinwerck about 7.30 PM from here we went to Bac St Maur by omnibus. I joined the Bn about 8.30 PM at Fleur Baix where they are billeted. 