
and Gertrude Elliot and other distinguished artistes.
Tues. 21st Nov.
Mrs Duvas & Miss Cummings the people with whom I have been staying at Hendon just outside London saw me off at to the train. Their kindness to me I shall never forget. It was like home. It's rotten leaving to go back to the mud & hell again. However, I shall be alright when I get back with the boys again. I saw some splendid plays during my leave. The Bing Boys and High Jinks being lovely. I saw George Robert and several other accomplished and clever artists on