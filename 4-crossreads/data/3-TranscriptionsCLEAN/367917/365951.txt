  
 satisfaction to get onto German soil, after all the fighting we have done,  think most of us are much keener to get back to Australia as soon as possible. 
 Walter McCallum was round the other day saying goodbye, before he returns so I suppose we will all be on our way home shortly. 
 Wishing you & all your family the Compliments of the Season. 
 Yrs. very sincerely H. Barlow From Capt H Barlow. 20th Bn. 