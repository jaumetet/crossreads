  
 [Transcriber's note: Diary begins at Mellicourt, NW of Paris - later written from England/Wiltshire, at an Officers' Instruction school, Candahar Barracks, Tidmouth] 
 Should any find this book will they please forward to the following address Mrs G.A. Barwick "Mayfield" Campania Tasmania 
 914. Sergt. A. A. Barwick C. Company 1st Battalion A.I.F. 27/3/17 