  
 Monday 27 September 1915  Col. Featherstone. P.M.O. for Australia visited the Hospital and made an inspection.  Dined with Mrs Newmarch at the Occidental Hotel.  Spoke with General Ford and some others.  
 Tuesday 28 September 1915  In town this morning called at Chem Laboratory, Med. School, and other places.  Writing, reading & sleeping for the remainder of the day.  
 Wednesday 29 September 1915  About hospital all day.  