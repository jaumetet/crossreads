  
 Thursday 15 July 1915  Busy all day about the hospital   550 patients in residence this afternoon 
 Friday 16 July 1915  About hospital all day  2 small operations   All going well  Cable from the Girls: "All well" 
 Saturday 17 July 1915  In Cairo for an hour during the morning.  About hospital l  all day  Ken  Ke  Garrick & some friends from Melbourne called during evening. 
 Sunday 18 July 1915  Mass Basilica at 9-30 a.m.  Visited church of the Holy Familyat Matarha [Matarieh] at 5 pm  Letters to Girls, Mollie, Mrs Reynolds, Strickland, Armit, John Dalley, Doffie & Maggie, Manon, Mrs Rouse, Mrs Garrick,  