  
 very varied from blue garters for luck, to hand towels etc. 
 Uncle Walter is sug-gesting that Aunt Isett & I go away for a change & rest & we may. It is the 25th Anniversary of my wedding day this 26th of May, so I think I should have a little time off after that. Hazel is so well after her operation that we think she should take things on her shoulders a bit more than hitherto. 
 Carl T. has just come in & says there last word from Eric was that they had orders to go north - presumably to the Dardanelles. 
 It is such a lovely day here & we seem so quiet; how big the house will seem with you & Dene away!! 
 We have just come across a photo of the large hospital at Heliopolis & wonder if you were in it. Please tell us if you get your letters & parcels I am sending sox this mail & also say if there is anything we can send for your comfort. 
 Good by dear hoping you are quite yourself. 
 Your loving Mother. 
