  
 writing. I am so sorry for all the trouble about the pay it seems queer there is no way of fixing things up. 
 Your father is off skating & I am chancing this hoping an express leaves on Sunday. 
 All send their love & Billie has had a lovely red soldiers cap & a khaki waistcoat given him which he thought he was going to wear down to meet you tomorrow. I hope it will last till you come for he looks so nice in it. He is so well & so naughty these days. 
 It is too late to register this but I am chancing a note in it & will wire you another on Tuesday. 
 With love hoping tucker has improved 
 Your loving Mother. 
