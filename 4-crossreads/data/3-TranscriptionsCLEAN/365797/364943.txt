
afraid that he is leaving the Service. He has been far from well, some pressure at the base of the brain, purely old age I suppose & he exercises too heavily skates hard as usual.
Dorothy is going into learn typewriting, but some how I haven't much hope of her doing great things. Alice Clarke told Father she is making more money than she knows what to do with, teaching some new short hand on the typewriter. She certainly looks very flourishing, dresses so stylishly & has such a bright professional manner.
Have you seen Eric yet? or Laurence, or the Cuthie's. They have been gazetted as 1st Lieut! this week.
By this time you no doubt have heard from Oliver from
