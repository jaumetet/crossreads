  
 Saturday & he said Bob was expecting to go back to the firing line any day. He had had a lovely time - had 60 to have it on when the Bank cabled how much was he to have so Capt. said up to 100 but no more! 
 Evie Fry has just had her 21st birthday & had a jolly day I believe, getting many nice presents. Reg sent 5 & her Uncle George 5; Aunt K. & S. gave some of Grandma's jewellery. We haven't given anything so far I have not been to town. 
 Hazel & Father are well again; poor Hazel it isn't very gay being here alone, but we try to keep her busy. 
 You will be glad to hear that dear old Dorothy Burnett 
