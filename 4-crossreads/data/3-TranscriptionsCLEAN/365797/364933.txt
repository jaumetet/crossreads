  
 I am an engaged lady I have to be more sedate. Dene is so particular you know, & I have to do my best to live up to his ideals, 'tho' the poor chap sometimes has about ninety fits at the things I do. Never mind doubtless I shall be trained & turn out quite a respectable young woman. 
 I do admire that moustache most awfully of yours, but am not quite sure whether I prefer you without one or not. You know on principle I do not approve of them, but of course with a soldier these things are different. I shall have to wait & see you before I decide I suppose. 
 Last night we visited Mr. & Mrs. Thompson & she showed me your last post-card & also a very charming photograph of you when you were quite a little boy - of course ages ago. 
 Billy is growing into such a big boy & remembers everything. He still does a great deal of visiting in Lindfield 
