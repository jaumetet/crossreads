
imagine them just as I saw them last, the brats, growing up are they?  I grudge very much the cut this war's taking out of our lives:  however its got to be done, but I'm looking forward to the end of it.
I had a letter from Mrs. Billy Ellis from New Zealand.  She writes regularly to me:  they were all well at the time.
And now I shall stop for today.  I've given you absolutely all the news I've got and there isn't any more to tell about this side of the world:  we shall be busy for the next couple of days getting into action again, our rest is finished.
Give my love to old John & to Graham & to you.
Cheer ho & every good luck go with you all.
Yours sincerelyJock C. Ellis