  
 18 February 1917 
 St John's Wood Barracks, London, N.W. 
 Dear Mrs. John 
 I heap reproaches on myself that I haven't written to you for a whole month:  such, Madam, is the gratitude of man, for I used to like getting your letters, specially in action:  and now all that is past, pro temp, and I have a soft job here for two months before I go back to France. 
 I must own I feel rather a rotter sitting here safe and sound when I'm fat & well and sound:  but there you are - when I was offered this work, I jumped at it. 
 And now am fed up a bit with myself for taking it on: 