  
 14 July 1916 
 The Chestnut Troop, R.H.A. 
 Dear Mrs. Robertson, 
 Marvellous, wonderful!!  I've had two letters from you, and it was very good to get them also, simply full of news, they were and made me almost think I was back on the Murrumbidgee once more. 
 So I'm just dashing into a return letter to you, regardless of the fact I've got no news to tell, but I can start off by answering some questions you asked, and thus hope to get into a good letter writing vein. 
 (Apropos of above, in your letter dated Tog. May 27, you accuse one of not replying properly to what you write:  but some replies are better not made, as they are over three months old from the writing of your letters to the getting of mine.) 
 Question (1) Do I get your letters?  Yes, thank you. (2) Am I a pup?  Yes, you said so. (3) How am I?  (Do you mean, How am I a pup, or what health I enjoy.) (4) Have I had rheumatism?  Not too badly, thankyou. 
 The above four seem fairly straighforward and are the main things you have asked which don't come under the category of the  '3 months old no good'  lot. 
 Whatever did you go and take on little Tommy Gibbons again for?  I thought he had turned you down well enough before for the effect to 