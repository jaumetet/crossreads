  
 surprise - the French Secret Service had found out it was to begin March 17th & the British were to make a show up this way at the same time. 
 However the old Hun hurried matters on three weeks earlier, and when they attacked, the French Artillery guns took 24 hours to change their front so as to fire on the German infantry, during which time the Germans secured the 4 mile advance of which they made so much boast. 
 Of course the terrific number of men they lost was a bad blunder on their part, as the French outwitted them after the first two days, and a 4 mile advance was nothing to justify such appalling loss of life. 
 I hope now that internal affairs in Germany are getting strained:  there are constant stories of small famines in various things like bread or potatoes. 
 When the general Allied attack takes place this summer, the German lines must go somewhere, unless they turn & beat us, which is impossible. 
 Remember me to John & little John, best health & wishes to you all. 
 Yours sincerely Jock C. Ellis 