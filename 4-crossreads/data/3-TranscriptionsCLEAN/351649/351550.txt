  
 to you at Toganmain except that all's well (touch wood). 
 Also that things are not half as bad as might be, and we are not so very uncomfortable considering. 
 For instance we get wood to burn now for nothing, which is unusual in France:  it comes out of High Wood (if you've ever heard of it, but probably you have not done so). 
 The French country folk have got very mean, and only sell stuff to us at the present full values for everything so that the cost of living has gone up tremendously. 
 Now I got two very nice letters from you in one mail, hence I'm very uppish tonight and write by return. 
 Its becoming a 