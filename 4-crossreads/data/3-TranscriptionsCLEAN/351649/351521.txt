  
 25 Half Moon St. London 
 14 November 1915 
 My dear Katherine 
 Many happy more Xmas's and New Years to you all at Burrabogie.  I just heard you had some good rains during October and that all is well, which is good. 
 How is little Tom???  Answer me at once, have his legs got any fatter or not. 
 There is very little doing on this side of the world except war:  everyone is fighting, and indeed I feel quite out of it in plain clothes:  there is a keen demand for decent men for officers and I am much exercised as to whether I should not chuck up the rotten medical profession:  in order to brush up my work, I am doing one of the jobs at the Hospital:  what a filthy business it is and what drudgery & no chance of ever getting a smack at the German hogs. 
 There are hundreds of Colonials in London and one meets them everywhere, both women and men:  I shall get very stout if not careful eating such huge dinners and smoking fat cigars. 
 Address any letters you feel disposed to write me to C/o Dalgety, 45 Bishopsgate St., London, E.C. 