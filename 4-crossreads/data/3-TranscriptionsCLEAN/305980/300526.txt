  
 21:  Left Romani 2 a.m. reaching Katia ( 6 miles) at daybreak.  Our patrols in touch with the enemy, brining in 16 prisoners.  Returned at 9 p.m. 
 23:  Left Romani for Katia at 11 p.m. This stunting to & from Katia was carried out by the 1st & 2nd Bde on alternate days, so as to keep in touch & watch any enemy movements. Rumour has it that the enemy will attack very soon. 
 25:  Taube brought down & we were in action at noon, 5 of our boys wounded & 35 Turks captured. 
 29:  Two batteries accompanied us today & gave Abdul a lively time.  7 of our boys wounded & one killed in the action today.  Five of our planes bombed enemy troops causing heavy casualties.  Reached Romani again at midnight. 
 August 
 1:  Left Romani 3 a.m. for Katia.  Monitors bombarded Oghratina from Bay of Tineh.  Our planes & artillery also gave Abdul a lively time, 70 surrendering to our men.  Left at 10 p.m. reaching Romani at midnight. 
