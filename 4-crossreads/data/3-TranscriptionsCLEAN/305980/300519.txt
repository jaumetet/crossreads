  
 attending the scores of natives whom come to be treated by the "white medicine men".  The Omdah keeps us well supplied with poultry, eggs etc. & cannot do too much for us. 
 26:  Great commotion in village, the Omdah arrived to get our boys in to quell the disturbance caused by the people in the market place.  Our batteries were practising in Minia & thought that the Senussi had arrived.  It was a hard job to pacify the natives, who cleared to their homes from the town. 
 29:  One of our boys drowned in the "Yousef" today, being washed off the bridge whilst on patrol duty. 
 March 
 11:  Left Pazlit  7 a.m. reaching Rhoda (23 miles) 6 p.m. People flocked from the villages & gave us a fine welcome.  Over 1000 mounted men on the March was a fine sight. 
 12:  Left Rhoda 9 a.m. , arriving at Darout (19) 6p.m.   Fair town & had a lively few hours there.   Fine carriages & glorious avenues of palm & wattle trees through village. 
