
taubes flew over, dropping 5 bombs but did not hit the column.  Two men died on the way & we brought them to Arish for burial.  Reached "152" at 9 p.m. handing wounded over to Lowland amb. the C.O. refusing to admit the Huns, who had to go to the Egyptian native hospital.  Back to camp at midnight.
13:  Patrols brought in 14 Turks & two Hun N.C.O.s who were found at El Birge.  Three mines exploded with machine guns on the beach this morning.
19:  M.O. & 4 men drowned in the surf today, bodies washed ashore & decently buried.
24:  Rugby & officers of 156 Scottish Bde., ambulance won 13-3.  Fine fellows & we had an enjoyable game.  During the match taubes were over & owing to shell cases falling we hadto cease playing; one Scotty having a leg broken by a nose cap.
February
9:  Left Romani 10 a.m., reaching Kilo 143 at 6 p.m. camping for the night.  Moved on next day to Mazar, then on to Tilul.  Left here on the 12th reaching Abd at 3 p.m. taking over
