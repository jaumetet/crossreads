  
 6:  Moved off at 5 a.m., reaching to L.H. camp at Sarfsi - 5 miles - before breakfast, but could not obtain food here & all hands "broke".  We repeatedly asked to be sent away, but could not go without orders. 
 7:  Visited Greek Orthodox church, fine building; many of our boys & a few sisters buried in cemetery in the grounds of the church. 
 9:  Managed to be taken on the ration strength of 1st regt. details, so we had plenty to eat now.  Play with 1st against 2nd details (rugby) winning by 9 to nil. 
 12: Troops arriving in thousands; rumour of evacuating Peninsular.  Sports to-day, our team second in relay race & I fell when leading in 100 yards , smashing my elbow & kneecap. 
 13:  Woke up & found I had a dose of mumps; sent to No. 3 A.G.H., Lemnos.  Good treatment & best of food. 
 24:  Discharged from hospital & as all L.H. men had left for Egypt, was attached to 1st infantry camp.  Mr. Jones, M.P. for Wales lectured in  [indecipherable] on the war & we had a good sing-song to finish up. 
