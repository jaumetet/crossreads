  
 7 Herbert Crescent, London S.W. 
 1 July 1916 
 Dear Judge Ferguson, 
 Nothing I can say will be of any comfort to you, but I must write to tell you what we have lost in Arthur.  If we had only had more officers like him, things would have been much brighter and better than they are now. 
 The regiment has lost its best Company officer - the ideal one, the Company has lost a parent - a fairly stern one says a "B" Company corporal I met in the street yesterday, but that is high praise these days.  And we - Walter MacCallum, Wright and I - who lived close to Arthur and often messed together on Walker's Ridge, shall miss what the men so aptly call "a white man" dreadfully. 
 Arthur is gone with the consciousness of a life well spent and work well done.  Will you all, whom he has left, accept our sympathy for your loss. 
 Very truly yours, Evan S. Richards. 
 (Captain Evan Selwyn Richards, Signalling officer, 20th Battalion, invalided from Gallipoli, rejoined his regiment in Flanders in July, killed in action 5th September.) 