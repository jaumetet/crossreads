  
 1915 July 8 Thu.  A meeting  250 more wounded arrived to-day by several Hospital Trains (mostly Australians). - One of the wounded is S.S.M. King (see March 29th). 