  
 Monday 25 September 1916 Out at Rifle range all day near Mount Meridith [Meredith].  Shot fairly well. 
 Tuesday 26 September 1916 Getting ready to leave.  Got fountain pen from boys. Went out and took a photo of Bill Graham's grave which Haliday had found and put a cross on. 