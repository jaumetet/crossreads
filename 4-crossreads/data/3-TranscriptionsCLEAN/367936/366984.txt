  
 Friday 26 May 1916 Ordinary work.  Very cool day owing to head wind. 
 Saturday 27 May 1916 Still in Red Sea.  Saw wonderful sunset.  Sun started to set looking like a balloon then got all knocked about and a black object showed up in middle of it;  looked like a ship, but dissapeared when sun set.  Suppose it is the atmosphere. 