  
 Monday 5 June 1916 Went to Dr.  Have mumps.  Spent morning getting my kit on and waiting round.  Then went up to camp hospital in Ford Motor Ambulance.  Wonderful what heavy sand Ford can pull through. 
 Tuesday 6 June 1916 This hospital only temporary, in tents near Tel-el-Kebir main railway station.  Very hot day. Received 12 letters from  home  Australia.  No (1) from Mother. 