  
 Saturday 30 December 1916 Left camp at 8 a.m. and whole Brigade rode out East on a recognoitering expidition.  Rained all morning. Went out about 15 miles.  3000 turks & some cavalary reported at Rafa. 
 Sunday 31 December 1916 Stood to arms at 5 a.m.  Rode back all morning.  Worked hard at making a bivey to sleep in in afternoon. 