  
 France 27.4.18 
 Dear Fergie Thanks for your very interesting letter of 18 Feb written on the first day of term. 
 I can imagine you in that nice room of yours, which is pleasantly cool even in February, resisting a strong temptation to just look into the last novel and conscientiously sitting down to write what was to me a very interesting description of your vacation trip. 
 I have been over most of the N.S.W. side of it on a motor cycle and remember the Brown Mountain part of it very well as my machine had damaged her timing gear and as a consequence was running so hot with a retarded spark that she would barely climb at all. I had a beast of a time, and delayed Cliff 
