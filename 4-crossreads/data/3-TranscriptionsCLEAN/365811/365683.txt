  
 5 AIF asking for the appointment of a notary at Corps H.Q. It would have been a simple matter to have provided that the signature of a C.O. battalion or equivalent command should be sufficient to validate any document. 
 Heydon's letter was a good one, but I doubt if it will produce much effect, or even if it is very much to the point. 
