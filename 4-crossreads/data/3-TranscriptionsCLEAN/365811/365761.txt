  
 4 else would go near her save one Spanish Jewess who could scarcely keep herself let alone caring for others. 
 She was lying in a sort of stable place crawling with flies, with one 'boarding house blanket' to cover her & nearly starving. The people owning the building, for whom she had worked, intended throwing her out & as she is an  orphan it would have been onto a vacant piece of ground alongside. 
 All the concert money - taken from us of course - "was not for these cases" & no one could help her I waxed eloquent - I was quite pleased with my effort and managed to scare them into getting a carriage & moving her to Jaffa. But its disgusting to have to do with such people. I put them in the category Z- 
 Excuse my bad mood tonight - these people may be alright according to their lights but as David Harum says "the burn a mighty poor brand of oil" 
 Remember me kindly to Major & Mrs Curtis 
 Kindest regards to yourself & all at Wimbledon Yours very sincerely R. J. Hunter 
