  
 We had rather a rough time in the Ypres battles - I was with Colonel Stacy then whom you may remember was a crack tennis player at the Varsity. Colonel Ivor Mackay (who used to be adjutant of the old 26th in Sydney) is still going well, although he has had a very long spin. 
 Young Jack Mant is now signalling officer of the 1st battalion and still has that bright smile of his. He did splendid work up at Ypres. I had a letter from Beaver yesterday - he is now coming over to France to rejoin his battalion and seems anxious to get into the pay. 
 Keith will probably be interested to know how these younger people are. 
