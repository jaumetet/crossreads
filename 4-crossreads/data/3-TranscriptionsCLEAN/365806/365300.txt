  
 [Tanscriber's note:  This page follows on from page 12] ago.  Our Arthur, like yours, was one of the very best. He was very much a man - one of God's good men!  You have our deep sympathy in your loss!  Such experiences make us less selfish.  Thank you for that fine testimony to your boy, by a fellow Officer!  A week after the other cable came, a wire from Sydney 
