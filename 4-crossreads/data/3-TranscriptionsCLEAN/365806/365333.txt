  
 3 to a height of about 2000 fee in a machine.  The sensation was quite indescribable and simply left the impression that the R.F.C. is the only branch of the service in which anything is worth while. 
 Whenever I see an aerodrome in future I can picture myself hanging round the gates in the hope of being taken up for a fly. 
 Alroy Cohen went to Hospital.  He had a dreadful time down in the mud.  His legs are too short & he used to get totally 
 4 bogged. Pitt is probably going to fall into a very good thing if only he works conscientiously enough in the early stages.  He is at present attached to a Brigade staff for instructional purposes and there is a strong possibility that he will be chosen to attend a School of instruction in Staff work in England lasting for some months.  Pitt is a very capable officer but is so dreadfully lazy as to leave grave doubts as 
