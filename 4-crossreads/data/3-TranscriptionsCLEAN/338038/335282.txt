  
 c/o Sis 6.12.16 My dear Father, 
 Your letters of 7.9.16. 12/9/16 (Card) and 17.9.16 have come to hand since my letter of yesterday and you can imagine with what joy I opened them after almost [indecipherable but probably 6] weeks without hearing, and although they were behind the dates of letters received by Sis & Ted, they were none the less welcome. 
 The card of "A Bush Sawmill" is a real touch of home - I am sending you a card of a place we visited on Sunday last - Torquay, a famous watering place on the South Coast. There is no denying the beauty of the Devonshire scenery, and coming as an ordinary tourist from Australia one could not help being enthralled. I am afraid however I did not enthuse as much as Sis would like - I have been too long away 
