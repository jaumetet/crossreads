  
 [Printed letterhead] Commonwealth of Australia Department of Defence Melbourne  23 May 1918 
 Dear Mr Garling, I received your letter on my return from Queensland and deeply regret to hear of the death of your gallant son.  Will you please accept for yourself and convey to you family my deepest sympathy. 
 I am indeed glad to hear that your other son has put up such a splendid record in the Officer's School. I wish him every possible success and a safe return. [In margin] X Pat 
 Yours sincerely Kenneth Mac [indecipherable] Director-General Australian Army Reserve W.M. Garling Esq. 
 [Terence's brother, 2nd Lieut. Raymond Wise Garling, known as Pat, age 26.   His commission was terminated at his own request, 20 June 1918, citing the death of his brother and family reasons] 
 