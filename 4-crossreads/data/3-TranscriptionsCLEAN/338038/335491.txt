  
 [Continuation of previous letter] P.S. You may be surprised to hear from a complete stranger, but I would like to pay some small tribute to the memory of one, who was not only a commanding officer, but a very real friend to every man in his battery.  For your address, I am indebted to Mrs King of Woollahra. T. S. Sherwood P.O. Macleay River N.S.W. 5/5/18 
 Mr & Mrs Garling "Angledene" Burns Bay Rd. Longueville 
