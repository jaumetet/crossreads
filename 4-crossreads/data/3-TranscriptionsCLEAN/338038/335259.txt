  
 in a day or two to form & train a Howitzer Battery for his Brigade the 12th - It is only a remote chance of his striking Gerald or Jim Castleden, but in case he does I am sending him a note to keep an eye on them. They would be lucky to get into Richard's Battery as he is one of the best Artillerymen I know, and is sure to have a good Battery. 
 Leslie I have not seen for a week or so.  They  He came over one day with some letters from home to show me but I happened to be down at the O.P. - 
