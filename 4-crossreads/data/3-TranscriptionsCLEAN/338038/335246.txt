  
 Sunny France 3.8.1916 My dear Father & Mother, 
 I feel very guilty at leaving you so long without a letter but must ask you to forgive me. We soldier people get most horribly slack at doing anything in the way of writing when we are moving about so, as we have been ever since our arrival. 
 So far we have been in the firing line without a spell for about 6 weeks. It is the supreme test for a Battery to see how it works under fire, and my little crowd have so far come through well, so I am rather proud of them. Our casualties have been very light, only two wounded. Most of our work has been the usual give and take business "Holding the line"  altho' we were 
