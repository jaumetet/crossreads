  
 SS "Haverford" June 1916 My dear Father & Mother 
 We are at last out of the "Land of Egypt" and en route for Marseilles, thence we don't know where - but expect to go somewhere in the North of France. 
 We left Alexandria on the 2nd inst and have come a very roundabout course so far on account of submarines, which are supposed to have their bases    off   on the African Coast. After leaving Alex we went up to the North of Crete, then South West to Malta and are now just creeping along the Western Coast of Sardinia 
