  
 He came via Marseilles and our troops were then just moving out of Marseilles into Camp. 
 Eric Richards has come back looking fairly well, and of course cannot say enough for the way English people have treated him. He seems to have recovered and is now commanding a Battery in Colonel Rabetts Brigade. 
 All the news of Tom Swallow & Peter Forsyth from you, must have bearing on some previous letters of yours which have not come to hand. Although I notice Pete is going out to Mount Abundance so I take it he is going "on the land." 
 The news about the Canines is always interesting  I would give a good deal 
