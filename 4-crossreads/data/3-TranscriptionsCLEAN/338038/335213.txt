  
 Max Shelley has been down to see me several times and played at a concert we gave the other night in the Y.M.C.A. tent here. The latter institution is doing grand work wherever it is possible for them to push their branches. Lord Radstock, one of the London Council of the YMCA was here for our concert and gave a very fine address - 
 Athol Cochrane is down here now, & was along to see me the other night. 
 Ted Olding just back from England & looks very fit. Says Richards is still there, perfectly well, but not allowed to rejoin yet on account of being an enteric - 
 Arny Brown wrote me a few days ago from the Canal where his Battalion now is, and I was greatly pleased to hear he has received a Commission, now a 2nd Lieutenant - Have heard nothing from Les so far 
