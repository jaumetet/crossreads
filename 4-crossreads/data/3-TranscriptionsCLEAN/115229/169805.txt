  
 here I met Cec. 
 The weather by this time was becoming very frosty, and we had to wear our cardigans. On the night of the 24th we marched about 5 miles to entrain again in cattle trucks. We travelled back through Boulogne, Etaples and Abbeyville to Pont Remy where we detrained and rested for a day only drawing on meals allowance.  Next morning the 26th we walked in the pouring rain for 
