  
 Sunday 4 March 1917 
 Last night I watched the dancing at Shepherds & Continental Hotels. [The following sentence crossed through.]  Many of my former partners knew me not but I did not care to dance. 
 Today I went out to the HOSPITAL & saw all our wounded men & had dinner with General Meredith our late Colonel but now commanding 4 L.H. Brigade. 
 Cairo makes me tired, much dust & windy. Khamsin blowing - I must tell you what happened at SHEPHEARDS HOTEL - A Foreign Princess who has been there a long time tucked her table napkin into her  Bust  dress & when she had finished forgot it & sailed out of the room with the table napkin. Some of us sent the Head waiter after her. 
