  
 Monday 26 February 1917 
 ZEITOUN Cavalry School on Wesminster Dragoon Horses great collar  maketed  marks 1/2 draft animal, very suitable for Gunners or Poison Cart but awful brutes to ride. Only  3  2 paces are known WALK TROT anything over Trot is called galloping!!  However these are side lines & a man can learn a lot. 
 [The following sentence is crossed through] except if he is told to shorten his stirrup leathers, use both hands on them, put foot in now not ball of toe, etc:-  rather amusing after all these years. 
 IN Cairo for dinner & to buy uniforms etc. I am going to lead a simple life & save my pennies. 
 Tommy Officers are united up with us. 
