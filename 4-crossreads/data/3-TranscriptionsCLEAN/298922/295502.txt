
Tuesday 3 July
Started off on a 2 days stunt expect some fighting & on return I hope to go Cairo on Regimental business - Turksare all around us here & we get the usual evening plane over us daily & see some good fights. We move again tonight at midnight & get in touch at 6 A.M. 40 miles there & back to blaze the track for our railway to B--?
We got 8 Turksby ambush yesterday. Their lances make good bivy poles. When I say WE I mean the 4th L.H. not my particular Regiment.
