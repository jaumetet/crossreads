  
 Friday 2 February 1917 
 One gets very tired of the eternal sand, in face, ears tucker Bed etc. One continual dust storm for a week - We must eat lbs of it cooking in the open - but again MUD is worse. 
 Terrific sand storm. Think the worst we have had. 
 Enemy airships over again last night.  Got pen going again.  
 Allanson is despatch rider now. 
 Did I explain a new nice sort of sandal the Infantry wear in about 4 sizes bigger than boots & strapped on like snow shoes the idea is to make marching easier. 
