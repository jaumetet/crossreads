  
 Thursday 1 February 1917 
 Just took my Squadron to Rail-Head to be DELOUSED - !!  "Modus operandi" - Engine supplies steam to closed in van into which each man's clothes & Blankets are put he meanwhile going through a DIP - 
 [The following sentence crossed through] It would do some of those sheep owners who won't dip their LOUSY sheep to come here & be dipped themselves. 
 Khamsin still blowing. 
 Reported enemy with their machine guns from planes on Yeomanry at EL BURG last night - I found several copper bullets & shrapnel around our Bivies after last visit. Jim Ayre just read me a lot of HAY letters with much news of the River & Riverina. 
 Still Bandaged up like a sore finger - pampering my neck as BAINS FATHER would say 
