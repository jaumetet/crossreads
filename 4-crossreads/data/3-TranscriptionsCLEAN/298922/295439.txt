  
 Monday 23 April 1917 
 Move out to support again. This is a new line of Trenches. Casualities have been heavy. I hope Nowell Forth is alright. I believe he came out just before this fight began. 
 Bombed again today & more horses & men Killed. I must say I do not like bombs. 
 Our horses are SHIN SORE coming on to the hard ground after the Desert Sand. There are no trees in this part except  on ground  Orchards of Almond, Peach, Apricot, Vines Figs Pomegranates etc., not in season yet - tho' 1000s of acres of Barley & Wheat are nearly ready for the stripper. NO FENCES in this country. 
