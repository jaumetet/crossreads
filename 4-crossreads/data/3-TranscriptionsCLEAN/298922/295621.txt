  
 Sunday 18th November 1917 
 Went & saw Laura who is getting on well. Then I went & had lunch with the Cottons, the wife going to some friends Capt. Riddles. Laura will be up tomorrow, she gave me your letter of Sept. 9th & JOES, a long interesting letter of Home news. We are off tomorrow so post this. 
 With love from both to you dear little Mother. 
 We go by all sea route - P. & O. from Tilbury Docks Tomorrow. 
