  
 Wednesday 18 April 1917 
 Another hot day. Big guns firing all around & enemy planes bombing - got a headache but could always do 3 nights out of bed at the Picnic Races so can do it now. All is going well & the SKYLARKS singing many a brave man's Requiem. 
 In advance tonight moving out at 11 p.m. & my Squadron took up a position before dawn in a deep donga. Turksshelled us all day could not even light a Fire to boil the quart. Can see great activity in Turksroads. Fought on hill near 470. 
