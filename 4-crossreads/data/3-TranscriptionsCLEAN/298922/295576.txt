
Saturday 22nd September
Went to Fencing School this morning, all the young Italian & French Bloods go in for it here but never BOX.
I have been made an Hon. member of UNION & Mohammed Aly Clubs & meet a number of people - but this does not get over my impatience.
[The remainder of the page crossed through.]Posted notes yesterday but they are only of interest to the family I think. How different this life to that of Clare Station!!!! but I never forget. Went to Races & paid my expenses then on to San Stefano as a guest to dinner & the usual HOTEL dance.
