  
 Friday 23 March 1917 
 Letter from ISHERWOOD my brother in law who is at SALONIKA. Good news on all fronts, I anticipate a resolution in Germany before it is all over. 
 ENEMY aeroplanes over again this morning, but they did not drop any bombs. 
 Yesterday the Races were held at Rafa. 
 Anzac mounted Division won 5 out of 7 Races. I did not go - the Races were sort of Public & a Totalisator was worked. 
 Also heard our Division enters GAZA tomorrow. Just had a bathe & am now Field Officer for the next 24 hours. 
