  
 Tuesday 30th October 1917 
 Re Joe's letter & REVOLVER. I sent a Turkish one with Camel HAIR LEASH attached by Capt. Geo MILLS, a wounded flying man, the same family that had the photo sent you - Sydney DENTISTS out Parramatta way - hope he delivers soon. Lavender may have it. 
 I will give your letters to Laura. 
 We leave Blackpool midday today for LONDON & arrive at 8 p.m. or 9, the way the trains run - for breakfast I had Porridge, Kippered Herrings, Sausage & Egg & toast & Tea, so we do not starve. 
