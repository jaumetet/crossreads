  
 Wednesday 25 April 1917 
 ABUSSITTA 
 Snakes, Scorpions etc. more plentiful here & much more insect life of every description, reason I suppose bigger population than the Desert. 
 Managed to get some tinned Fruit sent by Pack Horse from Canteen at Railhead this morning. 
 [The following paragraph crossed through.]  Australian NEWS SHEET cabled to the Troops consists of "Egg Laying Competition", "Plague of 'Possums"!!  "Dinner to Holman"!! etc. !!!  What we would like to know is "Seasons" - Repatriation, and other items of interest. 
 Very Hot about 110. 
