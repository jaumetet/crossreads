  
 Sunday 11th November 1917 
 Lunch Laura at 1.30 at her Flat - she is always on the go somewhere & so never fattens. Dr. JAGGER sends her for an operation on Tuesday Tonsils to be cut, & she goes to a nursing home for a week, more expense for her. I read Day's letter of September & glad to hear alls well at home & you have been to Wangaratta. Can you tell me if that LOCOMOTIVE toy of Joe's is at Bronte. Laura & I were talking about it today. 
