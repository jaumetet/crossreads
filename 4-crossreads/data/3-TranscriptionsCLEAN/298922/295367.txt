  
 Saturday 10 February 1917 
 Copied Packed & said Goodbye to El Arish, going back 4 days march for a spell on the Flank. 
 [The following paragraph crossed through] Papers from England & magazines from the new wife. Weather warmer but still pampering my neck. And all the way from Bronte came my old African spurs & the lighter racing ones, thank you for posting both. 
 Also letter card from May saying how busy she is & forwarding her song. Please acknowledge if the Power of Attorney reaches Joe I posted 2 mails ago. A Parcel of Tucker from the wife also came along - We have a canteen here now & the Railway feed us. 
