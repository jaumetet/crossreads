  
 Sunday 2 December 1917 
 Our American friend showed us his FOB this morning & his wonderful watch. The fob was made of Human Skin. Church Service in the Saloon by the ubiquitis Y.M.C.A. As each day passes & nothing happens one is thankful - one of the Nurses on board wanted to know if we did get through all right would the Insurance Coy. give her back the money she paid on her luggage as premium. 
  I enclose my last WILL, please acknowledge when writing.  
