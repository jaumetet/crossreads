  
 Wednesday 12 September 1917 
 Alex Daily Telegraph Aug. 23 an English Paper has a good article by Mazzly on the MOSAIC I mentioned we found at Shedal. I shall cut out & post but mention as so many letters are lost these days. The Cotton Crop is ripe again in Egypt & the Mills busy, a regulation is in force limiting the amount grown as the "Fellahin" would grow all cotton & so starve the country of cereals.  Found letters from Dolly & Laura up to Aug. 21 - former was very sad at my not getting any leave, my cable had not then been received.  
