  
 Friday, 16 March 1917 
 Final Exam. I have done well. 
 Last night we had all the Instructors & Imperial Cavalry Officers to dinner, it was one of the rowdiest RAGS I've been in - quite up to a Picnic Race Club night. I am very bruised & sore today, but have not laughed so much for a long time. 
 I go to Continental Hotel tonight & stay Cairo till Monday then return to Desert. Our Brigade has gone to the front line again but not much fighting here now. 
