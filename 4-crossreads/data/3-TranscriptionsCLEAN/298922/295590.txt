  
 Saturday 6th October 1917 
 1,000 One thousand miles by rail through ITALY then another 600 through France (about). We expect to be 8 days on the train, so I will not be in England by the 10th. No tucker is available at any town we pass, but fruit & vegetables in plenty can be seen. We travel very slowly only burn Briquettes & little coal but we are going in the right direction. When we pull up at a Town or Station guards are put all around the train  as [indecipherable] riots have occurred at some places.  
