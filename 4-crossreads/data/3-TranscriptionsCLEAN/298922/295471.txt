  
 Friday 25 May 1917 
  We hear General S-ts is coming here - he will be popular.  
 Men & horses recovering from want of sleep. As long as a man or a horse is young, the power of recuperation is there. I am adding a wrinkle or two as each year of WAR goes bye. 
 KAISER's grandmother's birthday yesterday!!  We did not celebrate it in the usual way - but had a RUM issue for the men on a/c of extra fatigue. 
 Dust very bad in this Camp & bombed again today. 
  Letters from Mother 8/4/17 & 161/4/17 with Cuttings, DAY 8/4/17, JOE 8/4/17 & Mrs. Stronge.  
