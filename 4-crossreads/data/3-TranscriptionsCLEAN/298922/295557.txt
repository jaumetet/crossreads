  
 Tuesday 4 September 1917 
 No word re boat yet. I would of course like to go to Australia & see you all, but am considered very lucky in getting to England.  All today have been interviewing Red + Commissioner & Commandant re getting DOLLY to come back with me to Cairo .   Nowell did not turn up but came for breakfast he has a cold, but is better today. I go to see him tomorrow. All my friends want to know "How I worked it to get home". 
