  
 Mudros West Lemnos 2 Sept 1915 
 3. Aust Gen Hosp. 
 My beloved Parents In great hurry wish I could write more. Mrs Dick is leaving today. I am very well, very busy. Plenty dysentery here. Have a bed to sleep in. Do write me & tell me all news. I long for letters. Hope you and all well I am so anxious for news of you. Heard mother was ill. Much love from Your daughter Betha See Mrs Dicks. She will tell you all first hand news Col Dick is my Medical officer & is very 