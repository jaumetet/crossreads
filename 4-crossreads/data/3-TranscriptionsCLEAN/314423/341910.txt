  
 Addresses No 33173 Bdr. W. G. Wilson c/o London Bank of Australia 71. Old Broad st, London E.C.2. A.I.F. & War Chest Club Horseferry Road, London S.W.1. Mrs W. Riley Coolaness Irvenstown Mrs Andrew Muldoon Drumard Kesh. Mrs Kitty Martin Annesclan Tettigo Miss Kitty Gilmore Enniskillen Mr. John Gilmore Kesh. Fermanagh. Pte. Gavin Johnson. 3794. B. Company. 46 Battalion, A.I.F. 12 Brigade France 
 [Shorthand not transcribed] 
 [Transcribed by Eric Hetherington, Adrian Bicknell for the State Library of New South Wales] 
 