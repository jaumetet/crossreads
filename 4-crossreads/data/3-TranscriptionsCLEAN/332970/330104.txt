  
 allowed to approach, and a great  sale  business is effected. The result is that our biscuit & bully is supplemented by bread (at 1/- a loaf) figs, nuts, raisins, Turkish delight, etc. The figs in this part of the world are threaded like beads on a long strand of tough grass & are very good. They cost about 6d. a lb. 
 23rd Apr 1915. Force Order Special General Headquarters 21st April 1915 
 Soldiers of France & of the King! 
 Before us lies an adventure unprecedented in modern war. Together with our comrades of the Fleet we are about to force a landing upon an open beach in face of positions which have been vaunted as impregnable. T he landing will be made good, by the help of God and the Navy, the positions will be stormed, and the War brought one step nearer to a glorious close. 
 "Remember" said Lord Kitchener when bidding adieu to your Commander, "Remember, once you set foot upon the Gallipoli Peninsula you must fight the thing through to a finish." 
 The whole world will be watching 