  
 December 21, 22 "In The Key of Blue" - Elkin Mathews, 8/6. 
 Christmas night programme completed, with a tail piece by Ken of the Chateau towers and Archway. 
 Today the Count shews his good judgment of men:  he away to Brussels with Col. Marks. 
 Early morning, and Ken, Hardy and Gordon of the Sigs. (-- or of the "Many Leaves"!) about the Chateau drives, in shorts and Staff Caps, "What Athletes these strange "Australians" are!" 
 December 22 5. An ill report on the achievement of the Committee.  From the Canteen George unable to buy the two cases of Whisky.  He in tonight, and a re-yarning of arrangements:  Tomorrow to go out buying - he, Mac, and I - vegetables, tinn'd stuff and Fowls.  Tonight around to "Q" and find they have already arranged with the Count's buyer for a Keg of Beer - for themselves:  so we commandeered it, and ordered again two barrels. 
 Mail, a syllabus of the School of Architecture from the Liverpool University.  Will see Vin, a so-so Catalogue from Heffen of Cambridge, and a parcel of books - including Pater and Plato - from Mrs. H. 
 Up to the 'Mons' girl Estaminet with George, E. & B. and Mac: 
