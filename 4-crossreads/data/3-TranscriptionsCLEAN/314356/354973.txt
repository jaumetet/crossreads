  
 January 2 special of 10 days:  also a 72 hours leave to any chosen city may be granted by Divl. Commanders. 
 Tonight at A.D.M.S. met the renowned Marie:  she's a jolly Flemish animal, small but strongly built, full of laughter and impish unexpectedness, she learnt her English in bed! 
 19. (Entry Dec. 13 187.) Somewhere in the last diary is an important entry on G.H.Q.'s turning Miss E. Rout down, and threatening to close her hotel.  Now Aust. Corps have confirmed our appreciation of her work and are backing her, so a victory for us - over a war office that provides Brothels for the Indian garrison in Peace time, and ignores V.D. in abnormal times till the diseased would build a Corps, and then half-heartedly turns to an apparently genuine use of preventatives - and places licensed brothels "out of bounds". 
 Entry 41 Tonight read a copy of her letter to G.O.C. (?) of the N.Z.  She's right - but rather too fierce.  It would be rather interesting to know where she has her authority to so flatly contradict the U.S. Official V.D. returns! 
 But apart, to her a lot is owing, and that Corps have recognised that is well worth while. 
