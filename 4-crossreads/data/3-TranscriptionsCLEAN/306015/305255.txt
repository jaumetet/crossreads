  
 decent selections each evening and during this time I really enjoy life in the fullest degree.   Soft strains of harmony in a world of apparent discord! what a pleasing contrast! 
 July 17. A section of us  were  was picked for special bayonet instruction to-day.   A Scots Sergeant, of the Guards, is our instructor and for three days we are to be put through a course of physical training and bayonet work.   The course is strenuous but very interesting.   It will help us to be confident in any hand-to-hand encounter which may come our way. 
 July 18.   Too wet for parade. 
 July 19     Physical training & Bayonetting.   After lunch I walked to Achiet-le-grand to have a look at the magazines etc in the 
