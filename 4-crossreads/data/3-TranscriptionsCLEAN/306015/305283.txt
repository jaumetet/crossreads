  
 the return trip, but accidentally missed him.   After 10 this evening several Fritz planes bombarded St. Omer & for a while things were very lively;  search lights were playing & anti-aircraft guns booming quite incessantly, and machine guns rattled like Kettle-drums.   The planes returned twice and did a fair amount of damage in the town. 
 Sept.   3.   Wood-stunts again, the new method of attack being explained to us by the Brig-Genl.. 
 Fellowship meeting in evening. 
 Anti-aircraft guns very much alive all night. 
 Sept.  4.  Route march & 'march-past' the Brig. Genl.. 
 Sept.  5.  Route march all day.  In the evening we were looking forward to a little more bomb excitement but it did not come. 
 Sept.  6.   A Long march and a divisional 
