  
 From "Le Petit Chose", Daudet. (p. 255 Collins) 
 "The bells of Saint-Germains also visited me many times a day.   Their visits cheered me.    They entered noisily through the window & filled the room with music.    Sometimes joyous, giddy chimes, hastening in their semi-quavers;  sometimes gloomy knells whose notes fell one by one like tears.   Then I had the 'angelus', the midday 'angelus', an archangel with garments of light who arrived all shining - the evening 'angelus', a melancholy seraph, who rode on the moonbeams, and made the chamber humid as he shook his mighty wings." 
