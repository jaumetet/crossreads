  
 May 7.   Dental Parade in the morning, and a Fatigue parade to remove the A.M.C. tent in the afternoon.   The latter was capped by a fine cup of tea from the hospital.   (Tres bon!).   S.C.A. Hut in evening. 
 May 8.   "Bull-Ring" again! We were lectured concerning Patrol work and also had exercise at entrenching with the small entrenching-tool which we carry on our equipment.  The afternoon was occupied with lectures on fire control & discipline and also visual training. 
 A gentleman gave us a fine lantern-lecture during the evening on Egypt and surrounding countries. 
 May 9. I was detailed as a picquet along the road South of the camp this morning.  While on duty I saw a number of Hun prisoners working on the railway line.  They seemed very decent chaps & the boys seemed to think so too, for, after passing a few not very complimentary remarks about them they tried to speak to them & gave them 
