
The campaign had been a failure, yet a spendid one, but it was not the fault of the troops;  it was the fault of those who planned it.  A chap near me was fooling with a Mills bomb, (a new variety we had just received) when suddenly the pin flew out and he hurled it overboard where it exploded as it touched the water.  Thus we bade Gallipoli, Adieu.
164 Pte. Jno Booth5th Australian Machine Gun Coylate A Coy, 20th Battalion5th Infantry Brigade
also12444 Trooper Jno BoothLegion of FrontiersmenNew South Wales Unit
[Transcriber's notes:Kosciuscko - now spelt Kosciuszko - P. 17A.M.C. - Army Medical Corps - P.10Khukri - a curved knife used as a tool and as a weapon by the Ghurkas - P.17
[Transcribed by Judy Gimbert for the State Library of New South Wales]
