  
 Leave Victoria Stn by train for Folkestone - the "Anzac Coves" realising what a good time they have had do not grumble. Arrive Folkestone & board vessel for France - A pleasant trip across - Arrive Boulogne midday - Report Military Camp Back again to the drudgery of Mechanical Army Life May 16th It would be false to say I am anything else but miserable - However I am not grumbling or 
