  
 Friday Feb. 9th Final preparations - Bid Madame adieu, and in company with Jack Shaw, I entrain at Bailleul Station, loaded with personal & theatrical goods - arrive Calais 2.30 p.m. but  fail  to board "packet" at 5.15 p.m. for Dover. Pass night in Calais. 
 Saturday. Feb.10th. Embark at 8.30 a.m. - a rough trip across the Channel. Arrive at Dover & entrain for London - Arrive Victoria & taxi to War Chest - thence to Court Theatre & then back to War Chest - At night. 
