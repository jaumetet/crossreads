  
 leave for Edinburgh by 10.30 p.m. train - A dreary night. 
 Feb.26th Arrive Waverley Station Edinburgh - Old & familiar scenes. Put up at Old Waverley Hotel. - At mid-day I meet my dear little Scotch friend - Afternoon spent in writing - shopping  etc - Movies at night (Mary Pickford) Feb.27th Diary & letters - again meet Jack Carey. - A quiet but enjoyable evening spent at 59 Brunswick St - Home of my friend. 
