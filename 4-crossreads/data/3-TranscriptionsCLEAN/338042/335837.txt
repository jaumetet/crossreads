  
 Mar. 26th Further important successes by Germans - Bapaume. Noyan etc. captured - A Gloom over London - All theatres including our own do poor business - Copy of "Diary while in England" sent Home - Also Photos. Progammes etc. Mr. Burchell Mar 27th Letters of appreciation from Miss Harland & Mrs. Wheeley - Seven more Australian Letters come to hand - Houses increasing in size - Seymour Hicks 
