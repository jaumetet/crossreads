  
 Sunday 27 Jan. Work during morning. 
 Afternoon rode into Bailleul to play football for D.A. sigs against DA. Harry Richards & George Leverett also came in. Chaps very good to us. Had a good game but got beaten by about 17 points. 
 Had tea returned home & was advised about going forward tomorrow. Spend evening packing & arranging. 