  
 Tuesday 13 August Up at 2 a.m. but lines fixed up. 
 Up at 7 am with 10th Bde out but did not go on it. Had breakfast & packed up, tramped to H.M. Picked up finally by box car to Bussy. 
 Afternoon swim. Evening at ease. Mail. 