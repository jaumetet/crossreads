  
 Sunday 31 March To Central Hall. Meth. Church. Anniversary service but not much different from the ordinary. Met Frank Rogerson. Had great sing. 
 Afternoon visited Braid Hills again. Very misty. 
 Evening strolled about & finally turned up at Station in time for 10 p.m. train. Left Will who could not travel until tomorrow. Farewelled Edinburgh people in Sure Scotch style. 
 Frank Rogerson in same carriage. Good sleep. 