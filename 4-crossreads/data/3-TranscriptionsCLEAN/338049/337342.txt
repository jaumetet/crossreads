  
 Sunday 12 May Up & cleaned for presentation decoration parade. 8.30 fell in & finally in order on Aerodrome ground. 
 General Birdwood  arranged  arrived & church parade started. Medals presented & then ribbons. March past & rain hurried end. Afternoon at ease. 
 Evening Mod bad. Had to keep her on move as she could not keep still. Vets paid 2 visits. Getting worse. Finally shot about 10 p.m. after tiring evening. 