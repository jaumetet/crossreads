  
 Saturday 28 August, 1915 
 We are holding "Courtneys Post" near the extreme right of the Australian position. There are Turkish trenches on every side of us. It makes you a bit shaky for a while to hear bullets thudding into the sandbags a couple of feet from your head or shells shrieking over the hill into Popes Post 200 yards away. 
 The Indians out here are very cool customers. It is great see them plodding along with the mules taking no more notice of the bullets than if they were flies. 
 This place is a regular network of trenches, you could easily lose yourself in them. They are 8 feet deep in the front line & they need to be for they are being fired into day & night. 
 Had a swim at Hell Spit this afternoon. Expected to be shelled any minute but was determined to have a dip. 
