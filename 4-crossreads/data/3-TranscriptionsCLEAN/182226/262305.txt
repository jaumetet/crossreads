  
 Sunday 10 October, 1915 
 Disembarked at 11.30. 
 Took some more interesting snaps & saw a submarine in dry dock but couldn't get a photo of it. 
 Cigarettes & matches were given us as we came ashore also some milk. 
 We were put in a motor ambulance & then followed a drive of about 3/4 of an hour right across the island to St Georges Hospital. It was a lovely spin & all the roads are perfect so we were able to go at a good pace. 
 The hospital we are in was formely a barracks & it is very comfortable. 
 I am in Ward B2. 
 There is some great scenery here & I am anxious to get loose amongst it with my camera. 
