  
 Sunday 23 January, 1916 
 Mess orderly today. 
 All the G'burn boys had their photos taken with Major Fitzgerald. 
 Got orders to move. Struck tents at 4.30 p.m., lay in our overcoats till 12.45, moved off 1.20 a.m. went by train to Ismailia. Marched from there to Ferry Post about 4 miles. 
 Our Pack was 250 rounds of ammunition, 2 blankets & waterproof sheet & all spare clothes etc something like 120 lbs. 
