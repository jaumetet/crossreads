  
 Monday 13 September, 1915 
 Another man sent to Lemnos, Pte Simpson. 
 A very hard day, two trips to the beach, shelled both times, the second time the shells were going all round me but most of them went in the water & failed to explode. I had only been back from my second trip & hour & a half before going into the gun pit all night. 
 Met Paddy Matthews & Les Patyen down on the beach & had a yarn to them. 
 Water has been very short lately for some reason or other & what we do get is very salty. 
 It was very cold on watch on the gun during the night but I managed to get a fair amount of sleep so do not feel too bad this morning. 
 The artillery on both sides has been very lively. Pte Halfford, one of our men got a slight graze from a bullet on the head, the first man we have had touched. 
   
