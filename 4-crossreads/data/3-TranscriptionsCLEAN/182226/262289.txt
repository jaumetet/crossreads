  
 Friday 24 September, 1915 
 On duty again. 
 Carried up rations. 
 There was a demonstration by us at night together with the guns of the fleet & land batteries. The Turks made a very spirited reply. 
 The two machine guns were going we had a cross feed in No 2 gun which took a good deal of fixing. Flame absorbers had been made for both guns but collapsed after a few rounds. 
 The Turks exploded a mine, trying to destroy one of our zaps, which they failed to do. 
 The Turks use a very fine flare for night use. It gives out a pale yellow light lasting about 2 minutes it floats right down in front of our trenches making every part of them show up. They are only used in case of a demonstration by us. 
