  
 Wednesday 25 August, 1915 
 Was sent to the base for overcoats about 6 miles altogether. We were sniped at more than was comfortable. I carried the coats on the side nearest the enemy, "just in case". In the afternoon I had to dig a gun position & deepen a trench. 
 Our dinner consisted of boiled rice, biscuits, jam, bovril & bully beef, with tea. I was cook for Mac & I. 
 Slept out in the open all night, was glad of my overcoat. It is considered bad form to run here or to duck - for anything except shrapnel, anything is permissable for that. In fact when we had to go out in the open under shrapnel one chap dropped what he was carrying & beat it for his life. The next day, the general came along & frightened seven years growth out of him. Told him he could have him shot for it, but he said he'd do it again under similar circumstances. 
