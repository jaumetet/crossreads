  
 Saturday 9 October, 1915 
 Arrived at Malta at 5 a.m. The town is very picturesque & is built more on the Moorish style than anything else. 
 Went right across the bay for orders then came back to Valetta [Valletta] harbour. 
 It is all artificial work & is very picturesque though dirty. 
 There were 6 big French ships of war in the harbour, we gave them a cheer as we went past. 
 I got some fine photographs of the harbour. 
 The usual bumboats came round the ship selling chocolate. 
 All the little pulling boats in the harbour are like the Italian Gondola, only smaller. 
 We expect to disembark in the morning. 
 We started coaling but stopped again almost immediately. 
