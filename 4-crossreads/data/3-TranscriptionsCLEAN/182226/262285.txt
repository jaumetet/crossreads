  
 Monday 20 September, 1915 
 Crook again. Had to go down & bring about 40 lbs of meat up the hill. This finished me. 
 Two men killed & 6 wounded by one of our own bombs. 
 Things were very quiet, it has been a lovely warm day & a bright moonlight night. 
 One of our destroyers lay out in the moonlight, right close to the shore, trying to tempt Beachy Bill to have a shot. As soon as he had he would have been blown sky high by a monitor carrying a 14" gun & lying right out to sea waiting to locate it by the flash but Mr Turk is too shrewd to fire at night & give the position away. 
