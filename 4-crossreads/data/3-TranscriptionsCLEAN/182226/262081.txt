  
 Monday 28 February, 1916 
 We are to have the machine guns of the Brigade, are going to be formed into one company. Lewis guns will take the place of the maxims in the Battalions [indecipherable] &[?]  have taken on the job of scouts. We will be on Headquarters staff & big kits[?]. This change takes place from the 1 st  March. 
