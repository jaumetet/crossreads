  
 Friday 25 February, 1916 
 We put in the finishing touches on our pit but we still have about 20 yards of communication trench to do & 2 dugouts to put in. 
 We are getting used to the walking back & forward to the job also to the hard work & my back is beginning to show less signs of complaining than at first. 
 I managed to get a pair of sox & a balaclava cap from the comforts fund. 
