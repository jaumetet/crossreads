  
   &  did not find the time a bit too long in fact, all exclaimed how sorry they were to let us go, but it was past our bedtime. 
 When we get slack again three more sisters are going with other Colonel we have who is the Surgeon-in-Chief. Dorothy will be among the next lot. 
 About 2 days ago, three more sisters came to help, they are from the 3rd A. Gen. Hosp. lent to us until their own is ready for them. We were delighted to see them but things are not nearly so busy now. It means we shall be able to get a few hours rest.  I've had all sorts   &  conditions of officers to nurse. Colonel   &  General. 
 Sent way a Major General this afternoon   &  his A.D.C. both most charming men. Poor fellows wounded rather badly, while near the firing line with a wretched Hun shell - How I hate the Huns. 
 Received a letter from Ron the other day   &  poor chap is in Camp in England again. He received my letter with the 10/- in, so I was glad.  Poor boys don't get much 