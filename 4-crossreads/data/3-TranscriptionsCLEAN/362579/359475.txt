  
 No 2. A. Aux. Hosp. Southall. 19.1.18 
 My dearest Mother   &  Sisters, 
 Have received a bonza mail this morning, am feeling awfully excited, was told about noon, that I was to hold myself in readiness for transport to Aussie. It may mean a few days or a fortnight before we depart, Oh, to think I shall most likely see you all in a couple of months or less. 
 Eddy's letters are much improved, he will be an interesting boy now, not a little baby chap. 
 So sorry you cannot 