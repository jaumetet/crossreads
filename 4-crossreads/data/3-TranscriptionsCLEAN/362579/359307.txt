  
 We are all very delighted. Col. Has been Mentioned in Dispatches, so we are giving Sister O'Dwyer a Supper to-morrow night. Only inviting our own M. Officers. Hope it will be a jolly evening. 
 Colonel Powell (late 1st  Stat. Hosp. Ismailia) has gotten the C.M.G. so quite  a number of honors are flying around. 
 Had the Night-off on Thursday so had a very nice time. Dorothy, Kelly   &  self got up about 3.30 p.m.   &  had an afternoon picnic with the 