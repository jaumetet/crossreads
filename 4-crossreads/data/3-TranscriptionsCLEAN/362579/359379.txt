  
 pink bedsocks that Alice made. I did not wear them last winter   &  they were in an old suit-case with some other things   &  someone grabbed it out of my tent, so it turned up the next day. 
 I shall have to spend some money to buy new clothing if my trunk is lost. 
 Quite a number of the old Staff of 1st A. Gen Hosp. are here now. So it is quite a reunion. 
 Esther Hart   &  I share a room it is great to be with old pals again. she came out on Kyarra with us   &  we were very friendly in Egypt. 
 We are very comfortable here   &   the food is excellent - everything so nice   &  clean. Shall not feel the cold nearly so much as last year. 
 I'm not on duty yet no word 