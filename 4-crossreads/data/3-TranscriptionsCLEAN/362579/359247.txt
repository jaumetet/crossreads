  
 3rdd Aust. C.C.S. C/o 29th C.C.S. B.E.F. France 8.1.17 
 My dear Mother   &  Sisters, 
 Received Belle's letter dated Nov 6th yesterday, which I was delighted to get. 
 Last night I got a letter from Fred   &  he had not received one from me for months, so you see the letters go astray because I have written very often. He got the P.C. I sent so wrote straight away. He is well also all the boys. I'm interested in, that belong to his Reg. 
 Had a nice little letter from Norman   &  he too had not heard from me, nor received some cigs I had sent. 
 He was at Convalescent Camp Rouen when writing but was leaving for Etaples - so shall write to him there. Have not heard 