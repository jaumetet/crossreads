
3rd A.Gen.H.B.E.H.France7.10.17
My dearest Mother   &  Sisters,
Had a lovely lot of home letters this last week. One from dear old Belle 16th July with darling Mother's of 17th July. I am so pleased you had such a happy day  &  everything is so successful.
How I should love to be home again, but there is heaps of work to be done.
Poor old Kelly   &  yours truly are disappointed about