  
 My dearest Mother   &  Sisters, 
 Am delighted to say a nice mail arrived from Australia about 2 days after I last wrote. One from Alice dated 11th Feb   &  Ethel's with dear old Mothers dated Feb 25th. 
 The socks arrived to-day   &  yesterday a nice little New Year Gift from Annie Roddick. A Diary   &  Calander  so you see my old friends are still thinking of me.  I was so pleased to know that you are all well   &  seems to  be getting along all right. 
 Very pleased to hear that Elsie Gourlie is so happy, hope she has got the right man this time. 
 Have not heard from Charlie since I returned, suppose the poor kid is in France by now, hope he will be kept out of the firing-line for a time. It is awful to see the wounded here, we are very close so get the very bad cases. I'm afraid I shall soon be leaving here,  have done 6 months now so may 