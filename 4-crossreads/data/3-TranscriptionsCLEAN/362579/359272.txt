  
 We are hoping three of us get it together, we shall go to Blighty (England)   &  stay at the Ivanhoe Hotel where most of the Aust. Sisters stay. Only get about 12 days, so cannot say if I shall be able to visit any of the counties, shall have a try. 
 Be sure to address my letters in future to C/- Miss Conyers. My movements are so uncertain. 
 There is nothing to write about this week, have not been into Doullens, so no p.cards to send Eddy. Hope that darling is better   &  keeping stronger. I must try to send him something from England. 
 Fondest love   &  kisses to you all. 
 From Yours lovingly Edith 