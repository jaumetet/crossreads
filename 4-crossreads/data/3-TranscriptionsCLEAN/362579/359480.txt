  
 on Friday afternoon, had tea with Sister O'Dwyer   &  some other friends at Southwell Gardens - then went to Myra's for dinner   &  had a jolly time. Got back about 11. p.m. Shall not be able to go out much now. Packing to-morrow. Had a heavy fall of snow on Wednesday   &  Thursday but very wet   &  muddy yesterday   &  today . 
 Am feeling very well. Heaps of love   &  kisses. Hope you are all well. 
 Yours lovingly Edith 