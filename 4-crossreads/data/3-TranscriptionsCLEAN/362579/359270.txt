  
 3rd A.C.C.S. B.E.F. France 10.2.17 
 My dear Mother   &  Sisters, 
 Am still in the same spot but our Happy Home is moving. Oh, our hospital is going nearer, to we are attacked to 29th until our Huts can be built ready for we poor sisters. Three of us have applied for Leave but all leave is stopped for the present. We are like a lot of lost sheep. 
 I'm the only Sister in our camp this morning - strange. Orderlies   &  the huts all robbed of their furniture etc. the only patients are those who can walks   &  only about 100 at that. It is deadly lonely   &  shall be 