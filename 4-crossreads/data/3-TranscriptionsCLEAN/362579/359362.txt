  
 We are not in same tent, because she got here about 2 days before us, so had settled in one with Annie Bell. I'm with a Sister Macdonald (Queensland) who  was with me at the other C.C.S.s. to my joy quite a number of letter here for me. Alice's of 21 May. Ethel   &  Belle's of 27th May. The only one from Belle from Talbot, also must have missed some from home - suppose they have gone for ever to the bottom of the sea. 
 Received nice letter from Jess   &  Mollie. 
 Got one from Charlie   &  think he must have been on the move. They are not anywhere near us but we expect 