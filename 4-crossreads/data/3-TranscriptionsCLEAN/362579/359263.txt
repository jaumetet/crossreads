  
 Dorothy went on Night Duty last night   &   it was a beastly cold night. Suppose I shall not see much of her now. Still it is nice to have a pal near. 
 We may get another Sister on our Staff. Then there will be two for Night-duty, which will be much nice. Poor patients that came to-day are just from the Trenches covered in mud   &  tired out. Had a hot wash, put clean clothes on   &  well wrapped in blankets   &  put onto stretchers. Poor chaps think it heaven. 
 Had a good feed   &  slept soundly ever since. 
 Fondest love   &  kisses  &  hope to hear good news in a few days. 
 From Edith 