  
 Friday 12th Paraded in morning to give in Overseas Kit & draw UK Kit afternoon C.O. inspection etc 
 Saturday 13th Messed about all morning Got a weekend Pass Went to Sutton V - picked up Hall & went to Pictures. Had Poached Eggs on toast at Weslean Hut Stayed in Training Camp for night 
 Sunday 14th Stayed at Training Camp all day a good deal of rain 
 Monday 15th Went on parade as usual but an Officer took about 20 of us to see the Draft that leaves tomorrow make a sham attack on a trench line - A Platoon in attack it is called & is the latest idea for advancing with  a  few casulities as possible There are 2 Sections of 6 men & 2 NCOs each with rifles & some of the men have rifle grenades, & 2 Lewis Gun teams of 9 