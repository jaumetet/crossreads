
was Curate from end to end Got back to Camp about 11 PM after a few quite days out of the army. Have not given much detail about the leave.  The days & short & dark for sight seeing especially when one turns out late
Friday 28thMedical examination  Detailed for Coal Guard
Saturday 29thFinished Guard at 9 this morning Did some washing afternoon
Sunday 30thFell out as an R C this morning & as the R.C. Parade was going to Church as the O.P.D's were coming back I changed places & missed Church altogether rather a low down trick to play on oneself
Monday 31stWent to the Bull Ring Parade ground this morning fully determined to train diligently untill put on Draft for France but came a guttzer as I was detailed for Guard