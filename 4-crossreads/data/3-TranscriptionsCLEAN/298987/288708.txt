  
 they do with so many towns.This place looks as if it would hold a population of at least 5 to 6000 in normal times There are small villages all around The country is not particularly good & much of it is forrest or wood - there do not appear to be any inductees. All farm hands must live in the town as there are very few farm houses even then there seem to be too many towns 
 Monday 15th Went with a working party of 100 to a camp about 2 miles off The Officer in charge could find nothing to do so marched us back & told us not to go on Parade as we would be wanted Order obeyed, have spent the afternoon in the Hut 
 Tuesday 16th A little musketry & a lot of loafing Put tags on my overcoat  too as we  keep the tails out of the mud have to wear them on all parades now 
 Wednesday 17 