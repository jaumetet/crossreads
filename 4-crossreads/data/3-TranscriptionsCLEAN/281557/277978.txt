  
 Tune: Rock of Ages 
 Australia's battle Hymn. 
 God that made our Fathers strong Lead us when the dangers throng God that made our mothrs pure Make us steadfast to endure On the waves or tent or field Be our sword and battle shield Hail our Empire spreading far Great in peace and dread in war Banner brave as in the past Float on fort and tower and mast Let thy blazon flutter free South and North and every sea Here thy sons shall guard the gates of thy southward shores and straits On wide ocean we shall meet One unsundered flag and fleet And the pathway of the sea  Prove our broadening destiny Oh for peace or battle pray Nelson's signal points the way Duty's claim and country's call Shall be our concience for us all Set that signal high and then "Forward" Quit yourselves like men.                                             Refrain 
 God that watest through the day Gurad each seaward coast and bay God in loves unsleeping might Keep our homes through darkest night 
