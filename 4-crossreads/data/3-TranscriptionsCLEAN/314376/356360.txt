  
 49 and I had eventually to turn into another hut altogether sleeping under a table with some blankets borrowed from a chap who lived in the hut. 
 I met some of my old friends of the 16th reinforcements of the 3rd here from whom I had previously parted in Egypt, they were sent across to France in a draft the day that I arrived having been in Perham Downs (the day) for quite a time before I arrived. 
 I may say that I rejoined my old Battalion again here. You may remember that I was transferred to the 55th Battn in Egypt but on account of being "put back" at Alexandria missed them altogether. 
 I was very kindly received at the "house from home" a 
