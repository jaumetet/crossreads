  
 186 this I mean that it was possible to have a shell plumped pretty well in on top of us. 
 We were lying crunched up here and all of a sudden heard the cry "My God, I've lost my leg". Some poor fellow had evidently been struck by a shell. He started to cry also "Water". Generally the first thing that is done after a man is hit is for him to cry for water. 
 We stopped in this place for a couple of hours and then got the order to move up to a communication trench. We left our Great coats heaped up here. 
 I was under the impression we were going straight into an attack. Someone called out 
