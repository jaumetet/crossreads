  
 188 this for all branches were brought up to help in the attack. 
 I had brought the remains of a tin of jam along with me and had the intention of taking it along to the front line. Unfortunately in the excitement I forgot all about it. 
 We were detailed to fetch bombs down to Brigade H.Q. as the communication trench was filled up it was decided to go over the top and take the risk of being hit with the German shells. We reached the bomb dump without mishap although the shelling was pretty warm. 
 In addition to carrying 2 boxes of bombs we had our equipment and rifles. It was as much as we could to stagger along. The Germans were peppering the road 
