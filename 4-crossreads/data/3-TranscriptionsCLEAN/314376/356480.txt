  
 169 angry men are hard to pacify. 
 There was one young fellow killed as the result of the operations of the covering party. His name was Irwin. He had only been with the Battn for a couple of months. We had dug in very close to the Bluff and I suppose the Germans thought us very presumptuous for doing so. Anyway we had only been established for a very short time when the sentry on guard noticed the Germans coming through the Barded wire and making for our trench. 
 He fired off his gun and attracted the attention of the other men. 
 We all stood too and prepared to defend our position 
