  
 156 we had across open fields and I think, this was the hardest part of the trip. One pleasing feature of the journey was when past Bapaume (the way was across duckboards) we had a fair sight of green fields again. Being winter the trees were bare. Just outside Bapaume was a bare trunk of a tree which had previously been used as a resort for a sniper and also an observation post from which the trench system we formerly occupied at Flers was under review. 
 Eventually we arrived at our destination feeling fagged out. 
 We were complimented by our officer Mr Leslie on our endurance. We billeted in a stone stable or outhouse and 
