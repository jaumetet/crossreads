  
 54 rather lazy, but not me. 
 One noticeable feature was the number of recreation huts scattered about the encampment. The Y.M.C.A., Church Army, London City Mission, Salvation Army, all had places. 
 I remember one night after I had settled down, a great gale of wind sprang up and blew the London City Mission tent pretty well down. The Missioners, some of whom slept in the tent, had the greatest difficulty in keeping the central pole upright. I might say I got out of bed about 1/30, the thought being that I might want assistance. The Y.M.C.A. was a big hut. On the outside was a notice to the effect that it had been donated by the Revd and Mrs Sheldon. 
 Inside was a counter where it was possible for the soldier to buy any 
