  
 230 to within 50 paces they saluted him keeping their hands to the salute all the while. 
 Their huts too were very clean and orderly. They are I believe supplied with a different kind of food to what we have. This is on account of caste. 
 Coming back to our huts again we would have "lights out" blown (again) about 9 o'clock. I can relate an incident dealing with this. 
 One Sunday night I was desirous of attending a Service at the Y.M.C.A. Just as we were starting an order came that we were to take the Brigades Guards rations over to them at Bray. This was hard enough but we set out. 
