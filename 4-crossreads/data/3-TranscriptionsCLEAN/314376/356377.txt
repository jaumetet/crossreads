  
 66 it and the Sergt-Major would then detail someone else. 
 I received a parcel from home whilst in this camp but unfortunately it was just as I was leaving for the line. I was only able to have a little bit of the good things packed. 
 I divided the rest amongst the others. 
 The time now came for us to go up the line. 
 The Ypres sector at this time was a "quiet" part of the line, what is meant by quiet is nothing very exciting. 
 Generally when a division has been in some particularly hot part of the lines they are sent to a place where there is not much to be expected from the enemy, a sort of armistice prevails "you keep quiet and I'll keep quiet". I might say that whilst at the camp, I would go of a night 
