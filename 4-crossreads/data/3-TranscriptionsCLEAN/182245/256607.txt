 a6576118.html 
 326 
 1917 APRIL 
 2 MONDAY 
 A fair cow of a day. The ground 
 was covered with snow in 
 the morning & it was blowing 
 a cold gale all day & snowing 
 as well. Cleaning & fixing up 
 a loose box for my horse in 
 morning & lay & read all afternoon 
 Had a letter from Alan tonight 
 & he has his full lieutenancy 
 now & is a flight commander 
 as well. 
