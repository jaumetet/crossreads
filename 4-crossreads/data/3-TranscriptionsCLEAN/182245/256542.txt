 a6576053.html 
 261 
 JANUARY 1917 
 27 SATURDAY 
 Fine & Sunny. Went for a bath 
 again in the morning & it was 
 great. Had an inspection of 
 horses, harness ect by Capt 
 Campbell & he passed everything 
 Cleaning harness ect in afternoon 
 We were the section on duty, but 
 did not have to go out. 
 It was the Kiasers birthday 
 & we were half expecting a big 
 "strafe" but it did not come off. 
