 a6576125.html 
 333 
 APRIL 1917 
 9 MONDAY 
 Raining in the morning & 
 blowing very cold in 
 evening. Cleaning harness 
 act all morning  ^  two teams  & went 
 up to the battery in evening 
 We have always to take 
 six horse teams now, so 
 one man does not get a 
 spell every now & then. 
 We had a progress "500" evening 
 & had some good games. Bill 
 Constable & self were a tie 
 for first place with two other 
 couples & got second place 
 in a show poker hand for 
 places. 
   
