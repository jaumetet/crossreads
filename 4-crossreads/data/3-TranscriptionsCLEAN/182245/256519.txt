a6576030.html
238
1917 JANUARY
4 THURSDAY
Raining first part of the morning
& very cold wind rest of the
day. We were told that
the 27th Battery had been broken
up & that the right section
were going to the 25th Battery
& the left to the 26th. We got 
all packed up ready to go
& then the order came we
were not moving today & we
had to unpack it all again.
Clean harness & grooming
in afternoon. I am glad
we are going to 26th Battery
as we will have men over us
instead of an ignorant lot of lads
Although I dont like the moving
part of it at all.
