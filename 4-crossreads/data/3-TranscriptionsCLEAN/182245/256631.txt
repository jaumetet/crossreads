 a6576142.html 
 350 
 1917 APRIL 
 26 THURSDAY 
 Fine but dull & cold till late 
 evening when the sun came out. 
 We had 5 AM reviellie & moved 
 off at 8.30 AM & went to the 
 top of a big hill near "Desvres" 
 where we were inspected by 
 Col. Macartany & I was 
 complimented on my harness 
 After that we took up  ^  our  several 
 battery positions & came 
 into action. Gen Grimwade 
 inspected them. We got 
 home about 4 pm, & cleaned 
 harness till 5.30 pm 
