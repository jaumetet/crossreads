 a6576111.html 
 319 
 MARCH 1917 
 26 MONDAY 
 Raining off & on all day, & 
 very cold. Cleaning harness 
 all morning & I had to go 
 up to the battery in afternoon 
 on the pill box to report 
 to Capt Playfair, & got hauled 
 over the coals for a trifling 
 thing I put in a letter to 
 Alan about our bosses, - which 
 by the way was quite correct, 
 though I ought to have had  
 more sense than to put it in 
 a letter they would read. Anyway 
 I wrote the letter nearly three 
 weeks before it was 
 returned to me - no 
 wonder our letters are delayed. 
