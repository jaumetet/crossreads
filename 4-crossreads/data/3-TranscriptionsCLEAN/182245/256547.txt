 a6576058.html 
 266 
 1917 FEBRUARY 
 1 THURSDAY 
 Fine & Sunny. About stables all 
 morning & went up to Armentieres 
 with a G.S waggon in afternoon 
 Things were very quiet up there 
 though, hardly any shelling at 
 all. Got a letter from Mother. 
 
 from PAGE 157   6.6.17 
 tonight God keep you sweetheart 
 & remember I always loved 
 you & that I tried to do 
 my bit for the good old 
 homeland & you Linda dearest 
 Heard Alan was safe, but was 
 a prisoner in Germany & had 
 lost his left arm. Poor Alan 
 but its great to know he is 
 safe. 
