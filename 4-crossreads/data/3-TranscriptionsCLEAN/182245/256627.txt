 a6576138.html 
 346 
 1917 APRIL 
 22 SUNDAY 
 Fine & beautifully sunny & 
 warm. Cleaning harness 
 all day. Leave was granted 
 in afternoon to about 40% 
 & most of them went to a place 
 called "Desveres" about 6 
 kilos away. I got news from 
 mother that Alan had been 
 missing since 14 th  of the 
 month. I only hope that he 
 has not had any serious 
 accident, but has only been compelled to land in the 
 Hun lines. Lord grant that 
 thats all that has happened 
 He had just gained his 
 captaincy & been recommended 
 for the Military Cross. Its 
 rotten how all the best 
 are taken. 
