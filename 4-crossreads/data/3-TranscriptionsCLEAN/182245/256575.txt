 a6576086.html 
 294 
 1917 MARCH 
 1 THURSDAY 
 Fine & sunny. Cleaning harness 
 all day. We had to turn out at 
 6.30 pm & go down to the old 27 th  
 position in "Houplines" & draw 
 ammunition and take it to 
 our right section & then get 
 another load & take it to the 
 centre section. Got back at 3 AM 
 Saw a very smart bit of work 
 at the waggon lines in afternoon. 
 a Hun plane got into a cloud 
 coming over our way & hung 
 in it till he got over our 
 stables, & then he dived out 
 & went for an observation 
 balloon closw bye with his 
 machine gun. He missed 
 it first go & came back again 
 but missed it again. The 
 men came down in parachutes 
 & landed in our gun park. 
 he was only about 700 ft up 
