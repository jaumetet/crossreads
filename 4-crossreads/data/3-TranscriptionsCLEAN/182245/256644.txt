 a6576155.html 
 363 
 MAY 1917 
 9 WEDNESDAY 
 A beautiful warm day. Cleaning 
 harness all day, but I got all 
 mine finished & wrote a few 
 letters. The 26 th  Battery played 
 H.Q rugby in evening & we 
 won by 26 - 5. Fritz doing 
 a bit of shelling all day. 
 The roumer about Russia 
 was without foundation 
 thank goodness, & we hear we 
 are pushing old Fritz back 
 everywhere. Got a letter from 
 mother to say Alan had 
 the Military Cross. 
