 a6576139.html 
 347 
 APRIL 1917 
 23 MONDAY 
 A beautiful warm sunny day. 
 I was at a gas lecture in 
 morning & we had a half 
 holiday in afternoon. Bill 
 Hunter & self went up a hill 
 near by & lay in the sun 
 all afternoon yarning & 
 writing letters. We had a 
 beautiful view of the country 
 & it looks gloriously pretty. 
 Campbell recomended me 
 for S t  John's Wood Officers 
 school & filled in particulars 
 for same Had no more 
 word about Alan. 
   
