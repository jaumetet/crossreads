  
 our little Baggage Pile. We got some Tarpaulins & it kept the rain off but as the ground was so flat the water hung around. We had to dig trenches to carry the water off. I slept in a tin shed in the yard in the evening to escape the wet ground. 
 Wednesday 22nd December It was fine to-day & things were a little more comfortable. I recvd my first Xmas Hamper to-day - one from dear Mrs Attwater & a bundle of papers from others. It was a bit cold. 
 Thursday 23rd Dec It was bitterly cold. I went across to Camp & got some Pay & also an Xmas Billy Can sent by Miss Bailey 57 Taronga Road E. Malvern Victoria 
