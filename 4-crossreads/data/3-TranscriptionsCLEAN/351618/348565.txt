  
 The assault was made before the break of day But many difficulties turned their way And not a man did flinch that fight But charged the enemy till put to flight We said well done - They were heroes every one 
 In the years to come history will tell Of the great charge and how the brave Australian fell And as we ponder a deep prayer we breathe For the many heroes who now lay a-sleep In a place well known as Gaba-Tepe (Teep) 
 Tuesday 8th July I was on duty between 2am -3am this morning. Very lonely and plenty of bullets coming over and on our gun pit. At noon we opened fire on the enemy on to some gun pits. It some demoralised them and 
