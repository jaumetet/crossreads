  
 course we had some atrocious hot days with millions of flies to add further to the inconvenience of the heat. 
 Our Howitzers had a series of shots into some trenches of the enemy today and we had our gun and one other trained on to open fire. We did not fire but the enemy shelled us and killed one of our gunners named Driver. He was a fine little chap. About a week ago another of our lads named Bombardier McFarlane was shot by a machine gun. When he left for the Hospital Ship he was not expected to live. I am not sure if I mentioned before about the net-work of trenches [Indecipherable] in front of our gun by the Turks. Everywhere as far as the eye can see loopholes for attacking us can be observed. 
 At 10PM we fired several shots over into the Turks trenches and then withdrew our gun from the Pit. Star 
 All that he had he gave His joy of life is youth To Battle for the truth All that he has - this grave 
