  
 get articles indented etc. 
 17th Sept Tuesday A cool breeze blowing all day. Like rain again. Every one wants blankets etc. Wrote letters to-day. 
 Saturday 18th Sept Quiet 
 Sunday 19th Went to Ordnance again. My gang fitted themselves out - ! - a good deal of artillery firing to-day. last night our infantry attacked the Turks & captured several trenches near lone Pine 
 Monday 20th Sept A Blowy day & keen 
 Tuesday 21st Sept The same. We had a lot of shelling to-day. 
 Wednesday 22nd Very keen again. Winds just like our Westerly. Busy on fresh indents etc. Ordnance & Ground 
