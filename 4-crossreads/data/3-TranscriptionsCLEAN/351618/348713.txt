  
 East & Hodgens & ohs [others]. 
 Saturday 18th December My sleep was a good one last night notwithstanding I did not appear a welcome visitor in the Sergeants tent in which I had to rest. They tried to crowd me out. After Breakfast I packed up & went back to the Baggage it the Pier. I went to the Canteen & got some tinned fruit etc for the Guard & also got some Beer which I had to manage with a little bit of diplomacy. We fared well in cooking stuff & I had a game of Bridge with the 2nd Batallion Corpl & Corpl Preston & another comrade 
 Sunday 19th December It looked overcast to-day and I am dreading any rain. The evacuation still continues and batches of men are arriving from Anzac & Suvla here every day. 
