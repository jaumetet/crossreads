  
 Monday 15th Nov To-day it was showery and looked as if we were in for some heavy weather. Owing to bad sea the 3rd Brigade Infantry could not leave last evening for a spell at Lemnos. Went Y.M.C.A. Canteen for 7th Battery but found no supplies had come across. The Turkish Artillery opened up on all our positions and sent across a big lot of shells. On the beach a number of men were wounded. It rained heavy in the evening. 
 Tuesday 16th Nov The sea is still very angry looking. Yesterday while coming along to North Beach [just North of Anzac Cove] the scene was very fine although carrying destruction with it. The sea was lashed into spray on the beach & over the jetties etc. 
 The Gurkas here are a fine 
