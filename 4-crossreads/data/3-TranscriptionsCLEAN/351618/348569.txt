  
 and in return received a lot of shelling. Several Light-Horse men were killed. 
 Tuesday 13th July [1915] Tobacco issue day. Fairly quiet except towards evening when the firing line was very busy as the Turks tried to creep up on our trenches. I was transferred back to my old gun and shifted up. Olly Gunderson visited me. 
 Wednesday 14th July Not too good - no appetite Mentioned in despatches in connection with work done early in our operations [Sparkes' service record states "Special mention in Div Orders No. 161 for acts of conspicuous gallantry or valuable service during period 6 May to 8 June 1915".  See page 62 below]. Myself and a few others of our 7th Battery. Last night there was plenty of rifle fire. 
 Thursday 25th July Feel better today. Quiet up till sundown. I wrote a number of one page letters while at Gaba Tepe and these were all done while the shells thundered & the bullets pinged on all sides. Of course you would all have heard 
