  
 The Governemnt, led by Lieut Nelson (15th) contending that "The White Australia Policy should be altered after the war." The Government was beaten by about 300 to 20, the leader of the opposition (Cpl. Champion, 13th Bn (a N.S.W teacher) becoming Premier for next week. He is to argue that "State Governors & Parliaments should be Abolished." 
 Some of last nights speeches were very interesting & well delivered. There were speakers from every state. 
 This evening our Camp Pierrots give their first entertainment in YMCA Hall. 
 Entry Sunday  20/5/17  
 Raining this afternoon. I arranged to motor to Bath with Capt MacPherson (16). Lt Towers (16) & Lt Thorpe (15) but car did not turn up. So we remained in mess & spent an 