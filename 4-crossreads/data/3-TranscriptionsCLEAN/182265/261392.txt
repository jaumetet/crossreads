  
  11th  This morning I slept in late. When Westwood came in it was about 11am. So we waited here at Hotel Moderne for lunch. I wrote to Jay & prepared postcards for the post. This afternoon we visited Bastille, Boulevarde Germain, Chambre des Deputes, Effel Tower, the Great Wheel on which we went. 
 Returning we went through  Commerce  a very busy shopping suburb. Here I bought a few articles for Billee. 
 A tube (Metro) brought us to  Madeleine   from  near  Rue Royale  in which is the famous  Maximes  where we had a couple of Manhattans with another Australian officer we met there. 
 After Maximes we walked Boulevards des Capuccines & continued right along the street to Republique. 
 After dinner we had an interesting conversation with Miss O'Connor an Australian Artiste who has lived many years in Paris & Madame Rieu whose husband, an Englishman, was killed two months ago in The Foreign Legion of France 