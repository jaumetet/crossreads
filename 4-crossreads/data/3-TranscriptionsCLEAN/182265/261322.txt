  
 Chap who once lived in Petersham & who belonged to the Petersham Choral Society. Dined at Australian Officers' Club at my expense & then at his expense we visited Savoy Theatre where Henry Irving was playing " The Bells ", preceded by "Waterloo" - both excellently acted. Met Major Marks & "Capt "Bom" Wells. 
  22nd - Tuesday  - Visited British Museum, but found it closed. Met Lts John Brown & Wallach (13). Caught 2pm train from Paddington to Birmingham. 
 Visited Dudley Road Section Hospital & saw Reg & his mate Moroney. They were in the same tent in Kiama, came together, in action together & both now have right wrist severely wounded. One holds his meat down with a fork while the other cuts, one pulls one side of a knot in a bootlace, while the other pulls other side.  
  Story  
 Capt. T (19)  "Well what about surrendering boys." 
 "Not while we've got a bomb or a bayonet Sir." This reply so heartened the 