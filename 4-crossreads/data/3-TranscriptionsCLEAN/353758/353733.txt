
continuous rifle fire all round and schrapnel bursting all round the beach - we went to the head of the valley and occupied a ridge (afterwards known as Pope's Hill after Colonel Pope) - About 200 Yards from the Turk's Trench we kept up a continuous rifle fire on them and they returned the compliment many of our chaps killed and wounded but I hope more of the enemy.
