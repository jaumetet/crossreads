  
 [Some of the previous entries appear to be Hutton's diary for part of 1917, written in the front of the 1916 diary. There are no entries from February 10-February 24] 
 Friday 25 Left Sydney on a flying visit to say good bye Saturday 26 Arrived home on final leave Sunday 27 Down on the farm with the good folks Carnsdale, the place I will see at the end 
