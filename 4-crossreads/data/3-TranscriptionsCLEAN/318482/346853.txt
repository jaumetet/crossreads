  
 Copy. 
 To The Official Secretary, High Commissioner's Office, Australia House, Strand, W.C. 2. 
 Chief Pay Office, Australian Imperial Force, Administrative Headquarters, 130, Horseferry Road, London, S.W. 1. 8th February, 1919 
 re Hon. Captain Lambert, G.W.  Official Artist. 
 I am in receipt of your letter No. 1735 of 31st January together with enclosure as mentioned therein, and as requested have cabled to the Staff Paymaster, Cairo, in accordance with Captain Lambert's wishes. 
 2.  Upon receipt of a reply that the necessary action has been taken, I will advise you the date on which the allotment of 20/- p.d. to Mrs. Lambert comes into operation. 
 (Sgd.)  S.B. Holder, Major. for Chief Paymaster 
