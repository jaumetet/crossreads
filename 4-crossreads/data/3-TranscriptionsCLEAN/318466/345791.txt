  
 Sunday 12 May 1918 Went home to Highgate for the day.  Have to wear dark glasses & shade when I am out. 
 Monday 13 May 1918 Rumours of an Air Raid which did not eventuate. 
 Tuesday 14 May 1918 The bus conductors are very decent & always let the wounded chaps on before the rush.  Am gettg. to know one of 'em on the Charing X run!! 
 Wednesday 15 May 1918 Went with a party from the Hospital to George Robey's concert at the R.A.C.  He gives one every week for Colonial Officers. 
 Thursday 16 May 1918 Read a bit in the morng. to Fox our patient.  Doc says there is no hope of him gettg. his sight back. 
 Friday 17 May 1918 Most of the ward are up and walkg. now.  All wearing dark glasses. 
 Saturday 18 May 1918 Leave for week end from Hospital spent at Highgate. 