  
 Sunday 10 November 1918 27 today - 1891. Fed to the teeth. No birthday present from Rup.  Thank you!!  Had lunch at Bowes with Ede.  Some people came over to tea & supper. 
 Monday 11 November 1918 Armistice signed.  News came thro' at 11 o/c.  Was in my bath!!  Took Rup to lunch in town & everyone was mad!!  Saw the King. 
 Tuesday 12 November 1918 Into Town again on my own.  To Aussie House, Lloyds & Dominion Express people.  Probably sail on Saturday next.  Bought a watch.  Very cold.  People still mad in Town. 
 Wednesday 13 November 1918 Rup went to Town for lunch with Mrs. Curtis.  Ede came over to tea.  Took her out to dinner later as the house was not exactly lively. 
 Thursday 14 November 1918 Up to Aussie House for labels & final instrons.  Back in afrn. & packed luggage & took same to Euston with Muriel.  That new watch of mine is a washout. 
 Friday 15 November 1918 Down to C.P.R. & got tickets altered for a double berth.  On to Euston & bagged a seat early.  Ede, Muriel & the Tibbitts came to see us off.  Reached Liverpool at 5.30 & stayed "Adelphi". 
 Saturday 16 November 1918 Embarked at 12 noon on the "Balmoral Castle".  Beastly cabin so got it changed.  Not a bad ship.  Food quite good.  The MacIntoshes are aboard.  Beaucoup A's also. Embarked on the Balmoral Castle for New York.  The food on her is a great change to England's war rations. 