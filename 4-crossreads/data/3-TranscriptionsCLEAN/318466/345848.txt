  
 Sunday 1 December 1918 Entered the Mountains & are crossing them all day.  Some bon engineering.  Scenery very rugged & grand. 
 Monday 2 December 1918 Arrived in Vancouver in the rain at 9.45.  Went to the Lotus Hotel & was not much struck with it.  Got passport vised & tickets fixed. 
 Tuesday 3 December 1918 Brekker in bed.  My cabin trunk came along but no other baggage.  Raing. like Hell.  Wrote to Dad J. in the evening. 
 Wednesday 4 December 1918 Went on "Niagara" in morning.  Saw our cabin.  Lunched at the Good Eats!!! & then took Rup to Stanley Park.  The zoo was a failure but the trees quite bon. 
 Thursday 5 December 1918 Had lunch with Capt. Hamilton & his wife at The Hud. Bay.  Went to the Pictures afterwards & dinner at the Vancouver. 
 Friday 6 December 1918 Raining.  Lunched at the Citizens Club with the Hamiltons.  Out to  Cairngorms in afternoon.  Dinner at the Castle Hotel & dance after. 
 Saturday 7 December 1918 Packed up.  Lunched at the Hudson Bay Co.  Got the baggage down to the "Niagara".  Saw the Hamiltons off to New York.  Slept the night aboard the "Niagara". Embarked on the "Niagara".  Had the week in Vancouver. 