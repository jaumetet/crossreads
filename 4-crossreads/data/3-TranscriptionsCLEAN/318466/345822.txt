
Sunday 1 September 1918"Hopped the bags" at 6 a.m.  Sgt. Stephenson was wounded early.  Killed later on the stretcher.  Was shocked badly myself & broke down later & had to leave.
Monday 2 September 1918Left the Line late last night.  Don't remember much what happened.  Feeling done to the world & very jumpy.  Am now at Transport Line.
Tuesday 3 September 1918Had a restless night and plenty of bombs.  The Bn. came out of the Line about 50 per Coy.  Feeling deuced nervy & shall report to M.O. tomorrow.
Wednesday 4 September 1918Went up to the Bn. in morng. & saw the C.O., sent up my report later & waited to go off to Rein. Camp but the orders did not come thro.
Thursday 5 September 1918Orders came thro' late last night & I moved to Corbie in morng. in 47th Batt. sidecar.  Reached thr. in aftern.  Learn that the 14th nucleus are movg.
Friday 6 September 1918Up early on a job re the movg.  Am transferred to the Wing.  Into Corbie for a bath & on to Amiens.  My valise not turned up yet.  Met Capt. Burrell in Amiens.
Saturday 7 September 1918The Div. Wing moved from Corbie to Bray.  I marched part of the way & then hunted round for my valise wch. I found.  Saw Alb on the road & spent the night in Corbie with him.Met Alb this week after a long interval.  Saw him in St. Omer in July 1917.