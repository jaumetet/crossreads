  
 Sunday 17 February 1918 One of our 'planes crashed just behind our lines - believed hit by one of our 18 pdrs.  Lovely day & very cold.  My bed of stretchers is not too good.  Spent eveng. at Forret Farm. 
 Monday 18 February 1918 Weather still good.  Feelg. tired after last nights walk to the shell holes.  My applon. for staff job went thro'.  Called on the 89th Battery. 
 Tuesday 19 February 1918 Anor. topping day.  Paid visit to the 53rd Bn. re taking over.  Boche 'plane brought down.  Feelg. grumpy. 
 Wednesday 20 February 1918 Came out of the Line & were relieved by the 53rd.  Our home is a Black Hole of Calcutta.  Took over H.Q. mess. 
 Thursday 21 February 1918 McGlashen & Grimsby join Mess.  Went round with C.O. in afternoon.  Wrote v. long lre. to Rup.  Lord, How I do hate the Kaiser. 
 Friday 22 February 1918 Raing. nearly all day & bad visibility.  Cigarettes & Punch from Rup.  Heard that my staff trainee applon. had gone on to Corps. 
 Saturday 23 February 1918 Paid canteen 50 fr. on A/c.  Paid a visit to the Comforts Fund show & "hummed" anor. box of biscuits!!  Things are very quiet.  Comforts Fund Canteen is quite a good place.  Everything is given away free & is much besieged by the diggers. 