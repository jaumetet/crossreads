  
 Sunday 17 March 1918 Called on the M.T's & had a trip in thr. motor cycle round the country.  Ratted the dugouts with Slaughter & lost the bally ferret!! 
 Monday 18 March 1918 Dug out for the ferret but it was not thr. Into Bailleul later on motorcycle & bought a few things.  Ferret turned up after all!!  Had a bath in Slaughter's place. 
 Tuesday 19 March 1918 Raing. for the first time for weeks.  Stayed in all day and rested my foot.  Dalkeith came back from Blighty. 
 Wednesday 20 March 1918 The shelling of the back areas is no fun believe me!!  The Officers Club is getting unpopular!  Had a rat hunt with Slaughter in afternoon.  Stayed to dinner & played Bridge. 
 Thursday 21 March 1918 Hun Big Attack begins down South.  Came up to the jolly old line again.  My foot is still putrid.  Went over to the good old Pillbox at Ourset [?] & relieved Bone (53rd Bn.). 
 Friday 22 March 1918 Nothg. doing much cept that everyone is more or less on the q.v.  Had my heel dressed at the A.D.S.  Two lres. from Rup.  Gen. Hobkirk goes. 
 Saturday 23 March 1918 On the dawn watch & all.  All's quiet on the western front!!  The Boche make thr. big offensive down at Bullecourt, etc. and manage to get thro' a good distance. God gave us our Relatives. Thank God we can choose our friends!! G.W.B. [The quotation is actually by Ethel Watts Mumford] 
 