  
 Leonard V. Bartlett 
 Egypt - Gallipoli - France Enlisted 1st Sept. 1914 Wounded Bullecourt, May 1917 Gassed - Villers Bretonneux, April 1918 Discharged - February 1919 
 [Transcriber's notes: Mondicourt - abbreviated to Mondict. - Page 19 Villers Bretonneux - misspelt as Villers Bretoneux - Page 21 A.I.B.D. - Australian Infantry Base Depot - Page 40 A.S.C. - Army Service Corps C.P.R. - Clerk (Pay and Records) - Page 47 F.M.O. - Full Marching Order G.C.M. - General Court Martial H.S. - Home or Depot Service - Page 37 L.T.M.B. - Light Trench Mortar Battery - Page 43 R.F.C. - Royal Flying Corps S.B. - Sam Browne belt - Page 43 S.M.O. - Senior Medical Officer - Page 37] 
 [Transcribed by Judy Gimbert,Betty Smith, Peter Mayo for the State Library of New South Wales] 
 