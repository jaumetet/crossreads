 a6518015.html 
 40 Eveleigh St 
 Redfern 
 April 30th 1916 
 To no. 8597 
 S.Q.M.S Waters, J.(G?). 
 19th Field, Butchery 
 A.S.C.    A.I.F.   Egypt 
 Dear Tom, 
 Believe me, although I have not written before, you are always in my mind's eye whom I look back and think of the times we met during the last ten years. Firstly at Parramatta or Chatswood, when you chastised me for asking for "Joe", having just arrived back from Manila P. Ils. I had the free and easy ways of our brothers in U.S.A. and have never been able to shake them off. And I certainly feel that you will be the same when you return to this land of nice weather, you will be the same everybody around you will look out of date, everybody impractical, unforseeing (to you). They don't seem to have any trouble in getting butchers, the last batch of fifty was received in a few hours. I attended a meeting (and am keeping up the attendance) of the delegate and I found the speeches vitally interesting, the delegates could not shake off the Idea that the Sydney union is a little primitive in its business methods and administration, of which is not without foundation, the reason I presume was loss of confidence by members for many reasons. Mr Woods was getting a bit off his chest by saying shopman should knock off to time and I said during my residence in Newcastle 