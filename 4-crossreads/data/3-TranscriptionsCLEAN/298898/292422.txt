  
 the family, three spanking girls which meant that we missed tea & spent 4 hours there. Had sundry bottles of champaygne, old English 1904 at 18 francs 50 a bottle which meant that we had a most enjoyable time. Drove home at a great pace & finished celebrating the New Year at our billet. 
 Tuesday 2nd Jan. 
 Had leave granted to England today & have just completed packing. My first leave for over two years so am looking forward to it greatly. Went by car today to various battalions to evacuate sick & had a good run round. 
 Have just returned from leave & will try & write up my diary for that period. I found it utterly impossible to do so when away, my time was too much occupied, spare time all too little was spent in sleep. 