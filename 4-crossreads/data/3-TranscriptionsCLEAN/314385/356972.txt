  
 Sun Aug. 27th Revalli at 6.30 am & duties allotted. 
 I am put to guard a German prisoner I am so pleased he is a casualty walking wounded he cannot talk English & is massive. Another one has just come in & I have to keep one eye on each. 
 This place is a prewar chateau owned by a Parisian banker and is now used as a dressing station there about 100 rooms & it is built on 3 sides of a square with a beautiful garden of roses in the square & a massive iron rail & gates on the frong & acres of beautiful grounds at the rear. 
 My prisoners moved on & I am put into the dressing room as dresser (6 of us) & 3 shifts 8 hours on & 16 off we dress and bandage the wounded as they come in from the front but only walking cases. 
 Mon Aug 28th Pack room duty today receiving patients & checking thier packs. Patients are comming in more 