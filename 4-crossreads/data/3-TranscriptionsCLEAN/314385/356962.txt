  
 [Previous page duplicate] 
 Diary of Harry Pidcock Mason 1916 
 Monday August 14th - After breakfast twenty of our ambulance I included were warned to parade with full packs in marching order, what's going to happen now thought I however most of us were picked from the tent division. 
 Ambulance motor cars were brought round which we boarded and were taken away and after an hours journey we found ourselves landed at No 3 C.C.S. staffed by B.E.F. & we were evidently sent there as auxilliaries it is close to a hamlet called ["Ponchvilliers"?] the railway runs by & takes all the patients to the Base hospitals as they are evacuated. 
 This C.C.S. is very commodious and is capable of holding four to five hundred patients and is all under canvas run by the R.A.M.C. We were paraded and allotted duties on on our arrival as stretcher bearers and unloaders 
 Tuesday 15th Our duties are as the cars arrives from the Main & advanced dressing stations with stretcher cases to unload and take them to the receiving tent where 