  
 [Administration Order also included the German translation, not shown here] 
 THE MILITARY GOVERNMENT OF NEW GUINEA. ADMINISTRATION ORDER No. 1 Colonel W. HOLMES, D.S.O.,V.D. Brigadier Commanding, 11th September 1914 
 1. All inhabitants are to submit to the directions of the Officers of the occupying force. 
 2.Inhabitants are forbidden to be out of doors any night between the hours of 10 p.m. and 6 a.m. without special permits. 
 3. Inhabitants are forbidden to hold or attend meetings. 
 4. No newspaper, circular, or printed matter is to be printed, published or issued without permission. 
 5. No spirituous or intoxicating liquor shall be manufactured or sold without permission. 
 6. Descriptions of all privately owned boats and vehicles are to be handed in at once to the Provost Marshal of the occupying force. 
 7. It is forbidden to injure or cut the telegraph or telephone lines.  If the telegraph or telephone lines are injured and the offender cannot be discovered a fine will be imposed on the inhabitants of the neighbourhood where the damage was inflicted. 
 (Sgd) Francis Heritage Major Brigade Major   
   
   
   
   