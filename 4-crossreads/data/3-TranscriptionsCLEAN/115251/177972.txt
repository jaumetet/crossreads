
PROCLAMATION:
Proclamation on behalf of His Majesty George the Fifth, by the Grace of God, of the United Kingdom of Great Britain and Ireland, and of the Dominions Overseas, King, Defender of the Faith, Emperor of India.
BY
Colonel William Holmes, D.S.O.,V.D., Brigadier Commanding His Majesty's Australian Naval and Military Expeditionary Force.
Whereas the Forces under my command have occupied the Colony of German New Guinea;
And whereas upon such occupation the Authority of German Government, has ceased to exist therein.
And whereas Doctor Haber, the Acting Governor of German New Guinea, has surrendered and handed over the Administration to me;
And whereas it has become essential to provide for proper Government of the said Colony, and for the protection of the lives and property of the peaceful inhabitants thereof.
Now I, William Holmes, Companion of the Distinguished Service Order, Colonel in His Majesty's Forces, Brigadier Commanding the aforesaid Expeditionary Force, do hereby declare and proclaim as follows:-
(1)  From and after the date of these presents, the Colony of German New Guinea is held by me in Military Occupation in the name of His Majesty the King.
(2)  War will be waged only against the armed Forces of the German Empire and its Allies in the present War.
(3)  The lives and private property of peaceful inhabitants will be protected, and the laws and customs of the Colony will remain in force so far as it is consistent with the Military situation.
(4)  It the needs of the Troops demand it, private property may be requisitioned.  Such property will be paid for at its fair value.
(5)  Certain Officials of the late Government may be retained, if they so desire, at their usual salaries.
(6)  In return for such protection, it is the duty of all inhabitants to behave in an absolutely peaceful manner, to carry on their ordinary permits so far as is possible, to take no part directly or indirectly in any hostilities, to abstain from communication with His Majesty's Enemies, and to render obedience to such orders as may be promulgated.
(7)  All male inhabitants of European origin are required to take the oath of neutrality prescribed, at the Garrison Head Quarters; and all firearms, ammunition, and war material in the possession or control of inhabitants are to be surrendered forthwith, as is also all public property of the late Government.
(8)  Non-compliance with the terms of this Proclamation, and disobedience of such orders as from time to time may be promulgated, will be dealt with according to Military Law.
(9)  It is hereby notified that this Proclamation takes effect in the whole Colony of German New Guinea from this date.
Given at Friedrich WilhelmshafenThis  Twenty fourth day of September 1914.
(sgd) William HolmesBrigadier Commanding
Witness:(sgd)  Francis HeritageMajor.Brigade Major.
God Save the King: