 a6491164.html 
 May 8  th    Left Park at 5.30 A.M. for Ailly-S. Somme on supplies to the brigade at Freshencourt  the . Lord Strathcona's Horse, Royal Canadian Dragoons & Fort Garry Horse (Canadian Cavalry Brigade) were standing to all day expecting action, but there was nothing doing, they are camped at Frechencourt. When we got back to Park we had to change a wheel as our tyre was done. 
   
 May 9  th    We left the park at 6.A.M. for Flesselles railhead & took Amunition to a dump at "Daours" on the Corbie road - We saw one of our planes bring a Fritz machine down over Amiens - the road was very rough. 
   
 May 10  th    We left Park at 8.A.M. for Flesselles & took a load of Amunition to Daours dump - We watched some French naval guns in Action just outside Daours.The weather is much better now but not too warm - We got back to Park at 5.30.P.M. 
   
 May 11  th    We left park at 6.A.M. for Ailly-S.S. on Supplies to Frechencourt - We brought a load of wool back to Ailly.S.S. in the afternoon - got home at 5.P.M. 
