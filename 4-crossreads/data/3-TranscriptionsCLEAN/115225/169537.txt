 a6491140.html 
 Mar 5  th    We did'nt go out again to-day, we went up to Suzanne's in the afternoon & I got Suzanne to sew some colour patches on a tunic for Joe - Frank & I went to Bailleul in the evening & saw the Sentimental Blokes at the concert hall - To-day has been fine but a bit cold. 
   
 Mar 6  th    We did'nt go out again to-day, some of the lorries went out - Frank & I got passes & went into Bailleul for a hot bath. had tea at Marie Louise's 41/2 francs each & got home again 7.30 P.M. lovely day. Warm 
   
 Mar 7  th    Fine day but misty. we left Park at 6.30 A.M. & took a labour party to Neuve Eglise  Frank & I went for a walk over Messines ridge way forward of the18 pounders. it was misty & we went further than we had intended, we ran into the 18 pounder anti tank gun out on its own. on our way back we got home at 4.P.M. Jim got back from Blighty. he had been boarded for Aussie & passed & was waiting going home, he went out & got boozed & hit up a bit rough & they gave him his khaki & sent him back to France. We went up to Suzanne's in the evening - 
