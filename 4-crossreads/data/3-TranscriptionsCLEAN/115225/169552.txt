 a6491155.html 
 before sunset. we turned round, & went back as quick as we could, the road was under fire, & it was difficult to dodge the dead horses etc  on  & shell holes on the road - We went to Boves, where we were halted by the sentry as the town was being shelled. Eddie phoned up S.M.T.O. & found out that we had been given the wrong Berteaucourt. We were then directed to go to Berteaucourt les dames which was also by a Vilage of Domart, but near Vignacourt. We arrived there (after travelling all night) at 4.A.M. 
 Apl. 13 had breakfast & a sleep & at 6.30 we loaded up with headquarters stuff & took it to S  t   Roche (Amiens) Station the 1  st   Div still entraining - We got back to Park at 12 noon had dinner & went to bed till 7.30 P.M. had tea & wrote letters - We had an interesting time on the French front & were lucky to get out as we did - 
   
  Memo   The weather has been generally rainy this week with a little sun - 
   
 Apl 14  th    We left Park at 8.30 A.M. for Varennes stone dump & fetched a load of stone back to the Allonville  les Alencons   Petit Camon  road where they were making an amunition dump - 
