 a6491130.html 
 Jan 29  th    I got up & had breakfast & caught 8.30 A.M. train to Calais, where I had dinner & caught 1.20 P.M. to Bailleul - (on the Paris trip a man travells in comfort on a French civilian train paying his own fare, which is quarter rate to Soldiers) had supper at Marie Louise's Joint & walked out to the park where I arrived 8.P.M. where I told of the good time I had spent in Paris & I swore that I would never go to Blighty again on leave, Paris will always do me 
   
 Jan 30  th    Talking shop again now - The lorry had'nt been out since I left, Jim went to Blighty on leave on 21  st   last night was cold but to-day is warm & the sun bright, I spent the day reading in the lorry - a 15 minute barage at night . 
 - otherwise all quiet - 
 Jan 31  st    Quite a bit colder to-day. I stayed in the lorry all day & studdied French which I wanted to learn for my next leave in Paris. I wrote several letters in the evening - 
   
 February 1  st    Nothing doing to-day - cold misty day, had the back down all day & studdied french by artificial light & read all day - 
