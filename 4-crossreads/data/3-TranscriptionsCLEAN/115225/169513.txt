 a6491116.html 
 batteries & collected the other personnel of our little unit & went back to Park about 11.P.M. We enjoyed the day though it ws anything but a homey Xmas. it was way ahead of last Xmas spent on the old Medic. 
   
 Dec 26  th    We went out on supplies as usual the snow from last night stayed on the ground & it was squally again to-day, with sunshine in between whiles. Jim stayed the night with his brother in the amunition section at La Creche 
   
 Dec 27  th    I picked Jim up on the way to Steenwerck for supplies - We went back for coal for the brigade & delivered same  We had some whisky at the 18  th   canteen & went back to Park feeling very pleased with ourselves & looking  thro  at the world through rose-coloured glasses - We stayed in the lorry all the evening & with the primus kept good & warm. We have an issue of rum every night which we use as a night cap - 
   
 Dec 28  th    We went to the Caestre tyre-press & had a  new  rear wheel re-tyred & came back Via Hazebrouck & La Mott, Foret de Nieppe & Vieux Berquin. certainly a long way round but we wished to see  
