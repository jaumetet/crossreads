 a6491123.html 
 Jan 18  th    Fine day & warm, I brightened up the buss a bit in the morning - which consisted of wiping it over with a mixture of petrol & oil to make it look as if it had just been painted, so that Fritz could see them well with the sun shining on them - I read all the afternoon - I received an Xmas billy from Queensland which I thankfully acknowledged in the evening - 
   
 Jan 19  th    Fine & Warm - nothing to do all day  My Paris leave came through & I leave to-morrow morning - Frank & I went to Bailleul & had a hot bath - went back to Park & straightened up my kit - 
   
 Jan 20  th    I walked into Bailleul & caught 8.A.M. train to Calais had lunch at a restaurant just outside the station & caught Paris express at 12.10 - I handed a note to an M.P. on the station at Etaples as we passed for my sister in which I said that I would hop off & see her on my way back from Paris - arrived in Paris 8.30 P.M.  met at station by charabanc & we were taken round to Pepiniere barracks had our passes fixed up, had a good meal at Canadian Y.M.C.A & were taken by charabanc to an hotel recommended by Y.M.C.A.  Hotel Prince Albert 
