
26.10.17. Wet all day. Home  Taylor in Ypres. Carried & chopped wood. Water and generally cleaned up & Read
27.10.17. Fine day. Rebuilt door of dugout and did little jobs of making signboards & a sieve. Wrote some. Both home
28.10.17. Taylor out. Good day  Took two loads 100 duckboards to 9th    &  10th Batt in morning and aftn cleaned up procured coal and wrote in evening  Poole gone out. Sunday
29.10.17. Taylor out. Fine day   Continued guard usual. Wrote in morning cleaned up in afternoon.
30.10.17  Wet & strong wind. No work. Read &  cleaned up.
31.10.17. Lovely day. Horselines in morning. Read & cleaned up. Letters.
1.11.17. Fine  Cloudy. Nothing till guard at 4.30. 3 infantry prisoners. Usual guard.
2.11.17. Guard  Wrote 5 letters and cleaned up for leaving for line to-morrow. Pay in evening. Cloudy but warm.
3.11.17. Up at 3 and moved off to dump at 4.30 with blankets etc in pack and caught lorries about 5.30 to Hell Fire. Thence marched to Railway line to pill boxes at Westhoek where No 3 stayed &