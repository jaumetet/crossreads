  
 Friday 1/3/18. Snowing & raining. Bn marched to Rifle Range but unable to shoot (10 miles). 
 Saturday 2/3/18  Snowing & raining  had a game of bridge  wrote to Doff. 
 Sunday 3/3/18. Church parade, went for a long ride, very cold weather. 
 Monday 4/3/18. Battalion to Rifle Range at Escoulles [Escalles?] - very good shooting under bad weather conditions 
 Tuesday 5/3/18. More Shooting- Bn very good indeed, had a game of bridge. 
