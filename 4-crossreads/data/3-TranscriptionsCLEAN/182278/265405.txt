  
 Wednesday 20/3/18. Raining heavily all day- relief postponed  had a game of bridge. 
 Thursday 21/3/18  Left by car at 6 am to reconnoitre front- heavy gas shelling by Hun. Took over Bde Front from the "Old Bde". 
 Friday 22/3/18. Feeling a bit off color from effects of gas. Saw Ralston re vickers. 
 Saturday 23/3/18  Went around whole support line with Read of Engineers. Hun Offensive Bewteen Arras & St Quentin begun. 
 Sunday 24/3/18. Took over neucleus Garrisons from 5 th  Bde- Went around 
   
