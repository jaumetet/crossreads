  
 Monday 15 October 1917. 
 On watering parade. 
 In afternoon at 4.45 pm. "Jacko" came over & was shelled and returned to his own land. Another Taube appeared and one of our battle planes went up to meet him. The fight lasted 5 mins. for position; a machine gun rang out and one of the planes nose dived and then tumbled like a feather over & over towards earth: 1/2 way down an explosion occurred in the machine and then with smoke coming from her she fell right to earth. 
 The victor planed down a little to make sure if machine & pilot & observer were done & then made off. The victor to us is at present a ? 
 Latest news 8.30 pm. Taube brought down by same Flt Officer that brought the Taube 
 [Continued on previous page.] 