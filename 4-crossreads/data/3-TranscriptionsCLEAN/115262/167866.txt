
Monday 9 April 1917.
Caught 10.45 am train to D'nong.
Spent day in D'nong & caught 8 pm train back to Melbourne.
Had a good day's enjoyment.
(Enjoyable Easter leave over)
Stayed at Burnley (74)
Bed at 10.45 pm