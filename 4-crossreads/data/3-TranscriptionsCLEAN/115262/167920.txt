  
 Saturday 2 June 1917. 
  "Still at sea"  & air very foul in No 3 & No 4 deck. Paraded with bad cold & was given "no duty". (1 st  time since entering camp that I've had to parade sick). Japanese Cruiser signalling  "short of coal"  
 We are now 1 day behind time & at 6 pm have 155 tons of coal left in bunkers to get to Colombo - now 250 miles away. 
 Stayed above deck & read & yarned. Went aft at 6 pm & watched "log". 
 Air slightly improved & cold feeling a shade better. 
 Speed 8 to 10 knots per hr. 