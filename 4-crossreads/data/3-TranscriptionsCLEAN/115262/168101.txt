  
 Friday 30 November 1917. 
 Everything quiet until "Jacko" started with  6 inch  5.9 inch Howitzer shells to shell the watering place of the British Yoemanry at 4 pm. 
 Shells went wide at first and landed short near Hospital tent. Cpl. & W.O. knocked over by concussion of shell. 
 Village stopped several, killing & wounding  natives  bedouin inhabitants. 
 Shelling ceased at dark. 
 On picquet at 6 pm. 