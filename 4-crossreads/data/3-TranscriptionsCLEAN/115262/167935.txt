  
 Sunday 17 June 1917. 
 Slept up on deck and got up at 5 a.m. and went for usual shower before 6 a.m. (reveille.) Went to church parade at 10 am, "Axie" & A.S.T. spoke & Capt Chap Lydgate gave a short but impressive address. It was very good. 
 Sports in afternoon. In the "O'Grady" drill  went  competion the C.B.F.A. team won. JRL. & myself being only ones left . Prize for team 3/15/-  
 Land on Port side sighted at 6 pm. in the shape of rugged mountains.  
 Several boats seen today. 