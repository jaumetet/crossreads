  
 Thursday 2 August 1917. 
 The camels again. After breakfast equipment fixed up for 4 hrs march into the Desert tomorrow. 
 During evening a party of us went to Heliopolis. Saw right through Luna Park for 5 P.T. & then went up through the principle streets. The buildings are beautiful & the latest type having been rebuilt during the last 10 yrs. Prior to this these buildings were some of the oldest in the world. 