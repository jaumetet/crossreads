  
 Sunday 9 September 1917. 
 Quartermasters fatigue today & various duties entailed by that fatigue. 
 "Again a little more time to myself"  A wonder.  
 Reported that the Taube that came over the other day was brought down near the Waddy. 
 Another Taube over again this morng. Anti-aircraft guns gave him a hot time. As usual the British planes appeared when he was gone. 
 Bombardment on the line today. 
 Water issue 2  pints  bottles for washing drinking etc. Total allowed by G.R.O. 2 pts. per man per day. This water comes right from Kantara. Is pumped from there. 
 Geo. Kelley at last came actoss me after being baffled so many times. 