  
 Monday 10 September 1917. 
 Routine as usual till 9 a.m. Cleaning, restuffing & fitting saddles all day & in the sun & dust with 11/4 hrs for lunch. A solid day again. Heard that another Australian mail landed at Suez 2 days ago. Everybody waiting for same. 