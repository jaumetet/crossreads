  
 Friday 1 June 1917. 
  "Crossed the line"  at 10 a.m. 
 Have developed another cold, & unluckily I am mess orderly again.  with  G.S.S.Cmrn on with me. 
 Rough weather - a big swell on. 
 Air still foul in No 3 deck. 
 Dinner 11.30 am (early) and preparations for "crossing the line" initiation at 2.30 pm 
 Great sport & everyone being drenched to the skin. No distinctions drawn between officers & men. Officers faring worst. 
 I had to go through the performance and have now "crossed the line" 
 Travelling 8 knots (slow) tonight 7.30 pm. 
 Very heavy seas. Boat shipping them very badly. WIth heavy head feeling crook, turned in early. 