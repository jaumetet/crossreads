  
 Thursday 14 June 1917. 
 A very hot morning and a good breeze. Slept on deck  last night  but on account of wind had to come below at 3 a.m. while crew removed awning. Dolphins seen again today. 
 Tonight we pass Perrim [Perim] & Appaur [Afar?] and enter the Red Sea. Slept below. 