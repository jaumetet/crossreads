  
 Thursday 4 October 1917. 
 Jacko never put in an appearance today until late. (As he comes every day now I've decided not to note every time he comes.) 
 Australian mail 6 th  since arriving in Egypt. Small mail which closed in Australia on July 19 th  week later that previous mail but took 7 weeks longer to reach here.  
 (One letter that I expected never arrived.)  
 Received 6 letters and packet of local papers. 