  
 Monday 8 October 1917. 
 Bringing up supplies from Rafa. Got a little bushed as night came on before we got in sight of Aerodrome on return. Struck an outpost & he put us on our track & supplies were got through safely. 
 On return delighted to find another Australian mail (direct) 8 letters & packet of local papers. 
 One of our battle planes brought down a Taube this morning. 
 Geo. S.S. Cameron back from Marakeb beach with Archie Scott. 