
and gunner on each rescued but both machines wrecked. News came throuh that "Konigsberg" was now a total wreck & on fire. 25 men & Capt. of her were killed, Monitors came out at 3.30 pm Sun 11th July 1915 with 4 men killed & 5 wounded.
 
Jaffa
Jaffa is one of the oldest known cities in the world. Pliny says it existed before the flood & Josephus attributes its origin to Phoenicians. It stands on a low rounded hill dipping into Med. Sea, & to east there stretches the broad Plain of Sharon as far as the hills of Samaria and Judea. Orange groves of Jaffa are famous. In peace time over 12,000,000 oranges were exported annually.
In normal times only one train a day left Jaffa for Jerusalem. Started 1 p.m & arrived at 4.50 p.m. Round about Lydd which was 
[Continued on page 31.]