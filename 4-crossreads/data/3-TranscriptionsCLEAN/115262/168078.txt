  
 Wednesday 7 November 1917. 
 No stand to. What a blessing. Turned out at 6.30 a.m. &  had a wash . (First one since over 60 hrs ago) Something to eat, saddle up & off again to the front line to relieve B Section. 
 This time I was in the group to relieve a B Sec. group with 4 th  Battn I.C.C. Bgde. 200 yds from our front ridge which is 400 yds from front line Turkish trenches. Continual rifle & machine gun fire overhead. Stray bullets everywhere. 3 pm "Jacko" started shelling our ridge. Dug in under cover of rocks & lay low when the shells whistled, & chanced our luck. 
 When night fell firing ceased with the exception of the sniping. Turned in at 6 pm wrapped in greatcoat & waterproof sheet. 
 [The following sentene struck through; part can be read:] [indecipherable] an hr's firing from 3 to 4  pm  am and turned in again to be awakened at dawn by the KK & S'pire Batty [indecipherable] 
 Rained a little at midnight. 
 Official announced that Gaza & El SherIan have fallen. 