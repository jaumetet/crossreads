  
 Thursday 30 August 1917. 
 Nothing out of the ordinary today except that bombardment on Gaza is still continuing with the same energy. This started at 1 a.m. this morning. This evening at 9.30 the shrapnel shells could be seen bursting over the trenches. "Very" lights were also plainly seen 
 Expect relief tomorrow. 