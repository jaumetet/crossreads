  
 Friday 16 March 1917. 
 In camp. 
 Parade called by C.O. troops at Camp Hd Qrs regarding w-e [week-end] leave for embarkation men. 
 Afternoon parade in full marching order. Inspection of universal & sea kit. 
 Paraded for week-end leave & were turned down. 