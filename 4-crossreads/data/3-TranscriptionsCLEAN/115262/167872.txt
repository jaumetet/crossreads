  
 Sunday 15 April 1917. 
 Up at 8.30 a.m. Caught 11.5 train from Flinders St to Dandenong. 
 Spent till 4.15 pm at "Elstre". 
 4.25 train to Lyndhurst. 
 6.10 pm train from Lyndhurst. 
 Till 8 train spent evening D'nong 
 Back to Melbourne by 8 train. 
 Arrived Burnley 9.45 pm. 
 An enjoyable week-end. 