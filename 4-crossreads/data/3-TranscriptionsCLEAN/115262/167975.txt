  
 Friday 27 July 1917. 
 6 am reveille. 
 Read out in orders that the action I took regarding the letter yesterday is contrary to censorship. 
 Notified that tomorrow we move out to Abbassia & the list of men to go read out on parade. I am included in original crowd. 
 Boys having a blow out tonight & the bulk of them merry celebrating the moving out to Abbassia. 