  
 Sunday 15 July 1917. 
 Church parade this morning fell out crook and at 1 p.m. leave given to go into Ismalia (as we understood.) 
 6 of us hired a "gharry" and were driven through the streets of Ismailia. It was very interesting to us as everything was quite new to our  mi   nds  ideas.  
  Coming back we were stopped by guard on the bridge entering Ismailia on the Tel-el-Kebir - Nafisha Rd. Marched back to Isolation Area under an armed guard with bayonets fixed. 1 guard to 2 men. Classed ( by officers ) as prisoners & placed under open arrest.  
  Turned in early feeling crook as I was when I fell out on church parade. Leave was 2 p.m. till 5.30 pm. "Gaggat" & the "Boy" were clinked in Ismailia.   