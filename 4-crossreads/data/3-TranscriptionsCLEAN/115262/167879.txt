  
 Sunday 22 April 1917. 
 In camp & on fatigue duty. On A.M.C. QM's fatigue. 
 A little spare time in afternoon.  
 Went down to service at the lines in evening. 
 Wrote 2 letters in afternoon & did some mending & sewing. Soon got tired & assisted the Mess orderlies. 
 More washing done 