  
 Wednesday 31 October 1917. 
 2 am found the Imperial Cml Brigade and other Brigades of Light Horse, Batteries etc. 21/2 miles from Turkish lines in the hills defending Beersheebah. A little firing going on but not much at midnight. 
 Bombardment commenced at dawn and is going the whole time. On each side of our collecting station are batteries of our big guns, which keep the shelling going. 
 Camels & all our  surplus  packs have been taken away behind the hills 3 miles for protection from shell fire.  
 24 hrs rations in our haversack is very hard to live on in the sun with no protection from it. 
 Wounded arrive some badly knocked about. Watched the shelling of 
 [Entry Continues on previous page.] 