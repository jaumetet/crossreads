  
 Sun Nov 5th - 16 Last night a Zepp was over and dropped a fair number of bombs causing much damage including a French ammunition dump where thousands of rounds were exploded. I happened to be on duty from 6PM to 8AM. and at one time 3 bombs dropped together shook the place to some order. 
 Wed Nov 8th - 16 All along our left front for about 3 miles Fritz bombarded our trenches very heavily and then tried to advance but failed.  The Fritzs got as far as No man's land and were stuck knee deep in mud and our boys opened the machine guns 
