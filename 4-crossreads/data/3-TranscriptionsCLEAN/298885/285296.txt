  
 very keen on dancing, even when dancing around their own kitchens with a little music, they are in their elements 
 Consequently it can be well imagined how much they enjoyed a good hall, a floor in proper condition and a good orchestra 
 This dance was instigated for the benefit of the civilians and it did not need two eyes to see how much they enjoyed it 
 The evening came to a close at 12.30 and by 1AM the last civilian had left. 
