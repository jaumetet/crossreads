  
 bed at 10.30 I was told to report to the Battery at once and after an eventful ride along the dark roads and through scrub arrived there at 4AM. It was "NO, Bon" 
 Mon Sep 4th - 16 Today there has been a continual fall of 5.9 all around us, the roof of our telephone dugout is covered with clods of earth showered on as each shell hits the ground. 
 Tues Sep 5th - 16 3 spooks and myself came forward to OP in front line we are here for a week. and do not feel too comfortable as the front line is mined for several hundred 
