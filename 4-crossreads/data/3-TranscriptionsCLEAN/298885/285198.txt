
the biggest shop in the town and wounded several civilians and 3 Aussies
Mar 11th - 18Moved up into action again per boot a distance of 10 kilometres to Sherpenberg
Here our camp is rather a novelty.  Instead of level ground we occupy the side of a steep hill and our huts hold a prominent position. Being at such an altitude we are favoured with a splendid view of the surrounding country to say nothing of the fascinating sight presented by the flashing of guns and bursting of
