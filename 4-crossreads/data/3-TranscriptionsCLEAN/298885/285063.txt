  
 Shells landed on next billet to ours about 400 yards away 
 Thur Apr 27th - 16 Our lads caught a spy - (of which there are several) a woman cutting the telephone wires.  Reported infantry attack tonight 
 Fri Apr 28th - 16 At 11PM last night we had our first experience of a gas attack. What with everyone excited and rifles, mortars and guns firing, there was something doing. 
 At 12.30AM all quiet again 
 Sat Apr 29th - 16 Our boys caught 2 more spies, both farmers, one 
