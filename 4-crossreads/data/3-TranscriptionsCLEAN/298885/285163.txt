  
 on account of mud etc 
 In most places our guns were up to the axle in mud and the gunners up to their waists. 
 Aug 7th - 17 As the weather is still wettish, things are quiet except for an occasional counter attack of Fritz's in all of which he has failed. 
 Aug 22nd - 17 For the last two weeks the weather has been very unsettled and our front has been held up by a very strong resistance by Fritz but we are pushing forward steadily 
 As for aireal activity it is 
