  
 the week-end. 
 Easter Mon. Apr. 24th -16 German guns firing very heavy and set fire to a cottage & causing a quantity of ammunition to explode.  Also air craft are taking advantage of first class weather and both sides having dinkum duels all day. 
 Sniper missed me again. 
 Tue. Apr. 25th -16 Relieved from this post and proceeded back to battery and came straight on duty for 24 hours.  Anniversary of Gallipoli Landing. 
 Wed. Apr. 26th -16 Enemy's guns very active 
