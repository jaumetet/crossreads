  
 weather and are laying their eggs in such a manner over Fritz to make one think that a laying competition was in progress for a limited time only 
 Mar 29th - 18 Owing to Fritz's continuous energy in the vicinity of the Somme we have been ordered to prepare to depart at the shortest notice for the Somme areas. 
 Mar 30th - 18 Orders cancelled on account of report of Fritz massing for attack on our present front. 
 Apr 4th - 18 During the last few days all has been comparatively 
