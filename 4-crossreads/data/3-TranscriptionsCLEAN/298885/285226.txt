  
 roof the result of No4 just missing the barn where all the boys were laying (not sleeping) 
 Fortunately no one had been hit and after a few had landed without doing any damage except to blow in the Officer's Mess the excitement abated and the shells passed on to the villiage which has and is being shelled every night. 
 May 30th - 18 This morning we were told to shift camp again so we moved further along the road to the next farm 
 Here we are comfortable and a little more protection 
