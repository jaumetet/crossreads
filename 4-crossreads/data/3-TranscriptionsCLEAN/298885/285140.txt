  
 some perfect picturesque scenery. 
 Anyhow we arrived at Bavilincourt about 8PM. with the sun still high in the sky, as at this time of the year it is nearly 9PM before it disappears below the horizon. 
 This place we are billeted in is about the best we have had since being in France. 
 It is a fine Chateau in the heart of Bavilincourt and constructed with bricks and mortar which is certainly not the case with the majority of buildings in these villages. They are mostly 
