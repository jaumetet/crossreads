  
 with any importance now as cancellation can happen in 5 minutes with the army. 
 May 2nd - 19 Word has come through that all 1915 men are to proceed to London tomorrow to take part in a march of 12000 Colonial troops through the city. 
 Some two hours later we were told that we would also move to another Camp in 3 days time to join our earlier Quota (16th) and will leave Devonport on 12th Inst. On troopship "Soudan" 
