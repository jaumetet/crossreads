  
 21 August Have breakfast with the New Zealanders.  We march up behind the firing line on the left.  Expecting a big advance against Hill 971.  Pte Vanstone.  Hit Shrapnel.  Well, we are preparing for a bayonet charge. 
 22 August "18"th in action lose 600 men.  "C" & "D" Coys. sent into the firing line to relieve 13 Batt.  "17"th in a gully in rear of the 16th. 
 23 August See Pte Styles of 13 Batt.  Am on guard.  A & B Coys of 17 ordered into the firing line with the 13th. 
 24 August We go into the trenches of the 13th Batt.  I have dinner with 3 of my old mates of the 5th of the 13th.  See Brierly & Clark. 
 25 August Am on well fatigue with Harman in the valley behind our firing line.  Our duty is to draw water out of a 40 ft. well for the 17th Batt. 
 26 August Harman and I get the water supply job for good.  See Pte Lewis & Hickson. 
 27 August Harman & I brave the shapnel & go down to the beach for a swim.  Big advance on the left by the 13th, 15th, 17th & 18th Batts.  4 Platoons of "A" 17th held in reserve.  Lieut Pye & Corp. Wolf wounded.  Lieut Gombert & Serg. Robertson killed.  17th take 2 trenches.  Brierly & Clark wounded. 
 28 August No. 4 in the trenches.  Some of the men in the charge turn up for rations.  Lance Corp. Bennet give us a vivid account of the charge. 
 29 August Taube flies over and drops 3 bombs misses us by a 1/2 mile.  The Turks drop plenty of shrapnel round us, some of it comes within 3 yds. of us.  Harman & I go into the trenches at 7 PM. 
 30 August Come out of the trenches at 7 AM.  Sgt. Chisolm wounded.  1 Platoon of the Essex wiped out by shrapnel in Reserve Gully. 