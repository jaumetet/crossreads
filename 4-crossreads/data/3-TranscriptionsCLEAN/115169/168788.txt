
by noon
Feb 13th [1915]  at 8 A.M. we arrived at Kingston once again and secured to wharf, S.S. Ridley a collier came alongside at 9 A.M.
Feb 14th at 8 AM natives commenced to coal us and coaling completed by 6 P.M. We remained at Kingston for a few days giving leave and cleaning ship, and on
Feb 22nd at 9 AM we left Kingston for Havana
Feb 23rd at 7.30 A.M. we arrived and dropped anchor on the lee side of Coy Sel Island, which was a small flat uninhabited island and at 9 AM S.S. Eastwood, collier came alongside of us, and we commenced to coal finishing at 6 P.M. after which both of us left for Havana.
Feb 24th at 9 A.M. we arrived abreast of Havana to relieve "Conde" on patrol duties, -our Captain paid a visit to "Conde", after which at 11 AM the latter left for Kingston & Martinique - at 12.30 PM French S.S. Sumarra left Havana and when abreast of us she stopped, - our whaler was sent across to her and our boarding officer
[Contd. Page 98]
