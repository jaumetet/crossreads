  
 Feb 5th at 6 A.M. we arrived off Havana and relieved H.M.S. Berwick on patrol duties the latter leaving for Jamaica at 7 A.M. 
 Feb 7th 6 medical cases were reported on board supposed to be climatic bubo, tropical diseases 
 Feb 9th French cruiser Conde arrived at 11 AM. to relieve us, rather unexpectedly after only a few days away from Jamaica, but we were to proceed this time to Gulf of Honduras in search of Karlsruhe's attendents and so we took our departure in fine weather at 12.30 P.M. for Bay Island British Honduras. 
 Feb 11th at 6 A.M we arrived at Bay Island and at once commenced a vigilant search f all islands in the vicinity, which were all more or less comparatively small, but no trace of anything was observed, and so at 4 PM we shaped course for Kingston 
 Feb 12th at 9 A.M. commenced to work up for full power trial, - weather was most favourable at this time, but when we were fully prepared the weather had become very inclement so much so that the trial was finally abandoned 
