   
   
   
 7-654 C 
 ILES, G  H      ML MSS. 1988 
   
 "My Reminiscensies of Four and a Quarter Years Of War", 3 Aug. 1914- 26 May 1919 
   
 1v. 
 Chief Petty Officer G.H. Iles served on H.M.A.S. Melbourne throughout the war 
   
 Contents 
 Describes the movements of the Melbourne, i.a. the capture of Samoa, Nauru and German New Guinea, Aug.- Sep. 1914, convoying Australian troopships to Port Said, Sep.- Nov. 1914, the sinking of the Emden by the Sydney, Nov. 1914, patrolling off North America and West Indies, Nov. 1914- Aug. 1916, patrolling and convoying in the North Atlantic Aug. 1916- Nov. 1918, the surrender of the German Fleet, Nov. 1918, and the return to Australia, 25 Feb.- 26 May 1919. 
   
