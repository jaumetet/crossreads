  
 whilst every officer and man, followed our late shipmate to his last resting place headed by our Ship's Band, - Even the day was dull as if lamenting the departure of our late officer, and he was buried very close to the beach, whilst Admiral Patey & Staff and our Captain and officers surrounded the grave and saw the last resting place of his body, - then the 3 volleys, and last post closed the ceremony. 
 June 8th [1915]  we were lowered and refloated, then taken alongside the wharf again, and where we remained until 
 June 14th  when we replenished bunkers, 837 tons 
 June 22nd  at noon H.M.S. "Caesar" arrived and dropped anchor outside harbour. 
 June 23rd  at 9.30 am. we departed for Martinique a French West Indies island, - weather fine, and it was a most pleasant voyage south 
 June 26th  at 2.30 PM we arrived at Martinique and proceeded alongside wharf,- the approaches to this place was most interesting and historic, for on our port side we passed Mount 
