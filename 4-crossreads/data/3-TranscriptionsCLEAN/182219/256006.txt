
from the beach, scores of surf boats were plying to & fro, it was a scene of indescribable bustle & noise. Thousands of natives were there unloading the boats & packing the camels or stowing the cargo in great dumps, it is a great boom that we have the ocean so close as the line is held up & communication with supply sources would otherwise be severed returned across the desert these being a wonderful event of which Capt H - took a number of color plates pictures.
28 Dec
A limber from H'dqrs
