  
 bivouac & Johnson saved the situation by discovering a limestone cave into which we moved & as we had to cook our meals in the same cave we were nearly smoked out for the time being but better the smoke that a wet shirt so we thought. 
 Jan 2nd 1918 
 The camel Tport arrived at camp at 7.30 am & all our gear was packed up for a move. The roads are quagmires & extremely bad for camels, their padded feet slipping & sliding so that with the regulation load of 300 lbs on the poor beasts are absolutely helpless. Scarcely better off are their native drivers, these destitute beings have little or no shelter during the rains with the 
