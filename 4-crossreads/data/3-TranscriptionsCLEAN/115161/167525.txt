  
  A BIT ABOUT MYSELF  
 My name I need not mention, let it suffice that my friends call me Gill, while I am known as Gillie by some of my friends of the sweeter sex. I am not related to the makers of the famous razor-blades, bearing a name similar to my own. 
 I enlisted in March 1916. Left Australia for the front in September of the same year. Returned to Australia in July 1920. 
 I went as a private, returned as a sergeant. 
 I went there thin, returned fat, very fat. 
 I went there short of stature, returned no taller. 
 My chief attainment is complaining (or growling), and I place myself second to none in this respect. I claim to have brought growling to a fine art. 
 The only privilege granted to us in the A.I.F. was to growl, and I didn't let an opportunity escape me. I growled when in camp, to get on the transport. I growled on the transport, to get to England. I growled in England to get to France. I growled in France because I was there. I growled while on leave, because I had to go back to France.  I growl now that I am home again, because (I suppose) I have not to go back. I shall growl I expect to my dying day. Such a habit has growling become that I fear I should growl if I had everything I wanted. Growling has fairly got into my blood. I remember on one occassion being referred to by a big fellow as "that little pup", so I suppose that epithet was used on account of my continuous growling, (or perhaps his lady-love had made a good "lot" of me.) Ah, well! 
 I am only just a dark very undersized, insignificant, sober-minded being, of a most selfish disposition, and as the case with small men, conceited and quarrelsome, and mean to a degree, 