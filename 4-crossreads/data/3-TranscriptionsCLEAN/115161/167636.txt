  
 in the ruins of the sugar refinery. 
 While cleaning my equipment I noticed that my gas mask was punctured in several places, and many bullet marks were visible in my tunic and breeches.  I was glad that after all I was a small man. 
 We wondered when we should be moving further "back" for our anticipated rest.  We were soon to know, when we were to move and in which direction. 
 CHAPTER XXV1. 
  A DISAPPOINTMENT.  
 As before stated we felt sure we were moving away from the front for a spell.  Of course we don't.  The authorities apparently imagine that we revel in this kind of sport.  However we keep on smiling, we may, some of us get a longer rest than we expect before long.  Anyway what's the use of worrying?  We are content so long as only mud and our good luck sticks to us. 
 After remaining in this village for two days, we are ordered to prepare to go forward again.  We wonder what has happened.  Had the enemy retaken the position which had cost us so dearly to gain? and had our relief been driven out?  No! this could not be. 
 Well! we all march off again observing the same rules as before leaving the ruined village about 9 p.m. and after a few "spells" arrive at a new position, adjoining a railway line, soon after midnight.  Our company was escorted to a fine "cutting" in the hill, with very high banks on either side, and good dug-outs which had lately been occupied by the enemy.  We congratulated ourselves on our good fortune.  Naturally we spoke too soon, for after we had been there for a few minutes only it was discovered that we were in the wrong place.  We moved further along to a spot on the railway line which was all flat country. 
 Here we have to make our own dug-outs, and not much time is wasted as the enemy occupies the hill a very short distance in our front, and we must get out of sight before daybreak. 
 A corporal is detailed to pick two men to accompany him to a listen- 
   