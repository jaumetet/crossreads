  
 shell holes all linked together. Roads have ceased to exist, and many teams and guns are bogged on what was once a road. I washed the wounds of the mules ready for the vet  to dress, and that is all I have done this morning for I was up very late, & there was plenty of others to clean up. 
 In the pack stunt last night we lost 2 mules and 1 horse and early this night Pat. O'Neil lost two draught horses. They got away from him in the mud while he was on a divisional stunt. 
 Saturday Oct. 13. 1917 (1167.) 
 There was a drizling rain last evening and night. There is a sea of mud all around the huts and through the horses lines. We have a long walk through mud, water & shell holes to the cook house 3 times a day for our meals. It is very annoying. There was the usual bombardment last night. Fritz put many shells near the camp early this morning. There are dozen of Transports camps adjoining us, our own Brigade and artillery Transports. 
 I received a letter from my wife this morning No 82. dated Aug 1. She complains of not receiving any letters for 5 weeks, owing to a mail boat being sunk. I hope she is able to tell me she has got some in her next. 
 To day the war has been going on for 1167 days  or 42 lunar months, including leap years.  
