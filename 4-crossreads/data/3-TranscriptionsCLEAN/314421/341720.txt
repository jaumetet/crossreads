
their boots. I put on my trousers but the boots looked too wet & cold and so I crawled back into the blankets, so did Darkie. At Intervals he kept up the shelling all night until morning. About 1 O'Clock all hands were wakened up to get ready for a shift as the Battalions were moving out. All the pack mules and Cookers with the Mess cart moved out at 4. a.m.
We are to have breakfast at 6 a.m. I have packed up my things and wrote up my diary while waiting.
Bill Mitchell, of A Company, was one who got up & went out, and has not yet returned.
We got away about 8. a.m. There was a lot of packing up to be done, and some things had to be left behind, for there was no room for them in the Limbres. When we were going out the enemy was occasionally putting in a few shells & later on we heard that an officer in the 40th. Batt. was killed.
On the road we passed through Ypres and went on to the Railway station at Flamertingbe where we were  to train for our destination, but after waiting for several hours during which we had dinner we found that there was no train available and we would have to take the road all the way.
This is a very busy military station & the trains are all
