  
 French barbers shop while waiting for a shave. I also obtained some P.C. of the town. 
 Wednesday Oct. 17. 1917. (1171.) 
 Revellie 6.45. 
 We groomed fed & watered our animals the first thing and then had a good breakfast of stew made out of fresh meat, & bread & jam & butter. 
 I noticed that one of the streets is named Rue Miss Cavell, in honour of Nurse Cavell who was murdered by the Germans early in the war. 
 A great string of boats is plying on the canal this morning. The river Lys runs through the town. We are to start at 9.30. 
 Leaving Arques we crossed the canal, along the Rue Miss Cavelle, and passed out of the town, turned the corner at the brickfields, and headed for Blequin along the same road that we travelled last August on our first visit. At the Brickfields we had a good view of St. Omer which is only 3 k. away. The road is hilly and is lined with houses most of the way, in fact it is a string of Villages the chief of which are Blendicques, Wizernes, Lumbres, where we halted for dinner. Here I got some more P.C. There is a large brewery here. The next village is Nielles Lez Blequin, where there is a flour mill worked by a water wheel. 
