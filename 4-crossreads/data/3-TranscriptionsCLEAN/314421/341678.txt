  
 of rest and "nothing doing" to the casual observer, but in reality the period of rest is a period of great preperation. 
 There was another air raid at various points in Suffolk, Essex, and Kent. The majority of the raiders did not venture far inland, a few headed towards London, but failed to reach the metropolis. Casualties are not yet reported. 
 We were issued with matches tobacco & cigarettes today. Also with 2 green envelopes, the first for a long time. 
 We are to move from this camp to morrow morning, also the Battalion. 
 Monday October 1. 1917. (1155.) 
 Another month has gone by and the war still goes on but the initiative is still with us all along the line, and the prospect of the Allies being victorious this year is very bright. The enemy is on the defensive on all the fronts, even the Russian fronts, and Turkey & Austria-Hungary are in a bad way, while there are political & social discords in Germany. 
 I am over 10 months in France and on the 3rd of this month will be 16 months since I left Victoria and am over 19 months in the army. 
