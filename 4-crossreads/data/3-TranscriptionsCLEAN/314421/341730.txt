  
 Friday October 19. 1917. (1173.) 
 Dull & cloudy, with light misty rain. 
 I paraded sick this morning, because my feet have been sore for about a week, through being continually in the mud at Ypres & have broken out at the heels. I was given light duties, and had them dressed with Iodine. 
 [Shorthand transcribed as follows] I wrote a letter to my wife today, letter 96, 5 pages.  I sent it in the official green envelope.  I included a German post card taken from a German prisoner. 
 I also wrote to Cis tonight, in answer to her letter of August 7, No. 5.  [indecipherable]. 
 We were paid later tonight.  I drew 20 Francs, 14.8.  [End of shorthand] Saturday October 20. 1917. (1174.) 
 It is cold & frosty this morning. Between 9 & 10 O'clock last night we were startled by the explosion of bursting bombs. There were 6 or 7 explosions in all. They were dropped by enemy planes and  were  judging by the sounds, fell about a mile from this village. 
 [Shorthand transcribed as follows.] I sent silk post cards to the following today. L. Douglas, WGamble, W.Curtis, Miss K.E. Miller, Miss Alice Miller, Robert Wilson, Eric Wilson Maggie Kittson, and also to my wife & sister, 10 in all. [End of shorthand.] [Shorthand transcribed as follows.] In the forenoon I went out and got some more French post cards and sent them to the following. [End of shorthand.] My Mother Irene Ring. My Uncle. J.Gilmore, Kitty Gilmore, & Kitty Martin, making a total of 15 silk Postcards in all, cost 7 1/2  francs. 
 Sunday Oct. 21. 1917 (1175.) 
 Frosty last night. A fine day. There was a half holiday yesterday and again this evening. 
