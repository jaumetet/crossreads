  
 few more snaps & etc, after this she insisted upon me going home with her for tea which I finally did & had a good time there for they are nice people, I was supposed to be in by 7 but I never left her home till 7.15 how the people all stared for I was the only one in blue to be seen & they knew I was a long way over my pass, I did not get in till 8.20 & I don't think anyone spotted me. 
 6th. Rilen went away this morning to go on leave so I am on my own now, he was not a bad sort & we got on well together. 
 My remittance from Mr Duke 10 came also I asked for 7 but he thought he would be on the safe side I suppose & sent more along he's a fine sort sure enough. 
