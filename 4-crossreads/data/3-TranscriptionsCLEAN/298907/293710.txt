  
 28th. Had a very funny dream this morning about Grandma wonder if anything has happened to her. 
 This afternoon Farrell & I went up town & got my prints from "Bayntons" they are very good, we then paid my usual call at my flash little cafe, for our cup of tea, I am well known in here now, & welcomed. 
 During the afternoon 3 funny little incidents occurred, first was my discovery of a "Barwick Street", next got into a tram with a Belgian Colonel who was wearing the same ribbon as mine only he had his medal also, as is usual with them, then in the same tram a little further on a girl got in who I had been looking for since the first few days I had been in this city. 
 Farrell & I spent a couple of hours boating on the reservoir it was lovely & no mistake. 
 There was a big row in our Ward this morning, through the lot of 
