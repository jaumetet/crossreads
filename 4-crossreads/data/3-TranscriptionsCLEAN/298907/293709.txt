  
 Sister M&ouml;ller & Cottons book, they are both very old books & they say they only let a privileged few write in them, I am the only "Aussie" who has ever entered his name for I looked through them just to see, so I'm sure I ought to feel honored, I had quite a long yarn with Sister Cotton last night, she is a fine woman. 
 My character writing is also on the boom again, for I am being flooded out by the nurses & sisters who want theirs Sister Cotton reckons that what she has seen of my opinions is simply wonderful, for she says she knows all the nurses well, she reckons it wonderful how I have hit them off 
 Took Doherty down with me to a friends place  yest  this afternoon he is a good chap & we had a good time. 
