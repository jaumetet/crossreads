  
 glorious evening the clouds rolled away & the sun came out nice & bright making the whole land look just like one huge flower & vegetable garden & the trees looked as green as could be for all their leaves were washed clean of the dust, & everything smelt so lovely. 
 I dropped into the Daimler tea rooms & here I met Doherty & McGuire so we then had a run round town I was on the look out for films but never a one could I get they are as precious as gold & just as scarce. 
 8th. Into the city again this morning searching for films but not a one could I get for love nor money & they reckon they wont have any in till Wednesday. 
