  
 Arrived at Suez at 5.30 p.m., and were taken on board the Hospital ship "Kanowna" (A.U.S.N. line, a sister ship to the Kyarra). 
 20th Left Suez at 7 a.m., passing on our way out the Canadian Hospital ship Loyalty lying at anchor.  At 9.30 a.m. met a French passenger steamer. 
 21st Burial at sea of a patient who died of brain fever. 
 22nd Fearfully hot, right out in the Red Sea.  The ship is vibrating like a milk shake. 
 23rd Still very hot.  Thousands of porpoises chasing shoals of flying fish all round the ship. 
 24th Sunday - Passed by Aden at 8 p.m., and met an Indian Hospital ship on her way back to the Dardanelles from India. Out of the Red Sea now, thank goodness. 
 25th Met transport Argyllshire with Australian troops on board.  They passed very close to us, & gave us a great cheering.  They 
