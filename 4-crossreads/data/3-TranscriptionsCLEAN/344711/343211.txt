  
 that generally only takes 20 or 30 minutes.  However, we got safely on board, and I was sent to bed. 
 5th M.'s birthday.  Many happy returns.  Still anchored off Anzac, and a lot more sick and wounded came on board.  Some also put off "Salta" on trawlers, sent to Imbros. 
 6th Another Hospital ship came in through the night.  Two men died through the night, and were taken off this morning by the mine-sweeper "Loch Broom", and taken  up  out to sea for burial, both being covered with the Union Jack.  Left at 12 a.m. for Imbros, & left Imbros 3 p.m. for Mudros, arriving there 7 p.m. 
 7th Left Mudros at 9 a.m. for Alexandria.  Saw S.S. "Aquitania" sister ship of the "Lusitania", with four funnels, in Mudros Harbour. 
 9th Arrived Alexandria 9 a.m.  Harbor full of shipping.  Left at 2 p.m. for Cairo, in Red Cross train.  Had a fast trip, arriving Cairo 5.30 p.m.  Taken in Red Cross Ambulances to Gezireh Palace Hospital.  This is a lovely place 
