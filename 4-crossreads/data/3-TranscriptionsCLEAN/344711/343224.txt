
24thPassed Gabo Island at 6 a.m.  A Queenslander died today, and was buried at sea.  We all came up on deck for the burial service.  Passed Kiama at 9 p.m.  Bush fires all along the coast.
25thArrived Sydney Heads at 3 a.m. and anchored in Watson's Bay.  At 9 a.m. we went up the Harbour to No. 1 Wharf, Woolloomooloo Bay.  Were taken in motors to Randwick Hospital & had a good reception all the way.  Had dinner and got leave for 24 hours.  Went up to Riverstone.
30thReceived my pass just in time to catch the Moree mail.
January 11th 1916Received my discharge as medically unfit.
Sea of Marmara - misspelt as Sea of Marmora - P. 4Kasr-el-Nil - sometimes spelt Qasr-el-Nil - P. 9Gizeh - also spelt Giza - P. 11Babel-Hadid - also spelt Bab el Hadid - P. 25
[Transcribed by Judy Gimbert and Grahame Bickford for the State Library of New South Wales]
