  
 September, 1914 16th Joined 6th Australian Light Horse at Rosebery Park. 
 22nd Transferred to Rosehill. 
 29th Transferred to Liverpool. 
 October and November spent in horse and rifle drilling & instruction. 
 December 19th Left Liverpool at 8 p.m., riding all night, and arrived Sydney 7 a.m. in the morning.  Loaded horses and gear on Transport A29, "S.S. Suevic" and anchored out in harbor. 
 21st Left dear old Sydney 2 p.m.  Sea very rough coming through the Heads. 
 24th Xmas Eve A concert on board.  Passed through Bass Strait into Great Australian Bight.  Went on guard at 6 p.m. 
 25th Xmas Day Still on guard.  Tough chicken for dinner. 
 26th Orders issued that all letters that contained any 
