
Arrived Bab-el-Louk 9 p.m., formed up and our squadron led the march through the town, to the sounds of cheering, coo-ees, & all sorts of noises.  The residents turned out, clapping and waving hats and handkerchiefs.  We marched to Bab-el-Hadid, Cairo's railway station, entrained there, and left for Alexandria at 12 p.m.
16thArrived Alexandria 6.30 a.m.  At 9 a.m. went on board transport A25, formerly the German steamer Lutzow, but now a prize.  On guard on the gangway at 11 a.m.  At 5 p.m. we left Alexandria, and heading out over the Mediterranean we began our last journey to the Front.  Had bully beef and biscuits for tea, and went to sleep in a lifeboat out of which I was emptied by the lying-in-picquet at 12 p.m.
17thHad nothing to do all day.  Sea very calm.  Sighted a few transports bound for Alexandria.
18thHeld a voluntary church service in the morning & Colonel Ryrie made a little speech.
