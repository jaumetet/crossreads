  
 Decembre 12 Getting ready today to move out to Rest Billets tomorrow. 2nd F Coy taking over our work on the Messines Front. Things very quiet on this Front. 
 Decembre 13 Entrained at De Hanebeek to go out to rest billets near Boulogue. 14th Bde all on the move. 