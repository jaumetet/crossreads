  
 Aout 14 Had a short march in the morning & had a lecture by D.I. Lecturer on the new gas the Germans are using. Nothing else of importance. 
 Aout 15 Had Coy route march today - distance about 14 miles. Got back about 3p.m. every body feeling very tired. 