  
 February 7th: Returned to Heliopolis at 7 p.m.  Issued with bread and sardines. 
 February 8th: Lecture on Visual training. 
 February 11th: Commence preliminary musketry. 
 February 12th: Shoot at Miniature Range. 
 February 13th: Issued with Lady McQuarrie Patriotic Society's shirt and sox. 
 February 14th: Proceed to Tel-el-Kebir.  Rise at 3.30 and march to Helma Station and there entrain in open trucks.  Arrive at Tel-el-Kebir about noon, and there join our battalion.  Meet Sergeant Butt'worth who belongs to 30th Battalion.  Notable features of Tel-el-Kebir - miles of old trenches left by Arabi Pascha, who built them and used same against the British.  The English and Scotch lost three hundred and forty lives.  The Scotch smashed the Araboup with a bayonet charge.  We drilled and paraded over the ground that was once a battlefield.  As usual the Y.M.C.A. are here with a structure for our comfort. 