  
 5.7.18 
 Dear Mum, This is just a P.C. as the Lights have blown out & it is nearly dark. I expect to go on Leave to see Aunt Annie on Tuesday or Wednesday so I'll drop you a line on the way when I've arrived there. Tommy Williams (Dad will remember our Little Pay Clerk) & I rode about 20 miles on our Bikes To-day. It is pouring with rain now. I'll have some supper & go to bed real early tonight. Ellis is away on leave at a Beautiful Place called Bournemouth. He returns on Tuesday. 
 Love to all. Letters following.  Harry 