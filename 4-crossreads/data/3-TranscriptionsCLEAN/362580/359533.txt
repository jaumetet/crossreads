  
 to Australia again on transport.  She has been expecting to leave for over a week now but on Saturday she was still waiting for her boat. 
 Two of our Sisters were spending the day in Cairo and saw her.  Quite a number have gone & are going now, so one never knows what will happen to yours truly. 
 I saw George Bower on Friday for a few minutes & he looked so brown & well.  He is a Corporal now and his Officer told me he was a good fellow & is sure to get on well. 
 I must write a few lines to dear old Alice, she always writes me such nice letters and gets up early in the morning to do so.  I dont think there is any more news this week.  Hope to receive an Australian mail to-morrow.  It is after 3 a.m. & I'm feeling very sleepy so must walk about.  Fondest love & kisses to you all & hope you are all well. 
 From your loving sister, Edith. 