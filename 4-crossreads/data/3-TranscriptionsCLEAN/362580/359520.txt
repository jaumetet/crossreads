  
 The mittens you sent I gave to Norman last night.  Poor old Keith may be on his way to Australia now.  Hope he is much better.  I received a letter from his mother & Jean to-day - poor woman, she will be upset when she sees her dear boy without a limb. 
 Vi must have been very ill too - I had a letter from Mrs & Mr Osburne yesterday & Vi was staying with them.  I cannot find out definitely where poor old Fred is, so unable to write or send anything to him. 
 Little Eddy's letter is lovely & the pansy arrived quite safely.  He is a busy boy, having a garden of his own.  Hope he had a joyful time for his birthday. 
 I did not buy a Panama but got a little  Grey Felt  & everyone but Dorothy says it suits me & like it very much. 
 I did receive the card the  family  sent so I  trust  the family receive & like my photo I sent with Sister Hart (of Queensland) 