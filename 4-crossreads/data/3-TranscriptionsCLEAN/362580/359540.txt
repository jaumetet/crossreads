
a good old sort.
We finished long before them - had another stroll & then returned to duty at 9 p.m.  Feeling very happy & contented.  He stayed all night but left early next morn.  Most likely I shall see him for a few moments on Tel-el-Kebir Station on our way to Cairo.
Am getting another cotton crepe dress, it is fearfully & hard to find dressmakers.