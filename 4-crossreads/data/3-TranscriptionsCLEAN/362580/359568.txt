  
 1st A.G.H. Heliopolis. Egypt Nov. 8th 1915. 
 My dear old Alice, 
 Just received your nice newsy letter No. 27.  Funny that it is the only one from Australia to me by this mail.  Suppose all my friends are too busy or disgusted that I have not been able to write oftener to them. 
 I just wrote a few days ago to you all so have not much news to tell this time. 
 Dorothy & I have been into Cairo this afternoon, we both are getting a gorey cotton crepe, made in the uniform style to wear in the street; our outdoor dresses are rather warm, especially as we have to wear a cape with it.  It will cost 1.  mind will be here to-morrow, hope it will be alright.  We wear panama hats with a chocolate band finished with a little dash of red ribbon.  I'm still wearing my old one with the band & had it cleaned.  It does not look too bad - Tried to get a new one to-day but was not successful.  We also got some of our money changed into gold, so we are both getting 10 or 15 in gold 