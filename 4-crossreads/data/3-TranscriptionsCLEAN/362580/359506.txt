  
 1st A.G.H. Heliopolis. Egyp.t Dec 14th 1915. 
 My dear Belle. 
 I have just received your letter No 32., so my luck has been in lately in the4 way of letters & parcels. 
 I'm glad to hear you are all well & that you are happy.  Hope you will have a cool summer & that mother will not feel it too much. Poor little Eddy is young to be troubled with toothache, hope he is alright now.  He is a man having a leather band in his hat.  How dreadful about poor old Edmund, he has bad luck - Hope you like the Photo I've sent you & that you get them safely.  I'm feeling very well & taking things very quietly.  Have no faithful male admirer at hand at present, so keeping very good hours.  Anyhow I shall always go very slowly while here now.  We have been very busy, 