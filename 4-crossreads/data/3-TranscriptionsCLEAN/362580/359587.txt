  
 married about two weeks ago & was told about 3 weeks afterwards that she had to go back to Australia & to leave the service.  So the couples will have to waite until after the war is over. 
 It is quite right, because we really belong to our country now. 
 Well, I suppose everyone makes great mistakes at some time in life.  I've made many, I know to my sorrow.  I do hope you are all well & not having too hot a summer. 
 If I get a chance I shall send home my brown costume & a few clothes that I cannot wear but that some of you may be able to make good use of.  I must try to find out the best way to send them. 
 Give my love to all my friends & tell them to write to me some times. 
 Fondest love & kisses to you all & trust that you are not feeling the hardships of war too much. 
 From, Your loving sister, Edith. 