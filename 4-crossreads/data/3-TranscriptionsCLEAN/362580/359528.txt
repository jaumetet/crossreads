  
 must try & do some shopping.  All my boots and shoes seem to be wearing out.  This is a very hard place on shoeleather.  The weather is beautiful but warming up, hope we shall be in a cooler place for the summer. 
 The last letter was Numbered 45, so no doubt 44 will turn up next week. 
 I also got a lovely long letter from Mrs & Mr Osburne. 
 One from Mollie & Jess, I was quite excited at getting them.  Have only had two mails in seven weeks & not one paper.  Dont be disappointed if my letters are not regular, I shall write every week, but all are censored now, so may be longer in travelling.  Have met quite a number of my old friends of last year & it has been ripping to meet again, especially after the anxious time it has been for us all. 
 Fred is quite well now & is working hard, there is a very busy time for all the men 