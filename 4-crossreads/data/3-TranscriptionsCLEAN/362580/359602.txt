  
 and Easter Sunday but am afraid there will be no opportunity of seeing Edie. 
 Edie asked me to visit her cousin Ronald who was an inmate at Reading Military Hospital.  I called but he had been dismissed from the hospital just 2 days before. 
 With Kindest regards to you all - hope all are well 
 Yours as ever - Edmund - 