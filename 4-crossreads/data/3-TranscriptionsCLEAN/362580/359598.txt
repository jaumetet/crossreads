  
 morning, returning to London by the first train on that day.  Edie is returning to France next Friday. 
 I shall be in Paris during Holy week and Easter Sunday.  All my expenses are to be paid - so it is alright for me.  Trust you and the family are keeping well.  No need to worry about Edie's health.  It has been a wonderful and a fine experience for her.  Worse luck I have to return soon - leaving here on May 15.  If I were single I would stop here for 3 years.  So Annie has lost her husband!  Means another job for me later on. 
 With kindest regards to you all - not forgetting  Eddie  
 Yours as ever Edmund. 