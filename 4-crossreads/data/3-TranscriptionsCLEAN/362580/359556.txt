  
 "Porch House" Torrington Devon 1-3-17 
 My dear Auntie Bella 
 Hope you'll forgive my not thanking you for your kindness in sending good wishes for Xmas which I received about a month ago.  Have been enjoying myself immensely reading several home letters & was jolly pleased to hear young Eddie was improving, poor kid must have had a rough time, please give him my love & best wishes.  Got a most cheerful note from Ede this afternoon saying she'd had a topping holiday up in Scotland 