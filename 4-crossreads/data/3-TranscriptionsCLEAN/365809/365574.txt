
Australian Imperial Force.Base Records Office.Victoria Barracks.Melbourne 8th October 1917
Dear Sir,In continuation of my Communication of the 9th April last, concerning 2 cameras and the keys of 2 trunks, the property of your son the late Captain A.G.Ferguson, 20th Battalion, I am now in receipt of the following report from Australian Imperial Force Headquarters, Cairo, which is forwarded for information.As soon as the keys come to hand they will be promptly transmitted :-
" I have to advise that the keys have been collected from Thos. Cook and sons, and will be forwarded to the next-of-kin with the next shipment. Exhaustive enquiries have failed to locate any trace of the cameras referred to above. The O.C. 20th Battalion, has been communicated with for any information he can supply, and immediately on receipt of a reply you will be further advised."
Yours faithfullyWHOsborne MajorOfficer i/c Base Records.
Mr. Justice D.G.FergusonSupreme CourtSydney N.S.W.
