  
 11/5/16 enemy aeroplane brought down in our lines 
 14/5/16 we were releaved from the firing line by the 8 Bt we went to Elbow farm in reserves to them 
 25/5/16 our billets shelled  no casualties 
 29/5/16 We took over our old part of the firing line 
 30/5/16 We shelled german firing line 