  
 [Newspaper: The Kangaroo: Out of his element. The representative newspaper of the Australian Imperial Expeditionary Force (1st Battalion), published on board the Troopship "Afric" - 1914; Volume 1, Novr. 17, 1914, No. 20.] 
 [Transcribed by Barbara Manchester for the State Library of New South Wales] 
 