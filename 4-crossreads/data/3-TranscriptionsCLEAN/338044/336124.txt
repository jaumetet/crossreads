  
 27th Nov:- Discharged from hospital to take up duties in the morning. Visit Pelham de Mestre in Aust. Int. Base, when I see W.O. Gillies &  arrange to start if possible. On leaving hospital am marked for light duties. 
 28th Nov:- No duties to-day &  we parade for instructions re duties. Manage to secure a tent to sleep for the night &  turn in early. 
 29th Nov:- Commence in office in hospital but only work for a few hours, when after parading to the O.C. of Hospital arrange for transfer to the Base &  spend the afternoon making preparations. 
 30th Nov:- Nothing to do & having no money left, stay in &  write letters 