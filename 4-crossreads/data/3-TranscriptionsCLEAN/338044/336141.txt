  
 1916 
 3rd Jan. Monday It is now as cold as ever I wish & the dreary long night duty is most miserable. We are wrapped in Greatcoats & mufflers all night long, but still feel cold & also sleepy, for it is impossible to obtain a good sleep during the day, as we should then have to go without our meals.  Sleep from 4 to 4.30 am; (our 1/2 hour off) 
 4th to 7th January:- Nothing out of the usual monotonous routine of duty &  sleep occurred during the week  & no prospect of any removal from this miserable night duty. I never wish again to hear opinions about Egypt's not being cold 