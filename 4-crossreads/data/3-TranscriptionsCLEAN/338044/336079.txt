  
 1915 
 29 Sept 1915: Last night at Liverpool Camp. The scene is a lasting one. Mothers, relations, wives, &  friends are here in hundreds. Scenes of last farewells can hardly be described. Special guard on the bridge to Prevent the Reinforcements who are leaving to-morrow from going to town  Huge bonfires everywhere. Rioting Prevails. The canteens are sacked and cookhouses destroyed. No sleep for most of the "boys". 
 At the gate numbers are disappointed because the soldiers are not allowed to the station for a last good-bye. 
 Farewell concert in A.M.C. tent to Reinforcements 