  
 although the watches keep correct. Of course this is owing to the change of latitude the ship's time being changed daily  Our  Pet mascot, the kangaroo is very seasick &  several think he will not see the voyage out. Poor old fellow lies down anywhere on deck & does not eat. A few pups we have are also sick 
 5th Oct 1915:- 
 I get out at reveille & go down to breakfast. Feel well, but still a little shaky   Weather much calmer &  very enjoyable. After three good meals feel well . Run into a school of     Sea Pidgeons   "Mother Cary's Chickens", dense mass of them. Four whales spouting half a mile from us 
 