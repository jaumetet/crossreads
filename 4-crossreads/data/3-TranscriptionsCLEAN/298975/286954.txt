  
 Sept 1917 Saturday 8th Was put on the Sanitary staff today with 2 others Started digging a pit for rubbish Rambled down the country lanes in the afternoon & bought about 18 very nice plums for 6d 
 Sunday 9th Rained gently most of the day Went for a walk in the evening 
 Monday 10th A very fine warm day. On the sanitary job all day. 
 Tuesday 11th Quite hot today Things as usual 
 Wednesday 12th Gave up the Sanitary Job & went on pariade. Went through Miniture Range Score 38 Went over to next camp 