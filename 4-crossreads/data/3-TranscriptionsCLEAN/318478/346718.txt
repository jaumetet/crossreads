  
 of a ten foot deep C.T. 
 Spruce says he has always had a leaning towards art & beauty and he thinks that Port Stephen is the most beautiful place in the world.  When I told him he would never be as good a painter as a mule catcher he very cleverly replied that a man does not value  his  the gifts that are handed to him with his birth. 
 I am getting keen to reach Cairo and some news of you.  This trip has indeed cut me off from you but I need not I think dwell on the fact that it would be good to have you alongside when I am working even if I was snappy & even if you said the wrong thing about painting. 
 I am hoping for the best news of the boys though naturally I am prepared to hear of breaks in behaviour & breaks in health knowing or rather feeling that they have enough of you in 'em to get through well in the long run. 
 Yours Ever G.W. Lambert Hon. Capt. A.I.F. C/o H.Q. Cairo. 
