  
 Dramatic interval.  It was my pigeon to get up, light fire & produce morning tea. 
 Reviewing the foregoing pages, I feel that the best thing that I can write is a description short, and perhaps acceptable, of the hiatus which must unfortunately bridge over the gap from the time when my self-imposed train duties & the vibration of the horse truck. 
