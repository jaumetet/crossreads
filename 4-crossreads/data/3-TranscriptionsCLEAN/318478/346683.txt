  
 Paris en route to Gallipoli Jan. 20th 1919 
 Dear Mr. Box 
 Will you kindly arrange with H.Q. Horseferry road Pay Master's Office that my wife Mrs. Lambert 25 Glebe Place, Chelsea be paid  a separation  an allottment out of my pay, amounting to 1.0.0 per diem.  I suggest that you might arrange that she receives this allottment fortnightly, that is 14.0.0 every two weeks.  This arrangement to come into force on or about 1st March and made by fortnightly  regular  payments. 
 I further beg you to notify the paymaster at Cairo or Cairo through H.Q. Horseferry Rd. that I have made this arrangement.  I take it that the allottment would be sent to my wife in the form of a cheque. 
 So far the journey has been very enjoyable compared to my first sojourn in the East. 
 I have just seen Gullett & Mr. W.H. Hughes at the Majestic, they both looked optimistic. 
 Yours truly 
 G.W. Lambert. 
