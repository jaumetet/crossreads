  
 Cairo April 6th 1917 [1919 ?] 
 Dear Mick;  two days ago I wrote you from this place thanking you for two letters and assuring you that I would send on the diary the last batch of which I think I mailed at Constantinople.  I also told you I was in hospital with Dysentry and as this letter referred may not have been mailed by a rather jovial visitor I write you after two days of rather rotten moments.  Let me briefly say that I am over the worst & quite on the mend though the starvation business leaves me weak.  Every body here is charming & my treatment wonderful.  It's a good rest & costs nothing - I am not allowed to write or talk much so can only say cheerio & hope you & the sprigs are fit. 
 G.W. Lambert Capt. A.I.F., War Records Cairo 
