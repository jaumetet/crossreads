  
 August, 1915 Monday 23 Last night had horrible nightmare 1000s of Germans Barb wire 6" long & saw all my mates who were killed, I shot the Germans but they would not stop - I did not take my camera to Dardanelles, but the scenes are impressed on my mind for all time & it seemed too serious a job to be photographing. 
