  
 April, 1915 Thursday 29 Field day at  Ismalia  Canal & went to Pyramids at night to Moonlight supper in honor of Australian success across the water great joy at our mates work but Censor will not let me give particulars, sorrow for the killed - Letters from Laura 9/5/15 & P.C. from Charlie 28/3/15 N.A. Farrer all's well. We expect a move very soon now & are impatient at delay. 
