  
 August, 1915 Tuesday 24 Had another dream last night - Joe will remember Alma - well the Germans were shelling the Homestead from Trial Shaft & I made the men make the underground Tank into a Bombproof dug out - Let old Panic the stallion loose so he would not get burnt & undid all the dogs - Funny what brain cells lie hidden all these years - Poor Germans fancy them wanting DL 
