  
 Tuesday November 12th 1918. 
 The Daily Mail of today's date brings the glorious news which the world has been waiting for, Germany gives in, which practically has put an end of all hostilities which has been raging since Aug 14th 1914. 
 Germany Gives in. 
 Armistice Signed. 
 Conditions of the Victory. 
 Occupation of Germany up to the Rhine. 
 All Submarines to be surrendered. 
 Great Rejoicings in London and Paris. 
 Admiralty, per Wireless Press, through the Wireless Stations of the French Government. Marshal Foch to the Commanders-in-Chief :- 
 Hostilities will cease on the whole front as from November 11th at eleven o'clock, French time. 
 The Allied troops will not until further orders go beyond the line reached on that date and at that hour. 
 Signed  Marshal Foch. 