  
 to Wilhelmshaven; the German Base, to superintend the disarmament of the rest of the Fleet. 
 No Demonstration 
 Procession of Broken German Hopes. 
 On Board [H.M.S.] Curacoa North Sea. 
 From the D.M. Correspondent, Wednesday 20/11/18. 
 The surrender of the first batch of twenty U-Boats was accomplished at seven o'clock this morning some seventy miles out to sea. More than eighty others are to be handed over between now and the end of the week. 
 On arrival at Harwich last night I was escorted on board M. L. 13 whose destination was somewhere in the North Sea. Steaming eighteen knots we passed down the River Stour by the Cork Lightship and the wrecked [H.M.S.] Arethusa, which could plainly be seen in the moonlight. 
 After steaming some 20 miles across the North Sea we sighted the Harwich forces 