  
 at Sunset today and will not be hoisted again without permission. 
 At 12,30 came what may be said to be the last signal of the Naval war:- 
 "Negative man action stations". 
 Thus the German Fleet ended most ingloriously the struggle which Williain II began twenty years ago. 
 I hear that 50 German destroyers which are on their way over to surrender are one V30 struck a mine and sank. The cruiser Koln was delayed by condenser trouble and towed by one of the German battle cruisers some part of the voyage. 
 The return of the Fleet with its prizes to the Firth was one of the most splendid spectacles which man could imagine. For miles the lines of British ships crossed the sea moving with exquisite precision with paint & brass work sparkling in the sun, with the glorious White Ensign flying and signalman busy with their rainbow hoists of signal flags. There was no exultation or desire to trample 