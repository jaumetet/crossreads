  
 [Post card with the following printed on it:] A Request Will you kindly write a few words on this postcard when you receive the parcel, as the sender would, of course, be glad to hear from you that the parcel had arrived safely? 
 The postcard is addressed to the sender. 
 [Below is a sketch of a military person and a sailor who is smoking a pipe, and the words:] A bit of alright from home. Southern Cross Tobacco Fund organised by The Over-Seas Club General Buildings Aldwych, London. W.C. 
 [The postcard is addressed to:] F. Burrowes Plumpton New South Wales Australia 
