  
 [Reverse of the above post card.  Also typed with a blue ribbon, stamped and dated 15 Feb 1918, Fovant Camp, Salisbury] On Active Service Abroad. 
 Master Frank Burrowes Franurra Rooty Hill N.S.W. Australia 
 We have seen lots of camels like the one on this card, and I am glad that I dont have to ride on them because they look as if they were awful rough. We saw some Australian soldiers riding on them when we were coming across the desert. And another time about thirty of them loaded up with big packs of something and going down to the ship to send the stuff away. From Dad 
