  
 [Reverse of the postcard referred to on previous page. No stamp or date] Miss Ruth Burrowes Franurra Rooty Hill N.S.W. Australia 
 I hope you will write sometimes when you have not too many lessons to do, cos I just love to get the letters from my little kiddies, and if you don't write to me, sometimes you might forget that you have a dad. 
 Goodnight little girl lots of love & kisses from Dad XXXXXXXXXX 
