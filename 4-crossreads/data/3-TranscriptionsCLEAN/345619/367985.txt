  
 [Reverse of the above postcard. No stamp or date] Miss Ruth Burrowes Franurra Rooty Hill N.S.W. Australia 
 One of the colors off my coat is for you & one for Frank. Mum might sew it on your dress or hat, if you would like her to, cos she is a nice Mum. Good-bye little girl for a little while. lots of love and kisses from your old Dad. 
