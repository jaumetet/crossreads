  
 4 I've sent home to Dorothy a book of Rodin's statues. I'd like to send everyone something but you will have to wait a while. I sent one of Brangwins famous drawings of Belgium to Mal, I'd like Dorothy to see that some day. We have all been reduced to privates here, they don't recognise Australian schools at all. I'm disappointed, for many of the men over us know very little even now. But worst of all it leaves me on 2/- per d. so that I cant send too many cables & the such like. 
 Well Mother I'm on a draught read to go to France. Once more the 1st Div. had been chopped up on the Somme & Col. Price killed. Goog Tyson left today at a minutes notice for France, so they must want Officers badly. Birdwood soars higher & higher on the ref of the Anzacs, but poor Anzacs. I saw a procession, the Lord Mayors procession, in London a few days ago. The reception our troops got was just hear rending, & my word they looked fine too. You cant get away from it. The N.Z. & Austr's are the goods. 
 Well everybody I hope you will cheer up soon. Remember me kindly to all who ask after me, & now with best love to all 
 Your loving son 
 Dene 
 Give Billy a kiss from me, dear kid. 