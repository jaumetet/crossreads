  
 On  Active Service 
 Mrs Arthur Fry  Northcote Rd Lindfield N.S.W. Australia 
 Malta. 27th Sep. 
 Dear Mother 
 We arrived at Malta yesterday morning but are not embarking the wounded till 9 this morning. It's very funny to see the old slouch hats coming round in the funny sandola boats. Australians everywhere! Malta is awfully pretty, I wish I had a box of colours here to colour these cards. We are not likely to get ashore here but we have had a good deal of leave when possible (about 3 days in Alexandria) so must not grumble. 
 I do hope Wilga has got over the measles without any permanent effects on her beauty. Lovingly Dene. 
 On  Active Service 
 Mrs Arthur Fry  Northcote Rd Lindfield N.S.W. Australia 
 No stamps available 