  
 7 scanned by a soldier looking for a dear pal, or perhaps someone in the same quest as myself. I cant help thinking how you all must long for just such an hour as I had, to do the dear lad of so many happy memories, a small homage in reward of his sacrifice. But I must stop, I only upset myself this way. 
 I will give you a description of how to find the grave. Enter the village 