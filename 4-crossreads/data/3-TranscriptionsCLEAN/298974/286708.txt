  
 3 Sept. 15 
 Dear Mother 
 This is right in the centre of the town and is very old & historical. I don't know how far it goes back but I think from some scripts & writings on the out side, to about Richard 3rd. Its crumbling of course, but is beautiful to look at. It was formerly the gate to the city. We expect to leave England for Dardanelles on the 8th. I had 3 1/2 days in dear dirty muddied old London. 
 Love to all 
 Dene 
 Mrs Arthur Fry  Northcote Rd. Lindfield, N.S.W Australia 