  
 In the train Exeter 12th Oct. 
 Dear Mother Arrived Plymouth today & set out at 6.30 P.M. for camp which they tell us is not Salisbury but Wools or some name that sounds like that near Weymouth. I'm awfully well, but had neuralgia & an absys on the boat. The sea was calm all the way from the Cape. We took 7 weeks & 3 days - a very long trip. Best love to all Dene 