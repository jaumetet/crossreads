
of punch from the Commandant
24/12/17Snow again but only light wind blowing rather heavy  we are allowed lights until 12 oclock tonight   packet all day yesterday  tins today [indecipherable] last the first of the NCO were taken away to Soltau Central for Holland  they were given 1 hours notice to pack up  what excitement it caused here some of the old pris could not keep themselves in control.  quite jubilant  no wonder being penned up for 3 years.  8 went at 1 oclock this afternoon 18 at 5 PM.  concert tonight  extension of lights.
25/12/17Christmas day spuds & cabbage for dinner  German Church service this evening
26/12/17Boxing day snowing like yesterday all day.  we had extension of lights last night & tonight
