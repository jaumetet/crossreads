  
 more cases would come after about four when we were to pack up & join A Section. Someone had made a mistake however, for when we were half-packed the constant succession of cars recommenced, & we had to unpack again & carry on. 
 Somewhere about 6 oclock however the arrivals ceased & we commenced the tiresome job of packing up. Then stretchers, salvage & all kinds of things that littered the green fronting the roadway, were collected, lorries & cars were filled with stuff & sent off. 
 I found myself with the last group of people, & we had a weary wait from dusk to dark, & a (luckily) fitful moonlight, until at about 11 oclock the last lorry came, & we loaded it & left. 
