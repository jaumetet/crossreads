  
 Tuesday 20 Aug 1918 
 Yesterday (Monday) I swam in the morning & after dinner, went for an afternoon's sport on the river (or Canal rather) with Bert Cunnington from the Q M Store, & George Goode from B Section Tent Sub. 
 They had commandeered a wandering bridge section or span composed of planks & barrels. This was very useful as a punt for use from bank to bank of the canal, saving a long walk round by one or other of the pontoon bridges. We got oars, however & rowed down river to the next bridge below, then took it in turns to tow up the river from the tow path, the others fending the boat off the bank with oars. 
 Today, with the exception of a little digging this morning, we have been at the 
