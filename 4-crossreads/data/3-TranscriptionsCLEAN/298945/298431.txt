  
 Sunday 8 Sept '18 
 Yesterday morning, when we awoke, we found that most of the transport had packed up & we were due to leave at 10 oclock. On the previous evening Barlow, George Goode & I had strolled over the hills at the back to a concert given by the Sentimental Blokes, & enjoyed a rare treat. 
 So we marched out at about 10 to the road turning to the left of Cerisy, past waiting batts  groups  of infantry men of our Brigade until we reached the end, then we got aboard some motor lorries that arrived at that moment. We understood that we were going up the line, probably as a reserve division at first, & to go in later. 
 Along the terribly dusty road we travelled through Morcourt & Mericourt, both of which villages were not knocked about so much as those further 
