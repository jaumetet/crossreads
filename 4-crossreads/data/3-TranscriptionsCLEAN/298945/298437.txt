  
 bombs in Hamelet, a couple of hundred yards away, and unfortunately caught some of the 3rd Division ( 4  6 Batt, I think) while they were resting at a Comforts place, on their way out. This morning he bombed a battery just over the road from us, but unlike last night I was half asleep & did not notice it much. 
 This afternoon we discovered some men digging a gun position about 100 yds from our dug out, so we shall have to consider clearing out  soon  before he starts shelling & bombing it. (Proved to be a searchlight position) 
 At daybreak this morning the 4th Batt  attacked the German position with the idea of advancing about 1000 yds I believe. Fritz was rather better in his defence, evidently because he is desirous of hanging on to a particularly high hill in front of our lines. We had one or two Fritzes through. 
 As far as I can see, the main trouble 
