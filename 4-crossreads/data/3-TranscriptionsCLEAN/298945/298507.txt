  
 x Sal Tuesday 15th Oct. 
 Since last Thursday I have been working at top speed, & have collected a cold which has made matters rather worse. On  Sunday  Monday I had a little straffe from the Colonel which was easily bourne but concerned an annoying affair. 
 Last Saturday's Committee meeting was attended by two delegates from the 2nd & one from the 1st Field Ambulance as a confirmation of the inter Ambulance discussions which were commenced when we were at Hondeghem. These had been invited as a result of the previous weekly Committee meeting, but by an oversight the Colonels signature was not obtained to the invitations, although the Minuite of Meeting which would have given him the invitation was sent down for his perusal, which it, however did not get. 
