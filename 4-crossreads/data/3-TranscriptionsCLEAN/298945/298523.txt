  
 Here is what I have written in Bert Emmott's autograph album - my first original poem - least of all sonnet: - 
 "A topic sketch, I know, is what you fain Would have; yet I have thought 'twere better to Evolve some helping thought, to give to you - Clothed in rough chaff, - a simple little grain. On life, the calling which you choose, - or brain Or hand, if you success desire to woo, Demands you pass through tests: heights are taboo Unless the standard of each test you attain 
 So in the moral side of life you'll find Soul testing opposition, needing strength To overcome; after a fight, at length You'll reach a point - to failure half resigned Weakness will whisper to you: "Never mind". Fight on: that foe can ne'er be left behind." 
