  
 of machine guns firing at aircraft: A minute later a huge Gotha, shape like this came over. 
 [Drawing of biplane] 
 It flew absolutely directly towards us, & must have seen our huts & tents with the long line of waiting motors. We held breath & waited for the bombs we felt sure would be ours for no asking, but he flew straight over. He may have seen that it was a dressing station. A moment later he dropped five bombs to the right & behind us, apparently on the main St Quentin road 
