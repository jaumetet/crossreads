  
 1st Light Horse Comforts Fund Depot 28 Moore St Sydney 
 April 17 1919 
 Lt.Col, T.E.W. Irwin D.S.O. C.O. 1st Light Horse 
 Dear Colonel Irwin We cannot tell you how delighted we are to know that our beloved 1st Regt is home again. To you, your officers and men we send our very heartiest greetings and we hope that we may have the opportunity of joining with the 1st. L.H. Veterans in welcoming you all. 
 We count it a great privilege to be associated with a Regiment which has established such glorious traditions With every good wish Yours faithfully For the 1st L.H.Comforts Committee 
