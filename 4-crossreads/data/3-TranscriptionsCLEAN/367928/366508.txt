  
 1st Light Horse Regiment Australian Imperial Force 
 Routine Order No.98 Lieut-Colonel J.B.Meredith, Commanding H.M.A.T."Star of Victoria" A.16 3rd December 1914 
 Detail for tomorrow Friday 4th December 1914 
 Captain of the day    Capt.T.E.W.W.Irwin  B Sqn Next for duty.   Capt . J.M.Reid A Sqn Subultarn of the day   Lieut.J.O.M.Nickoll  C Sqn Next for duty    Lieut  G.H.L.Harris  B Sqn R.O. Sergeant   No.68  Sgt Adams.G. R.OP.Corporal  No.284  Cpl Ward.W.B. R.O.Trumpeter  No.511  Tptr Helff. H.G. 
 [Then follows details of disembarkation at Alexandria - not transcribed] 
 