  
 of Reincarnation & his belief in Theosophy as a religion. He certainly explain the religion according to well know scientific facts but the foundation to our mind is only an assumption. 
 Go to see Ma'adi Power Station & are shown over it by 2nd engineer 
 2/4/16 
 Go to church & on return receive orders to pack up ready for moving in morning 2nd L H Bde details will move off at 9am. Eric feeling unwell. 
 3/4/16 2 am Reveille blows & all are called out to break camp. Fires are lit and all rubbish burnt. By the light of these fires tents are struck & everything packed up. At 8 am rations are served out & 12 motor transports arrive to transport the camp. All the sigs hang about under our gallant Serg Ryan who is neither use nor ornament as he bums round until we get fatigues  Craige & I therefore go down & help load a lorry with kitchenware & as the last few articles go in we crawl forward & take up our position where the 