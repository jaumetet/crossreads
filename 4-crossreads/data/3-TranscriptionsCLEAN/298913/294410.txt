  
 28/9/16 Capt Downing adjusts set. Have a talk to him and put in for a transfer to transports. Visit Eric after tea & start ear biting with young Gow & Hoppy. 
 29/9/16 Leave Romani at 7am  for Amara. Have dreary trip out & arrive at 2pm 
 30/9/16 WA2 leave 6 am for Romani. Find camp in filthy state so have to clean up. 
 2/10/16 Go out to Bir el Abd to try & bum some coal from the engine driver of the railway  train but has now reached Bir-el-Abd. Have poor success owing to just arriving too late. 3rd Brigade quarantined for cholera which has shown itself again. 
 4/10/16 Find lovely green centipede in my bed on turning in  Stunt leaves for Mazar. 
 5/10/16 Aeroplane on the way out to Mazar. 
 7/10/16 Have some fun trying to get war news from the Tommies on the buzzer. 
 For Continuation  See No. 3 
 Diary. 
 Over 