
Transcribers note: Kings Crest on top of Page.
He whom this scroll commemorateswas numbered among those who,at the call of King and country, left allthat was dear to them, endured hardness,faced danger, and finally passed out ofthe sight of men by the path of dutyand self-sacrifice, giving up their ownlives that others might live in freedom.Let those who come after see to itthat his name be note forgotten.
Pte. Thomas Bell20Bn.  A.I.F.
