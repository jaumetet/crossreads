  
 proceeded to Boulogne again for the purpose of embarking for the front. 
 We marched through the better part of this city this time, the French people giving us the "glad hand" and "eye too" at times, but we were on the march! We reached Boulogne Station about 1, and entrained in "nice comfortable cattle trucks", forty in each! 
 The day was hot and oppressive, but we all had good spirits, so who cared, even if a cobber did put his feet on one's shoulder to rest them like! We could see little of the passing scenery. 
 We came to a stop next morning (Aug 8th 1917) at Harfleur. Tired and hungry, we proceeded to the Base Camp, Havre to prepare ourselves for our little encounter with dear old Fritz. 
 On Aug 11th 1917 I paid a visit to Havre, electric cars take about three quarters of an hour to reach the main portion of this town. The electric cars were manned 