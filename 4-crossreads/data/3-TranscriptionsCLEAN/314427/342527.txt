  
 too, to enable the mine sweepers on each side of the bow to be drawn in. We carried them from Avenmouth. We are now out of the mine danger zone. The mine sweepers considerably reduced the speed of the ship. Our course today was from 20 to 30 points W. of South 
 Friday April 4 1919.   145 4th day on the voyage. It is very stormy this morning, & the sea is running very high. The sky is overcast with  cloud.  During the day the weather moderated a good deal. 
 I bough a book from  the educational officer this morning on shorthand price 2/6. It is one of the latest books on Pitmans Shorthand & is entitled "Pitmans Shorthand.   Rapid Course. I was inoculated again today as well as most of the men in the ship. This is my first inoculation on the ship and the second for the trip. I was also in the spray room for about 10 minutes. 
 We are about 200 to 300 miles from the land, & late this evening were about opposite Lisburn. We will soon be out of European waters. 
 Saturday April 5. 1919.   146 5 day on the voyage. The sea is calm this morning. The sky is cloudy but the sun is shining and brightly. 
