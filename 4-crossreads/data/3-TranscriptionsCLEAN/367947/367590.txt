
dead bodies lie rotting in the sun. Talk about extremes. This life shows up almost everything. We are resting I might say.
Have heard all sorts of Rumors about Italy & Greece & Bulgaria. War news is generally out of date by the time we get it, but on the whole the situation is getting more favorable each week. There can only be one end and there is no doubt the world will profit by the results obtained though effects will last some years.
Well old man will close. I don't want to put anything in about the trench fighting as the censor might kick the lot out.
Give my love to Clare, Al, Ede, & all the others. Hope things keep bright for you and all and that this war ends soon.
Good bye for the present from your loving brother Will.
Address Sgt Major McG. 8th L.H. Regt etc