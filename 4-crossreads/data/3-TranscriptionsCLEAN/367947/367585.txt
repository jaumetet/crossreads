
than hitherto. I will try & repay you for your kindness to me by everything in my power.
Give my love to Clare & the kiddies. Always keep them foremost in your thoughts. Give Al & Ted Ede Hen Nell Alec & all he kiddies my love. If I come back will have many a good story to tell. If I don't well it is God's Will. Dont forget the little girlie & the boy Jack, must now say goodbye. Hope you are doing well & prosperous.
Good bye & God bless you alwayYour loving brotherWill