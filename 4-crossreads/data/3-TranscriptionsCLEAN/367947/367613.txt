
but don't get downhearted. This will all end soon & things will be alright.
Wait till I come home. You'll be quite surprised the way things are going to go. So cheers. Hope dear old Clare is up to form again. And also all the kids & Al & Ted & all the others. Tell them some day to expect a letter each, but I don't know when.
We are continuously moving & life is fairly strenuous. Our horses are getting What Ho' but they are good stuff & stand well.
Now excuse this note & don't think I'm shelving any questions from your last letter. Fact is I've come out here in such a hurry that I haven't got any of the old letters here.
Now heaps & heaps of love to you all. You'll probably get this long after Xmas but lots of fun & a happy New Year.
Yours gratefullyBill
(Dont forget that par about Lizzie)