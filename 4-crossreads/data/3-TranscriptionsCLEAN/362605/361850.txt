  
 decide our future arrangements. 
 Going to France was likely to shorten our stay in this Hemisphere but You see we are back at the old place amongst all our old acquaintances.  We do not expect to be called up for demobilisation until about May or June & then we aught to be home by Xmas.  The whole delay is shortage of shipping on account of strikes etc.  We have chaps here who aught to be Home now but are still waiting for a boat. 
 Well, Dad, we will be sure to send a Cable as soon as we are ready to sail but just carry on a little while longer & expect us for the swimming season, which we have so sorely missed, even if it is November or December. 
 Best wishes to Otto & my other Pals Love From Harry 
 P.S. I am writing to mum also by this mail 
 H 