
11.10.16a very quiet time this afternoon & at 9 oclock tonight we caught the motor car & came back to Camp   A few American sailors are knocking around Alexandria & they have no trouble to buy an argument but the lads have behaved very well here & I have seen no more than half a dozen lads the worse off for liquor
12.10.16Thurs 12thCaught the train at 10.30 this morning & had a good trip back to Kantara. I slept the greater part of the journey, we arrived at Kantara at 4.30 & found
