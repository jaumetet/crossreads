  
 however our man recommenced the flight after our guns had ceased firing and took after the Taube again at flying speed The sound of their machine guns could be heard for many miles through the air. I was watching through a powerful telescope and the two of them went away towards the enemys lines until they completely disappeared. 
 Later. Have just heard that the Taube was eventually brought down and our plane had a bullet through the petrol tank caused by our own impetuous machine guns which one could naturally expect - incompetence! 
 29th June Friday 1916 Romani. 
 No Aero Scares today been training Signallers. Nothing to Report. 
 30th June Friday 1916 Romani 
 Been getting ready today for one of these forthcoming "Stunts"(reconnaissances) out in yon desert. 
 Turned out on parade at 1 oclock and after watering the horses we forwarded on a long and weary night march 
