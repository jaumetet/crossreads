  
 30th May Tues 1916 Romani. 
 Awake at 2.30. this morning got saddled up. 6 signallers already on patrol (night patrol and 4 others to go at 4am. and the remaining 2 at 6 AM. The party of Signallers to go at 4 oclock were late and nearly missed the patrol. Great rushing around for 5 minutes. I was all on my own all day and I took advantage of that by having as much sleep as possible. Flying Machine came. close overhead and dropped a message and sailed away again 
 31st May Wed 1916. 
 The whole Brigade turned out at 10 oclock last night and we took part in one of the longest and most tedious night marches I have ever participated in 
 We moved out in column of route in an easterly direction. The night was dark and we only had the stars to guide us. "Stop Talking" and "stop Smoking" was the order we marched across the desert, the soft sand over the horses' fetlocks all the while. The country 
