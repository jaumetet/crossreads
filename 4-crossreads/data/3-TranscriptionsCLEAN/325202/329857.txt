  
 & no moving masses of horses & men - just a desolation without any life except an occassional man looking after water tanks etc 
 We left Bazentin early Friday morning & after about a mile  joined  got on the main Albert-Bapaume road & joined the column of traffic passing through Le Sars & Warlencourt at the latter place the Germans had blown up a mine in the middle of the road & so the track had been taken round it. Seemed a waste of labour as it was an easy matter to run the road run & could hardly delay our troops. 
 Halted on the outskirts of Bapaume for dinner 