  
 one of the family was home 'en permission' & they opened a large bottle of Champagne in his honour so what with the violin & choruses we had a merry evening 
 Nov 15/17 The Chateau, Fauconbergue arrived here last night & are stopping here a few days. The Chateau is a very fine place but the owners are away in Paris & it is given over to a H.Q. Billets when troops are moving through. We have got our horses tied up practically on the front lawn 
 The Country is now quite different to flat Flanders it is hilly with running streams. 