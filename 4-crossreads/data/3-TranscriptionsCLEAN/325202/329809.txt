  
 close by & we are billetted in some empty barns. 
 Have just walked down to see a batch of German Prisoners who have just been brought in - about 200 behind barbed wire of course. A smattering of old men & boys but the majority are solid lumps of young men. 
 July 19 Wednesday We got immediate marching orders at 4 p.m. and within an hour were on the road we soon struck the main road & joined the mass of traffic proceeding towards Albert about 5 miles away. 
 I have never seen such a mass of material & men, either side of the road was just one series of bivouacs 