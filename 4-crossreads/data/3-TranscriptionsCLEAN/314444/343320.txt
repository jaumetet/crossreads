  
 [Lieutenant, later Captain, Claude Elmhurst Benson DCM, No 874, a clerk from Townsville, Queensland, joined the army on 20 August 1914 at age 27, and embarked on HMAT A5 Omrah on 24 September 1914 from Brisbane with the 9th Battalion, H Company. He was awarded the Distinguished Conduct Medal for gallant conduct and ability on 25 April 1915 at Gaba Tepe. He was killed in action in France on 2 July 1916. 
 Describes the torpedoing of the "Southland" off Lemnos Island and comments on the campaign by The Honourable William Holman, MLA, NSW Premier and his Opposition counterpart, Sir Charles Gregory Wade KCMG, Member of the NSW Legislative Assembly in favour of conscription. Includes covering letter from Walter A Cox of Sydney.] [Letterhead of Walter A. Cox] 8th Decr. '15. 
 Mr. Ifouled, Principal Librarian, Public Library N.S.W., Sydney. 
 Dear Sir: 
 According a paragraph appearing in to-day's "Herald" I have pleasure in handing you herewith letter received from my cousin Lieut. Claude E. Benson, which I think is worthy to be deposited amongst those you are collecting for preservation in the Mitchell Library. 
 With Compliments, I am, Dear Sir, Yours very truly, W.A. Cox 
