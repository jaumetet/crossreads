  
 [Sergeant George Murdy Flynn, a police constable of Redfern, NSW, joined the Army on 24 August 1914 aged 29, and embarked from Sydney on HMAT A14 Euripides on 20 October 1914 with the 1st Field Ambulance. He served at Gallipoli and later in France. He returned to Australia on 4 December 1918. 
 Includes a humorous postcard entitled "A close shave".] This letter is for yourself 
 France 29/10/16 
 [Jan 22 Rec'd] Mr. Harris Dear Sir, 
 Just a few lines in answer to your welcome letter of Sept 7th just to hand also a parcel containing 26 pks of Three Castles Cigarettes, and papers galore I say Sgt. dont you think its nice to have a fairy Godmother, like Mrs Avery & Miss Judd at home, Its awfully kind of Both of them, I had a letter from each of them this mail, they said they were sending a parcel for Xmas. So the receipt enclosed in your letter must be another parcel altogether 
 The two men Geo Maitland & John Hobart havent arrived at this unit yet, and Nye & Rodgers I dont think they will get to the 1st again, We are having a very rough time now, It rains every day and its very cold, with mud knee deep everywhere, Cant keep the feet warm at any price, I will look 
 [Written vertically in the left-hand margin:] don. Harris, 1921 
