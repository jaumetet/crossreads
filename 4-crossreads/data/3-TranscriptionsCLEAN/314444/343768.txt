  
 poem that was read in our forest walk to-day:- 
 To My Mother 1916 (Rifleman Cox) 
 If I should fall, grieve not that one so weak and poor As I Should die! Nay, though thy heart should break, think only this: That when at dusk they speak of sons and brothers of another one Then thou can say, "I, too, had a son, He died for England's sake." 
 [At the bottom of the left-hand page is a drawing of a chess board and a chess piece (king).] Well, this will be three years away from you, besides the 1911 one when I was in England, but I hope 'twill be the last one of absence and that next one will be your happiest ever. D.V., we'll have a great outing that day. I was so glad to know from your letters that you were picnicing and going out a lot. There's nothing better to drive away loneliness. I have received lots of 
