  
 [Corporal Kenneth James Howell, No 1921, a linotype operator from Goulburn, NSW, joined the Army on 18 September 1915, aged 21, and embarked from Sydney on HMAT A37 Barambah on 23 June 1916 with the 55th Battalion. He served in France and returned to Australia on 8 May 1919.] [On letterhead of the YMCA "with the Australian Imperial Force".] France 26th May 1918 
 My Own Dear Mother x 
 An hour ago I was in a beautiful, 50-yearsold, beech forest. From our camp it looks like a huge green bush, but once amongst it one becomes aware of closely-huddled-together trees of fifty feet in height and two or three feet in thickness. Amidst the stillness of this miniature jungle about 500 of us sang and whistled hymns, and made the "dimanche trinite" 
