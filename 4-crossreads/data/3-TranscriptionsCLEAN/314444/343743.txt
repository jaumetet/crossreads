  
 France, 26th Sept. 1917. 
 Dear Mrs. Hill, 
 I take this opportunity of conveying my deepest sympathy at the death of your son, Tom. We were sergeants together and intimate friends always, and I have never met a person whom I respected more both as a soldier and a gentleman than Tom, and I am sure all those who knew him had the same opinion. 
 He died bravely and had already been recommended for another decoration for his invaluable work and example set to his men. Your son is buried in a military cemetery in Belgium not far behind the line, where his grave will be well cared for. 
 I only wish that I could express on paper my deep sympathy to you, for one whom I loved and honoured so much. 
 Yours very sincerely Roy A. Smith Lt. 19th Batt. A.I.F. 
