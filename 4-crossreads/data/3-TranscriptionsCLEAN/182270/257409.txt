 a6578113.html 
 1048 
 MONDAY, APRIL 7, 1919. 
 38 
 Fine & calm. Sighted land at dawn 
 & got into Albany at 12.18 pm & dropped 
 anchor in the Sound. It was a very 
 poor homecoming for the Western 
 troops as there was not a soul 
 to meet them. They were taken 
 off in barges & put into quarintine 
 for three days. We got away 
 again at 6 pm. About 740 troops 
 got off - over half the number 
 on board. Got a wire from 
 home. 
 Days run 325 miles 