 a6578089.html 
 FRIDAY, MARCH 14, 1919. 
 1024      14 
 Fine & cool. Played games all 
 day & read on the deck. One 
 of the troops is very bad with 
 pneumonia, but they expect 
 him to live through it all 
 right. 
 Days run 335 miles 