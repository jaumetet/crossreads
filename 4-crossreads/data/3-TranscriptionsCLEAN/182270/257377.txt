a6578081.html
THURSDAY, MARCH 6, 1919.
1016      6
Fine & beautifully warm. Played
gmes for a bit & then lay in
the Sun & baked. It was just
glorious. Passed H.M.S Challenger
in afternoon. Saw birds today
for the first time following us, also
a whale & some flying fish.
Had a impromtu Jazz band
on deck at night & it was
a good break in the monotony
Days run 348 miles.