 a6578096.html 
 1031 
 FRIDAY, MARCH 21, 1919. 
 21 
 Fine & warm. Shore leave was 
 granted. We got into the wharf 
 at 6.30 AM and started coaling 
 right away. Got off at 10.30 AM 
 and went up town with L t  Morcombe 
 met L ts  Jeffries & Smith & went out 
 to Haupt Bay & back round 
 Table Mountain & Groote Shuur. 
 Called in to get a feed of grapes 
 on the way home and struck 
 some jolly fine prople - the Betrams 
 & spent the afternoon with them. 
 Had dinner at the Grand Hotel 
 & went to "The French Maid" after 
 but could only sit out the first 
 act & then came home to bed. 