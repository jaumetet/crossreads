  
 on the coast again. 
 I must say that on the march through the town, the streets of which are very narrow, I have never seen so many drunken men and to their shame, all Australians, but most of them, on a rest, from the front. 
 On arrival at the camp we were told off to a hut which seemed very comfortable though things were anyhow then. Before we had been there five minutes, one of the heroes ? came in and aired all 
