  
 are our greatest wants at present. 
 It is rumoured that we go to Weymouth shortly. This will be an all night place to spend our summer. We are beginning to look upon ourselves as "Six-bob a day tourists" but I suppose we will be hard training soon. So far we are having an easy time. 
 We are all in good health and doing handsomely and have so far thoroughly enjoyed myself. I hope you are all well. While you are having winter we have a glorious summer 
 With so many sights to see time for writing letters is a little limited compared with the time we had on the boat As soon as we get our mail we will have to get busy. 
 Well I must close this much now with best love & wishes 
 Your loving son Sid 
 [Text in margin] P.S. I hope you got my cable which I sent on arriving here. 