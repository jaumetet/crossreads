  
 25th Tuesday I started leave to London about 11 am today & have to go back about 5 pm Saturday. We arrived in London about 4 p.m. and have already heard a debate in the House of Commons. 
 I am writing this sitting on my bed in a Y.M.C.A. so that I can post it tomorrow but must close. I will give further London news when the trip is finished & I write for the next mail. 
 Yours with best love Sid 
 Wallie Fielder arrived  here  Salisbury yesterday He came across France & is doing well. I also visited [Tigheldean] famous for the spreading chestnut tree & the village smithy. It was very good. 