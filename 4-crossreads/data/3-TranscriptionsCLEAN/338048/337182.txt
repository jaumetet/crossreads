  
 [Printed material p.6] 
 - on 1st May, 1880 - On May 1st, 1880, the Public Instruction Act was passed. It transferred authority for education from the existing Council of Education to the Department of Education, headed by a Government Minister and answerable to the State Parliament. It was under this Act, too, that parent organisations working in conjunction with public schools in the State were inaugurated. 
 - Mrs. Totti Cohen, O.B.E., B.A., L.L.B. - Mrs. Totti Cohen, O.B.E., B.A., L.L.B., has held office as President of the Federation of Parents and Citizens Associations of N.S.W. since 1973, and was an Office Bearer and member of the Executive Council prior to that. She is well known throughout the State as a tireless worker on behalf of Education, and her retirement as President marks the end of a decade of consistent activity for parent and community groups. 
 Organising Committee Mrs. A. Benson (Convenor), Mrs. J. P. Jeremy, Mr. N. Arentz, Mr. N. Cohen, Mrs. J. Ryan, Mrs. J. Ragg, Mrs. E. McGill, Mr. S. Wimmer, Mr. P. W. Medway 
 Orchestra Sydney Girls High School, Forest High School, Barrenjoey High School conducted by Barbara McCrae 
 Quartet M. McSullea, G. Dodd, O. Watkins, M. Waught 
 Table souvenirs with compliments. 
 [Transcribed by Sandra Mutton for the State Library of New South Wales] 
 