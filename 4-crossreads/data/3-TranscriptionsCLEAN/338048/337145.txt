  
 put in about 6 hrs in the train. 
 As usual arrangements were defective but finally things got settled & had a good night's rest 
 Tuesday 26th Revielle at 6.15 (what a change from 5 am) & fooled about all day on fatigues & getting camp in decent order. Continually tired today the last few days tearing around evidently requiring a little rest. 
 Wednesday 27th Mess orderly today but for a change was rather busy. Had a little time off to write. Rather wet day though the King inspected the 3rd Divisional and other Australian & New Zealand men. There were 45,000 men inspected I believe & there was a fine parade 
 Thursday 28th General fatigues clearing up again and nothing worth noting with exception of it being a fine day. Rumours re 4 days leave & a move to France are becoming current. Writing in evening & posted mail 
 Friday 29th Made a mounted number & had to work at stables This is slightly promising but evolves extra work. On horse picket at night but had easy time 
 Saturday 30 Owing to being on horse picket I was not on first parade. Rest of the day holiday owing to sports. Walked to camp of German prisoners & saw them building a road 