  
 [Printed material p.1] 
 New South Wales Education 
 1980 Centenary Dinner in the presence of His Excellency, the Governor of New South Wales Sir Roden Cutler, V.C., K.C.M.G., K.C.V.O., C.B.E. and Lady Cutler, A.C., D.St.J. 
 Saturday, 2nd August, 1980 Masonic Centre Sydney 
 [Handwriting at top of page] War Diary May 27 1916 to June 18 1916 