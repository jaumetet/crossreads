
Monday 12thAbsolutely cloudless sunrise & a most brilliant sunshining day.
News received by wireless about sinking of H.M.S. Hampshire & Russian capture of 71,000 Germans. This is first news received since we left port and is very acceptable.
School of whales seen spouting about 1 mile to port. Spouting looking like a cloud of spray. In afternoon whale appeared about 300 yds away & spouted. You see a brownish back or head appear about 6" above water & then stream of spray about 6 ft high. Spray lingers in air about 5 sec.
Meal arrangements improving enabling one to have meal in greater comfort.
Tuesday 13thFine day sea calm, further war news but not very important. Work as usual
Supposed to be rather north of the Cape & southerly course taken during night. Expect to reach port about Saturday.
Wednesday 14thAnother fine day. Sea slightly rough early but calmed down.
News re capture of 141,000 received. War news evidently usual thing now, we are in touch with Durban. Had finger which was slightly septic & lanced about 4 days treated. Dead skin removed & codine & boracic applied. Nothing much the matter.
Thursday 15thRather rough News re Kitcheners death announced. Weather boisterous in afternoon & evening. S.W. gale raging.
Friday 16thLand reported in sight as soon as we were up. It appeared to be a high bank of cloud but as I had expected to see only a very low cloud-like object when we first