
hard fight. for the big word that is the news we receive late that night, also we have captured a great number of guns including one 12in gun complete on the railway also a train load of huns just back from leave, gives you an idea how fast the advance was & the shape the huns were in.
However the night passed off quietly, the next day about ten A.M., we