  
 avail, - we were properly ashore. How lucky we were to have calm weather. There was no panic whatever. Everyone took it calmly and philosophically. 
 The "heads" then struck on the idea of lightening the ship, by throwing coal from the forward hold, overboard. The soldiers were put on to the job and each company had an hour. This was kept up all Thursday, Friday & Saturday 20th February. What solid work it was too!!?? 
 One could not see 10 feet down the hold for coal dust, and we all got black from head to foot. It took an hour after each shift to get clean. The dust got in our eyes mouth ears - in fact everywhere & as none of us were in condition it was jolly hard graft. 
 Floated off 