  
 We did not have the promised storm, but instead the wind and sea went down & the whole trip to Colombo was as calm as could be. On crossing the Line, of course there were the usual celebrations, & all the Officers had to go through the mill. Some of the lads got a bit of their own back then. 
 No leave was granted in Colombo, though [indecipherable] & myself managed to get one whole day ashore. We had a real good time & saw everything there was to be seen. It was quite a new experience to ride in a "rickshaw" & the coolies who pull them seem tireless. Dined at the G.O.H. a beautiful Hotel, better than any we have in Sydney. 
 On leaving Colombo we made for Suez. The weather has remained quite calm, and I do not suppose we would have got a trip as smooth as this once out of 20 times. We did not stop at Aden. As we passed 
 [A page appears to be missing.] 
 