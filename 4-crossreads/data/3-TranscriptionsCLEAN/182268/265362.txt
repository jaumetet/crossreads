  
 Tuesday 16/10/17  Leave to England turned down  went & saw Collett re. same. 
 Wednesday 17/10/17. On Court of Inquiry re. train stopping at Hell Fire Corner instead of proceeding to Birr X Roads [Birr Cross Roads]  Dinner with [Fernie?] 
 Thursday 18/10/17. Left by train to Hazebrouck  there caught leave train arrived at Boulogne at 2 am Friday. 
 Friday 19/10/17. at Morleys for lunch after good trip across  met Hardie & had dinner with him. 