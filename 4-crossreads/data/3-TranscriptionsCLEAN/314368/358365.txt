  
 rumour going around, though that all mail may be stopped in a while for a few months to Australia , but it may be only a hearsay. I have not written to Percy for some time so will have to send him a note as well to night.  Have not much of interest to tell you mother so my note will only be a short one to night. 
 Trusting that this will reach you and find all as well as ever, with best and fondest love to all, 
 Your loving Son, Gordon. 