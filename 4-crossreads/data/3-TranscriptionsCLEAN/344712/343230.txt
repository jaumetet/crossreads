  
 This note book was given to me on the 27th of June 1915 at sea, by the Purser on board the transport "Berrima", and is a diary containing my impressions & experiences from that date.  Having been written originally in pencil, it was almost unreadable, and for that reason I have been to compelled to copy it over with ink. 
 Fredk. C. Burgis 1363 Private D Coy. 20th Batt. 5th Brigade A.I.F. 