  
 Saturday Dec 16th:  8 miles to Franvillers Sunday 17th:  To Dernancourt. Tuesday 19th: To Becourt Wednesday 20th:  To Montauban Sunday 24th:  To Bernlafay Wood. Monday 25th:  Xmas Day. In dug-outs. Delville Wood Thursday 28th:  In Needle Trench 
 Saturday 30th:  In front line 
  1917  Monday Jan 1st:  Demonstration 
 Tuesday 2nd:  Back to Needle Trench. Wednesday 3rd:  Back to Bernafay Wood. 