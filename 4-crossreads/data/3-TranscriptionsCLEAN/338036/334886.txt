  
 [16th Mar. contd] active.  Showers of shrapnel fell around us several times. 
 Saturday 17/3/17 Our last day at Erquinghem Baths.  I was successful in obtaining a complete new outfit.  The Tommies (57th ) Kings Liverpool Regt arrived during the afternoon to relieve us.  The afternoon was occupied in packing up & bath etc.  Had tea with Dick Watts in the cabinet opposite & it was some tea.  Then to Erquinghem to bid farewell to Madame Vandermason & back to Pont de Nieppe [indecipherable] Epinet we arrived at new billets at about 10.30.  Saw  Crook  Sam at Orderly Room & was soon O.K. 
 Monday 19/3/17 Heard the good news of the fall on Bapaume & later the fall of Peronne.  The Germans retreating 10 miles 
 Tuesday 20/3/17  Received letters from Ernie Capetown & Miss 