  
 [30th Dec. contd] paid me a visit & [indecipherable] Swinnerton also came down to see Ern.  To day it is thawing.  We are moving once more to Ravelsberg & today was spent in carting a heap of our stuff as on this occasion everything has to go - even the fittings.  Feeling very much off color tonight & had a temp of over 102 throughout the day.  Staff johns fixed me up & towards morning I got much better. 
 Monday 31st   New Years Eve At midday I felt all right again & hopped out of bed.  By the afternoon the shift had almost been completed. 
 [Following page crossed out] Encyclopedia of Social Reform (Frank & Wagnalls)  1897 Pooles Index to Periodicals Handbook for Literary and Debating Societies Laurence Gibson 
 [Transcribed by Peter Mayo, Darren Blumberg for the State Library of New South Wales] 
 