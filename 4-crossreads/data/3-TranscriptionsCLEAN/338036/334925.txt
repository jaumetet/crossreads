  
 Sat 14 [July contd] Sports meeting Steenwerck.  Good afternoons sport. Sunday 15th Trip to Kandahar Farm this morning with despatches.  Things fairly quiet.  Attack on observation balloon failed. 
 Monday 16th Large number of patients evacuated from Ballieul to our D.R.S. today.  Another attack on balloon failed.  Some heavy shells followed the attack. 
 Tuesday 17th Wed 18th Thursday 19 Pack up & the Ambulance marched to Ravelsburg Rest Station near Ballieul - changing over with N.Zs.  I walked with Bob Thomas via Ballieul. Friday 20th Fritzs long distance guns very active. 
 Saturday 21st Bombing raids close by by enemy planes. 
 Sunday 22nd Enemy's big guns again very active, firing on the Nieppe Ballieul Road very heavily.  In afternoon I counted no less than 21of our observation balloons & Fritz amused himself by firing at them occasionally but without success.  Geo Pickering & I went for stroll to Ballieul & there I saw young Arthur Phillpot (12 or 13th) who had been in Blighty for 5 months wounded.  Had a chat over scraps & past times etc.  At night Fritz again came across on his bombing raids and dropped several in close prox to our hospital.  The heavy Archies & searchlights kept him a name="a6502088">  
  [22nd July contd] busy.  He also dropped an aerial torpedo (?) on the No 2 ACCS Steenwerck & regret to state that young Joe Cox of B Section & 3 others were killed.  Joe was with a party of our Ambulance assisting the CCS during the heavy [indecipherable] work.  Had a look at Jack Hoskins grave this afternoon & saw the grave of Brig Genl Brown of N Zealanders.  He was buried with a private on one side & a Lieut Col on the other. [Joseph Cornelius Cox 11935] Monday 23 Talks of another push soon on this front 
 Tuesday 14th Notification that I am picked for the next stunt & greens issued for Brigade colors again. Boys humour.  "Billy Poole is as humorous as a hangman"  Getting a niggle instead of a bite.  Letters to home. 
 Wednesday 25 Raining hard.  Russians retreating.  Guns quiet for once on account of bad weather. 
 Thursday 26 Australian mail arrives.  First for 6 or 7 weeks. 
 Friday 27 Long range guns active. 
 Saturday 28 At night trip to Ballieul & saw the Cooees (our Division) but very flat show.  Pioneers band OK. 
 Sunday 29 A storm came up early in morning & the effect was brilliant.  Our planes had a good fight to get back but conquered & landed safely.  Later I learnt however that along the front 4 planes failed to return. 
 Monday 30th morning usual 