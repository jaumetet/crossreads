  
 Tuesday Nov 13th It was a lovely day. Slight swell on, but not a ripple on the water. Land in sight all day. We have some beautiful big albatrosses following us. Some are very big. 
 Wednesday Nov 14th South Australia, Victoria & Tasmania is getting paid today. We have to have all our things fumigated today, & it is about time some of them was fumigated too, by the scratchings of the fellows. 
 Thursday Nov 15th Very big swell on today. Very choppy too. Two whales were sighted on the Starboard side & three on the Port side this morning. Paid 1 today. This completes our full payment for the voyage. 
 Friday Nov 16th Rough sea today. Very windy & cold. 