  
 My travels with the 12th Rfets of the 3rd Batt. 
 Brown 47 Albion St Annandale 
 Kept a diary until after the battle of Pozieres, when this was written up.  Whilst resting at Hermines the old diary was rewritten in this form & then the narrative continued from time to time. 