  
 Tuesday 4/8/14 
 I was sleeping on the table in the bureau, the two clerks, Wilking and Kurmann in the hammock when all of a sudden the command "clear ship for action" was given, "Askold" in sight! 
 I have never dressed myself as quick as I did that morning. Everyone is rushing to his post. We leave the bureau and go to the rudder engine under the armoured deck, where a small hospital station has been arranged where bandges are at hand in case the hospital should be hit and flooded. 
 Down here are the 1st Engineer and the hospital orderly, the two clerks, the cook's mate and myself. We are absolutely isolated from above and the heat is unbearable. We are still of the opinion that "Emden" will soon come into action with "Askold". 
 The rudder is shifted to the larboard side by means of the machinery with a terrible noise. 