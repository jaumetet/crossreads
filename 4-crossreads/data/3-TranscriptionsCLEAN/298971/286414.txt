  
 unit.  Estaminets closed for 3 days. 
 10th October (Tuesday) 
 On Waggons touring round the streets after rubbish.  No more souvenirs from Fritz.  Heated arguments against conscription, in our billet till late at night. 
 11th October (Wednesday) 
 On Waggons again today and visited dentist.  Debate on Conscription at Y.M.C.A. 6 pm.  Only 5 votes for conscription from the whole audience. 
 12th October (Thursday) 
 Off duty on dental leave 
