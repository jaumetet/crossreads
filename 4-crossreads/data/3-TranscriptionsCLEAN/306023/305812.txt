  
 Sunday 26th. Got up early and went  for a short  around to Westminster Cathedral. 
 Returned for breakfast and with my chum set out for another trip. We strolled down through St Jame's Park, where I noted that the lake had been drained and the land was being built on, and down The Mall to the Palace where the King was to present a number of VCs and other honours. 
 There was a great crowd present, and again as last night in the Strand we were practically mobbed by the girls. 
 The Guard's Band was playing, and is the most perfect band I have ever heard. They played the score of "Cavallera 
