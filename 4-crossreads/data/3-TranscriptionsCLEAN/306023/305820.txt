  
 old associations etc, London hasn't a building like the Daily Telegraph office or the Commonwealth Bank, and the G.P.O and the Town hall and the Queen Victoria Markets are splendid examples of their various  forms  styles of architecture. St Andrews Cathedral is a fine building of the Gothic style, and St Marys will be better. 
 Trafalgar Square is an imposing place, but though Sydney has no such square, Martin Place and Moore St together serve the same purpose and just as well too. 
 London has no street like Macquarie Street, and Centennial Park track is quite equal to Rotten Row, and Pitt Street equals Picadilly and Bond St, as does Paddies' Market equal Petticoat Lane. 
 Such monuments as the Admiralty Arch & 
