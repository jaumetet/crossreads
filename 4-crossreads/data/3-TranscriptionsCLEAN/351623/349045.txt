  
 11 Oct 18 Coaled ship from collier Constantine. 
 12 Oct 18 Out firing 
 16 Oct 18 Went out & anchored for the night in Lagos Harbour 
 17 Oct 18 Out on patrol. 
 19 Oct 18 Came in again & anchored off Charlestown. 
 20 Oct 18 Coaled ship from Transporter. 
 24 Oct 18 Alongside wall at dockyard Rosyth 
 25 Oct 18 Coaled 
 26 Oct 18 Went out in stream to let L. Cruiser Calidon go into dock. Came back to wall 
 28 Oct 18 Went out from wall to let Aussie & Arjos in to dock. 