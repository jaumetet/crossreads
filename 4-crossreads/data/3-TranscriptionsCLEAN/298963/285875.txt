  
 sorry for but I cannot take any interest in him. 
 Jock comes from Ayr & is a bonnie lad but he is badly knocked about & will I am afraid never walk again as he used to among the bonnie Purple heather. 
 Our ward is full & the little sister is run off her feet, still she smiles & keeps on going. 
 Jock knows her history which is, by the way, an interesting one.  Her mother is Scotch, her father is English, they gave her an education and intended that destiny being kind she would rise to something high.  Poor little sister, she did not rise. 
 When everything else 