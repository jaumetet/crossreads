  
 we can't get rid of chats. 
 If the Germans were better looked after than we were their chats, we mighty hungry. 
 11th June Camp life grows pretty monotonous, yet if we told to go into the line, I suppose would curse our luck.  I think we must find something to occupy our time.  Today we drilled for 4 hours while poor old "Kirkie" Capt. Kirkwood watched us.  In the afternoon "Lord Brasso" - our O.C. - otherwise known as Col. Brennen rode out to inspect us.  We cursed him up & down but it didn't make much difference. 