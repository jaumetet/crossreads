  
 1st May Nothing happened worth recording. 
 2nd Also uneventful, "C" section proceeded to Beecourt Chateau. 
 3rd Played cricket against 1st Amb. Supply Train, won by 100 runs at Aliert. Our opponents treated us royally, supper supplied after the match & a lorry brought us home to Pozieres. 
 4th Got orders at 4 a.m. to prepare to go into the line.  to  Left at 7 a.m. for main dressing station, loafed about all day, met a lot of old boys at night went  to  into line, encountered two or three duds of tear gass, & sundry other unpleasant experiences but reached the Regimental aid post without casualties, 