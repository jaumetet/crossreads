  
 sacrifice lie here taking their last sleep in this life for a time deaf to sound of fritz's guns & our own. 
 5th The day passed uneventfully.  In evening went along railway line, saw damaged tank & went along another battle front. 
 6th Marched to Byzantin with heavy packs, this the old Posieres battle field.  Here the blasted scene of desolation was very noticeable.  Debris & salvage lay everywhere - forests were laid bare to the ground. 
 7th Marched to Bapaume - 10 miles - over the old field through deep mud in places, for the rest over duck boards.  Arrived in Bapaume rather tired after 3 hours hard marching.  Not a single home in this part is left undamaged, some have been pulled 