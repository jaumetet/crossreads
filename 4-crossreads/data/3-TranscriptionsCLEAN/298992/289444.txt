  
 the Xmas tin OK. I think I told you in a previous letter, but the Birthday one has not come to light yet. Had a pair of socks from Dorothy, a pair of mittens from Lily Flecknoe (& they were just fine during the cold weather, when I had a good deal of writing to do, and about Xmas time I also got a lovely billy from Una. That is all that has come along so far, and I think you are all sports to send them. Its just fine to hear you name called out, and a parcel or letters are handed to you. I am sending a couple of photos of the boys gathered round receiving their mail. 
 Must stop now, if I want to get the rest of those 26 letters answered before we move off, we may not have much opportunity to write them. What oh, this for writing paper. Pretty isn't it? 
 Hope you are all keeping well, I will write again soon & tell you what is happening. 
 Heaps of Love to all 
 Alan 