  
 Burns, Philp & Company, Ltd Island Line of Steamers 
 S.S. "Moresby" Voy.102. At Sea, 18th Sept. '14 
 The Manager, Island Department, Messrs Burns Philp & Co. Ltd., Sydney. 
 Dear Sir, 
 The "Moresby" left Butaritari at 5 p.m. on Saturday 5th September and made for Tarawa, where we arrived 1p.m. on the following day. 
 The Schooner "Polenia" was there when we arrived, and came alongside at 6 a.m. on Monday, to take over some trade which we did not have time to put aboard the "Tambo" at Butaritari. 
 Captain MacLeod transferred to the "Polenia", Captain Handley bringing to Sydney the "Moresby". 
 We had 8 Passengers for Tarawa, vis: Mr Workman, Mr. Fowler, who was relieving the Engineer of the "Polenia", a Chinaman and 5 Natives. I collected from Mr Workman   6 - being ;pound 1 for his own passage and  5 for the passages of the Natives. The Chinaman was also charged  1. 
 I have to state that as there is no Ticket Book aboard the "Moresby", no tickets have been issued for these passengers. 