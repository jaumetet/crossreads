  
 I am perfectly well again bar not being quite so strong & having lost a fair amount of flesh. 
 March 2nd Tomorrow I leave for Base details so once more I am going out into the world.  The last few days have been as usual bar a little excitement one night when all lights had to be put out owing to the approach of some air craft but nothing more was heard of the matter.  Yesterday I had my first swim of the season & found the water rather nippy. 
 March 3rd 
 Left Montazah about 9.30 and changed at Sidi Gabar a few miles out of Alexandria.  We then proceeded to the barracks where we obtained a warrant for our tickets.  The place was alive with soldiers all Englishmen.  It was here the battle of Alexandria was fought & Sir Ralph Abercrombie was killed a monument being erected at the spot where he received his mortal wound.  We arrived at Cairo about 3.30 & at overseas Base detail camp an hour later.  The contrast between my late surroundings & my new was 
