  
 where a shark was following the ship. 
 We had gramophones & plenty of Records, cards, & numerous games so we were not short of amusement, & although we were not allowed any lights for fear of Submarines, we thoroughly enjoyed the trip. 
 4 days after leaving Colombo we sighted Smoke on the Horizon to the Stern & later we found out that this was the HMAT "Ceramic" carrying the 17th of the 1st 2nd 3rd & 4th Battalions. 
 On the morning of the 8th of May we sighted Suez & at 4 pm we pulled into the Suez Canal with the Ceramic just behind us. She would have beat us easily only for stopping twice on the way to bury two of the boys who had died of meningitis. We stopped in Suez one night but were not allowed ashore & next morning entered the canal itself on the way to Pt. Said. 
 Never shall I forget this 