  
 12-2-1918 The three men up before the Colonel, waiting a Field, General, Court, Martial. Sent to compound. Things are now very quiet. 
 13-2-1918 Ash Wednesday Raining all day. The whole of the civilian population going to church Town picquet to be furnished, consisting of one officer, one Sgt, Corp Bdr and six other ranks. Horses to be again dipped for mange. 
 14-2-1918 Cold and miserable. The Colonel back from leave. Our S.M. in Hospital. Having his nose X rayed . The M.O. fears that it is fractured. The Farrier of the 3rd Bty. Sgt Quelch, evacuated to hospital. Heart affected 
 15-2-1918 General Walker inspecting our lines this evening. Scabie and teeth inspection. Col Dean acting C.R.A. During General Anderson absence. Tonight is bitterly cold, freezing 7-30.P.M. Artillery active 
 16-2-1918 Terrific frost. Our 8th Brigade had a hop over 