  
 at 2 oclock dropped dozens of bombs. An enemy plane over at 5.P.M. Had a try for our camp, but missed it 
 11-10-1917 Fine day. A few shells flying about. Hun planes over again, went through the sam performance as last evening, dropped 3 bombs. A new C.O. Colonel Dean. Col Anderson is now C.R.A. Gunner Allen wounded at gun position. Bty Sgt Major Fitzsimmons to go to B.H,Q as first class W.O. Bombardment going apace on our left. 
 12-10-1917 Raining almost all day. Terribly muddy Brother and detachment down for a rest. Only two guns in action. 8 casualties in Bty yesterday. Eleven killed and 4 wounded in the 4th Bty yesterday. 
 13-10-1917 Again wet and windy. Terrific hail storm this morning. Very quiet on our front 
 14-10-1917 