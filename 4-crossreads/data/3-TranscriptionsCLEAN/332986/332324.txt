  
 guns firing at it, did not hit it. General Anderson in our lines, 21 rounds to regester a gun, Mug officer. 
 20-1-1918 Sunday Fine day, Sgts Flett and Buchannon down from guns. Major Byrne gon to Bty. A Terrific stunt away on our right. A 14 inch gun in action, somewhere in rear of our wagon line firing at 50,000 yards. The Chateau that was burned down was valued at 50,000 Francs. Lieut Stoddart rejoined Bty. Been away some months. 
 21-1-1918 Showery, All day. Moonlight nights. Cold. Shod General Andersons horses. Capt Staley at wagon line. Lieut Mulholland transferred to 3rd Bty. 
 22-1-1918 Beautiful day. Our aeroplanes busy. Fair amount of artillery work. The French advanced 21 Kilometres without opposition 
 23-1-1918 A few isolated showers, Otherwise fine, 