
6used here, so there is a general duck as a passenger steamer comes along. The warning is (Bob down before you're spotted). The passengers, I believe, throw tins of cigarettes etc over to the lads when passing, I haven't been there on such an occasion yet.
Have just received parcel of Trench Shirts sent from home. All parcels sent to me have come to hand so far.
Sunday 14/5/16Had a bosker swim this morning went down at 6-30 it was a treat. Two cargo boats went through while we were there. Soon after we had finished breakfast the "Medina" was sighted coming through the desert she looked great.
It's a dirty windy day
