  
 3 Breakfast was on soon after & I had another good snack with Eric Clarke. I took one of our tables out of the wagon & secured a couple of boxes. 
 The Adjutant came along after we had finished & told me to have our things brought over here to this hut where we soon made things a bit ship-shape. 
 There happened to be an empty tent on the lines so we dug ourselves in there (4 of us). That same afternoon we all went down to the Canal for a swim it was great after perspiring like we did all day. The water is lovely & clean but very salty. It is fairly shallow along the banks for a couple of yards but 
