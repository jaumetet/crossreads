  
 for the city has not long been liberated & every overhead bridge is in ruins across the track. 
 Roubaix is more a residential resort from Lille It has narrow streets & 2 squares in one of which is found the Town Hall & the old Church marked like most churches here with the words Liberte Fraternite Egalite, so marked same way as said because these things no longer exist in France & the church is a fitting sepulchure for  them  past ideas. 
 I return to Lille after passing through Park at Roubaix. In Lille I try to cash a cheque but as it is Australian Bank they refuse. British Paymaster cannot give me money because I am Australian & so I must carry on with what I have. In afternoon I spend at Club with diary notes & dine there in evening for 4fr. Bed costs 2 fr. I go for walk round city after dinner & then turn in. 
 9 January 1919. On rising this morning find I have lost my cigarette case & thinking it might be at Brencq I set out for it having just enough time to reach there & catch train. Unfortunately the case is not there & it takes me all my time to reach Railway by 10.30. 
 However I need not have hurried as train does not arrive till 12 & is then shunted into a 
