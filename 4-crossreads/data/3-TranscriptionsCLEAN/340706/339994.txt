  
 40 finding no officers in the mess I return to B Coy have supper & then take Beatons billet again. 
 14-1-1919 This morning I breakfast with A Coy & then visit billets at 9 am am allotted to No 2 platoon. Routine here consists of breakfast 8.15 am. visit billets 9 to 9.30 then loiter around football field till10.45. fall in & route march or battalion parade. 
 The whole of the afternoon is free to do as we like. Lunch is 12.30 & dinner 6.0 pm. The night is free The one mania in this battalion is football & it is pitiable to see grown men, pretending to be children & with minds incapable of grasping the larger ideals & so degenerate in a eager childish way to amuse themselves with something that cannot interest them. 
 The billets are arranged as artistically as possible as marks are awarded to each company & everyone save the diggers simulate keenness Owing to intervention of my company commander Major Carr I manage to get two suits underwear & towel from Baths. After dinner 
