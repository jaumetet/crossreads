  
 [Diarist appears to have written the words "Forward [maricort?]" in the left hand margin alongside the entries for dates 26 Sun and 27 Mon] [26 Sun] Mounted piquet at 6pm. 
 [27 Mon] 6pm. Went to T.M. Pits with a load of bombs  Had to wait until dark as Fritz could observe. road. Got into Gas coming home & a few stray bullets. Back to lines at 2am 
 [28 Tues] Did not get out of bed till near 11 am. 
 [29 Wed] Carting for bomb breaks Concert in the evening. 
