  
 Left Sutton Veny. 
 [The Diarist has written the following in the left hand side margin:] Issued with rations at Warminster 
 [12 Th] Rev. 2-30am. breakfast 3 am  fell in 3-30. Left camp 4 30am  Left Warminster station at 5-30 am. Arr. Oxford 8 am   Birmingham  Birmingham 10 am. Crew. 12-15pm. & Liverpool warf 2pm  Went on board the H.M.T.S. 'NESTOR' at 2-30pm. Tea at 6 pm   "A" Dock No. 7. mess   Cast off 6-30pm. & lay off in river  Issued with blankets & hammocks 
 [13 Fri]  on board H.M.T.S. 'NESTOR" . weighed anchor 4-30 am. Breakfast 7am   Dropped Pilot at  Maidenhead  Hollyhead. Dinner 12noon  Tea 5pm.   Very heavy swell. Very few of the boys came down for meals  
 [14 Sat] Took in mine catchers at 4-30 pm. Very heavy swell, & only a few of the boys came down for meals  Went to bed at 7 pm. 
