  
  Pont Noyelles.  
 [9 Th] 1pm. Went with Limber to the 27th Batt. for 12 boxes of bombs. Took them to the 25th Batt. Mounted Piquet 7 pm. 
 [10 Fri] Orders to pack up & move out at 9-30 am  Arr. back to section at 12 noon. Camped about 1 Kilo from  Pont Noyelles.  
 [11 Sat] Went to Querreul for canteen stock. Sports at the section in the afternoon 
