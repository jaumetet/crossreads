  
 as slowly as he can & I think his idea is to delay in as much as possible so that the allied armies have no time to organise an attack on his line of defence before the weather breaks for the winter. 
 The Div has had a weeks spell & other troops have gone on ahead of us - It is now our turn to  taking  take up the running again & we are to leave here in the morning at daybreak with some 30 kilos to do. 
 Labour Companies have been cleaning up the town & Corbie &  I have  there  already  are already a few civilians back 
