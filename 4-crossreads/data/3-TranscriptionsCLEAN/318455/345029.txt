  
 on my way up yesterday - The whole of the track up is  strewn like  marked out like a paper chase with German Gas Masks that the Fritzs have discarded. 
 Had a pretty wakeful  night  time last night there was a roaring bombardment of our guns on our sector - & then although I thought we were pretty well out of range here he started put a steady shell every few minutes  with  into Corbie the town that adjoins this the other side of the river & being a bright moonlight night of course Fritz was dropping 
