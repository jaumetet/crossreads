  
 is as steamy as a  washhouse  laundry. 
 Nov 12  We had a run  
 Good news has been coming over the wireless apace these last few days. We have been waiting for the inevitable & last night came the rumour & it was confirmed this morning  that  on the noticeboard that Germany had surrendered & signed our armistice terms,  There was  
 In 4 days time we should reach Fremantle. 
  Amen  
 [Transcriber's notes: Pg 3: Bruce Bairnsfather, 1887-1959, was a famous English humorist and official cartoonist of World War I] Baghdad spelt Bagdade Brucamps spelt Brucamp Dikkebus spelt Dickebush Ebblinghem spelt Eblingham Fouilloy sometimes spelt Foilloy Halles spelt Halle (El) Qantara spelt Kantara or Kantarah Taranto spelt Tarante Villers-Bretonneux spelt Villers-Brettoneux Wallon-Cappel spelt Wallon-Capel] 
 [Transcribed by Barbara Manchester for the State Library of New South Wales] 
 