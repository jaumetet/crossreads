 a6680056.html 
 1918   May 
 9 THUR  Glorious day. Moving 
 camp to the right. 
 Fixing up 
 home. 
 Heavy shelling at night 
 10 FRI  Cold dull day. Went into 
 Hondeghem. morn & aft. Met Ger- 
 maine. Fairly quiet. Rec 
 letters from Amy & Amy Mac. 
 Writing at night 
 11 SAT  Fine day  Posted letters 
 to L. Berry (Else P. & Gladys) 
 & Amy & (Celie Halsall.) 
 Rec. letter from Tense. 
 Went to Bavinchove 
 at night saw Blanche 
 Germaine had a  
 pleasant evening. home 
 11.30 PM  Heavy fog & 
 mist. Quiet night 