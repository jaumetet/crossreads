 a6680040.html 
 1918   March 
 14 THUR  Arr. London. 5.30. crossed to 
 Victoria St. Met Sgt. Dowe. Left 
 8 am. arr Dover 10.30 had 
 dinner sailed 2.30 PM arr Calais 
 4.30 & marched to No 6 Rest Camp 
 tea & issue of Blankets. turned 
 in early. 
 15 FRI  Reveille 6.am. Bft 6.30 moved 
 off 7.30 am issued with dinner & tea. 
 Left Calais 4 am arr Ballieul 1.15 pm. 
 Went out to Au Progres had bon time. 
 Posted PC 's  to Amy, Celia May Lizzie Amy Mc 
 & Capt Mooney. 
 16 SAT  Left  Au Progr  es 10.45. am.  
  arrived Battery 2.30 PM. Glorious  
  day. Guns very active on  
  both sides. Many new  
  arrivals at Battery. Fixing  
  things up. Rec. letters  
  from Amy & photos. & Tense  
  Gladys C & photos, Else Parker   
  & card from Julie & P.C.  
  Billy Williams from England.  
   
   