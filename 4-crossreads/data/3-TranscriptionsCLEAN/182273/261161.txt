 a6680090.html 
 1918   September 
 5 THUR  Posted 'Aussie' to May Webb. 
 Arrived back about 6.15 am 
 Fine day. very warm. Battery 
 manouvres in morning 
 2 Bde Sports in aft. Writing at 
 night. Fresh successes everywhere 
 Quiet 
 6 FRI  Fine day. Bty manouvres 
 in morning. Packing up for 
 moving off tomorrow. Quiet 
 Posted letter to R G. Clarke. 
 Received letters from Amy & Zelia 
 7 SAT  Fine day. Warm. Moved 
 off at 9 am. via Bray to 
 bivouack near Zusanne. Fixing 
 up camp. ' Fritz ' reported 
 to be evacuating. Received 
 letters from Amy Mac & 
 Celie. Writing at night. 
 Very quiet. 