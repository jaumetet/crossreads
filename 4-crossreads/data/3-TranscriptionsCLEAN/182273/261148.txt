 a6680077.html 
 July   1918 
 21 SUN  Fine day. cool. very 
 quiet. Forward wagon 
 line est: guns 
 to move forward. Major came 
 down. Meteren taken yes- 
 terday. Very quiet at night 
 Swim. 
 22 MON  Fine day. blowing hard 
 Amm. going up. Preparing  
 new gun positions. French 
 offensive going well. 
 Quiet at night. 
 23 TUES  Raining all day. 
 Amm. going up. Quiet in 
 day. Quick Dick active 
 at night. Bombs at night 
 24 WED  Fine day. blowing. Rec. 
 letter from Julie. very quiet 
 Preparing for stunt. Sent 
 pad up to J.W.C. Posted 
 Service Cards to. J.M.S. & G.E.B. 
 Bombs. 