 a6680046.html 
 1918   April 
 4 THUR  Dull day. raining on & off 
 Balance of Bty. relieved 
 in aft. Walked down to W. Line. 
 Called at 2  nd   Bdge met W. Flintoff 
 & some of the old boys. arr: W.L.7.30 
 Finished old job. 
 5 FRI  Dull day & raining.  Posted . 
 Had bath & change in morn. 
 Out on byke in aft. called at 
 D.A. La  Clytte at night. Rec: 
 letter & photo Amy Mc & letter B.W. 
 6 SAT  Rec: letter from Tense (Naours) 
 Fine day. Writing. Posted 
 letters to Amy (& May & Celie). 
 Fixing up for move. Very 
 quiet. Rec. letter from 
 Tom (U.S.A.) playing  
 cards at night 