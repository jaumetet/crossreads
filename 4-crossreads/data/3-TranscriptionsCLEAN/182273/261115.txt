 a6680044.html 
 1918   March 
 28 THUR  Burial service on R.G.A. men in dugout. 
 Fine day. Digging all 
 day. but no luck. Major S. 
 came back. Rec. letter  from  
 May Webb. Quiet.  Sep  Slept 
 near lock. 
 29 GOOD FRI  Raining on & off. Digging 
 all morning. Recovered nearly  
 all our possessions. Dugout 
 badly blown in. Rumour out 
 re going to Somme  C. Dowe sent  things from W.L. 
 30 SAT  Raining on & off all 
 day. Guns fairly active 
 on both sides. Battery 
 in action best part 
 of the night. quiet night 
 Writing letter in 
 aft. 
   