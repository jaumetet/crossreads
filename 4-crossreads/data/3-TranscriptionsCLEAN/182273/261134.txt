 a6680063.html 
 June   1918 
 2 SUN  Warm. 
 Fine day. Very quiet 
 Posted letter to 
 Else Parker. Preparing for 
 stunt tonight. Went to 
 Staple at night. Quiet. 
 3 MON  Good success in 'hopover' 
 250 Huns & m. guns 
 mortars etc. Fine day. cool 
 Amm: going up. Quiet. 
 Stayed in at night. 
 4 TUES  Fine day. Cool. Amm. 
 going up. Out on byke in 
 aft. Very quiet no bombs. 
 Playing bridge at night 
 5 WED  Fine day. Cool. Bath 
 Washing. Posted letter to Gladys 
 Rec. letter from Julie. Quiet 
 Amm. going up. Writing 