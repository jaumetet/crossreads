  
 Sept 1st 1916 Left Camp near Mission Station at 6. a.m, trekked through hills leading our horses till 1 p.m some very stiff hills to climb Trekking tomorrow at 5. a.m. Germans evidently going South. Night in Camp 
 2-9.16 Stand to arms at 5 am till 6.am. offsaddle at 6. a.m. waiting for orders Five different columns of ours supposed to be in vicinity. 
 Monogora fallen 4 or 5 days ago. News of Roumania having joined the allies. 10.am Trekked off to the hills again per foot to take another Mission Station climbed some awful hills arrived at the place we camped  3  4. p.m. Stayed for night. 4th 5 aM on our left 2nd and other regiments acting in conjunction on the other wing. Picket at night. 
 3.9-16 Reveille at 5.30 am move off in direction of  Mission  Farm & Hospital. more hills. they seem endless and bigger each day. camp for a while at 12 30 within mile or so of Mission for snack. Germans supposed to be in possession of hills on other side Station (I have my doubts) 
 Chingara "Dorp". Reached  Station  Farm afternoon and camped for night Germans Gone 
 Plenty of entrenchments  Kaffirs looted the Germans Houses before leaving furniture all broken up. picket. 
 4-9-16 Colonel and Staff arrive on scene "everything safe". Shift camp further up the hill might in camp. 
 5.9.16 Another patrol on foot Sive an Jinmy Fraser Banks and self left in camp. Time expired for rations and no hope of getting any more 
 Shift camp at 3 pm further on into neck and camp for night. plenty dead cattle all along the road. G's not far ahead. big guns going in distance 
 6. Warned for patrol mounted 10 am. patrolled for about 5 miles. passed 2nd Reg camped at a Farm House they had burned. 
 Scout fired on about 2 miles ahead of us by G. rear guard camped and sent out a picket of D. Sqd till 6 pm. returned to camp for night. 
