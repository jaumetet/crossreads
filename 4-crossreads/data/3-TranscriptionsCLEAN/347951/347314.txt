  
 13-11-16 Orders to shift camp at 7 am to day. 11 pm now no sign of waggons moved camp at 5. pm about a mile and half E of Morogoro above aviation camp better camp than the last close to water 
 14th Nov Wrote to Vi and Aunt. 
 15 Received letter from Addie and one from Harry Butler.  Wrote  
 16 Nov. Unfit men left for Daresalam 7. a.m. to day. 200  hundred  observation cases expected in from Kilossa 
 17th Wrote to Vi. Ad. Harry. B. 
 18 Visited Dennis went for a ride up to old Detail camp in Phaeton 
 19 First train from Dar essalaam to Morogoro arrived on the 17th visited Dennis had lunch. 
 20 usual Routine 
 21st walked up the hill trying to purchase poultry or eggs no luck 
 22 Visited Dennis. Met Tim Thompson in hospital Gen Britts in Town 
 23/11/16 24-25-26 Usual Routine Camp 
 27 Camp Routine Y.MC.A sent up a few necessaries soap etc 
 28-11-16 News of surrender of 63 Germans 200 askaris one Howitzer three machine Guns somewhere in the Iringa District. 
 29 All saddles and bridles to be handed in tomorrow 
 30/11/16 Routine as usual attack of fever again 
 1.12.16 Routine still fever 
 2-3-4 Authentic news of Gen Northeys Force having had an encounter with Germans somewhere South G. N. force buried 71 Germans 300 askaris 500 prisoners - result of months work. Good Work. 
 5-6 Routine as usual 
 7th News of 3 of our motor cars being captured and burned on the Morogoro Kisaki Kisaki Rd 
