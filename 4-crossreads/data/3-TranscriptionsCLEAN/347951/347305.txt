  
 up with Britts column. Enslins Horse 
 Rations 3 days overdue. Trek on 5 miles into hills south of Monogora. Germans somewhere in hills Enslins had several encounters previous to our arrival say Germans destroyed [indecipherable] Ammunition in hills and abandoned 9 pounder. Tremendous range of hills like Kilimanjaro minus the Snow anticipate plenty of climbing Church service on arrival in camp. Anticipations realised re climbing. 
 28-8 16 Trek on foot 6. a.m. through mountains trekking all day. awful climbing. Horses come to meet us at night back to camp. 
 29-8-16 Continue Hill climbing all day trying to get to German Mission Station. Struck Mission at 2.p.m after a hard days climb picket at night raining, good soaking no rations for 6 or 7 days once issued yesterday no time to cook. ("MGeta") 
 30-8-16 Shift camp a few miles beyond mission station still raining. to days rations on the road for us. Lord knows when we will get them ("MGeta" Mission Station) 
 German Naval officers at MGeta Mission sick from the Koeningsburg. "Monogora" supposed to have been taken within the last day or so General Northey reported to only have taken 'Irangi' lately, reported taken month ago. All the Generals having a consultation back at last camp (fine night) Stable guard 
 31-8-16. Rations arrived from the 2nd Reg. the only decent issue we have had since we left the line "Davy Bremmer" wounded at the fire 
 Moving camp some time to day busy cooking all the morning still no mail. 3 of the 9th wounded and two killed yesterday on ahead 
 Rations of Bisciuts, Rice, Meal Tobacco, Bacon & Jam. Red Letter day. Picket on top of hill till 12 pm relieved by dismounted men Hell of a climb both up and down. Davy Bremmer sent to Mission Station till cured 
