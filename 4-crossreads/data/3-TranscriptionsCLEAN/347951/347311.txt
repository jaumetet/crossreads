  
 30/9/16 Trekked at daybreak about 6 miles and camped for breakfast trekked on again about 12 miles feeling rotten raining camp at dark. 
 Sunday 1-10-16 Reveille 5. am Still feeling rotten moved at 11 am about a mile handed over sound horses to go back. Sive went to Hospital Rations for two days bisciuts 12 coffee Sugar 2 spoons each 1/2 lb meat 
 1.10.16 40 men got a lift in towards Morogora per wagon 
 2-10-16 Camping same place probably here for some time waiting for transport Nearly all the remainder of men walk on towards river given as 12 miles probably 15 or 20 (not me) to sick Rations again for 4 days biscuits 12 coffee sugar 2 s. fresh meat 
 3.10/16 Spent a horrible night no sleep and dreadful head. Hope we soon get transport and get out Joe Ashton went to Hospital yesterday 
 4th 10/16 Still feeling very rotten. sent to Hospital 4 pm thank the Lord Roof over us at last 
 5th 6th Still Hospital Left for Morogoro per Motor Car arrived late at night via  Kisia? [Marked X with corrected spelling at end of page.] 30 miles per car and 15 per rail motor rail reached the hospital about 9 p.m. "bed" the first since Jan. 
 7th Hospital - Light diet, and Quinine 3 times per day. Saw "Sive" from the Convalacent Camp. feeling better after night in bed 
 8th Sunday. Hospital feeling a lot better Dr. promised I can rejoin Sive at Convalescent Camp. ordered to go to Remount Camp no transport 
 MKissia X 
