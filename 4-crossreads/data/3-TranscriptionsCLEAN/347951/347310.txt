  
 no relief. Germans make a half hearted attack on the right. Rations flour, rice 1 cup each sugar 1 spoon tobacco-tin of bully 2 men 
 23.-9-16 Relieved at 9. am. to go back to camp for a day or so Quiet day 
 Rheumatism getting bad again rum issued night in camp 
 24 Sunday. In camp saw the Doctor for Rheumatics ordered back to rest camp 2 miles in rear rejoined Sive. Rumours of whole Reg. being sent back for a rest for a month and to be re equipped Everybody in rags and barefooted. Rain still hanging about. rest camp full of poor devils with fever fearful country plenty big game Elephants Hippos crocodiles etc. 
 25.9.16. Still in rest camp. Georg Cottinghams body found in the long grass partly burned (2 days ago) No doubt about poor old Sam Harris death; Issue of rations 1 cup flour. 3 bisciuts 1/2 cup rice sugar 3/4 cup 3 men. 1 box matches 8th lb tobacco to last for four days. Still in rest camp. 
 26-9-16 Sick Parade rest camp full of sick men / still in rest camp 
 Rumour of 1st Brigade going to Morogora for rest:- Patrol of 3rd Reg gone to Van Deventer about 30 miles 
 27 No Rations arrived yet. 1 Biscuit 2 spoons coffe & Sugar "Darawa" Rest Camp 
 28 Rest Camp 7 Biscuits 2 Spoons Coffee 2 sugar. 1/4 fresh meat rations. Set of men going to Morogora from Hospital sick on waggons 
 29 Left Camp per horse for rest camp direction of Monogora Trekked 12 miles camped for night Rations 2 days 12 Biscuits Coffee Sugar 2 Spoons 
