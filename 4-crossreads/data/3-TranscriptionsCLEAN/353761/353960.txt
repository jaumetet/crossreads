  
 (29) explosion of a secret mine long hidden in the cellars beneath by the cunning Hun, and fired by clock work. As we pass this historical spot on this early morning march, there is with us an officer of the tunnellers who was instrumental in saving several men from the ruined cellars after the explosion. To do this he and a party of sappers worked inceasingly for four days and nights. There is a typical town square fronting the site of the vanished building, and in it a stone pedestal almost undamaged and still bearing in gold letters, the name of a famous French general who in 1872 beat the Prussians 