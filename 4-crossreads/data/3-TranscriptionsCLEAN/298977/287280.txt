  
 Bulford, 20th January 1917. 
 Dear Mrs. Richards, 
 It's quite beyond me to write and express how sorry I am about Roy.  He was my chum and it's something I always will be really proud of. 
 He had a duty to do and he did it as a gentleman, and I've thought how great it must have been for him to hear "Well done", because it was well done and he did hear.  He is a great loss to you, only you know how great, but it must be great to have been the Mother of one who has given all he could so nobly. 
 I only saw the news in the "British Australasian" two days ago and yesterday heard direct from Mother. 
 I send you all my deepest sympathy. 
 With love, (Sgd) Charlie Witney 
