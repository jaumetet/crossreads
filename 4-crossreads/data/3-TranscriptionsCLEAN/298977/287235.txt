
France.6th June 1916.
My dear Mother,
I received three parcels yesterday, so things are looking up.  One was the Vest, which is more than handy just now, as I am without an Overcoat.  The next one was from Ruby with a couple of pairs of socks, writing pad etc., and the other one as far as I can make out, as there was no note with it, was from Mrs. Moore.  Please thank her very much for me, it was very good of her to send it.
Things are as per usual here.  The fine weather has broken and we have had a spell of rain and cold, but it looks as if it is going to clear off again.
There was a mail in some days ago, but a very unsatisfactory one.  I had a letter from Father, but it was the only one from home.
We have been kept going some lately and I have no time to sit down and try and think out a letter just at present, but will write again in a few days time.
Am still O.K. and going strong.
Kindest regards to everyone round about and love to all at home.
Your loving Son,Roy.
5th Aust Machine Gun Coy.,A.I.F.France.
