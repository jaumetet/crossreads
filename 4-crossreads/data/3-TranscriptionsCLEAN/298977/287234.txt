  
 -2- 1st June 1916. 
 Les Stewart must have come out of his shell all right.  I can hardly imagine him trying to put one over on the Ticket Collector.  Great must have been the fall thereof.  His no-breakfast scheme will stand him in good stead in the Army, but this time it won't be a fad - it sometimes is a necessity. 
 Sorry to hear Mr. Doyle's eyes are troubling him again.  They seem to be giving him a rough time. 
 Glad to hear another of Mr. Cizzio's brothers has joined up and certainly hope his luck is better than his Brother's.  Patriotism affects people in different ways, but I certainly would not care to have the consciences of some of those chaps whose only job is to keep the post in front of the Pubs from falling down and obstructing the traffic.  There's traffic here of a different sort and plenty of it at that, that needs obstructing. 
 Got a Bulletin from you this mail also.  Its always looked for eagerly and read from the red page to the last advertisement.  We can generally get a good argument or two out of it and a lot of the Cartoons etc., give glory to the walls of our various dug-outs. 
 Do you still collect Magazines from America.  A few sent over every mail would be very much appreciated.  Reading matter is at a very high premium here. 
 Well this is finish.  Weather still great and am feeling A.I. 
 Kind regards to all in the Office and love to all at home. 
 Your loving Son Roy. 
 5th Aust Machine Gun Coy., A.I.F.  France.  
