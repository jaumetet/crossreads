  
 -2- 30th October 1915 
 July  last.  How did they get on.  Very pleased I missed them. 
 Love to all at home, Your affectionate Son, Pte 2380 Roy Richards. 
