  
 13 And swaying above us oscillating slings of ammunition, and cases of bully beef and biscuits etc. 
 Here we are in the second shed 
 Horses! 
 Yes horses for France 
 All kinds of horses, they have just come in from the country by rail, and will soon be shipped for duty at the Front 
 They are wanted for the guns, for general service for the Red Cross, for munitions.  In a word for the numberless lumber 