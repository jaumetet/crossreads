  
 6 Many Australian soldiers are there in groups and in twos and threes 
 The French soldier looks quite part of the place. 
 He fits 
 But not so the Australian, he looks out of place, the product of a new world. Too new and smart to suit 
 And when a company of armed Australians march through led by a brass band their bright presence seems quite an anomaly. 
 In the estaminets (estaminay) there is a little corner counter, a number of small tables and a fire stove.  Caffe is always hot on the stove, also there are generally large pots there, for much of the kitchen cooking is done in the saloon; (A matter of making best use of the heat) As well as drinks, good food can be had in these places.  French and Australian soldiers fill these comfortable warm saloons and are served by bonny French lasses 
 Often there is a French billiard table at the far end, this has no pockets.  The play is to make cannons only. 
 Sometimes while taking my caffe and rum I amuse myself by making sketches of characteristic of French men. 
 They are greatly pleased at this 
 I am scribbling this simple description as it may be of interest to you all. 
 On the journey from where we were to where we are it has been a series of autumn pictures 
 Many of great beauty, all of interest. 
 It is late autumn now, and still in the latters of golden wreck there is fine effect. 
 Nature seems, in fear of outrageous winter, to throws all her treasure to the earth. 
 There is much I wish to write of but cannot just yet. 