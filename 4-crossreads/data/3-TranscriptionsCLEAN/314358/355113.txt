  
 Wareham 28th Decm 1916 Dear Beatrice Let me give you a rapid sketch of Ypres as seen by me and an outline of my life during the few interesting days which it was my good fortunes to spend in so notable a point of the western front. 
 The wrecked convent school in which my section was billeted was a good example of the broken shelter which troops were compelled to put up with, and in most cases, glad to accept. 
 As I have told you, in a previous letter, we used a spacious old hall as a kitchen  This presented a splendidly weird scene at night. 
 Hugh beams of timber were dragged in from the wreckage of other parts of the building add heaped on a fire which was simple made on the tiled floor and kept burning all night 