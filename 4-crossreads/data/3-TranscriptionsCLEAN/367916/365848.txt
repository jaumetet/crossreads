  
 15.1.19 
 Dear Mother & Father, Still going along happily. Dreadful time the last 2 days. The school of education here has started & there has been a tremendous lot to be done from my point of view. Things are now pretty well smoothed out. 
 Have had to build classrooms baths cookhouses butchers shops. Put in a water supply on a bore with pump-engine & elevated tank, install a new electric lighting set put in stoves get furniture & in my spare time fix up little details like tennis courts, put out fires in factories a large place about 30 yards from where Phyllis lives which was completely gutted, and interview 60 or 70 students. These reams & reams of correspondence so things have been doing 
 Anyway I have got something to show for the timber & it all looks pretty right now. Am running a free coffee stall a few odd concerts tons of football boxing golf ping pong quoits baseball. The only thing we dont do is to keep silk worms. 
 Got some good chaps here now & the school should be a really good show. 
 Got a ripping parcel from Mrs. Pope today also one from Mother [indecipherable], latter contains 3 bars welcome Nugget. 
 Best Love Your affectionate son C.R. Lucas 
