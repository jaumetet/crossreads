  
 B.E.F. 3.5.18. 
 Dear Mr. Justice Ferguson, I had a mail in from home a few days back & the [Paler] mentioned he'd seen you so I thought I'd drop you a line. 
 It is now, thank Heaven, the 23rd hour of my 24 on duty at an observation post which is in a very cheering spot, viz the undertaker's house just outside the - graveyard, & at this moment I have come down to cellar because I can see nothing for ground mist. 
 Yesterday afternoon I had much pleasure in putting a battery onto a pill box around which were many Hun working parties & believe me it was pleasing to see not all of them ran away. I've grown bloodthirsty on Fitz's lately, & so is everyone; the general attitude being to curse him when he strafes us & then do one's uttermost to kill a Fritz someway. 
 In this part of the front we held him & he hasn't fancied another try & from what I know about the locality he'll have a rough time if he does. This morning early he was putting gas across here but not enough to worry & a new stint of his also, balloons 
