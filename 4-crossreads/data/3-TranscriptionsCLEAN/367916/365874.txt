  
 The Royal Club for Officers Beyond the Seas 11.7.18 
 Dear Mother & Father, Finished our trip down The Thames yesterday. What a glorious time we had. Five beautiful hot days plenty of swimming a good boat good places for meals & beds & everything as nice as it could be. Yesterday morning it rained pretty hard that was our sixth & last day so we just got into one of the locks with a steamer tied on behind then put on our coats & got towed so the rain did not matter much. 
 I am not at all keen on being back in London after the lazy open air life on the river but as it is raining hard today it is just as well we got finished when we did. 
 I received a parcel of socks. They were very nice & were especially welcome at the time as I was wearing slacks & was just going to buy a new pair. I seldom use slacks (trousers) but when I do I always have to have a pair of brown socks - 
 I am going back to France sometime next week. I report on 17th in London & hope to be able to go straight across 
