
The Royal Club for OfficersBeyond the Seas14.7.18
Dear Mother & FatherSunday morning raining and horrible so decided to clear up all my letters
I am not sure which of yours I have answered anyway I have here letters from Mother 25 April 1st 3 5 May & Father 7th May. I have just been enjoying reading them through again. Very glad to hear that the trip to [indecipherable] came off at last. It must have done you both a lot of good.
I got a very nice letter from Col Goddard the other day rather decent of him to write He is still in France & going strong. Have been to 3 shows at the theatre on 3 successive nights all complimentary tickets. It is possible to get tickets almost any night now for nothing if you have to pay for them they cost 13/6 each.
I dont suppose you will be very pleased to hear that I am going back to France next wednesday. I have had a holly good time over here since I began to recover from the gas & as it is just on 3 months since I got it it is just about time I got back again. I found a lot of Australian stamps in my kit Have had them ever since I left home.
[The following text is written in the left-hand margin][indecipherable] sent these stamps [indecipherable] to us
