  
 5 our little bit of the war. 
 Things are looking pretty good just now, and there seems some reason to hope that by the time this reaches you we shall have begun to take the offensive. 
 The French have just begun their offensive south of Soissons with the result so far of 15000 prisoners and 200 guns. Their cavalry are out, and it looks as if they were rolling up the right flank of the enemy push round Rheims. It is possible indeed that with a million 
