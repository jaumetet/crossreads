  
 April 16 1918 
 My dear Judge Ferguson I have just chanced on to your Red Cross appeal on April 16. It is, if I may say so, admirable. Nothing is harder to write: it should have won your fund a million. 
 And what a very beautiful piece of prose is that of [indecipherable] in the "SMH" of the same day. 
 Down here for a few days' work. Very hot everywhere & in Jordan Valley it is hell. Often up to 120: a lot of primary malaria & much other sickness 
 Kindest regards to you all Harry [Gilbert] 
 