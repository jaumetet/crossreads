  
  25/11/15  The 2nd Aust. Div has arranged for the following Signals by coloured rockets to indicate hostile attack on various portions of their lines AAA Russell's Top - 3 white rockets Popes - Red, white, red rockets Quinns - 3 green rockets Courtneys to D16 incl. White red white D16 to B8 inclusive green white green Lone Pine 3 reds 
 [Separate Item] Received from Capt Weir O.C. No1 Subsection 12 double Cylinder grenades W H James RSM 1st A LHR 13-12-15 
