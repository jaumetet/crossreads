  
 [A copy of the despatch from Holmes to the Minister of Defence can be found at MLMSS 15 Box 2 Folder 3 Digital Order no. a8093 pages 75-79] 
                   H.M.A.S. "Berrima" 
                        Rabaul, 
                          26th September 1914. 
 The Vice-Admiral, 
   Commanding H.M.A. Fleet 
     Simpsonhafen:  
 Sir; 
 Referring to my interview with you on board the Flagship at sea on 23rd instant, in regard to the wireless message from the Minister for Defence as to the terms of capitulation agreed to by me with the Ex-Governor of German New Guinea, I have the honour to forward herewith for your information, a copy of a despatch I have prepared for transmission to the Minister by the first mail which leaves for Australia. 
 I have the honour to be 
              Sir, 
 Your obedient servant, 
 (sgd.) William Holmes, Colonel 
                             Administrator. 
