  
 From - Brigadier. 
 To - Rear-Admiral C. 
  12th Septr. '14:  
 Have directed Beresford withdraw from wireless and move all his force Herbertshohe as garrison.  His strength will be four Naval Companies and two Machine Guns.  Can "Warrego" take those at present at Kaba Kaul base to Herbertshohe on her way to harbour to relieve Colonel Watson's Infantry, whom I am re-embarking "Berrima". Will then move to Rabaul, carry out original plan.  Beresford instructed treat with Governor if necessary.  Do not require more assistance from you.  Will advise Governor as to my commanding on shore. Beresford already instructed remove instruments from wireless, but will definitely ascertain shortest time to reinstate this station. 
