  
 Naval Signal 
 From - Herbertshohe 
 To - Berrima 
 Date - 21/9/14 
 Time - 2.45 
 Brigade Major to Brigadier 
 In August Dr. Haber ordered Wilhelms-Hafen reservists concentrate here.  Message just received from Dr. Haber that an officer & approximately 40 men has landed at Weberhafen from a transport & marched yesterday morning to Panlil. Haber sends this morning notifying cessation of hostilities & ordering them to surrender themselves Rabaul or Herbertshohe 
