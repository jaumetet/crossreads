  
 Naval Signal 
 Ship of Origin - OC Infantry 
 Date - 12/9/14 
 Time Received or Sent 12.2 AM 
 To Brigadier 
 I propose to move at 5am tomorrow morning coast road until contact with Beresfords party.  I then intend to move south with my left flank on road from Kapa Kaul to supposed position of Wireless Station after shelling ceases.  Half Coy will move along road which branches from coast road 1,100 yds East of Wunapope.  Half Coy to occupy position at cross roads due south of Herbertshohe one mile from Grunapur & to patrol to Grunapur.  Near here today 6 men were taken on suspicion.  The 12 pdr to move to KabaKaul cross roads & thence along road on my left flank. reference map issued by OC intelligence this morning. 
 From - Watson 
 I concur in your proposals    
