  
 Naval Signal 
 From- Encounter Colonel Watson 
 To- Berrima Brigadier 
 Date- 14/9/14 
 Time-7.20 
 Encounter. Begins. Ordering passage for marching on Toma at 5 AM with 4 companies & machine guns. Land your machine guns by 4.45 AM complete. Sketch captured showing enemy occupying position. It is right on Matawta to bend of road near Wunatora with post at Gire Gire. Will you by vigourously shelling ridge & plantation sweep position on map at daylight. My troops will not move south from coast roads until shelling ceases.045. 
