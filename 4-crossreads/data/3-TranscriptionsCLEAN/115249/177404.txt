  
 Naval Signal 
 From - Brigadier 
 To - Flag 
 Date - 13.9.14 
 Time - 7.30 am 
 Commander Beresford was instructed to forward a report to me early yesterday, this has not been received, a reminder has been sent this morning. The casualties as far as known are 
 Killed Capt Pockley - Lt Comdr Elwell - A.B. [indecipherable] William[s] AB Courtney, AB Moffat,  and another .  Wounded Lt Bowen AB Sullivan I, AB Street W, and AB.Tonks J.  One of the last three mentioned died soon afterwards, his name is not available at present. 
