  
 A.G., A.I.F.  1/41 dated 2.7.18 
 Officer Commanding 1st Squadron A.F.C. 
 The attached copies of letters are forwarded in confirmation of my 1/41 dated 11/3/1918. 
 (Signed) E.G. Chauvel, Lieut. General, Commanding A.I.F. in Egypt. 
 A.A.G., A.I.F., same reference & date. 
 The report of the G.O.C. Palestine Brigade R.A.F. and the remarks in your covering letter G.S. 74 of 24/7/18, on the 1st Squadron Australian Flying Corps, are extremely gratifying to me, and will be forwarded by me to the G.O.C., A.I.F. so that the Head quarters of the Australian Imperial Forces may be appraised of the high standard which this Squadron has attained. 
 (Signed) H.G. Chauvel, Lieut. General Commanding A.I.F. in Egypt. 