  
 soon came back & went to bed again No one was hurt a few had narrow escapes. Have heard since it was a Hun raide & we captured 120 prisoners. 
 Saturday 2nd March Went out on the job until about 9 & got some stuff together a cold windy day with a little snow 
 Sunday 3rd March Same as yesterday 