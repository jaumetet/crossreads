  
 Of war so far. This is up to Sunday night 
 Monday 28th January Had a very short march this morning - without Packs Nothing in afternoon Can hear the guns from here & saw some shell bursts around a plane this morning & some to night behind us. They were a good distance away. 
 Tuesday 29th January Left Castre this morning at 9.30 & marched to the Battalion about 12 to 14 miles. Got here about 