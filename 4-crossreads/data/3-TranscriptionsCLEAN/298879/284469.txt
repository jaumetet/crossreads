  
 November 1915 Thursday 18 Fine Trenches drying nicely. Boys all busy making dugouts waterproof. My dugout escaped the wetness pretty well. 
 Friday 19 Cool wind blowing. Did nothing all day. Our anti-aircraft gun nearly hit an enemy aeroplane today. 