  
 October 1915 Friday 15 Breezy still. Batt. on a route march. N.Z. band playing in Y.M.C.A. tent again 
 Saturday 16 Dull.    Flag waving again. We took the lamps out last night for a little practice. The Div. canteen is in full swing. They have been tremendously busy since opening. 