  
 March 1915 Monday 1 Still feeling ill. Measles do get a hold of a chap. Taken up to Mena House in the afternoon. All the patients out of the hospital to be shifted also as the Imperial Authorities have taken it over 
 Tuesday 2 Temperature still high but measles almost disappeared . Passed dreary day. Sleep badly at night. No Appetite. 