  
 Notes about Cairo 1 People 2 Traffic (a) vehicles trams motors and trains 3 Business 4 Buildings 5 History of Cairo's Surroundings 
 [1914] August 21st Passed the doctor to join the Exp. Force to help England at W. Maitland Drill Hall 
 Aug. 24 Left Maitland by Taree Mail & joined the 2nd Batt at Randwick Racecourse 3rd      Left Randwick & came to Kensington Racecourse. 
 Sept 25th B. Coy went on board the S S Suffolk 
 Sept 26th Arrangements made to go on board cancelled. Great disappointment 
 Sun 18th Oct. Embarked on board the S S Suffolk A23 transport about 9AM.  Steamed through the Heads about 4PM the same day. The sea was fairly rough. Food on board clean & enough to eat. Hammocks comfortable to sleep in 