  
 July 1915 
 Thursday 1 Strong cool wind blowing. The sun shines warmly and not a cloud is to be seen. It is one of those weary beautiful days where one seems lifted into another world. The waves gently lap, lap against the shore and I lie under a shady tree where the music of the sea gradually wafts one into a peaceful sleep. 
 Friday 2 Dull. Cool wind blowing For the want of something to write -- 
  "To the Belgians"  O race that Caesar knew That won stern Roman praise What land not envies you The laurel of these days Lawrence Binyon 