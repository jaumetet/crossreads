  
 Guns moved forward again last night.  A number of tanks passed our wagon line, late last night.  Enemy shelling heavily.  Bomb dropping eased off a little.  Our search lights well forward.  Saw an aeroplane come down in flames.  Weather is very hot for this country. 
 15-8-1918 Eleven horses killed and nine wounded in third Battery last night with bombs.  Our lines about 20 yards away never got a scratch. 
 16-8-1918 Reveille 2-30 a.m.  Left Rozieres at 5 a.m.  Arrived at Aubigney 10-30 a.m.  Camped on bank of river.  First good wash for a week. 
 17-8-1918 Generals Glasgow and Anderson inspecting.  Nothing doing only cleaning up and reorganising.  All of the Australian Artillery camped here.  Weather fine. 
 18-8-1918, Sunday Morning showery, evening fine.  Troops permitted to bathe in river.  Things quiet. 