  
 30 March, Sunday 
 F. Some snow storms. 
 Recd letter from M.R.L. (L/L). Over 130 admiss, & I work until 4 pm. Wrote until 6.15 & D & Lord & I walk all around & to Bulford. Meet Cpl Niuttall & we have supper at Garmain Canteen & listen to Mechanical Music. Walk around to Sling Y.M.C.A. Home & sit by fire until 10.30. Have long talks about the good old times. 
 Sent Dane's letter & Blighty to Auntie. 
 31 March, Monday 
 F. but tries to snow. 
 I go to English Class & am at Dance 7 pm. I play until 9. I get astray in Tango. Come Home & write 4 pages & post letters at 10 pm 30. An awful L - letter. Bill Trouter comes down to sleep & snores. D. & D. go back to London. Matt comes over. 
 Recd letter from Miss Scroggins & Ellis also wire. Lights out starts at 10.30 pm. 
 Wrote M.R.L. 