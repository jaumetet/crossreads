  
 22 March, Saturday 
 F. We go to look for Venus but we can't fix the telescope only see bright lights. 
 We go to Ramsey & to Pictures. See a Lone drama. Tea at Angalie go shopping & pay debts. Then we go to Mr & Mrs Gibsons. Home by Lezarpe Road meal nice & his bit & chatty all the way. I rush to Glen & meet M at P.O. We walk from the Bridge home & I relate incidents of my own Home life. Supper & cards until bed-time & we go to Room together & I see N.C.L. 
 23 March, Sunday 
 F. Lovely Day. 
 M & I sit by fire & talk etc. we are both very lively & happy especially at Tea. We sit & yarn by the fire at night & I am very tired. We have music & song & all go to bed at 11 pm. We look over spare stuff in my room. Auntie gives me a ring, also one for Ellis. I engage myself to be faithful for ever & [indecipherable]. 
 I pack my bag & give M my old Pocket Book. 