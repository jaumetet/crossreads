  
 26 June, Thursday 
 F. I begain work again in earnest & the boat's rolling makes me & my tried before I am finished my work. Work all afternoon. I go to Poetry lesson. Tell Sister to go to B & get on B-------. I try to write to Auntie but I am none too bright so I write 3 pages & get off to bed in Hospital on seat. 
 27 June, Friday 
 W. I watch a gale blowing up. We get it very windy but choppy & swell very slight. I get very tired hopping about all the same. I go to Book Keeping but Branch accounts are tricky. 
 Gale blowing worse. I go to bed in top bunk in Hosp. 