  
 24 June, Tuesday 
 F. Kloof St. Sent Boa & Feather to Aunt. 
 Jack, Art, Cliff & I go to Soldiers Welcome & Anzac Lounge also buy P.C's etc at Tukney's in Dock road. We have morning tea at A.L. & Mrs Norton takes 4 of us out to Observatory in train. We have tea again & lovely dinner. Go to C. Rhodes Home & then to this monument cost 80,000. I am very tired in agony with tooth ache, we see menagerie & wild native beasts. Back to gardens. Dinner & music meet Dr Wood (Gavin works there) Supper & Photos shown. Tram & motor back to ship. 
 WROTE Auntie 2 & M 8 
 25 June, Wednesday 
 F. I write to M.R.L. & J.C. throws letter in water. I write another & give to wharfinger to post. A boat comes in with 1000 fritz's on board. We operate all morning. I am in awful agony with tooth & dentist pulls it out for me. I gain instant relief & I go to bed all afternoon & again at 7 pm. We have pudding, lollies & fruit for tea. We sail at 2 pm & I see the last of the pinnacle about 5 pm. 
 Wrote M.R.L. 9 