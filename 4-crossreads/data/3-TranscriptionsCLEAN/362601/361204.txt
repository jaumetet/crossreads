  
 15 May, Thursday 
 F. Up at 4.30 am & I declare that I am not awake when called. Say good bye. It is very very bad touching. I arr Doyles' but should have caught 9 train as boat didn't leave until 10.30. I walk around to Port Skillion & write letters. Sleep on boat nearly all way. Catch 5.20 train. Dinner on train have good trip & grand meal. Stay at 10 Waterloo Rd. taken there by Y.M.C.A. girl. Fire somewhere near at night but I sleep well. 
 Wrote M.R.L. (2), Auntie Annie (2), Dad. 
 16 May, Friday 
 F. Leave London 6.10 am. Front 11 am. Recd 2 letters from Home. Lottie, Marion Clarke, M.R.L. (2), M Mckinnon. I am very lonely. Meet Billy & go by Y.M.C.A. to write. Go pictures after tea. I have to wangle for bed & get down on hard floor, & go to sleep before the mob gets in. I sleep all night but my neck aches next morning. 
 Wrote M.R.L. 