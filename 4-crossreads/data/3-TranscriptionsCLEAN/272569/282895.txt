
Frid. 28-7-'15Camp 1 p.m. & marched to Vadencourt (near Contay) slept in huts. March cruel on men whose feet have grown tender in trenches.
Sat. 29-7-'153.45 am. Reveille. Off 5.15 am. Another cruel forced march to Lavicogne beyond Talmas) Billetted in Cafe stable 11 a.m. Everybody finished. Slept until 5.30 p.m. tea & clean up to bed again. Sneaked away & wandered around outskirts of very pretty village with decided Home atmosphere.
Sun. 30-7-'16Reveille 7 a.m. Left 8.30 am. Very hot but everyone seems fit. Scenery most delightful yet. Valleys Cultivated every square inch of them.