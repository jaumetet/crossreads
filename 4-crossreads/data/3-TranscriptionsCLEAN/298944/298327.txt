  
 Friday 2nd August 1918 Yesterday evening at 8 pm we marched out of our old Main Dressing Station where we had spent so many weeks, and marching round the siding to the outskirts of Hazebrouck, boarded motor lorries at 10 or 11 p.m. 
 Soon after we started many beams of searchlights began to  seen  appear, betokening the approach of our old 
