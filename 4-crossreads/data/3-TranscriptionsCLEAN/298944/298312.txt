  
 Thursday 1st August On Tuesday evening we held a committee meeting  to  to decide, among other things; how we would carry on up the line. While the meeting was in progress, news arrived that the orders for proceeding up the line were cancelled. It seemed that a divisional move was imminent: Accordingly, classes were not discontinued. 
 I spent most of Wednesday in writing up  and  articles & reports of our scheme 
