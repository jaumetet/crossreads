  
 when he was in charge of them. 
 So we move more quickly now, & more comfortably: coffee or cocoa supplied by Y.M.C.A. or Aust Comforts Fund appears at stopping places. Last night at Wizerne we each had a cup of cocoa & 10 White City cigarettes; which are above the ordinary run. At the same time, there should be quicker communication with the Somme from the Northern sector, than a night's railway journey via Abbeville. 
 At present we are dodging about a 
