  
 when I think of you which isn't at all seldom.  Keep up your pecker & I'll be out soon & the chins will rave.  You'll have to be very firm to choke me off for I shall bore you quite likely. 
 Your letters are absolutely the choicest things.  I won't remark on the handwriting but you ought to know I wouldn't have the most perfect copper plate in place of it. 
 Au Revoir Yours Wilki 
 I know what a heart tearing biz it is to read Jock's letters when you know he has gone.  I'm still getting Jim's.  I'm still in the field & don't really want to leave, while my pals are here too but if they keep on going & the weather & things are too awful well I dunno, I'm pretty safe as I'm not a bit brave & daring.  Good night.  Love to the Boss & Graham & Mac also you. 
