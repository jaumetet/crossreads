  
 has a commission. 
 I don't know whether I have told you what a comfort your letters are, they are great & you're a brick.  I roar over them at times.  Graham must be a great kid everyone speaks so well of him.  I laughed over your party but I wouldn't have any more such unless the Boss is home. 
 Thanks for telling me of your romance.  I didn't feel like laughing at you.  I'm not surprised, I'm jolly glad & I'm sure it will be OK.  You have it in you, you know only you must keep it up & not get tired or bored. 
