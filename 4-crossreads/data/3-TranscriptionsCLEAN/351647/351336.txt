  
 I have the blanket bag your mother gave me on leaving.  I've used it constantly ever since & haven't washed it yet.  What ho. 
 How is the winter going at Tog.  How is Mac & is he still the sweet tempered light hearted blithe lad.  Also I hope Graham is well.  I'm just dying to see him. 
 The Boss well & cheerful I hope also Tom Fog & whats my successor doing.  Do write soon & tell me everything about the garden & the personnel & your latest dresses.  How is the new Jackeroo & who you've had staying there.  I've not heard anything of your book.  Don't say you have dropped it. 
 I must fly & post this.  My love to all & specially yourself. 
 Yours as ever Jim Wilks 
