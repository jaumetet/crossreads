  
 it was good to meet them.  They are well & doing OK, Judy is a Captain & the other two are just on getting their third stars. 
 We celebrated on tea & toast, also music!  They had a piano at the tea place (quite nice & nice girls) & they would have me play, very stiff in the knuckles my dear, made me go cold & sick inside to feel the awful hash I made.  Damn this B war. 
 I must go.  I hope the shearing does well & things are promising.  I'm simply hungering for news, its very hard to be stoical & philosophical. 
 Tell the Boss I'll write soon & I hope I'll have good news. 
 Only I won't be home for next February in Manly I don't think. 
 I haven't had a word from Mrs. Johnson or Miss Annie for months, & I don't like writing again.  My love to Graham & the Boss & Mac & very much so to yourself.  You're a brick & a dear & a sport & even with all that you don't know how I'm holding myself in.  You people are very precious to me.  Saw Brian Mac today, very impressive he is but quite all right & sent his regards. 
 So long Jim Wilkie 
