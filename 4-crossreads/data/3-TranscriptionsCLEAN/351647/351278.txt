  
 I see Stewart Walker was wounded 
 France or thereabouts 14 September 
 My dear Mrs. Jack, 
 Just a few lines to tell you I received the parcel & to thank you so much for the contents.  The silk sweater is just great, beautiful & in the absence of pyjamas I sleep in it & it is O.K.  The muffler is a joy too & the socks too, although they did cause rather a sensation.  It was sweet of you to make them for me & I simply can't tell you how I appreciate them.  The book came also, I haven't read it yet, I've been so busy here.  We are suppose I think to be resting but I still have to go hard.  It's a sweet place & we have good quarters but the weather is rotten worse luck.  I could stay here for ages but we are almost certain to move off again before 
