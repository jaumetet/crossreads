  
 remarkable for their freedom of language than anything else.  We drank to absent friends & our home folk & got very worked up by the time the New Year came in.  Monsieur Gorus the Belge Interpret & rather a friend of mine embraced me most affectionately, a kiss first on one cheek & then on the other much to the delight of those present & his example was joyfully followed by three or four others before I recovered from the shock & was able to create a diversion. 
 Finally climbed to ma lit about 3-30 a.m. & slept soundly till 1 p.m. 
 These little affairs make a bit of a break.  Since then I've been quite busy & there has happened very little of interest. 
 Things are still very quiet, it is a change to our last spell of work. 
 I still have young Metcalfe here with me, nice lad, just a trifle on the large side. 
 I loved your letters & wondered how the Picnic Race meet went off on the 29th ult.  I shall be very anxious to hear all about it. 
 How I wish I could have been there, never mind perhaps there'll be another some day. 
 How is the book getting on.  I often wonder about it & the first one too. 
 I had a letter from Miss Arn on Xmas Day & a parcel too.  I sent her that last photo of Graham, I liked it so much, he looks a greatest little card.  Quite a fascinating smile & the dead ring of the Boss.  Miss Arn & Mrs. Johnson were charmed with it.  I get quite nimpy when I think of Tog & you all & feel I simply can't stand it any longer, but I suppose we must go on with it a bit longer. 
