  
 ages. 
 My brother is still in London & although working hard, has a very good time.  People are very nice to him & he is weekending at swell places & being rather feted all round.  He is dying for me to go over & promises me no end of a good time but I haven't a hope for a while at least. 
 Things are not bad here, I enjoy being out in open country again.  Up till today the weather was great.  I cycled into the nearest decent town last week to see Duncan Macfarlane but he was out so I just did some shopping 
