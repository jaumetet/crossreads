  
 worrying & complicated (I hope). 
 I didn't like being at Etaples much but still it was a change.  Had some music & saw the Pictures & met a few very decent chaps & it was a change of scene. 
 They thought here I had gone for good & were most surprised when I blew back but I think they were glad.  I hope so.  They reckoned I had achieved a job in Blighty, I wouldn't have minded a terrible lot but I would rather stick to the Brigade than have any of those Base jobs.  They are fairly poisonous places I think. 
 I am glad you managed to keep matters well in hand on the trip.  You know you love steering your way through these dangerous waters & it is deliciously exciting I know but my dear, very perilous & really so unsettling. 
 I wish you'd stick to your book but:--- I haven't heard much news your way lately.  Mac wrote me a bonzer letter & told me you were home (at which he seemed more than pleased) he also told me of the excellent results of the shearing & wool sale & of the 
 [The next page appears to be missing.] 
 