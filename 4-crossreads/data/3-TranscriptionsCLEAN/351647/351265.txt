  
 were decidedly lively, not to say noisy, you've no idea of it, the earth & air & everything one continuous quiver.  And its rather fine to watch but I don't like it.  Our men are doing well but this sort of thing doesn't suit them, they want something they can be up & at. 
 I suppose by this you are quite all right again.  I hope things are good at Tog [Toganmain].  How is Graham.  I expect I'll see a big difference in him when I get back.  Goodness only knows when that will be.  I don't know & can't even guess.  I should hate to spend the winter here. 
 I haven't heard from Miss Hilda lately, I hope she is better.  The mails have been simply rotten lately.  I am sending you another little paper they send us, there are some rather witty things in it. 
 The mules here make a beastly row.  I wonder what will have happened by the time you get this.  And what you are doing.  It would be O.K. to be back.  I'm just aching for a bit of music tonight. 
