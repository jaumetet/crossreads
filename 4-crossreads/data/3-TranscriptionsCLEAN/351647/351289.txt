  
 Brian Mac sends his kindest regards & dear old Bill Holt asks after you.  Bill is the real true metal all right.  I see the fourteenth fellows & in fact all my pals much oftener now as in this new job I get round them. 
 I'm glad the book is progressing & am dying to read it, mind you send me a copy of the typed proof. 
 Glad you are at Manly & enjoying life, oh I wish I were down there too.  Never mind perhaps next January or February. 
 I'm well & very comfy but tonight tres fatiguee so will off to ma lit.  Compres vous. 
 Will write quite a sensible letter in a day or two. 
 My love to the Boss & Graham  and  yourself. 
 Yours as ever Jim Wilkie 
