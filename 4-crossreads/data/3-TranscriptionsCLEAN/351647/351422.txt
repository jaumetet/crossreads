  
 thinking of you. 
 I really think we'll be out of the line for Christmas which will be a treat.  We will be housed in some little village I hope.  I expect the W.O. & the Sergeant & I will find & rent some little room where we can sit by the stove & smoke & have a dry warm sleep. 
 I did some washing three days ago but can't get it dry.  It won't dry in the open & the dugout is too small to hang it up there. 
 My two special pals Jock Merklejohn & Harry Purnell have been to see me the last day or so & as they walked over 31/2 miles over beastly country I felt very pleased.  They are great chaps, I wish you knew them but they are not subtle (thank heaven). 
