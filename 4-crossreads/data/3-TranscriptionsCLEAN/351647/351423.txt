
&the guns roar about us & Fritz drops a shell or two about at irregular intervals but he's not a bit startling.  The batteries are still in the line, they have a livelier time as they are working.
One of my pals in my old batt. was wounded this morning.  I am awfully sorry but glad it is not serious.  This is the second time.  He's not among the ones you know.
Our main excitement just now is the arrival of the Christmas parcels, lots of feeling though when some of them don't turn up that we know are coming.  Two that Mother sent have not reached me & one Jim McLean sent & one Harold Chancellor sent.  Its so
