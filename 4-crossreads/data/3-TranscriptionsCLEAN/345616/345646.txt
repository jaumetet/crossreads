  
 Tuesday 8-10-18 Alnais Camp the last place we were in. Got out of Train at 8 am & marched 10 kilos in the dark & rain with full Pack etc. got here Vismus-du-Vol. quiet little village. Lot of the Boys off to explore other Villages near by. Turning fine 61-2-0 
 Wednesday 9/10/18 fine morning but nippy. slept like top all night Boys all dead beat, pub open from 10-30 to 1-30. 6 to 9. Wrote to Amos  & sent him letters. Also wrote to Nell & trustee Ray Syd, Viv & Mrs Parsons & Arthur. 61-3-6. 
 Thursday 10/10/18 Nothing doing. still waiting no parades. a lot of sun today stuck up in one of the estaminets by one of the Goal Birds just returned from 12 months Morgan. 61-5-0 
 Friday 11/10/18 Nice & mild but rain hanging about Paid 40 francs 61-6-6 
 Saturday12/10/18 Raining all day. Parcel from Nell & Yathella  S.C.Fund. 61-8-0 
 Sunday 13/10/18 Rained all night & doing so now, great news the huns accepted Pres/ Wilson's terms 61-9-6 
 Monday 14/10/18 Bitterly cold night could not sleep through it. Excitement very great every where over the huns giving in or ready to give in. Anxiously waiting for todays paper. Am drawing 2/6 a day since Cpl 5/18 got  credit in Book yesterday of &#163;4-9-6 & 14 days &#64; 2/6 = 35/- &#163;6-4-6 now owing me to date. &#163;5-3-2. &#163;61-10-6 wrote to Yathella S C fund & Grace Bros/ 
 Tuesday 15/10/18 Rained all night, still raining have to take 3 chaps for Court Martial about 5 kilos get back some time tonight. &#163;5-5-8 &#163;61-12  Deferred Pay. 
 Wednesday 16/10/18 dull & fine [indecipherable] rain again got to take Prisoner to Court Martial again Another day with no dinner I suppose. &#163;5-8-2-  61-3-6 
 Thursday 17/10/18 Still raining might have to take prisoners again to day. splendid news coming in Lille almost taken &#163;5-10-8. &#163;61-15-0. deferred Pay wrote & Posted letter to Nell last night. Wrote & posted letter to Viv. 
 