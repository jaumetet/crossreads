  
 Thursday 22 August 1918 Stand by for H.A. alarm. Pilot Lt. McGuinness. In air 2 h. 50 m.  H.A. of 2 B.F. over our drome, meet a Hun.  Walker & Letch brought down in flames. Capt. Brown & Fin bring Hun down in our lines, both Huns O.K. but machine shot about and crashes. 
 Friday 23 August 1918 In bed with a temperature, a touch of fever. 