  
 Tuesday 9 July 1918 H.A.P. Pilot Lt. McGuinness in a 250 h.p. In air 2 h. 55 m., no excitement. 
 Wednesday 10 July 1918 Camera test.  Pilot Lt. Lukis, in air 50 m.  Saw two Huns but owing to dud engine could not follow them. 