  
 Monday 30 December 1918 Cable from Home, telling Uncle Willy died on 12th.  Very sorry about it as I thought a lot of Uncle Willy, he was always a good friend to the ones at Home.  Decide to give up trip to England & America at once & proceed Home at earliest opportunity. 
 Tuesday 31 December 1918 