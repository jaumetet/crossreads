  
 Monday 8 April 1918 Cloudy, no photos.  Letters from Home.  Wrote Mother, Mrs. Howey, Mrs. G. Lee. 
 Tuesday 9 April 1918 Photos.  Pilot Capt. Addison, in air 3 h. 35 m.  A good got a good lot of photos, very successfull.  They were very important as they were wanted to complete map. News from Hun prisoner that Oliver Lee is a prisoner and safe. 