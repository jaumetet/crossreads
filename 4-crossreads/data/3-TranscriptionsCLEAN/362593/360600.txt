  
 Friday 13 September 1918 
 Saturday 14 September 1918 Recco, Jenin, A'Fuleh, Haifa. Met large 2 seater Rumpler over Jenin, forced him to land 3 miles away from aerodrome, crashed under-carriage.  Huns jumped out & ran for it, fired 150 rounds at machine & 200 at machine on ground. Pilot Lt. McGuinness.  In air 2 h. 45 m. 