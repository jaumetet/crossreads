  
 on stable picquet. I may mention that my brother Harry enlisted after I did & he has Sydney with the 17th Battalion. 
 6th Went down to Rosberry Park as a gaurd over stores etc for range squadron. Had an easy three days. 
 11th/  Sunday on gaurd came off Monday night. 
 16th Innoculation for troop Kit inspection, armed gaurd for pay officers. 
 28th Went down to Lewisham to see May. Parade inspection by Col. Kirkland & staff 
 Sept 29th / Nominal roll call & issue of under clothing etc. 