 a6692046.html 
 
 
 
  1917  
 42 
 
 
 July 21 
 work.. Spoke to several of the men.  Severe thunder-storm developed during afternoon and we all had to rush for cover in order to keep dry.  Managed to get some. 
 
 
 July 22 
 Parade 9.30.  Full marching order.  Inspection by O.C.  Marched out of Corbie for 8 mile route march.  Hot and tiring and, losing our way, made it worse.  Swim after dinner.  Had tea in town on steak and eggs.  Great.  Got cakes for supper.  Met Briggs and Simpson. 
 
 
 July 23 
 Parade 9.30.  Pontoon grounds  Cleaning up and putting pontoons back on waggons, and gear all in its place.  Afternoon.  Swim.  Town picquet at night from 8 to 9.30. 
 
 
 July 24 
 Parade 8.30.  Cleaning up in morning.  Spent afternoon on equipment. 
 
 
 July 25 
 Route march in morning.  Swim in afternoon.  Read of death of Percy Forsyth in "Times" for first time. 
 
 
 July 26 
 Morning taking sections of canal.  Afternoon off to clean gear.  Bought pair of shorts.  Found I had rubbed skin off big toe.  News of big Russian Retreat.  Cows! 
 
 
 July 27 
 Went for hot bath and spent rest of day in preparation for to-morrow's big march. 
 
 
 July 28 
 Rose 4.45.  Breakfast 5.30.  Fell in 7.  Marched out of Corbie, of which we had all grown so fond, and headed for Lealvillers, 14 miles away, with Achieux as our destination.  Halted at Warloy at noon, under the cooling shade of a forest in the town.  Had lunch here and a sleep of an hour.  Resumed about 2 and marched in stages until about 5.30, when we reached Achieux, nearly all dead.  Went for swim in the concrete baths which was most welcome.  Went up town with Herb and Tim for a feed.  Turned in and slept out under the trees where we were encamped. 
 
 
 July 29 
 Rose 7.  Heavy thunderstorm early.  Tim Woods and myself detailed by Lt Clarke to draw section of canal.  He, Herb and I went to Y.M.C.A. hut and did it.  Had eggs in Achieux.  Returned camp and spent a very dull and miserable afternoon in hut writing and resting. 
 
 
 July 30 
 Fell in at 4 and marched to Railhead at Belle Eglise.  Another move.  Left at 8, after entraining, for Steenbecque, over the Flemish border. 
 
 
 July 331 
 Arrived Steenbecque after a very tiring railway journey all night, huddled together in a horse box so that one could not sit down and, at the same time, stretch one's legs.  Glad to be able to extricate myself from such conditions.  Got there 4 am.  Had hot coffee and eats.  Marched three miles to billet at Sercus.  Had sleep till 12.  Walked to 
 
 
 
   