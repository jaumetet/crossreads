 a6692036.html 
 
 
 
  1917  
 32 
 
 
 Apl 27 
 famous battle field regions of the Somme.  After a very hard and fatiguing march, made all the worse by the excessive weight of our packs, consequent upon the additional load they were made to carry in the shape of the rations,with which we provided ourselves yesternight.  We felt like unloading the stuff many times on the road, so as to lighten our loads, but we had to learn what a godsend this extra rationing was to us at supper time on many a night after we joined our company.  We arrived at Mametz about 2.  Detailed to hut, taken on ration strength, company strength, and all the rest of the things that a soldier does when he joins his company, including the sewing on his sleev-es of the colors which designate the brigade, company or division, and nature of the regiment to which he is attached.  Larsen and I went for a stroll over the fields whereon our Australian soldiers had proved to the world their invincibility as soldiers, but a few weeks before our arrival there.  Returned to camp for tea and attended a concert in the YMCA after given by Fifth Divisional Concert Co. 
 
 
 April 28 
 Attached to No. 2 section, 15th Field Company, Australian Engineers, 5th Division, Australian Imperial Forces.  Spent most of the day in our hut.  Had another stroll around the ruins with Larsen in afternoon. 
 
 
 
 Apl 29 ) 
     30 ) 
         ) 
         ) 
 
 Spent these days wandering round the neighbourhood of Mametz and doing odd jobs in the camp.  This is really a rest camp and as a consequence there is not much work done, just enough to prevent the men from growing stale from their inactivity. 
 
 
 
 May 1 ) 
     2 ) 
     3 ) 
     4 ) 
       ) 
       ) 
       ) 
 
 
 Time spent in training, fatigues for Sports and Show, which are to be held on the Recreation Grounds below the camp. 
 Sports turned out to be a very tame affair.  In the morning of the day which these were held, we had our first experience of witnessing a visit of a Fritz plane coming over and being bombarbed by anti-guns.  Found it rather thrilling and fascinating. 
 
 
 
 May 5 
 On Fatigues at Sports grounds at Pozieres, making tables and benches.  Had a terrible head all day.  Drove over on a limber which made head worse.  Did not have any lunch, not feeling inclined.  Set out on return to camp and arrived there at 1.30 damned near dead.  Lay down and went to sleep till tea time, when I awakened feeling very much improved. 
 
 
 May 6 
 Inspected by O. C. and dismissed for the day.  Windy.  Spent day in hut and doing s  odd bits of washing and mending necessary. 
 
 
 May 7 
 Walked out to Delville Wood with Larsen.  We were told about this stunt before we went out and the telling made us the more anxious to see the place where all 
 
 
 
   