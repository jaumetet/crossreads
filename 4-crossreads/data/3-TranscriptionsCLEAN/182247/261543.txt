 a6692066.html 
 
 
 
  1918  
 62 
 
 
 Apl 11 
 back to the far flung land from which we all sprang.  Instinct had told me that the day was not far off and so it had proved to be.  For one afternoon, the company to which I was attached at Littlemoor Camp were summoned on parade, told that we were to embark on a nameless vessel on a certain day, and there it rested.  We spent our time, in the interim, in packing and re-packing bags, buying little comforts for our long voyage, and making ready in the various ways that soldiers have for the coming change.  Then one morning, on parade, we were ordered to proceed to the Orderly Room, where we answered our names to a roll call.  The next afternoon, we were paraded there again with our kit bags, and handed labels with which to label our-selves on this coming journey.  One almost imagined oneself a leg of mutton, or some such thing, parading round with one's price exhibited upon a most pretentious looking piece of board, with Millitary signs and heiro-gliphics, which none of us could translate, all over it.  However, this was a symbol of movement, if nothing else.  About April 11, early in the morning, so early in fact that we were up to beat the daylight to the Railway Station, we were paraded on the road in front of the Camp, and from there marched to the Railway Station at no great distance from the camp.  We entrained with the usual Military delay and fuss, and when settled, it was not very long before the train began to set out, with its load of light and homesick hearts for Liverpool, and the transport which was to bear us to our native land, or the land of our adoption, at any rate.  We spent the whole of the day in the train, a very cold day outside the railway carriages, as the country for most of the journey was covered with snow;  But, with the comfort and modern apparatus on the English trains we felt none of the outside discomfort whatever, for the inside of the carriages were kept as warm as toast by the steam-heating appliance in use.  After travell-ing right across England we eventually arrived at Liverpool Docks at about 5 the same afternoon.  Were lined up on the platform, checked and marched on to the transport lying alongside the docks.  The Transport proved to be none other than the old Australian White Star Liner "Suevic."  The New South Welshmen were stuffed in a troop deck up in the forecastle, and soon settled down to their new life.  I applied for leave to sleep in the Hospital wards which was granted, as I found that the stairs were too much for my weakened heart, for the present, at any rate. 
 
 
 Apl 12 
 We pulled out from the Docks soon after daylight this morning, and were on our way down the Mersey toward the sea when we came on deck after breakfast.  It was a very foggy morning and we could distinguish nothing from the time we rose until the time we left Liverpool a speck in the distance.  When the fog cleared we found ourselves at sea, one of a very large convoy, 
 
 
 
   