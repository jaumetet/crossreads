 a6692016.html 
 
 
 
  1916  
 12 
 
 
 Dec 25 
 breakfast and then went on deck.  Church parades at 10.30.  Sang the anthem at services of Church of England and other denominations.  Had"Xmas Dinner."  Special Menu - Boiled Pork, green peas, yams; fruit, which was supplied by the officers, and sago pudding which looked like a gathering storm, but tasted o.k.  Cigarettes were also handed round as a donation from the O.C. Gray.  Gave three cheers to O.C. Troops and Captain Jermyn of the "Marathon."  Also our own officers.  Formed many mental pictures of the folks at home, wondering what part of the world it was in which we were facing grimly the sinister intentions of Hun and Company.  H.M.S. Swiftsure arrived this afternoon.  She was saluted by our transport in the usual manner and custom according to tradition.  The ship's ensign was dipped aft and all the men on deck stood to the salute, as the trumpeter on the Swiftsure blew the call.  The ceremony was very impressive and once again drove home to our minds the beauty and severity of the tradition and discipline in the good old British Navy.  H.M. King George's message of Xmas Greetings was read the troops this morning at Church Parade.  Gave him three hearty cheers for his thoughts.  Afternoon  Lay on deck and had a sleep.  Evening.  Sang Comrades in Arms with Quartette at Sacred Concert which was held on deck. 
 
 
 Dec 26 
 Boxing Day.  Rose as usual early.  Had Breakfast and went on parade at 9.30.  At 10.30 anchor was weighed and with H.M.S. xxxxxx Kent in the vanguard our vessel and three other ships steamed out of Freetown harbor.  At last we were to leave this Hell on earth for something cooler.  Steamed into a calm and tranquil sea.  Not one wet eye on the ship through leaving the place.  Hammock parade in afternoon.  Also lecture in First Aid by Sapper Fern.  Very interesting.  After tea had to get our hammocks out of the bin in the dark.  Found the job inconvenient and annoying.  Soldiers are not saints when it comes to where they have to get in for their cut of anything.  I know that I was not and only thought of rescuing my own little bundle in the general scramble.  Had game of Five Hundred with Herb Larsen, McNab and Tim Woods in Sergeants' Mess. 
 
 
 Dec 27 
 Parade as usual.  Fine day.  Saw tropical sunrise at sea.  Very grand and impressive sight.  Caught up with strange steamer during morning making the same way as us.  She was ordered to fall in with the convoy. 
 
 
 Dec 28 
 Weather growing much cooler and comfortable.  Feeling difference in change of longtitude.  Nothing special.  Had lectures on points of the war gained since the outbreak, by Lt. Gray.  Very entertaining and instructive. 
 
 
 Dec 29 
 Lovely day.  Bit on warm side still.  Fell in with a big convoy of 6 ships off Dakar, French Sierra Leone. 
 
 
 
   