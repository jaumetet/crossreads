 a6692050.html 
 
 
 
  1917  
 46 
 
 
 Sep 9 
 Company drill in morning  Afternoon off.  Spent it getting new boots and having rest in billet.  Herb Larsen was announced as winner for design for Company Xmas Card design.  Very nice with a motto supplied by myself. 
 
 
 Sep 10 
 Parade 9.  Marched back to billet.  Standing by await-ing movement orders for the line.  Inclined to head-ache but better than yesterday. 
 
 
 Sep 11 
 Parade 9.45.  Caught motor lorry from Sercus to Ypres via Poperinge.  Dusty run but much preferable to footing it.  Familiar sound of gunfire etc. become new after a good spell away from them.  Seems like a lively place.  Passed through the dead city of Ypres.  Only a shambles with its famous Cloth Hall barely recognisable.  Bivouacked behind and to the left of Ypres.  Plenty of hostile aircraft about on this sector.  Think things portend to the lively side. 
 
 
 Sep 12 
 Parade 8.30.  Spent day on dug-outs for our own comfort.  Herb, Don Beaton  Snowy and Alex and I spent all day building one to hold 5.  It proved very satisfactory and cosy.  Nice to be able to lie down on the soft earth again.  Feel quite snug and at home in our own little "'ole". 
 
 
 Sep 13 
 Went dental.  Spent morning messing around in quite true military fashion.  2 pm.  Took motor and went to hospital over near Poperinge.  After having tea at 4, caught another motor to Rest station.  Slept there all night on a real bed with real blankets and covers.  Hardly knew what to do with them.  Had good night's rest.  Day very dull and cold. 
 
 
 Sep 14 
 Went to C.G.S. and had tooth fixed up at last.  At 1.15 set out, per boot, to return to camp, 10 miles away.  Reached there tired and hungry and ready for a good bunch of parcels from home. 
 
 
 Sep 15 
 Parade 8.  Spent day erecting huts for Divisional Headquarters.  Turned in early.  Had a visit from John who had just come on to the sector and had seen one of our company in the Y.M.C.A. and found out where I was.  Handed him some of the good things from home. 
 
 
 Sep 16 
 Parade as usual.  Spent day trimming steel hats with hessian.  Not a bad job.  Fritz fairly busy with shrapnel.  John came over after tea and spent an hour or so with me in dugout.  Glad to have him near me again. 
 
 
 Sep 17 
 Parade 8.  Worked on huts all day.  Windy but fine. 
 
 
 Sep 18 
 Parade 8.  On huts all morning.  Afternoon off.  Warned for duty in the line at 3 am. in morning.  Day very dull and miserable. 
 
 
 
   