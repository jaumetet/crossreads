 a6692048.html 
 
 
 
  1917  
 44 
 
 
 Aug 11 
 Marched to Bleringham to construct an aerial roadway.  All day show.  Arrived back 4.30 very tired.  Washed and spent evening under trees writing. 
 
 
 Aug 12 
 Route march of 5 miles.  Nice day.  Spent afternoon in fields among the wheat sheaves writing.  Lovely. 
 
 
 Aug 13 
 Inspection by General Hobbs.  After tea, rode 10 miles along the canal banks to Arcques to see John.  Met him with Keith Eaton and Phil Brennan.  Very glad to see them.  Jack asked me to try and get him transferred to my company. 
 
 
 Aug 14 
 Route march in morning.  Bath in afternoon.  Wrote letters after tea.  Spoke to Lt Clarke re John. 
 
 
 Aug 15 
 Ten mile route march.  Very hard and tiring.  Returned billet at 1.30.  Got paid in afternoon, 2/4/-.  Wrote John telling him that he can be transferred to my unit and advising what to do to bring it about.  I have my doubts about it. 
 
 
 Aug 16 
 On fatigues in morning.  Map reading lecture in sun in afternoon. 
 
 
 Aug 17 
 Went out to canal to sound it.  Put aerial roadway over and tested it.  7 miles both ways.  Got back tired and hungry. 
 
 
 Aug 18 
 A rest day.  N. C. O's out selecting defence positions. 
 
 
 Aug 19 
 Lectures in morning.  Spent afternoon in sun writing. 
 
 
 Aug 20 
 Route march past General Birdwood in morning at Bleringham.  Squad drill in afternoon.  Bike ride round country after tea.  Glorious day. 
 
 
 Aug 21 
 Gate picquet in morning.  Afternoon off.  Nice day. 
 
 
 Aug 22 
 Went dental at Sercus.  Had musketry practice in afternoon. 
 
 
 Aug 23 
 Route march in morning.  Bath Parade after dinner.  Handed Jack's transfer to Lt Clarke for attention. 
 
 
 Aug 24 
 Route march morning.  Lecture on laying out of a trench system in afternoon. 
 
 
 Aug 25 
 Signalling practice all day, in special Morse class.  Attached to Company signallers temporarily. 
 
 
 Aug 26 
 Company drill in morning.  Afternoon off.  Spent it in billet writing. 
 
 
 Aug 27 
 Dental parade all day.  Had the tooth attended to.  Miserable day.  Rest of company on a route march all morning. 
 
 
 
   