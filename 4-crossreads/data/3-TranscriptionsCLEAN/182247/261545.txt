 a6692068.html 
 
 
 
  1918  
 64 
 
 
 June 6 
 were rather disgusted with this turn of events, as we felt that as the ship had to pass Sydney almost to get to Melbourne, they might have put her into our own home port first.  However, once in Melbourne, we were not long on board.  We were sorted out into our various states and entrained on the pier at Williamstown.  Once settled, the train soon moved out toward Spencer Street, via Albury, to Sydney and Home, sweet Home.  These troop trains were rather a revelation to us.  Everything had been studied to the finest degree to make the lot of returning men as comfortable as was possible.  We were treated like kings with regard to comforts at the hands of that loveable organisation - the Red Cross Society - and we did nothing but eat, and eat, and eat, right from Melbourne to Albury.  There we got out to change trains, and had to eat a big meal which was provided for us by the authorities.  After this we entrained again into carriages fitted up in English Hospital Train style, with a real bunk for every man, in which all hands slept and spent a most enjoyable night's rest.  Next morning we awoke early and found 
 
 
 June 8 
 ourselves moving across New South Wales to the tune of about 50 miles per hour.  We soon arrived at Goulburn, where we breakfasted.  Entrained again, we set out on the most glorious June morning that ever broke, on the last lap of our last journey towards home.  Those of us who were fortunate enough, availed ourselves of the free look-out of the carriage verandah, from which we received many thousands of fond home-coming wavings from kind womanly souls who made one feel that it was good to be home at last.  As we raced towards Sydney shrill engine whistles rang out, people cheered, and we were even now running through a continual stream of flags and banners on both sides of the line, the latter bearing such appropriate messages of welcome as prompted the generous heart of he or she who hung it out for us to see and to be glad for.  About 10.30 we neared Sydney, and, to the accompaniment of continued shrill locomotive whistlings, women and men cheering, and the many other manners of welcome as only the Australians know, we slowly crawled into the big station at Sydney's Gate, which we all recognised as the Central Station.  We were ordered out of the train into waiting motor cars, and here began the most triumphal procession of welcome and thanks from a nation's people, that man ever experienced.  What great Caesar and Pompey and Napoleon felt, after returning from their greatest vic-tories, we experienced in no less degree.  The welcome was a great deal more than even any of us had ever dreamed it would be.  It was colossal and proved too much for most of us.  Emotion got the better of us to such an extent that when we arrived at the Buffet in Sydney Domain, to again face those dear one from whom we had been absent for so long, and for whom we had longed every minute of that lifetime, we were left with no power of speech.  And what a meeting that was. 
 
 
 
   