 a6692052.html 
 
 
 
  1917  
 48 
 
 
 Sep 25 
 Rose 6.  Could sleep no longer owing to severe bombardment by our guns which shook the dugout and at intervals, caused the sides to crumble and nearly bury us.  Did nothing all morning but sleep.  Warned for duty in the line in afternoon.  Told we were to go to front line that night as road party to mark out the road in the stunt through Polygon Wood, after first wave had gone over, so that supporting troops would know the track to follow.  I was made a picket carrier.  Did nothing all afternoon except conjure up possibilities as the outcome of to-night's stunt.  Told to fall in at 1.45 am. in morning in battle order.  Got things ready for the job.  Examined Gas masks.  Put diary in overcoat in case anything happened to me and asked Tim Woods, in case I should get pipped, to forward it on to John, which he promised to do. 
 
 
 Sep 26 
 The day of days.  Rose 1.30 and had tea. Fell in 2.  Marched out to front line ready for hop over at day-break.  Reached there about 4.  Guide took us to wrong sector.  Went to find our right place.  Did not return.  We stayed on.  Zero hour arrived.  Barrage opened 5.45 am.  Infantry moved forward.  We did not but awaited instructions from Sergt. Forbes.  Our gunfire sounded murderous and terrifying.  Counter barrage opened soon after.  One of the first shells struck and exploded on the parados of our trench kill-ing Faze, Urguhart, McGowen and wounding White, Parker and Scotty Irvine.  Some of our party left for aid.  Dave Hilton and I decided to stay there and look after and tend the poor wounded.  Others left at intervals leaving Dave and I alone with the casualties.  We waited in this veritable hell - or worse - until about 10.30.  No help came.  A party of prisoners came out of the line and I sent Alex Irvine back with them as I could not stand to see him in such a state of nervous collapse.  Bill White and Jackie Parker died of their wounds after we had given them all the water we had.  Dave Hilton and I decided to stay there no longer as we could serve no purpose by so doing and were only courting danger - or death - by remaining.  We were preparing to gather our gear together to start out when - Crash.  A shell burst clean in our faces.  I went down and remained there I know not how long.  When I came to I was smeared in blood and in awful agony.  I thought only of death as an end to my pain.  Struggled, in my delirium, to get my bayonet out for a sinister purpose and thought of Dave.  Called him by name.  No answer.  Raised my stricken body so as to get my back against the trench side to try and see where Dave was.  He lay prone in the bottom of the trench quite dead.  This was the finish of me.  I started to crawl out on to No Man's land to try and get help and ease from my fearful pain and fell forward on my face exhausted.  How long I lay there I know not.  I suddenly felt someone roll me over on to my back and say "what has happened, Noel, old man?"  I looked 
 
 
 
   