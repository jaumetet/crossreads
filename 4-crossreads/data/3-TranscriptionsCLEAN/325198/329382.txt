  
 7 Oct, 1916 Sunday 8th Poor old Sam's has got word that one of his brothers has been killed in France & the other wounded, he is cut up.  I just have been over to consuld the list in front of Orderly room.  Glad to see it has no Burgess on it.  Stevens went off to hospital this evening he is pretty seedy & has a bad foot, he should have been away long ago. 
