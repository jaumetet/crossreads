  
 2 Aug, 1916 
 [Contd. from page 152] But will not own up when we are asked "Who's life had we assisted? That it was but a common horse Who's leg rope had got twisted. 
 So now you know our history Of how we served the King And how we watered horses And made the brushed ring Now don't proclaim us heroes For we're bashful to the core And when the war is over We'll be Civvies ever more. 
 Ducker 8 of 6th 
