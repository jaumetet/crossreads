  
 25 Aug, 1916 Our Brigadier is full of cheer He shouted out "Hooray" "Youre making history" he said, And then he rode away. 
 And in this tale if you read on, You'll find out how its done, Of how old Abdul came to fight, And how a victorys won. 
 When Abdul left Damascus With an Army Corp or so, He never even asked us Permission for to go. 
 He came along the sea-coast And swanked  at Ber el Abd And uttered there a big boast Of the land that he had grabbed. 
 He marched to Ograbina And stayed there for a week As much to say "We've seen yer So don't give us any cheek" 
 And then he came to Quatia And camped around the well As much to say "We'll face yer And give yer ---- hell" 
