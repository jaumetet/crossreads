  
 Left Nursing Home and returned to my flat. 
 Sunday November 21st     Spent the day in London. Gina lunched and dined with me. 
 Monday November 22nd     Arrived in Leeds at 1.30 and lectured in the evening. A Godforsaken hole and a still more God-forsaken audience. 
 Tuesday November 23rd     At Birmingham. A fair audience, but local manager said he had not been given proper time to advertise 
 Wednesday November 24th     Left Birmingham at 8 a.m. Arrived London at 10 a.m. Lectured at Queen's Hall in the afternoon. We had afairly good audience. Today my letter on Winston in four columns appeared in the "Times", and it made a great sensation, being reproduced in nearly all the other papers. Dined with Fenton and went to the Palace. 
 Thursday November 26th     Was to have lectured at Nottingham, but cancelled lecture and stayed in London. 
 Friday November 27th Went to Manchester, an awful place, with thick fog and heavy rain, which kept people away. Ormrod, my chairman made a fool of himself and was finally counted out and hooted off the platform for speaking too long. 
 Saturday November 27th Arrived Liverpool at 12 p.m. Saw the New Adelphi Hotel for the first time where I met my Uncle George, whom I had not seen for many years. He is once more back in the Navy, and commands a squadron of trawlers, but I don't think he ever goes to sea himself. Lectured in the afternoon and caught 5.25 trainto London. 