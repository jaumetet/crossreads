  
 May 1918 Wednesday 8 
 Dussy-la-Daours Still raining, had a run at midnight to Villers Brettoneaux, plenty of the Bosche dead still unburied. Paid a  visit  to Jame's grave and took a snap of same. Wrote to Uncle Will, Mum, Toots and Adrienne also Ray. Slight gas bombardment at Corbie and Fuilloy Met an American from Florida in Engineers. 