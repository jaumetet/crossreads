  
 August,1918 Thursday 15 
 Cayeau Weather very warm and inclined to be very hot. Received letters from Home. Went up to Bty position for wire and material that we had already carted up on previous days. Salvaged quite a considerable quantity of wire, very good material too. Geo Stone's pass came out for Blighty, he is very lucky indeed. Had a few bombs over, no damage. 