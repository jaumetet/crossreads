  
 January 1918 Tuesday 22 
 At Beutin Grand parade and march past at Attin. Inspection by Generals, Brown and Hobbs. I was mounted today on a very famous charger, named by some "wag" "Semaphore". After the inspection Hobbs made a little speech. But we heard very little indeed. I believe we are moving shortly. 