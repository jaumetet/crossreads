  
 June 1918 Wednesday 26 
 In Billets. Erronvillle Weather glorious a perfect day. Rode to Abbeville with Smithy as horseholder. He is the luckiest person I know to be granted ten days leave to Paris. Abbeville is slightly bent in places owing to the attention paid to it by Fritzer planes. Paid a visit to my friend of yesterday  Suzanne , and had some oeuf good mangy. 