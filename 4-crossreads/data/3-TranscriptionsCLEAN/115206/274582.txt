  
 January 1918 Saturday 12 
 At Beutin Still on Battery Runner, but have not been so busy today. Weather cold and wet, would rather be in by the fire than out.  We played 49th battery football and beat them badly, by 35-13. Boys are all jubilant, as this victory puts us at the head of our class. 