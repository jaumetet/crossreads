  
 February 1918 Monday 4 
 On the road Left billet at 9.30 for the Guns. Smithy, Adey, Cody and Garford. Jamie and I as horseholders. Arrived after a tour through Bailleul and Neuve Eglise, at old Battery position, near Messines. Returned to Battery's billets and had lunch en route.  Felt pretty tired so retired at an early hour. Weather very cold and damp 