  
 October 1918. Saturday 12 
 In reserve, Ramicourt Weather wet. Tommy burial party very busy getting the dead buried. Trumpeters played the Last Post over 200 buried. Received a few letters, very welcome too. Had a shot with Jim's revolver. Had a good rest all day. 