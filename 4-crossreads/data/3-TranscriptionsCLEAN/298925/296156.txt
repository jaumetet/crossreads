  
 all dressed and waiting for passes. 
 24th  Last night 12  aeroplanes  Zeppelins visited Essex and S.E. districts round London doing considerable damage to minor properties, and killing 30 and injuring 99 people. 
 The first hint of the raid had come in the shape of Policemen tapping windows and telling people to be careful because Zeps were about, so most of the people decided not to go to bed but stayed up to see the Zeps.  The night was very dark and cloudy and at 11.30, the noise of a Zep could be heard like the sound of a threshing machine 
