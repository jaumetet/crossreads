  
 with waterbottles.  From Exeter we travelled a good deal along the coast so that for miles you could toss a stone into the water from the carriage. 
 The scenery is delightful.  Bathing machines and canvas dressing sheds line the seaside resorts which abound here. We passed Axminster where the famous carpets are made also an old castle such as we have read about in a forest in which we could see quite a number of deer.  We arrived at Torre station at 7.30 a.m.  Where we were met by the mayor and aldermen of the town with a special 2 decker 
