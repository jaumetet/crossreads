  
 May your joys be as deep as the ocean And your troubles as light as its foam. 
 Think of all the kind words that have ever been said And of all the kind thoughts that ever have sped And of all the good luck that to you could befall And be sure that my heart for you echoes them all. 
