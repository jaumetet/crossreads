  
 29th  Started practicing the Mass bands of the Brigade. 
 30th  Divisional Church parade on Common near Camp 9th Brigade Band Played past.  Presided over by the Bishop of Salisbury from one of the Roman Tuberlix or burial grounds. 
 After church parade was a march past and general salute to General Monash. 
 About 80 German Prisoners arrived here to-day, nearly all of which had lost limbs, under a strong guard. 
