  
 [Letterhead of Church of England Australian Fund for Soldiers Overseas] [Printed text] Church of England Australian Fund for Soldiers Overseas. With the Australian Imperial Forces. 
 Canon Garland V.D. c/o Anglo-Egyptian Bank, Cairo. Representative Abroad 
 14th Australian Hospital Port Said May 29. 1918 
 My Dear Aunt, I wonder will you have heard that I was wounded in the thigh about three weeks ago, and after much travelling and stay-overs in several hospitals on the way down, eventually arrived here at Port Said. 
 I had thought I was only lightly injured but owing to the bullet having damaged an artery a haemorrhage came on one night and I had to be hurriedly operated on; I was then hoping to get better when a few days after, it was discovered that the track of the bullet was dirty, thanks to the dirty Turk's ammunition. However after the second operation which I had this week perhaps things will go well now. Lets hope so at any rate. 
 All your letters have come along regularly and I thank you for them. I hope you are keeping well. Summer is here now and we are having very hot weather, but every evening up to the present is cool. I expect that I shall be on my way home as soon as I am able to be moved, so you may look for me soon. 
 The English Church Padre is writing this for me as I am not able at present to write letters. 
 Every good wish Your loving Nephew Will. 