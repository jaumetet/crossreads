  
 25 Sun February Mike very bad green feed did not agree with him  General Dobell Chetwode & Chauvel visited front line tenches otherwise very quiet 
 26 Mon February Letters from G & N.  Taubes over   Loveredge knocked by mule  Harry Cook landed 2nd Bgde stunted to Rafa Railway passed camp Duchess went gay  6 Prisoners 
 27 Tues February  Rained like Hell   Son & myself got wet to the skin  up at 3  2nd Bgde met Turkish Cavalry.  Rum Issue  Q M arrived drunk  Miller & Shean had a box on   more Turks 
 28 Wed February Horses out in the crops  Taubes over  General went to Rafa  more Turks 