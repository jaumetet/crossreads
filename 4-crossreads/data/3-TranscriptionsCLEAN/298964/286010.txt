  
 10 Th May As usual on front with Aeroplanes very busy with bombs  Aus mail in letter from D. N.G. also writing all lights out at night 
 11 Fri May not feeling too well had a terrible job to finish letters  Usual on front large quantities of stores being removed back.  Big attack expected 
 12 Sat May Usual on front  Artillery very active  two Divisions arrived and camped at Khan Yunus  Keeping very warm and dusty 