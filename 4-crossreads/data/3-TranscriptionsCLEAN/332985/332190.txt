  
 Steenvoorde.  Arrived 10 a.m.  Very bad billets.  Lieut. Morris gone to D.A.C.  Lieut. Seagraves from D.A.C. coming to Bty.  This is the 3rd time we have been at the town with the unpronouncable name.  Almost in Belgium.  Rained most of the day. 
 19-7-1917 Reveille 3-30 a.m.  Move 6 a.m.  Arrived at Rennehelst  10 a.m.  Our second time here.  Hundreds of wagon lines here.  14 men dead on a G.S. Wagon, gassed with a gass shell.  The enemy shell this place occasionaly.  Weather wet and windy.  Guns go into action tonight.  Enemy shelling road heavily.  Tight go to get our guns into action. 
 [Note: pg. 1. C. De Guerre = Belgian Croix de Guerre.] 
 [Transcribed by Judy Gimbert, Judy Macfarlan for the State Library of New South Wales] 
 