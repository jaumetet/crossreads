  
 handed over to the 46th Bty. 
 6-3-1917 Moving off tomorrow 8 a.m.  Pulling guns out at midnight.  Things very quiet.  Snow all melted, very muddy. 
 7-3-1917 Left Bottom Wood at 10 a.m.  Arrived at Behencourt at 4-30 p.m.  Second time in this village, very cold. 
 8-3-1917 Snowing this morning.  Very cold.  Great stir up, the C.R.A. in camp.  I am kicked out of shop.  Likely to be here some time.  3rd Bty. gone to school.  1/2 an orange per man issued in lieu of jam. 
 9-3-1917 Snowing, and raining all day.  Shod 10 horses.  The billet where we are was built in 1713.  Staying here 15 days, going to do Battery manoeuvre. 
 10-3-1917 Wet and miserable.  Nothing doing. 
 11-3-1917 