  
 out today, went like a board.  Everything frozen.  Bread, meat, tinned stuff, light a fire under water cart.  One of our planes down, a dozen yards away.  Shoeing smith Heyden Hospital.  Very busy shoeing and frost cogging. 
 4-2-1917 Fairly quiet.  Three guns out of action.  Hun shelling Albert with 12 in. shells.  No shells in our wagon line. 
 5-2-1917 Not so cold as usual.  A big stunt on last night.  Our Infantry hopped over.  A lot of ammunition going to Bty. 
 6-2-1917 Terrebly windy;  Bitterly cold;  Freezing all day.  A fleet of enemy aeroplanes over last night, dropping bombs, and opened fire with their machine guns.  Brig. Gen. Leslie now in command of 1st Division. 
 7-2-1917 Bitterly cold, a high wind blowing.  Still freezing.  9 Hun planes over in one 