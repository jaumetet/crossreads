  
 18/5/17 Things rather quiet on our Front.  A good shells came over last night, a fair amount with Gas.  Weather clearing up. 
 19th Unusually heavy Artillery bombardments.  Still on Deep dugout at Bug St. near Noreuil. 
 20th Nothing fresh to report.  Heavy Artillery work as usual.  Working on Deep dugouts. 
 21st Left Bug St. this morning with Sec. Office "M. Brigdon" & 15 other ranks to put wire out at Front Line.  Was caught in Heavy Barage while waiting in Supports but had no casualties. 
 22nd Had rough but successful night last night, got large carrying Party right to the Front Line near Reincourt [Riencourt] with wire & "Corkscrews" and did a fair amount of wiring in front of Lines.  The Enemy discovered us about 2 a.m. & suddenly opened a heavy Barage fire on us & we had to leave in a hurry.  Had very wet & dark night for work. 
 23rd Sec. mostly resting & doing a little camp work etc.  Nothing.  No. 1 & 3 wiring. 
 24th Nice fine weather.  Things quiet.  In the afternoon I took one of the 68th R.E. Officers out to the Front Line.  They are taking over from us. 
 25th Left Vaulx today & came to a camp near Fremicourt. 
 26th Still near Fremicourt.  Having a rest.  Nice fine weather. 
 27th Nothing fresh to report on on this Front.  Big success on Italian Fronts. 
 28th Still having nice fine weather.  Nothing fresh doing. 