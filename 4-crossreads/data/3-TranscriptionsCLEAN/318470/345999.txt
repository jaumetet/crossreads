  
 Sunday 30 January 1916 Working "Pontoon" bridge on Canal.  Great preparation going on for supposed coming attack.  Nothing else of importance. 
 Monday 31 January 1916 Working Pontoon bridge.  Very windy making handling bridge very difficult. 
 Tuesday 1 February 1916 Working bridge as usual.  Nothing fresh reported.  7th, 8th Batts. crossed Canal today. 
 Wednesday 2 February 1916 Working "Pontoon bridge".  A great deal of traffic crossed today.  Also much traffic on the Canal. 
 Thursday 3 February 1916 Windy today making work on Pontoon bridge very hard.  Nothing of importance going that we have heard of. 
 Friday 4 February 1916 Working on Pontoon bridge.  Windy making work difficult.  Not so much traffic as usual across Canal. 
 Saturday 5 February 1916 Very windy today, work on bridge being very hard.  Heavy rain in evening & early last night. 
 Sunday 6 February 1916 Not much doing today.  Took walk down Canal for about a mile in the morning to view scene of last years attack by the Turks. 
 Monday 7 February 1916 Had easy day at work today.  Nothing of importance going.  Beautiful day.  Working on "Pontoon bridge". 
 Tuesday 8 February 1916 Usual work on Pontoon bridge.  No news of importance going.  A lot of shipping going through Canal. 
 Wednesday 9 February 1916 Working on Pontoon bridge.  Nothing of importance reported.  Very heavy wind & a little rain.  Wind made Bridge difficult to work. 
 Thursday 10 February 1916 Usual bridge work this fine day.  Nothing of importance going. 
 Friday 11 February 1916 Usual work on Pontoon bridge.  Nothing out of the usual reported.  Much preparation going on across the Canal.  Very windy. 
 Saturday 12 February 1916 Working on Pontoon bridge as usual.  Bridging Train to take over tomorrow. 