  
 Sunday 7 May 1916 Very hot day.  A lot of boys vaccinated.  Had easy day. 
 Monday 8 May 1916 Setting out Hutting, also supervising work. 
 Tuesday 9 May 1916 Whole Company went to Rifle Range in morning and had Rifle practice.  Lecture afterwards. 
 Wednesday 10 May 1916 Had fairly easy time in Camp.  Had short lecture on Demolition morning  practically afternoon. 
 Thursday 11 May 1916 Went to Rifle Range in morning practically in morning - little Camp fatigue in afternoon. 
 Friday 12 May 1916 Turned out 7 a.m. & took part in Divisional Battle movement. 
 Saturday 13 May 1916 Had charge work erecting Compound at Canteen - wet.  Things quiet. 
 Sunday 14 May 1916 Very hot and dusty, got orders to pack up ready to go to Rail Head tomorrow.  Y.M.C.A. night. 
 Monday 15 May 1916 Left camp 5 a.m. & marched to Rail Head, about 7 miles distant.  My Sec. No. 2 marched another 4 miles & took up duty. 
 Tuesday 16 May 1916 Stationed "Duntroon".  We are in charge work Hutting, water supply etc. at "Gundagai", "Katoomba", Mt. Kembla & Duntroon. 
 Wednesday 17 May 1916 Extraordinary heat yesterday & today.  Reported yesterday to 118 in shade in No. 2 Officers tent & today 125. 
 Thursday 18 May 1916 Still very hot but not so hot as yesterday.  Working on water reservoir & other camp work. 
 Friday 19 May 1916 Heat wave has passed over and it is now much cooler.  Working on water conservation scheme. 
 Saturday 20 May 1916 Same as yesterday.  Nothing fresh reported. 