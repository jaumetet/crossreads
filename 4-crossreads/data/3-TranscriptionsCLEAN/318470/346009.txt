  
 Sunday 9 April 1916 No drill for Sec.  Was kept very busy most of the day preparing for work in trenches tomorrow. 
 Monday 10 April 1916 Had party working in trenches, also Coy. of Infantry working revetting, etc. 
 Tuesday 11 April 1916 Constructing trenches in Bridge Head defences about one mile from Ferry post. 
 Wednesday 12 April 1916 Same as yesterday.  Supervising trench construction by our own Sappers & Infantry. 
 Thursday 13 April 1916 Still working in trench revetting & instructing Infantry.  Tremendous heavy wind. 
 Friday 14 April 1916 Still windy.  Heavy rain settling dust a great deal.  Working revetting trenches. 
 Saturday 15 April 1916 Reported L. Horse had brush with party Turks off Serapheum capturing 40.  One L.H. man killed. 
 Sunday 16 April 1916 Only morning Parade.  No work in Trenches.  Beautiful day tho' very warm. 
 Monday 17 April 1916 Work in trenches in morning.  Handed over to No. 3 Section.  Musketry in afternoon. 
 Tuesday 18 April 1916 Musketry - squad drill -  trench work .  Nothing of importance reported. 
 Wednesday 19 April 1916 Instructing Section in Musketry all day, training very hard.  Nothing of importance reported. 
 Thursday 20 April 1916 Musketry all day again today.  Weather very warm.  Nothing of importance going. 
 Friday 21 April 1916 Usual drill and Rifle exercises.  Nothing of importance going. 
 Saturday 22 April 1916 Out with Section.  Rifle exercises, squad drill & Demolition. 