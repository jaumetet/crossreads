  
 Sunday 18 June 1916 Arrived Alexandria about 6 a.m. & went aboard the Kinfauns" [King Janus?] & remained in Harbor all day. 
 Monday 19 June 1916 Lying in Harbor all day.  The "Georgina" with 14th & 15th F. Coy. Drivers & Horses aboard got away about 6.30 p.m. 
 Tuesday 20 June 1916 Lying in Harbor all day.  Weather very hot.  Do not expect to leave for several days for some reason or other. 
 Wednesday 21 June 1916 Lying in Harbor all day.  A few Transports left.  Yachting in Harbor in afternoon, very pretty. 
 Thursday 22 June 1916 Left Alexandria at about 10.30 a.m.  Had very good trip during day.  Sea nice & smooth. 
 Friday 23 June 1916 Still making very good progress.  Weather calm & cooler than in Egypt. 
 Saturday 24 June 1916 Said to have sighted submarine about noon.  Still making good trip.  Weather rather good. 
 Sunday 25 June 1916 Altered course for some reason about daybreak & ran into Malta, ship here all day. 
 Monday 26 June 1916 Left Malta about 6.30 p.m. with "Caladonia" & 2 other Transports.  Escorted by destroyers & cruiser. 
 Tuesday 27 June 1916 Making good progress.  Sighted African coast in morning.  Travelling along Sardinian Coast in evening. 
 Wednesday 28 June 1916 Made good passage.  Arrived Marseilles about 8 p.m.  Anchored in outer for the night. 
 Thursday 29 June 1916 Left Marseille port about 9 p.m.  Left Kingfauns [Kinfauns?] Castle about 3 p.m.  Every thing going well. 
 Friday 30 June 1916 Making good journey.  Country extremely beautiful along the Valley on the Rhone from Mornas to Lyon, also right along. 
 Saturday 1 July 1916 Making good trip.  Nice weather & everything looking beautiful. 