  
 Sunday 3 December 1916 Repairing Needle Trench Deep Dugouts & laying Duck walk tracks.  Very cold - Ice everywhere. 
 Monday 4 December 1916 Same routine as yesterday.  Fine clear day.  Enemy aeroplane brought down. 
 Tuesday 5 December 1916 Building Dugouts and doing other improvements in lines.  Heavy Artillery fire by Enemy.  Warmer. 
 Wednesday 6 December 1916 Usual fog, freezing, very hard laying Duck walk track & general improvements. 
 Thursday 7 December 1916 Same as usual repairing trenches, laying tracks, etc.  Very cold. 
 Friday 8 December 1916 Ice everywhere, ground frozen hard, still doing repairing in trenches, etc. 
 Saturday 9 December 1916 Heavy fogs & freezing hard.  Same work as usual. 
 Sunday 10 December 1916 Usual work in trenches.  Still very cold.  Heavy misty weather.  Lots of shelling both sides. 
 Monday 11 December 1916 Bright this morning.  Aeroplanes very busy.  Saw two enemy planes brought down. 
 Tuesday 12 December 1916 Cloudy again today, heavy artillery fire on both side.  Fritz very busy. 
 Wednesday 13 December 1916 Usual work in trenches.  Activity both sides artillery.  Fritz very busy - mostly shelling old villages. 
 Thursday 14 December 1916 Heavy hostile shelling with fairly heavy casualty list.  Usual trench work.  Fairly fine weather. 
 Friday 15 December 1916 Heavy hostile shelling, a good many casualties.  Carrying on work as usual in trenches, etc. 
 Saturday 16 December 1916 Fine in morning, raining, snow in afternoon - fairly quiet. 