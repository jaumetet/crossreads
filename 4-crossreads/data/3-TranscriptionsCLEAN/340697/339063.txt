  
 discovered during this truce that there were even more casualties than had at first been reported 24.5.15 
 The Triumph was sunk by a torpedo today. She steamed about 2 miles and seemed to be making for Anzac but sunk about 1 1/2 miles off Gaba Tepe. As she went down she fired a broadside. We hear that 60 of her crew were drowned 25.5.15. 
 We landed a trench mortar today. It is a Japanese invention and throws a projectile 28 lbs. weight a distance of about 1/3 
