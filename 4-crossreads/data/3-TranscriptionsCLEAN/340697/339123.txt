  
 advanced and took a little more of Pozieres. C and D Coy were releived and sent back into reserves D Coy is about 60 strong 24.7.16 
 The 3rd Bn with A. coy of our bn advanced and took the remainder of Pozieres. Another sig and myself ran a wire across to Bde H.Q. a distance of a quarter of a mile. It was cut to pieces by artillery fire before we were half way across. The Bde expects to be releived tonight 25.7.16 
 We were releived last night 
