  
 Left Vignacourt at noon and pushed on 11 1/2 miles to Allonville 13.7.16. 
 Went back Vignacourt with despatch on cycle 14.7.16 
 Left Allonville this morning and arrived at Warloy Baillon at about 2 p.m. We expect to discard packs here and proceed to firing line 16.7.16 
 Left Warloy Baillon. Marched through Albert and proceeded to firing line. 19.7.16 
 Got a slight dose of gas running cross a gully last night. Feel as though the 
