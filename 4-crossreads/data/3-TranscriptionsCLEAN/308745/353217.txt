  
 Copy of letter France 14-4-18 
 Lieut Pockley was killed about 31/2 miles due east of Cachy and just north of Bois de Hangard  We made an attack on the 30th ult. and he was in charge of our left flank company.  During the advance he was severely wounded and died where he fell very shortly afterwards.  On being hit he refused to allow the stretcher bearers to carry him until some of his men who had fallen close by had recd attention.  He died where he fell, game to the last. 
 Yours faithfully E A Clarence 
