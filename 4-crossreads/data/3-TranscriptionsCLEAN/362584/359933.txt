  
 Dhumah Edward St, Glebe June 28th, 1916 
 Dear Mrs Ferguson 
 It doesn't seem as if words could have any meaning now. 
 I couldn't believe that anything beyond wounds could happen to Arthur, from his childhood I have always dreamed such great things for him. 
 Short as his life was it has fulfilled all promises, no one can 
