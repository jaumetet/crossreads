  
 9. 
 remarkably well, & I think will shortly get their commissions. It was the that Robson introduced me to Charker, & we had a long yarn about Arthur, at least as long as we could make it, as there wasn't much time. I asked Robson round to lunch the next day, & arranged for Jack Mant to come too. It was just like the 'Varsity Union again, & we didn't have to make conversation either. 
 Duffy & I called on Mrs Wiss on Tuesday afternoon, & she promised to do everything she could for us. I am going to write & let her know next time I get any leave. 
 It was now time to be thinking of getting back to camp; & I was beginning to feel fairly confident this time that my orders were all wrong, & didn't like the sound of meeting at Waterloo station, seeing that our train had come in at Paddington. Anyhow I decided to make sure, & went up to Waterloo to find out exactly where the train started from etc. this is what I found out. 
 1. Nobody knew anything about us 2. There were no trains to Chiseldon 
