  
 France 
 3.12.16 
 My dear Mrs Ferguson 
 I am still in hospital but getting on very satisfactorily and am able to get up for a couple of hours each day. I am still feeling very week as the result of lying in bed. I am now told that I am to be sent to England for a spell of sick leave. Of course that will be very satisfactory from my point of view & I shall be able to go back to the Battalion feeling a new man. I am writing to Keith tonight and if he is still fortunate enough to be in England he will be the first man I shall look for. 
 The hospital here is a very large place taking officers only & is splendidly managed. It is a Red X hospital; the work done by the Voluntary Aid Nurses is splendid and they are all most charming girls. I shall probably be sent across to another Red X hospital in England and from there allowed to go on leave. 
 Have been having a grand time 
