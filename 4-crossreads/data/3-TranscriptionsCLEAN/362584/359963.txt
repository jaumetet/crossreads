  
 10. 
 3. All trains to Chisldon left from Paddington 4. The last train would leave at 5.30 pm 5. There was a train at 9.40 pm which went as far as Swindon (about eight or nine miles from the camp) where it would arrive at 11.30. This started from Paddington. 
 Feeling rather annoyed, I got in touch with Headquarters & they calmly informed me that they had given me orders for troops on leave from Salisbury, & that the orders I should have received were quite different. 
 Of course that was very comforting, but unfortunately didn't help me in the least, as I could not possibly see the men before nine o'clock. 
 At last the time came, & we were all on the station with the very pleasant job before us getting from Waterloo to Paddington in time to catch the 9.40. A staff major fixed it for us by procuring passes for the tube for the whole lot. I was dazed by his magnificence, & threw him a salute every time his eye wandered round my way. He didn't 
