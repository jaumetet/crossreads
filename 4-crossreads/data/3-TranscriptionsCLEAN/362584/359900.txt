  
 walked round there one afternoon, & know nothing more about it than its name. it was one of the most eventful walks I ever had. Five times in twenty minutes the Medical Officer & myself missed running into various kinds of shells by inches. Russells Top is the best place to be out of, I've ever been in. it was the key of Anzac held by the 20th. Beachy Bill [Turkish cannon] you know, & Soixante Quinze [75 mm field-gun] is the nearest approach to greaze lightning I've ever seen. 
 We were expecting a mail in a day or two. Ever since we've been here we're done very little but read letters. That's why I don't find much time to write. 
 Your affectionate son 
 Arthur. 
