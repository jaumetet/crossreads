  
 France 
 14.6.1916 
 Dear Mrs Ferguson 
 I have just this minute heard the sad news. One of our men went to see your son & was there when it happened. 
 Nothing has upset me so much since I left Sydney. 
 We were all very fond of him & I had rather it had been anyone rather than he. 
 When we were coming over from Egypt I was on the same boat as your son, & as he was the senior Infantry officer on the boat, he was our O.C. We all got on so well together, & could not have wished for a better man to command us. 
 As far as I have been able to find out so far, he was in the trenches having lunch with the officers of 
