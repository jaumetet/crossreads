  
  At Sea 15.6.17 
 Dear Mrs. Vernon As our long voyage draws to its end we appreciate more and more what you good ladies have done for our lads. 
 I can assure you that the lads realise now what you all do for them. I had 
 Given out half that tobacco to the 24/1st some time ago, and was keeping the remainder until they asked for it. Yesterday the canteen ran out of tobacco, so you can imagine how they appreciated their tobacco when I gave it to them.we find the white hats and shoes invaluable for these voyages. 
 Our voyage has been without incident the raider, as you will have heard having been captured by the Highflyer off the East coast of Africa. 
 I will be glad to get to the regt. Again . I have been away too long. With kindest regards dear Mrs.Vernon, and again thanking you Very sincerely yours Douglas Waddell. 