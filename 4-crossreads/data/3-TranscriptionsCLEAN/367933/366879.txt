  
 The A Section of the 1st L.H.Field A is included in our family and we hope to get sufficient financial support to enable us to send comforts to the 4 D.A.C.now in France. 
 We cannot lay too much stress on the fact that the men at present in Egypt must have large supplies of vegetable and fruit. Climatic conditions are unspeakably severe and we must do everything we possibly can to keep our men physically fit. 
 We are going to give them a good supply of xmas fare but we cannot stop at that.  Supplies must be continuous. 
 We are immensely indebted to Mr R L Baker who organised a Carnival in March this year for the various L.H. Funds. Our share was the large sum of 343.10. The Parramatta Lancers have contributed 50 as did also Miss Vera Reynolds and Mrs. Harold Baker. 
 Some few of our supporters contribute 