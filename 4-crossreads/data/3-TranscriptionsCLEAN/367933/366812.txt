
8th February. 1917
The Secretary1st Light Horse Comforts Depot.
Dear Madamyour parcel of 33 pairs of hand knitted socks for 1st Light Horse Details arrived safely yesterday. and I wish on behalf of the men to convey to yourself and all other ladies of the Comforts fund their sincere thanks and appreciations for same.
I also received the two parcels addressed to myself for which I must thank you very much . I am sending them out to Tpr Frater and Tpr Thomas as I do not think they receive very many
Yours faithfully
J.N. MacMillanA Sqdn1st Light Horse Training Regt
[See page 67 for letter from Trooper Thomas]
