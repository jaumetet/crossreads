  
 run by the D.A.P. who publish a paper called  The Route. I do some water-colours of the Transport Fleet. I sell two of them. 
 Jan: 3 Usual routine.  At mess to-day a youth is rather rude to me - my vis-&agrave;-vis, a rough Bushman, turns to the youth and says, "Aven't you got an eart, mate; Can't you see' e ain't used to this kind of life like we are?"  Though rough, the boys were very kind to me. 
 Jan; 4 Usual routine.  Am mess orderly all day. So occupied with painting forgot to draw rations - am in a terrible state; 22 hungry men will be no joke to face. Every morning at 5 I sneak into the sergeants' mess to get a cup of coffee - up to date have had nothing stronger than water to drink as I cannot drink the tea, it being always sugared. 
 One morning , just before Reveille, went to get some tea; it was given me in a metal cup which I cannot drink out of; I said to the cook, "Wait a minute, I'll get a china cup" - this was too much for the lads, who immediately began to call out for their valets and shaving water!  I never heard the end of this incident for some days. 
 Jan; 5 Recently I have been sleeping up on the sergeants' deck; strictly against regulations, but no matterl 
 Jan;  6 Usual duties. Am reading Victor Hugo's "Les Miserables"; 
