  
 Very few troops are left here.  The heat is intense & we are right in the desert with very little shade. There is a refreshment booth run by N.Zealand ladies the other side of the railway lines. 
 Monday 12th June 1916 
 Today a Captain has arrived to take charge of us. An orderly room tent has been erected & we seem to be here for some time. Every day rumours come thro' of intended "movings off", in fact several times a day. 
