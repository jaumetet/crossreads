
Friday 11th August
Today Yesterday at 2 p.m. the five of us went to Marseilles.It is about 6 miles from here (Sante) to the centre of the town.The most remarkable thing about the town is that nearly every other woman is in deep mourning.Soldiers there are in plenty & among them one sees a great number of decorations.  Some have four or five medals & the majority have the strip of gold braid on the sleeve, denoting a wound received in action.  (? service)
