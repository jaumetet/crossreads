  
 we shall take a back seat. 
 Thursday 3rd Aug. 1916 
 Everything in regard to arrangements for comfort of troops in order. Everything going like clockwork.  I must admit that there is no room for an accusation of slackness or wilful neglect such as  I have had to make  could have been made in regard to other troopships I have been on. Lifeboat drill yesterday afternoon.  Met Sgt. Hodge one of our reinforcements at Anzac & Orderly Sergeant for 3rd F.A. for some time. Had a good yarn. He gave me a photo of our 
