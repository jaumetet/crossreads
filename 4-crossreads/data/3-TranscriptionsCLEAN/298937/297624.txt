  
 Wednesday 26th July 
 This morning the news arrived by phone that we were to leave in half an hour (which we did not know of till later).  As it was impossible to go at such short notice, it was cancelled & other orders issued. 180 were to leave on Friday or even tomorrow for Alexandria & the rest are to follow on Saturday. As the same  tale  news applied to all battalions along the line, & as the New Zealanders left last night, I believe this is "dinkum". 
