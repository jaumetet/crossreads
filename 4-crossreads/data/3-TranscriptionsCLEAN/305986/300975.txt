  
 Two million Beer Wasps die; Shackleton has met his fate; The Bulgars fly on Bikes; The Kaiser runs a "two-up" school Dutchmen flood their dykes 
 The King is now in New South Wales And says he's gong to stay; Zeppelins bomb a few more babes; Yanks are making hay. And this goes on for ever, And seldom is it true, But I know for a blankey cert That Tomorrow we'll have stew 
 Many a ship's been lost at sea For want of a skilfull Master. Many a Tommy has lost his tart Cause Anzac's Proved the faster. 