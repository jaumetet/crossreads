  
 Against our knightly foreman Are worthy of their well loved land As e'er were knights or Yeoman 
 The Atavastic Maid 
 Listen, Sweetheart to my plea Cut this highly cultured game All this fine gentility Grows to be exceeding tame What I want is lowbrow love Heavy, knock down, cave man stuff I'm no cooing turtle dove Treat me rough kid, Treat me rough 
 Can the soft and weepy sighs Chop the meek and humble prose 