 1915. 
 Dec. 1. 
 Scapa Flow, & filled up with coal.  Here we heard of still further destruction of German submarines.  It was said that, in one case, 9 of them were caught about to lay mines, by our patrols which came on them suddenly before they could make good their escape.  The patrol vessels captured them with crews intact.  Snow fell heavily during our stay here, but its appearance was only for the space of a few days, conditions then becoming milder & the snow vanishing.  Whilest we were at Scapa Flow a collision occurred between the two new ships "Barham" & "Warspite", while the Battle Fleet was out at sea.  This accident was kept a dead secret as far as possible, so that the information could not leak through to Germany, for obvious reasons. 
 Dec. 7 
 After gunnery & torpedo exercises were completed, we left for our base at Rosyth, where we arrived the following day, without mishap. 
 Dec 8. 
 Spells of bad weather were frequent during December & there was an occasional frost. 
 Dec. 25. 
 Xmas. Day was spent on board, all ships being at 2 1/2 hours' notice for full speed.  A light cruiser squadron with destroyers, was sent out to look for any enemy movement, it being thought that he would probably like to display his activity on Xmas. Day, but, however, nothing was seen.  The day was spent very quietly.  Divine Service was held at 10 a.m. & afterwards the men were 