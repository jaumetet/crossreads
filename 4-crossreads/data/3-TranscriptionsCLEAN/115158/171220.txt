Officers on board H.M.A.S. Australia.  Aug. 1914.
Rear Admiral: Sir G. E. Patey. K.C.V.O.; R.N.
Secretary: Fleet Paym. C.E. Rodham. R.N.
Flag. Lieut: Lieut. Commander.  B.R. Po&euml;. R.N.
Clerk to Sec: Asst. Paym. L.S. Brown. R.N.

Captain. R.N. S.H. Radcliffe.                 Sub.Lieut. R.N. C.M. Merewether. Commander R.A.N. G.F. Hyde.             Asst. Paym. R.N. L.S. Mosse Robinson
Lieut. Comdr. R.N. C. D. Longstaff;        Asst. Paym. R.A.N J.D.Jackson
                            J.G. Walsh.             Gunner. R.N. D. Ogilvie.
                            H.C. Allen.                                  (T).J. Wilkes.
                            A.R.A. Macdonald.                        A.C. Newton.
Lieut. R.N. W.M.V. Lewis.                                          A.E. Shute
                (G).F.C. Darley.                                         W. Gregory
                     D.C. Pillans.                   Boatswain R.N. A. McCutcheon.
                (T). J.G. Crace                     Signal Bos'n R.N. - Phillips.
                      O.C. Warner.                 Carpenter. R.N. A.H. Martin
                      C. Euman.                     Warrant Armourer. - Prideaux.
Lieut. R.A.N. - Hodgkinson.                   Midshipsman R.N. H.H. McWilliam.
Lieut. R.N.R. H.L. Quick.                                                  C.F. Cresswell.
                    W. Williams.                                                W.H. Bremner.
Eng. Comdr. R.N. W. Crump Johnson.                             F.A.B. Haworth-Booth.
Eng. Lieut. Comdr. R.N. R.A. Lee.                                       A.L. Pears.
Eng. Lieut. R.N. H. Bleackley                                             H.A. Packer.
Eng. Lieut. R.A.N. T.W. Ross.                                            R.R. Lyle.
                            S. Beeston.                                          E.E. Hill.
                            C. Bridge.                                      R.G. Fenton-Livingstone.
Fleet Surgeon. R.A.N. A.R. Caw                                         J.B. Findlay.
Fleet Paymaster. R.N. R. Wardroper.                                  J.W. Pinkey.
Surgeon. R.A.N. W. Ramsay Smith.                                   G.H.E. Molson.
                         W. Scott Mackenzie.                               E.W. Billyard-Leake.
                         W. Hornibrook.                                        V.R.S. Bowlby.
Clerk. R.A.N. J. Hehir.                          Chaplains. R.A.N. Rev. E.W. Riley.
                     A.E. Sharp                                                 Patrick. G. Gibbons.
Artificer Eng. R.A.N. H.A. Vary.
                               C.A. Reid.