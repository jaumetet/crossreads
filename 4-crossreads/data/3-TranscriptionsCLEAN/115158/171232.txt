  SEA WAR ROUTINE 
 Three watches - Red, white & blue, from "Man and Arm" & "Darken Ship" to "Hands Full In" at 7.5 am.  Then work as usual in two watches. 
   
 2.00 am  Cocoa etc. at Quarters 
 3.45 am  Call the Watches (Name them) 
 3.50        Watches to muster etc.  (Name them) 
 4.00         Relieve the watches (name them) 
 % 5.00     Cocoa etc. at Quarters 
 % Daylight.  Scrub Decks.  Unrig Night Defence. 
 6.35       Watch & Stand by watch, lash up & stow. 
 6.45   Hands lash up & stow. 
 %7.5  Hands fall in.  (Stand fast forenoon war watch) if so piped.)  Steering Hammockmen fall in.  Clean Mess Decks and flats. 
 % 8.00  Breakfast etc.  Forenoon war watch to relieve if piped. 
 8.40  Out pipes 
 % 8.45  Cooks and sweepers clear up mess decks.  Remainder fall in.  Clean brightwork & clear up decks. 
 % 9.20  Divisions and Physical Drill. 
 9.45    Clean Guns. 
 10.15   Both watches for exercise.  Divisional Drills as necessary, and refitting work. 
 % 11.15  Secure, if work is finished.  Clear up decks, then pipe down. 
 % 12.00 noon  Dinner. 
 12.30 p.m.  Afternoon war watch to relieve if piped. 
 % 3.25   Cooks. 
 3 . 30.  Hands to tea & shift into night clothing. 
 4.00    Afternoon war watch to tea & shift into night clothing. 
 4.15    Hands Man and Arm ship (Stand fast afternoon war watch).  Then wash clothes, etc. 
 5.55  Second dog war watch muster at guns (if piped).  Steerage hammockmen 1/2 an hour before sunset, stand by hammocks 1/4 hour before sunset.  Man and Arm ship & darken ship at sunset. 
 7.10  Cooks 
 7.15  Supper.  Stand fast War watch (Name the watch) 
 7.40  Stand by war watch to muster. (Name the watch) 
 7.50  First watch to muster. 
 8.00  Relieve the watch  Second dog war watch to supper (Name the watch) 
 8.25  Watch below for exercise fall in, clear up decks for the rounds.  (Name the watch) 
 8.45   Rounds 
 11.00  Cocoa etc. to men at Quarters. 
 11.45  Call the watch  (Name the watch 
 11.50  Middle watches to muster.  (Name them) 
 12.00  Relieve the watches. 
  NOTE.  
           Time marked % to be reported to the Executive Officer.  No one is excused "Exercise Action", "Night Action", and Drills.  Men are not to smoke at their quarters.  Navigation Party are excused after Divisions, except for "Clear Lower Deck" and Drills. 
   
   
   
   
   