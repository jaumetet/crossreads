 1915 
 April 21. 
 We had not very long to wait either, for 24 hours after coaling, we were at sea again. Enemy wireless signals were heard, but although we went as far as their minefield off Heligoland, we saw nothing of them.  
 April 23. 
 Early one morning we passed close to a steamer & a sailing ship, both Norwegian, & both on fire, the work of a German submarine. The crew had, apparently, been rescued by a passing steamer, probably the day before, because their small boats were drifting about empty. There was no sign of the German submarine. "Fearless" sank both ships, which were endangering traffic.  
 April 23. 
 We returned to harbour the same night, having failed once more to find the enemy. The German newspapers boasted that their ships were at sea, but failed to find the hated British Fleet. They could not have ventured far from their base. 
 April 26 
 Whilst in harbour we commenced to make smoke issue from all funnels in the evenings, hoping thereby to mislead spies, who constantly watched our movements at Queensferry. This was done by burning rags, etc. at the base of the funnel, causing the ship to smoke as if she were raising steam in all boilers. 
 May 7. 
 The sinking of the "Lusitania" by a German submarine, without warning, aroused intense indignation throughout the world, & it also found the Admirality still at a  