                 SHIPS IN PACIFIC OCEAN  
 
 
 
  Nationality  
  Ships  
   Guns   
 
  Torpedo  
  Tubes  
 
  Speed  
 
 
 British 
 
 "Minotaur" 
 
 
 (4-9.2 inch 
 (10-7.5"   
 
     5 
    23 
 
 
   
 "Triumph" 
 
 (4-10" 
 (14-7.5" 
 
      4 
    19 
 
 
   
 "Hampshire" 
 
 (4-7.5" 
 (6-6" 
 
     2 
    22 
 
 
   
 "Yarmouth" 
 8-6" 
     2  
    25 
 
 
   
 
 "Newcastle" 
 8 T.B.D's 
 3 Submarines 
 Gunboats & Sloops 
 
 
 (2-6" 
 (10-4" 
   
   
   
 
 
     2 
   
   
   
 
 
    25 
   
   
   
 
 
 
   
 "Australia" 
 
 (8-12" 
 (16-4" 
 
     2 
   271/2 
 
 
   
 
 "Sydney" 
 "Melbourne" 
 
 8-6" 
     2 
    25 
 
 
   
 
 "Encounter" 
 3 T.B.D's 
 2 Submarines 
 Gunboats & Sloops 
 
 
 11-6" 
   
   
   
   
 
 
     2 
   
   
   
   
 
 
   20 
   
   
   
   
 
 
 
   
 
 "Psyche" 
 "Pyramus" 
 
 8-4" 
     2 
    18 
 
 
   
 "Philomel" 
 8-4.7" 
   
    18 
 
 
 
 
 French 
 
 
 
 "Montcalm" 
 
 
 
 (2-7.6 inch 
 (8-6.5 
 
 
 
     2 
 
 
 
    21 
 
 
 
   
 
 "Duplix" 
 3 T.B.D's 
 Small Gunboats  & Sloops 
 
 
 8-4.7 
   
   
 
 
     2 
   
   
 
 
  21                          30 
 
 
 
 
   NOTE : Above is a list of warships in Pacific   Ocean on outbreak of war on Aug. 4th 1914. Later Japan declared war, which increased the preponderance of Allied ships  
 Enemy nations are underlined in  Black . 
                                                             (OVER 