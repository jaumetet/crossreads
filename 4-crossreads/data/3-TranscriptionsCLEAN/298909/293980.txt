  
 went to evenings devotins & Father King gave us about an hours humouros anecdotes to pass away the evening, very hot all day & dusty. 
 15/5/16  Monday 
 Even hotter than Saturday - Very hot wind blowing - it was cruel drilling - I took a few Photos of the Boys in our tent, went for a swim in the moonlight - we all seem to agree that we would like to try our luck in the firing line in preference to staying another three months. 
 16/5/16  Tuesday 
 Very hot - Drilling from 5 am till 9 am - rest of day we were trying to keep the dust out of the Tent. 
 17/5/16  Wednesday 
  122  degrees down in the operating E.P. Tent - I have never & hope to never again experience similiar conditions. A dust storm raging all day, you could not see 50 yards ahead & the wind was that hot it almost burnt your face.  At 12.30 pm, two stretcher squads were ordered out over to the Lake trenches, 