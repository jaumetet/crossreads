
19/5/16  Friday
Fairly hot. Not feeling too good yet. Had an easy job on Guard all day. Had a good dip at night.
20/5/16  Saturday
Rather cool all day - at night it must have been down to 70 degrees - it was too big a difference & nearly all the Camps have colds. All our leather equipment was called in & weather web gear issued, also our colours, only for our crosses, it is hard to distinguish us from the Infantry - Had the day to myself.
21/5/16  Sunday
First Mass at 4.30 am - second at 5/30 am. I went to second - Done very little all day.
22/5/16  - Monday
Quite a respectable day - only for the flies & dust. Went down to the Clearing Hospital & rigged up some large Tents. Had a grand swim at sunset, a large Boat passed & threw over some Cigars & plug tobacco, needless to say there was a great scramble in the water.