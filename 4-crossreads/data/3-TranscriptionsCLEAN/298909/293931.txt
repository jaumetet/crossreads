  
 17/3/16 Friday 
 A Surprise Fall in about 5/30 pm they wanted 36 AMC for somewhere - 14/2nd L.H.F.A volunteered & hopped out to a man only to be turned down again - Very cold at night. Was Orderly Corporal first time today - got along alright - had a lot of running about & felt it with sore heel. 
 18th/3/16 Saturday 
 An awful dusty & windy day - very hard on the eyes - Had charge of a fatigue party at Headquarters at Helmieh - finished up at noon. Myself & the boys went to the Zoological Gardens - It was both an education & an object lesson - to see the number & variety of Birds & Animals - Giraffes - Hippotamus - Alligators - Zebras - Elephants - Snakes - Vultures - Dogs - Wolves, Foxes. There are several variety's of Kangaroo's & Wallaby 