  
 very fair pictures Charley Chaplin etc. 
 1/4/16  Saturday 
 Very hot all day - Cleaned up the lines till 11 am. No passes granted till 3 pm. Went into Cairo & was going out to  Gizeh  - none of the party had passes, so the picket, would not let us over the bridge, in fact we were lucky not to be detained. Went to a variety show at night - after making a good inspection of the fashionable parts of Cairo - round about "Shephard's Continental etc. 
 2/4/16  Sunday 
 Very cold last night - Orderly Corporal - never said a word of course as Sunday is the best day to be on tours etc - Didn't mind so much when news came through that general leave of all Camps was stopped till further 