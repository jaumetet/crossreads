
27/4/1916  Thursday
Had a swimming parade at 6.15 am. Drilling on the sand all the morning. We had to get up about 5 am & roll out as an airship was located, it turned out to be one of our own, so we were quite disappointed at not having a bit of fun. Had a Kit inspection in the evening, they called in a lot of stuff & only left us with a bare change - it means we shall not have quite so much to carry. Wrote to Tom O'Hagan.
Can notice the Gun Boats throwing out their search lights all around them all night.
28/4/1916  Friday
Quite a cool day. Had two good swims. Stretcher drill & lectures all day.
29/4/1916  Saturday
Had two good swims - Struck tents & cleaned up the Camp - While having a swim two large boats passed thro the Canal, but women or no women on board, the lads were in stages of dress or undress, most of us swam alongside.