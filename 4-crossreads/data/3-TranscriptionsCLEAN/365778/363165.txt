  
 Monday 1/1/17 Major Read 28th Bn reported in lieu of Bridges sick. Work progressing well. Tuesday 2/1/17 
 Bridges evacuated - things going well. 
 Wednesday 3/1/17 
 Route march in Marching Order through Daours. Afternoon (holidays) went over to site for Rifle Range. 
 Thursday 4/1/17 
 Wisdom arrived with Murphy. I left for front line to see 26th Bn. 
 Friday 5/1/17 
 Spent day with Battn at Pommieres stayed night at Bernafy Wood. 
 Saturday 6/1/17 
 Returned to Bussy at midday - Wisdom & Murphy left. 
