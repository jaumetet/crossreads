
Monday 14/5/17Training commenced in earnest, had letter from Fred.Tuesday 15/5/17Visited by General Gough who came to watch the training. Saw Bas; took men for stiff march.Wednesday 16/5/17Raining, lectures in huts, had a game of Bridge.Thursday 17/6/17Musketry, Battalion shot well, raining.Friday 18/5/17Marched at 10.30 am for Bouzincourt, had midday meal on road - now in camp near Martinsart.
