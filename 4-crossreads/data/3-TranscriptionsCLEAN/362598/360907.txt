  
 Sunday 2 January 1916 Church Parade & Sacred Concert at night.  Troop-Deck Sergeant. 
 Monday 3 January 1916 Troop-Deck Sergeant.  Cards at night. 
 Tuesday 4 January 1916 
 Wednesday 5 January 1916 [This entry crossed through.] Half-way between Fremantle & Suez.  Mock trial.  Bob Carter Clarke. 
 Thursday 6 January 1916 [This entry crossed through.] Crossed the line early this morning. 
 Friday 7 January 1916 
 Saturday 8 January 1916 
