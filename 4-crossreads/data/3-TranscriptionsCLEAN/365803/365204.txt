  
 and the unfortunate Coy Commander is dreadfully busy. The men are all on splendid condition but have had plenty to do. 
 Intelligence here is exceedingly  cheering & hopeful. I feel that there is a good time coming. 
 Fondest love to all I cannot say anything else. 
 Yours affectionately 
 C R Lucas 
