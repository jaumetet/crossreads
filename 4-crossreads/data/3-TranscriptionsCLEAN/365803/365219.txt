  
 Third Australian Division. Lark Hill Camp. Salisbury Plain. 26th July 1916. 
 Dear Judge, 
 Your letter of April 27th to Colonel foster and his reply to you of May 6th have only come into my hands to-day as the course of the post is now very protracted. 
 Far from my considering it officious of you to bring under notice the special qualifications of Dr Brissenden of the 3rd Cyclist Co., we are always very glad to heat from authoritative sources of the special qualifications of any of the personnel. - However, unfortunately the 3rd Cyclist Co. no longer forms part of the 3rd division but all cyclist companied have been otherwise grouped; nevertheless I shall make it my business to endeavour to locate this unit and to ascertain the commander who is responsible for same so that I may communicate to him the purport of your information. 
 Apart from this action, in case for any reason it should fail, I would like you to know that tour experience is that any man of education and character speedily forge their way to the front and it is quite likely that when Brissenden acquired the necessary technical military knowledge he will be speedily singled out for preferment to a higher rank. 
 The principal advantage of knowing that he is a barrister of high standing will be that one will know exactly where to go if the services of such a man are required, as they no doubt will be, in the capacity of Deputy Judge Advocate or similar office. 
 Thanking you very much for your communication. 
 Yours very sincerely, 
 John Monash Brigadier-General Commanding Third Australian Division 
 Mr Justice Ferguson N.S.W. 
 Mr Justice Ferguson N.S.W. 
