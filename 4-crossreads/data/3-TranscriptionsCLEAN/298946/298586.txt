
our running on one engine.  This afternoon the boat has been going at something like its old speed.
Early morning communion & morning service at which I helped with distributing the books as usual I have been attending the 6 oclock services every evening down in the aft hold, but ommiting the Sunday evening services.
There is one on at present, a memorial service for Geddes who went overboard.
The weather today has been splendid.  This afternoon I lay down & had a splendid sleep & sun bath.
Thursday 14th Aug
Today like Sunday we travelled very slowly all morning on one engine & only went 248 miles.  The mileage is going from bad to worse.
During the week we had two good lectures by P.W.F. Wallace: one