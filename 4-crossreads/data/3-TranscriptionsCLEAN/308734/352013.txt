  
 Wednesday Jan 23rd 7.15 got up, had a good nights sleep, ear getting better.  9.30 went to Dr with report from Dr at Sutton Veny.  had my nose douched.  During afternoon we were all put into Isolation again, owing to a death from meningitis.  Draft left for France. 
 Thursday Jan 24th  77 Days from Melb 7 AM got up.  weather very nice & mild.  Had buzzer & flash reading for morning.  1.30, parade for pay (15/-).  3 PM to 4 PM Gas mask instruction on how to put them on. 
 Friday Jan 25th Weather still mild.  went to Dr again about ear, then helped to clean up.  8 of the chaps were told they had to go into isolation at Sutton Veny. 
 Saturday Jan 26th 6.30 everyone busy getting ready to get away.  8.30 the Sutton Veny crowd marched away.  only leaves 10 in our class now.  Had to go to Dr. with special report about my ear. then had to help clean out class rooms.  no parades for afternoon, wrote letters.  had to go through Formalin. 
 Sunday Jan 27th 7 AM got up, my turn for mess orderly.  8.30 and 10.30 had to go on parade for Formalin disinfectant.  no Church parade. 
 Monday Jan 28th Weather very raw.  Had an examination on flags & Disc. during afternoon had some more formalin disinfectant. 
 Tuesday Jan 29th The school were all re alloted to new classes.  Went to Dr again about my ear, 3 of them had a look at it, had it syringed and hot compresses put on it, said it would have to be opened up.  2 PM was transferred back to No 2 battery.  Did nothing all afternoon.  5PM had my ear attended to again. 
