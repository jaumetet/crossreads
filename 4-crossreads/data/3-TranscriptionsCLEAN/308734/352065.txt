  
 Warminster with Rowl King to get some Photos.  arrived back in camp 9.30 tired and footsore.  Five months since I left Australia. 
 Wednesday April 10th up at 6.30 had been raining heavy all night.  had the usual gun laying, jerks and lectures had to go night gun laying at 8.30 PM. 
 Thursday April 11th put in another application to get into the saddlery. Our class was detailed off for guard, had rifle exercises till smoke oh, then jerks and lecture.  2 PM had a medical inspection then dismissed to get cleaned for guard.  3.30 PM parade at Bty Office, got off guard as I was the best dressed and cleanest man on guard.  printed Photos the rest of the afternoon. 
 Friday April 12th 12 of us were on Q.M. Fatigue all day.  had to sort old clothes, one of the dirtiest and smelly jobs I have had, had to make up old shirts and flannels into bundles of ten.  After tea went to Concert at Anzac, was rotten so came back and went to bed. 
 Saturday April 13th passed in my paybook on early morning parade.  8.15 AM Gun laying till 11 AM then Jerks till 12 AM.  Filled in the afternoon with photographs.  Made a leather case for Jack Bowring to hold [indecipherable]. 
 Sunday April 14th 6.45 had to clean stables till 7.30 AM.  Went to Church service at C of England to make sure of getting to church.  11 AM had a hot bath (very nice too).  Went to Fellowship class at Y.M.C.A.  After tea helped to Wash and dry the dishes.  Wrote some letters home. 
 Monday April 15th new time table, have to get up half an 
