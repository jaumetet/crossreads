  
 23/8/17 Windy & hot today. we went out for a walk this afternoon needless to say we perspired some during the walk.  Nice & fresh now & again enroute. apples. galore. 
 24/8/17 Rumours of a move it is for sure this time?  I wonder.  dirty day today.  Splendid war news from Italian & French front. 
 25/8/17 Announced that we are to be ready to move sunday next or monday   rain today   of course the packing of kits are in full swing.  2 pair of sox or 
 26/8/17 German equivalent. Parades Galore today counted about a dozen times.  we are to move off tomorrow morning at 6 oclock  Revillie at 2.30 AM 
 27/8/17 we fell in at 4 oclock pitch dark left camp 6 AM march 
