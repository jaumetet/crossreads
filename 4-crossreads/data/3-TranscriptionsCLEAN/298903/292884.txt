  
 on my way.  Afternoon called on F. Gissing, had tea there & then did the pictures arriving back at the hotel at 11.30 p.m.  Collected Australian mail at L.M. the first for many moons. 
 Sunday 23rd Feb.  Wrote letters in the morning.  Afternoon went to see D.W.A. at Bayswater but found they had shifted so did not see them.  Had tea at the Aldwych Y.M. then went to a concert in the Queens Hall splendid orchestra & good music.  Heavy cold on me. 
 Monday 24th Received a ring from D.W.A. giving me the new address so went round for tea and dinner.  Arrived home about 11.30.  Sleeping much better lately the threatened insomnia disappearing. 
 Tuesday 25th Feb.  At 2 oclock met D..W.A. at Earls Court & took tube to Kew then went over the gardens.  Looking very well considering the weather & war conditions 
