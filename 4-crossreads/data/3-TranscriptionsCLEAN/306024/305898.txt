  
 though not elaborate. After the dinner the C.O made a short speech and gave his greetings. 
 [The following paragraph crossed through:] As usual his subject was of the [indecipherable] attainments of the Battalion and the rewards gained. I must say here that it was the first occasion on which it did ring true and sincere to me. Without a doubt he was extraordinarily brilliant, but to the general mind, lacked the bond of open sympathy with the men. 
 While at Sivry we had a good deal of snow and much amusement was obtained from sleighing, or rather attempts at that sport. As the sleighs were only built for the use of the local small child, humorous accidents were common. Snowballing was of course the  carried on  usual thing. 
  Another chap and I were fortunate enough to get away.  After we had been there a few days the people  took to us  made themselves very obliging and on account of the other battalions leaving for fresh areas there was abundance of accomodation and very 
