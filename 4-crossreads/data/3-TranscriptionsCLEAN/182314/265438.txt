  
 Rabaul 
 13.1.1915 
 Dear John 
 Since Colonel Holmes was so nice to me I would like him to know that Col. Petherbridge has asked me to stay on and today went further & asked me  to  if I would like to go south to bring back my wife. Major [Heritage?] has been a [brick?] but I don't think he cares much for the new regime. Ravenscroft returns to his Company. The [indecipherable] returns tonight with Morrison & Chambers- I believe Martin is on the way in a destroyer  E. Coy lost a Sgt in the surf at Litape. I expect to [indecipherable] in February in the [indecipherable] & if you are still in [Sydney?] I will [look?] you up. The [indecipherable] returns [today?] from the island after [indecipherable] trip. Blackett is [full?] of it & says they are [great?] coconuts islands. It looks as if Col. P. is going to let [me have my own?] department after all so I am happy. [Tull?] has [collared?] Col [Winter's?] house so I am taking Fry's when he leaves. The [indecipherable] Gov t.  [Store Keeper?], but so far they haven't [indecipherable] a Treasurer. 
 I hope you will have found your little wife well & flourishing & that you are [indecipherable] yourself. 
 With kindest regards to Colonel [Holmes?] [Indecipherable] to Basil & yourself. 
 Yours sincerely, 
 [Guy V. Manning?] 
 [Written in left hand side margin:] 
 I went up the hill at 6 pm on the day you left & the [indecipherable] up 1/2 way up the hill. Tell Basil. 
 G.M 
   
