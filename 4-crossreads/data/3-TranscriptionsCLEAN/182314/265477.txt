  
 [Form letter] 
   
 War Office, 
 Imperial Institute, 
 South Kensington, 
 London, S.W.7., 
 23 rd  October 1919. 
   
 The Secretary of the War Office presents his compliments to Mrs R.J.A. Travers, and would be glad to be furnished with a reply to the letter from this Department of the 22 nd  April, No. 121/H/3449 on the subject of the late Maj:Gen:William Holmes, C.M.G., D.S.O., V.D., Spec:List. Staff Aus: Div: H:Q: 
 Mrs. R.J.A. Travers. 
