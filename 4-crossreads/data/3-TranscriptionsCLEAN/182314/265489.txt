  
 Government House, 
 Sydney. 
 29th May, 1920. 
   
 My dear Travers, 
 I think I need hardly tell you how fully my wife and I appreciate the fact of the excellent arrangements made by your committee for the reception and entertainment of us during our stay in New South Wales. The private letter which I wrote to you yesterday on the subject will I think have shewn you what we feel, and I shall be grateful if you will now convey our formal and grateful thanks to your committee; and also thank them so much for their good wishes for us for the future. 
 We both of us so much hope that we may meet you again some day. 
 Yours very sincerely, 
   
 [Signed] [W.R. Birdwood?] 
