  
 7th Australian Infantry Brigade. 
 [Stamped: Headquarters. 7th Australian Infantry Brigade. No 114/255 Date 6th July 1918] 
 "I am to inform you that the G.O.C., A.I.F., has approved as a special case of Lieut. Colonel R.J.A. Travers, DSO., 26th Bn., being permitted to return to Australia for termination of his appointment in the A.I.F. 
 Arrangements are being made by A.I.F. Administrative H.Qrs., for his early passage to Australia, and it is desired that he should proceed to London at a convenient date to await passage. 
 Please submit through Headquaters, Aust. Corps, your recommendation for the command of the 26th Bn., vice Lieut. Colonel Travers." 
 H.Q., 
 26th Battalion . 
 With reference to above copy of letter from D.A.G.A.I.F. 
 Herewith Warrant for Lieut. Colonel R.J.A. Travers, DSO., to proceed to A.I.F., H.Qrs., London. 
 Please report date of departure to this office. 
 [Signature] 
 Captain, Staff Captain. 
 7th Australian Infantry Brigade. 
