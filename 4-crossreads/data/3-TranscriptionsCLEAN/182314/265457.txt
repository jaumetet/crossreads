  
 7th Australian Infantry Brigade 
 December 21st 1916. 
   
 Lieut. Colonel Travers, D.S.O., 
 26th Battalion. 
   
 2nd Aust. Div. Provisional Battn. BUSSY. 
 Administrative arrangements will be as follows:- 
  Transport : 1. Six G.S. Waggons complete and one spare pair of horses have been detailed from 19th Reserve Park for attachment to the Battalion to provide Transport. (Authority 1st ANZAC 1 908) 
 5th and 7th Brigades will each detail one water cart for attachment to the Battalion. 
  Supplies : 2. On 31st inst. each Brigade will send the rations for 22nd inst. by 56th A.S.C. Lorries from the refilling point at BECORDEL to BUSSY Church. 
 From 23rd inst inclusive, the Battalion will be rationed by 1st ANZAC CORPS TROOPS SUPPLY COLUMN, who will deliver at BUSSY from 22nd onwards. H.Q. 7th Aust. Inf. Bde will forward indents to S.O. CORPS TROOPS SUPPLY COLUMN on 21st inst. 
 Indents for Supplies will be sent direct to S.O. CORPS TROOPS SUPPLY COLUMN daily from and including 21st inst. 
  Ordnance : 3. Camp equipment (Camp kettles, [Soyers?], Stoves, Blankets, Brooms) are being supplied by ORDNANCE 1st ANZAC who will advise H.Q. 7th Aust. Inf. Bde direct when the stores are available. 
 Indents for clothing etc. for the reinforcements to replace U.S. etc. will be submitted by H.Q. 7th Aust. Inf. Bde direct to O.O. CORPS TROOPS. 
 Care will be taken that indents are submitted on the proper days. 
 Indents should be sent in to 7th Bde H.Q. in sufficient time to allow of indents being forwarded to O.O. CORPS TROOPS on the days set apart. 
 Care will be taken to obtain receipts for all Ordnance material issued, with the exception of Blankets, which when once issued, become Regimental property. 
 [Initialled J.T.] 
   
 [Signed] [Indecipherable] 
 Capt. 
 Brigade Major. 
 7th Aust. Infantry Brigade. 
   
 Rec'd 
 5 p.m. 
 23 rd  Dec 
 [Initialled] [Indecipherable] 
