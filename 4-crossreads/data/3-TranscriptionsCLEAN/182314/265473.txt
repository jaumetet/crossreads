  
 Australian Base Depot, 
 Monte Video Camp, 
 Weymouth. 
 28th. January 1916. 
   
 From:- Commandant. 
 To:- Major Travers. 
       Australian Imperial Force, 
       Bostall Heath Camp, 
       Abbey Wood. Kent. 
 Memo:- Under instructions from General Officer Commanding: Australian Troops: United Kingdom:, you are to proceed with the next draft to rejoin the Mediterranean Expiditionary Force. You are therefore requested to report at this Depot on Sunday next, 30th. inst. 
 Railway Warrant enclosed. 
 [Signature] 
 Lieut.-Colonel. 
 Commandant, Australian Base Depot. 
 Enclsoure. 
 F.G. 
         
   
   
