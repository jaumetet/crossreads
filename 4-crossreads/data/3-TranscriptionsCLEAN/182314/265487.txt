  
 War Office, 
 London, S.W.1, 
 23 rd  October, 1919. 
   
 121/H/3449. 
 Madam, 
 I am commanded by the Army Council to inform you of the wish of H.M. Government that the next-of-kin of all officers and men who have fallen in the defence of their country should receive a plaque and scroll, bearing the name and regiment, as a memorial of their patriotism and sacrifice. 
 In order to ensure that the plaque and scroll reach the next-of-kin of the late Maj: Gen: William Holmes, C.M.G., D.S.O., V.D., Spec: List. Staff Aus: Div: H:Q: I am to enquire whether you can inform this Department of the name and address of the late Officer's  nearest  relative  now living , in the following order of relationship:- 
 Widow 
 Eldest Son 
 Eldest Daughter 
 Father 
 Mother 
 Eldest Brother 
 Eldest Sister 
 Eldest Half-Brother 
 Eldest Half-Sister 
 [The following sentence has been placed over the top of the original letter:] In order to avoid the possibility of any error in the inscription of the memorial, the spelling of the late Officer's Christian names should be carefully checked. 
 I am, Madam, 
 Your obedient Servant, 
 [Signed] [R.M Drade?] 
   
 Mrs R.J.A. Travers, 
 Cairnleith, 
 Littleheath Road, 
 Mosman, N.S.W. 
