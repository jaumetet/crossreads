  
 At 5.20am & we were off trailing it up. Smoke clouds were so dense that we could hardly see two yards. 4th Bn reached first objective forward of Hargicourt. 3rd bn went on through us after another barrage at 8.26am & on to 2nd objective in the defences of the Hindenberg Line 
 Thurs Sept 19th:- Casualties were very light in Bn - only about 30. Plenty of prisoners roped in & we are now supporting the 3rd Bn. 
 Sat Sept 21st:- Relieved tonight by the 7th Bn in support line & moved back via Hargicourt to Hesbecourt & put up in dugouts. 4 men in Coy were wounded coming out. 
 Mon Sept 23rd:- Marched from Hesbecourt at 9 am via Hervilly, Roisel, Marquaix to huts between Tincourt & Buire. 
 Tues Sept 24th:- Marched through Buire, Courcelles, Doingt, Peronne, Quinconce & Clery & bivouacked at side of river near Busnes. 
 Thurs Sept 26th:- Marched back through Biaches to near Peronne & entrained for Longpre, passing through Rosieres 
