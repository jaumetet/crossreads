 France 22.2.17 
 My dear Judge 
 I am very well & in good fit condition. The mud is everywhere & in everything. The cold weather has left us for the present but as a matter of fact we would all prefer the frost & ice to the mud. I am again temporarily commanding the Battalion the Colonel being away on leave. This means that I shall take the Battalion into the line next tour of duty there. I am enjoying the position immensely (at present). 
 I heard today that Keith has dropped into a good staff job. How I hope that it is true. I hope to find out definitely tomorrow. There is no news. Everything is going well. 
 Best Love Yours affectionately 
 C.R. Lucas 