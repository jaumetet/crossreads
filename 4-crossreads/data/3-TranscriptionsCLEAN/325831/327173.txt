 he was setting a good deal for his people. I expect it has all petered out by this time & I hope it has. 
 The weather in the South of England is not what I hoped for, very wet & cloudy & cold & I am afraid New South has spoiled us all for this country. Perhaps the winter here is a bad average this year but it doesn't appeal to me in the least. I shall be glad to be back in Australia again. Glad to say this leaves me well. Please give my kindest remembrances to Mrs F & Dorothy; Mr & Mrs Carter [on side of page] & to Miss Twan. 
 With all good wishes & regards to yourself 
 I remain very sincerely 
 M M M[?] Sinclair. 