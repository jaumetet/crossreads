 2. This pen, on the other hand, is the worst scratchiest & most filthy contraption in city of London. 
 Miss Miles & I are going to the theatre tonight - 'Home on Leave - which is supposed to be very good. We also hope to do many other things, circumstances permitting, all of which I will tell you about when we have done them. 
 Have a good time in N.Z., & don't worry about the war because it's going to be over in about a month or so. 
 Remember me mushily to the family 
 Yours in the most respectful & loving taste 
 Keith 
 P.S. If you send letters through the bank, I receive them between a week & 12 months earlier than through the Expeditionary mail. 
 Another P.S. I sent you a cable yesterday. 