 8. old 26th Battn in Sydney & later of the 4th AIF is now acting as Brigadier to the 1st Bde. 
 He played in a football match against our battalion about two weeks ago. One enthusiastic young subaltern, during the game, had a violent little difference with someone who was hanging on to the ball & spoke his mind a little too expressively - to his subsequent horror he found he had been ill-using & abusing his Brigadier. I do not suppose there is any previous record in military history of a Brigadier taking part in a football match on active service. 
 Well pour le present, au revoir 
 With best wishes to Mrs & Miss Ferguson & yourself 
 I am 
 Yours very Sincerely 
 J L Flannery   1st Bn 