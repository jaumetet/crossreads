  
 Friday 23 February 
 I had a little sleep on deck during the night.  The trip was very smooth.  We reached Le Havre at 3 a.m. disembarked at 8 a.m. and marched to a rest camp some distance from the wharf.  Issued with tickets to provide meals, and allotted huts to sleep in.  Sea planes were flying over the harbour as we came in and two airships were busy in the air all day.  The huge sheds for housing these machines are next to our camp. 
