  
 Monday 16 April 
 Short march this morning before breakfast.  This incinerator job is a very soft one allowing us practically the whole day to ourselves.  This time is spent near a good fire, reading or writing. Showery again today and very cold. No further orders are on relative to moving. 
