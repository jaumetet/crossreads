  
 Monday 22 October 
 To 1st Anzac Corps. Hd. Q. To Poperinghe To 14th Field Ambulance The Signal Service consists of 2 parts - The Telegraphic or Telegram Department and the Despatch Rider Letter Service.  In the first case the messages, written on telegram forms, are sent by wire.  In the second place the papers are marked D.R.L.S. and are delivered by Despatch Riders who have regular runs throughout the day. 
