  
 [Map drawn of area and villages around Bapaume] 
 A.S.C. - Army Service Corps A.D.M.S. - Asst. Director of Medical Services D.D.M.S. - Deputy Director of Medical Services Exp. Force Canteen - Expeditionary Force Canteen C.C.S. - Casualty Clearing Station D.S.C. Workshops - Divisional Supply Columns Workshops D.R.L.S. - Despatch Rider Letter Service A.P.M. - Assistant Paymaster 
 Quent now known as Queant Aveloy - Avluy Wardreques - Wardrecques Boesingen - Boeseghem Wittel - Wittes Godewaerevelde - Goewaersvelde Dickebush - Dikkebus Westoutre - Westouter Flerte - Fletre Poperinghe - Poperinge Reninghelst - Reningelst Brandoek - Brandhoek Vlamertinghe - Vlamertinge Dranoutre - Dranouter Locre - Loker Flete - Fletre Wytschaete - Wijtschate Oultersteen - Outtersteene 
 [Transcribed by Judy Gimbert and Patricia Ryan for the State Library of New South Wales] 
