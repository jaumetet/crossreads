  
 Saturday 14 April 
 Glorious day.  All leave to bearers from this camp has been stopped thanks to two men getting drunk in  Armiens  Amiens yesterday.  They were run in by the Military Police. On incinerator. Excitement caused by news of the Arras Offensive.  13,000 prisoners reported in papers. Our little hut it a picture of comfort.  The fire seldom goes out. Route march this morning before breakfast, as yesterday. 
