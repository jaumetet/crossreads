  
 Monday 22 January 
 Bitterly cold.  Our boots get frozen and have to be warmed and softened over the fire before we can put them on.  Went to a concert tonight in a big new Y.M.C.A. Hut near Montauban and it proved a very fine one indeed.  Some really good artists are to be found in the Army. 
