  
 Sunday 11 March 
 A beautiful day.  Had nothing to do but stroll round Albert.  Shells seem to have ceased coming over this way. 
