  
 Judges' Chambers, Supreme Court, Sydney. 
 3 May 1915 
 My dear MacLaurin, 
 I open my letter to say how proud we are of the news that is coming through.  We know very little yet - only that you have landed on the Gallipoli peninsular & are putting up a big fight.  And now we are beginning to get the casualty lists.  They hurt. 
 The first news we got was a cable from the British Government congratulating Australia on our men's splendid bravery & magnificent achievement.  Then a cablegram from the King.  But they didn't tell us what it was all about.  It was this that evoked from Adam McCoy "A Rhyme for Censors" in the Sun.  Now the news is trickling through, but we are told that there will be no more communiques till the next stage of the operations is complete. 
 The Rabaul business is unpleasant.  Cols Watson & Paton are being kept there till the enquiry is over I understand, to give evidence, I suppose.  It's hard luck for them, they will have to wait for the next contingent. 
 I saw Jim McManamey 
