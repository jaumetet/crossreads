  
 You can bet your life we always turn up smiling. They can sing their hymn of hate Till their vocal organs grate, An' extend their bloomin' frightfulness in strafing. But we listens good an' tight, With a smile of pure delight, And its 'Eaven 'elp the blighters when we're laughing. 
 C.W.C. 
  London Opinion  
