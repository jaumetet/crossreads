  
 19th Batt Popes Post 3-2-15 
 Lieut Cullen, The Court Martial of No Pte Davis D Coy takes place at 0200 today at 17th Batt Quinns Post. You are a witness in this case. Will you please let me know if illness will prevent you from being there. A Gray RSM 
