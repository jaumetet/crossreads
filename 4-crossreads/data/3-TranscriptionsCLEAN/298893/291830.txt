  
 19th Batt Popes Post 6-12-15 
 O.C. C.Coy Lieut Boyden, Will you please let me have 3 men for a bomb fatigue to go to Anzac tonight. Fatigue will report to a N.C.O. at Brigade water tanks at 6.50 PM 
