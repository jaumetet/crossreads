  
 F. Windy. [4/3/17 Sun] Recd. letter from John Lawton. Billy comes over. I go to L. Hill for a walk. Wrote to Mem. Nette. Auntie A. Bess. Sent. Tabs for Q.O.M. 
 F&W. Snow [5/3/17 Mon] Ellis made Sergeant. I write to Queen & H.Hey. Ellis arrives from Gy [Germany?]. at East Post. Nothing starting doing 
 W [6/3/17 Tues] Wrote to Jennie. John Dad & Mum. Very wet day. I do the pay for 1/- issue 
 F [7/3/17 Wed] Letters from Maisie Hudson - Marjorie. 5 from Home. Bessie P.C. from Mrs Clarke. 60 Patients come 60 Patients sent to P.House. 
 F [8/3/17 Thur] Letters from Marg. Went to bed sick last night at 5pm & am sent to staff ward to stay in bed. I have a sponge down. 
 W [9/3/17 Fri] Letter from Dulce Butell A little bit better 
 Snow [10/3/17 Sat] Improving 
