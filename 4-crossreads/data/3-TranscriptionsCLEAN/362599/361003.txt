  
 F [29/4/17 Sun] Wrote to Laura & Bess. Played for Kirk. Go to Kirk at Bulford Sent P.C. to Doris & wrote to Dulce Butell. Saw induction of new Parson & heard archdeacon preach. 
 F [30/4/17 Mon] Lovely day. Wrote P.C. to Emmie Johnstone. Inoculated again. Play cricket on oval. Arm very sore. Go to bed at 8pm. Sleepless night on account of arm. Pay Parade. 
 F [1/5/17 Tues] Wrote to Marion. Ida & Wal. Amy. Played cricket again. Have Cricket meeting. Practise. Polling Day starts. Go to bed very late at 10.30pm 
 F [2/5/17 Wed] Ellis goes to S'Bury. I play cricket am No 11. Score 3 runs but put on 30 for last wicket. Am called Ranji by patients on Oval. Dave is Captain. Go to bed very hungry. Win Match. 107-47. Glorious weather. 
 F [3/5/17 Thurs] Glorious & Perfect Day. I go to Albany Ward's alone. Send films to be developed. Lovely moonlight night. Dreamt of Marjory & Queenie Westy. 
 F [4/5/17 Fri] Recd. letters from Esme & Jennie. Wrote to Jennie & Practised Cricket again. Do no geed at all. 
 F [5/5/17 Sat] Very hot. Go to S'bury with Keogh & Dave. Go to Mrs [space] for Tea. Movie & walk around River [Avon-not the same one as at Stratford] walk & Cathedral Close. Have plenty of nice food etc. Very cold at night & having left off Cardigan catch a very slight cold. Had letter from Mem & Cyril. 
