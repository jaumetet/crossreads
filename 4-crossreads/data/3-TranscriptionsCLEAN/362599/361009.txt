  
 F [10/6/17 Sun] Wrote PC. to George. Capt. Ellis visits us for a few Hours. After dinner E & I with Hunt Mac & Krogh go to Bulford for a swim. It is very cold but we enjoy it very much. I go for a run & then sit in bed to finish my white shorts 
 F [11/6/17 Mon] Spend an Hour at the nets & have a few sprints with some N.Z. Sgts. Do no good. Recd. letter from R.L. Sit in bed & answer it. Ellis goes to Amesbury on Bike. 
 F [12/6/17 Tues] Go to S'bury to my Friend. Go for a walk to the Riverside Walk & go to Organ Recital. Have Tea & supper there. Have usual good time. Pay Parade. 
 F [13/6/17 Wed] Recd. letter from Jennie. Wrote Home & to Cyril. [Indecipherable] again I am improving. E. goes to Salisbury. Hear that D. Lord's Brother has his hand blown off. 
 F [14/6/17 Thurs] Played Cricket for 3 hrs & then trained for running. Was Reliev. Corp. had to go to every ward, finish at 11 with S/Sgt. Bertie. Had 2 papers from Home. 
 F [15/6/17 Fri] Gen. Shep. Goes to P.Down Recd letter from Peggy. Bess & Dad. Go to Wards with CW Hunt. Recd. paper from Isle of Man. Promotions come out for more N.C.O's. dated 24.4.17. Wrote to Peggy. 
 F [16/6/17 Sat] & Hot as Hades. Con don't come & we walk to Bulford & get one. Run in Race & get 2 or 3rd in heat all Pro's out & was outclassed. Had awful walk back. We have Tea in our Room with Scotty & Ted Cox. We are knocked out completely. The Country is looking an absolute Picture but rain is wanted Badly. Our Boys get trounced at Cricket 
