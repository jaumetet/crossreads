  
 W [11/3/17 Sun] We have concert by the Razzle Dazzle Comedy Co. in our ward. We have the song "A little bit of Ribbon, a little bit of lace, & a little bit of silk as well" 
 F & Dull [12/3/17 Mon] Wrote to M.R.L. Dulce Marjorie Hudson. Margaret M. Read Book of Charles Dickens Reprinted Pieces 
 F&W [13/3/17 Tues] Disch. from Hosp. Return to my Room. Wrote John Lawton Billy came over from L. Hill 
 F [14/3/17 Wed] Recd. letter from M.R.L. Wrote to M.R.L. made a pair of white drawers. Started indoor work again. 
 F [15/3/17 Thur] Recd. letter from Aunt. Annie Ellis goes to lark Hill on D.C.M. all day. E. D. & I go to the theatre at night. 
 F [16/3/17 Fri] Wrote to Mrs Clarke & Dad. Mum & George. Ellis goes to DCM at 2pm. All unit classified but me. I am too busy. 
 F [17/3/17 Sat] Glorious Day & Dave & I walk to Fighledean. We meet Billy & His Cobbers & have Tea with them. We walk Home round through Durrington. I go straight to bed on my return pretty knocked out. 
