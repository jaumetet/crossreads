  
 Desert Life 
 I spilled some water on desert sand. (It's far too precious to waste.) The effect was like a grasping hand, Imbibing in frenzied haste. Swiftly it soaked in the desert's thirst, Like raindrops on furnace fires Raced with the hot wind to drink it first Quenching the angry desires. 
 The desert, our neutral enemy He's partial to neither side - Mocking at mortal's impunity And the powers that divide. The sentry rides on the dim skyline of a barren wilderness, A woeful camel train, the only sign of life, in his loneliness. 
 Picture the Bedouin desert born! And his wall of imagination, Picture the Trooper Austral born, And the hot sands inspiration. Our lives seem likened to waterdrops As we ride through pulseless heat For man's own soul the desert sops, Achieving his great defeat. 
 Alf P.K. Morris MED Sergt. 12th A.L.H. 
 Hagley Park Outpost Sinai Desert 21/8/16 