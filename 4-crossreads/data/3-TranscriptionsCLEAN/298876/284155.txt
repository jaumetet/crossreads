  
 Sunday 12/3/16 
 About 200 Catholics marched about 11/2 miles into Zeitour Convent & attended Mass at 7.30 am - This is a very nice Convent & run by French Sisters. We passed around the hat & got a good few shillings for them. 
 About 100 of us took on a half day trip with a Guide named Simon, one of Cooks Reps - There were 20 Cabs employed & cost each man 20 pt. After driving through the rich & poor quarters of Cairo, we visited the Nile, which we crossed by native ferry - went thro Pharaohs Daughter's Palace, also saw where tradition has it that Moses was found in the Bulrushes by Pharaohs daughter - saw the Nilometer a marble Pillar erected 716 AD. Visited Coptic Church, 600 yrs AD built over the Cave in which the Holy Family rested during their flight from Palestine to 
