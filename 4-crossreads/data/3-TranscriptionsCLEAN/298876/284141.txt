  
 Wednesday 1/3/16 
 Very muggy all day & night, sea very calm. Land came to view about 7 am. Typical Picture book country of Arabian & Egyptian country, very mountainous quite a study in almost every ridge & mount - mostly of a very barren dry appearance. So different to our Australian Country & even the mountains are a novelty - shapes pointed - some conical - some as in terraces - some perfectly round - oval & so on. 
 First glimpse of civilization since Colombo, is to see a business like fort on the Arabian side. I think they said X "Fort Mocha" - also an inlet or secluded Bay   thus for cruisers etc to shelter load & coal etc. Then we came to several picturesque Islands - one they said Solitary Islands & their imposing Light Houses or Wireless 
 (Mocha Coffee)X 
