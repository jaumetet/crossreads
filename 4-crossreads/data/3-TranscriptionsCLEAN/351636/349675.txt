  
 Dear Mr. Layton - I am sending on Mrs. Carey wife of Jack Tarbut Carey 1857 15 Batt. returned wounded & left again for the front 9th R. 14 Batt. 
 She will explain her case to you as it is somewhat unusual. 
 Yours truly B. Booth. 
