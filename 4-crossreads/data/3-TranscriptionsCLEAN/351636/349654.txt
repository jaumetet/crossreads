  
 The Centre 92 Pitt St. Sydney August 17 - 1915 
 Dear Mrs. Erwin, I find the War Chest is not able to give anymore, so I am enclosing a form for you to fill in for the Lord Mayors Fund. 
 Please add also a list of you debts and we shall send a letter of recommendation for you from the Centre. 
 I have pencilled in a few remarks which express what you want - Fill up the form fully. 
 I am Yours very truly Mary Booth P.S. hope you are very well 
