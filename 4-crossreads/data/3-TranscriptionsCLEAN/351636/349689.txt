  
 The Centre 1st February 1916 
 Dear Mr. Layton Can you help Mrs Tueiley - she seems in very real need. Her military payments are small, and though she had help from the Patriotic Funds when her husband was in Camp, she has managed to keep going, but now finds it almost impossible  to keep  as her husband is only allowing her 3/- not 4/- per day as he told her. 
 I do hope you will be able to help her. 
 Yours faithfully [indecipherable] A Bolton 
