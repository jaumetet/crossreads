  
 camp (free) pictures tonight, not bad, 
 Wednesday 17th Oct. fine but prospects of rain, was put on R. Boat Roll (Supplementary) & warned not to leave camp, an hour later a couple of dozen were told they were on the roll & the rest practically struck off. All those struck off came from Codford with me. We are very unfortunate alway being messed about. Concert tonight. 
 Thursday fine, was on butcher's fatigue yesterday afternoon cutting up meat for the stew. K boat roll went today, the seat collapsed from under me & made my headache again. 
 Friday fine, on sick parade. M.O. wanted me to go to Hospital, but as it would affect my going Home I refused, he put me on no duty for 3 days. 
 Saturday Oct 20 still have headache fine though at times look like rain, boat roll in but hasn't been called 