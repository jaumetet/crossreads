  
 Situation Report 14/10/17 
 Everything OK Casualties Nil Change of Personnel Nil S.A.A. expended 3,000 
 This position is becoming less tenable artillery is just in rear & we are being heavily shelled by howitzers, 5.9 & shrapnel. enemy aircraft all out on its own. Can you give me any idea what time tomorrow relief will take place. Plenty of water, rations, S.A.A.  Rum issue received. PHS Weston 