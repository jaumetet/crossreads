  
 O.C. 13th M.G. Coy. 
 Everything OK. S.A.A. expended 2,000 Casualties Nil Change of Personnel Nil Relief by 24th M.G. Coy complete & satisfactory. Heavily shelled during night. Charts handed over to incoming Coy. 
 PHS Weston Lt 15/10/17 6.30am 