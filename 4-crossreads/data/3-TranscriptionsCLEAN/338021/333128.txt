
To OC 13th M.G. CoySituation report
New gun received & mounted last night.  a.a.a. Am returning Pte Neal as his leg is too stiff to be of any use in action. O.C. took away spare parts last night. My co-ordination was wrong apologise a.a.a.
Thanks for letter & cigarettes.
Casualties NilS.A.A. expended NilHeavy firing occasionally during night by enemy artillery. No damage
CHP Weston Lieut8.15am 25/9/17