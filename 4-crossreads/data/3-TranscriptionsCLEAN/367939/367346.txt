
At the Front28/9/1915
Dear Ted
Another line trusting it will find you all well leaving me just the same still in the trenches I am still looking for mail but devil a one can I get but it will come some day I suppose
how is poor old Dah getting along now. Hutchy is still away I cannot hear of him
is Sid still thinking of coming he wants to get it out of his head his knee would never stand it as Infantry we may get our horses again after a while I hope so