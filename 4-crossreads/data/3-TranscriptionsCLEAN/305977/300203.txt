  
 which contained the fish of the Nile.  Went on the afternoon patrol.  There were very few in town so had an easy time. 
 Thursday 23rd  [Apr] Nothing to do all day or at night either, so had a good sleep in camp, in the morning & a stroll up town in the afternoon. 
 Saturday 24th Did no duty during the day.  There were a lot of soldiers in town in the afternoon, but they got out very quietly & when we went in at 10 oclock there was hardly anyone to be seen.  We made no arrests at all, but let several go.  It was about 2am when we dismissed.  I wrote and posted a letter to Australia likewise one to mother. 
 Sunday 25th A very hot & dusty day.  Had an easy morning in the reading room & in the afternoon had a stroll around. 