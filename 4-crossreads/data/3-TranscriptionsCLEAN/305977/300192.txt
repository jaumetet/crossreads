  
 March 1915 controlled on enemy in trenches. 
 Friday 26th Our Squadron were doing patrol work in the morning & firing drill in the afternoon.  We were outlying picket at night.  The 1st Brigade went back to Camp at Heliopolis after returning from Helouan where they went on a route march. 
 Saturday 27th Was on duty all day at the canteen.  The Brigadier inspected the lines in the morning.  Came off duty at 9.30pm. 
 Sunday 28th Usual church parade in the morning.  Had leave in the afternoon & with McCarthy went out to Heliopolis & saw Jack Davidson.  Came back to Cairo with them, & went to St Andrews Church of Scotland.  There was a good service & some fine solos sung. 
 Monday 29th Was mess orderly with Thorne.  Did no parades.  Wrote 