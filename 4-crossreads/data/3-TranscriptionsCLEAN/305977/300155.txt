  
 & the Ayrshire is right behind. 
 Sunday 3rd  [Jan] Church parade at 9.30.  Boat stations & fire stations afterwards.  Like any other week day on board ship.  Selected a good spot to sleep on upper deck. 
 Monday 4th The weather is getting warmer as we near the tropics.  We are steering N.N.W.  Must be going to Colombo.  A lot of boxing was done tonight.  They are having a tournament starting from tomorrow night.  Entered my name. 
 Tuesday 5th Was put on stable picquet.  Got a bosker headache in the morning & was feeling pretty crook after dinner.  Was right again after tea.  Boxing tournament started.  They made 