  
 July 1915 till four 
 Thursday 15th Came back to our dug-outs at 6 & slept or tried to till 12.  The afternoon was very hot.  B & C Squadrons came out & took over the trenches in front.  We had a fairly quiet night & I managed to get in 4 hrs sleep in the bottom of the trench. 
 Friday 16th B & C Squadrons got to work on their dug outs behind their support trenches.  John Turk could see us from their look out at Gaba Tepe & he put in 4 high explosive shells.  We worked in the smaller portion after that.  I did a solid day with the pick & shovel.  Was on covering party at night.  The Turks did a bit of firing & tried to bomb us but they didn't get close enough.  Got relieved at 1am. 
 Saturday 17th We went into the trenches at 6pm & came out at 6am.  Was on fatigue in the afternoon getting bags up from the beach for the new firing line. 
 Sunday 18th Went back into the trenches at 4pm. 