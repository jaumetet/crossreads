  
 France 17.9.17 
 Dear Dad I am sending you my medal for safe keeping.  I received it yesterday from Gen. Birdwood in Clairmarais Forest, where we are now resting.  (Near St. Omer, Uncle Emil will know it well.)  I am giving it to a mate to despatch from England, which I think will be a safer route. 
 Your affect. Son George. 
 Recd. 22.11.17 