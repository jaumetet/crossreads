  
 gas burns.  The latest gas is invisible & has no smell, and one does not know it's over until seized with a fit of sneezing or else gets burnt by it. 
 Bryant Sheehy came to see me about a week ago, and he is looking splendid.  He has filled out a good deal. 
 I hope soon to visit some of the friends of the Husband of my Father's Sister;  as we are not very far from a certain town  where  mentioned in a couple of the addresses he gave me.  Furlough to England has started again, and I expect to go in about two months time. 
 Best wishes, Your affect. Son, George 