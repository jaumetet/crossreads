  
 surplus supplies onto borrowed ambulance wagons we returned to the billet & commenced to fry steak for our tea. This finished we turned in & never moved until 9am next morning. 
 8.11.16  Wednesday 
 Raining hard & very cold. The column arrived at 10am & we have been working right up to 4pm. The transport arrived at 12 noon after a rough passage & the whole village was soon one mass of vehicles, all looking for their respective camps. However after about an hours confusion things quietened down again & once more resumed the normal. 
 The Battalion returned from the trenches today & they looked very sorryful & miserable. They were mud plastered & tired & altogether done up. 
 It came out fine this afternoon & aeroplanes were fairly active. Distant sounds of artillery fire reached us this afternoon & several explosions of bursting shells came from Albert way. 
 9.11.16  Thursday 
 Rained all night & came through our barn like a sieve. Our Blankets were  saturated & we spent a rotten night of it. All of us have caught colds & it is  a regular coughing match among us. There was considerable 
