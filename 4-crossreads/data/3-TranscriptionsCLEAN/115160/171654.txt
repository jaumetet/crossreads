 Page 139 
 [Leave or Duty Ration Book No. 025609] 
 [Name} Drummond J. 
 [Rank] Pte 
 [Number] 13754 
 [Unit or ship] 5th Aust M.T. Coy 
 [Proceeding from] Field 
 [Beginning of leave] 8.8.18 
 [End of leave or duty] 22.8.18 
 O.A.S. 
 [Signed] A. J.C. James, Major 
 5th Augst M. T. Coy 
