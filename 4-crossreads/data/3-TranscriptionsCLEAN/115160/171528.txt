 Page 13 
 3 
 Sunday 29th 
 It was a lovely morning but it has come on to rain again.  Last night we saw a Fritz caught in the lights.  One of ours was after him, but after dropping his load in the bushes to get away, he managed to regain his own territory. 
 I have been in the habit of including most of the letters I receive from Scotland when I send my "Greenie".  I'm afraid this will stop, for more stringent censorship forbids this practice. 
 I sent today a few illustrated newspapers and a German sand-bag enclosed.  You will note that it is all paper too. 
 Well, Love, there is not much else to mention at present.  I am quite OK and hope you are all in the same good health and spirits.   
 Fondest love to Bess & Lock, and all the best for yourself. 
 Your loving Son 
 James xxxxxx 
