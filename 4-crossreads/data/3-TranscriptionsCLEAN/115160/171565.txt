 Page 50 
 Among the Ruins of Athies near Peronne 
 23/11/18 
 Darling Mither, 
 The week-end is round agin, and during the week censorship regulations have been relaxed to the above extent. 
 I received two parcels a few nights ago, one with a bosker cake and t'other with Bess' Balaclava and sweets for which I am very thankful.  The cake was in excellent condition and not mildewed at all. 
 Frosty mornings are still favourite, although the sun manages to break through during the day.  It is of course pretty fresh, but I am keeping well. 
 24/11/18 
 Funny thing how Dad managed to tip in his last letter that I would have been well past the Hindenburg Line.  Yes, we were right up as far as Bohain that time we were sent forward.  We had an opportunity of seeing some of his defence works which were taken by our great lads.  It was a regular eye-opener.  We are not very far from it as I write. 
 25/11/18 
 Foggy but mild.  May be on the move in a day or two.  Wild rumours are everywhere as to our destination - perhaps Coblenz, perhaps demobilisation for Australia. 
 Will write in a day or two, 
 Fondest love to all 
 Your loving son, 
 James xxxxxx 
