 Page 100 
 89 Campbell St 
 Riccarton 
 Aug 29th 
 Dear Jim, 
 I was so pleased to have your letter yesterday with the good news you were well.  Jim the censor did make a job of it had he cut up in two leaflets & left one I was at a loss to read it, the interesting parts were all cut out allow Mr. Censor he wont let much pass him. I did see by the papers you were all so busy in France & we know that, hear too according to the large list of killed and wounded war office notices being delivered in this town just now we have had some great surprises, poor fellows they have paid the supreme sacrifice & what for is all this fighting surely Jim 
