 Page 132 
 2 
 
 
 
 Fresh vegetables (when obtainable) 
 8 ozs 
 
 
 or Dried vegetables 
 2 " 
 
 
 Jam - now 2 oz 
  3 "  
 
 
 Tea 
 1/2 " 
 
 
 Sugar 
 3 " 
 
 
 or Sugar (when Sweetened Cond milk issued) 
 21/2 " 
 
 
 Condensed milk 
 1 " 
 
 
 Salt 
 1/4 " 
 
 
 Pepper 
 1/100 " 
 
 
 Mustard 
 1/100 " 
 
 
 Pickles (thrice weekly) 
 1 " 
 
 
 Tobacco or Cigarettes (once a week) 
 2 OZS 
 
 
 Matches (thrice fortnightly) 
 1 box 
 
 
 Lime Juice (when authorised) 
 1/160 gall 
 
 
 Rum (when authorised) 
 1/64 " 
 
 
 (b) Equivalents: 
   
 
 
 Frozen Meat 1 lb = P Meat   
 9 ozs 
 
 
 which includes Veg Rn or M & V 
 1 tin 
 
 
 or P & B 
 2 2/9 tins 
 
 
 
   
