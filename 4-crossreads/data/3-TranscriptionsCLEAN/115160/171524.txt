 Page 9 
 6 
 not be too plentiful at first.  As offsets to this, though, there are the Repatriation Scheme and the going ahead of the new industreis that have opened up since it started.  There are a terrible lot men who will not go back to their old employments, and I can see the land being more popular with the advance of the Railway System. 
 I don't think any of the Greenock folk have any idea of going out except Robert who is not yet out of his time.  If he is an allround tradesman there should be opportunities there for him now that we are started on building our own ships.  The same applies to Mary's husband - Nichols, who is an allround boilermaker. 
 No doubt it will take some time after the finish before Australia can resume her Immigration Policy, for there will be the big difficulty of shipping space for some time.  The altered conditions that are bound to  
