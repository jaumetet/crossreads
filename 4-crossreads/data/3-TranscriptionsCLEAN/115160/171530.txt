 Page 15 
 France 14/10/18 
 Darling Mither, 
 I knew when I was sending off last week's letter to Lockhart that I would not have much to add to it for you, and much less time to do it on the Sunday.  And so it turned out for we were away at Railhead by 8 am for the busiest morning we've had on the job.  But we got through it quite well, notwithstanding that our boss left that morning for his leave in Italy.  We were already packed up, and after dinner set out on a long journey away back.  It was 9 o'clock when we got to our destination, and we had to hang around to get a place to pitch a camp.  It was well after 1 o'clock before we were fixed up for the night.  Well, isn't the news good.  It looks as if the war is pretty nearly on its last legs and we should be able to have our own way. 
 I have just received Lock's latest  
