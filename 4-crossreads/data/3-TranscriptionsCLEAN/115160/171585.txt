 Page 70 
 Beaumont, Belgium 
 29/12/18 
 Darling Mither, 
 We left Laudrecies over a week ago at 8 am in the rain.  Going up a steep hill our bus came to a halt but after a bit of persuasion we got another go on only to finally break down further along.  It was dull and cold but it was on a main road that we stopped so we soon had a tow from one of our own lorries that happened to be passing.  We swopped on to another at the Column, and got here about 2 pm. 
 We have had the trains arriving  
