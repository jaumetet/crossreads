 Page 63 
 [On letterhead of War Chest Fund, Sydney, NSW] 
 Landrecils, France 
 17/12/18 
 Dear Lock, 
 Always pleased to have your letters even if they are short and sweet at times.  I see Poor Old Dad had a rough spin for a while. 
 I was glad to have the Balance Sheet and notice you are keeping things going very well.  I trust you were able to get in your debts well and made no bad ones.  The war being over - or practically so - thinks ought to look up a bit on your side.  Has the butter problem improved now: I hope so.  You had been having some good draws too.  Try and get your stock taken round about the 4th of March, for by doing it then, it gives  
