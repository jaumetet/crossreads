 Page 58 
 their experiences would make your blood boil.  They have not seen their daughter for over 4 years, being carried off by the Huns, and Monsieur came back after 18 months a prisoner in Germany full of rhumatics & haggard through want of nourishment. 
 It was a little over a week before the span of the armistice is up, when we ought to know what to look forward to.  As it is, everything is somewhat unsetted, altho' it would appear that there is little likelihood of a resumption of hostilities. 
 All sorts of hearsay float about as to what is going to be done with us, but there is little that is definite at all.  The Education Scheme seems to be taking a bit of shape, but being scattered like Pa's Cows, I don't know what our Unit is doing at all. 
 There is little news to mention, but I may be able to drop a few lines in a few days, by when possibly some October mail may have reached us. 
 I hope you are all in the very best of health.  I'm O.K. with a bit of a cold, but plenty of Emulsion and a wee drop o' rum to keep it in check.   
 Fondest love to Dad Bess & Lock, 
 Your loving Son James xxxxxx 
