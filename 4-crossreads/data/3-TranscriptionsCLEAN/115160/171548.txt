 Page 33 
 2 
 another draft for which I am very grateful.  I wrote you some time ago on the matter and will be pleased if you will discontinue until such times as I again write you.  I am still holding well over here.   What you have sent will not come amiss and will be handy when I want it. 
 I enclose a bill for some purchases I made when finishing with my leave.  It was my biggest expense. 
 Well, Mither, things are very cheerful looking, aren't they.  Germany has come a gutser at last and left to battle away on her own.  On paper it looks as if the end will come any minute, but I some-how have the idea she will stick it to the last fence hereself.  Once  
