
in the recent offensive (still in progress) the newspaper accounts of the fighting are enough to make any Australian proud. The reports are written by British Correspondents too. I believe the Australians were going out for the Winter after this stunt, but it was such a success & the casualties were comparatively so light that they are coming in again. I've got a loan of a runners Bike & am learning to ride now.
Wednesday 26.9.17  Poor old Cheeseman got killed a day or two ago. I don't think I mentioned that Furnell G. & his brother & Major Hockley  were  all died of wounds caused by the same shell. Baron Von P. Schott came back last night on his own initiative McLannan is doing 14 days at the base for taking a fall out of the M.P.s.
27.9 The energetic Fritz favoured us again last night. He was very sociable &  generous. Between shutting off the light for him and stopping to replace the belt on our refractory Dynamo the lights seemed to be more often off than on. It is an eerie sensation to look up into a cloudless sky on a moonlight night & hear the drone of one or more planes apparently quite close & yet be quite unable to see them. One of them caught in the glare of the searchlights immediately started dropping bombs. We could see the red streaks as they came down (probably incendiary bombs)
29.9 SaturdayFritz came over again last night. Our searchlights were going all the time. They caught one who got
