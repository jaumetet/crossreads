  
 19/10/16 Crossed the line 
 24/10/16 Arrived at Dakka had feed of fish 
 2/11/16 Arrived at Pymouth & went to Wareham 
 3/11/16 Went to Hurdcott via Southampton 
 24/11/16 London four days leave. Met Jack Wirth & went to Butchers Ernie dead. 
 11/12/16 London again to bank. Good trip home. 
 14/12/16 France at Boulogne (Blanket Hill) 
 15/12/16 Etaples 
 23/12/16 Buire to battalion Good Xmas dinner. Spent New Years Day with (Archie) Griff, Ben & Strange. 