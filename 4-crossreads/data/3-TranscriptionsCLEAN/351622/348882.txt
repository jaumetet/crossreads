  
 30 August 1916 Wed At 2 am. I sent a letter on its way to Os. The busiest night we have had since we arrived here some 100 cases came in. It was awful falling & slipping all over the place in the dark in the mud. Raining all day as usual. I slept all day. 
 31 August 1916 Thur Raining very heavy. During the afternoon a Canadian field Ambulance come to relieve us. Went up the town to see the Brigade coming through. Saw Ronny & Mac & marched along with them for a little while. Mac was wearing the military cross ribbon. Saw old Jimmie & joined in with him for 100 yds or so 
