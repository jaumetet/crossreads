  
 burst over in one of the paddocks. 
 4 May 1916 Thur Spring is almost on us now & the result is that the fields & pastures that surround us are beautiful covered with green grass & the trees covered with blossom. That big German gun has been letting go pretty frequent as usual all day. Good night if one hits here. You can see them [text continued at top of page] 
 5 May 1916 Fri Very busy all day Captain Lonnen brought in. Very fidegty. Spent a lot of the day in the estaminet with Steve & in the evening with Marsh & [Snow] Harris. Terrible bombardment to-night the 20th Battalion right in the thick of it, several wounded 2000 shells in 2 hours. One coves brother killed [indecipherable] 
 [Note in margin] Eric & [indecipherable] leave for England 
