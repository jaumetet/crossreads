  
 School. Jimmie left about 7 oclock. This has been a great day for me. 
 20 May 1916 Sat Jimmie came to see me from Erquinghem. A great meeting adjourning to the estaminet at 11 am & stopping there till 7 p.m. Poor old Jim very broken up concerning the death of Frank. Several grogs all day & Bell joins up with us after he had finished his work at the gas [text continued at top of page] 
 21 May 1916 Sun Up at 5 am a most beautiful day. Started work with Cecil Davis in S. Ward. Cec has argument with [Brem] & leaves to go on night duty. After dinner went into Steenwerck with Ken where we made our usual stay in the estaminet. Bottle of Cider. Arrived back early & enjoyed a good bath & a change of clothing. 
