  
 15.8.16 Sent cable home. 
 14 August 1916 Mon Any amount of work about 60 patients in our ward now & only Dick & I to look after them. Jeff Woods takes over night work in our ward. Visited the next door estaminet several times throughout the day. After tea spent a few hours with Q.M.S. Sharpley of 17 Batt. in the estaminet. Played Pontoon till bed time. 
 15 August 1916 Tues Arose 6.am. & gave out the different medicines in the ward before breakfast. Dick Powter leaves for Head Quarters as a witness in the Court Marshall case between Col Phipps & Sgt McDonald. [indecipherable] Power takes his place here. Many of the boys return home full of merriement. Pontoon school starts at 9 p.m. & Sgt Thomas cant sleep for noise. Picks up his blankets & evaporates. Saying I 
 [Text continued in margin] havent [indecipherable] a man yet but I will get some sleep. 
