  
 I did not see him but recognised his dear old voice on pulling up the flap at the back. 
 8 November 1916 Wed Just after breakfast whilst up to my eyes in work I received the sad news that Jim was at last wounded & lying over at the 15th Dressing Station. I immediately slipped over & was just in time to say a few words to him as the car was on its way to the clearing station [text continued at top of page] 
 [Note in margin] He has had a long run. 
 9 November 1916 Thur Received letter from Mother stating that Vere was on his way back to Australia. I feel very depressed now as all my old friends are either dead or wounded. Norm Burgess receives M.M. for his services in Gallipoli. 
 H. Burcher severely W, Cocky Killed, Hubert Killed, Frank Killed, Roy Jolly Seriously Wounded, Vere Wounded, Norm Lewis Killed, Capt Lonsdale Killed, Jim Fay wounded. 
 Severe bombardment before going to sleep. Taubes busy dropping bombs bombs C.R.S. 6 killed 
