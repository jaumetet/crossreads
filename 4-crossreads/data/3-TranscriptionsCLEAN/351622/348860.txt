  
 billeting in an old stable pig-stye. 
 17 July 1916 Mon The main body moved off from [Picquingy] at 9.30. I stopped behind with the Quartermaster till 5.30 pm. During the day I spent most of the time in the village. Our way out we passed the village of [Breilly], Ailly on Somme & Saint Sauveur. Arrived Longpre - Amiens 8.30 [text continued at top of page] 
 [Note in margin] Letter & money from Linfield 
 18 July 1916 Tues Route march of 30 to Amiens. After dinner & another route march doing for the day about 20 kilometres. On arriving back several of us took French leave to Amiens. I went with Harry Benson, Sid Murphy & McGee. It was great to have a ride in the tram again. Had some fun in the estaminet with the [indecipherable] 
 [Note in margin] [indecipherable] in the river & back 
