  
 Mac & all my comrades in the infantry who will be taking part in that great charge across No Man's Land. Worse even than Verdun. 
 4 August 1916 Fri Started work again at 12 oclock in the receiving room. Wounded have been pouring in all the afternoon. They are quietly dressed & pushed off to the Casualty Clearing Station where they are attended to in better style. To-nights' the night for our Division the 2nd is going over the parapet in hopes of taking the Ridge. My thoughts are of Jimmy Ronny [text continued at top of page] 
 [Note in margin] The past seven days have been the most exciting of my life. 
 5 August 1916 Sat Worked all last night on account of the number of wounded coming in. The boys were very successful in their charge & took the German's first & second line of trenches. Plenty of wounded German prisoners passed through our hands. Poor beggars that seem glad to be captured & werent they hungry. Amongst the wounded was Toby Brerton & Sgt Hill although neither were of a serious nature. Slept all the morning starting work after dinner. The division is expecting to be relieved to morrow for a rest & to reorganise 
 [Note in margin] Bombardment 15 minutes. 9 p.m. 
