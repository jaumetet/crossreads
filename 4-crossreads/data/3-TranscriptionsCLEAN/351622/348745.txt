  
 [Previous pages cover] 
 With Compliments from Joyce Walters 
 [Notes in margin] Capt Harry Ronald D.S.O. wounded. Capt Keith McKenzie M C & Bar wounded 
  Should anyone find my diary please send return to [indecipherable] Newtown  Sydney N.S.W. Australia 
