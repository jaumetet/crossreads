
I knew him from the time he joined the battalion & if I can give you any other information I would be very glad to do so.
Yours faithfullyA.E. Waugh
P.S.  With regard to Beckett & Miller if your list of bombers is on back of photo of King & Queen you will find there names & Reg. Nos.A.E.W.