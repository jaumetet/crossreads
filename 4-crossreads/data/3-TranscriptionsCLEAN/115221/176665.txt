  
 The Young Mens Christian Association. With the British Expeditionary Force 
 Etaples Administrative District. Headquarters, 30, Rue du Rivage, Etaples General Secretary Adam Scott 
 Y.M.C.A. A.P.O.S. 11, B.E.F. France 
 2nd May 1917 
 I have much pleasure in sending you snapshot Photograph of Grave as requested. 
 The Association makes no charge for this Copy. 
 (Signed) Adam Scott, General Secretary. 