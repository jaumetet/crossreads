  
 3 April 1915 Saturday 
 The 4th Brigade gets marching orders but to our great disappointment the reinforcement's are to be left behind. But our range practices are cancelled so think there is a chance yet 
 1915 April 4 Easter Day 
 Nothing to do all day with one of my cpls. visit the N.Z. Artillery   &  examine the new Howitzers   &  Field Guns They are all painted in patches of dark red yellow   &  brown which makes them blend with the ground a lot better than any one color would. A very bad sand storm springs up at dusk 
