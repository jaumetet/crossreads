  
 9 May 1915 Sunday Rogation Sunday 
 After a    quilt  quiet night with only intermittent firing we take    a  stock of the position Our trenches are on the ridge *   &  the Turks are on a ridge opposite about 200 yds away   &  the valley in between is covered with dead bodies of Australians and Turks. At other parts the distance between the Turks   &  us is only from 15 to 30 yds   &  at night both parties throw hand bombs into each others defences * Pope's Hill 
 1915 May 10 Monday 
 At daylight the 13th are called out to support the 15ths positions * (who lost heavily in a futile charge during the night) but were not required. While we were near their trenches the wounded kept coming down in all stages of mutilation, Heads shot off, faces smashed   &  bullet holes in almost every possible place. the sight was shocking. * Quinn's Post. 
