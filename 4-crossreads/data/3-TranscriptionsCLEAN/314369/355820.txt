  
 29 April 1915 Thursday 
 Parades as usual 5 pm Just sat down to tea when our C.S.M. comes in with the news that we are to be off in a few hours   &  to get everything ready to move. We work nearly all night on our Conduct sheets   &  Casualty sheets so as to have the whole day for packing Go to bed at 2.30 am 
 1915 April 30 Friday Read out the orders to the men at 6 am this morning much to their joy Spend the day in preparing to evacuate Our kit bags are all stored in a shed. While all we carry is our packs   &  1 Blanket   &  W.P. Sheet w/- 24 hrs rations. Leave camp amid much congratulations at 8 pm   &  entrain for Alexandria 
