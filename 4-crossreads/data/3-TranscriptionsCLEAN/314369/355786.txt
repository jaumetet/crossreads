  
 20 February 1915 Saturday 
 We remain quiet   &  wait for high tide. 9.30 Flood tide we pass wire hawser again but it breaks as soon as we put on the strain 9.45 To our great joy Bee slowly moves astern   &  frees herself. Luckily she has sustained no injury. She turned about and moved straight forward. We make Thursday Is. about 4 pm. 6 pm All hands go ashore Most of them come back drunk 
 1915 February 21 Sunday 1st in lent 
 Change places w/- the Bee. We move away from the peir   &  she moves up and takes in stores. Her men get a run ashore but cannot get drunk which is a great advantage. Weather very hot nearly everybody gets prickly heat. Lie about all day watching the pearling schooners. Divers examine Bee   &  report all serene. Leave port at 7 pm. 
