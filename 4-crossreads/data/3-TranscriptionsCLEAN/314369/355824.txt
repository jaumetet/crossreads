  
 7 May 1915 Friday 
 We are woke up by the sound of a big gun close to where we are about 5 a.m. Later on some of the old Battn come down from the fire trench   &  tell us about the terrible time they have had. In two splendid charges they took several of the positions held by the Turks but lost nearly six hundred killed   &  wounded including nearly all the officers 
 1915 May 8 Saturday 
 When not in the firing line the men live   &  cook in the holes in the hillside known as dugouts. During the day Dad   &  I dig out a hole for ourselves   &  start batching. We have plenty of preserved tucker in the shape of Biscuits, Bully Beef, Bacon, cheese, Jam, Ham,   &  any amount of tea   &  sugar. At seven in the evening we take our first turn in the firing line 
