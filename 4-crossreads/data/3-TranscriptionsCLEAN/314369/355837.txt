  
 2 June 1915 Wednesday We have a little drill w/- rifles by way of a change. in the morning   &  an address by Major General Sir Arthur Godley complementing the 4th Bde. on their past good work   &  hoping for more of it in the future We go for a swim in the evening   &  return to find    w   the whole Bat is to go on a big sapping job at 8 pm. 
 1915 June 3 Thursday 
 We are not relieved until 8 am this morning Return to camp tired   & Hungry Get a bit of breakfast   & have a sleep till 12 o'clock Get orders to stand to arms at 10 pm as a big attack is to be made from Quinn's post   &  we may have to support the N Z's   & Light horse. Stand by till midnight but are not required 
