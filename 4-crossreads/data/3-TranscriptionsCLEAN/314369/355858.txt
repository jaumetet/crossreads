  
 14 July 1915 Wednesday 
 Well this is a rather personal day for me I am 20 yrs old today so have reached my first score. We receive an advance of 10/- pay in anticipation of our visit to Imbros. Weather beautiful 
 1915 July 15 Thursday 
 Weather still Hot Provide working parties for the new water works which is being undertaken by the Engineers so as to be able to supply all the outlying posts without having to carry the water long distances 
