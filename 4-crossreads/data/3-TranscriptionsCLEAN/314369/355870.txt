  
 7 August 1915 Saturday 
 Improve position taken up  &  see Gurkas at work doing same on our right Main position appears to be far in advance of us  &  we can see the Turks on the run from N.Zs   &  Tommies Move into Bivouac in a sheltered Gully &  settle down for the day 
 1915 August 8 Sunday 10th After Trinity 
 Pulled up about 2 am   &  get into a position to prevent the Turks making a turning movement About 6 am the 15th   &  16th Bats make a charge on Hill 971 but lose hundreds &  have to retire We keep busy improving our trench  &  they put a M.G. in right near us. We have to relieve 'D' Coy some distance further along the firing line Work watches all night 
