  
 12 September 1915 Sunday 15th After Trinity 
 Very hot all day   &  am not feeling too good Plenty of music in the afternoon Dad comes in for a chat in the evening 
 1915 September 1915 Monday 
 Things getting very monotonous begin to wish I was back again among the boys. There was always a bit excitement out there to keep you going, but here it is like a big dull ache 
