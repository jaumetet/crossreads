  
 31 August 1915 Tuesday 
 The difference in treatment here from our previous knocking about is marvellous. The sisters cant do enough for you anything you want you have only to ask for it. The tucker too for dinner we had roast chicken &  Baked spuds   &  Custard w/- a big cup of lemonade. Our wounds are dressed twice a day They injected some juice into me to stop any infection from the foot 
 1915 September 1 Wednesday 
 Everything very quiet. Write a letter home but cannot get it posted yet. My throat gets very sore towards evening   &  I get something from the sister which fixes it up by morning 
