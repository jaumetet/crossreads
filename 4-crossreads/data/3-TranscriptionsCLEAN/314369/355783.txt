  
 14 February 1915 Sunday Quinquagesima Sunday 
 Wake up   &  find Seang Bee in line with us but much closer to shore which is just in sight 
 9.30 Church parade 
 Sit on rail all afternoon and look at the sea which is dead calm 
 Course NW BY N all day play hide   &  seek w/- Seang Bee. 
 1915 February 15 Monday 
 Keep Seang Bee in sight all day Bee Keeping very close to reef our officers think it very dangerous. At about 12 noon we Pass Collier "Titanic" bound South give her a good old cheer. 4 pm bee Turns inwards   &  cuts into the inner passage to try   & pick up some of the lost distance 
