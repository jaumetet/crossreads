  
 24 September 1915 Friday 
 Proceed to make myself at Home which is not difficult. There are very few restrictions. Leave is granted to a limited number each day   &  the remainder go out without leave if they feel so inclined 
 1915 September 25 Saturday 
 After dinner today I make a tour of the Ghizeh Pyramid district. Visit the Pyramid of Cheops  &  the smaller ones in the vicinity as well as the Sphinx   &  excavated tombs, some of which are covered with Heirogliphics   &  carving 
