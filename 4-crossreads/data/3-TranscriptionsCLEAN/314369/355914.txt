  
 3 November 1915 Wednesday 
 We are all examined by the medical officer to see if we are fit to return to the front 
 He will not let me go. My foot is too tender yet 
 1915 November 4 Thursday 
 The details leave Zeitoun for Alexandria en-route to the front Clean the Sleeping hut thoroughly. 
