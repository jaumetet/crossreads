 a6575200.html 
 87 
 1916   SATURDAY 5   AUGUST 
 Cold in morning but warm towards mid-day. Bill Hunter & Self were mess orderlies & hut orderly. Two eighteen pounder & 21 howitzers & some waggons came for the division in morning and a lot more in afternoon. Mother came out for me at 4 pm but Campbell would not let me go till six although I was doing nothing. Had tea with mother at Country Hotel & got a car after & went for an hours run round Odstock & a couple of other little villages & it was wonderfully pretty. Stayed at the Country that night. 