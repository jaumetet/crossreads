 a6575236.html 
 123 
 1916   SUNDAY 10   SEPT. 
 A cloudy cold day, but there was a little sunshine about 4 oclock. Poss Nivison & I got a car & went to Salisbury to get some petrol & then on through Andover & Basingstoke to Reading, to see Alan. It was a beautiful drive & everything is beautiful & green. We had dinner at the "Ship" Hotel in Reading & then went to look for Alan but found he had gone on leave. We came back to Andover & had dinner at the "Star & Garter Hotel" & got back here for tatoo. We had a great day although I would have liked to see Alan. 