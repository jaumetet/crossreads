 a6575206.html 
 93 
 1916   FRIDAY 11   AUGUST 
 Fine & hot but cloudy. Out signalling with the mob again all day. I was very stiff after the football  match yesterday, but it did me a world of good. The battery  goes  went on duty at 6 pm but I am exempt from all duties being a signaller so did not get anything to do. I finished "Aunt Sarah & the War" & I liked it very much. There is a lot of sound common sense in it. I would give anything to be able to write letters ect like the ones Owen Tudor wrote. 
 Campbell told me we would be in the firing line in eight weeks. Just see how true this prophesy is. 