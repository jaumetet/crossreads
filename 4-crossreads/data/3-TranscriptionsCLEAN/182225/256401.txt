 a6575265.html 
 152 
 OCTOBER   Monday 9   1916 
 Fine but cloudy. Helped to remake C Sub stables in the morning and we had to scrub out the hut in afternoon & then move all out things outside & the AMC man sprayed it all with some disinfectant. Poss & I went out to the Maple Leaf pictures at night & my word there was a rough show on. Campbell told me that he had recommended me for a commission all right, but he did not know when I would get it as he said it would most likely be in the "sweet bye & bye". 