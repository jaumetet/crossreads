 a6575251.html 
 138 
 SEPT.   MONDAY 25   1916 
 Fine & warm. We had an inspection (full marching order) in morning before Campbell. Out with the signallers the rest of the day and we had a lamp parade at night. Cleaning up all our gear at night as we have to march to Bulford tomorrow I believe. 