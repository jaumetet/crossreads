 a6575052.html 
 FEB.   WEDNESDAY 2   1916 
 Hot again. Was on cooks fatigue all day. It was a good day to get the job as the whole camp had to parade in drill order in front of the Colonel Pearce down by the railway. Got late leave & came in to Lisheen got the 6 oclock boat. Linda & Self lay on the tennis court & had a long yarn about matters. Got the 10.45 train back to camp 