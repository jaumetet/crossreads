 a6575327.html 
 210 
 DEC.   SUNDAY 10   1916 
 Cold & bleak, with a light fall of snow in the morning & out exercising horses in the afternoon. I had leave to go to London for an operation to my throat but though Auntie Florence trying to get it through headquarters things got into a bit of a mess & I am left in the Soup. Had a row with Campbell (Capt) in the morning about my work. He said if I wanted to stay in the battery I would have to shake myself up; but he could not give me a single instance where I had not done my work properly. 