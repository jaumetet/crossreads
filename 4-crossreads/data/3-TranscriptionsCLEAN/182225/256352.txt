 a6575216.html 
 103 
 1916   MONDAY 21   AUGUST 
 Fine & warm. We were all innoculated for para-typhoid in the morning & out signalling in afternoon  We should have had a lamp parade in evening but the fellows were pretty bad from the innoculation so we did not have it. There was three batteries of 6 in howitzers firing just below our huts all day 