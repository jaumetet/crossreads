  
 16. Thursday Sun in morning.  Then [indecipherable] up  Afternoon perfect.  Clear & warm Went to Miss N. at 10.15 & left at 6.15.  Mrs Meggitt &  May called to see me told me of Geoff's aberration. 
 17. Friday Went to Dr. Spindlo at 10.  In afternoon went to Lyceum to reception to American Mission.  In evening  F. Post came.  We went for a walk &  [indecipherable] lawn etc till 11.45  lovely weather 
 18. Saturday Lovely warm day.  No rain.  Tried to do a little work in morning.  Claude Kinred came about 2.30 &  we went to Kew to see rhododendrons & azaleas.  Former a little passe.  Went to Walsoe's for tea &  stayed till 10.  Got home late very tired.  Glorious yellow of laburnam &  rose of pink & double pink May 
