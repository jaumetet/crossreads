  
 August 1918 1. Thursday. Was to have dined with Lady Byles but Mrs Green rang up  & I met her at A. & Navy tea rooms & thence via H. Pk. to St Pancras & had some dinner & saw her on train for Scotland. Fine day - sun sometimes. 
 2. Friday. Went to see Sir J. Cantlie at 11. He said I was in bad state - flu on top of malaria. Crawled home & got to bed with attack after lunch. Cold & dull & heavy rain. 
 3. Saturday Cold wet day. Suffering neuritis. Went to bed after lunch. Mrs French came to see me but A.A.S. went away for the week end to Mrs Holmes. 
