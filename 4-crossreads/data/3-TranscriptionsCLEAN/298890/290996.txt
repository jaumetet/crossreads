  
 19. Sunday Lovely day.  Little grey &  cold at even.  A.A. & I went to hear Dr. Orchard &  [indecipherable] Ph. But Miss Yates acted bear with scalded head & we had to come home. E.C. Brown came to tea & stayed till 9.30.  Great yarn about Camp.  Big raid started about 11 & went on till about 1.30.  Quite noisy at times but I went to sleep. 
 20.  Monday Lovely Day Very exhausted had to lie down.  Mrs. Page came to tea in garden  Called on Lady Byles after supper 
 21.  Tuesday Agnes rang up & I went down at 3.30 to see her off from Hotel.  Went with them to Euston.  Then went to Young Brown & we dined at Florence & went back to hotel & saw Webber.  Found F. Post at home 
 22. Wednesday Lovely warm day.  Went to Miss N. at 10 left at 5.30.  Big thunderstorm turned it cold 
