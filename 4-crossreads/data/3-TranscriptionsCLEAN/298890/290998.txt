  
 26 Sunday Feeling terribly weak all day.  E.C. Brown called after lunch for a little while.  Then I went to Miss Wren for tea & she took me out on heath.  We heard cuckoo.  Occasional glimpses of sun.  Very feeble. 
 27. Monday Went down to help at Minerva Cafe.  Home at 5 & [indecipherable] had a snooze.  Feeling pitifully depressed.  Warmish day. 
 28. Tuesday Feeling terribly feeble & depressed.  Cloudy, warmish day.  Shower in afternoon.  During afternoon called on Editors for Miss Newcomb.  In evening went to Gertrude Peppercorn's piano recital with A.A.S. 
 29. Wednesday Fine day.  Very feeble - want of food partly I think.  Lay down in forenoon  in afternoon did editors again for Miss Newcomb's conference. 
