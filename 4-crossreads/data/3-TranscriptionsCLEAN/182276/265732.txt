  
 9th. A.M.G. Corp 52 Continuation of Report 6am 14/2/17 to 6am. 15/2/17. from enemy's lines in quick succession in our sight. No action followed however  Wind light S.E. hazy - freezing at night 
 D. Whitehead Lt. O/C 9th. A.M.G. Coy [Adv?] Hdqtrs. 
