  
 Intelligence Report No II from 6 am  30  1st to 6 am 2nd Right Sector:- No 5 gun fired indirect on I 18 Central Roads & trenches around Ruelle de la Noix 1.30 - 2 pm 102nd Brigade bombarded enemy lines with Trench Mortars. Between "Stand to" and midnight our Lewis guns & Snipers were active. The enemy M.G's & Snipers retaliated. After midnight very little firing took place Left Sector:- No 7 position fired indirect on I 6 A 5/0, Road and Dump at Intact Farm. No 8 - on I 12 A 4/9 Trenches & buildings round Retaliation Farm. 
 B.J Carey Capt 9th M.G. Coy 2-12.16 
