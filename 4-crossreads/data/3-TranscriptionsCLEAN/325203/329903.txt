  
 [Caption on the back of the photograph] No 6874 Pte Francis Wm Roberts C Co 9th Platoon 21st Battalion 6th Brigade 2nd Division Australian Imperial Force Killed in the attack on Mont St Quentin France on Sunday 1st September 1918 
