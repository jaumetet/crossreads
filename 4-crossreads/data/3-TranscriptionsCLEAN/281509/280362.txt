
1916 May 19Friday
Still going north.  Breakfast at Macon. Bob & self pinch down to baker's buy loaf. taken to cafe.  coffee & rum. Scenery still beautiful & ever changing from one delight to another, valley scene, ribbons of green crops, chocolate soil background of hill, houses, road a lovely sight. Waving acres of grass & crops. Harvesting & hay making.  Avenues along every road. River scenes, arched bridges, barges.Dijon. French officer talk English Red cross nurses, cold coffee, bitter.Tony & self hospital orderlies for night. Cold troublesome, head very full.
