  
 they were of constant danger to us whose dugouts were situated directly beneath the mule tracks. 
 Their quarters were situated in Rest Gully, No. 3 outpost and various other places of refuge but the Turks seemed to have knowledge of their whereabouts & consequently they suffered heavy casualties from shrapnel & high explosives. 
 The transport were unceasingly at work but mostly after dark as our positions were too exposed to the Turkish observation posts. 
 The mules were very enduring, and sure of foot, & were easily able to climb mountains of some 800 feet high.  We daily awaited their arrival & their cheerful Indian leaders whom we called Johnny as we knew that  [indecipherable] to bring us our daily bread. 
