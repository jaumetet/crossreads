  
 24 for some time sapping ahead of their trenches towards ours forming a new firing line. This was dangerous to us. & we made saps towards theirs & blew up three. We then charged [11 Batt. Supported by fire from Second Light H. & rest of third Brigade. We were successful and killed  60 or more Turks. Our casualties were 11 killed and 74 wounded. Men behaved gallantly & deserve high praise 
 Aug 6. A great flanking move by us & we left Rest Gully at 7.P.M. on our adventurous exploit 
 Friday 28 May 1915. 40 men sniped in Shrapnel Gullly 
