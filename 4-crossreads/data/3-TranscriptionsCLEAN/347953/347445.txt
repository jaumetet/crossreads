  
 Gambling 
 Pay day was always awaited with eager expectation by all, but that day invariably brought forth mischief and regrettable incidents, especially when the new men were gathered together on the transport ships or at places like Gallipoli, Lemnos etc. they could not be spent on [indecipherable] or where amusements were not to be had.  In these cases the men gathered together to gamble at crown & anchor, two  up,  [indecipherable] and other gambling games.  Money was wasted & thrown away as if pound notes were only shillings.  As  usual nearly all lost everything they possessed except a few who were lucky.  One particular case stands out above 
  
 all others.  A man named Williams of our battalion won 200 pounds at two up, &  gave it to our adjutant who placed it to his account. 
 [A sketch of the head of a Turkish soldier entitled Abdul]. 
