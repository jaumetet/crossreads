  
 Lemnos 
 Lemnos was the intermediate base and contained a well sheltered harbour and good green lands which were apparently very fertile for cultivation.  Hundreds of warships and transports lay in its beautiful harbour as well as the Greek sailing vessels, and overhead the aeroplanes were continually flying to & fro giving it a most unique spectacle while on land could be seen thousands upon thousands of Bell tents & marquees where the infantry were quartered previous to transportation to Gallipoli.  Hospitals, too, were clearly visible.  They were built either of weather boarding or 
