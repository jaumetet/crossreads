  
 honoured, and their memories cherished and upheld for all time. 
 Suvla Bay 
 We all knew that Aug. 6 was to be a   great  day of great events and expectations.  A final dash for the key to the situation controlling the forts of the Dardanelles.  We now know that it failed completely but the battlefield of Suvla is worth recording for its spectacular display of artillery duels and the splendid but futile attempts of the English and Australasian regiments to gain impossible positions under such conditions as then existed.  The water supply was hopeless & badly organised as also was the 
