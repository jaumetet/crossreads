  
 27 
 [indecipherable Thursday] 9 Bombardment of Sari Bair mountains by our land batteries Dec 18 13 Batt. Commenced by degrees to evacuated their position on Durrant's Post. 
 Dec 19 Embarked at Lemnos [indecipherable] liner Tunisian which just missed being torpedoed Dec 25. We spent our Xmas at Lemnos Island & had an enjoyable time. A Turkish Aeroplane tried to bombard our camp but without success 
