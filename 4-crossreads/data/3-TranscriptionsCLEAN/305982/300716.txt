  
 service .  Cards given by General Cox for raid.  Brigadier gave out news about Aust move.  favourable news  I went to under ground caves.  French people used to be in when the English used to be at war with them. very cold inside. 
 24/7/16 Cold again this morn.  nothing exciting going on all to day.  Horse in wheat sheaving. 
 25/7/16 On the move to day we left at 9 oclock where we will find out later.  Beautiful country we went through about 8 mile.  rest for dinner  Breakdowns wounded [indecipherable]  we were going forwards Artillery plenty of it all over the place   hard march for some big stuff into the bargain we arrived at our destination about 6 oclock this evening. Rubempre 
 26/7/16 Rubempre  Warloy  is the name of the village we are in plenty of troops etc passing both ways.  W  plenty of soldiers  we are stationed at a farm  Big place (horses)  Bayonet fighting all day 
 27 On the move again to day for where I am told Warloy  plenty of artillery  [indecipherable] & troops passed through village  hellish muggy [indecipherable] drop down 
