  
 In many cases I found evidence of a commencement to methodically pack up household goods which had been abandoned half way through.  First news of a possible advance by Fritz on Hazebrouck must have arrived & then, suddenly & startlingly the near gun fire. 
 We got some antique popular English music, & a French copy of some overtures including [indecipherable] & "Norma".  Also I found some splendid post-cards, including a Napoleon series. 
 At other houses I got 
