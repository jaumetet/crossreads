
Thursday
Saturday 18 May
The principal events of the past three days have been in the shape of extra work & increased heat, & these are bound up in each other.
The weather has suddenly become warmer then hot.  One wakes up after a heavy dead sleep, (which the northern warmth always brings & not the Australian heat) to promise of a hot day in the warm haze.  Unusual heat in the morning & midday keeps ones mind occupied with the thought of the discomfort
