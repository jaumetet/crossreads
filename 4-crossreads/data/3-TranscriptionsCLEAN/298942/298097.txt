  
 all day.  We carry shoulders high, one at each corner, which makes the work very light compared to two to a stretcher stunt. 
 Shells fell near all night, & the next cottage along the road was hit. 
 This morning he shelled Fletre with heavies for some time & set one place alight, or else smashed up a plaster wall, for the clouds of deep pink were sent up     from where the shell hit. 
 We have a quiet time in this living room when not 
