  
 track, & Blackwell, Rogers & I pushed the stretcher full of rations & equipment back to the central post.  The two squads above our late post who had carried to us had to go to a new post directly behind the line.  Thus we just have the one main line of evacuation now, in about four or five relays. 
 Sgt [indecipherable] detailed my squad for work during day time between central relay & waggon loading post. 
 After supper (dusk) Capt Young & he took me with them over their rounds.  We visited all the posts 
