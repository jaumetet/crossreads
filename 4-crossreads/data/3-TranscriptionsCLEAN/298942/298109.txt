  
 There are rumours that the division will soon be relieved.  Some say in a day or two & others not till the end of the month. 
 Yesterday morning an old man & a boy appeared in search of some household goods.  Later I saw them returning with a [indecipherable] & some beds, & a very big pail. 
 It is impossible to convey a full impression of what Fletre looks like.  Meteren can now be likened to Ypres in the complete destruction that has 
