  
 overtaken it, but Fletre being in an earlier stage beggars description. 
 One can say that it looks as if an earthquake had occurred at one part of the  town  village & big fire at the other.  At places where he has sent over incendiary shells, or the ordinary ones have caused fires the shell wreckage has been effectively finished off by the fires.  Apart from these effects which are grouped, the rest is just smashed houses, with terrible domestic confusion inside.  Each house 
