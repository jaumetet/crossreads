  
 indications I should judge that we will be in the line till Fritz has finished his push.  Any day he may commence, & that he will push us is as certain as can be.  Therefore I think that this soft time will be followed by an unusually severe one.  He will not cease trying til all means have been exhausted, & though he may not commence here, it is certain that he will try here sooner or later for Hazebrouk & the coast. 
 I suppose, provided we hold him in, things will be 
