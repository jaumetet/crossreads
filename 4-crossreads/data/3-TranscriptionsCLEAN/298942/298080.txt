  
 which Paddy Daly delighted us by assuming a most extraordinary top hat that he had salvaged from somewhere, my squad & two others ( Dickson's & Steven's) started off: we for the next relay post, & they for the R.A.P. of supports. 
 It appeared that ours would be a mile & a quarter distant but we would have a wheeler to convey the wounded.  Our track was a road, but as this is under direct observation of Fritz, ordinary journeys such as[indecipherable] up 
