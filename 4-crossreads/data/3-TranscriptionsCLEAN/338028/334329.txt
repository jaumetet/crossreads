  
 Friday 16th June We are now doing the sick parades of the 5th L.H. reg. There Dr being away I had to sit up half the night with a delirious patient, dispatch him to hospital in case. 
 Saturday 17th June Things are very quite here. Terribly has camp very monotonous, everybody seems to have the hump, no one more than myself. They got the Taube that bombed us at Kantara. 
 Sunday 18th June C. Parade early a.m. by Col. Murtbuncl[?] Woods who is visiting the camp. Terribly hot day, in spite of our shady camp. 
 Rec letter from my brother, my make S.M.B. Squadron was fortunate as he obtained his com, only two were granted. 
 Early in a.m. twelve of our aeroplanes went over in the direction of El-Arish with what results we do not know. 
 General parade in p.m. before Gen. [Granville] Ryrie, who in his usual self-booming way said farewell before he left for England, he had his recognized publicity manager Tpr Bluegum ([indecipherable]) with him. 