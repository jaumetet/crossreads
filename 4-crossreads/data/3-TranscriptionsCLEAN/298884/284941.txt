  
 200 men. A number of the officers are away on leave and it looks as if they will not be able to get back in time to prevent themselves being taken off the boat roll. 
  27th   Innoculation for Flu has been proceeding apace with all men who are going home on the Derbyshire and the Euripedes. 
 The Sergeants in the Depot gave a dance in  the  one of the canteen huts huts which provided a very pleasurable entertainment for  the  all who were able to dance. 
 I went along at the invitation of some of the Sgts but only stayed a couple of hours. 
