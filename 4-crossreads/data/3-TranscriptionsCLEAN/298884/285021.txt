  
 round that we have to call at Hobart before Melbourne after all.  Much groaning from the Victorians as a result. 
 Papers were sent on board this morning & they were eagerly sought after by all.  The flu epidemic comes in for a lot of consideration in the columns of The Register. Mr Kidman the cattle king prophesies a higher price for meat.  Victoria has launched its first big steamer. Sydney has adopted an improved method of dealing with meat supplies & that is about all the news which interests me at present. 
