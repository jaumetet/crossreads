  
 thing. 
 I was unfortunate in having to go on duty at 2 P.M. as it spoilt the day for us. We were unable to go outside of town hardly as by the time we had finished our shopping & had lunch it was time to go on duty. 
 I was told off to take charge of a Piquet on No 2 Post & Lieut "Brolga" Brett put the lot of us (myself and 8 men) in Rickshaws & took us to our duties.  We came first of all to No 1 Post which was on the first bridge along the sea front road 
