  
 stones & beads. We were instantly offered chairs by the native shopmen & asked in English what we would like to see. I was  not  terribly keen to see anything but was highly amused at Mr Young (who travelled through these parts in pre war days). He asked for all sorts of silks to be shown him, priced them & talked about them for the best part of an hour. After having almost everything in the shop pulled down & handed to him for close inspection he wandered out without buying anything. 
 J. Ramsamy's shop [written up the side of the page] 
 