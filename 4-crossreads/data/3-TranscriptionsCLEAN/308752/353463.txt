  
 He is lucky missing that training in Egypt & not being in France during the Winter. 
 July 12th 1917. The Military Tournament held last Wednesday was such a success, that the display was given again today, on the occasion of the Inspection by His Majesty King George. 
 July 15th 1917. A great sports afternoon, three cricket matches & a soccer match. I was playing with the 2nd Team against another Field Amb. They prepared tea for us & gave us a good feed. We had a win for a wonder. 
 July 17th 1917. Brigade route march to C ---  a distance of about 12 miles. Stopped the night in the town, & returned on the 18th. Met Stan. Purse 