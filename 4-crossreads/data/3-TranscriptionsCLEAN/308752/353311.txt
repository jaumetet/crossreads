  
 Sunday 26th December Another easy day for us, did a lot of washing, made a much better job of it than the previous one, the clothes turned out a dirty yellow instead of black. My first big mail arrived 13 letters in all, including a letter from home, with which I was extremely delighted. We are having perfect weather. 
 Monday 27th December Shifting camp to a more suitable position, every-one working hard, kept at it till a late hour at night 
 Tuesday 28th December to Friday 31st December. Monotonous camp life Stretcher drill & route marches etc. every one is sick of it. They are singing out in Sydney for men & still more men, & here we are wasting our time & still likely to, for a long time to come. 
 January 1st 1916. New Years Day. Holiday in camp. Had a trip to Ein Goschen, in the ambulance waggon with a patient. Weather very cold.  Sheepskin vests issued, needed in the early morning very much. 
 Sunday 2nd to Saturday 8th January Same routine drilling all day. 