  
 from Mosman Bay here, also Capt. Wilkinson. (Physical Drill Instructor) 
 July 19th. 1917. What awful memories does this day bring back to us.  That terrible battle on the 19th July last one of the never forgotten days in my experience. 
 July 29th 1917. Preparation all the week for a big move somewhere. I believe we have a long trip before us. It will be a change to move to distant scenes. 
 July 30th 1917. Ambulance move off today & board the train at A ---.  Train left at midnight.   At 9 a.m. we passed through a large Port of France, at 11 a.m. arrive at St. Omah or Omer, a pretty French town.  Probably Leave will be granted to this city. We then marched 