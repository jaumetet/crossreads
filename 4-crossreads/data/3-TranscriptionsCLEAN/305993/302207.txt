  
 July 11 Shifted from Victoria Docks to Gravesend & there embarked all the soldiers & there wives & baggage. 
 July 12 9 p.m.  Departed from Gravesend for Australia & called at Portland on the 13th & put out Channel pilot ashore. 
 13.7.19 At 3 p.m. left Portland for Australia via St. Vincent, Cape Verde Islands for bunker coal & then  Cape Town  Port Natal & Fremantle. 
 14.7.19 Fine weather prevailed & everyone seem to be happy & contented yet a few homesick after leaving their near relatives for a distant country.  1 a.m. we passed Ushant Lt. off the French coast. 
 15.7.19 At 1 a.m. we passed C. Finisterre, Spain, fine weather prevailed throughout & very few suffering from sea sickness.  Concert held in the 2nd Saloon this night which was packed & proved a great success. 
 17.7.19 At 7 a.m. we passed within 5 miles of the Islands of Maderia & at 11 p.m. passed the Canary Islands.  Cricket & deck billiards was the sports this day.  This day our Captain & the Commanding Officer of the troops lectured all the Troops.  All paraded away from the wives re their conduct & advised them not to be too familiar & so conspicuous & loving whilst on deck.  This is no doubt a great failing amongst all young married couples.  But with a crowd of 550 viz. 1100 men & women all together in a small space, even a small percentage is very noticeable (besides single troops 200, hospital staff of certified nurses & orderlies & about 60 children under 4 years of age). 
 18.7.19 Everything improved a lot after the advice given yesterday.  Instead of lounging on deck the majority today 
