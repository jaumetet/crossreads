
4th Bn A.I.F.Tel-el-KebirEgypt
4.2.16Dear Judge
I am afraid I have been most discourteous in my letter writing to you - which is a very poor return for all your kindness in writing & sending me papers. But you must really forgive me. My time is not my own.
My third visit to Anzac did not end so disastrously for me as after the landing & Lone Pine - & was short but full of the incident. I wrote you a hurried note from there. I was pretty busy getting my Bn out of Reserve & into the Fire Trenches when I got back. Then came the preparations for the evacuation, & finally the evacuation itself.
It was with very mixed feelings that I left Anzac. Relief at the prospect of a rest, after the strain of the last few days when our line was so thin - but oh! the gallant men we left behind. But they made the great sacrifice, & one can say no more.
Now that the Peninsula has been completely evacuated, it can do no more harm to let you know some of the cases that were carried out, & which helped to bluff the Turks. Some 15 days before the evacuation, orders were received that not a shot was to be fired for 3 days (unless heavily attacked). This was done & had friend Abdul
