  
 I enclose Myles' last letter - 
 Queen St 24.1.16 Dear Judge Ferguson 
 For my souls good I strolled across to the Agricultural ground this evening hoping for a glimpse of Keith. 
 A sentry told me I'd find him past the dog-kennels near the skating rink. I wandered through innumerable buildings dedicated to trade - ultimately discovered his sleeping quarters - a pleasant airy little building dedicated, if I remember right to Comstocks worm tablets - or was it Weagh's Baking Powder? He was not there. 
 Post-ultimately I found him surrounded by some NZ cons[cripts?] in Ayres Sarsaparilla! 
 There were two notices that seemed applicable to myself. One - on the door - said "Men not admitted here, so GET OUT", the other - on the door-post said "This post will stand up by itself, & doesn't need your support". I noted that the writing was  not  Keiths (which by the way is extraordinary like yours). He came out, having settled his business, with face glowing & in fine spirits - given up the Adjutancy - back in 
