  
 R.M.S. Makura Off Honolulu Feb 4/16 My dear Ferguson 
 Just a line to let you know that all so far is well. We reach Honolulu tomorrow & Vancouver on the 11th & should be in England about the end of the month. 
 I begin to see quite clearly what has to be done there & am hopeful of doing some of it. The time at my disposal is far too short. Well by going full speed ahead I hope to get through somehow. 
 Hoping all is well with you 
 W.M. Hughes [Prime Minister] 
 