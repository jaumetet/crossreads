  
 hostile attack upon the position he was then holding, and it has been my extreme pleasure to officially bring his conduct under the notice of higher authority. 
 I am not at liberty at present to give you any details owing to the strictness of our Censorship regulations, but no doubt you will placed in the possession of same all in good time. 
 Suffice it to say he is a fearless gallant boy and I am proud to have him under my command. 
 With kindest regards and best wishes, Yours sincerely William Holmes Brig. Genl. 
