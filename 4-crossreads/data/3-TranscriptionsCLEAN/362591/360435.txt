  
 In the Field 18th May 1916 
 Dear Justice Ferguson, 
 I am taking an early opportunity of replying to your letter of 18th March. 
 I have made some enquiries re Myles O'Reilly.  He returned to this unit some little time ago, just prior to our quitting Egypt, and is now with A.N.Z.A.C. Head Qrs. and, from reports, getting on well.  I shall keep an eye on him. 
 I suppose you know your son's Coy stood the blunt of the old Bn's little adventure.  He is quite well, and does not look much the worse for the trying time.  FitzGerald is at present on Leave in London.  Col. Lamrock is keeping very well, and I am happy in that my old Bn. and the new one are linked together in any adventures that befall. 
 With best regards, Yours very sincerely, Geo. F. Murphy 
