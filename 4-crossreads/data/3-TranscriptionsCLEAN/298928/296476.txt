
sailing over the camp.
3rd Have finished bull-ring and are classed out & will very likely go to-morrow.
5th  Left 3rd A.D.B.D. for another taste of the front line at 9 am.Left Le H. 4 p.m.               Weather perceptibly colder going across France, in horse trucks as before.
6th  Detrained at Hazebrook marched