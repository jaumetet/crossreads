  
 transferred to the first Army Corps. 
 1st  Taken in a M.T. to Steenwerck to play at a vaudeville show  by "Anzac Coves". A permanent pierrot troupe.  Billeted in town  and having a fine time. 
 6th  Two German Balloons came over to day dropping  pamphlets containing glowing & exagger- 