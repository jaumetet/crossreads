  
 [Transcriber's note: 
 p2.  Perham Camp, Wiltshire p6.  25 Sept. 1917  Departs for France  - Le Havre p10.  6 Oct. To Marbecque p13  10 Oct.  To Ypres p15  11 Oct.  Passchendaele II action.  Reinstated in band p22  15 Oct.  Hell Fire Corner p23  Rest camp p27  Ledringhem p34  9 Nov. Returned to line via Vieux Berquin p35  Rossignol camp near Nieppe p42  Back with battalion (in band) p53  3 Jan 1918  Returned to Merris rest camp p57  31 Jan. "Canteen Corner" camp near Steenwerck p57 5 Feb.  To England on leave. p58 - 66  Various verses etc.] 
 [See end of diary for place name corrections] 
 No 5 Diary 1433 S.B.Young. 36th Batt Band "Luton" 5th Avenue Campsie N.S.W. Aus. From 25th Sep. 