  
 [Transcriber's notes: Many parts of this diary, sent home as letter pages, have been censored at a later date by person unknown.  Some of the over-written words do not appear to be correct and have been written as originally intended by the author where discernable. In many cases personal information has been crossed out with the word "omit" and later, due to Army censorship, other sections are also crossed out. 
 Frank Weir was a member of the 1st Light Horse Division who was sent to Egypt to defend the Suez Canal and other British territories in the area against the Turks. He previously served at Gallipoli and also in Africa during the Boer War.  He became a Captain during the period of this diary and details daily life for the men and horses.  Some details of the Romani and El Arish battles are included.] 
 Personal Reminders No of bankbook "     "  watch "     "   bicycle  Glasses 62765 "     "  stores ticket "     "  season  " "     "  telephone Telegraphic address 
 Size in boots          gloves      Collars     hats Weight          height 
 Insurance Accident Burglary Fire 
 Life Servants Owner's name and address 