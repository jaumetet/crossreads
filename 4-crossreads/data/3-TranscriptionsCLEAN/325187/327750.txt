  
 [Wednesday  January 5 1916] 
 Regimental Drill mounted & in afternoon went No.1 Hospital to get a tooth fixed up-  There I met Riley from Deniliquin. Did I tell you Norman (G[indecipherable] Robertson & his wife is here as Dr in the 1st L.H. Field Ambulance Cold today & raining part of the 2" we get in the year.  We will not be leaving here for a week yet so post this with love 