
Allonville is a fair village and the seat of Jean Hennessy the brandy & whisky distillers. It has the usual large church & spire but the building is in a better state of preservation than most village churches & it is built of bricks. Aimens which can be plainly seen, lies in a depression about 4 kilos away. Its magnificient cathedral stands out promently. Looked at through the glass its bears some shell scars, for Aimens is shelled both day & night, at irregular intervals & in the early morning it is bombed all this with the last few months. And from having a population of some 200,000 it has dwindled down to 200 who are living in cellars, & are there only by permit.
We are now a mobile Brigade, and are under 20 minutes notice to move at any time, but it cannot be done especially at night.
I sent a whizbang to [four lines of shorthand - my wife and wrote to my sister today.  I also packed a few things which I made up of a French 75 shell case to be sent to my wife.  I feel much better this evening.]
Wednesday May 15. 1918. 1381.A beautiful day Nice warm Sun. I did not parade sick today, as the day was so fine and I felt a lot better. I am doing light duties, helping to clean a Limbre
Tonight a number of Aeroplanes came over & were subjected to a lot of fire from machine guns and archies. They could be plainly seen in the rays of the searchlights which made them look like big transparent crosses in the sky They are flying low.
Thursday May 16. 1918. 1382.A hot day. I am taking things very easy today. There is not much to do and the heat is very trying.
The heat this afternoon was very intense, especially late in the evening, about 6.P.M.
I was on picquet (4) this morning today from 4.30 to 7. It is a
Friday May 17. 1918.1383.a beautiful morning. I had a very easy picquet this morning. The horses being in a stable they required no attention, & the morning was an ideal one. It is going to be another hot day.
This morning at the aerodrome there was a march past by the whole battalion with the Transports before General Monish

