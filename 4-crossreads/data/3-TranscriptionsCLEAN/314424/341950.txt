  
 oats growing, but which is mostly all fenced off. We salvaged several bags of oats & barley & a kit of engineers tools yesterday. 
 Last night I received a "Weekly Times Dec. 22. Christmas number. It was very interesting. 
 I heard from E . artillery that the German advance was on a front of 55 miles to a depth of 20 to 25 miles. 30 miles of the front being French. 
 We had roast pork for tea also a little champagne. Many of the men were drunk last night & have sore heads today. 
 Monday April 1. 1918. 1337. Easter Monday. 
 The horse lines are in a sea of mud, but this afternoon is fine, and the weather seems to have taken up a bit. 
 There was pronounced enemy aerial activity this morning , & one bomb fell not far from our lines early this morning. 
 The artillery around have also been pretty active in straffing the enemy. 
 Starky Treginza was up before the major for being drunk & away from parade. He was admonished and given 7 days Royal Warrant. 
 Tuesday April 2. 1918. 1338 The weather has 
