  
 Transport lines. 
 I was on second picquet last night from 11.25 to 10 to 2. 
 This evening I went for a drive on the water cart with A. Valentine to Behencourt about 41/2 kilometers away. While there I was able to get a paper and had to pay 3d for it. 
 The Americans are now in the thick of the fighting and are giving a good account of themselves. 
 Saturday May 4. 1918. 1370. A fine but cloudy day. 
 There was the normal artillery duel last night and at intervals throughout the day. As I write it is being renewed for the night. 
 Snowy Price our cook returned from the Base tonight. So my position as second in command in the Cook House finishes tonight. 
 We heard a few days ago that Trevena has had his leg taken off. 
 We had to remove from our billet (at least those who were in it, for most of the men slept in other billets & in tents) to make room for other troops. My mate Attenborough & I are sleeping under a cart in the cook shed. 
 Sunday May 5. 1918. 1371 A dirty drizzling morning. 
 [three lines of shorthand - I wrote a further letter to my wife last night and this morning x letter 127.  4 pages of a post card and 2 pages in green envelope.] 
 I am on my old job again doing odd repairs to Limbres, etc. 
 Tonight if required I am breaksman on C.C. McGlouchens Limbres for R.E. duties. 
 This evening I received another letter from my dear wife dated March 5. No 123. 
