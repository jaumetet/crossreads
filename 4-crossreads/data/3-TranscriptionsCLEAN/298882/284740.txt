
16.12.17
Yesterday all the Aust. Base Camps were amalgamated and are now all camped on the opposite side of the valley just between Harfleur and Montvilliers.  We are well protected from the cold South Winds here by a long high range of hills.  The officers mess here is rather crowded as a result of the Amalgamation.  Last night was a lively one here.  A number of officers having been warned