
Near Abeele.  25.10.17
We reached here yesterday from Dickiebusch.  All the men in good billets for once though scattered.  Ground is in a bad state.  Raining nearly all day.
31.10.17
News of great Austro-German victory on Isonzo front.  100,000 prisoners and 200 guns captured from the Italians.  Gen Cadorna accuses portion of the Italian Army on the front