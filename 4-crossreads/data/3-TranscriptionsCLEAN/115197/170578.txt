 Bursting through the dead brown leaves which cover the woody country, little green leaves & creepers are to be seen.  The bare branches of the trees show signs of returning life, and the early Spring flowers  and  are budding and nodding in the sun-lit fields -"Oh to be in England now that April's here!" 
 Pass through Salisbury Wilton etc and disentrain at Warminster 
