  July 2nd  
 News of the Aeneas, Beltana & Durham.  These three vessels are ahead of us and are experiencing rough weather. 
 Conclude "Our Boys Beyond the Shadow" . 
 The growing popularity of our Wednesday evening hymn service is well marked on this day, when despite the bitter winds, rough seas and failing light a good number of the "boys" sit out the service on deck. 
