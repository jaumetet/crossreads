 Diary - letters & preparation for to-morrow's departure. 
  April 26th  
 Most reluctantly and with a sad heart I leave "Elm Bank" - and later in town I bid farewell to Mr Scales. 
 Take 10.15 a.m. train from Paddington to Warminster.  Return to Sutton Veny Camp. 
 Report to Orderly Room & take up new quarters.  Blanket 
