  
 again in a few days after which he galloped off with his long retinue of underlings stringing out in rear, the whole outfit being followed by rude epithets from  the  diggers. 
  2nd  
 I have to hold myself in readiness to proceed to a gas school. 
 3rd  4  
 I received orders this morning 9 oclock to pack up and get to Div. H.Q. at Oisement (about 9 kilos away) by 10 oclock with my valise and batman.  Being Sunday morning & having nothing to do I was still in bed when this news came along.  However I managed to reach Divisional H.Qrs. about 12 noon where a red-decked and apparently greatly concerned Staff Captain asked me many 