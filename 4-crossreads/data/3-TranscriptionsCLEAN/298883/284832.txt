  
 Aust. Battalion 
 10th It rained heavily all night.  The men  t  in the trenches had a cold wet night. 
 Lieut. Steber and myself went for a walk into Peronne and crawled about amongst cellars and up on to the ramparts from where we had a good view of the ruined town.  We finished up with visiting our Y.M.C.A. as a branch had been established there also.  We bought some biscuits and were given some magazines to take back to the Battalion with us. 
 Major Kerr has very kindly sent along a commendatory letter to the C.O. here about 