  
 July 13th Anniversary of Enlistment - Good weather. 
 July 14th A large mail from home. Route march (6 miles) - During march we encountered Cavalry (British) who were training - Still farther on March we meet Mounted Machine Gun Corps (?) 
 July 15th Six months to day since we sailed from Sydney - more papers & letters from Home. Easy Day. 
 July 16th Sabbath & Letter writing and Lecture on Gas & gas attacks - Midnight trip to Amiens (N.Z. Hospital) 
 July 17th Nothing sensational happens during day. 
  July 18th Feet & Boot inspection. Once again the "Ghost Walks" 
 July 19th Route march of 9 miles from our own village through Aerodrome on to Bertangles. We halt at the Aerodrome and are given our first opportunity to see planes stationary in act of ascending and descending. - Mail closed 
 July 20th Once again we move camp - leaving Vaux for the village of Herissart - We are billeted with 25th Batt - 
 July 21st Still at Herissart - On duty in dressing station - Mail. 
 July 22nd After an easy day we move off at 10am, and after a rapid march of 6 m we arrive at Vadencourt. 