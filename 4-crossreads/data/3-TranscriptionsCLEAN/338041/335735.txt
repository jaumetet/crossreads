  
 Sept. 24th Sabbath. "B" Section take over Divisional Baths - We are billeted in a Convent & have every convenience tending to make us comfortable, - Our handy position to town a valuable asset - but demanding a power of wealth. - Sports 
 Sept. 25th "Talbot House" & Rev Clayton - visits to Y.M.C.A & town. Photos posted home - Daily hot & cold showers. 
 Sept. 26th. A busy day - Good news from England & the front. 
 Sept. 27th "Our Canteen" - Parcel from Home - Aerial combats. Rumour of Combles & Thiepval. 
 Sept. 28th. Rumour confirmed - "Confession" - Belgian Priest - Card etc 