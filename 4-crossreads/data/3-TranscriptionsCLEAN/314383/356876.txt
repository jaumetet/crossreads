  
 & provided quite a pleasant afternoon for us with afternoon tea thrown in when we had a great blow out on pancakes and honey. 
 May 24th Edna's birthday. Thoughts of home.  Am really feeling a bit home sick today, thought I had overcome this, but at times it comes back. 
 This afternoon we held a sports meeting, where we had inter dominion competitions. The N.Zs scooped the pool.  Tug of war & relay race with the Australians as runners up.  Quite a big crowd of Jews & Armenians present, who were quite at a loss to understand the vanquished cheering the winners. 
 May 25 Major Starnes D.S.O. New Zealand with 30 men ordered to push on towards the Caspian.  We are divided into 
