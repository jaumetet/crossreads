  
 April 22] Still at Ruz. Yesterday it cleared considerably & today the ground is much drier. 
 April 23rd 
 Today we left Ruz, journeying by Ford car to Kusr-u-Sherin on the Persian border.  Quite a fleet of Fords conveyed us to the appointed place, & each was driven by an Indian who handled them to perfection. 
 April 24th On the outskirts of the camp are the ruins of the ancient city & wall.  The wall or rather what remains of it is massive, being about 20 odd feet high by 3 feet thick.  The most interesting of the ruins are those of two old castles built entirely of stone.  One 
