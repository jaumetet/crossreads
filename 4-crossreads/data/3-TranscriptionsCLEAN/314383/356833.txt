  
 people are full of delight at these strange goings on of British Officers - the Russians seem a fine lot, & join in any fun that's going. 
 Feb 6. Arrived at Faenza renowned for its beautiful China.  Lunched at a very fine hotel, good music.  Quite a stir created by one of our armoured car.  Chaps taking a violin and playing beautifully great applause. 
 Feb 8. Arrived Toronto, furthest South in Italy.  We go into camp & there are 
