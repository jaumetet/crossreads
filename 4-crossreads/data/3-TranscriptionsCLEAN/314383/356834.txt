  
 to wait for a boat to take us across to Egypt.  The Bishop of Gibraltar lecturing here on the Caucasses had a yarn with him after, gave us cards of introduction to some people out there. 
 Feb 10 Embarked on the "Malwar" some 40 nurses on board.  Food good &  beds lovely. 
 Feb 12. Splendid trip.  We commence Russian classes, the Russian teaching us is a full blown Count who escaped after the Revolution, was one time an airman, walks lame on  a/c of a bad crash he once had. 
