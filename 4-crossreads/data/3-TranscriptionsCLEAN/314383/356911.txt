  
 Turned back.  Utterly disappointed  & fed up.  San Jud 
 July 28  Genja July 29th  Taken Tepe.  Persuaded Major Chaildecott to give me orders in the name of Major Stairnes to remain here as an advance post with the idea of trying to raise a force from the tribe here in order to break through to the Lake, or if they come thro' be in close touch.  The Governors Brother, a Sidar, paid us a visit & for a certain sum is willing to help. 
 July 30th Kay Nicol & I went along & paid our respects to the Gov this evening.  Took 4 Sgts as bodyguards. 
