  
 stepped out to stay.  All these chaps have tons of guts. 
 June 6th 17 miles today.  Felt sick when I left, dysentry and an awful head which I had all last night.  Extremely hot, just done up.  The road is back over the same track.  Met 2nd party at night who are on their way up to garrison Kasvin.  Picked up an Assistant Surgeon from the Cavalry, the first medico we have yet had. 
 June 7th A1 today 18 miles over easy country, much richer and more populated. 
 June 8th 15 miles, terribly hot, country still fertile with plenty of vineyards & crops ready to harvest.  Inhabitants appear 
