  
  Feb   Mar 7th 
 Today Hitchcock and I blow along to a Remount depot and ask the Colonel for horses.  He gives us one each, but what horses?  As soon as we mounted they bucked like fury and then boltt.  We got into a narrow side road which is walled on either side.  The first person we see is "Pistol Pete" the Colonel so called on a/c of always having his six shooter attached. 
 Then my nag goes like the wind, bowls over a gang of Arab road workers, narrowly misses banging into a car then crashed on a bend. I become unconscious and wake up with a scared Tommy enquiring about my health. Sprained my left wrist, split my top lip, skinned my face in 
