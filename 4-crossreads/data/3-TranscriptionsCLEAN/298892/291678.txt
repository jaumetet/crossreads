  
 get attached to a Field Ambulance. I received a very nice letter from Kate Chisholm and will endeavour to reply but you know what a poor correspondent I am.  Excuse the scrawl. Only for the fire I couldn't write at all and as it is I have to hold my hand to the blaze every few minutes to warm it sufficiently to hold the pencil. Your loving Son Eugene. 
 P.S. I am sending a few holy pictures which appealed to me. They were scattered about with 
