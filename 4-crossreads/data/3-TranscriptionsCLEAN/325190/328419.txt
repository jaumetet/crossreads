  
 Friday 28th Sept '17 Second anniversary of landing on Gallipoli. Went to Delhi again yesterday for eye specialist exam, & this morning was paraded at orderly room for inspection by the Major in connection with O.T.C. Papers to be held back pending a board on specialist's report. I'm damned sick of this business from first to last, & would far rather shut the whole thing up & go over again. 
 Sunday 30th Sept. 1917. On guard tonight. The first of the three big drafts (450 in all) left at 3 this afternoon. The last one of 200 was picked 
