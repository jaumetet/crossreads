  
 the food & says the soup is very salty as thin as threepence & tastes like stove polish. 
 Raining like fury & cold stiff wind blowing. Awnings taken down & of course no shelter. The best tea so far came tonight - sago & raisins. Breakfast  this morning was but a sausage & a quarter each, & I  was fit for a small ox. 
 The lads amuse themselves over my attire, it being pure & simple (very) & such a remarks as Oh mother! Look at that child! are quite common 
 20/1/16 
 Mess orderly again with Gertie Hooper - more agony . Glorious breakfast of curry & rice. Rice not been soaked, meat honestly all fat & the curry part is grease which set like yellow wax when cold. Washing up this with luke warm water is quite a contract. Not bringing my domestic qualification into use too well disgusts Gertie & he agrees to scrub the floor, wash up, wipe up, scrub the table  etc & all I 