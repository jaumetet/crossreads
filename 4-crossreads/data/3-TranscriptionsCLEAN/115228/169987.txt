  Jly 28th '17  :- During the past 
 week our Brigade has 
 been acting as Duty 
 Brigade of the Division, 
 consequently we have 
 had a good deal 
 of reconnaissance work 
 to do capturing a 
 few prisoners during the 
 week. Today our Regt. 
 moved off from the 
 Brigade camp, situated 
 about 11/2 miles S.E of 
 Tel-el-Fara, & took over 
 three redoubts further 
 round on the flank. 
 These are man every night. 
   
