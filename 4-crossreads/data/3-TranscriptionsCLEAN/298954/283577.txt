  
 The scurvy outbreak is getting very bad.   Between 20 and 30 of the worst cases are in the hospital but fully 50 % of the prisoners are suffering more or less from this disease. 
 The flesh of some of the patients is quite black and putrid in patches.   Many are quite helpless and are in a terrible condition. 
 Some fresh potatoes have arrived, but no other fresh food to speak of and the poor devils with scurvy continue to rot visibly. 
 Fri.  22nd Feb. 50 Iron Crosses arrived - also a little fresh meat:-   the meat was much appreciated. (by those who got it, I, alas, am again on a tinned milk diet!)   Wiesmanski, in the next bed, got an Iron Cross - quite a touching little scene tonight when mein Herr Kommandant and suite blew into the hospital and presented it.   I was scowled at fiercely for grinning during the ceremony. 
