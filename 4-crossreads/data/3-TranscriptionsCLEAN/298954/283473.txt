  
 1917 3rd June Certain stewards are permanently assigned to  those  our tables - the only difference with the crew is that each man takes his turn as "peggy", as the steward is called. 
 The "Wairuna" was brought alongside the raider this morning and the work of plundering her commenced in earnest.  We were allowed up on the poop deck in the afternoon:- coaling planks & baskets were rigged between the two vessels & a big coaling party had commenced to transfer coal.  I obtained permission to go on board the "Wairuna" to "bring over my personal effects" (which had already been passed over by my steward.) 
 However, I managed to give the "tip" to those on board to bring over all the food possible, & also managed to plunder a bundle of towels etc. from the linen-room. 
