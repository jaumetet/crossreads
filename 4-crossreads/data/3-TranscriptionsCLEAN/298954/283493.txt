  
 The remaining mines were lowered to no 3 hold again today. 
 Wed 4th July. Rumours that a torpedo was taken up on deck to replace one fired last night.   The "Wolf" is steaming S.E. at full speed in a heavy sea.   We are not allowed on deck at all today (as yesterday.) 
 Thu. 5th We came on deck for our hour's exercise this morning to find "Wolf" a different ship. 
 Her telescopic masts & funnel have been lowered to give her a low, squat appearance.     She is still running in a S.E. direction with a heavy sea on her quarter.   Sleet and light powdering of Snow.   The Paymaster was on the poop during our hour - a queer looking Hun with a head the colour and shape of a damaged bladder of lard. 
