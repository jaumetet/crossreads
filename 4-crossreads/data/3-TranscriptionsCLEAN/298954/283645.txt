  
 Sat 18th May An all-day picnic to the Stechlinsee.   The Stechlinsee is a big lake some six miles away over the hill.   In the beech forest near Augustablick we met two Bosche females with the Wanderlust, knapsacks, flat feet, good complecions, and flirtatious instincts. Swimming most of the time. Lunched with Newstead, Ingham, Hall, & Capn Cole. Newstead originated the Eden Ballet - decorating our nudity with oak and beech leaves with striking effect. 
 A horrible disaster after Appel - I am to go to Brandenburg on Wednesday! 
 Tue 21st Been very busy working with Collier on our "best-seller." 
 Ward (bless 'im) interviewed the Kommandant & protested at my leaving Furstenberg & the Kommandant, impressed with my German bow and convinced there from that I  must  be a Person of Quality, assured me that I would be a very short time only in Brandenburg. 
