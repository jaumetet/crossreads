  
 1918 Tue 2nd Apl. Dreary, basalt-coloured towns whisked past, varied by very new red-brick farmhouses and, strange to say, not a single specimen of the famous Holstein breed of dairy cattle was to be seen during the entire journey. 
 We arrived at Hamburg at 5.30 pm;  sharing our compartment en route with two really pretty and quite chic ( and  affable) girls, a dashing widow, a worried-looking haus-frau, and two unter-offiziers. 
 I treated my unter offizier to dinner in the 1st Class Restaurant of the big station at about 6.30p.    the large hall was crowded, chiefly with officers and their women-folk;   & the entrance of the Englander again created quite a thrill. 
 We occupied a table right in the centre of the room, and I proceeded to cause an absolute sensation. 
