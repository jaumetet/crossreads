  
 1917 After trying various anchorages around the island the vessel was stripped of her provisions & coal, an amount of kauri gum & hides were stored in the forward holds of the "Wolf", & the Engineers and crew came on board this afternoon. 
 Fri. 15th June The 9 officers of the "Wairuna" are pro. tem. by themselves in the little corner next to the mine-room.  In addition, we have Martin, the Creole Mate of the "Dee".  5 hammocks are on top - there was a rush for these as the top hammocks remain in position all day. 
 The unfortunates owning bottom hammocks have to roll them up of a morning to make way for the two collapsible tables, have no place to sleep in during the day, & must replace their hammocks at nine every evening. 
 Martin, Rees, myself, McKenzie & the Captain have the top row in the order named - Rees and I find it very comforting to ly in our hammocks 
