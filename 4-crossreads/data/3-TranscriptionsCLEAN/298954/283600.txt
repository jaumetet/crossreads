  
 Tue 2nd Apl. (cont.) We alighted at the Bahnhof.   What was apparently the Trawler's Quay is very similar to the Fisherman's Basin at Hobart, but in this instance a battle-cruiser in dock and a network of steel-trellis at a submarine yard provided a background which little old Hobart has not! 
 The main court of the Bahnhof was crowded, and the advent of an Englander followed by an unteroffizier and three soldiers carrying baggage caused quite a sensation, 
 I earned the regard of my escort by buying them beer and coffee in the Wartesaal;   after the first drink the braves always addressed me as "Mein Herr".   We kept up quite a conversation with my 20 words of Deutsch eked out by French phrases. 
