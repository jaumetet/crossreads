  
 A couple of broadsides were sufficient to bring everything movable to the deck. 
 Dust, splinters of wood, bolts, and lyddite fumes filled the hold;  the Australian corporal yelled out that all were to ly flat on the deck to avoid splinters. 
 Very few needed any urging to do this. 
 The Mauritius niggers produced huge rosaries and prayed volubly.   After the other vessel had had sufficient the lift-boats full of passengers and crew were brought alongside "Wolf". 
 Those rescued were bundled into the former mine-chamber, no 3 hold.   Men, women, and children were all mixed up together. 
 In one corner (we were looking through the rivet-holes in the bulk-head) was a group of Europeans including a few women.   The women were calm but very pale. 
