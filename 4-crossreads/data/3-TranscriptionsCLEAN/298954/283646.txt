  
 1918 Tue 21st May Farewell trip to the Roblinsee swimming.   (I have grown to love Furstenberg and the Roblinsee and almost dropped salt tears into the limpid lake.) 
 Evening with Frampton, Clarke, Balfour and Ansell. 
 Wed. 22nd. A very mournful breakfast with James at the unholy hour of 6.15 am.   Dear old James almost wept into the cocoa, and Collinson (my orderly) acted as though attending my obsequies. 
 Quite a crowd  attended  gathered at the gate to Farewell Jago and I, well forward being Frammy in negligie (very much so.) 
 To leave their beds at such an hour showed such genuine "palliness" that I again almost sobbed.   (Isn't this a tearful page?   Tears have been about to be shed in the Roblinsee, in the cocoa, down my back, and at the barbed wire gates!) 
