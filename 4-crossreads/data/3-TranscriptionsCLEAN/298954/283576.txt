  
 1918 Thu. 21st Feb. The concert last night was followed by disaster. 
 Had today been promoted from a cane lounge to a hammock owing to a fresh rush of scurvy patients and as I was clambering up somewhere near the roof the hammock ropes snapped and I was precipitated right on top of a scurvy-stricken son of Han sleeping right underneath. 
 The ebony-hued one yelled, as also did dear old Webbie as he rushed around looking more than ever like an angular old female in his long kimono and weird-looking blanket-hat. 
 Moans and morphia this morning - I blamed the fall last night and Webbie blamed the large feed of dried potatoes and "Mrs Crippen" I insisted on eating yesterday. 
 I was strapped into a coffin-like stretcher and carried forward again feeling very low and [indecipherable] - tearful almost. 
