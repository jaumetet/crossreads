  
 "An Awful Story" Caste. Princess Pellitoes .. a coloured damsel ..   H.L. Smith King Gobblu  H.J. Heck. Miss O. Perc, the passenger    R. Alexander The Chf Officer ... of the wrecked vessel  ... E.A. Buckingham The Bosun   ...   C. Hampson Pall ..  Another passenger ... O. Newble. Cannibals etc. Songs, "Tell me".   Smith & Hampson -           "Upidee" (parody Excelsior")   Ensemble. -           "On Moonlight Bay"    Ensemble. 
 Smith's make-up was great, a little frizzled wig, a black skin, a bouncing figure & his only garment made of rushes. 
 Sun. 29th A repeat concert, much better, all old stuff.   The "Story" was ditched (no black make-up left) Smith & B sang "Milestones", Dennelt.  "I love the Ladies" & "Swim Son Swim". 
 Tue. 1st Oct. A stormy general meeting of the English theatrical company. 
 The committee were censured for putting on such a show & a lively meeting is predicted for Friday. (I think, talk & do nothing but things theatrical now.   'Tis a most welcome diversion.) 
