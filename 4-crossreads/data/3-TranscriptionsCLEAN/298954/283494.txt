  
 1917 Mon 9th July A sail was sighted at abt 3 pm.   Position 26 1/2 S. 166 3/4 E.   The boarding party left the ship at abt 4.30 pm in the launch - it was not necessary for the "Wolf" to fire a stopping shot. 
 At 4.35 pm heavy smoke was sighted on horizon.   "Wolf", suspecting a trap, rushed off at full speed, leaving the boarding party on the sailing ship to take their chances.    Steaming hard all night. 
 Tue 10th July. The seaplane, which was unshipped and stowed below during the run in the Tasman, was hastily re-assembled this morning and rose at 9.30 a. in perfect weather to locate the abandoned sailing ship. 
 Wed 11th "Wolf" did not rejoin her capture till this morning.   The raid laid off the vessel and transhipped food and other stores in the ship's boats.   A light swell running.   A woman and a little girl waved across from the poop of the ship. 
