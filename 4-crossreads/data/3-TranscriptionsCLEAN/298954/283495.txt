  
 1917 Wed 11th July (cont.) The crew were transhipped at about midday and the vessel was shelled during the afternoon, in all 39 shots were fired - the gun on the poop was in action and rained down a shower of dust and splinters from the hatchways on to our quarters. 
 We left the wreck at dark. - it presented a weird sight as the shell-fire had scattered the cargo of case-oil over the side. 
 The oil was burning on the water in the radius of perhaps half a mile from the burning barque.   She was the 3 masted barque "Beluga" of New York, 55 days out from San Francisco to Sydney with a cargo of case oil.   Crew of 11 and the Captain's wife and daughter. 
 Thu. 12th The crew from the "Beluga" bring an amusing yarn on board.  The "cruiser" which frightened the "Wolf" on Tues. night turned out to be a "three masted oil-tanker (probably our old acquaintance the sugar boat "Fiona". 
