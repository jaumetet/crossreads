  
 March, 1917. Friday 23 Company and D.H.Q moved out in morning. Cleaned cylinder and had a look round the evacuated camps and collected some magazines, jam and fire wood. Went down to Tricourt Village as pump wouldn't work but it recovered. Cold & windy day. 
