  
 March, 1917. 3rd in Lent. Sunday 11 Sunny part of day & cloudy rest. Usual parades inspection of arms and dismissed. Played bridge in morning. Leave to Albert at 3 after Savage practised giving orders on us and being sent back for belt. Very slow - no pictures - Church service instead. Didn't wait. Home at 7. 
