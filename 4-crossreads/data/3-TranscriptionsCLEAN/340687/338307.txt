  
 January, 1917. Wednesday 3 Feel very tired this morning but arose about 10.30 & wrote this up and started answering letters. Dull windy day. Read paper and wrote letters in the afternoon and evening until 10.30. Thought I might be off so took French leave and Dave did not dispute it. Ingram home at 1 p.m. Rumours of leaving on Friday but heard nothing further yet. 
