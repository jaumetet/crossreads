  
 June, 1917. Monday 18 Day hot and thundery in aftn. In morning ran out a raft and at 2.30 full marching order for kit inspection. Passed rest of day pleasantly reading various useful things. 
