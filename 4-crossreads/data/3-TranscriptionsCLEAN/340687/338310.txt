  
 January, 1917. Epiphany. Saturday 6 Slept deeply until 11 a.m. and got breakfast or rather dinner at 12. 2 parcels. Sarah - unknown. Stayed in bed until 3 when I visited Ludlow Day off to-night and am going out to survey our positions to-morrow morning at 8. Went early to bed and had a good sleep. 
