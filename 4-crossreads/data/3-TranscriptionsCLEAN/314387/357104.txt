  
 Saturday 15th 
 Usual trench duties no poses In trenches until 26th June June 27th Relieved early morning by 56th Battalion came back to La Hoyose line in dug outs - June 28th Nothing during day nightime went up towards line laying cable. June 29th Sick all day did nothing nightime went to Benecourt June 30th Training for a raid 