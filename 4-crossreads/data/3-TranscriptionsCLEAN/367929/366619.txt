  
 May 1915 7 Fri Told that I was to take the regiment and we were to leave Sat night. Short notice. 
 8 Sat Left camp at 11.30 pm and marched with heavy packy to Helmieh entrained at 4.20 for Alexandria. 
 9th. Issued with puttees and infty pants. Embarked on DEVANHA with 30 on the Kingstonian. Fine weather. 