  
 [On back of Docks picture] Dardanelles 3 June 1915 Dear Girlie Am quite well. The chap who was my mate in the orderly room was shot in the foot. The man who accidently broke my glasses has been shelled. I shall shall have some great tales to tell if I come back. My  step father has also enlisted. I have no paper or envelopes and the  censor ship is very strict indeed, we arrived here on 17 May. I am  getting plenty of tucker. We live on the side of the hill the Australian and  N.Z. men stormed when they landed and have dug out places  in the  earth to protect us from the shrapnel shells on top. The weather is splendid  you know more of the news than we do. 
 Passed by no. 2199 Censor 
 Addressed to Miss Flo Ryan at Clarenden st. 
