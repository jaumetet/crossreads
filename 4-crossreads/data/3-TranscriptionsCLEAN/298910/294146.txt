  
 9/5/17  Wednesday 
 Slept till midday - then got up & had some dinner. Raining all night - cleaned up slightly in the afternoon - Bert Trail and I went for a stroll through the Village of "Faurelle". Our Division seems to be moving up - so expect we shall have to go in again. Got a Gas alarm about 2 am but nothing eventuated. Fritz shelling the Main Dressing Station all day & night, several motor cars blown to pieces - one Sergeant killed & many injured. Heard Fritz had retaken "Fresnoy", having used some new Gas on the Canadians. 
 Thursday  10/5/17 
 Very warm - dull to drizzling rain. We are camped alongside an Aerodrome and it is most interesting watching the Planes ascend & descend. Wrote a long letter to Aunty Lyons. Colonel Manifold & the A.D.M.S. addressed all the Bearers of the different Field Ambulances, he said he conveyed the highest compliments from General Gough & General Birdwood about the way we worked under heavy fire 
