  
 Gun on the line near Carnoy, it is a wonderful piece of mechanism & was firing at 14 miles range. Some fellows were blowing out rats with gelignite - the result was that also blew up an old German Mine at Cost Corner - near Montabaun Camp - some 90 casualtys - 10 killed outright - many huts - tents blown to bits. 
 Friday  16/3/.17 
 Nice day. Heavy trench & patrol fighting. The Boys still forcing Fritz back. Good few trench feet & wounded coming through. 
 Saturday  17/3/17 
 Nice day - Very mild - At 7.30 am the Australians pushed on after heavy fighting & took possession of Bapaume, great excitement in all ranks - men who were fagged - men who were back in camps some 6 miles - although they had only settled - speedily volunteered & were most eager to be up & doing - men who would probably have been in our the Amb hands tomorrow, from various complaints exhaustion etc, forgot about such things 
