  
 men with trench feet, the Doctors are trying hard to get not only a cure but a preventative for this painful complaint. Got a letter from Aunty Lyons. 
 17/11/16  Friday 
 Fine - but cold - nearly frozen. Very few wounded. A Taub last night treated us all in this vicinity to machine gun fire during the night. Glad to say one of airmen who was up got him, brought him down not far from here & again we had the displeasure of dressing the occupant & who in answer to why he had shelled etc our quarters, very profusely apologized & said they thought it was an aerodrome. Heavy bombardment going on. 
 18/11/16  Saturday 
 About 3 inches of snow fell - started to rain about 10 am. Bitter cold. Very quiet & very few wounded coming in. 
 19/11/16  Sunday 
 Drizzling rain & very raw. At night we got a bit of a rush of patients - mostly sick & trench feet, held for the night, will evacuate them early in the morning. General Birdwood 
