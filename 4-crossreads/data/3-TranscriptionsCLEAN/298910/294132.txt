  
 Birdwood decorated many of the Rank and file of our Division at H.Q. 
 Saturday  14/4/17 
 Cold - Raw day - a little rain. Just doing jobs about the Camp. Heavy Artillery firing. 
 X  Easter Sunday  X  15/4/17 
 Drizzling rain all day. Helping to make shelter for our transport horses. Stan DClatke & I went up late in the evening to Irish Guards Canteen - just outside of La Fransloy & we bought for our mess about 100 Francs worth of tinned Sausages - Coffee & Milk - biscuits etc, we thought it was Xmas again. At daylight we got orders to be ready to move out at a moments notice - we found out later that units such as A.S.C. got orders to remove to Albert - because Fritz counter attacked the 1st & 2nd Divisions, he came over in mass formation 17 deep - he attacked & counter attacked six times - on one occasion he took 2 Villages from us & captured a lot of our guns - waggons etc - In a final dash our Boys not only got the Villages, guns & waggons back, but took 200 prisoners & two other Villages - Our Division went out to 
