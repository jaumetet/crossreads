  
 I've always wished to see the world, I ad no chanst 'before Nor I don't suppose I should ave, if there and't been no war I used to read the tourists books, the shipping news also. An I ad the chance of going, so I couldn't help but go, 
 We ad a spell in Egypt, first before we moved along, Across the way to Suvla where we got it & strong We had no drink when we was dry, no rest when we was tired But I've seen the pyramids an Spinks which I had oft desired. 
 I've seen them little islands too, I couldn't say their names An Towns as white as washing day & mountains spouting flame I've seen the sun come lonely up on miles & miles o' sea, Why folks have paid a undred pounds, an seen no more than me. 
 I always wished to see the world, I'm fond of life & change But a bullet got me in the leg an this is passin' strange That when old Englands shore all wrapped in mist & rain' Why its worth the bundle to be coming home again. 
 When we recall fond memorys dear Which cling to us from year to year Of by gone days, in lands afar, Those memories sweet how dear they are Remembrance. 
