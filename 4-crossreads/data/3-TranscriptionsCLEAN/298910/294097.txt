  
 of Artillerys bombardment & barrages, the snow shows distinctly Fritzs trenches & the barb wire entanglements. Fritz must have heavy casualtys for they could see him deserting the trenches during the barrage. 
 24/1/17  Wednesday 
 Heavy frost & snow still about, a lot of the Boys cleared the snow out of an old sap & we had some great skating. A lot of Fritzs aeroplanes about - many big shells dropped all around us & too near to be healthy - luckily a lot of them were duds. 
 Received 40 letters, some dating back to June, from G. Hegerty - F. Hegerty - G. French - Misses Nock - Jim Hagan - Jack & Aunty - Frank - Annie - Eileen - B. Hall - F. Newburn - P. Wilkern  & others. 
 25/1/17  Thursday 
 Heavy frost - bitterly cold. Fritz shelling all around us night & day. Killed the Lt, Q.M. & Warrant Officer & a few men of the 12 Fld. Amb. Who are camped just opposite on the other side of the road as the 12th F.A. thought he might repeat the 
