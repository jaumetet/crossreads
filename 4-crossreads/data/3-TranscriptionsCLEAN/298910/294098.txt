  
 dose during the night, they came over & camped with us - He tried hard to get us - put some 30 shells within a radius of 100 yds all around - he had direction all right but not quite the distance - no doubt he is after the Rly line, but all the same he has no excuse for shelling us as both the 12th & us happen to be flying our signifying Flags & lights of an night. Fairly busy during the night. 
 26/1/17  Friday 
 Heavy frost - absolutely the coldest night I have ever experienced - should say the Thermometre must have been much under zero - probably 30 degrees - Hardly any cases in last night. 
 Saturday  27/1/17 
 Heavy Frost & cold wind blowing - The snow is all contracting with the frost. At daylight this morning the 29th Division on out right opened up a severe bombardment & followed it up, taking two lines of Fritzs trenches, they advanced 800 yards on a three mile front, captured a very important redoubt near Les Bouefs & brought in about 400 prisoners 
