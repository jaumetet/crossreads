  
 Sunday  29/4/17 
 Very nice day. Lined up after parade for Voting - Legislative Assembly. 
 Monday  30/4/17 
 Rather a hot day - Went down to Fricourt and had a bath. Got the afternoon off & went into Albert, all the Estaminets have been closed by the Military for some reason - so was disappointed at not being able to get a good feed. Went to the Anzac Coves Show at night & quite enjoyed it - not too bad. 
 Tuesday  1/5/17 
 Very hot & sunny. Drilling & many lectures, Gas etc. 8th & 14th F. Ambulance called back up the line - cant make out how we missed. 
 Wednesday  2/5/17 
 Sunny & hot - all fatigues & drill ceased at 11.30 am. Had the day off, running off heats etc for Divisional sports. We had wrestling on horse back - pillow fighting etc. I won my first heat in the wrestling although I was nearly pulled to pieces. 
