  
 After some fooling about I was shoved into No 13 Hut. 
 Sat. 23rd Nov. Up before Dentist & Doctor for Classification 
 Mon 25th Nov. Result of classification B.1.A.2. 
 Mon. 2nd December Up before the Doctor again, still B.1.A.2. 
 Fri 6th December Put on the "Nestor" boat roll for Australia, and warned for draft. 
 Sat. 7th Dec Inoculated and medical inspection at No 5 Camp. Spent the rest of the afternoon in Warminster buying a few things for the voyage. 
 When I got back to camp, I was told not to leave the hut, may be marched off any moment. 
 Sun 8th Dec. Transferred to No 2 Camp. 
 Mon. 9th Dec. Handed in some of our kit to Q.M. and issued with extra uniform 
