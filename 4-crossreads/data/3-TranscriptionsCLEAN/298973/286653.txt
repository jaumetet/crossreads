  
 Cpl. Patten states that Leut. Fry was in B Coy & was wounded in the first stunt at Pozieres. 
 The company was in support at the time. 
 It happened on 13th August when a shell hurt and quite close to Cpl. Fry, a piece of shrapnel striking him in the abdomen. He was then removed to First aid station & died on 14th August & was buried at Warloy. 
 D. O. Stees Capt. 
