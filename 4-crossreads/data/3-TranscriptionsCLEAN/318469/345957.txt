  
 30th Late in afternoon Enemy subjected us to fairly heavy bombardment in vicinity of Courtney's Post but did very little damage.  Our Artillery replied otherwise things quiet. 
 31st Things quiet on both sides today, although our Howitzers did good work in afternoon dropping shells into "German Officers Trench" & on "Johnsons Jolly". 
 1-9-15 Enemy's Artillery very quiet today in this Sector.  Our Howitzers did good work in afternoon shelling "German Officers Trench".  Things quiet generally. 
 2nd Not much of importance today.  A little shelling on both side.  It is expected the enemy will make an attack between now & 6th. 
 3rd Nothing much of importance during the day.  Our Howitzers did a good deal of shelling usual & appeared to get well into the Turkish positions.  12 of our boys (Engrs) had to go into the firing line & stand to from 9 to 7 a.m. tomorrow. 
 4th Same as yesterday - nothing fresh.  Still expecting attack from enemy.  Usual shelling & some heavy stuff from enemy late in the afternoon.  Warned for firing line again tonight but did not have to go. 
 5th Just the usual Artillery & rifle fire today.  Nothing fresh of importance. 
 6th Not much doing today out of ordinary.  5th & 6th Brigades came into lines today & were a very welcome sight to see as our boys are tired out - those who are left of them - many being ill after 20 weeks fighting.  The new lads appear to be very fit & well - a good lot. 
 7th More of the new men came into the trenches today.  The old boys are very relieved as they initiate the new comers into the art of trench warfare.  Not much doing of importance. 
 8th Nothing of importance today out of the ordinary - a little bombardment on both sides - mostly ours. 
 9th Things quiet - still a little shelling as usual & the usual trench rifle fire.  Nothing out of the ordinary. 
 10th Off duty today.  Not feeling too well.  Nothing fresh except arrival of new troops who are very welcome. 
 11th On light duty today - feeling better than yesterday.  Nothing of importance.  A little shelling on both side.  The weather is getting much more cool.  Expect to be relieved soon. 
 12th Resumed duty today.  Not much doing of importance.  A little shelling & usual rifle fire on both side. 
 13th Most important thing happened in our Camp today was the arrival of the 4th F Coy to relieve us.  A little rain this morning.  Things quiet.  Do not know when being relieved. 
 14th Nothing fresh of interest.  Same old work all along the Line. 
 15 Got word today that we are to leave camp tomorrow for an unknown destination for a few weeks holiday.  Half Coy going, other half going later on.  Think going to Lemnos Island.  Things as usual along firing line. 
 16th Today we handed over duties to the Fourth F, Coy & we leave for only holiday tomorrow.  Should have gone today but for some reason or other cannot get away. 
 17th Nothing for us today except getting ready to leave for our holiday.  Things quiet along the firing line.  Left camp about 7 p.m. & joined 2nd F Coy at "Whites Gully".  Got away from Anzac Beach about 10.30 & went aboard the "Partridge".  Helped unload some mail bags & then got to sleep on deck. 
 18th Left from off Anzac for Lemnos about 8 a.m. & had a beautiful trip across to the Island. 