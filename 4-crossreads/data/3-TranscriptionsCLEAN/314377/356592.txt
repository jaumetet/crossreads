  
 cards. He dropped a bomb on the tent doing a lot of damage. 
 Whilst here word came to me that I was to have my leave to England - As will be expected I was filled with anticipation as to the change. 
 We were whilst on these billets employed on fatigue work at or near a place called Zonnebeke We would be taken up to this in a light railway wearing tin helmets as a protection from shrapnel. It was a dangerous job. Every day there were causalities whilst I was there 
 One time a German aeroplane flew over the gun pit and dropped 3 Bombs quickly one 
