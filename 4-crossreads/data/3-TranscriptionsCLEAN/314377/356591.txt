  
 when killed The ends of the propeller had been sawn off. 
 Our next move was in motors which took us in round about fashion to Chateau Segard. Here we did a forage round for food. Our search was rewarded by finding 4 tins Bully Beef. 
 We also had a look round for Hessian. This we employed in covering the windows of huts to prevent the light showing out and making a conspicuous target for the German airman who were flying around in the vicinity 
 We heard that one had spotted a lighted tent in the 2nd Battn on which a number of men were playing 
