  
 to help the farmers on their farms. 
 We did one of our longest route marches, a distance of 15 miles. F.M.O. - full marching order 
 Attended the Y.M.C.A. tent frequently whilst here. There was cricketing [indecipherable] here and the chaps would enjoy themselves of an evening. The lice were bad here, as elsewhere. They never liked leaving us at all. We were set to work given an iron and ironed the seams of the trousers with them. 
 We had periodical inspections of our Gas Helmets to see that they were in good working order We were very partial to French bread but it was very hard to obtain supplies 
