  
 underground dugouts dug along the sides of the bank and covered with curved sheets of iron supported by sandbags of earth This was to protect the troops from aeroplanes which were becoming more and more active as the days went by. 
 I had some money with me the remainder of my leave allotment. This I gave to a sergeant to mind as he was not coming into the line with us. Shortly after we set off for the line 
 It was a gruelling march and a long one to our destination There had been heavy shelling hereabouts and the ground was all chopped about by 
