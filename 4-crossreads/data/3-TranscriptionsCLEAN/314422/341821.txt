  
 Sunday Dec 30 1917 
 1245 Very cold all day. There was the usual half holiday today. But it was so cold I did not go away from the camp. We have a good hut to sleep in, a [indecipherable] hut - but it is rather far away from the stables. The Sgt. Corporals and a few privates sleep in it. I am one of them. This hut has a corporal of the A.M.C. in another division in it with a stock of medicines and it is used as a dispensary. The others sleep in a barn which is very cold and draughty - 
 Monday Dec 31 1917 1246 New Years Eve Reveille 6.a.m. We got word late last night that the Battalion was shifting to Neuve Eglise & the transport 
