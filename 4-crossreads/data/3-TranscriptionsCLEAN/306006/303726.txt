  
 at El Burj. But as we left over a mile of Turkish wire which is much superior to our D3 wire I intend going out tomorrow to reel the Turkish wire in. 
 3-2-17 Sat 3rd Leask, Wride & I left here at 9 oclock this morning & had a very enjoyable ride to El Burj  Went to the 5th Mtd Bde Head Qrs & after a good deal of arguing recovered all the Turkish wire. Had dinner there & returned, The Tommies are getting the wire road down in good style also the railway. 
 An enemy plane came over El Arish this morning but was out of range A mine was washed up on the beach today 