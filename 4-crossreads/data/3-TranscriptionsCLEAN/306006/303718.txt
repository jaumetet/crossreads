  
 been nothng in orders, & he is still Major as far as we know. He had been dubbed "Woodrow Wilson"  since then, & B Sqdn as neutrals 
 24-1-17 Wed 24th I have a badly swollen throat & the Dr was thinking seriously of sending me off to Field Ambulance this morning but decided to give me till tomorrow to show an improvement; Two English Officers were out looking over the country today, a Taube flew over us today very high & received a warm welcome at El Arish from the Anti Air Craft guns. 
 A few more Turkish deserters came in today 