
guard in the rain all night & day & consequently got drenched to the skin & it was terribly cold rain. We were relieved at 4 P.M. on April 5th when it was still raining.
April 6thStayed in camp all day.
April 7thGetting prepared for our departure. down at the wharf loading horses on barges to be carried to the transports.
April 8thWent aboard the Malda which is worse than the Ionian on account of her being smaller & also because we have all the transport horses on our boat. We were kept busy all day loading her with stores etc. & worked till late at night.
April 9thStill out in the bay at anchor & still loading with stores etc. Appointed sweeper today.
April 10thIssued with Iron ration & a further 150 rounds of ammunition making the total 200 rounds. Still at anchor in the bay received pay.
April 11thD & C companies went ashore to practice disembarking.
