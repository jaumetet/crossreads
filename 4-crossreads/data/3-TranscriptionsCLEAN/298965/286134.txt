  
 fighting range. 
 Feb 3rd The Second Contingent arrived & are now camped at Zetoun & amongst them were Two or three [indecipherable] viz. I. Lamb, G. Smith & J. Ryan. The 7th & 8th Batt. Arrived back in camp without seeing any scrimmaging. The gardens zoo, & Museum of Cairo stand out on their own & the buildings in Heliopolis which is a suburb of Cairo are said to be the best in the world. The hotel out there which has been converted into a military hospital is absolutely the biggest  hotel in the world. 
 28th Feb Sunday Instead of going on church parade we got the order to down tents etc & at about 2 p m the baggage transport together with escort moved off on their 10 mile march to Cairo station at which place they arrived at about 5.30 P.M. I being amongst the baggage guard 
