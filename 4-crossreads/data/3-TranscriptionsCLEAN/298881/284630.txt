
to begin with, then we will talk peace.  There was an end to the most horrible war but a bad peace went on staggering from one wall to another.
As for a workers and soldiers council in this country we could not allow a sectional organisation to direct war or make peace.  That was the duty of a nation as a whole.
Let us think steadily of the winning of the war.
Both eyes on victory
Not looking cross-eyed for victory.
These phrases by the Premier were greeted with tremendous applause by the huge audience.  M. Kerensky, Russia'sStrong Man handed in his resignation