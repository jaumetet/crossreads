  
 28.1.15 
 will probably be shifting to Heliopolas next Saturday.  there has been some fighting near the canal by to-nights paper reports, but none of any consequence. 
 Thur 28th This morning an advance guard to Heliopolas left here to get things in order for us there, I believe we shift on Saturday;  The A & B Sqds returned at dinner time   Stan had to go on duty in the orderly room, as the other Sq. signallers were out on the march  had no lamp practice to-night as the lamps were sent away on the transport, with the advance guard. 
 Fri 29th Went out with led horses this morning & digested rather a large 