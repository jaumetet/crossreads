  
 Dec 7th 
 managed to get away again last night & they were all caught coming on board.  a strong guard was put on to stop them getting away, but the guards were helping them to get down the ropes to the boats.  (Creighton) was the only signaller that went ashore;  They were all hauled up before the Col at ten oclock this morning; & those that went in one night only, was fined 2 days pay & 2 days cells & those who went in both nights brought 6 days cells & pay. 
 The Colonel apparently found out that bluff would not act, so he arranged to get us leave & all the N.C. officers were allowed on shore & 5 men from each troop.  Stan & I were told that we were lucky. Alexandria is a dirtier town & [indecipherable] 
 