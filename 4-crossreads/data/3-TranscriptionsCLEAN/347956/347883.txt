  
 9.2.15 
 until after midnight;  It was 2 AM when we arrived back in camp, & had to go to bed hungry as we could get no bread;  The ground was hard & the picks blunt & we had to sink the trenches 4 ft 6 in deep, & we just about knocking when we got back & took no rocking to sleep 
 Tue 9th Were hauled out at 6 o'clock this morning as usual, & had another Brigade scheme;  we were marched out to where we sank the trenches last night & had to fill them up again; so it is a bit of a mugs game digging trenches in the night & filling them up in the day time. 