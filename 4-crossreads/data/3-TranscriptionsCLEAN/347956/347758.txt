  
 Oct 8-16 
 Thur 8th To Maroubra Sig. went in surf bathing afternoon  concert in camp at night 
 Fri 9th Branding saddles morning.  mounted signalling afternoon.  went to Mosman & were supposed to be signalling got back to camp 12 [indecipherable] Sat 10th Concealment work.  went to S. Major's evening had a good spread, & concert, a very good evenings sport & back to camp at midnight 
 Sun 11st Church parade & a Christening morning went to Felicia & to Anniversary service, to camp at midnight 
 Mon 12th Signalling at Maroubra beach went in for a dip at dinner time. stayed in camp evening 
 Tue 13th To Maroubra Sig. had "Idleboy" in for a splash in the surf.  to Mascot after tea 
 Wed 14th Sig. off hill to the station at Maroubra the artillery practicing all around us. Went to Maroubra for the evening 
 Fri 16th Sham fight morning.  washing afternoon.  gave a dinner to S.M. & party Jessie & Nowland were present & we had [indecipherable] evening 