  
 Sep 24 - 30 
 Thur 24th Was paid for Idleboy.  Packing up all day, & went to the Crown Studios to sit for P.C Photos.  then to the Sydney Hospital & then to M Edwards & camp 
 Fri 25th Called out at 4.45 AM to embark about 60 horses sent on board, marking kits & rugs all day; the Pen family brought out tea, Mr Pen arrived here from N'mine. 
 Sat 26th Received orders that the Star of Victoria is not ready & cannot embark.  could not get passes  Stan & I scaled the fence.  saw boxing contest between McCoy & Griffith, a draw 
 Sun 27th Reg. picking up scraps of paper morning, no Parade afternoon.  got passes evening & went to Felicia, then to S.H. with Jess 
 Mon 28th Flag drill morning.  mounted station work afternoon.  scaled out & went to Manly & to Sydney Hospital evening 
 Tue 29th Flag drill all day lantern drill at 7.30 PM  Stan got leave & went to Felicia 
 Wed 30th Flag drill morning  mounted station work afternoon, went to the city & had my teeth filled  [indecipherable] Stan & Jess went to Felicia 