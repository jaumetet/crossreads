  
 II 
 when it was seen that the poor fellows had ended all their troubles. 
 The battalion going out had evidently been "spotted" for after they had passed the shelling became so very violent that the remainder of the journey through the Sap was by "fits & starts" & it looked at one particular time that the end would never be reached. However being almost empty the damage done was not then very great, although one or two poor fellows who had been congratulating themselves on being "Walking Cases" never finished that journey 
 At the end of the Sap it was necessary to take a rest for between the end of the Sap and a wall affording cover, was an "exposed" place, which had to be crossed on the run. So after waiting four or five minutes to get ready, one by one "the sprint" was made & every one passed safely. A sigh of relief was given by each, for it was only a "casual" shell that would now do any damage & it would have been the "rottenest" luck to have "stopped" one of these at this part of the walk. Nine dressing stations in all had to be passed & at not one, could any attention be given on account of pressing cases demanding instant treatment. 
 For four miles the walk continued. All along that road could be seen horse & motor lorries, "tearing" & galloping along going up loaded, coming back empty While on each side was a long "string" of wounded men making progress as well as possible, in twos & threes, helping men with wounds in feet or legs, but every one in the best of temper smoking their cigarettes & pipes as if they enjoyed them, & trying in every way to make their painful position as light as possible. 
 Wherever possible drivers of vehicles would stop & help the worst cases with a "lift". Even the artillery men, of the nearest batteries, turned out their limbers to help the struggling but lighthearted men along, the slightly wounded in every instance giving place to a more severe "case". 
 At last the dressing 
