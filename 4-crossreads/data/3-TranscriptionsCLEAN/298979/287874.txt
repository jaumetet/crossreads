  
 A Coy men, who had made their bay in the trench fairly comfortable, 50 or 60 yards further down where the water was even deeper. 
 A Coy men strongly objected to this change! Why could he not send his own men to the new place? Why should A Coy men whose work made a bad "posie" a good one, have to hand it over to B Coy men who had done nothing to improve it? The Officer was told, It was plainly evident A Coy had no officers & he was taking advantage of it! 
 Of course talk like this should have been resented but the officer must have known he was doing wrong in treating A Coy men in this manner. Like everything in the Army it is self comfort, & self interest invariably first all the time. 
 Naturally he wanted his own men to be as comfortable as possible but for it he certainly had to put up with some insolence & the men suffering would never forget it, no matter how fine an officer he might possibly prove later. 
 When posted in the new position they did not even attempt to improve it for they did not know how long they would occupy it. 
 Half an hour after this A Coy's Sergt put in an appearance & tried to get the A Coy men put back in the post they had improved but he failed. He explained that he had been posting men on the "Wood" front and on the left. 
 Shells from the Hun batteries had been continuously falling all along the trench since day light but although it was bursting close so far no casualties had been reported so far. 
 Snow rain & sleet fell alternately & standing in the bays with water over the boot tops & snow piling on the hat & coat did not tend to make the men too cheerful probably the shell-fire so long as it fell harmlessly was really a blessing in disguise for it certainly kept the men from feeling too acutely 
