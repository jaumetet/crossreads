a6666038.html
51
& Indians.  The engines are is stoked with wood & we stop at several stations to procure fuel.  It is an up hill pull for about 30 miles, alongside the Barada river all the way.  About 3 pm we reach the summit of the eternal hills & commence to descend.  We cross a large plain & about 4 o'clock we arrive at Ryaak.  The station buildings have been burnt.  A large Aerodrome was has been captured here with a number of planes.  At 4.30 we arrive at [Moolacca ?] (Zahle) & we transhipped into the Clearing Stn.  It is a two storied building & has been used as a Turkish Hosptl.  It was originally a hotel & a signboard testifies that it was the Hotel Orient Madame Antoni.
31st October
   I stroll through Zahle which is on a hill & meet a Yankee girl, also