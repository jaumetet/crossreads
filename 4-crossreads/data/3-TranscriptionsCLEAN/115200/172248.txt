  
 35) 
 [were?] coming in all Directions  we made our way to the Beach to find out where to go and find the Battalion  They were on [popes?] post  But we Did not join them until April the 29 th   So we were away from our Battalion for the first 5 days  We leave the beach on the above Date and go up Schrapnel Gulley and join the Battalion 
 finished 
 [Diarist has drawn a line to mark the finish of this section.] 
 30/9/15 
 No 2 auxiliary Hospital 
 Still there is no [outlook?] of me getting away from here. I have been here a month now and I wish that they would Dischard me 
   