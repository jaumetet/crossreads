  
 Should this diary go astray or be lost the return to the following address would be deemed a great favour by the owner. Sgt W.K. Gillies Australian Engrs 
 Mr W Gillies c/o H.M. Customs Sydney N.S.W. Australia 
 [Transcribed by Lynne Palmer and Judy Macfarlan for the State Library of New South Wales] 
 