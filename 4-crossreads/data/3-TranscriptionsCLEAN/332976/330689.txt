  
 [Transcriber's note Sgt William Gillies is an engineer on duty in France in various war fronts on the Somme River. He details the construction of engineering aspects of the front including bridge building, construction of trench ladders, bunks, dugouts, pontoons etc. The diary covers the time from March 1916 at Engineers School in Sydney to September 1917 at Polygon Wood.] 
  Sgt. William Keith Gillies Lawry Av. Mosman. Sydney 
 Diary 
 Active Service Abroad with the Field Engrs. 15th  2nd  Coy A.I.F. Rgt. No. 7073 
