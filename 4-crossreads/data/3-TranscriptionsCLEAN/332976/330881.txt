  
 June 1917 stream where it was parked & later in the day received two other pontoons & superstructure from Bray sent down by the Divisional pontoon Park. 22nd   We carried on with our pontooning again to-day & four more pontoons arrived.   The other two companies are working further up along the river above the lock & we have picked a very suitable spot. 
