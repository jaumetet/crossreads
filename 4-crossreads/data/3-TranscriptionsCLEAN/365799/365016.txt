
[Post Card:]Photograph taken by Pte A. F. Fry No 1340, 2nd Reins. 13th Batt. 4th Inf. Brigade AIEFPurser SS MorindaBurns Philp & Co Ltd. 1914.
[The following text is handwritten along the right-hand side margin:]Copies from back of photograph.