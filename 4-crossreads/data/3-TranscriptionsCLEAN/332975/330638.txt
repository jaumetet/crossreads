  
 Left Sydney 5 p.m. o'clock 20th Dec. 1915 - S.S. Aeneas.  Sea very smooth.  Sighted land at Melbourne - again at Albury [Albany?] - no stops - about 2000 men aboard.  Concert on Christmas Eve.  Very Dry one and a poor feed at that.  Buried a man at sea about 2 days sail from Australia on 29th Dec. 1915, 4 o'clock.  Parade for 2 hours a day.  Plenty of fun a board. 31st Dec. Grand concert. 
 1916 - Sporting programme. 1st January 1916 Pillow Fight - Cock fight - Mop Competition - Bun Eating Comp. - Tugo-war. 
 Morris 2nd prize Bun Eating, 10/-. 