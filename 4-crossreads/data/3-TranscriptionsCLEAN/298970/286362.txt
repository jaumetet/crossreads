  
 We carried down a few cases this evening. 
 25th July (Tuesday) 
 Practically no sleep during the night.  The Germans were shelling heavily - 150 H.E.'s into our front parapets killing numbers and wounding a few.  There were 20 casualties altogether.  A Shell nose cap dropped at the door of our dug-out. In the afternoon we changed over to the new dug-out.  Two of our men who have been here for 2 days are being relieved by another two so we hopped into the better dug-out. 
 26th July (Wednesday) 
 Only one case to carry down today.  Two were shot through the head but they died shortly afterwards.  We never carry dead unless they have passed away while in our hands, in which case we 
