  
 On Piquet tonight and for the first time I saw star shells.  They make a fine fireworks display. 
 13th July (Thursday) 
 On Piquet all day in charge of Gas Gong.  Three of us, whilst walking through the village tonight were hailed by civilians to do a little favour which required "First Aid" "experience".  London Rifle Brigade arrived from Ypres, also the Shropshires.  Some of our division came out of the trenches today, having been relieved by the "Tommies".  Antiaircraft shelling throughout the day. 
 14th July (Friday) 
 Guns have been silent for two nights.  Royal Garrison Artillery with 60 pounders leaving last night and this morning.  More arriving to take their place.  More of the Rifle Brigade & Shropshires arrived. 
