  
 December 19 41. Books from Frank S., so library compleat again. 
 Expecting from T.B.C. B. Construction, Georgian Poetry 1916-17:  Padraic Pearse Collected Works, 2nd Vol.:  The Wild Swans at Coole, Cuala Press:  and Swinburne's "Songs Before Sunrise". 
 December 20 42. The frost of last night the heaviest of two winters:  and all day long the grey cold mists have further whitened the trees and doubly hardened the ground.  Tonight I have written to Mother:  still I owe letters - about half a dozen:  they can wait. 
 Today C. Section to the 1st C.C.S.:  Les, Johnny, Bert, Buckholz and Namby up to the D.R.S.  Billy F. and self running dressing room for local parades and casualties:  a good soft snap of a job. 
 Pay'd today:  drew 40 F., pay'd certain debt.  No mail. 
 Finished the reading of "Blinds Down".  George E. in today leaves me promised book of the Poet Ghorki's "Heart Ache". 
 Tonight to read J.C. Squire. 
 December 20, 21, 22 43. The Boche by drop'ed notes in Bailleau promises his presence there for Christmas ?  All day the guns have been quiet. 
 December 21 44. Another day of close heavy white Frost, Mists. 
 Mail:  Batsfords Elementry Building Construction, and half an afternoon reading confirms the vague idea of slackness.  Fagging now will mean double it did a year ago, and its damn dry stuff to write the least of it. 
 December 22 45. Mail:  a bonzer letter from Ron - in hospital in Cairo, dated 24.11.17. 
 Homesick?  Jove, Ron so am I!  What a bonzer time we'd have, those few nights meeting in Cairo, by the Egyptian Bar, just for one more of them.  And all the others are dead, oh Ron, keep alive and live through this damned war. 
 Catalogue from C.W. Beautmonts and a writing at once for 1st edition of J.E. Flecker's "Coll. [Collected] Poems" and a setting aside of James Stephens "Five New Poems", 1913, 8/6 
 T.B.C. arrives, "Georgian Poetry, 1916-1917" and Christmas T.L.S.  Letter from Ron gem of day. 
