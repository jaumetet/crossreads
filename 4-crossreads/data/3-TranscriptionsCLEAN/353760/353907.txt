  
 Quehen Isques, B, Nov. 27 - Dec. 15 Senlecques, B, Dec. 15 Wavrans, B, Dec. 16 Dranoutre, Belgium, B, Dec. 17 - January 31 R'Avelsburg, France, D.R.S., Jan. 31 - March 1 Kemmel, Belgium, M.D.S., March 1 - Bailleul on way to Paris, March 9 Paris, March 10 - 17, Train, 18 Kemmel, M.D.S., March 18 La Clytte, M.D.S., March 24 
 Cpl. J.R. Ackerley Alec Waugh K. de B. Codrington 
 Gerald's address 6th Squad, A.F.C., Salop. (?) 
 [Transcribed by Judy Gimbert for the State Library of New South Wales] 
 