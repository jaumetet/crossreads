  
 February 14 159. Tomorrow night the Battalion celebrates Christmas, we here are holding a Bal Masque, enthusiasm seems well spread over the unit, and some good costumes should appear.  Les and Ray C. are Two Ballet girls - costumes of fancy paper, Namby is taking her children, accompanied by Cowboys - in the hut here, now there's isolated yarning, and a general working at 'laidite' endurance and at the trimming of weird hats. 
 We're hoping to produce a second number of Gheitz - with the Bal Masque as the item of interest, Cartoons by Frank and self, and paragraphs generally.  Contributions to date in Gheitz Box are - a well-worn half penny and the enquiring "Who was it who bathed in Villian St. lately with a ---?" 
 However, all things going well, a second number is in the air, and there has been several enquiries. 
 Back from 3rd Batt. Details, Eric Saxby. 
 February 14 160. From a Boche plane dropped into the ground by hut a balloon loaded with illustrated papers - printed in French.  Some fine photographs of prisoners at Cambrai, the high Boche Council and of captured French towns.  Also some rather good Boche Cartoons from Lustige Blatter. 
 161. Turning I yesterday read an hour of "Selections from The Writings of Lord Dunsany". 
 162. Another of Ray's bulls, he talking to another chap who questioned answers "Well my answer to that is in the  affirmative  negative!"  Ray - "Never mind your negative or adjectives, I want a plain yes or no!"  Telling it to Basil, heard another of two chaps at Buire - "What's a deux Franc", "Two Francs you silly --!" 
 163. Although I'm stronger now in books than I've been since at Wimereux, I'm disappointed still at not receiving books from Davis and Oriole, and the pencils from B.  In 
