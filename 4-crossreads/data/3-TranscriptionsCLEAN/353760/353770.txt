  
 [Transcriber's note:  This is the fifth diary of Private Frank Henderson Molony who is with the 1st Field Ambulance.  The diary commences when he is in Quehen Isques, he moves on to Senlecques, to Wavrans, Dranoutre in Belgium, to Ravelsberg, France, Kemmel in Belgium, Bailleul in France and at the end of the diary he is at La Clytte.  In March he has leave to Paris and fully occupies his time at the Opera, trips to Versailles and Fontainebleu, visiting the Louvre, Musee Cluny and various other places of interest.  He is also involved in the production of a magazine for the men in his unit.  As in previous diaries he describes the action around him and the horrors of war.] 
 Next of Kin 
 F. Molony "Muston" Queen St. Woollahra Sydney. 
 125 F. Molony December 2, 1917 
 1st F. Amb. A.I.F. 
 Quehen Isques, France 
 5th Journal 
 December 2, 1917 January, 1918 February March 
