  
 C/o Bank of N.S. Wales 29 Threadneedle St., London 
 3.12.17 
 My dear Babs, 
 Have just got your No. 13 & 15 today & feel bucked thereby intensely, in one of them came the magic carpet of 3 gum leaves & I can assure you they had the desired effect of transporting me back to Australia, right up to Togomain, [Toganmain] though as I don't know that spot I had to imagine myself in a gorgeous garden bathed in sweet scents intoxicating & sensual that went to my head like strong wine, but I must say that I do prefer you in the flesh to yourself in the spirit, a misty cloud.  I couldn't evoke any response from the spirit. 
 And so Cas is going up & you have two charming girls to amuse him, I fear you are an inveterate matchmaker, is that the measure you allow 2?  Well I must say you are a sport, but if I know Cas (&he is the sensible person I think him) he will find you far more entertaining than six of the others. 
