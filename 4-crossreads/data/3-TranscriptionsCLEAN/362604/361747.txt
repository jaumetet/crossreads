  
 Australian Red Cross Bulford 15/9/18 
 Dear Dad, 
 I am answering your letters of 30/6, 7/7 & 14/7.  It is Sunday as usual & I have finished rather early.  I intended writing this afternoon but I met an acquaintance in here & He was telling me a bit about life on the other side.  He was lying in a Hospital gassed when the Germans raided all the Hospitals by Aeroplane & hundreds of Patients, Orderlies & Nurses were killed.  We had scones for Tea to-night & as we were very hungry 'cos tucker has been a 