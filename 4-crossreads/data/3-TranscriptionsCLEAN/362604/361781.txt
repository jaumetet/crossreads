  
 Australian Red Cross Bulford 20/10/18 
 Dear Mum, 
 I have 4 letters from you this week, 21/7/18, 28/7, 4/8 & one other.  It is Sunday afternoon & nobody works but "Father" on Sundays.  It is a dull miserable sort of day but not cold.  We will soon be having the beautiful ice that you have described so well in your letter.  It is quite alright for a novelty but when one wakes up in the morning with socks, towel, soap, tooth-brush, in fact everything that has been exposed a little damp, frozen like a board, the novelty wears off.  I always put my 