  
 Frederick Stobo Phillips 
 2nd Lieutenant [indecipherable] 32 Machine Gun Section 1st Battalion 1st Division Australian Imperial Forces Killed in action Nov. 5 1916 at  Gueudecourt, France.  
 Left  Australia Nov 25th 1914.  
 "To every man upon this earth Death cometh soon or late. And how can man die better Than facing fearful odds For the ashes of his fathers, And the temples of his Gods?" 
 "Blessed are the departed, who in the Lord are sleeping - they rest from their labours & their works follow after them." 
