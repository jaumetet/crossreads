  
 Narrative up to 12 noon 8/2/15 
 The enemy behind GEBEL HABEITA remained in the same camp until the night of 6 th /7 th , when they vacated it. During the 7 th , their camps were seen along the WADI MUKSHEIB, and their retirement v&icirc;a GEBEL MUKSHEID and BIR GIFGAFFA is confirmed today. To the north, their retreat towards EL ARISH is believed to continue. 
 Until important events render it advisable, the issue of Narrative will not be continued. 