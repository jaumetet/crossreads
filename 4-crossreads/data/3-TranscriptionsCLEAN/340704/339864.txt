  
 November 1917 Monday 5 Early reveille moved farther to Shellal. Went into Shellal for water. On the go all the time. Men irritable with all the messing about. 
 Tuesday 6 Standing to arms all day. Inf attack Sharid Went out to Hill 510 on outpost thought Turks might break thro. C Sqd late. We took up outpost then had to hand over to C Sqd when they arrived. Very cold 
