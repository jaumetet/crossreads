  
 September 1917 Monday 24 Outpost to 520 and 630 got shelled and fired on all day Did some scouting away out in front 
 Tuesday 25 Out again to take hill 630 got fired on and shelled We took 630 
