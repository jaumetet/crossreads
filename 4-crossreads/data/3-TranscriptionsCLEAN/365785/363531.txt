  
 A.I.F. Gifts Store - The Savoy Hotel - Cairo.  1915 To the men at the front from Australian Comforts Funds to remind them that Australia Sympathises in their splendid sacrifices, appreciates their achievements and encourages their best efforts in the great - imperial - cause! 
 "Who lives if England dies? Who dies if England lives?" - Kipling. 
 The funds represented are: The Citizens "War-Chest" fund, Sydney. The Lady Mayoress's Patriotic League, Melbourne. The Queensland Patriotic Fund, Brisbane. The Victorian State Schools Patriotic League, Victoria. 
 Together with many contributing funds from other localities. State Funds from W.A. and S.A. are, we hope, being incorporated. 
 Administered through the Commissioner, approved by the Department of Defence for these and kindred organizations other than the Red Cross. 
 Note. - It is particularly desired that the limited supplementary supplies of Milk foods be reserved by the O.C.s of Units or their Q.M.s and issued on requisition of the M.O. of Unit to such men as require only a few days on such ration to restore them to good health.  Hereby do we best help maintain the efficiency of the Unit. 
 Printed at the Nile Mission Press, Cairo. 
