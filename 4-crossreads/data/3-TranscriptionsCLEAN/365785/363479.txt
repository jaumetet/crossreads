  
 Army Medical Corps. Subject. From 6th A.A.M.C. To Major W.H. Read, O.C. 6th A.A.M.C. 
 Victoria Barracks, Sydney, September 16th 1914 
 Sir, The N.C.O's and men of your command desire to express their sincere thanks to the ladies of Wahroonga for their liberal donations of books, periodicals, games, etc., so kindly sent them whilst in Camp. They were highly appreciated by them and were handed over to the 7th A.A.M.C. who are now in Camp and will no doubt also appreciate the thoughtfulness prompting the Donation. 
 A. Higgens W.O. 
