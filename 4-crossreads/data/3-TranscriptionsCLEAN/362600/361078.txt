  
 F 24 Sun Recd. letters from George. Dad &  Mum. 
 Lovely Hot Day. D & I walk to Anesbury. All around River Banks. Meet Ethel &  Silly Nellie. Take Photos. Walk Home very tired out 
 No Tea left. But we have good supper. Wrote to Mum. Clocks put up 1 Hour 
 F 25 Mon Lovely Day. We have cricket meeting. Arr ord. Corporal 8am Parade Streets. I write all night. 
 Wrote Dad. George. Eileen Stirling. Nell 
 F 26 Tues. Lovely Day. Ken & I go to see "A Better 'Ole" It is very good. On New Staff March in 2 St Sergeants 
 Wrote Jennie. MRL. 
 F 27 Wed Am not well. Have long horrid day & go to be at 7pm & read etc. 
 F & W 28 Th E & I go for walk round Block & pay for stand up seat at pictures. See Pictures of Turkish Harem &  very good. I come Home & go straight to bed. Pay-day & I am in need of it. 
 F 29 Fri March night Jude to Sarum stay with [indecipherable]. We go for walk [indecipherable] also. Have a real nice time Show all Photos. Nice ride Home. 
 [indecipherable] of MRL has Bath. [indecipherable]  everywhere  &  meet Top in Douglas near Turkish Harem arr [indecipherable] esplanade. 
 W 30 Sat We met all day until 5pm other lovely. I clean up room & have a bath  & sew &  write letters. 
 Wrote to cm Hunt. Dave. MRL. Chris 
