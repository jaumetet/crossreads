  
 F 15 Sun Very busy day. I go to write letters & meet Norman so I can't write until after Tea. 
 Wrote MRL (Dad.um. Sent Photos.) Roy. Jennie 
 W 16 Mon We have noises in the night & Ellis gets up to investigate Very windy [weany?] & wet. 
 Wrote Maisie Carter. Lottie. Ida. (strickout) Roy (strickout) We play Billiards & Bridge. 
 W 17 Tues. Go to Sarum 12.29. Go shopping & to close. See procession & Gen. Slater in his cars. Go to c10.13. Back to Close. Go to see H [indecipherable] No good. Take Girl to [altar?] with Bridal bouquet. See old Chest in Vestry. Go to do 13 after supper Very dopey. Bed 1am. 
 F & W 18 Wed Very wet at 9 pm & Tam comes down. I write Poetry. 1355+1 letters to Here Wrote Esme. Sadie. Maisie. Hardom. Marnie Clarke. Nettie. George. 
 F 19 Th With 2 showers 1/2 each & lovely rainbow at 6 pm. I go for walk around block & E goes to dance. Recd letters from Marie re sweets [indecipherable]. I have a long hard day & fix Electric Cooker after Tea. Wrote M 12 LES. NETT. marched out on 17.9.19  Dulce & H.Hey 
 F & W 20 Fri Go to Pictures with Harry. I Recd P.C's from Norm (Lord St L'pool) & Mr G (Lord St S'Port). We have another patient strike & co comes around my division 
 Wrote Laura. Mr [WO?]. Gives me list of jobs that he's neglected. 
 F & W night 21 Sat Go for a walk around the block at night. Recd a letter from K. Pine. Have a H- of a day. Tommy. W. comes down & we have a long yarn. We are both in a very bad skin yet at all [indecipherable] 
 Wrote Mem. Marg.Alice. [PC] to Maisie. Ken Pine. Norm. Doris. 
