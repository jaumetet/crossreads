  
 Sunday, 12/8/17 Up 8 a.m. & after breakfast went to promenade for couple hours then back to town.  Looked around streets then had dinner - roast duck etc. costing only 1/7.  After dinner met Tommy who had been to Yanco & Narrandera district & then went by train to Christchurch.  Came back & had tea 5 p.m. then walked to Central Stn. catching 7 p.m. train for Salisbury - via Sthampton., Eastleigh & Romsey.  Arrived Dinton 10.15 p.m. & camp 10.45 p.m. going to bed 11 p.m. 
 Monday, 13/8/17 Out on stunt all day arriving back 5 p.m.  Received letters from Aunts Sara & Jane.  Wrote to Aunt Jane. 
 Tuesday, 14/8/17 Rained heavily during parade so had easy morning.  Afternoon fine.  Took laundry to Fovant after tea. 
 Wednesday, 15/8/17 Couple slight showers during morning. 