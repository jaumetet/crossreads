  
 4.  By 
 4.  By travelling 1st. class I was enabled to meet men with whom I had departmental business;  the result of these meetings was advantageous to the work of the Department. 
 In one case I managed to get facilities from Mr Wilkinson, Crosswood Estate Agent, for bridge building Maenthur Wood and other advantages connected with this Working. 
 In another case I met the Military Representative of this District and the R.O. and in these interviews settled matters which would have been carried forward almost interminably by correspondence. 
 If however the rule is a hard and fast one, then please deduct difference. 