  
 Wednesday 24 April '18 Last night we slept in full dress expecting a call up theme, but it never came. Instead Fritz came over with swarms of planes & bombed the whole district for hours. It was very late before I got to sleep. The stunt we heard of came off. Some of our chaps attacked Meteren, but, although they gained some ground, it is uncertain if they took the village. I fancy not. 
