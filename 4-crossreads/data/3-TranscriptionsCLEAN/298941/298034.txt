  
 of this church is useless for observation, & not used anyhow. Fritz seems to make aim at the churches before all else. Our old candle-stick steepled church at Caestre, is reported to have been smashed up. The dressing station in Caestre must be in rather a dangerous position. To think of that road from Fletre to Meteren over which Grey & I walked last winter (not long ago) 
