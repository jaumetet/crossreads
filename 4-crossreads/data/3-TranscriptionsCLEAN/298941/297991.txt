  
 We have passed through Bologne & Etaples, so on this journey I have been within sight both of the hospital & convalescent camps I was in. Weather is putrid. Engineers, one or two infantry & machine gunners & our four A.M.C. are in this truck - 30 all told All night it appears there were constant querulous demands to remove feet from faces & vice versa, but I spent a very good 
