  
 the latter won. The Anzacs, or those who were on the Peninsular, came in for a great deal of chaff from the "banditers", concerning their "old eyes & war weariness". By virtue of the latest order, any man who was at Lemnos or on lines of communication in Egypt while the Peninsular fighting &lt; was going on is entitled to put A up over their unit colours, 
