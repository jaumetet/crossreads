  
 in a field & then on per motor lorry to Brigade H.Q. on Caestre-Cassel road, about 2 kilos from Caestre. Here we are at present billeted in a barn. Hot tea & bread & jam tonight. Heavy guns & pretty active. On the horizon in the [indecipherable]; a big red flare shows where Bailleex is burning still. Heavy shells are falling on Paestre which I hear is knocked about badly. 
