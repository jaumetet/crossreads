  
 113 milk for which she takes a place in the queue & how her health stands the strain of her work & the wait in the cold damp air of these dark winter morning with the snow on the ground passes my comprehesion I would have succumbed to it long ago She really possese a reserve of energy that if I could acquire I could be whatever I desired. 
 After breakfast I dress quickly in old clothes which I possess & a pair of Marie Lebretors slippers that I salvaged at my little chateau de 17 Rue Campagne Premier. Then I betake myself into bedroom with a blanket & hot water bottle or brick provided by Brise or her mother & dig into law then the hour of dejeuner (noon) 
 I am not called till the soup is even in the plate & then I am called. I might add that I am consulted as to if I am ready before meal is served. Everyone must speak in whispers & tread on tip toe if I am working. A King could not be better served. 
 A wish is always interpreted as an order & obeyed as such. The food I eat is chosen If I once refuse a plat it never appears again. If I show a 
 [Sketch on right-hand side of page]. 
