  
 which are extinguished by girl who verifies the tickets & for 5 minutes or so before depart of train of train you rest in darkness except for station lights. 
 The line follows Boul Mich in which are escape boxes for steam. Often a surprise to a stranger when he sees one of these boxes idly smoking. Port Royal is an open station & after that tunnel again till clear of city. 
 At St Remy les Chevreuse (20 miles from Paris) we descend & as usual French customs we must cross line to leave station. After 5 more miles  the  during which train empties passengers who flock to point opposite gate the train moves on but stops immediately & remains there for about 1/4 hour. Everyone gets impatient & I suggest to Brise to cross the compartment of train but train moves on. 
 Day is glorious & sky clear. 
 There is a track leading to Chevreuse & naturally I take to avoid main road. We pass Chateau de Cobertin a deserted & badly placed chateau when so many lovely spots about here. 
 On other side some one desirous of view has constructed a chateau on the side of a hill that is almost perpendicular & without any visible means of communication save by climbing hill & by facing his chateau he has lost the best view of the valley. The ruins are visible & easily find the way 
