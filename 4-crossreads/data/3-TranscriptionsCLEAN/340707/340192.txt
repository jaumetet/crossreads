  
 has no traffic. 
 Here I saw a one legged soldier looking mournfully on the hilarity & dancing & then go up & ask a girl for a dance & she pretending not to notice the wooden leg did a few slow turns with him for he had gained the right to such a small return at least that is how this pretty laughing little creature put it though he smiles were lost in tears as she spoke. 
 We did not dance but moved on to Montmartre 
 14-7-19 I have called this 14-7-19 but really there was no division line. I do not know where midnight found me. 
 Montmartre ie Boul-Clichy, Rochechouart has been transformed into a pleasure ground. Side Show, [indecipherable] shows for real champagne swings, aeroplanes, merrygorounds of all kinds from motor cars to pigs, steam swings, relics from Noumea. Every conceivable side show is there. 
 We visit steam swings a contrivance which succeeded in frightening more girls than all other side shows combined & hence most popular. Ride last about 2 minutes & costs 1 fr Merrygorounds are usually hand propelled & cost 50c. The machine asks a pourboire but never gets any. 
 The relics of torture were good but nothing from Noumea was visible though this is what attracted me. We saw pancakes 
