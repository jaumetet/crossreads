  
 with numbers like this Illuminations there are none for the electricians have gone on strike. The decorations are I think better than those of Paris & some of the private firms are excellent eg Frascatis & Army & Navy 
 After supper at the Strand we adjourn home. 
 There was one thing that I liked about the French procession & that was that in front of all marcher were wheeled a 1000 war cripples. This had no counterpart in England but seats in the Mall were assigned to widows & orphans, to Chelsea pensioners to wounded soldiers & to school children. In France school children were provided for but I dont know about the others. Seats were made of empty ammunition boxes. Some was done at Paris I find 
 22-7-19 Rose at 5-30 am this morning & breakfast was ready for me & Lily was waiting to say good bye. This will be the last time I shall see her as she is being married on Bank holiday. 
 I get a good seat & run into Folkestone at 10 am By mistake of MLO I get on Boulogne boat & dont bother to change when I see Calais boat or tub, a Belgian affair. MLO tells me I can run 
