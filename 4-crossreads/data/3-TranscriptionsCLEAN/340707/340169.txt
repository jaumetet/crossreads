  
 41 but on the whole very quite 
 25-6-19 I rise at 10 am & breakfast in bed & then write up diary from 18/6/19. 
 In the afternoon I meet Duke at 53 Ave Montaigne which is the American HQ or rather one of them for they are over organised which perhaps accounts for their failure as an army unit. We drive down in a car to Place du Theatre Francias & dine at [indecipherable] for 4.50 fr which is cheap considering locality. 
 After this as it is too early to visit Montmarte & Duke wishes to meet someone at 8 pm who incidentally does not turn up. We take coffee at Grand Cafe & met one of Duke friends & we arrange a little dinner for tomorrow night. 
 About 9 I take the lead to introduce Duke into Bohemia for he has been very good to me. We visit Cafe Enfer This adjoins & is similar to Ciel. Demons await on you everywhere - Consommations [indecipherable] cost 75c - Red lights & red decorations 
 Place resembles a grotto & the statutes are usually human [fiends] & guillotined heads with the blood still oozing. Guitar & string band supply music & when hall is full we are invited upstair to the [indecipherable] which on arrival is absent. There is a stage & on this a woman is standing in tights & then flames mount at the bidding of Satan & consume her till nothing 
