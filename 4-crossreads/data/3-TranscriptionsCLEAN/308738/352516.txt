  
 Sept. 18 Saturday 1915 Working all day. The sorter who got drunk & insulted Cpl Grice got 28 day CB at Abbassa. Received parcel from Maude today everyting came to hand in 1st classs condition. 1 Pair socks, 4 tins tobacco 1 pipe, colored K chiefs, 1 tin lollies. Was issued with 1 suit of kharki clothes, 1 razor, 2 towels, 1 holdall, 1 pair boots, 1 pair braces, today Dick Pladell says about this date the mail bag was taken up to Cooks Agency & registered under the name of Thomas. 
 Sept. 19 Sunday 1915 On Guard from 12. 30 today till 12. 30 Monday. at Post Office. Went to Scots church with Pethard both morning and night. Afternoon spent in writing up a detail account of my doings from leaving Australia. 
 Sept. 20 Monday 1915 Working all day.  Posted letter to Maude & Harry 
