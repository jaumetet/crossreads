  
 Dec. 25 Saturday 1915 Woke up at Mena House to find all sorts of provision made for our cheer. Lottery tickets were give & parcels with all sorts of dainties handed round. Concert at night. Went for a walk with a few friends today as far as the Pryamids, there are crowds out to see the sights & the camels & donkeys are having a lot of work. 
 Dec. 26 Sunday 1915 Not  too bright. Resting all day. 6th A L Regiment arrived at Alexandra. More letters from Home I wonder how they are putting in their Xmas. 
 Dec. 27 Monday 1915 Dr says I can go to Helonan Regiment intraining for Maadi Am feeling better & really do not think that I am bad enough to be sent back. 
