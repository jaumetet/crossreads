  
 We were soon transferred to another Amb. train and reached Waterloo Station about 2 p.m.  From here we were taken per amb. car to Wandsworth hospital where after shifting about a lot from one ward to another I was allowed to settle down finally in D4 Ward. 
 23rd I was attended to by an eye specialist and a throat specialist today. 
 25th The Germans attacked Villers Bretonneux at last yesterday and drove our people out. 
 26th The Australians and English troops attacked the town during the night and drove the 