  
 21st The long looked for Hun offensive has begun.  It broke out on the point where the British and French Armies meet like a fearful storm.  Masses of huns have been flung into the fray so far they have recaptured a lot of ground in the Somme area. 
 22nd The Huns have taken Bapaume and have pushed our line back on a front of about 60 miles to an average depth of about 5 miles. 
 1st April The Huns have now reached Albert and have taken Montedidier from the French.  This latter is a serious loss. 