  
 T.B.C. Notes . s. d. 114096 - 14.6.18 - "Resentment" - 3.11 S.184096 - 7.6.18 - Pencils, 6 - 2.3 184096 - 19.6.18 - Portrait of Artist - 5.1 185096 - 4.7.18 - 3 Sheldonian Series - 8.2 The Anchor - 6.10 Returned July 17 - Cuala Book wanted:  Darn them!  19.7.18 - Book of S. & Wonders - 5.7  (P.13648) - 22.7.18 - Story Teller's Holiday - 2.4.8 24 (?) - Hauptmann - 5.16 (?) 
 Pot-Boilers, Clive Bell 5/-, C.U. 
 Book Notes "Rosas and Camilla", Masefield "The Somme Battles", Masefield "Counter Attack", Sassoon, 17.7.18 
 [The following ten lines crossed through.] "A Story-Teller's Holiday", George Moore "Secret Springs - Dublin Song", poetry L.L.R. "Resentment", Alec Waugh "Judgement of Valhalla", Gilbert Frankeau "The Crock of Gold", Stephens "Look!  We have Come Through", D.H. Lawrence, C.W. 5/- "The Oracle of Colour", Kidder "Memories of Childhood", Freeman "The Tale of Igor", C.L.B., 10/6 "Reincarnation", J. Stephens "The Gold Tree", J.C. Squire 
 "Faith of Our Fathers", Card. Gibbons 
  The Little School, S. Moore  
 Last Poems 2/6, Francis Ledwidge, H.J. 
 [The following two lines crossed through.] "Raptures", W.H. Davies, C.W.B., P.B. "The Dark Fire", 3/6/18, W.J. Turner, S.J. 3/6 
 "Folly", T. Maynard, E.M., 17/7/18 
 [The following seven lines crossed through.] Exiles, J. Joyce Georgian Poetry, 19.3.16 William Blake, Symons, 7.6.18 The Elizabethean Playhouse, 7.6.18 "Portrait of the Artist as a Young Man", James Joyce, 4.6.18 "New Paths", Beaumonts "The Anchor", M.T.H. Sadler, 13.6.18 
 "The Contemporary Drama of Ireland", 5/-, G.A. Boyd, Fishburn 
  S. George for England, M.R. Liston  
 Essays Irish & American, J.B. Yeats, F.V., 4/6, 17.7.18 Poetry of R. Brooke, July 25.7.18 
