  
 August 5, 7 288. A place made for poetry:  by a wide circling pool of bright green tiny-leaved covered water, a splendid tree of branches that bend again to the ground - black branches tracering the live green of their leaves.  Another tree, standing sentinel before the main building, a mauve clear trunk, and branches that fantastically twist and turn, and leaves as large, and more gracefully pointed than those of the grape:  in yesterday's sun the colour of the trunk and branches, mauve green and gold dappled.  Now in the pale sunned afternoon sheep are grazing about:  but the shepherd is  not  by Millet [Jean Francois Millet] rather by Hassal [John Hassell]. 
 Disappointed at the Rupert Brooke book not arriving. 
 289.  Allonville, Aug. 7 The last entry, to compleat to the midnight arrival at Allonville. 
 Yesterday a long day, packs up and away from Wardrecques by 4.30, and in the deep grey of the morning along the wet road to St. Omer. 
 A long day in the train, twenty of us in a clean cattletruck, so full room for sleeping. 
 Detraining in the early evening, clear skies, sun and roads under the thin Somme mud. 
 Packs up and away again:  once we followed a Tommy's directions, and lost a mile in the approach direction. 
 Climbing a great hill by the country lovely in the shadow, the sky by the sun's setting a flaming cloth of gold. 
 In the narrow dark hedged, sunken road by a lorry collected "Maps" and a scattering of other staffs, and carried us in:  on to the rear H.Q. where we collected papers and gear. 
 Then till 2 this morning along main roads and bye roads, past the Madelaine, on through dark country, the morning mists, slowing rising and growing. 
 Then to the Chateau:  all the deep 
