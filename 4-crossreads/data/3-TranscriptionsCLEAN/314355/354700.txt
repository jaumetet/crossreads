  
 March 28, 29 16. Afternoon:  Our 4th Division, the N.Z. and the Guards are holding the Boche by Beaumont Hamel.  Good old 4th, they've been in every stunt of the fighting in France and Flanders, with the exception of Vimy Ridge and the Beaumont Hamel.  So Henencourt is still ours, so "Ginger's all right".  "I'll bet she's P.O'd long before this!", says Sol. 
 Probably we leave here on April 2.  Tommies doing forced marches to relieve us. 
 Posted away "The White Stone" and "Rebel Songs" home. 
 A grey day, and cold hard winds. 
 March 29 17. Good Friday, and agonies of gas amongst Artillery chaps through the M.D.S.  Two dead here this morning, two dying in Buck's car. 
 Through Bailleu with cars.  Bailleul with her deserted streets and lovely tower has the air of a mourning woman whose beauty and lovers are gone.  The utter feeling of loveliness [loneliness ?] is as poignant in its way as the tragedy and desolation of grey Ypres, but Ypres in her death is expressive of a tragedy that mourning would demean, in her grey ruin wander quietly and sadly the artists of her buildings. 
 Bapaume in her ruin, had a horrible air of thwarted terrible intention, and the cold hatred of the retreating Boche being about her ruin buried streets. 
 Albert?, the ruin of her cathedral expressed blatant pretentious grief, a shallow proud vanity being about her ugliness, the quiet poor silence of the little village was far more heart-rending than all Albert's  work  ruins. 
 18. Posted away Diaries IV and V and the old Henencourt sketch book, to Mrs. H.  Will post home "Holiday and O.P." of J. Davidson.  "Fleur de Lys" (M.F. book) and "Ginger Mick".  Will have to chance the losing of "Ginger M." and the Henencourt Rue de Bas autographs and Dranoutre Xmas Day page. 
 20. Adamson back to unit two days ago. 
