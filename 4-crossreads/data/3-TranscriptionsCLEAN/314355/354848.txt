  
 June 16 Mass was being celebrated by a tall green and gold caped Priest at the High Altar - a great all golden altar, with golden candles and golden flames.  It is a beautiful Altar, and a beautiful church. 
 But St. Omer is a wondering city:  her people are silent and they hear always the noise of night planes, and the compacted crashes of hellish bombs, a great many of the houses and shops are blind dead places, and Lorries and ambulance cars make nearly all the traffic. 
 Back at Cassel at 6, and a tea at the tea shop where they sell all the food control prohibits - pastry, preserved fruits, chocolate and tea. 
 A slow slow lazying from Cassel to H.Q., reading in a field "Exiles" and lying watching the clouds and the sun sinking behind Cassel.  Back at H.Q. the two black windows flanking the hut door, were the black eyes of a vicious gloating monster. 
 Now a half moon, whiter than silver, dazzling in a deep luminous night blue sky, over by Strazeele sudden gold star flashes and silence all about us.  Ends a day enjoyed and lived through, of sudden impulses and fresh impressions following one upon the other. 
 "Day that I loved, day that I loved, the night is here." 
 June 17 213. From T.B.C. notification of Waugh's "Resentment" being available, so wrote for it, also from them six Venus Pencils. 
 Tonight the first of "The Elizabethan Playhouse" essays read, interesting and full of facination, must read it well and fully. 
 With Andy C. to farmhouse, and a supper of  Eggs  Chips. 
 Early this morning heavy shrapnelling about Sylvestre Cappel. 
