  
 March 1916 Wednesday 15 March to Albert Park, guests of Red Cross Ladies. Excellent lunch. I had lovely 20 mile drive in car. Back to Melb at 1.30 special leave to purchase . Saw the block and the styles. Girls smostly done up, but still cold and distant. 
 Thursday 16 Left Melb at 3pm for Colombo. Only got 1 wine from home. No letters or word from anyone else.  Aust Flying Squadron joined us here before leaving 
