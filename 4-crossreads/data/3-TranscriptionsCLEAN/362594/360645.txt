  
 [Fevrier 23 Dimanche] Had a trip on Nile in a Feluea with [indecipherable] & two others had good time flying two sorties sent [indecipherable]. 
 Sgdn. goes on 7th [indecipherable] D.V. 
 [25 Mardi] Dinner at Shephard's with Mjr. Addings, Beaton, Glass, [indecipherable] & [indecipherable] Champagne.  Sqdn leaves on Sunday for Aust. & I leave with them. 