  
 found that the Order had never been issued from Headquarters.  So evidently it was a case of the working of wheels within wheels for some reason and the commotion all ended in a mull. 
 In brief I'll try and remember a few interesting items that have happened.  We are all in turn going to have a fortnight's furlough - Isn't that lovely? 
 Six of our Sisters have been mentioned in despatches and are going to get the Royal Red Cross - Lucky beggars I wish I could get it.  Rush is one of them. 
 The Duke of Connaught paid the Hospital a visit one afternoon, which was an inconvenience to everybody.  All leave was stopped for the Patients and the Sisters who had days off had to come back & stand to.  And after all he only visited 2 or 3 of the Wards. 
 Of Course there was Xmas & New Year and nothing out of the way happened.  The boys appeared to have a happy day - we had our ward prettily decreated with paper almond blossoms.  The New Year passed in ever so quietly - not even the tinkle of a bell.  So different to how we heralded in 1916 at Lemnos. 
 I have been to London 3 times but I'm not keen on it unless it is to go to a matinee or do something definite.  However fascinating 
