  
 Yesterday 4 of us obtained 4 hrs leave to go for a drive & though we can scarcely keep our eyes open 'we are so Sleepy' we enjoy seeing these spots.  The alleged spot where Moses was in the bulrushes - The oldest Coptic Church in Egypt.  The ancient City of Babylon.  The old Slave Markt & the present cemetery, where our soldiers are buried.  Of these again the camera will tell the tale. 
 This little tit-bit I meant to have mentioned before - that the Arrogan is stranded on its own bottles in Mudros Harbour -  glass  not  brass .  Serve them right. 
 13th  The Hospital seems to be emptying fast.  Yesterday morning crowds went away by the Runic & more today by the Karoola. 
 I'm feeling helplessly flat again, for on reading the orders for the day I see we must be inoculated for Cholera.  When we come back we'll be walking bacteriologists for germs, but what will it feel like to have a few will again? 
