  
 watches had to be put on an hour at that time. Our walk home was made enjoyable by the fact that the sky was clear & the moon was full & shed a clear silver light, & the stars twinkled down upon us [indecipherable]in their wonted way. We love the stars because we know that all our loved ones can see them as well as we can & we often whisper quiet secrets to them to carry across the seas for us. 
 Of course our absence from the isolation hut had been noticed by "the heads" but nothing came of it except a threat for the future. 
 April 8 
 Still "isolated". Exercise etc. I read. Emerson's essays on "History" & on "Love"; the latter is particularly inspiring and the former is a fine appeal for a practical, personal interpretation of historic events & characters. 
 April 9 
 Still "isolated". Exercise etc. I read a few of Edgar Alan Poe's poems & liked them very much. Poe has a delicate sense of 