  
 from the exterior, it is most spacious & exquisite & homely inside, & everything is of the best, (including the soup & trifle etc). We took a short cut home from this place & walked for about 3/4 of a mile over a ploughed field. We didn't mind the mud!!! 
 Feb 18 
 Church Parade in morning. In afternoon we again walked to Shrewton & had tea in a little private house. There are quite a number of private people, there who entertain paying guests for tea & to one of these places we went. 
 The place we chose was one of a terrace of houses built in 1842 out of brick & stone & mud. The terrace was built shortly after the great flood of '41 & all profits coming from the houses in the way of rent go towards the upkeep of the poor of the parish. There is a large plate on the terrace giving 