  
 Vol III 9 Feb 1917 - 6 May '17 
 [Pencil note] No. 6359 Pte McRae J.D. 19th Battn A.I.F. France 
 Diary of experiences etc On Active Service J Duncan McRae 
 To mother and father with love from Yours most affectionately, 
 Duncan. 