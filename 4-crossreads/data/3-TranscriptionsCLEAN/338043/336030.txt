
Dec. 3rdPack up, load lorries and leave Le Cateau at 9 a.m. - During our journey (by road) we halt at one point to allow the King & Prince of Wales to pass. The Royal party are at present visiting French Towns.- Arrive 2 p.m. at Avesnes. A good but crowded billet.
Dec. 4thA day which is spent at the small hall where we are to show - The Germans have left it in