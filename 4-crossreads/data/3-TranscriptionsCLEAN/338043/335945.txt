  
 & streets - Sea front - a gold mine discovered in the shape of an English Book Shop - We are camped at No. 4 Convalescent Camp, which has chiefly (for its occupants) British & American Troops. - Bed. 
 Aug. 8th Diary - Completion of "Arabian Nights Entertainment" - An afternoon free from duties is spent on the "Havre" beach - Unlike our Australian Beaches, pebbles (round & white) 