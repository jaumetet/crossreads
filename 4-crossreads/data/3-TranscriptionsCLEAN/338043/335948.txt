  
 route having run parallel with the Ocean we receive full benefits of exhilarating sea breeze - At night we give first show in  the American Y.M.C.A. to American Soldiers, Nurses, Surgeons & Independent American people - Great enthusiasm greets & dismisses - the Anzac Coves. 
 Aug. 11th Sabbath - A free morning gives the opportunity of seeing "Etretat" in all its beauty, on a glorious 