  
 Saturday 18th August 1917 Grooming and harness cleaning. Taubes over about 9.30pm Sky lit up by our search lights, got on to a Taube. Guns gave him a hot time but he managed to get away. 
 Sunday 19th 9.30 pm Taubes over dropping bombs. Church service 10am. Half holiday. Harnessed up wheelers about 3pm went to H.Q. for comforts. One of Fritz planes brought down near camp by anti-aircraft guns. 
