  
 Friday 5th October 1917 Nr Reninghelst Harnessed up and left lines at 2pm. going to Krustraat Dump for ammunition. About 2 miles from the lines, motor wagons bumped into my donk giving him a nast cut so had to unhook my two donks and return to lines. Had wound dressed by Vet. Serg. Of 2nd Battery 1st Bge. 
