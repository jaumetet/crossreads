  
 Tuesday 31st July 1917 
 Great number of German prisoners being brought down from the line. Very heavy gunfire all night. Heard we had a good advance our guns still going strong this morning. Very heavy bombardment about 10pm. It seemed as if 10,000 shells a minute were fired. The dark, cloudy morning is as bright as one of our well lit streets. 
