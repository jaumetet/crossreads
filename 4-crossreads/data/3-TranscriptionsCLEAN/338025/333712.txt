  
 Wednesday 13th June 1917 
 Amiens  leave. Left camp about 7.15 am and walked to Albert station     where I got the train. Left Albert 8.30 arr. Ameins 9.30am. After having a good look round and visited the Cathedral we left at 8 pm. Arr. back at camp at 10pm 
