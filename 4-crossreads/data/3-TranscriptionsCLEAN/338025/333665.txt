  
 Thursday 19th April 1917 Cleaning up old camp ground. Burning the straw, putting empty tins in shell holes, and getting a waggon load of timber for cooks. Harness cleaning and greasing waggon wheels in the afternoon. 
