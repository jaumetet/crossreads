  
 189 Today send order of 1st to Times Book Club for Symons "Spiritual Adventures" and France"s "The Amethyst Ring". 
 Symons has lately published a new volume of plays - "Tragedies" that has been rather well received by the 'Times Literary Supplement". 
 190 One of Nina Murdoch"s I dedicate to xxxx. "So we who did not know her mystic power Nor discerned yet of the subtle web she wove Had foolish vows we could not keep an hour Trembled and laughed and kissed - and called it love." 
 191 In today"s 'Daily Mail" 'For disparaging the British Army in a public house, a Canadian soldier named Southern, who said he had no recollection of the matter and urged his undoubted loyalty was at Portsmouth yesterday sentenced to one month"s imprisonment. 
 In one opinion expressed and otherwise, how long would we get in the land of "freedom of speech, and liberty". 
 192  Sep. 17 Today"s the Mater"s birthday.  Many, many birthdays - under better days.  Tonight I received two letters from home, one from Father and one from Mother. 
 193 Thousands of miles away we"ve discovered a lovely country.  Coming away from it we have found it, Australia. 
 194  Sep. 19 Came in a few nights ago one, A Taylor, of the R. Berks.  He"s down as 21, but 31/2 yrs. service - he was, or is in the permanent R.B.  Talking to him, he turns out to be far different to the average tommy.  He"s dark, black hair, clean well formed features, slim and finely built.  He enlisted when he was 15 - hence age is about 181/2 - that I guessed from his manner.  Life in the R.B. is evidently - for the recruit, something like a chapter from the "Life in the German Army". 
 One thing spoils him:  horrible tattoo mark of Brittania on his chest and small design on his arm - the usual type. 
 195 Bill of The Play German Empire Theatre of Varieties. Proprietor, Herr William Kaiser.  Tel. 2.L. Manager, Herr William Kaiser, 2nd Tel. 2.L.2. Treaties not guaranteed. Children in arms not respected. Coming, Coming. Farewell visit before going to St. Helena. 
 Ultimatum Willie The greatest comedian the world has known, in all his latest successes including: "My hopes Argonne" My Arsne Kind dearie O!" "Oh, Lorraine again!" "Has any body here seen Calais!" "Alsace where art thou!" "Don"t be Hun happy!" 
 Herr Gullem And his celebrated company of Actors direct from the Wolf.  Pross Berican]:  in a 1 act play entitled "Al-lies". 
 Count Branchoff The Diplomatic Dandy, in his world famous Repertoire including: "Why I left Austria, Hungry" "Ta Ta Bel Grade" "We all love Sophia" "Servia right.  Servia jolly well right" 
 Baron von Wholegg The great Illusionist and Contortionist, in a marvellous exposition of the "Scrap of Paper" trick, supported by the renowned [indecipherable] Count Ben Handy. 
 The Crown Prince and Lost Company In the screaming farce, "The Sham-Pan Shifters", an episode in the Partechnicon of the German Army. 
 Engaged at enormous expense, General von Pluck First time out of Europe in the popular song, "Come Tsing Taw me. 