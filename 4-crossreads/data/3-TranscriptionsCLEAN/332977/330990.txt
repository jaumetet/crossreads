  
 captured by the 11th Battalion, severe hand to hand go, trenches 8 ft deep Turk counter attacks easily beaten off, captured two machine guns, severe bomb and Artillery duel, we loose 22 deaths 70 wounded, Turks have 100 killed Turks squeal like pigs, they mined ours but we got in first blue light gave the signal to start, 3 men killed in B Coy by our Howitzer falling short 
 Monday 2nd heavy Howitzer fire on our guns 3,000 kitcheners army arrive 
 3rd two men killed by snipers, 8"shell falls in grave digs corpse out his head on our parapet, his clothes on telephone wire 
 Wednesday 4th Aug  Aeroplane duel in air Taube clears out sixth Reinforcements arrive, several English and Gurkhas arrive, Beachy land 40 rounds on new troops arriving Turk see blue light and blaze away for all they are worth 
 Friday the sixth Turks attack 11th trenches make a violent bomb attack, Turks capture trenches, but our lads take them again, we lose 300 men they took us by surprise, Tune and Walker snipe a lot of the Turks, our big battle starts  five  2 trenches taken 5.30 
 Saturday Big battle raging Artillery defeaning 
 The Battle of Lonesome Pine 40,000 Tommies under Gen Stopforth defeated after landing at Suvla bay 