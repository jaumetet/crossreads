  
 Oct 12th My Birthday we have some sport firing on a Turks blanket his batman will get the sack, Monty has a duel with Turks he throws 65 bombs at them Biscuits arrive from Ivy Tommy Hunter Corporal 
 13th 6 unknown men buried (here lies six dead heroe's, 23 Batt chap blows head off with bomb, he held it too long, we have to stand to Turk bomb throwers coming too close, eyes bad get a pair of glasses 
 Oct 20 Two days spell had lovely mutton dinner 
 Oct 21st 12th Battalion man shoots his mate while cleaning his rifle, he looks down Barrel, bad with Diarreha 
 Oct 16th Play accordion in Artillery Road, Turks fire trench bombs at us, our observers give us the office, howitzers try to blow up a trench mortar, they land two on our circular trench, Wade sings out who threw that pluck, 
 Oct 26 heavy bomb lands among nine Platoon packs, & packs are blown to pieces our machine guns knocked out 14 shells fired at it, our machine gun wont go off it is trained on a Battalion of  Turks when the bloke blows the whistle machine jams 