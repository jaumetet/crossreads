  
 18 Thursday Beachy heavily bombards the Beach Taube flies over guns chase it off finds out our Bivouac 
 Friday 19 Wind goes down water very scarce very cold Beachy gets 90 men 
 Saturday 20  Turks fire heavy shells on to our Bivouac one lands left of my dugout, several fall in Gully, we are not disturbed 
 Sunday 21 Church Parade, Gilbert goes to Hospital, officer & men killed first day they relieve us officer puts his head over parapet finish officer 
 Monday Nov 22 Leave Anzac on water tank, then on to Princess Ena very cold cast anchor 6 AM arrive Lemnos after very cold and rough passage 12 men stay on board all day and night 
 Tuesday Nov 23 Leave Princess Ena at 8 AM. Board Ferryboat Waterwith, very cold and hungry no food for two days land at 9 AM. March four miles to camp 2nd Brigade Band plays us in sleep in large tents 45 men 
 Wednesday 24. 25 Rested in camp received bottle Porter 
 Friday 26 Visited villages chummed up with women folks, learned how to make cotton, and 