  
 Saturday 12 Carting stones for roads and boundaries visited Pyramids and Sphinx pay day arranged mess, visited Mena House baths 
 Dec 18 Route Marches all the week. England annexes Egypt, our engineers leave for Suez to build Fortifications 
 19 and 20th had a swim a baths and visited Pyramids climbed 3/4 way up but retired. 
 Monday 21st Heavy tramp in the sand for 2 days 
 Dec 25 Church Parade 10th Sports 
 26th visited Cairo had a good look around nothing but marches till January 8th 
 9th Heavy cold feeling rotten 
 28 January Recovered from cold received Xmas cards from Maud and Glady's 
 Jan  29th 2nd  Australian contingent arrives at Alexander to camp at Heliopolis 17,000 Australians 3000 N Zealanders 
 Feb 1st Australians march to Jeda long line of troops, Artillery, AMC all out four miles long. 
 3rd 4th 7 and 8th battalions goes to Suez Canal fight against Turks 400 captured 