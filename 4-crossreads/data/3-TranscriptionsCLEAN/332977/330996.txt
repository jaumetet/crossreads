  
 Tuesday Sept 1st 700 Prisoners captured on left our lads bring them to Beach 70,000 Troops at Egypt to relieve us 
 Thursday Sept 3rd Clarke captures two Turks, they bring rations with them, I get a new pair of trousers 
 Friday Italians land at Boular, Watching bombard Beachy, Don and I build a snipers possie, 
 Tuesday Sept 1st  Dorflinger  Southland torpedoed off Lemnos 4 Pte and Brigadier killed 6th Brigade aboard beaches in Lemnos Harry Horder killed by 75 Gun both legs blown off, our boys in England make the acquain-tance of some ladies, the asked who the lads were "Australians" the boys replied, "You speak perfect English for foreigners" the ladies replied  they think every state in Australia speaks different lingo 
 Thursday Sept 9th Rocky Maxwell shot by our own Patrol while out on a screen, Baynes and Caine fired on while laying Barb wire things are hot in front 