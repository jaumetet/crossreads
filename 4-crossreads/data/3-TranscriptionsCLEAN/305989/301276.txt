  
 Sunday 30 September 1917 Scaled Church parade, got hand dressed.  Picquet at night.  Wrote [see images for possible Arabic numbers for 8 and 6]. 
 Monday 1 October 1917 Went to F.A. (Circers) and "Spaniard" fixed me up.  Warned for "dump". 
 Tuesday 2 October 1917 Fixed up dump & made bivvys, got some canteen stuff. 
 Wednesday 3 October 1917 Limbers came in with spare gear.  Quiet day. 
 Thursday 4 October 1917 Nothing Doing, shifted camp necessities from Dump & reloaded all limbers with spare gear. 
 Friday 5 October 1917 Quiet day, hands very sore, got them bandaged. 
 Saturday 6 October 1917 Played the B.W.I. cricket beat up by 31 runs real good day's sport. 
