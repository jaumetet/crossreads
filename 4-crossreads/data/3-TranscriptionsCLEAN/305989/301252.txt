  
 Sunday 15 April 1917 Went to church in morning, a Taube came over halfway through service.  Saw Andy Cunningham, pretty warm.  P.C. [see images for possible Arabic number for 6]. 
 Monday 16 April 1917 On Day Stableman:  busy day packing up for big stunt.  Left Khan Yunus at 8.30 & travelled all night. 
 Tuesday 17 April 1917 Arrived at Selal at sunrise, watered up at the Wadi Guzze, camped the day.  Taubes over all day they bombed but did little damage.  Outpost at night. 
 Wednesday 18 April 1917 Left outpost early and came back to camp & left there at 10.30 for supports about 5 miles east, came back & packed up for a big night's stunt, left at 8.30 & travelled all night. 
 Thursday 19 April 1917 Arrived at Hamerea at daylight & took up positions & were shelled heavily.  Turks attacked about 4, heavy firing all day, left possy and came back to Tel-el-Junmel. 
 Friday 20 April 1917 Arrived at Tel-el-Junmel stayed till 9.30 then made 5 hrs. to Guz-el-Selub where we were heavily bombed by 3 Taubes about 14 bombs did damage to the horses  inpaticular, watered at Wadi Guzzi. 
 Saturday 21 April 1917 Stayed out on outpost all day & were relieved at 4.30, proceeded to Wadi Guzzi watered up then proceeded on to Khan Yunus arriving at 9.30, on picquet at night. 
 Abassan el Kebir. 
