
Sunday 3 June 1917On Sanitary fatigue.  No church parade:  wrote [see images for possible Arabic numbers for 6 and 7].
Monday 4 June 1917Field Firing, on picquet at night.
Tuesday 5 June 1917Field firing in morning.  Cruel day, on Sanitary fatigue.  Taube dropped bomb within 100 yards of us but did no damage.
Wednesday 6 June 1917Field firing in morning, saw Guth Tomie:  got good mail, (16 letters).
Thursday 7 June 1917A couple hours drill in morning;  pretty warm, 2 parcels, & several papers.
Friday 8 June 1917Quiet day, a little dusty.
Saturday 9 June 1917Usual routine:  on sanitary fatigue.  Picquet at night.
