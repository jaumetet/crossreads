 a6490167.html 
 Sandhill Camp - 
 Longbridge Deverall 
 Near Warminster 
 Wilts 
 -----   "   -----   "   -----   "   ----- 
 overseas training Brigade 
 -----   "   -----   "   -----   "   ----- 
 It was from the above Camp that I was finally drafted to France. 
 We left on Thursday morning  Nov  Dec. 6  th   en route to Southampton our destination being Le Havre, where our Base in France is situated. 
