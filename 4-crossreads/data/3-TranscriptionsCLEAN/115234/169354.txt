 a6490135.html 
 Reveille",  Tomorrow, many a voice that is tonight upraised in jovial song, will be stilled for ever  "Ce Le Guerre," who cares? 
 Yonder on the brow is our objective, the sinister & illomened village of "Hamel".  Apres Midi, Don & I were sent back to the ancient town of Corbie.  The old place is siimply running with Champaigne: Fritz is shelling it intermitently.  Our 
