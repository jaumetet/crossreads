 a6490171.html 
 The villages of Ligny. Ligny Thilloy & La Barque were right close to Bapaume:  Each smiling little Hamlet was just a mangled heap of Debris when I last saw 'em.  Factory Corner was the busiest spot I ever saw behind the Lines.  A small heap of bricks represented the spot where the factory had once stood. 
