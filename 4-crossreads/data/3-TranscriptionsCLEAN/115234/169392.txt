 a6490173.html 
 The desolation of the Somme Battlefield surpasseth imagination. Of the fertile fields & pretty villages of Fricourt. Bicourt. Contal Maison. Montauban. Trones Wood. Longueville barely one stone lays on another.  Just a heap of ruins beaten into pulpy dust  Searching amid the ruins I have found a pair of Corsets.  A silk garter & a baby's 
