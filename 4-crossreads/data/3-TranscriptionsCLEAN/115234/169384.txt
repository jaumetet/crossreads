 a6490165.html 
  Note  
 The following lines were inspired by the sight of a baby's shoe lying  forlorn  among the debris & shattered remnants of what was once the prosperous village of "Longueville" abutting the sinister & illomened "Devils Wood" (Delville Wood) on the road to Bapaume.  In this particular sector the havoc wrought by modern war has been most complete.  One must have seen to  realise the awful desolation 
