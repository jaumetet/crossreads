  
 7. 2. 1904 
 My Dear Fry Many thanks for the notes you sent me: they are just what I require, and I shall not fail to make full use of them as occasion requires. 
 They are so ample that I shall be able to cut them into two - so you have given me material for two speeches. The charm about them is they will strike new chords in "Postal [indecipherable] Speechifying" and you know it is something these times to strike out on new lines. 
 As I told you I had 