  
 think favourably of this & I hope you will, I can give you the names of some of the others, but not all. 
 Lest it may appear strange that I should write you thus before having felicitated you on your new relations with Miss Macmanamy I would just say that I had waited to express myself personally, but the opportunity has not occurred except when you were in others company. Please accept my warm congratulations now & with best wishes for the happiness of each turn. 
 Yours truly Will Moginsly 