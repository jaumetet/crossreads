  
 Replied to 5-5-17 (Photo) 
 May 3rd, 1917 Sent 4 June 1917 see across 
 Dear Sir, I have just heard through Mr. McCulloch the sad news of the death of your son. It comes to me as a big personal blow. Whilst I know that I cannot fully realise your own great sorrow as a father, yet I feel that it may even be some little help for you to know that others are sad because they have lost a dear friend. And so I would try to express a very 