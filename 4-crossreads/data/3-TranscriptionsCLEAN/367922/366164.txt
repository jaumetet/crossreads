  
 grateful if you would be so kind as to let him have any particulars you think would be of use to him. Also a specimen of "Confused MS". 
 Am now getting settled here. Work is plentiful, as you can imagine in a new office. I am at the old game of correspondence and suppose it will continue for a few months at any rate. Their systems here are far behind those of N.SW. but the Commr. is gradually introducing N.S.W. methods much to the improvement of the work. 
 With kind regards & wishing you the season's comps. 
 Yrs sincerely H. A. Earl 