  
 29/1/17 Plenty of shelling round Hd. Qrs. last night and we had several casualties. 
 30/1/17 A raid by the Fifth Yorks last night and we hear it was successful. 
 We showed the advance party of the releiving bn round our lines last night. 
 I hear we go back to Hametz for a few days and then take over from 