  
 16/2/17 Marched from Fricourt Camp to Bresle about nine miles we expect to remain here six days and then return to Fricourt. 
 We are now attached to the Fifth Army the 4th Army having taken over more French territory south of Perrone 
 22/7/17 Marched from Bresle 