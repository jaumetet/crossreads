  
 right of Flers. This village is not damaged much by shell fire. 
 31/10/16 The rain has cleared after about a fortnight's drizzle. 
 The enemy shelled our position this afternoon. Some of the shells landed uncomfortably near our dugout. One which wounded 3 of our chaps set fire to a small bomb store about 18 feet from us and caused quite a stir. 
 We expect to be releived tomorrow night and remain out 