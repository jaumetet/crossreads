  
 shelled our bn Hd Qrs intermittently all night and this morning but we had no casualties. 
 Still miserable weather snowing nearly every day. 
 16/4/17 Shelled all around H.Q. today but still no casualties. Some of the shells were very large ones. 
 We took up another position between Twelfth and Fourth bns. last 