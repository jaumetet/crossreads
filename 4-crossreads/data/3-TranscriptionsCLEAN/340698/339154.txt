  
 2 days and then do a stunt. 
 1/11/16 Shifted camp to Flers. 
 We expect to go back to Delville wood but H.Q. only shifted a few hundred yards. We are camped in a cellar under the monastery. They shell the village every day. 
 2/11/16 Still in Flers. Charge postponed owing to bad weather. All preparations 