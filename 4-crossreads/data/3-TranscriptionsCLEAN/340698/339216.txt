  
 3/9/17 Embarked aboard the "Onward" this morning and remained aboard all day. But we did not push out owing to some trouble with U boats. Gen. Haig was travelling with us but he changed his plan and went across aboard a destroyer. 
 We disembarked again and the leave party went to a camp about 3 miles out of town. But I slipped away and remained 