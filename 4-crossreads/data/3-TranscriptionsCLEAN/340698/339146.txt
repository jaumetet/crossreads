  
 2/10/16 One of our planes set fire to two german captive baloons. 
 7/10/16 Went into supports at Swan Chateau 
 12/10/16 Returned to Devonshire camp by the same method as coming into the line. 
 Second and Third bns. had a raid tonight. They were both successful. We hear that there were 67 raids on the 