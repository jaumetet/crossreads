  
 Horseferry Road and were given money and clothing. Stayed the night at the War Chest Club. 
 Aeroplanes bombed London last night and did a fair amount of damage. 
 6/9/17 Paid a visit to the Tower and in the afternoon had a look at Kew Gardens 
 7/9/17 Went out to Windsor this morning and returned via Waterloo 