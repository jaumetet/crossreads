  
 6th Thursday. Kit Inspection  I was one Pair Boot Laces short  1d entered in my Pay Book  and a new pair issued.  We have had our Christmas Billy's given us  I scored cigarettes Etc.  and 1/2 Quid in mine 
 7 Friday  Another day at the Rifle Range 
 8 Sat.  Machine Gun Work & gas Masks  Cricket matches against Warminster 
 9 Sun.  Church Parade & Concert's 
 10 Mon  Usual drill 
 11 Tues.  Had a day of Field Skirmishing 
 12 Wed.  Pat Wilson Buried.  one of the Firing Party. 
 13 Thursday.  Usual drill 
 14 Friday.  Ready for Draft 
 15 Saturday.  Taken off Draft  I was the oldest and there were 2 over strength  had a Holiday 
 16 Sun.  On Guard 
 17 Mon.  Applied For 3 day's leave & got it.  Bee line London 
 16 Tuesday.  Having a good time  Met Col. Jolliffe, Got leave extended one week "I'm a good Tale Teller 
 18 to 21st Sunday.  Go to Scotland.  Beats London Bad. Edinborough Great 
 Back to London & Put in time till due at camp. 23rd 
 Tuesday 23rd  Back quite fresh but had to toe the Carpet  The S. Major did'nt know I'd had extended leave.  But soon Learn't 
 24th Wed.  Again on draft.  Have every thing ready.  Dr says are you alright.  I say yes alright  His Exam Over,  I am away this time.  Staying Folkstone the night  All shifting is done in night 
 25 Thurs.  In Bologne then Etaarp's  Camping there  All the rest here never fired a shot. 
 27  Frid  More drill, This is what is call the Bull ring 
 29 Sat.  This is solid Craft. Ive been learning how to carry Duck Board's all day.  The Practice is hard.  The Dinkum will Kill a fellow. 
 29 Sun.  Church. must have that French People pleasant very immoral 
 30 Mon.  More men arrive not many left be hind now 
 1st  May.  No Holiday for us plenty of it to do. 
 2nd Wednes.  Same Drill all day 
