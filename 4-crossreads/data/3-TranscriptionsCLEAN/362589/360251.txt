  
 13th Dec. 1915 
 My dear Father, Unless the exceptional good luck, I have had the last few days, continues, there is very little chance of my getting off the Peninsular. We have had a fairly strenuous time lately & we are going 