  
 comfortable dugout on the post & hope to still not have to shift. I have a wonderful orderly who will turn me out the best Christmas dinner on the Peninsula. I am glad yet sorry that Keith is coming along.  Suppose it had to come. I hope he will have better luck than Arthur you have done your share anyway. Best love to Mrs Ferguson & Dorothy & everyone. I would give a good deal to be back again even Joe Carlos' voice would be music. 
 Yours affectionately  Caesar  
  C R Lucas  