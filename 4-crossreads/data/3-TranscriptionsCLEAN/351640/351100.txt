  
 P.S. 27.10-15 - 11 a.m. 
 Dear Girls:/ Fancy seeing a real newspaper - Egyptian Gazette, after an interval of three weeks One paragraph announces that a mail for Australia will be made up in Alexandria on tomorrow, therefore these & other letters should have early dispatch from Egypt. Good. 
 The S.S. Demosthenes has been anchored in the harbour not far from many wharfs since 9 a.m. and no sign is made for moving to another place. It will come in due time. 
 The account of the death of Miss Cavell, by shooting in Belgium at German hands, is the chief cablegram information. Let us hope that some day the Kaiser and his men will be forced to pay heavily for their conduct. When? Oh when? 
 The situation in France has apparently little changed. Likewise does the news impress one in regard to the fight on the Western front. 
 A strong man is still wanted in both the political and the military lead 'mongst the people in London. Kitchener as an organiser has been, with his limited powers the correct thing. Oh for an Alexander, a Pyrrhus, a Hannibal, or a Napoleon, to direct and guide our fighting men? 
 Oh for a million fighting men of the same mental calibre and physical build of our Australians? Were they available a general of proper gifts and suitable education could do all that is required to make our cause victorious. 
 [Edith Louisa Cavell, 1865-1915, was a British nurse working in Belgium in the early months of the War, known for her efforts to save the lives of soldiers from both sides. She was arrested by the Germans, tried and executed by firing squad on 12 October 1915 for helping Allied soldiers escape.] 
 