  
 by an effort of man.  All the other suburbs are flat wastes of sand. 
 7 p.m.:/  Hurrah!  Hurrah!!!  Hurrah!!!!! Letters from you have just been handed to me.  As soon as dinner is completed, they will go with me to my room there to be read carefully.  The dates on the envelopes "23 March 1915 Sydney. N. S. Wales".  A letter from Dr Peck.  A copy of The Times, The Weekly Despatch from him too.  A third paper from Cairo. 
 Car dear:/ You had a long stay at Blackheath, and from the notes of Joe & Kitty the air was beneficial to you.  That is good.  It is a change of scene from Sydney.  A crowd of friends too would prevent ennuie. 
 It is well that Ted with his family are good friends with the St Kilda folk. 
 Good man Andy.  Should you see him convey my congratulations on his approaching marriage.  Such a step is a correct one for him to take. 
 The mails with you are not half as irregular with you as with us.  To get over all difficulties I write to you nearly every day and post on Saturday morning though the mail is not supposed to close until Monday & sometimes not then.  However my letters are in time to catch the first mail. 
 Sorry about Dr Barringtons son.  It may be that while I write (3 p.m. 29-4-15) you know who were killed and wounded at Gallipoli.  We but expect a great number of wounded in Egypt, the names of those killed & injured you will know of long before we do. 
 General Birdwood is a smart looking, small sized, sharp faced, soldier.  I have but saluted and been introduced to him. 
 Officers 