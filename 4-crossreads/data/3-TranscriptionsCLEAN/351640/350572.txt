  
 during each week for more than a few scrappy minutes devoted to me at Macdonalds, or Bridges, or home in a race with the moment when the letter box is to be closed. 
 On this sheet shall my pen stop for today, the envelope will surround it and its fellows, the post box will receive them, that with the first opportunity they may be on their way to the South & you. 
 I have written to Sir Matthew Harris, Hyman, & others.  The slackness of the day has given me the opportunity. 
 "After Summer succeeds Barren winter with his wrathful nipping cold; So cares and joys abound, as seasons fleet." 2 Hy. VI, ii-1. 
 "Whatever is great in human art is the impression of man's delight in God's work." The Two Paths - Ruskin. 
 Good bye!  Good bye!!!  Good bye!!!!! 
 May Fortune of her best smile upon each of you, & may all the places that you meet be ports & happy havens. 
 "Heaven from thy endless goodness, send prosperous life, long and ever happy to" Car, Joseph, Kitty, and their sister Buddie.  So wishes their affectionate & loving Father, 
 John B. Nash 
 The Misses Nash Macquarie Street Sydney N. S. Wales 
 [Sir Matthew Harris (1841-1917), alderman and free-trade politician, was president of Sydney Hospital from 1912 to 1917.] 
 