  
 [At the top of this and following pages of this letter is the letter M. Not transcribed.] 
 c/o D.M.S Cairo Egypt 26 May 1915 
 My Mollie dear:/ Your letter dated the 17th March, St Patricks day reached me yesterday, having been to London on the way.  Two months and eight days.   By the same mail came letters from the girls, one from each, dated up to 20.4-15, they were addressed direct to Mena. 
 Many thanks for the green harp, it is now fastened by a pin to the Shakspearian calendar in my room. 
 Glad to learn that up to the 17th March you, and the Sisters, had liked my letters, those relating to Mattarieh should be appreciated by any community of the R.C. Sister hoods .  Think you not so?   Letters come fairly regularly to hand now day. 
 Mena House is a very comfortable placed to live at.  The hot days are just coming, today 26.5-15 has been a real roaster, the worst day in Sydney could not equal it. 
 My climbing of the second pyramid Kephren was a something that very few young people are prepared to undertake, the coming down the great trouble as one has to face outwards and there is but little foot-hold or hand-hold. 
 Let me hope that all your new sisters are progressing famously, gradually taking in kindly manners to the new life upon which you have ventured.   Good luck to them all! 
 My Buddie's short legs have some trouble in reaching to the pedals of the organ.   What a game an organiste has with the feet, one does not know of this till he has watched a performer at close quarters.  You did well I know. Hurrah!   Hurrah!!!   Hurrah!!!!! [A line of Xs and Os.] 
 