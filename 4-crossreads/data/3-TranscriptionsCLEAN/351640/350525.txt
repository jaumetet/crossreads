  
 [At the top of this and following pages of this letter is the word "Girls". Not transcribed.] 
 Rails, about 18 inches apart, serve for what represents a tram car, drawn by a mule & donkey, or such like team. 
 Trees, other than palms, grow as if it they did not belong to the place. 
 The Europeans drinking coffee & other liquids at the cafes were not much to look at 
 5-30 p.m. - Leaving for Alexandria.  As I have not before been at any one of the mouths of the Nile I look forward to the call with interest. 
 14-1-15, 9 a.m. - Alexandria in sight.  Our good ship is bearing up towards the city.  Masts & funnels indicate the presence of a large number of sailing vessels and steamers.  Smoke from  stacks  chimney stacks bespeak work shops or factories. 
 10 a.m.  At anchor.  Once more in touch with civilisation as can be noted because the steamers have the coal poured into them by machinery.  The water cannot be deep as the screw at work stirred up mud  from  in large quantities, noxious mud too, much to the delight of the sea guls, which picked tit bits from the surface of the ooze. 
 11 A.M. - Mail bags aboard & Joe dear from you a letter dated 11-12-14, addressed to Fremantle.  Many thanks for it.  I thought that all my letters had gone on to London in the Malwa.  There would hardly be room for an oak tree in the estate outside our window.  Glad to learn that you were all well. 
 The advance of the Turks upon Egypt, the visit of Sir George Reid & Mr. MacKenzie to Cairo & other parts of Egypt, are the chief topics of interest here. 
 Colonel Martin has come to Cairo and we wait here is [his] return, when he comes we may know the next move to be played in our wanderings. 
 I fear me that Dr. Paton is not a good prophet.  Wish he was.  The war has not yet come to any of its great stages.  When the Summer of 1915 is opening we may expect to meet with the commencement 
 [Sir George Houstoun Reid (1845-1918), 12th premier of New South Wales and, after Federation, first Leader of the Opposition (1901-1903) and fourth Prime Minister of Australia (1904-1905), was appointed as Australia's first High Commissioner to London in 1909. From December 1914 to January 1915 he and Thomas Noble Mackenzie, former Prime Minister of New Zealand, and New Zealand's High Commissioner in London, visited Australian and New Zealand troops in Egypt. (Thomas Mackenzie was knighted in 1916.] 
 